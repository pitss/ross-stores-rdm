package com.ross.rdm.inventorymgmt.hhsplitcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.base.RDMMobileBaseBean;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMMobileBaseBean {
    private RichInputText inputFromContainerId;
    private RichInputText inputToContainerId;
    private RichInputText inputItemUpc;
    private RichInputText inputUnitQty;
    private RichInputText inputContainerQty;
    private RichIcon iconFromContainerId;
    private RichIcon iconToContainerId;
    private RichIcon iconItemUpc;
    private RichIcon iconUnitQty;
    private RichIcon iconContainerQty;
    private RichLink exitLink;
    private RichLink doneLink;
    private RichLink clearLink;
    private RichLink lotLink;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    private final static String SPLIT_ITERATOR_NAME = "HhSplitContainerSSplitContainerViewIterator";

    private final static String FROM_CONTAINER_COMPONENT_ID = "fro1";
    private final static String TO_CONTAINER_COMPONENT_ID = "toC1";
    private final static String ITEM_UPC_COMPONENT_ID = "ite2";
    // private final static String CONTAINER_QTY_COMPONENT_ID = "con1";
    private final static String UNIT_QTY_COMPONENT_ID = "uni1";

    private final static String FROM_CONTAINER_ITEM_ID = "Split_Container.From_Container_Id";
    private final static String TO_CONTAINER_ITEM_ID = "Split_Container.To_Container_Id";
    private final static String ITEM_UPC_ITEM_ID = "Split_Container.Item_UPC";
    private final static String UNIT_QTY_ITEM_ID = "Split_Container.Unit_Qty";
    private final static String CONTAINER_QTY_ITEM_ID = "Split_Container.Container_Qty";
    private RichLink hiddenFromContainerLink;
    private RichLink hiddenToContainerLink;
    private RichLink hiddenItemUpcLink;
    private RichLink hiddenUnitQtyLink;

    public Page1Backing() {

    }

    public void onChangedFromContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenFromContainerLink());
            actionEvent.queue();
        }
    }

    public String validateHiddenFromContainer() {
        if (Boolean.FALSE.equals(this.getPageFlowBean().getExecutedKey())) {
            OperationBinding oper = ADFUtils.findOperation("callValidateFromContainerId");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                this.showMessagesPanel(results.get(0), results.get(1));

                this.setErrorAndFocusOnInputFromContainerId();
            } else {
                this.hideMessagesPanel();

                this.clearInputFromContainerId(true);

                this.setFocusOnInputToContainerId(true);
            }
        } else {
            this.getPageFlowBean().setExecutedKey(Boolean.FALSE);
        }
        return null;
    }

    public void onChangedToContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenToContainerLink());
            actionEvent.queue();
        }
    }

    public String validateHiddenToContainer() {
        if (Boolean.FALSE.equals(this.getPageFlowBean().getExecutedKey())) {
            OperationBinding oper = ADFUtils.findOperation("callValidateToContainerId");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                if (CONF.equals(results.get(0))) {

                    this.clearInputToContainerId(true);

                    this.getDialogOutputText().setValue(results.get(1));
                    this.getConfirmPopup().setLauncherVar(null);
                    this.getConfirmPopup().show(new RichPopup.PopupHints());
                } else {
                    this.showMessagesPanel(results.get(0), results.get(1));

                    this.setErrorAndFocusOnInputToContainerId();
                }
            } else {
                this.hideMessagesPanel();

                this.clearInputToContainerId(true);

                this.setFocusOnInputItemUPC();
            }
        } else {
            this.getPageFlowBean().setExecutedKey(Boolean.FALSE);
        }
        return null;
    }

    public void onChangedItemUpc(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenItemUpcLink());
            actionEvent.queue();
        }
    }

    public String validateHiddenItemUpc() {
        if (Boolean.FALSE.equals(this.getPageFlowBean().getExecutedKey())) {
            OperationBinding oper = ADFUtils.findOperation("callValidateItemUPC");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                this.showMessagesPanel(results.get(0), results.get(1));

                this.setErrorAndFocusOnInputItemUPC();
            } else {
                this.hideMessagesPanel();

                this.clearInputItemUPC(true);

                this.setFocusOnInputUnitQty();
            }
        } else {
            this.getPageFlowBean().setExecutedKey(Boolean.FALSE);
        }
        return null;
    }

    public void onChangedUnitQty(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenUnitQtyLink());
            actionEvent.queue();
        }
    }

    public String validateHiddenUnitQty() {
        if (Boolean.FALSE.equals(this.getPageFlowBean().getExecutedKey())) {
            validateUnitQty();
        } else {
            this.getPageFlowBean().setExecutedKey(Boolean.FALSE);
        }
        return null;
    }

    private boolean validateUnitQty() {
        boolean isValid = true;
        String newValue = (String) ADFUtils.getBoundAttributeValue("UnitQty");
        if (newValue == null || "".equals(newValue)) {
            this.showMessagesPanel(WARN, this.getMessage("INV_QTY"));
            this.setErrorAndFocusOnInputUnitQty();
            isValid = false;
        } else if (!newValue.matches("[0-9]+")) {
            this.showMessagesPanel(WARN, this.getMessage("MUST_BE_NUMERIC"));
            this.setErrorAndFocusOnInputUnitQty();
            isValid = false;
        } else {
            OperationBinding oper = ADFUtils.findOperation("callValidateUnitQtySplitBlock");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                this.showMessagesPanel(results.get(0), results.get(1));
                this.setErrorAndFocusOnInputUnitQty();
                isValid = false;
            } else {
                this.hideMessagesPanel();
                this.clearInputUnitQty(true);
                this.setFocusOnInputFromContainerId();
            }
        }
        return isValid;
    }

    public String exitAction() {
        // TODO -> :GLOBAL.To_Container :=NULL; ???
        // TODO -> :GLOBAL.Container_id := NULL; ???
        return this.logoutExitBTF();
    }

    public String doneAction() {

        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_split_container_s_SPLIT_CONTAINER_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_split_container_s)) {
            if (validateCurrentField()) {
                OperationBinding oper = ADFUtils.findOperation("callF4DoneFromPage1");
                List<String> results = (List<String>) oper.execute();
                if (results != null && !results.isEmpty()) {
                    if (results.size() == 1) {
                        action = results.get(0);
                    } else if (results.size() >= 2) {
                        if (CONF.equals(results.get(0))) {
                            this.clearInputFromContainerId(true);
                            this.clearInputToContainerId(true);
                            this.clearInputItemUPC(true);
                            this.clearInputUnitQty(true);
                            this.clearInputContainerQty(true);

                            this.getDialogOutputText().setValue(results.get(1));
                            this.getConfirmPopup().setLauncherVar("F4");
                            this.getConfirmPopup().show(new RichPopup.PopupHints());
                        } else if (MSGTYPE_M.equals(results.get(0))) {
                            this.showMessagesPanel(results.get(0), results.get(1));

                            this.clearFormAfterSuccess();
                        } else {
                            this.showMessagesPanel(results.get(0), results.get(1));

                            if (results.size() == 3) {
                                this.setErrorAndFocusOnItem(results.get(2));
                            } else {
                                this.setErrorAndFocusOnItem(this.getCurrentFocusItem());
                            }
                        }
                    }
                }
            }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }

    private void clearFormAfterSuccess() {
        this.clearInputFromContainerId(true);
        ADFUtils.setBoundAttributeValue("ItemUpc", null);
        this.clearInputItemUPC(true);
        ADFUtils.setBoundAttributeValue("ContainerQty", new BigDecimal(1));
        this.clearInputContainerQty(true);
        ADFUtils.setBoundAttributeValue("UnitQty", null);
        this.clearInputUnitQty(true);
        ADFUtils.setBoundAttributeValue("ToContainerId", null);
        this.clearInputToContainerId(false);
        this.setFocusOnInputToContainerId(false);
    }

    public String clearAction() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_split_container_s_SPLIT_CONTAINER_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_split_container_s)){
        OperationBinding oper = null;
        if (ADFUtils.findIterator(SPLIT_ITERATOR_NAME).getCurrentRow() != null) {
            oper = ADFUtils.findOperation("clearBlock");
        } else {
            oper = ADFUtils.findOperation("initSplitContainerS");
        }
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            this.hideMessagesPanel();

            this.clearInputToContainerId(true);
            this.clearInputItemUPC(true);
            this.clearInputUnitQty(true);
            this.clearInputContainerQty(true);
            ADFUtils.setBoundAttributeValue("ContainerQty", new BigDecimal(1));
            this.refreshContentOfUIComponent(this.getInputContainerQty());

            this.clearInputFromContainerId(false);
            this.setFocusOnInputFromContainerId();
        }
        }
        else{
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }

    @Deprecated
    public String lotAction() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_split_container_s_SPLIT_CONTAINER_F9,
                                                RoleBasedAccessConstants.FORM_NAME_hh_split_container_s)){
        /* PKG_INVENTORY_MGMT.ASSIGN_LOT_NBR(P_DISPLAY.V_DISPLAY_STRING , P_DISPLAY.V_MSG_DISPLAY, :GLOBAL.TO_CONTAINER
         * ,:SPLIT_CONTAINER.FROM_CONTAINER_ID ,:SPLIT_CONTAINER.ITEM_ID ,:SPLIT_CONTAINER.TO_CONTAINER_ID ,
         * :WORK.FACILITY_ID ,:SYSTEM.CURRENT_FORM  );
        IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
        CALL_NAUTILUS_FORM('hh_lot_tracking_s');
        AHL.SET_AHL_INFO(:WORK.FACILITY_ID ,:SYSTEM.CURRENT_FORM); */
        }
        else{
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }

    public String yesLinkConfirmPopupAction() {
        String action = null;
        this.getConfirmPopup().hide();
        if (YES.equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("UpdateContainerTrans"))) {
            OperationBinding oper = ADFUtils.findOperation("callUpdateBolNbrInContainer");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                this.showMessagesPanel(results.get(0), results.get(1));

                this.setErrorAndFocusOnItem(this.getCurrentFocusItem());
            } else {
                if ("F4".equals(this.getConfirmPopup().getLauncherVar())) {
                    action = this.callSplitContainer();
                } else {
                    this.hideMessagesPanel();
                    this.setFocusOnInputItemUPC();
                }
            }
        } else {
            if ("F4".equals(this.getConfirmPopup().getLauncherVar())) {
                action = this.callSplitContainer();
            } else {
                this.hideMessagesPanel();
                this.setFocusOnInputItemUPC();
            }
        }
        return action;
    }

    private String callSplitContainer() {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation("callSplitContainerF");
        List<String> results = (List<String>) oper.execute();
        if (results != null && !results.isEmpty()) {
            if (results.size() == 1) {
                action = results.get(0);
            } else if (results.size() >= 2) {
                this.showMessagesPanel(results.get(0), results.get(1));
                if (MSGTYPE_M.equals(results.get(0))) {
                    this.clearFormAfterSuccess();
                } else if (results.size() == 3) {
                    this.setErrorAndFocusOnItem(results.get(2));
                }
            }
        }
        return action;
    }

    public String noLinkConfirmPopupAction() {
        this.getConfirmPopup().hide();

        this.setFocusAndEnabledCurrentFocusField();

        return null;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String item = (String) clientEvent.getParameters().get("item");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");

            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (FROM_CONTAINER_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputFromContainerId().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputFromContainerId().getValue())) {
                        this.onChangedFromContainerId(new ValueChangeEvent(this.getInputFromContainerId(), null,
                                                                           submittedValue));
                    }
                } else if (TO_CONTAINER_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputToContainerId().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputToContainerId().getValue())) {
                        this.onChangedToContainerId(new ValueChangeEvent(this.getInputToContainerId(), null,
                                                                         submittedValue));
                    }
                } else if (ITEM_UPC_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputItemUpc().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputItemUpc().getValue())) {
                        this.onChangedItemUpc(new ValueChangeEvent(this.getInputItemUpc(), null, submittedValue));
                    }
                } else if (UNIT_QTY_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputUnitQty().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputUnitQty().getValue())) {
                        this.onChangedUnitQty(new ValueChangeEvent(this.getInputUnitQty(), null, submittedValue));
                    }
                }
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getClearLink());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                /* ActionEvent actionEvent = new ActionEvent(this.getLotLink());
                actionEvent.queue(); */
            }
        }
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void setFocusOnInputFromContainerId() {
        this.showMessagesPanel(INFO, "Enter From Container ID");
        this.setCurrentFocusItem(FROM_CONTAINER_ITEM_ID);
        this.getInputFromContainerId().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputFromContainerId());
    }

    private void setFocusOnInputToContainerId(boolean showHint) {
        if (showHint) {
            this.showMessagesPanel(INFO, "Enter To Container ID");
        }
        this.setCurrentFocusItem(TO_CONTAINER_ITEM_ID);
        this.getInputToContainerId().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputToContainerId());
    }

    private void setFocusOnInputItemUPC() {
        this.showMessagesPanel(INFO, "Enter Item ID");
        this.setCurrentFocusItem(ITEM_UPC_ITEM_ID);
        this.getInputItemUpc().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputItemUpc());
    }

    private void setFocusOnInputUnitQty() {
        this.showMessagesPanel(INFO, "Enter Unit Qty");
        this.setCurrentFocusItem(UNIT_QTY_ITEM_ID);
        this.getInputUnitQty().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputUnitQty());
    }

    private void setFocusOnInputContainerQty() {
        this.showMessagesPanel(INFO, "Enter Container Qty");
        this.setCurrentFocusItem(CONTAINER_QTY_ITEM_ID);
        this.getInputContainerQty().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputContainerQty());
    }

    private void setErrorAndFocusOnInputFromContainerId() {
        this.getIconFromContainerId().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconFromContainerId());
        this.addErrorStyleToComponent(this.getInputFromContainerId());
        this.setCurrentFocusItem(FROM_CONTAINER_ITEM_ID);
        this.getInputFromContainerId().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputFromContainerId());
    }

    private void setErrorAndFocusOnInputToContainerId() {
        this.getIconToContainerId().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconToContainerId());
        this.addErrorStyleToComponent(this.getInputToContainerId());
        this.setCurrentFocusItem(TO_CONTAINER_ITEM_ID);
        this.getInputToContainerId().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputToContainerId());
    }

    private void setErrorAndFocusOnInputItemUPC() {
        this.getIconItemUpc().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconItemUpc());
        this.addErrorStyleToComponent(this.getInputItemUpc());
        this.setCurrentFocusItem(ITEM_UPC_ITEM_ID);
        this.getInputItemUpc().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputItemUpc());
    }

    private void setErrorAndFocusOnInputUnitQty() {
        this.getIconUnitQty().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconUnitQty());
        this.addErrorStyleToComponent(this.getInputUnitQty());
        this.setCurrentFocusItem(UNIT_QTY_ITEM_ID);
        this.getInputUnitQty().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputUnitQty());
    }

    private void setErrorAndFocusOnInputContainerQty() {
        this.getIconContainerQty().setVisible(true);
        this.refreshContentOfUIComponent(this.getIconContainerQty());
        this.addErrorStyleToComponent(this.getInputContainerQty());
        this.setCurrentFocusItem(CONTAINER_QTY_ITEM_ID);
        this.getInputContainerQty().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputContainerQty());
    }

    private void clearInputFromContainerId(boolean disabled) {
        this.getInputFromContainerId().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputFromContainerId());
        this.getIconFromContainerId().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconFromContainerId());
    }

    private void clearInputToContainerId(boolean disabled) {
        this.getInputToContainerId().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputToContainerId());
        this.getIconToContainerId().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconToContainerId());
    }

    private void clearInputItemUPC(boolean disabled) {
        this.getInputItemUpc().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputItemUpc());
        this.getIconItemUpc().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconItemUpc());
    }

    private void clearInputUnitQty(boolean disabled) {
        this.getInputUnitQty().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputUnitQty());
        this.getIconUnitQty().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconUnitQty());
    }

    private void clearInputContainerQty(boolean disabled) {
        this.getInputContainerQty().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputContainerQty());
        this.getIconContainerQty().setVisible(false);
        this.refreshContentOfUIComponent(this.getIconContainerQty());
    }

    private String getCurrentFocusItem() {
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        return pfBean.getFocusItemPage1();
    }

    private void setCurrentFocusItem(String itemId) {
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        pfBean.setFocusItemPage1(itemId);
    }

    private HhSplitContainerSBean getPageFlowBean() {
        return (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
    }

    private void setFocusAndEnabledCurrentFocusField() {
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        if (FROM_CONTAINER_ITEM_ID.equalsIgnoreCase(pfBean.getFocusItemPage1())) {
            this.setFocusOnInputFromContainerId();
        } else if (TO_CONTAINER_ITEM_ID.equalsIgnoreCase(pfBean.getFocusItemPage1())) {
            this.setFocusOnInputToContainerId(true);
        } else if (ITEM_UPC_ITEM_ID.equalsIgnoreCase(pfBean.getFocusItemPage1())) {
            this.setFocusOnInputItemUPC();
        } else if (UNIT_QTY_ITEM_ID.equalsIgnoreCase(pfBean.getFocusItemPage1())) {
            this.setFocusOnInputUnitQty();
        } else if (CONTAINER_QTY_ITEM_ID.equalsIgnoreCase(pfBean.getFocusItemPage1())) {
            this.setFocusOnInputContainerQty();
        }
    }

    private void setErrorAndFocusOnItem(String itemId) {
        if (FROM_CONTAINER_ITEM_ID.equalsIgnoreCase(itemId)) {
            this.setErrorAndFocusOnInputFromContainerId();
        } else if (TO_CONTAINER_ITEM_ID.equalsIgnoreCase(itemId)) {
            this.setErrorAndFocusOnInputToContainerId();
        } else if (ITEM_UPC_ITEM_ID.equalsIgnoreCase(itemId)) {
            this.setErrorAndFocusOnInputItemUPC();
        } else if (UNIT_QTY_ITEM_ID.equalsIgnoreCase(itemId)) {
            this.setErrorAndFocusOnInputUnitQty();
        } else if (CONTAINER_QTY_ITEM_ID.equalsIgnoreCase(itemId)) {
            this.setErrorAndFocusOnInputContainerQty();
        }
    }

    private boolean validateCurrentField() {
        boolean validationOK = true;
        String currentField = this.getCurrentFocusItem();
        if (ITEM_UPC_ITEM_ID.equalsIgnoreCase(currentField)) {
            OperationBinding oper = ADFUtils.findOperation("callValidateItemUPC");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                validationOK = false;
                this.showMessagesPanel(results.get(0), results.get(1));
                this.setErrorAndFocusOnInputItemUPC();
            } else {
                this.hideMessagesPanel();
                this.clearInputItemUPC(true);
            }
        } else if (UNIT_QTY_ITEM_ID.equalsIgnoreCase(currentField)) {
            validationOK = validateUnitQty();
        }
        return validationOK;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }

    public void setClearLink(RichLink clearLink) {
        this.clearLink = clearLink;
    }

    public RichLink getClearLink() {
        return clearLink;
    }

    public void setLotLink(RichLink lotLink) {
        this.lotLink = lotLink;
    }

    public RichLink getLotLink() {
        return lotLink;
    }

    public void setInputFromContainerId(RichInputText inputFromContainerId) {
        this.inputFromContainerId = inputFromContainerId;
    }

    public RichInputText getInputFromContainerId() {
        return inputFromContainerId;
    }

    public void setInputToContainerId(RichInputText inputToContainerId) {
        this.inputToContainerId = inputToContainerId;
    }

    public RichInputText getInputToContainerId() {
        return inputToContainerId;
    }

    public void setInputItemUpc(RichInputText inputItemUpc) {
        this.inputItemUpc = inputItemUpc;
    }

    public RichInputText getInputItemUpc() {
        return inputItemUpc;
    }

    public void setInputUnitQty(RichInputText inputUnitQty) {
        this.inputUnitQty = inputUnitQty;
    }

    public RichInputText getInputUnitQty() {
        return inputUnitQty;
    }

    public void setInputContainerQty(RichInputText inputContainerQty) {
        this.inputContainerQty = inputContainerQty;
    }

    public RichInputText getInputContainerQty() {
        return inputContainerQty;
    }

    public void setIconFromContainerId(RichIcon iconFromContainerId) {
        this.iconFromContainerId = iconFromContainerId;
    }

    public RichIcon getIconFromContainerId() {
        return iconFromContainerId;
    }

    public void setIconToContainerId(RichIcon iconToContainerId) {
        this.iconToContainerId = iconToContainerId;
    }

    public RichIcon getIconToContainerId() {
        return iconToContainerId;
    }

    public void setIconItemUpc(RichIcon iconItemUpc) {
        this.iconItemUpc = iconItemUpc;
    }

    public RichIcon getIconItemUpc() {
        return iconItemUpc;
    }

    public void setIconUnitQty(RichIcon iconUnitQty) {
        this.iconUnitQty = iconUnitQty;
    }

    public RichIcon getIconUnitQty() {
        return iconUnitQty;
    }

    public void setIconContainerQty(RichIcon iconContainerQty) {
        this.iconContainerQty = iconContainerQty;
    }

    public RichIcon getIconContainerQty() {
        return iconContainerQty;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (StringUtils.isEmpty((String) this.getErrorWarnInfoMessage().getValue())) {
            this.disableAllFields();
            this.setFocusAndEnabledCurrentFocusField();
        }
    }

    private void disableAllFields() {
        getInputFromContainerId().setDisabled(true);
        this.refreshContentOfUIComponent(getInputFromContainerId());
        getInputToContainerId().setDisabled(true);
        this.refreshContentOfUIComponent(getInputToContainerId());
        getInputItemUpc().setDisabled(true);
        this.refreshContentOfUIComponent(getInputItemUpc());
        getInputUnitQty().setDisabled(true);
        this.refreshContentOfUIComponent(getInputUnitQty());
        getInputContainerQty().setDisabled(true);
        this.refreshContentOfUIComponent(getInputContainerQty());
    }

    public void setHiddenFromContainerLink(RichLink hiddenFromContainerLink) {
        this.hiddenFromContainerLink = hiddenFromContainerLink;
    }

    public RichLink getHiddenFromContainerLink() {
        return hiddenFromContainerLink;
    }

    public void setHiddenToContainerLink(RichLink hiddenToContainerLink) {
        this.hiddenToContainerLink = hiddenToContainerLink;
    }

    public RichLink getHiddenToContainerLink() {
        return hiddenToContainerLink;
    }

    public void setHiddenItemUpcLink(RichLink hiddenItemUpcLink) {
        this.hiddenItemUpcLink = hiddenItemUpcLink;
    }

    public RichLink getHiddenItemUpcLink() {
        return hiddenItemUpcLink;
    }

    public void setHiddenUnitQtyLink(RichLink hiddenUnitQtyLink) {
        this.hiddenUnitQtyLink = hiddenUnitQtyLink;
    }

    public RichLink getHiddenUnitQtyLink() {
        return hiddenUnitQtyLink;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_inventorymgmt_hhsplitcontainers_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }
}
