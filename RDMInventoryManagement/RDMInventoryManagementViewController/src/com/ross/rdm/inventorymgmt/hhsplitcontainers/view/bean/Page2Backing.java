package com.ross.rdm.inventorymgmt.hhsplitcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.base.RDMMobileBaseBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMMobileBaseBean {

    // private RichSelectOneChoice lovReasonCode;
    private RichInputText inputReasonCode;
    private RichInputText inputUnitQty;
    private RichInputText inputMantain;
    private RichIcon iconReasonCode;
    private RichIcon iconUnitQty;
    private RichIcon iconMantain;
    private RichLink doneLink;
    private RichPopup confirmPopup;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;    
    private RichPopup succesPopup;
    private RichLink okLinkPopup;
    
    private RichLink hiddenReasonCodeLink;
    private RichLink hiddenUnitQtyLink;
    private RichLink hiddenMantainLink;
    
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    private final static String REASON_CODE_COMPONENT_ID = "reaCod"; // splitLov
    private final static String UNIT_QTY_COMPONENT_ID = "uni2";
    private final static String MANTAIN_COMPONENT_ID = "mai1";
    
    private final static String REASON_CODE_ITEM_ID = "RETURN_REASON.REASON_CODE";
    private final static String UNIT_QTY_ITEM_ID = "RETURN_REASON.UNIT_QTY";
    private final static String MANTAIN_ITEM_ID = "RETURN_REASON.MAINTAIN";
    private RichPanelFormLayout mainPanel;

    public Page2Backing() {

    }

    public void onChangedReasonCode(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
            pfBean.setExecutedKeyPage2(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenReasonCodeLink());
            actionEvent.queue();
        }
    }
    
    public String validateHiddenReasonCode() {
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        if (Boolean.FALSE.equals(pfBean.getExecutedKeyPage2())) {
            OperationBinding oper = ADFUtils.findOperation("callValidateReasonCode");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                this.showMessagesPanel(results.get(0), results.get(1));

                this.setErrorAndFocusOnReasonCode();
            } else {
                this.hideMessagesPanel();

                this.clearReasonCode(true);

                this.setFocusOnInputUnitQty();
            }
        }
        else{
            pfBean.setExecutedKeyPage2(false);
        }
        return null;
    }

    public void onChangedUnitQty(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
            pfBean.setExecutedKeyPage2(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenUnitQtyLink());
            actionEvent.queue();
        }
    }

    public String validateHiddenUnitQty() {
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        if (Boolean.FALSE.equals(pfBean.getExecutedKeyPage2())) {
            String newValue = (String) ADFUtils.getBoundAttributeValue("UnitQty");
            if(newValue != null){
                if (!newValue.matches("[0-9]+")) {
                    this.showMessagesPanel(WARN, this.getMessage("MUST_BE_NUMERIC"));
                    
                    this.setErrorAndFocusOnInputUnitQty();
                }
                else{
                    OperationBinding oper = ADFUtils.findOperation("callValidateReturnUnitQty");
                    List<String> results = (List<String>) oper.execute();
                    if (results != null && !results.isEmpty()) {
                        this.showMessagesPanel(results.get(0), results.get(1));

                        this.setErrorAndFocusOnInputUnitQty();
                    } else {
                        this.hideMessagesPanel();

                        this.clearInputUnitQty(true);

                        this.setFocusOnInputMantain();
                    }
                }
            }
        }
        else{
            pfBean.setExecutedKeyPage2(false);
        }
        return null;
    }

    public void onChangedMaintain(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
            pfBean.setExecutedKeyPage2(false);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenMantainLink());
            actionEvent.queue();
        }
    }

    public String validateHiddenMaintain() {
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        if (Boolean.FALSE.equals(pfBean.getExecutedKeyPage2())) {
            if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("Maintain"))) {
                OperationBinding oper = ADFUtils.findOperation("callValidateMaintain");
                List<String> results = (List<String>) oper.execute();
                if (results != null && !results.isEmpty()) {
                    this.showMessagesPanel(results.get(0), results.get(1));
                    if (results.size() == 2) {
                        this.setErrorAndFocusOnInputMantain();
                    } else {
                        this.clearInputMantain(true);
                        this.setErrorAndFocusOnReasonCode();
                    }
                } else {
                    this.hideMessagesPanel();

                    this.clearInputMantain(true);

                    this.setFocusOnReasonCode();
                }
            }
        }
        else{
            pfBean.setExecutedKeyPage2(false);
        }
        return null;
    }

    public String doneAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_split_container_s_RETURN_REASON_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_split_container_s)){
        OperationBinding oper = ADFUtils.findOperation("callF4DoneFromPage2");
        List<String> results = (List<String>) oper.execute();
        if (results != null && !results.isEmpty()){
            this.showMessagesPanel(results.get(0), results.get(1));

            this.setErrorAndFocusOnCurrentField();
        }
        else{
            this.clearReasonCode(true);
            this.clearInputUnitQty(true);
            this.clearInputMantain(true);
            
            this.getConfirmPopup().show(new RichPopup.PopupHints());
        }
                                                } 
        else{
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }
    
    public String yesLinkConfirmPopupAction(){
        this.getConfirmPopup().hide();
        /* OperationBinding oper = ADFUtils.findOperation("reCallMantainReasonCodes");
        List<String> results = (List<String>) oper.execute();
        if(oper.getErrors() == null || oper.getErrors().isEmpty()){
            if(results == null){
                ADFUtils.setBoundAttributeValue("ToContainerId", null);
                ADFUtils.setBoundAttributeValue("ItemId", null);
                ADFUtils.setBoundAttributeValue("ItemUpc", null);
                ADFUtils.setBoundAttributeValue("UnitQtySplitBlock", null);
                
                HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
                pfBean.setFocusItemPage1("Split_Container.To_Container_ID");
                
                this.getSuccesPopup().show(new RichPopup.PopupHints());
            }
            else{
                this.showMessagesPanel(results.get(0), results.get(1));

                this.setErrorAndFocusOnCurrentField();
            }
        } */        
        OperationBinding oper = ADFUtils.findOperation("callDoCommit");
        oper.execute();
        if(oper.getErrors() == null || oper.getErrors().isEmpty()){
            ADFUtils.setBoundAttributeValue("ToContainerId", null);
            ADFUtils.setBoundAttributeValue("ItemId", null);
            ADFUtils.setBoundAttributeValue("ItemUpc", null);
            ADFUtils.setBoundAttributeValue("UnitQtySplitBlock", null);
            
            HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
            pfBean.setFocusItemPage1("Split_Container.To_Container_ID");
            
            this.getSuccesPopup().show(new RichPopup.PopupHints());
        }
        return null;
    }
    
    public String noLinkConfirmPopupAction(){
        this.getConfirmPopup().hide();
        ADFUtils.setBoundAttributeValue("ReasonCode", null);
        ADFUtils.setBoundAttributeValue("ReturnGroup", null);
        ADFUtils.setBoundAttributeValue("UnitQty", null);
        ADFUtils.setBoundAttributeValue("Maintain", YES);
        
        this.refreshContentOfUIComponent(this.getMainPanel());
        
        this.setFocusOnReasonCode();
        
        return null;
    }
    
    public String yesLinkConfirmSuccessPopupAction(){
        this.getSuccesPopup().hide();
        return "Page1";
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String item = (String) clientEvent.getParameters().get("item");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");

            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (REASON_CODE_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputReasonCode().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputReasonCode().getValue())) {
                        this.onChangedReasonCode(new ValueChangeEvent(this.getInputReasonCode(), null, submittedValue));
                    }
                } else if (UNIT_QTY_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputUnitQty().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputUnitQty().getValue())) {
                        this.onChangedUnitQty(new ValueChangeEvent(this.getInputUnitQty(), null, submittedValue));
                    }
                } else if (MANTAIN_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputMantain().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputMantain().getValue())) {
                        this.onChangedMaintain(new ValueChangeEvent(this.getInputMantain(), null, submittedValue));
                    }
                }
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeySuccessPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOkLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    private void setFocusOnReasonCode() {
        this.showMessagesPanel(INFO, "Enter Reason Code");
        this.setCurrentFocusItem(REASON_CODE_ITEM_ID);
        this.getInputReasonCode().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputReasonCode());
    }
    
    private void setFocusOnInputUnitQty() {
        this.showMessagesPanel(INFO, "Enter Unit Qty");
        this.setCurrentFocusItem(UNIT_QTY_ITEM_ID);
        this.getInputUnitQty().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputUnitQty());
    }
    
    private void setFocusOnInputMantain() {
        this.showMessagesPanel(INFO, "'Y'es / 'N'o");
        this.setCurrentFocusItem(MANTAIN_ITEM_ID);
        this.getInputMantain().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputMantain());
    }
    
    private void setErrorAndFocusOnReasonCode() {
        this.getIconReasonCode().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconReasonCode());
        // this.addErrorStyleToChoiceComponent(this.getLovReasonCode());
        this.addErrorStyleToComponent(this.getInputReasonCode());
        this.setCurrentFocusItem(REASON_CODE_ITEM_ID);
        this.getInputReasonCode().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputReasonCode());
    }
    
    private void setErrorAndFocusOnInputUnitQty() {
        this.getIconUnitQty().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconUnitQty());
        this.addErrorStyleToComponent(this.getInputUnitQty());
        this.setCurrentFocusItem(UNIT_QTY_ITEM_ID);
        this.getInputUnitQty().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputUnitQty());
    }
    
    private void setErrorAndFocusOnInputMantain() {
        this.getIconMantain().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconMantain());
        this.addErrorStyleToComponent(this.getInputMantain());
        this.setCurrentFocusItem(MANTAIN_ITEM_ID);
        this.getInputMantain().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputMantain());
    }
    
    private void setErrorAndFocusOnCurrentField(){
        if(REASON_CODE_ITEM_ID.equalsIgnoreCase(this.getCurrentFocusItem())){
            this.setErrorAndFocusOnReasonCode();
        }
        else if(UNIT_QTY_ITEM_ID.equalsIgnoreCase(this.getCurrentFocusItem())){
            this.setErrorAndFocusOnInputUnitQty();
        }
        else if(MANTAIN_ITEM_ID.equalsIgnoreCase(this.getCurrentFocusItem())){
            this.setErrorAndFocusOnInputMantain();
        }
    }
    
    private void clearReasonCode(boolean disabled) {
        this.getInputReasonCode().setDisabled(disabled);
        // this.removeErrorStyleToChoiceComponent(this.getLovReasonCode());
        this.removeErrorStyleToComponent(this.getInputReasonCode());
        this.getIconReasonCode().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconReasonCode());
    }
    
    private void clearInputUnitQty(boolean disabled) {
        this.getInputUnitQty().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputUnitQty());
        this.getIconUnitQty().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconUnitQty());
    }
    
    private void clearInputMantain(boolean disabled) {
        this.getInputMantain().setDisabled(disabled);
        this.removeErrorStyleToComponent(this.getInputMantain());
        this.getIconMantain().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconMantain());
    }
    
    private String getCurrentFocusItem(){
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        return pfBean.getFocusItemPage2();
    }
    
    private void setCurrentFocusItem(String itemId){
        HhSplitContainerSBean pfBean = (HhSplitContainerSBean) this.getPageFlowBean("HhSplitContainerSBean");
        pfBean.setFocusItemPage2(itemId);
    }
    
    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    /* public void setLovReasonCode(RichSelectOneChoice lovReasonCode) {
        this.lovReasonCode = lovReasonCode;
    }

    public RichSelectOneChoice getLovReasonCode() {
        return lovReasonCode;
    } */

    public void setInputUnitQty(RichInputText inputUnitQty) {
        this.inputUnitQty = inputUnitQty;
    }

    public RichInputText getInputUnitQty() {
        return inputUnitQty;
    }

    public void setInputMantain(RichInputText inputMantain) {
        this.inputMantain = inputMantain;
    }

    public RichInputText getInputMantain() {
        return inputMantain;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }
    
    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }
    
    public void setIconReasonCode(RichIcon iconReasonCode) {
        this.iconReasonCode = iconReasonCode;
    }

    public RichIcon getIconReasonCode() {
        return iconReasonCode;
    }

    public void setIconUnitQty(RichIcon iconUnitQty) {
        this.iconUnitQty = iconUnitQty;
    }

    public RichIcon getIconUnitQty() {
        return iconUnitQty;
    }

    public void setIconMantain(RichIcon iconMantain) {
        this.iconMantain = iconMantain;
    }

    public RichIcon getIconMantain() {
        return iconMantain;
    }
    
    public void setSuccesPopup(RichPopup succesPopup) {
        this.succesPopup = succesPopup;
    }

    public RichPopup getSuccesPopup() {
        return succesPopup;
    }

    public void setOkLinkPopup(RichLink okLinkPopup) {
        this.okLinkPopup = okLinkPopup;
    }

    public RichLink getOkLinkPopup() {
        return okLinkPopup;
    }
    
    public void setInputReasonCode(RichInputText inputReasonCode) {
        this.inputReasonCode = inputReasonCode;
    }

    public RichInputText getInputReasonCode() {
        return inputReasonCode;
    }
    
    public void setHiddenReasonCodeLink(RichLink hiddenReasonCodeLink) {
        this.hiddenReasonCodeLink = hiddenReasonCodeLink;
    }

    public RichLink getHiddenReasonCodeLink() {
        return hiddenReasonCodeLink;
    }

    public void setHiddenUnitQtyLink(RichLink hiddenUnitQtyLink) {
        this.hiddenUnitQtyLink = hiddenUnitQtyLink;
    }

    public RichLink getHiddenUnitQtyLink() {
        return hiddenUnitQtyLink;
    }

    public void setHiddenMantainLink(RichLink hiddenMantainLink) {
        this.hiddenMantainLink = hiddenMantainLink;
    }

    public RichLink getHiddenMantainLink() {
        return hiddenMantainLink;
    }

    public void setMainPanel(RichPanelFormLayout mainPanel) {
        this.mainPanel = mainPanel;
    }

    public RichPanelFormLayout getMainPanel() {
        return mainPanel;
    }
    
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {        
        if(StringUtils.isEmpty((String) this.getErrorWarnInfoMessage().getValue())){
            this.setFocusOnReasonCode();
        }
    }
    
    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_inventorymgmt_hhsplitcontainers_view_pageDefs_Page2PageDef")) {
            navigation = true;
        }
        return navigation;
    }
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }
}
