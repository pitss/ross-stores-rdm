package com.ross.rdm.inventorymgmt.hhsplitcontainers.view.bean;

import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhSplitContainerSBean extends RDMHotelPickingPageFlowBean {
    @SuppressWarnings("compatibility:-2316260211346289608")
    private static final long serialVersionUID = 1L;
    
    private String focusItemPage1;
    private String focusItemPage2;
    private Boolean executedKey;
    private Boolean executedKeyPage2;

    private final static String FROM_CONTAINER_ITEM_ID = "Split_Container.From_Container_Id";
    private final static String REASON_CODE_ITEM_ID = "RETURN_REASON.REASON_CODE";
    
    public HhSplitContainerSBean() {
        super();
        this.focusItemPage1 = FROM_CONTAINER_ITEM_ID;
        this.focusItemPage2 = REASON_CODE_ITEM_ID;
        this.executedKey = Boolean.FALSE;
        this.executedKeyPage2 = Boolean.FALSE;
    }

    public void setFocusItemPage1(String focusItemPage1) {
        this.focusItemPage1 = focusItemPage1;
    }

    public String getFocusItemPage1() {
        return focusItemPage1;
    }

    public void setFocusItemPage2(String focusItemPage2) {
        this.focusItemPage2 = focusItemPage2;
    }

    public String getFocusItemPage2() {
        return focusItemPage2;
    }
    
    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
    
    public void setExecutedKeyPage2(Boolean executedKeyPage2) {
        this.executedKeyPage2 = executedKeyPage2;
    }

    public Boolean getExecutedKeyPage2() {
        return executedKeyPage2;
    }
}
