package com.ross.rdm.inventorymgmt.hhinvalidcontainers.view.bean;

import javax.faces.component.UIComponent;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhInvalidContainerSBean  {


    public HhInvalidContainerSBean() {

        
    }
      public void initTaskFlow() {

            //Forms  Module : hh_invalid_container_s
            //Object        : WHEN-NEW-FORM-INSTANCE
            //Source Code   : 
            //
            //Tr_Startup;
            //
            //startup_local;
            //AHL.SET_AHL_INFO(:WORK.FACILITY_ID, :SYSTEM.CURRENT_FORM);
            //

}
      
    private UIComponent currentItem;
    private UIComponent lastItem;
    private String currentItemId;

    public void setCurrentItemId(String currentItemId) {
        this.currentItemId = currentItemId;
    }

    public String getCurrentItemId() {
        return currentItemId;
    }


    public void setLastItem(UIComponent lastItem) {
        this.lastItem = lastItem;
    }

    public UIComponent getLastItem() {
        return lastItem;
    }

    public void setCurrentItem(UIComponent currentItem) {
        this.currentItem = currentItem;
        this.currentItemId = currentItem.getId();        
    }

    public UIComponent getCurrentItem() {
        return currentItem;
    }
    



}
