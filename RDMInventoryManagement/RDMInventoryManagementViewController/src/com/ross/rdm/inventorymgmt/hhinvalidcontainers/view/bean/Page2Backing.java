package com.ross.rdm.inventorymgmt.hhinvalidcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.util.List;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page2.jspx
// ---    
// ---------------------------------------------------------------------
public class Page2Backing extends RDMInventoryManagementBackingBean {
  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon errorWarnInfoIcon;
  private RichOutputText errorWarnInfoMessage;
    private RichLink f3ExitLink;
    private RichLink f4SelectLink;
    private RichLink f10NextLink;
    private RichLink f9PrevLink;

    public Page2Backing() {

  }

  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()  {
    return allMessagesPanel;
  }
  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()  {
    return errorWarnInfoIcon;
  }
  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()  {
    return errorWarnInfoMessage;
  }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public String f3ExitAction() {
        // Add event code here...
        return "goPage1";
    }

    public void setF4SelectLink(RichLink f4SelectLink) {
        this.f4SelectLink = f4SelectLink;
    }

    public RichLink getF4SelectLink() {
        return f4SelectLink;
    }

    public String f4SelectAction() {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_invalid_container_s_DETAIL_F4,
                                           RoleBasedAccessConstants.FORM_NAME_hh_invalid_container_s)) {
            hideMessagesPanel();
            OperationBinding oper = ADFUtils.findOperation("executeDetailF4Logic");
            oper.execute();
            return "goPage1";
        } else {
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;
    }

    public String f9PrevAction() {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_invalid_container_s_DETAIL_F9,
                                           RoleBasedAccessConstants.FORM_NAME_hh_invalid_container_s)) {
            hideMessagesPanel();
            OperationBinding oper = ADFUtils.findOperation("Previous");
            oper.execute();
        }else{
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;
    }

    public String f10NextAction() {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_invalid_container_s_DETAIL_F10,
                                           RoleBasedAccessConstants.FORM_NAME_hh_invalid_container_s)) {
            hideMessagesPanel();
            OperationBinding oper = ADFUtils.findOperation("Next");
            oper.execute();
        }else{
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;
    }

    public void setF10NextLink(RichLink f10NextLink) {
        this.f10NextLink = f10NextLink;
    }

    public RichLink getF10NextLink() {
        return f10NextLink;
    }

    public void setF9PrevLink(RichLink f9PrevLink) {
        this.f9PrevLink = f9PrevLink;
    }

    public RichLink getF9PrevLink() {
        return f9PrevLink;
    }
    
    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    public void showMessagesPanel(String messageType, String msg) {
      // CLEAR PREVIOUS MESSAGES
      this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
    
      if (WARN.equals(messageType)) {
          this.getErrorWarnInfoIcon().setName(WRN_ICON);
      } else if (INFO.equals(messageType)) {
          this.getErrorWarnInfoIcon().setName(INF_ICON);
      } else if (MSGTYPE_M.equals(messageType)) {
          this.getErrorWarnInfoIcon().setName(LOGO_ICON);
      } else {
          this.getErrorWarnInfoIcon().setName(ERR_ICON);
      }
      this.getErrorWarnInfoMessage().setValue(msg);
      this.getErrorWarnInfoIcon().setVisible(true);
      this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
}
