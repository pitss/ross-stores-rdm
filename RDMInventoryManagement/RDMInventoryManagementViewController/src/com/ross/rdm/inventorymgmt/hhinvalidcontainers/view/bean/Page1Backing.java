package com.ross.rdm.inventorymgmt.hhinvalidcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.util.Iterator;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMInventoryManagementBackingBean   {
    @SuppressWarnings("compatibility:6166587222109349806")
    private static final long serialVersionUID = 1L;
    
    private RichInputText lpnInputText;
    private RichInputText poNbrInputText;
    private RichLink f2DetailLink;
    private RichLink f3ExitLink;
    private RichLink f4CreateLink;
    private RichLink f5ClearLink;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelGroupLayout allMessagesPanel;
    private RichLink onChangedLpnLink;
    private RichIcon iconLpn;
    private RichIcon iconPoNbr;
    private RichIcon iconItemId;
    private RichIcon iconQty1;
    private RichIcon iconLocation;
    private RichInputText itemIdInputText;
    private RichInputText qty1InputText;
    private RichInputText locationInputText;
    private RichLink onChangedPoNbrLink;
    private RichLink onChangedItemIdLink;
    private RichLink onChangedQtyLink;
    private RichLink onChangedLocationLink; 
    
    private static final String STATUS_ERROR = "error";
    private static final String STATUS_WARNING = "warning";
    private static final String STATUS_REQUIRED = "required";

    private static final String INPUT_LPN = "lpn1";
    private static final String INPUT_PO_NBR = "poN";
    private static final String INPUT_ITEM_ID = "ite1";
    private static final String INPUT_QTY = "qty1";
    private static final String INPUT_LOCATION = "loc1";
    private RichPanelFormLayout pflMain;


    public Page1Backing() {

    }

    private HhInvalidContainerSBean getPageFlowBean() {
        return (HhInvalidContainerSBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhInvalidContainerSBean");
    }

    public void setFocusOnUIComponent(UIComponent p1) {
        this.getPageFlowBean().setCurrentItem(p1);
        super.setFocusOnUIComponent(p1);
    }

    public  UIComponent findComponentInRoot(String id) {
        UIComponent component = null;

        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
          UIComponent root = facesContext.getViewRoot();
          component = findComponent(root, id);
        }

        return component;
    }
        
    public  UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId()))
          return base;
      
        UIComponent kid = null;
        UIComponent result = null;
        Iterator kids = base.getFacetsAndChildren();
        while (kids.hasNext() && (result == null)) {
          kid = (UIComponent) kids.next();
          if (id.equals(kid.getId())) {
            result = kid;
            break;
          }
          result = findComponent(kid, id);
          if (result != null) {
            break;
          }
        }
        return result;
    }

    private void outlineInputField(String id, boolean disabled, String status) {
        UIComponent uiComp = this.findComponentInRoot(id);
        if (uiComp instanceof RichInputText ) {
            RichInputText ri = (RichInputText) this.findComponentInRoot(id);           
                        if (ri != null) {
                ri.setDisabled(disabled); 
                if (disabled == false && (status.equals(STATUS_ERROR) || status.equals(STATUS_WARNING)))
                    this.addErrorStyleToComponent(ri);
                else
                    this.removeErrorStyleToComponent(ri); 
                
                if (ri.getParent() instanceof RichPanelLabelAndMessage) {
                    RichPanelLabelAndMessage riPanel = (RichPanelLabelAndMessage) ri.getParent();
                    UIComponent uic = riPanel.getFacet("end");
                    if (uic instanceof RichIcon ) {
                        RichIcon icon = (RichIcon) uic;
                        icon.setName(status);
                        this.refreshContentOfUIComponent(icon);
                    }                    
                }              
            }
                this.refreshContentOfUIComponent(uiComp);
        }
    }

    public String changeLpnAction() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callValidateLpn");
        result = (List)oper.execute();
        
        if(result != null && result.size() > 1 && ERROR.equals(result.get(0)))
        {
            this.showMessagesPanel(ERROR ,result.get(1).toString());
            this.outlineInputField(INPUT_LPN, false, STATUS_ERROR);
            this.getPoNbrInputText().setDisabled(true);
            this.setFocusOnUIComponent(this.getLpnInputText());
            
        }
        else if (result != null && result.size() > 1 && WARN.equals(result.get(0)))
        {
            this.showMessagesPanel(result.get(0).toString(), result.get(1).toString());
            this.outlineInputField(INPUT_LPN, true, STATUS_REQUIRED);  
            this.getPoNbrInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getPoNbrInputText());            
        }
        else
        {
            this.hideMessagesPanel();
            this.outlineInputField(INPUT_LPN, true, STATUS_REQUIRED);
            this.getPoNbrInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getPoNbrInputText());
        }
        refreshContentOfUIComponent(getPflMain());
        return null;
    }

    public void onChangedLpn(ValueChangeEvent vce) {

            this.updateModel(vce);
            ActionEvent actionEvent = new ActionEvent(this.getOnChangedLpnLink());
            actionEvent.queue();
        }
               
    public String changePoNbrAction() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callValidatePo");
        result = (List)oper.execute();
        
        if(result != null && result.size() > 1)
        {
            this.showMessagesPanel(ERROR/*result.get(0).toString()*/, result.get(1).toString() /*this.getMessage(result.get(1).toString() , result.get(0).toString(), 
                                                                (String) ADFUtils.getBoundAttributeValue("FacilityId"), 
                                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode")) */);
            this.outlineInputField(INPUT_PO_NBR, false, STATUS_ERROR);
            this.getItemIdInputText().setDisabled(true);
            this.setFocusOnUIComponent(this.getPoNbrInputText());
            
        }
        else
        {
            this.hideMessagesPanel();
            this.outlineInputField(INPUT_PO_NBR, true, STATUS_REQUIRED);
            this.getItemIdInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getItemIdInputText());
        }
        return null;  
        }

    public void onChangedPoNbr(ValueChangeEvent vce) {

            this.updateModel(vce);
            ActionEvent actionEvent = new ActionEvent(this.getOnChangedPoNbrLink());
            actionEvent.queue();
        }

    public String changeItemIdAction() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callValidateItem");
        result = (List)oper.execute();
        
        if(result != null && result.size() > 1)
        {
            this.showMessagesPanel(ERROR/*result.get(0).toString()*/, result.get(1).toString());
            this.outlineInputField(INPUT_ITEM_ID, false, STATUS_ERROR);
            this.getQty1InputText().setDisabled(true);
            this.setFocusOnUIComponent(this.getItemIdInputText());            
        }
        else
        {
            this.hideMessagesPanel();
            this.outlineInputField(INPUT_ITEM_ID, true, STATUS_REQUIRED);
            this.getQty1InputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getQty1InputText());
        }
        
        return null;
    }

    public void onChangedItemId(ValueChangeEvent vce) {

            this.updateModel(vce);
            ActionEvent actionEvent = new ActionEvent(this.getOnChangedItemIdLink());
            actionEvent.queue();
        }


    public String changeQtyAction() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callValidateQty");
        result = (List)oper.execute();
        
        if(result != null && result.size() > 1)
        {
            this.showMessagesPanel(ERROR/*result.get(0).toString()*/, result.get(1).toString());
            this.outlineInputField(INPUT_QTY, false, STATUS_ERROR);
            this.getLocationInputText().setDisabled(true);
            this.setFocusOnUIComponent(this.getQty1InputText());
            
        }
        else if (result != null && result.get(0).equals("1") 
                 && ADFUtils.getBoundAttributeValue("QtyCheck").equals("Y")) {
            oper = ADFUtils.findOperation("validateMain");
            result = (List)oper.execute();
            if (result != null && result.size() == 3 && result.get(2).equals("LPN.LPN")) {
                this.disableAllInputTexts();
                this.showMessagesPanel(result.get(0).toString(), result.get(1).toString());
                this.getLocationInputText().setDisabled(true);
                this.setFocusOnUIComponent(this.getLpnInputText());
                this.getLpnInputText().setDisabled(false);                
                return null;                
            }
        } else
        {
            this.hideMessagesPanel();
            this.outlineInputField(INPUT_QTY, true, STATUS_REQUIRED);
            this.getLocationInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getLocationInputText());
        }
        
        return null;
    }

    public void onChangedQty(ValueChangeEvent vce) {

            this.updateModel(vce);
            ActionEvent actionEvent = new ActionEvent(this.getOnChangedQtyLink());
            actionEvent.queue();
        }

    public String changeLocationAction() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callValidateLocation");
        result = (List)oper.execute();
        
        if(result != null && result.size() > 1)
        {
            this.showMessagesPanel(ERROR/*result.get(0).toString()*/, result.get(1).toString());
            this.outlineInputField(INPUT_LOCATION, false, STATUS_ERROR);
            this.getPoNbrInputText().setDisabled(true);
            this.setFocusOnUIComponent(this.getLocationInputText());            
        }
        else
        {
            this.hideMessagesPanel();
            this.outlineInputField(INPUT_LOCATION, true, STATUS_REQUIRED);
            this.getPoNbrInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getPoNbrInputText());
        }
        
        return null;
    }

    public void onChangedLocation(ValueChangeEvent vce) {

            this.updateModel(vce);
            ActionEvent actionEvent = new ActionEvent(this.getOnChangedLocationLink());
            actionEvent.queue();
        }

    public String f2DetailAction() {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_invalid_container_s_LPN_F2,
                                           RoleBasedAccessConstants.FORM_NAME_hh_invalid_container_s)) {
            List result;

            this.hideMessagesPanel();
            this.outlineInputField(INPUT_LPN, false, STATUS_REQUIRED);
            OperationBinding oper = ADFUtils.findOperation("executeF2Logic");
            result = (List) oper.execute();
            if (result != null && result.size() > 1) {
                this.showMessagesPanel(ERROR /*result.get(0).toString()*/, result.get(1).toString());
                this.disableAllInputTexts();
                if (result.size() == 3) {
                    if (result.get(2).equals("LPN.ITEM_ID")) {
                        this.outlineInputField(INPUT_ITEM_ID, false, STATUS_ERROR);
                        this.setFocusOnUIComponent(this.getItemIdInputText());
                    } else if (result.get(2).equals("LPN.LPN")) {
                        this.showMessagesPanel(result.get(0).toString(), result.get(1).toString());
                        this.outlineInputField(INPUT_LPN, false,
                                               STATUS_ERROR /* result.get(1).equals(ERROR) ? STATUS_ERROR :  STATUS_WARNING*/);
                        this.setFocusOnUIComponent(this.getLpnInputText());
                    }
                }

                return null;
            } else if (result != null && result.get(0).equals("goPage2")) {
                this.getPageFlowBean().setLastItem(this.getPageFlowBean().getCurrentItem());
                return (String) result.get(0);
            }
        } else {
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;

    }

    public String f3ExitAction() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("executeF3Logic");
        result = (List)oper.execute();
        
        if(result != null && result.size() > 1)
        {
            this.showMessagesPanel(result.get(0).toString(), result.get(1).toString() );
            this.getLpnInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getLpnInputText());
            return null;            
        }
        else
        {
            return "backGlobalHome";            
        }
    }

    public String f4CreateAction() {
        
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_invalid_container_s_LPN_F4,
                                           RoleBasedAccessConstants.FORM_NAME_hh_invalid_container_s)) {
            List result;

            if ("qty1".equals(this.getPageFlowBean().getCurrentItemId())) {
                OperationBinding oper = ADFUtils.findOperation("callValidateQty");
                result = (List) oper.execute();
                if (result != null && result.size() == 2) {
                    this.showMessagesPanel(ERROR, (String) result.get(1));
                    this.outlineInputField(INPUT_QTY, false, STATUS_ERROR);
                    return null;
                }
            }


            OperationBinding oper = ADFUtils.findOperation("executeF4Logic");
            result = (List) oper.execute();
            this.disableAllInputTexts();
            if (result != null && result.size() == 2) {
                this.showMessagesPanel((String) result.get(0), (String) result.get(1));
                this.getLpnInputText().setDisabled(false);
                this.setFocusOnUIComponent(this.getLpnInputText());
                return null;
            } else if (result != null && result.size() == 3) {
                this.showMessagesPanel(ERROR /*(String) result.get(0)*/, (String) result.get(1));
                if (result.get(2).toString().equals("LPN.LOCATION")) {
                    this.outlineInputField(INPUT_LOCATION, false, STATUS_ERROR);
                    this.setFocusOnUIComponent(this.getLocationInputText());
                    return null;
                } else if (result.get(2).toString().equals("LPN.QTY")) {
                    this.outlineInputField(INPUT_QTY, false, STATUS_ERROR);
                    this.setFocusOnUIComponent(this.getQty1InputText());
                    return null;
                } else if (result.get(2).toString().equals("LPN.PO_NBR")) {
                    this.outlineInputField(INPUT_PO_NBR, false, STATUS_ERROR);
                    this.setFocusOnUIComponent(this.getPoNbrInputText());
                    return null;
                } else if (result.get(2).toString().equals("LPN.ITEM_ID")) {
                    this.outlineInputField(INPUT_ITEM_ID, false, STATUS_ERROR);
                    this.setFocusOnUIComponent(this.getItemIdInputText());
                    return null;
                } else if (result.get(2).toString().equals("LPN.LPN")) {
                    this.outlineInputField(INPUT_LPN, false, STATUS_ERROR);
                    this.setFocusOnUIComponent(this.getLpnInputText());
                    return null;
                } else {
                    this.getLpnInputText().setDisabled(false);
                    this.setFocusOnUIComponent(this.getLpnInputText());
                    return null;
                }
            } else if (result != null && result.size() == 4 && result.get(0).equals("1") &&
                       result.get(3).equals("LPN.QTY")) {
                this.showMessagesPanel((String) result.get(1), (String) result.get(2));
                this.outlineInputField(INPUT_QTY, false, result.get(1).equals(ERROR) ? STATUS_ERROR : STATUS_WARNING);
                this.setFocusOnUIComponent(this.getQty1InputText());
                return null;
            } else if (result != null && result.size() == 4 && result.get(0).equals("1") &&
                       result.get(3).equals("LPN.ITEM_ID")) {
                this.showMessagesPanel((String) result.get(1), (String) result.get(2));
                this.outlineInputField(INPUT_ITEM_ID, false,
                                       result.get(1).equals(ERROR) ? STATUS_ERROR : STATUS_WARNING);
                this.setFocusOnUIComponent(this.getItemIdInputText());
                return null;
            } else {
                this.getLpnInputText().setDisabled(false);
                this.setFocusOnUIComponent(this.getLpnInputText());
                return null;
            }
        } else {
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;
    }

    public String f5ClearAction() {
        
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_invalid_container_s_LPN_F5,
                                           RoleBasedAccessConstants.FORM_NAME_hh_invalid_container_s)) {
            OperationBinding oper = ADFUtils.findOperation("executeF5Logic");
            oper.execute();
            this.hideMessagesPanel();
            this.disableAllInputTexts();
            this.getLpnInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getLpnInputText());
        } else {
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;
    }

    private void checkEmptyItem(RichInputText component, String item) {
        
        if (INPUT_LPN.equals(item)) {
            this.showMessagesPanel(WARN, this.getMessage("SCAN_LPN", WARN, 
                                    (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                    (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            this.addErrorStyleToComponent(component);
            this.getIconLpn().setName("error");
            this.refreshContentOfUIComponent(this.getIconLpn());
        } 
        /*else if ("poN".equals(item)) { // no spec or code found for message if any other attribute is null
            this.showMessagesPanel(ERROR, this.getMessage("????", ERROR, 
                                    (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                    (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            this.addErrorStyleToComponent(component);
            this.getIconLpn().setName("required");
            this.refreshContentOfUIComponent(this.getIconLpn());
        } */
    }

    private void valueChange(RichInputText component, String submittedValue, String item) {
            if (INPUT_LPN.equals(item)) {
            this.getLpnInputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getLpnInputText());
            this.getPoNbrInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getPoNbrInputText());
            this.onChangedLpn(new ValueChangeEvent(component, null, submittedValue));
            } else if (INPUT_PO_NBR.equals(item)) {
            this.getPoNbrInputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getPoNbrInputText());
            this.getItemIdInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getItemIdInputText());
            this.onChangedPoNbr(new ValueChangeEvent(component, null, submittedValue));                          
        } else if (INPUT_ITEM_ID.equals(item)) {
            this.getItemIdInputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getItemIdInputText());
            this.getQty1InputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getQty1InputText());
            this.onChangedItemId(new ValueChangeEvent(component, null, submittedValue));                         
        } else if (INPUT_QTY.equals(item)) {
            this.getQty1InputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getQty1InputText());
            this.getLocationInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getLocationInputText());
            this.onChangedQty(new ValueChangeEvent(component, null, submittedValue));
        } else if (INPUT_LOCATION.equals(item)) {
            this.getLocationInputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getLocationInputText());
            this.getPoNbrInputText().setDisabled(false);
            this.setFocusOnUIComponent(this.getPoNbrInputText());
            this.onChangedLocation(new ValueChangeEvent(component, null, submittedValue));
        }
        
    }


        
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) 
        {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String item = (String) clientEvent.getParameters().get("item");              
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    if (item.equals("qty1") || item.equals("poN") || item.equals("ite1") || item.equals("loc1"))
                      this.valueChange(component, submittedValue, item);
                    else    
                      this.checkEmptyItem(component, item);
                } else {
                    this.valueChange(component, submittedValue, item);
                }
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2DetailLink());
                actionEvent.queue();                
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4CreateLink());
                actionEvent.queue();
            }else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5ClearLink());
                actionEvent.queue();
            }
        }
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    public void showMessagesPanel(String messageType, String msg) {
      // CLEAR PREVIOUS MESSAGES
      this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
    
      if (WARN.equals(messageType)) {
          this.getErrorWarnInfoIcon().setName(WRN_ICON);
      } else if (INFO.equals(messageType)) {
          this.getErrorWarnInfoIcon().setName(INF_ICON);
      } else if (MSGTYPE_M.equals(messageType)) {
          this.getErrorWarnInfoIcon().setName(LOGO_ICON);
      } else {
          this.getErrorWarnInfoIcon().setName(ERR_ICON);
      }
      this.getErrorWarnInfoMessage().setValue(msg);
      this.getErrorWarnInfoIcon().setVisible(true);
      this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }


    private void disableAllInputTexts() {
        this.getPoNbrInputText().setDisabled(true);
        this.removeErrorStyleToComponent(this.getPoNbrInputText());
        this.getIconPoNbr().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconPoNbr());        
        this.getItemIdInputText().setDisabled(true);
        this.removeErrorStyleToComponent(this.getItemIdInputText());
        this.getIconItemId().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconItemId());        
        this.getQty1InputText().setDisabled(true);
        this.removeErrorStyleToComponent(this.getQty1InputText());
        this.getIconQty1().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconQty1());        
        this.getLocationInputText().setDisabled(true);
        this.removeErrorStyleToComponent(this.getLocationInputText());
        this.getIconLocation().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconLocation());
        this.getLpnInputText().setDisabled(true);
        this.removeErrorStyleToComponent(this.getLpnInputText());
        this.getIconLpn().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconLpn());        
    }

    public void setLpnInputText(RichInputText lpnInputText) {
        this.lpnInputText = lpnInputText;
    }

    public RichInputText getLpnInputText() {
        return lpnInputText;
    }

    public void setPoNbrInputText(RichInputText poNbrInputText) {
        this.poNbrInputText = poNbrInputText;
    }

    public RichInputText getPoNbrInputText() {
        return poNbrInputText;
    }

    public void setF2DetailLink(RichLink f2DetailLink) {
        this.f2DetailLink = f2DetailLink;
    }

    public RichLink getF2DetailLink() {
        return f2DetailLink;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4CreateLink(RichLink f4CreateLink) {
        this.f4CreateLink = f4CreateLink;
    }

    public RichLink getF4CreateLink() {
        return f4CreateLink;
    }

    public void setF5ClearLink(RichLink f5ClearLink) {
        this.f5ClearLink = f5ClearLink;
    }

    public RichLink getF5ClearLink() {
        return f5ClearLink;
    }


    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setOnChangedLpnLink(RichLink onChangedLpnLink) {
        this.onChangedLpnLink = onChangedLpnLink;
    }

    public RichLink getOnChangedLpnLink() {
        return onChangedLpnLink;
    }

    public void setIconLpn(RichIcon iconLpn) {
        this.iconLpn = iconLpn;
    }

    public RichIcon getIconLpn() {
        return iconLpn;
    }

    public void setIconPoNbr(RichIcon iconPoNbr) {
        this.iconPoNbr = iconPoNbr;
    }

    public RichIcon getIconPoNbr() {
        return iconPoNbr;
    }

    public void setIconItemId(RichIcon iconItemId) {
        this.iconItemId = iconItemId;
    }

    public RichIcon getIconItemId() {
        return iconItemId;
    }

    public void setIconQty1(RichIcon iconQty1) {
        this.iconQty1 = iconQty1;
    }

    public RichIcon getIconQty1() {
        return iconQty1;
    }

    public void setIconLocation(RichIcon iconLocation) {
        this.iconLocation = iconLocation;
    }

    public RichIcon getIconLocation() {
        return iconLocation;
    }

    public void setItemIdInputText(RichInputText itemIdInputText) {
        this.itemIdInputText = itemIdInputText;
    }

    public RichInputText getItemIdInputText() {
        return itemIdInputText;
    }

    public void setQty1InputText(RichInputText qty1InputText) {
        this.qty1InputText = qty1InputText;
    }

    public RichInputText getQty1InputText() {
        return qty1InputText;
    }

    public void setLocationInputText(RichInputText locationInputText) {
        this.locationInputText = locationInputText;
    }

    public RichInputText getLocationInputText() {
        return locationInputText;
    }

    public void setOnChangedPoNbrLink(RichLink onChangedPoNbrLink) {
        this.onChangedPoNbrLink = onChangedPoNbrLink;
    }

    public RichLink getOnChangedPoNbrLink() {
        return onChangedPoNbrLink;
    }

    public void setOnChangedItemIdLink(RichLink onChangedItemIdLink) {
        this.onChangedItemIdLink = onChangedItemIdLink;
    }

    public RichLink getOnChangedItemIdLink() {
        return onChangedItemIdLink;
    }

    public void setOnChangedQtyLink(RichLink onChangedQtyLink) {
        this.onChangedQtyLink = onChangedQtyLink;
    }

    public RichLink getOnChangedQtyLink() {
        return onChangedQtyLink;
    }

    public void setOnChangedLocationLink(RichLink onChangedLocationLink) {
        this.onChangedLocationLink = onChangedLocationLink;
    }

    public RichLink getOnChangedLocationLink() {
        return onChangedLocationLink;
    }


    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        RichInputText uic = (RichInputText) this.getPageFlowBean().getLastItem();
        if (uic != null) {           
                this.disableAllInputTexts();
            //if (uic.getId().equals(INPUT_PO_NBR))
             //   this.getPageFlowBean().setDisabledPoNbr(false); 
            RichInputText uic2 = (RichInputText) this.findComponentInRoot(uic.getId());
            uic2.setDisabled(false);
            this.setFocusOnUIComponent(uic2); 
            this.getPageFlowBean().setLastItem(null);
        }
          
          
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }
}
