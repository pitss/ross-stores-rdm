package com.ross.rdm.inventorymgmt.hhsiphonmlps.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementPageFlowBean;

import java.util.List;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhSiphonMlpSBean extends RDMInventoryManagementPageFlowBean {

    private static ADFLogger _logger = ADFLogger.createADFLogger(HhSiphonMlpSBean.class);
    public static final String MASTER_CONTAINER_ID = "MASTER_CONTAINER_ID";
    public static final String PO_NBR = "PO_NBR";
    public static final String NEW_ITEM_ID = "NEW_ITEM_ID";
    private String focus = null;
    private boolean disableAllFields = false;
    private boolean errorOnSelectedItem;
    private String onLoadMessage = null;
    private boolean notAllowed = false;


    public void initTaskFlow() {
        _logger.info("initTaskFlow start");
        this.setFocus(MASTER_CONTAINER_ID);
        this.callIniTaskFlow();
        this.callTrStartupLocal2();
    }

    private void callIniTaskFlow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("initTaskFlow");
        oper.execute();
    }

    private void callTrStartupLocal2() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callStartupLocal2");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null && returVals.size() == 2) {
                this.setOnLoadMessage((String) returVals.get(1));
            }
        }
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getFocus() {
        return focus;
    }

    public void setDisableAllFields(boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public boolean isDisableAllFields() {
        return disableAllFields;
    }

    public Boolean getDisabledMasterContainerId() {
        return disableAllFields || (focus != null && !MASTER_CONTAINER_ID.equals(focus));
    }

    public Boolean getDisabledPoNbr() {
        return disableAllFields || !PO_NBR.equals(focus);
    }

    public Boolean getDisabledNewItemId() {
        return disableAllFields || !NEW_ITEM_ID.equals(focus);
    }

    public void setErrorOnSelectedItem(boolean errorOnSelectedItem) {
        this.errorOnSelectedItem = errorOnSelectedItem;
    }

    public boolean isErrorOnSelectedItem() {
        return errorOnSelectedItem;
    }

    public void setOnLoadMessage(String onLoadMessage) {
        this.onLoadMessage = onLoadMessage;
    }

    public String getOnLoadMessage() {
        return onLoadMessage;
    }

    public void setNotAllowed(boolean notAllowed) {
        this.notAllowed = notAllowed;
    }

    public boolean isNotAllowed() {
        return notAllowed;
    }
}
