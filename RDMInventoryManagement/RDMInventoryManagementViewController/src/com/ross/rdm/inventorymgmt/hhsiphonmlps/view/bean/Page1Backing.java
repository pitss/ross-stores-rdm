package com.ross.rdm.inventorymgmt.hhsiphonmlps.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMMobileBaseBackingBean {
    @SuppressWarnings("compatibility:6166587222109349806")
    private static final long serialVersionUID = 1L;

    private RichInputText masterContainerId;
    private RichInputText locationId;
    private RichInputText curItemId;
    private RichInputText poNBR;
    private RichInputText newItem;

    private RichOutputText dialogOutputText;


    private RichLink validateMasterContainerLink;
    private RichLink validatePoNbrLink;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f9ItemsLink;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;

    private RichIcon iconMaster;
    private RichIcon iconPonbr;
    private RichIcon iconNewItemId;
    private RichPanelGroupLayout globalPgl;

    private RichPopup confirmPopup;

    private final static String MASTER_CONTAINER_ITEM = "mas1";
    private final static String PO_NBR = "poN1";
    private final static String NEW_ITEM_ID = "new1";
    private RichOutputText siphonMlpOnLoadMessage;
    private RichDialog siphonMlpLoadDialog;
    private RichPopup siphonMlpOnLoadPopup;

    public Page1Backing() {
    }


    public String onChangedMasterContainerId() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callDisplayContainerInfo1");
        result = (List) oper.execute();
        if (result != null && result.size() > 1) {
            this.setFocusOnMasterCid();
            this.setErrorOnField(this.getMasterContainerId(), this.getIconMaster());
            this.showMessagesPanel((String) result.get(0), getMessage((String) result.get(1)));
        } else {
            this.setFocusOnPoNbr();
        }
        return null;
    }

    public boolean onChangedPoNbr() {
        String poNbr = (String) ADFUtils.getBoundAttributeValue("PoNbr1");
        if (StringUtils.isNotEmpty(poNbr)) {
            return this.validatePoNbr();
        } else {
            this.showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
            this.setErrorOnFocusedField();
            return false;
        }
    }
    //Noor
    private boolean validatePoNbr() {
        OperationBinding operPoNbr = ADFUtils.findOperation("callValidatePoNbr");
        List returValsPoNbr = (List) operPoNbr.execute();
        OperationBinding operPoDates = ADFUtils.findOperation("callValidatePoDates");
        List returValsPoDates = (List) operPoDates.execute();
        if ((operPoNbr.getErrors() == null || operPoNbr.getErrors().isEmpty()) &&
            (operPoDates.getErrors() == null || operPoDates.getErrors().isEmpty())) {
            if (returValsPoNbr != null && returValsPoDates != null) {
                String msg = "";
                String msgCode = "";
                if ((returValsPoNbr.size() > 0 && "0".equals((returValsPoNbr.get(0)))) && returValsPoDates.size() > 0 &&
                    "1".equalsIgnoreCase((String) returValsPoDates.get(0))) {
                    if (returValsPoNbr.size() == 2 && YES.equalsIgnoreCase((String) returValsPoNbr.get(1))) {
                        msg = this.getMessage("INV_PO_NBR", WARN);
                        msgCode = WARN;
                    }
                } else if ("1".equals((returValsPoNbr.get(0)))) {
                    msg = this.getMessage("INV_SIPHON_PO", ERROR);
                    msgCode = ERROR;
                } else {
                    if (returValsPoNbr.size() == 2 && YES.equalsIgnoreCase((String) returValsPoNbr.get(1))) {
                        msg = this.getMessage("INV_PO_NBR", WARN);
                        msgCode = WARN;
                    } else {
                        this.showInPoRangPopup();
                    }
                }
                if (StringUtils.isNotEmpty(msg) && StringUtils.isNotEmpty(msgCode)) {
                    this.setFocusOnPoNbr();
                    this.setErrorOnField(this.getPoNBR(), this.getIconPonbr());
                    this.showMessagesPanel(msgCode, msg);
                    return false;
                } else if (!this.isPopupShowing())
                    this.setFocusOnNewItem();
            }
        }
        return true;
    }

    private boolean isPopupShowing() {
        return "Visible".equalsIgnoreCase(this.getConfirmPopup().getLauncherVar());
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String item = (String) clientEvent.getParameters().get("item");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (MASTER_CONTAINER_ITEM.equals(item)) {
                    ActionEvent actionEvent = new ActionEvent(this.getValidateMasterContainerLink());
                    actionEvent.queue();
                } else if (PO_NBR.equals(item)) {
                    ActionEvent actionEvent = new ActionEvent(this.getValidatePoNbrLink());
                    actionEvent.queue();
                } else if (NEW_ITEM_ID.equals(item)) {
                    this.setFocusOnMasterCid();
                }
            }
        }
    }
    //Noor
    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmPopup().hide();
        this.setFocusOnNewItem();
    }
    //Noor
    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmPopup().hide();
        this.setFocusOnPoNbr();

    }
    //Noor
    public String f3Action() {
        return "backGlobalHome";
    }
    //Noor
    public String f9Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_siphon_mlp_s_MAIN_F9,
                                                RoleBasedAccessConstants.FORM_NAME_hh_siphon_mlp_s)){
        if ("MASTER_CONTAINER_ID".equalsIgnoreCase(this.getPageFlowBean().getFocus()))
            return null;
        if (this.isPoNbrEmpty()) {
            this.showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
            this.setErrorOnFocusedField();
            return null;
        } else {
            String locatrionId = (String) ADFUtils.getBoundAttributeValue("LocationId1");
            if (!this.isPoNbrEmpty() && StringUtils.isNotEmpty(locatrionId)) {
                if (this.onChangedPoNbr()) {
                    OperationBinding oper = ADFUtils.findOperation("executeF9Logic");
                    oper.execute();
                    return "Page2";
                }
            }
        }
        
                                                }
        else{
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }
    //Noor
    private void clearCurrentRow() {
        Row currentRow = (ADFUtils.findIterator("HhSiphonMlpSMainViewIterator")).getCurrentRow();
        currentRow.setAttribute("MasterContainerId", null);
        currentRow.setAttribute("LocationId", null);
        currentRow.setAttribute("PoNbr", null);
        currentRow.setAttribute("ItemId", null);
        currentRow.setAttribute("NewItemId", null);
        this.getMasterContainerId().resetValue();
        this.getLocationId().resetValue();
        this.getPoNBR().resetValue();
        this.getCurItemId().resetValue();
        this.getNewItem().resetValue();
        this.setFocusOnMasterCid();
    }
    //Noor
    public String f4Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_siphon_mlp_s_MAIN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_siphon_mlp_s)){
        String locatrionId = (String) ADFUtils.getBoundAttributeValue("LocationId1");
        if (!this.isMasterCidEmpty() && StringUtils.isEmpty(locatrionId))
            this.onChangedMasterContainerId();
        if (!this.isMasterCidEmpty() && !this.isPoNbrEmpty() && !this.isNewItemIdEmpty()) {
            if (this.callValidatePoNbr())
                if (this.callIsItemOnMlp())
                    if (this.callValidateNewItem())
                        if (this.callValidatePoDates())
                            if (this.callProcessTransfert())
                                if (this.callCreatTickets())
                                    this.clearCurrentRow();

        } else {
            this.showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
        }
        return null;
    }
        else{
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }
    //Noor
    private boolean callValidatePoDates() {
        OperationBinding oper = ADFUtils.findOperation("callValidatePoDates");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null && returVals.size() == 2) {
                this.setFocusOnNewItem();
                this.setErrorOnField(this.getNewItem(), this.getIconNewItemId());
                this.showMessagesPanel((String) returVals.get(0), (String) returVals.get(1));
                return false;
            } else if (returVals != null && returVals.size() == 1)
                if ("0".equalsIgnoreCase((String) returVals.get(0)))
                    this.showInPoRangPopup();
        }
        return true;
    }

    //Noor
    private boolean callValidatePoNbr() {
        OperationBinding oper = ADFUtils.findOperation("callValidatePoNbr");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null) {
                String msg = "";
                String msgCode = "";
                if (returVals.size() == 1 && "1".equals((returVals.get(0)))) {
                    msg = this.getMessage("INV_SIPHON_PO", ERROR);
                    msgCode = ERROR;
                } else if (returVals.size() == 2) {
                    if (YES.equalsIgnoreCase((String) returVals.get(1))) {
                        msg = this.getMessage("INV_PO_NBR", WARN);
                        msgCode = WARN;
                    }
                }
                if (StringUtils.isNotEmpty(msg) && StringUtils.isNotEmpty(msgCode)) {
                    this.setFocusOnPoNbr();
                    this.setErrorOnField(this.getPoNBR(), this.getIconPonbr());
                    this.showMessagesPanel(msgCode, msg);
                    return false;
                }
            }
        }
        return true;
    }
    //Noor
    private boolean callIsItemOnMlp() {
        OperationBinding oper = ADFUtils.findOperation("callIsItemOnMlp");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null) {
                String msg = "";
                String msgCode = "";
                if ("1".equals((returVals.get(0)))) {
                    msg = this.getMessage("INV_SIPHON_ITEM", ERROR);
                    msgCode = ERROR;
                } else if (returVals.size() == 2) {
                    msg = this.getMessage((String) returVals.get(1), (String) returVals.get(0));
                    msgCode = (String) returVals.get(0);
                }
                if (StringUtils.isNotEmpty(msg) && StringUtils.isNotEmpty(msgCode)) {
                    this.setFocusOnNewItem();
                    this.setErrorOnField(this.getNewItem(), this.getIconNewItemId());
                    this.showMessagesPanel(msgCode, msg);
                    return false;
                }
            }
        }
        return true;
    }
    //Noor
    private boolean callValidateNewItem() {
        OperationBinding oper = ADFUtils.findOperation("callValidateNewItemId");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null && returVals.size() == 2) {
                this.setFocusOnNewItem();
                this.setErrorOnField(this.getNewItem(), this.getIconNewItemId());
                this.showMessagesPanel((String) returVals.get(0),
                                       this.getMessage((String) returVals.get(1), (String) returVals.get(0)));
                return false;
            }
        }
        return true;
    }
    //Noor
    private boolean callProcessTransfert() {
        OperationBinding oper = ADFUtils.findOperation("callProcessTransfert");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null && returVals.size() == 2) {
                this.setFocusOnNewItem();
                this.setErrorOnField(this.getNewItem(), this.getIconNewItemId());
                this.showMessagesPanel((String) returVals.get(0),
                                       this.getMessage((String) returVals.get(1), (String) returVals.get(0)));
                return false;
            }
        }
        return true;
    }
    //Noor
    private boolean callCreatTickets() {
        OperationBinding oper = ADFUtils.findOperation("callCreatTickets");
        List returVals = (List) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (returVals != null && returVals.size() == 2) {
                this.setFocusOnNewItem();
                this.setErrorOnField(this.getNewItem(), this.getIconNewItemId());
                this.showMessagesPanel((String) returVals.get(0),
                                       this.getMessage((String) returVals.get(1), (String) returVals.get(0)));
                return false;
            }
        }
        return true;
    }
    //Noor
    private void clearErrorAndDisableActivComp() {
        this.removeAllErrors();
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
    }
    //Noor
    private void showInPoRangPopup() {
        this.clearErrorAndDisableActivComp();
        this.getConfirmPopup().setLauncherVar("Visible");
        this.getDialogOutputText().setValue(this.getMessage("INV_PO_RANGE", WARN));
        this.getConfirmPopup().show(new RichPopup.PopupHints());
    }
    //Noor
    public String getMessage(String msgCode, String messageType) {
        return this.getMessage(msgCode, messageType, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                               (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
    }
    //Noor
    private void setFocusOnPoNbr() {
        this.removeAllErrors();
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.getPageFlowBean().setFocus("PO_NBR");
        this.getPoNBR().setDisabled(false);
        this.refreshContentOfUIComponent(this.getPoNBR());
        this.setFocusOnUIComponent(this.getPoNBR());
        this.selectTextOnUIComponent(this.getPoNBR());
    }
    //Noor
    private void setFocusOnMasterCid() {
        this.removeAllErrors();
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.getPageFlowBean().setFocus("MASTER_CONTAINER_ID");
        this.getMasterContainerId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getMasterContainerId());
        this.setFocusOnUIComponent(this.getMasterContainerId());
        this.selectTextOnUIComponent(this.getMasterContainerId());
    }
    //Noor
    private void setFocusOnNewItem() {
        this.removeAllErrors();
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.getPageFlowBean().setFocus("NEW_ITEM_ID");
        this.getNewItem().setDisabled(false);
        this.refreshContentOfUIComponent(this.getNewItem());
        this.setFocusOnUIComponent(this.getNewItem());
        this.selectTextOnUIComponent(this.getNewItem());

    }
    //Noor
    private boolean isMasterCidEmpty() {
        String cid = (String) ADFUtils.getBoundAttributeValue("MasterContainerId1");
        boolean noValid = (cid == null || "".equalsIgnoreCase(cid));
        if (noValid) {
            this.setFocusOnMasterCid();
            this.setErrorOnFocusedField();
        }
        return noValid;
    }
    //Noor
    private boolean isPoNbrEmpty() {
        String poNbr = (String) ADFUtils.getBoundAttributeValue("PoNbr1");
        boolean noValid = (poNbr == null || "".equalsIgnoreCase(poNbr));
        if (noValid) {
            this.setFocusOnPoNbr();
            this.setErrorOnFocusedField();
        }
        return noValid;
    }
    //Noor
    private boolean isNewItemIdEmpty() {
        String newItemId = (String) ADFUtils.getBoundAttributeValue("NewItemId1");
        boolean noValid = (newItemId == null || "".equalsIgnoreCase(newItemId));
        if (noValid) {
            this.setFocusOnNewItem();
            this.setErrorOnFocusedField();
        }
        return noValid;
    }
    //Noor
    private void setErrorOnFocusedField() {
        switch (this.getPageFlowBean().getFocus()) {
        case "MASTER_CONTAINER_ID":
            this.setErrorOnField(this.getMasterContainerId(), this.getIconMaster());
            break;
        case "PO_NBR":
            this.setErrorOnField(getPoNBR(), this.getIconPonbr());
            break;
        case "NEW_ITEM_ID":
            this.setErrorOnField(getNewItem(), this.getIconNewItemId());
            break;
        default:
            break;
        }
    }

    private HhSiphonMlpSBean getPageFlowBean() {
        return (HhSiphonMlpSBean) this.getPageFlowBean("HhSiphonMlpSBean");
    }

    private void removeAllErrors() {
        this.removeErrorOfField(this.getMasterContainerId(), this.getIconMaster(), REQ_ICON);
        this.removeErrorOfField(this.getPoNBR(), this.getIconPonbr(), REQ_ICON);
        this.removeErrorOfField(this.getNewItem(), this.getIconNewItemId(), REQ_ICON);
        this.hideMessagesPanel();
    }

    private void callStartupLocal2() {


    }
    //Noor
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (this.checkOnLoadMessage())
            switch (this.getPageFlowBean().getFocus()) {
            case "MASTER_CONTAINER_ID":
                this.setFocusOnUIComponent(this.getMasterContainerId());
                this.selectTextOnUIComponent(this.getMasterContainerId());
                break;
            case "PO_NBR":
                this.setFocusOnUIComponent(this.getPoNBR());
                this.selectTextOnUIComponent(this.getPoNBR());
                break;
            case "NEW_ITEM_ID":
                this.setFocusOnUIComponent(this.getNewItem());
                this.selectTextOnUIComponent(this.getNewItem());
                break;
            default:
                break;
            }
    }
    
    private boolean checkOnLoadMessage() {
        String onMsgLoad = this.getPageFlowBean().getOnLoadMessage();
        if (null != onMsgLoad && !"".equalsIgnoreCase(onMsgLoad)) {
            this.getSiphonMlpOnLoadMessage().setValue(onMsgLoad);
            this.getSiphonMlpOnLoadPopup().show(new RichPopup.PopupHints());
            return false;
        }
        return true;
    }
    //Noor
    public RichInputText getActiveInputText() {
        switch (this.getPageFlowBean().getFocus()) {
        case "MASTER_CONTAINER_ID":
            return this.getMasterContainerId();
        case "PO_NBR":
            return this.getPoNBR();
        case "NEW_ITEM_ID":
            return this.getNewItem();
        default:
            return null;
        }
    }


    public void setGlobalPgl(RichPanelGroupLayout globalPgl) {
        this.globalPgl = globalPgl;
    }

    public RichPanelGroupLayout getGlobalPgl() {
        return globalPgl;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF9ItemsLink(RichLink f9ItemsLink) {
        this.f9ItemsLink = f9ItemsLink;
    }

    public RichLink getF9ItemsLink() {
        return f9ItemsLink;
    }

    public void setCurItemId(RichInputText curItemId) {
        this.curItemId = curItemId;
    }

    public RichInputText getCurItemId() {
        return curItemId;
    }

    public void setPoNBR(RichInputText poNBR) {
        this.poNBR = poNBR;
    }

    public RichInputText getPoNBR() {
        return poNBR;
    }

    public void setNewItem(RichInputText newItem) {
        this.newItem = newItem;
    }

    public RichInputText getNewItem() {
        return newItem;
    }

    public void setIconMaster(RichIcon iconMaster) {
        this.iconMaster = iconMaster;
    }

    public RichIcon getIconMaster() {
        return iconMaster;
    }

    public void setIconPonbr(RichIcon iconPonbr) {
        this.iconPonbr = iconPonbr;
    }

    public RichIcon getIconPonbr() {
        return iconPonbr;
    }

    public void setIconNewItemId(RichIcon iconNewItemId) {
        this.iconNewItemId = iconNewItemId;
    }

    public RichIcon getIconNewItemId() {
        return iconNewItemId;
    }

    public void setValidateMasterContainerLink(RichLink validateMasterContainerLink) {
        this.validateMasterContainerLink = validateMasterContainerLink;
    }

    public RichLink getValidateMasterContainerLink() {
        return validateMasterContainerLink;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public void setValidatePoNbrLink(RichLink validatePoNbrLink) {
        this.validatePoNbrLink = validatePoNbrLink;
    }

    public RichLink getValidatePoNbrLink() {
        return validatePoNbrLink;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setMasterContainerId(RichInputText masterContainerId) {
        this.masterContainerId = masterContainerId;
    }

    public RichInputText getMasterContainerId() {
        return masterContainerId;
    }


    public void okLinkOnLoadSiphonMlpPopup(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void setSiphonMlpOnLoadMessage(RichOutputText siphonMlpOnLoadMessage) {
        this.siphonMlpOnLoadMessage = siphonMlpOnLoadMessage;
    }

    public RichOutputText getSiphonMlpOnLoadMessage() {
        return siphonMlpOnLoadMessage;
    }

    public void setSiphonMlpLoadDialog(RichDialog siphonMlpLoadDialog) {
        this.siphonMlpLoadDialog = siphonMlpLoadDialog;
    }

    public RichDialog getSiphonMlpLoadDialog() {
        return siphonMlpLoadDialog;
    }

    public void setSiphonMlpOnLoadPopup(RichPopup siphonMlpOnLoadPopup) {
        this.siphonMlpOnLoadPopup = siphonMlpOnLoadPopup;
    }
    
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }

    public RichPopup getSiphonMlpOnLoadPopup() {
        return siphonMlpOnLoadPopup;
    }
}
