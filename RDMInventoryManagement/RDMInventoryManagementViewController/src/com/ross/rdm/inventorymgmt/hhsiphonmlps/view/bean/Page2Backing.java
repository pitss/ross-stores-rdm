package com.ross.rdm.inventorymgmt.hhsiphonmlps.view.bean;


import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;

import com.ross.rdm.inventorymgmt.hhrosscontinquirys.view.bean.HhRossContInquirySBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.trinidad.event.SelectionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMMobileBaseBackingBean {
    private RichLink f3Link;
    private RichLink f4Link;
    private RichTable lpnTable;
    static final String FIRST_RECORD = "FIRST RECORD";
    static final String LAST_RECORD = "LAST RECORD";
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);

    public Page2Backing() {

    }


    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            }
        }
    }

    public HhSiphonMlpSBean getSiphonMlpFlowBean() {
        return (HhSiphonMlpSBean) this.getPageFlowBean("HhSiphonMlpSBean");
    }

    public String f3Action() {
        this.setErrorOnSelectedItem(false);
        return "Page1";
    }


    public String f4Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_siphon_mlp_s_PAGE_2_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_siphon_mlp_s)){
        OperationBinding oper = ADFUtils.findOperation("executeF4Page2Logic");
        List<String> result = (List<String>) oper.execute();
        if (result != null && result.size() > 1) {
            this.setErrorOnSelectedItem(true);
            this.showMessagesPanel(result.get(0).toString(),
                                   this.getMessage(result.get(1).toString(), result.get(0).toString(),
                                                   (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                   (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            return null;
        }
                                                }
        else{
            this.showMessagesPanel("E",
                                        this.getMessage("NOT_ALLOWED", "E",
                                                        (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            return null;
        }
        return "Page1";
    }


    private void pageUpDownHandler(String upOrDown) {
        DCIteratorBinding ib = ADFUtils.findIterator("HhSiphonMlpSPage2ItemViewIterator");
        long trow = ib.getEstimatedRowCount() - 1;
        long crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long nextPosition = 0;
        if ("UP".equalsIgnoreCase(upOrDown) && crow != 0) {
            nextPosition = crow - 4;
            if (nextPosition < 0)
                nextPosition = 0;
        } else if ("DOWN".equalsIgnoreCase(upOrDown) && crow != trow) {
            nextPosition = crow + 4;
            if (nextPosition > trow)
                nextPosition = trow;
        } else
            return;
        if (nextPosition >= 0 && nextPosition <= trow)
            ib.setCurrentRowIndexInRange((int) nextPosition);
        this.refreshContentOfUIComponent(this.getLpnTable());
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (!this.getPageFlowBean().isNotAllowed()) {
            if(StringUtils.isEmpty((String) this.getErrorWarnInfoMessage().getValue())){
                    this.showFirstLastRecordInfo();
            }
        }
        _logger.info("onRegionLoad End");
    }

    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib = ADFUtils.findIterator("HhSiphonMlpSPage2ItemViewIterator");
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == trow - 1 && !this.isErrorOnSelectedItem()) {
            this.showMessagesPanel(INFO, LAST_RECORD);
        } else if (crow == 0 && !this.isErrorOnSelectedItem()) {
            this.showMessagesPanel(INFO, FIRST_RECORD);
        } else if (!this.isErrorOnSelectedItem())
            this.hideMessagesPanel();
    }
    
    private HhSiphonMlpSBean getPageFlowBean() {
        return (HhSiphonMlpSBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhSiphonMlpSBean");
    }

    private boolean isErrorOnSelectedItem() {
        return this.getSiphonMlpFlowBean().isErrorOnSelectedItem();
    }

    private void setErrorOnSelectedItem(boolean is) {
        this.getSiphonMlpFlowBean().setErrorOnSelectedItem(is);
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        this.setErrorOnSelectedItem(false);
        JSFUtils.resolveMethodExpression("#{bindings.HhSiphonMlpSPage2ItemView.collectionModel.makeCurrent}",
                                         SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                         selectionEvent });
        this.showFirstLastRecordInfo();
    }

    public String f7Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_siphon_mlp_s_PAGE_2_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_siphon_mlp_s)){
        this.pageUpDownHandler("UP");
                                                }
        else{
            this.showMessagesPanel("E",
                                        this.getMessage("NOT_ALLOWED", "E",
                                                        (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            this.getPageFlowBean().setNotAllowed(true);
        }
        return null;
                                                
    }

    public String f8Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_siphon_mlp_s_PAGE_2_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_siphon_mlp_s)){
        this.pageUpDownHandler("DOWN");
                                                }
        else{
            this.showMessagesPanel("E",
                                        this.getMessage("NOT_ALLOWED", "E",
                                                        (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            this.getPageFlowBean().setNotAllowed(true);
        }
                                                
        return null;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setLpnTable(RichTable lpnTable) {
        this.lpnTable = lpnTable;
    }

    public RichTable getLpnTable() {
        return lpnTable;
    }
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }
}
