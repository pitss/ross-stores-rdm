package com.ross.rdm.inventorymgmt.hhxrefcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMInventoryManagementBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichLink exitLink;
    private RichLink clearLink;
    private RichInputText inputFromContainer;
    private RichInputText inputToContainer;
    private RichIcon iconFromContainer;
    private RichIcon iconToContainer;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;

    private final static String FROM_CONTAINER_COMPONENT_ID = "fro1";
    private final static String SPLIT_ITERATOR_NAME = "HhXrefContainerSSplitContainerViewIterator";

    private final static String FROM_CONTAINER_ITEM = "Split_Container.From_Container_Id";
    private final static String TO_CONTAINER_ITEM = "Split_Container.To_Container_Id";
    private final static String UNIT_QTY_ITEM = "Split_Container.Unit_Qty";
    private final static String ITEM_UPC_ITEM = "Split_Container.Item_UPC";
    private final static String SHOW_CONFIRM_POPUP = "CONFIRM_POPUP";
    private RichInputText inputUnitQty;
    private RichIcon iconUnitQty;
    private RichPanelFormLayout pflMain;

    public Page1Backing() {

    }

    public void onChangedFromContainerId(ValueChangeEvent vce) {
        String newValue = (String) vce.getNewValue();
        if (newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())) {
            this.updateModel(vce);
            OperationBinding oper = ADFUtils.findOperation("validateFromContainer");
            List<String> results = (List<String>) oper.execute();
            if (results != null && !results.isEmpty()) {
                this.showMessagesPanel(results.get(0), results.get(1));

                this.getIconFromContainer().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconFromContainer());
                this.addErrorStyleToComponent(this.getInputFromContainer());
                this.setFocusOnUIComponent(this.getInputFromContainer());
            } else {
                this.hideMessagesPanel();

                this.getInputFromContainer().setDisabled(true);
                this.removeErrorStyleToComponent(this.getInputFromContainer());
                this.getIconFromContainer().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconFromContainer());

                this.getInputToContainer().setDisabled(false);
                this.setFocusOnUIComponent(this.getInputToContainer());
            }
        }
        refreshContentOfUIComponent(getPflMain());
    }

    public void onChangedToContainerId(ValueChangeEvent vce) {
        String newValue = (String) vce.getNewValue();
        if (newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())) {
            this.updateModel(vce);
            this.callValidateToContainer(NO);
        }
    }

    private void callValidateToContainer(String pSecondCall) {
        OperationBinding oper = ADFUtils.findOperation("validateToContainer");
        oper.getParamsMap().put("callSaveLaborProd", Boolean.TRUE);
        oper.getParamsMap().put("pSecondCall", pSecondCall);
        List<String> results = (List<String>) oper.execute();
        if (results != null && !results.isEmpty()) {
            if (results.size() == 2) {
                this.showMessagesPanel(results.get(0), results.get(1));

                this.getIconToContainer().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconToContainer());
                this.addErrorStyleToComponent(this.getInputToContainer());
                this.setFocusOnUIComponent(this.getInputToContainer());
            } else if (results.size() == 3) {
                if (SHOW_CONFIRM_POPUP.equals(results.get(2))) {
                    this.getInputToContainer().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputToContainer());

                    this.getDialogOutputText().setValue(results.get(1));
                    this.getConfirmPopup().show(new RichPopup.PopupHints());
                } else if (MSGTYPE_M.equals(results.get(0))) {
                    // SUCCESS CASE
                    this.showMessagesPanel(results.get(0), results.get(1));

                    this.getIconUnitQty().setVisible(false);
                    this.getIconUnitQty().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getIconUnitQty());
                    
                    this.getInputToContainer().setDisabled(true);
                    this.removeErrorStyleToComponent(this.getInputToContainer());
                    this.getIconToContainer().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getIconToContainer());

                    this.getInputFromContainer().setDisabled(false);
                    this.setFocusOnUIComponent(this.getInputFromContainer());
                } else {
                    this.showMessagesPanel(results.get(0), results.get(1));

                    this.setFocusAndErrorOnField(results.get(2));
                }
            }
        }
    }

    public String yesLinkConfirmPopupAction() {
        this.getDialogOutputText().setValue(null);
        this.getConfirmPopup().hide();

        this.callValidateToContainer(YES);
        return null;
    }

    public String noLinkConfirmPopupAction() {
        this.getDialogOutputText().setValue(null);
        this.getConfirmPopup().hide();

        this.getInputToContainer().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputToContainer());
        return null;
    }

    private void setFocusAndErrorOnField(String item) {
        this.getInputToContainer().setDisabled(true);
        this.removeErrorStyleToComponent(this.getInputToContainer());
        this.getIconToContainer().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconToContainer());

        RichInputText inputText = null;
        RichIcon icon = null;
        if (FROM_CONTAINER_ITEM.equals(item)) {
            inputText = this.getInputFromContainer();
            icon = this.getIconFromContainer();
        } else if (TO_CONTAINER_ITEM.equals(item)) {
            inputText = this.getInputToContainer();
            icon = this.getIconToContainer();
        } else if (UNIT_QTY_ITEM.equals(item)) {
            inputText = this.getInputUnitQty();
            icon = this.getIconUnitQty();
            this.getIconUnitQty().setVisible(true);
        } else if (ITEM_UPC_ITEM.equals(item)) {
            // TODO ????
        }
        if (icon !=null){
            icon.setName("error");
            this.refreshContentOfUIComponent(icon);
        }
       
        if (inputText != null){
            inputText.setDisabled(false);
            this.addErrorStyleToComponent(inputText);
            this.setFocusOnUIComponent(inputText);
        }

    }

    public String exitAction() {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation("callTrExit");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            this.hideMessagesPanel();

            this.getInputToContainer().setDisabled(true);
            this.removeErrorStyleToComponent(this.getInputToContainer());
            this.getIconToContainer().setName(REQ_ICON);
            this.refreshContentOfUIComponent(this.getIconToContainer());

            this.getInputFromContainer().setDisabled(false);

            action = this.logoutExitBTF();
        }
        return action;
    }

    public String clearAction() {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_xref_container_s_SPLIT_CONTAINER_F8,
                                           RoleBasedAccessConstants.FORM_NAME_hh_xref_container_s)) {
            OperationBinding oper = null;
            if (ADFUtils.findIterator(SPLIT_ITERATOR_NAME).getCurrentRow() != null) {
                oper = ADFUtils.findOperation("clearBlock");
            } else {
                oper = ADFUtils.findOperation("initXrefContainerS");
            }
            oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                this.hideMessagesPanel();

                this.getIconUnitQty().setVisible(false);
                this.getIconUnitQty().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconUnitQty());

                this.getInputToContainer().setDisabled(true);
                this.removeErrorStyleToComponent(this.getInputToContainer());
                this.getIconToContainer().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconToContainer());

                this.getIconFromContainer().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconFromContainer());
                this.getInputFromContainer().setDisabled(false);
                this.removeErrorStyleToComponent(this.getInputFromContainer());
                this.setFocusOnUIComponent(this.getInputFromContainer());
            }
        } else {
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
        return null;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String item = (String) clientEvent.getParameters().get("item");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (FROM_CONTAINER_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInputFromContainer().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputFromContainer().getValue())) {
                        this.onChangedFromContainerId(new ValueChangeEvent(this.getInputFromContainer(), null,
                                                                      submittedValue));
                    }
                } else {
                    if ((StringUtils.isEmpty((String) this.getInputToContainer().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getInputToContainer().getValue())) {
                        this.onChangedToContainerId(new ValueChangeEvent(this.getInputToContainer(), null, submittedValue));
                    }
                }
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getClearLink());
                actionEvent.queue();
            }
        }
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setClearLink(RichLink clearLink) {
        this.clearLink = clearLink;
    }

    public RichLink getClearLink() {
        return clearLink;
    }

    public void setInputFromContainer(RichInputText inputFromContainer) {
        this.inputFromContainer = inputFromContainer;
    }

    public RichInputText getInputFromContainer() {
        return inputFromContainer;
    }

    public void setInputToContainer(RichInputText inputToContainer) {
        this.inputToContainer = inputToContainer;
    }

    public RichInputText getInputToContainer() {
        return inputToContainer;
    }

    public void setIconFromContainer(RichIcon iconFromContainer) {
        this.iconFromContainer = iconFromContainer;
    }

    public RichIcon getIconFromContainer() {
        return iconFromContainer;
    }

    public void setIconToContainer(RichIcon iconToContainer) {
        this.iconToContainer = iconToContainer;
    }

    public RichIcon getIconToContainer() {
        return iconToContainer;
    }

    public void setInputUnitQty(RichInputText inputUnitQty) {
        this.inputUnitQty = inputUnitQty;
    }

    public RichInputText getInputUnitQty() {
        return inputUnitQty;
    }

    public void setIconUnitQty(RichIcon iconUnitQty) {
        this.iconUnitQty = iconUnitQty;
    }

    public RichIcon getIconUnitQty() {
        return iconUnitQty;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }
}
