package com.ross.rdm.inventorymgmt.hhaddtocontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhAddToContainerSBean {
    public static final String GENERIC_CONTAINER_ID = "GENERIC_CONTAINER_ID";
    public static final String LOCATION_ID = "LOCATION_ID";
    public static final String CONTAINER_ID = "CONTAINER_ID";
    private String page1Focus = null;
    private boolean disablePage1 = false;
    private boolean disablePage2 = false;
    private boolean showSuccessMessage = false;
    private Boolean executedKey = Boolean.FALSE;

    public void initTaskFlow() {
        //Forms  Module : hh_add_to_container_s
        //Object        : WHEN-NEW-FORM-INSTANCE
        //Source Code   :
        //
        //Tr_Startup;
        //
        //startup_local;
        //AHL.SET_AHL_INFO(:WORK.FACILITY_ID,:SYSTEM.CURRENT_FORM);
        ADFUtils.findOperation("initTaskFlowAddToContainer").execute();
    }

    public void setPage1Focus(String page1Focus) {
        this.page1Focus = page1Focus;
    }

    public String getPage1Focus() {
        return page1Focus;
    }

    public Boolean getDisabledGenericContainer() {
        return disablePage1 || (page1Focus != null && !GENERIC_CONTAINER_ID.equals(page1Focus));
    }

    public Boolean getDisabledLocationId() {
        return disablePage1 || !LOCATION_ID.equals(page1Focus);
    }

    public Boolean getDisabledContainerId() {
        return disablePage1 || !CONTAINER_ID.equals(page1Focus);
    }

    public void setDisablePage1(boolean disablePage1) {
        this.disablePage1 = disablePage1;
    }

    public boolean isDisablePage1() {
        return disablePage1;
    }

    public void setDisablePage2(boolean disablePage2) {
        this.disablePage2 = disablePage2;
    }

    public boolean isDisablePage2() {
        return disablePage2;
    }

    public void setShowSuccessMessage(boolean showSuccessMessage) {
        this.showSuccessMessage = showSuccessMessage;
    }

    public boolean isShowSuccessMessage() {
        return showSuccessMessage;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
}
