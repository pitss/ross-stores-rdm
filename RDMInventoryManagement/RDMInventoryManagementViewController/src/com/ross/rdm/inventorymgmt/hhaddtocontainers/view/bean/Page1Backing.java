package com.ross.rdm.inventorymgmt.hhaddtocontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.base.RDMMobileBaseBean;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMMobileBaseBean {

    private static final String LOCATION_ID_BINDING = "LocationId1";
    private static final String LEAVE_BLANK_ADD_HINT = "Leave blank for ADD.";
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f5Link;
    private RichInputText genericContainer;
    private RichInputText locationId;
    private RichInputText containerId;
    private RichIcon genericContainerIcon;
    private RichIcon locationIdIcon;
    private RichIcon containerIdIcon;
    private RichPopup confirmPopup;
    private RichOutputText confirmPopupText;
    private RichLink confirmPopupYesLink;
    private RichLink confirmPopupNoLink;
    private RichPanelFormLayout pflMain;
    private RichLink changeGenericContainerHiddenLink;
    private RichPopup labeledPopup1;
    private RichOutputText labeledPopup1Text;
    private RichLink labeledPopup1YesLink;
    private RichLink labeledPopup1NoLink;
    private RichIcon labeledPopup1Icon;
    private RichLink changeLocationHiddenLink;
    private RichLink changeContainerHiddenLink;
    private RichDialog labeledPopup1Dialog;

    public Page1Backing() {
    }

    public void onChangedGenericContainerId(ValueChangeEvent vce) {
        updateModel(vce);
        if (!isNavigationExecuted() && !getPageFlowBean().getDisabledGenericContainer()) {
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeGenericContainerHiddenLink());
            actionEvent.queue();
        }
    }

    public void onChangedLocationId(ValueChangeEvent vce) {
        updateModel(vce);
        if (!isNavigationExecuted() && !getPageFlowBean().getDisabledLocationId()) {
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeLocationHiddenLink());
            actionEvent.queue();
        }
    }

    private List validateLocation(String newValue) {
        OperationBinding op = ADFUtils.findOperation("validateLocation");
        op.getParamsMap().put("locationId", newValue);
        return (List) op.execute();
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        updateModel(vce);
        if (!isNavigationExecuted() && !getPageFlowBean().getDisabledContainerId()) {
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeContainerHiddenLink());
            actionEvent.queue();
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public String f3Action() {
        ADFUtils.findOperation("deleteRossEventcodeUseridTmp").execute();
        return logoutExitBTF();
    }

    public String f4Action() {
       
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_add_to_container_s_CONTAINER_F4,RoleBasedAccessConstants.FORM_NAME_hh_add_to_container_s)){
                   ADFUtils.findOperation("clearMethodBlock").execute();
                   ADFUtils.findOperation("clearContainerBlock").execute();

                   getGenericContainer().resetValue();
                   getLocationId().resetValue();
                   getContainerId().resetValue();

                   switchFocusToField(getGenericContainer(), HhAddToContainerSBean.GENERIC_CONTAINER_ID);
                   refreshContentOfUIComponent(getPflMain());
                   return null;    
               }else{
                   showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
                    return null;
               }
    }

    public String f5Action() {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_add_to_container_s_CONTAINER_F5,RoleBasedAccessConstants.FORM_NAME_hh_add_to_container_s)){
          String nav = null;
        if (HhAddToContainerSBean.CONTAINER_ID.equals(getPageFlowBean().getPage1Focus()) &&
            StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("GenericContainerId1")) &&
            StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue(LOCATION_ID_BINDING))) {
            ADFUtils.findOperation("clearPopupEditorBlock").execute();
            nav = "goPage2";
        }
        return nav;
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
            return null;
        }
    }

    private void queueValueChangeEvent(RichInputText field, String submittedVal) {
        switch (field.getId()) {
        case "gen1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedGenericContainerId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "loc1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedLocationId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "con1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedContainerId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        default:
            
        }
    }

    private void switchFocusToField(RichInputText input, String focus) {
        removeAllErrors();
        getPageFlowBean().setPage1Focus(focus);
        setFocusOnUIComponent(input);

        if (HhAddToContainerSBean.CONTAINER_ID.equals(focus)) {
            showMessagesPanel(INFO, LEAVE_BLANK_ADD_HINT);
        }
    }

    private void removeAllErrors() {
        removeErrorOfField(getGenericContainer(), getGenericContainerIcon());
        removeErrorOfField(getLocationId(), getLocationIdIcon());
        removeErrorOfField(getContainerId(), getContainerIdIcon());
        hideMessagesPanel();
    }

    private HhAddToContainerSBean getPageFlowBean() {
        return (HhAddToContainerSBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhAddToContainerSBean");
    }

    public void setGenericContainer(RichInputText genericContainer) {
        this.genericContainer = genericContainer;
    }

    public RichInputText getGenericContainer() {
        return genericContainer;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setGenericContainerIcon(RichIcon genericContainerIcon) {
        this.genericContainerIcon = genericContainerIcon;
    }

    public RichIcon getGenericContainerIcon() {
        return genericContainerIcon;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledGenericContainer()) {
            setFocusOnUIComponent(getGenericContainer());
        } else if (!getPageFlowBean().getDisabledLocationId()) {
            setFocusOnUIComponent(getLocationId());
        } else if (!getPageFlowBean().getDisabledContainerId()) {
            setFocusOnUIComponent(getContainerId());
            if (StringUtils.isEmpty((String) getErrorWarnInfoMessage().getValue())) {
                showMessagesPanel(INFO, LEAVE_BLANK_ADD_HINT);
            }
        }

        if (getPageFlowBean().isShowSuccessMessage()) {
            getPageFlowBean().setShowSuccessMessage(false);
            showMessagesPanel(MSGTYPE_M, this.getMessage("SUCCESS_OPER"));
        }
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmPopupText(RichOutputText confirmPopupText) {
        this.confirmPopupText = confirmPopupText;
    }

    public RichOutputText getConfirmPopupText() {
        return confirmPopupText;
    }

    public void setConfirmPopupYesLink(RichLink confirmPopupYesLink) {
        this.confirmPopupYesLink = confirmPopupYesLink;
    }

    public RichLink getConfirmPopupYesLink() {
        return confirmPopupYesLink;
    }

    public void setConfirmPopupNoLink(RichLink confirmPopupNoLink) {
        this.confirmPopupNoLink = confirmPopupNoLink;
    }

    public RichLink getConfirmPopupNoLink() {
        return confirmPopupNoLink;
    }

    public String confirmPopupYesAction() {
        String nav = null;
        ADFUtils.findOperation("finishVGenericContainerId").execute();
        nav = (String) ADFUtils.findOperation("finishKeyNextItemGenericContainer").execute();
        refreshContentOfUIComponent(getPflMain());
        return nav;
    }

    public String confirmPopupNoAction() {
        getPageFlowBean().setDisablePage1(false);
        refreshContentOfUIComponent(getPflMain());
        getConfirmPopup().hide();
        return null;
    }

    public void performKeyConfirmPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmPopupYesLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmPopupNoLink());
                actionEvent.queue();
            }
        }
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    public String changeGenericContainerIdAction() {
        String nav = null;
        boolean nextField = true;
        boolean finishKeyNext = true;

        Map result = (Map) ADFUtils.findOperation("vGenericContainerId").execute();
        if (result != null) {
            List errorsMessages = (List) result.get("errorsMessages");
            String containerExists = (String) result.get("ContainerExists");
            if (errorsMessages != null && errorsMessages.size() == 2) {
                nextField = false;
                finishKeyNext = false;
                if (CONF.equals(errorsMessages.get(0))) {
                    getPageFlowBean().setDisablePage1(true);
                    refreshContentOfUIComponent(getPflMain());
                    getConfirmPopupText().setValue(errorsMessages.get(1));
                    getConfirmPopup().show(new RichPopup.PopupHints());
                } else {
                    setErrorOnField(getGenericContainer(), getGenericContainerIcon());
                    showMessagesPanel((String) errorsMessages.get(0), (String) errorsMessages.get(1));
                }
            } else if (errorsMessages == null) {
                if (NO.equals(containerExists)) {
                    nextField = true; //Enable Location_ID field
                    finishKeyNext = true;
                } else if (YES.equals(containerExists)) {
                    nextField = false;
                    List errorMessages =
                        validateLocation((String) ADFUtils.getBoundAttributeValue(LOCATION_ID_BINDING));
                    if (errorMessages == null) { //LOCATION IS OK
                        switchFocusToField(getContainerId(), HhAddToContainerSBean.CONTAINER_ID);
                        finishKeyNext = true;
                    } else {
                        ADFUtils.setBoundAttributeValue(LOCATION_ID_BINDING, null);
                        refreshContentOfUIComponent(getLocationId());
                        setErrorOnField(getGenericContainer(), getGenericContainerIcon());
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                }
            }
        }

        if (nextField) {
            switchFocusToField(getLocationId(), HhAddToContainerSBean.LOCATION_ID);
        }
        if (finishKeyNext) {
            nav = (String) ADFUtils.findOperation("finishKeyNextItemGenericContainer").execute();
        }
        refreshContentOfUIComponent(getPflMain());
        return nav;
    }

    public void setChangeGenericContainerHiddenLink(RichLink changeGenericContainerHiddenLink) {
        this.changeGenericContainerHiddenLink = changeGenericContainerHiddenLink;
    }

    public RichLink getChangeGenericContainerHiddenLink() {
        return changeGenericContainerHiddenLink;
    }

    private void addLabeledContainer(String procedure) {
        OperationBinding op = ADFUtils.findOperation("addLabeledContainer");
        op.getParamsMap().put("procedure", procedure);
        List errorMessages = (List) op.execute();
        if (errorMessages != null && errorMessages.size() > 1) {
            //Focus last item of the list??
            if (errorMessages.size() == 3 && HhAddToContainerSBean.GENERIC_CONTAINER_ID.equals(errorMessages.get(2))) {
                switchFocusToField(getGenericContainer(), HhAddToContainerSBean.GENERIC_CONTAINER_ID);
            }

            String errorType = (String) errorMessages.get(0);
            if (CONF.equals(errorType)) { //Confirmation popup
                showPopupAddLabeledContainer((String) errorMessages.get(1));
            } else {
                if (WARN.equals(errorType) || ERROR.equals(errorType)) {
                    setErrorOnSelectedField();
                }
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            }
        }
        refreshContentOfUIComponent(getPflMain());
    }

    public void setLabeledPopup1(RichPopup labeledPopup1) {
        this.labeledPopup1 = labeledPopup1;
    }

    public RichPopup getLabeledPopup1() {
        return labeledPopup1;
    }

    public void setLabeledPopup1Text(RichOutputText labeledPopup1Text) {
        this.labeledPopup1Text = labeledPopup1Text;
    }

    public RichOutputText getLabeledPopup1Text() {
        return labeledPopup1Text;
    }

    private void showPopupAddLabeledContainer(String messageCode) {
        getLabeledPopup1Icon().setName("MULTI_DEST_ADD".equals(messageCode) ? "warning" : "confirmation");
        getLabeledPopup1Dialog().setTitle("MULTI_DEST_ADD".equals(messageCode) ? "Warning" : "Confirmation");
        getLabeledPopup1Text().setValue(getMessage(messageCode));

        getPageFlowBean().setDisablePage1(true);
        refreshContentOfUIComponent(getPflMain());
        getLabeledPopup1().setLauncherVar(messageCode);
        getLabeledPopup1().show(new RichPopup.PopupHints());
    }

    public String labeledPopup1YesAction() {
        String messageCode = getLabeledPopup1().getLauncherVar();
        if ("ASSIGN_DC_DEST".equals(messageCode)) {
            ADFUtils.setBoundAttributeValue("PopupAccepted", "1");
            addLabeledContainer("ADD_LABELED_CONTAINER1_PART2");
        } else if ("NO_FROM_BOL".equals(messageCode)) {
            addLabeledContainer("ADD_LABELED_CONTAINER1_PART3");
        } else if ("NO_TO_BOL".equals(messageCode)) {
            ADFUtils.setBoundAttributeValue("PopupAccepted", "2");
            addLabeledContainer("ADD_LABELED_CONTAINER1_PART3");
        } else if ("MULTI_DEST_ADD".equals(messageCode)) {
            ADFUtils.setBoundAttributeValue("PopupAccepted", "3");
            addLabeledContainer("ADD_LABELED_CONTAINER1_PART3");
        } else if ("BLD_MTH_FIN_C".equals(messageCode) || "BLD_MTH_RTE_C".equals(messageCode) ||
                   "BLD_MTH_NXT_C".equals(messageCode)) {
            ADFUtils.setBoundAttributeValue("PopupAccepted", "4");
            addLabeledContainer("ADD_LABELED_CONTAINER1_PART4");
        }

        getPageFlowBean().setDisablePage1(false);
        getLabeledPopup1().hide();
        refreshContentOfUIComponent(getPflMain());
        return null;
    }

    public String labeledPopup1NoAction() {
        String messageCode = getLabeledPopup1().getLauncherVar();

        if ("ASSIGN_DC_DEST".equals(messageCode)) {
            setErrorOnField(getContainerId(), getContainerIdIcon());
            showMessagesPanel("W", getMessage("INV_DEST_ID"));
        } else if ("BLD_MTH_FIN_C".equals(messageCode) || "BLD_MTH_RTE_C".equals(messageCode) ||
                   "BLD_MTH_NXT_C".equals(messageCode)) {
            ADFUtils.setBoundAttributeValue("LContinue", NO);
            addLabeledContainer("ADD_LABELED_CONTAINER1_PART4");
        }
        //NO_FROM_BOL,NO_TO_BOL,MULTI_DEST_ADD will close the popup only
        getPageFlowBean().setDisablePage1(false);
        getLabeledPopup1().hide();
        refreshContentOfUIComponent(getPflMain());
        return null;
    }

    public void setLabeledPopup1YesLink(RichLink labeledPopup1YesLink) {
        this.labeledPopup1YesLink = labeledPopup1YesLink;
    }

    public RichLink getLabeledPopup1YesLink() {
        return labeledPopup1YesLink;
    }

    public void setLabeledPopup1NoLink(RichLink labeledPopup1NoLink) {
        this.labeledPopup1NoLink = labeledPopup1NoLink;
    }

    public RichLink getLabeledPopup1NoLink() {
        return labeledPopup1NoLink;
    }

    public void performKeyLabeledPopup1(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getLabeledPopup1YesLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getLabeledPopup1NoLink());
                actionEvent.queue();
            }
        }
    }

    public void setLabeledPopup1Icon(RichIcon labeledPopup1Icon) {
        this.labeledPopup1Icon = labeledPopup1Icon;
    }

    public RichIcon getLabeledPopup1Icon() {
        return labeledPopup1Icon;
    }

    private void setErrorOnSelectedField() {
        if (HhAddToContainerSBean.GENERIC_CONTAINER_ID.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getGenericContainer(), getGenericContainerIcon());
        } else if (HhAddToContainerSBean.LOCATION_ID.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getLocationId(), getLocationIdIcon());
        } else if (HhAddToContainerSBean.CONTAINER_ID.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getContainerId(), getContainerIdIcon());
        }
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_inventorymgmt_hhaddtocontainers_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public String changeLocationHiddenAction() {
        if (!getPageFlowBean().getExecutedKey()) {
            List errorMessages = validateLocation((String) ADFUtils.getBoundAttributeValue("LocationId1"));
            if (errorMessages != null) {
                setErrorOnField(getLocationId(), getLocationIdIcon());
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            } else {
                switchFocusToField(getContainerId(), HhAddToContainerSBean.CONTAINER_ID);
            }
        }
        refreshContentOfUIComponent(getPflMain());
        return null;
    }

    public String changeContainerHiddenAction() {
        if (!getPageFlowBean().getExecutedKey()) {
            removeAllErrors();
            showMessagesPanel(INFO, LEAVE_BLANK_ADD_HINT); //RESET HINT
            addLabeledContainer("ADD_LABELED_CONTAINER1");
        }
        refreshContentOfUIComponent(getPflMain());
        return null;
    }

    public void setChangeLocationHiddenLink(RichLink changeLocationHiddenLink) {
        this.changeLocationHiddenLink = changeLocationHiddenLink;
    }

    public RichLink getChangeLocationHiddenLink() {
        return changeLocationHiddenLink;
    }

    public void setChangeContainerHiddenLink(RichLink changeContainerHiddenLink) {
        this.changeContainerHiddenLink = changeContainerHiddenLink;
    }

    public RichLink getChangeContainerHiddenLink() {
        return changeContainerHiddenLink;
    }

    public void setLabeledPopup1Dialog(RichDialog labeledPopup1Dialog) {
        this.labeledPopup1Dialog = labeledPopup1Dialog;
    }

    public RichDialog getLabeledPopup1Dialog() {
        return labeledPopup1Dialog;
    }
}
