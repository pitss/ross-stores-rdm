package com.ross.rdm.inventorymgmt.hhaddtocontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.inventorymgmt.base.RDMMobileBaseBean;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.sql.Types;

import java.util.Iterator;
import java.util.List;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Key;

import org.apache.myfaces.trinidad.model.RowKeySet;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Method.jspx
// ---
// ---------------------------------------------------------------------
public class MethodBacking extends RDMMobileBaseBean {
    private static final String GO_PAGE1 = "goPage1";
    private static final String BUILD_METHOD_BINDING = "BuildMethod1";
    private RichTable methodTable;

    public MethodBacking() {

    }

    public String f3ExitAction() {
        ADFUtils.findOperation("clearMethodBlock").execute();
        ADFUtils.setBoundAttributeValue("ContStatus", null);
        ADFUtils.setBoundAttributeValue("ContDestId", null);
        ADFUtils.setBoundAttributeValue("NewContainerFlag", null);
        getPageFlowBean().setPage1Focus(HhAddToContainerSBean.LOCATION_ID);
        return GO_PAGE1;
    }

    public String f4DoneAction() {
        RowKeySet keySet = getMethodTable().getSelectedRowKeys();
        String selected = null;
        if (keySet != null) {
            Iterator selectedEmpIter = keySet.iterator();
            while (selectedEmpIter.hasNext()) {
                Key key = (Key) ((List) selectedEmpIter.next()).get(0);
                selected = (String) key.getKeyValues()[0];
            }
        }
        ADFUtils.setBoundAttributeValue(BUILD_METHOD_BINDING, selected);

        String nav = null;
        OperationBinding op = ADFUtils.findOperation("validateAndSaveMethod1");
        op.getParamsMap().put("buildMethod", ADFUtils.getBoundAttributeValue(BUILD_METHOD_BINDING));
        List errorMessages = (List) op.execute();
        if (errorMessages != null) {
            showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
        } else {
            getPageFlowBean().setPage1Focus(HhAddToContainerSBean.LOCATION_ID);
            nav = GO_PAGE1;
        }
        return nav;
    }

    private HhAddToContainerSBean getPageFlowBean() {
        return (HhAddToContainerSBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhAddToContainerSBean");
    }

    public void setMethodTable(RichTable methodTable) {
        this.methodTable = methodTable;
    }

    public RichTable getMethodTable() {
        return methodTable;
    }
}
