package com.ross.rdm.inventorymgmt.hhaddtocontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.base.RDMMobileBaseBean;

import java.util.List;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMMobileBaseBean {
    private static final String GO_PAGE_1 = "goPage1";
    private RichInputText containerQty;
    private RichIcon containerQtyIcon;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichPopup exitPopup;
    private RichOutputText exitPopupText;

    public Page2Backing() {
    }

    public void setContainerQty(RichInputText containerQty) {
        this.containerQty = containerQty;
    }

    public RichInputText getContainerQty() {
        return containerQty;
    }

    public void setContainerQtyIcon(RichIcon containerQtyIcon) {
        this.containerQtyIcon = containerQtyIcon;
    }

    public RichIcon getContainerQtyIcon() {
        return containerQtyIcon;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) &&
                       "qty".equals(clientEvent.getComponent().getId())) {
                validateContainerQty(submittedVal);
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public String f3Action() {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_add_to_container_s_POPUP_EDITOR_F3,RoleBasedAccessConstants.FORM_NAME_hh_add_to_container_s)){
        ADFUtils.findOperation("deleteRossEventcodeUseridTmp").execute();
        ADFUtils.findOperation("clearPopupEditorBlock").execute();
        getPageFlowBean().setPage1Focus(HhAddToContainerSBean.CONTAINER_ID);
        return GO_PAGE_1;
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
            return null;
        }
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public String f4Action() {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_add_to_container_s_POPUP_EDITOR_F4,RoleBasedAccessConstants.FORM_NAME_hh_add_to_container_s)){
        String nav = null;
        if (validateContainerQty((String) ADFUtils.getBoundAttributeValue("ContainerQty"))) {
            List errorMessages = (List) ADFUtils.findOperation("addUnlabeledContainer").execute();
            if (errorMessages != null) {
                if (errorMessages.size() == 1 && "SUCCESS_OPER".equals(errorMessages.get(0))) {
                    getPageFlowBean().setShowSuccessMessage(true);
                    nav = clearAndGoPage1();
                } else if (errorMessages.size() == 3 && "POPUP".equals(errorMessages.get(2))) {
                    getPageFlowBean().setDisablePage2(true);
                    getExitPopupText().setValue(errorMessages.get(1));
                    getExitPopup().show(new RichPopup.PopupHints());
                } else if (errorMessages.size() == 2) {
                    String type = (String) errorMessages.get(0);
                    if (WARN.equals(type) || ERROR.equals(type)) {
                        setErrorOnField(getContainerQty(), getContainerQtyIcon());
                    }
                    showMessagesPanel(type, (String) errorMessages.get(1));
                }
            }
        }
        return nav;
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
            return null;
        }
    }

    private HhAddToContainerSBean getPageFlowBean() {
        return (HhAddToContainerSBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhAddToContainerSBean");
    }

    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    public void setExitPopupText(RichOutputText exitPopupText) {
        this.exitPopupText = exitPopupText;
    }

    public RichOutputText getExitPopupText() {
        return exitPopupText;
    }

    public String okExitPopupAction() {
        getPageFlowBean().setDisablePage2(false);
        return clearAndGoPage1();
    }

    private String clearAndGoPage1() {
        getPageFlowBean().setPage1Focus(HhAddToContainerSBean.GENERIC_CONTAINER_ID);
        ADFUtils.findOperation("clearPopupEditorBlock").execute();
        ADFUtils.findOperation("clearContainerBlock").execute();
        return GO_PAGE_1;
    }

    private boolean validateContainerQty(String submittedVal) {
        boolean valid = true;
        if (StringUtils.isEmpty(submittedVal)) {
            this.setErrorOnField(getContainerQty(), getContainerQtyIcon());
            this.showMessagesPanel(ERROR, this.getMessage("PARTIAL_ENTRY"));
            valid = false;
        } else if (!submittedVal.replace("-", "").matches("[0-9]+")) {
            this.setErrorOnField(getContainerQty(), getContainerQtyIcon());
            this.showMessagesPanel(WARN, this.getMessage("MUST_BE_NUMERIC"));
            valid = false;
        } else {
            this.removeErrorOfField(getContainerQty(), getContainerQtyIcon());
            this.hideMessagesPanel();
        }
        return valid;
    }
}
