package com.ross.rdm.inventorymgmt.base;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import org.apache.commons.lang.StringUtils;
//TODO The common bean name "RDMHotelPickingBackingBean" should be changed, then this class hierarchy should be changed too
public class RDMMobileBaseBean extends RDMHotelPickingBackingBean {
    @SuppressWarnings("compatibility:4366738590034945377")
    private static final long serialVersionUID = 1L;
    public static final String TR_EXIT = "TR_EXIT";

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    public RDMMobileBaseBean() {
        super();
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    protected String getMessage(String messageCode) {
        return this.getMessage(messageCode, WARN, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                               (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
    }

    protected void setErrorOnField(RichInputText input, RichIcon icon) {
        this.addErrorStyleToComponent(input);
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        this.setFocusOnUIComponent(input);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon) {
        this.removeErrorOfField(input, icon, REQ_ICON);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon, String name) {
        this.removeErrorStyleToComponent(input);
        icon.setName(name);
        this.refreshContentOfUIComponent(icon);
    }

    protected boolean isMessageShown() {
        return StringUtils.isNotEmpty((String) this.getErrorWarnInfoMessage().getValue());
    }
}
