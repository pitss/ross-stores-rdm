package com.ross.rdm.inventorymgmt.hhinvitemtroubles.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMInventoryManagementBackingBean {

    private static final String FIRST_RECORD = "FIRST RECORD";
    private static final String LAST_RECORD = "LAST RECORD";
    private static final String TROUBLE_CODE_ATTR = "TroubleCode";
    private static final String TROUBLE_ITER = "HhInvItemTroubleSItemTroubleCodeViewIterator";

    private RichLink f3Link;
    private RichLink f4Link;
    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessageOutput;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);

    public Page2Backing() {

    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            }
        }
    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            this.resetIterator();
            ADFUtils.invokeAction("Page1");
        } else if ("F4".equalsIgnoreCase(key)) {
            String troubleCode = this.getTroubleCodeFromTable();
            //Bind Trouble Code and navigate to Page 1
            ADFUtils.setBoundAttributeValue(TROUBLE_CODE_ATTR, troubleCode);
            this.resetIterator();
            ADFUtils.invokeAction("Page1");
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        //this.showFirstLastRecordInfo();
        _logger.info("onRegionLoad End");
    }

    private String getTroubleCodeFromTable() {
        String troubleCode = "";
        DCIteratorBinding iterBind = ADFUtils.findIterator(TROUBLE_ITER);
        if (iterBind != null && iterBind.getCurrentRow() != null) {
            troubleCode = (String) iterBind.getCurrentRow().getAttribute("TroubleCode");
        }
        return troubleCode;
    }

    public String getIconErrorMessage() {
        return getErrorMessage() != null ? INF_ICON : "";
    }

    public String getErrorMessage() {
        DCIteratorBinding ib = ADFUtils.findIterator(TROUBLE_ITER);
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        String text = null;
        if (trow > 0) {
            if (crow == trow - 1) {
                text = LAST_RECORD;
            } else if (crow <= 0) {
                text = FIRST_RECORD;
            }
        }
        return text;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    private void resetIterator() {
        (ADFUtils.findIterator(TROUBLE_ITER)).setCurrentRowIndexInRange(0);
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_inv_item_trouble_s_ITEM_TROUBLE_CODE_F4,
                                           RoleBasedAccessConstants.FORM_NAME_hh_inv_item_trouble_s)) {
            this.trKeyIn("F4");
        }else{
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }

    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessageOutput().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }
        this.getErrorMessageOutput().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessageOutput(RichOutputText errorMessageOutput) {
        this.errorMessageOutput = errorMessageOutput;
    }

    public RichOutputText getErrorMessageOutput() {
        return errorMessageOutput;
    }
}
