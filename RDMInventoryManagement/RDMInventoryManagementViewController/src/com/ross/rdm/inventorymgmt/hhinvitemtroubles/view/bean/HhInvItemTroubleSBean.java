package com.ross.rdm.inventorymgmt.hhinvitemtroubles.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhInvItemTroubleSBean {
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhInvItemTroubleSBean.class);  
    private String isFocusOn;
    private Boolean isContainerIdValidated;
    private Boolean isTroubleCodeValidated;
    private Boolean isError;
    private Boolean executedKeyPage1;


    public void initTaskFlow() {
        _logger.info("initTaskFlow() Start");
        this.initGlobalVariablesInvItemTrouble();
        this.initHhInvItemTroubleWorkVariables();
        
        this.setIsError(false);
        this.setIsContainerIdValidated(false);
        this.setIsTroubleCodeValidated(false);
        this.setIsFocusOn("ContainerId");
        
        _logger.info("initTaskFlow() End");
    }

    private void initGlobalVariablesInvItemTrouble() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesInvItemTroubleSBean");
        oper.execute();
        
    }

    private void initHhInvItemTroubleWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhInvItemTroubleSWorkVariables");
        oper.execute();
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsTroubleCodeValidated(Boolean isTroubleCodeValidated) {
        this.isTroubleCodeValidated = isTroubleCodeValidated;
    }

    public Boolean getIsTroubleCodeValidated() {
        return isTroubleCodeValidated;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }
    
    public void setExecutedKeyPage1(Boolean executedKeyPage1) {
        this.executedKeyPage1 = executedKeyPage1;
    }

    public Boolean getExecutedKeyPage1() {
        return executedKeyPage1;
    }
}
