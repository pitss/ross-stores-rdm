package com.ross.rdm.inventorymgmt.hhinvitemtroubles.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.hhinvitemtroubles.model.views.HhInvItemTroubleSContainerViewRowImpl;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMInventoryManagementBackingBean {

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private final static String HH_INV_ITEM_TROUBLE_FLOW_BEAN = "HhInvItemTroubleSBean";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String TROUBLE_CODE_ATTR = "TroubleCode";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";

    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private RichPanelFormLayout InvItemTroublePanel;
    private RichIcon containerIdIcon;
    private RichIcon troubleCodeIcon;
    private RichInputText containerId;
    private RichInputText troubleCode;

    private RichInputText troubleCode1;
    private RichInputText troubleCode2;
    private RichInputText troubleCode3;
    private RichInputText troubleCode4;
    private RichInputText troubleCode5;
    private RichInputText troubleCode6;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f9Link;
    private RichLink hiddenContainerLink;

    public Page1Backing() {

    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        /*
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateContainerId(enteredContainerIdValue);
        }
//        else {
//            this.getContainerIdIcon().setName(ERR_ICON);
//            this.refreshContentOfUIComponent(this.getContainerIdIcon());
//            this.refreshContentOfUIComponent(this.getContainerId());
//            this.addErrorStyleToComponent(this.getContainerId());
//            this.setFocusContainerId();
//            this.selectTextOnUIComponent(this.getContainerId());
//            this.showMessagesPanel(ERROR,
//                                   this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
//                                                   this.getLangCodeAttrValue()));
//            this.clearFields(false);
//        }
        */
        
        // OTM 3581
        updateModel(vce);
        this.getHhInvItemTroubleSPageFlowBean().setExecutedKeyPage1(false);
        ActionEvent actionEvent = new ActionEvent(this.getHiddenContainerLink());
        actionEvent.queue();
    }

    public String hiddenContainerLinkAction() {
        if (Boolean.FALSE.equals(this.getHhInvItemTroubleSPageFlowBean().getExecutedKeyPage1())) {
            String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
            if (StringUtils.isEmpty(containerId)) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
                this.refreshContentOfUIComponent(this.getContainerId());
                this.addErrorStyleToComponent(this.getContainerId());
                this.setFocusContainerId();
                this.selectTextOnUIComponent(this.getContainerId());
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                this.clearFields(false);
            } else {
                this.validateContainerId(containerId);
            }
        }
        else{
            this.getHhInvItemTroubleSPageFlowBean().setExecutedKeyPage1(false);
        }        
        return null;
    }

    public boolean validateContainerId(String containerId) {
        _logger.info("validateContainerId() Start");
        if (!this.getContainerId().isDisabled()) {
            if (this.getHhInvItemTroubleSPageFlowBean().getIsError() && containerId != null) {
                this.setErrorStyleClass(this.getContainerId(), true);
                this.selectTextOnUIComponent(this.getContainerId());
                this.getHhInvItemTroubleSPageFlowBean().setIsContainerIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getHhInvItemTroubleSPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getContainerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateContainerId");
                oper.getParamsMap().put("pContainerId", containerId);
                oper.getParamsMap().put("pTroubleCode", null);
                oper.getParamsMap().put("pFacilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));

                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel(ERROR, msg);
                            if (msg.contains("inbound trouble") || msg.contains("DUMMY SKU")) {
                                this.clearFields(true);
                            } else {
                                this.clearFields(false);
                            }
                        } else {
                            this.showMessagesPanel(ERROR, errorCodeList.get(0));
                            this.clearFields(false);
                        }
                        this.getContainerIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.setFocusContainerId();
                        this.selectTextOnUIComponent(this.getContainerId());
                        this.getHhInvItemTroubleSPageFlowBean().setIsContainerIdValidated(false);
                        _logger.info("validateContainerId() End");
                        return false;
                    } else {
                        // OTM 3581: ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, containerId);
                        this.getContainerIdIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getContainerId());
                        this.refreshContentOfUIComponent(this.getInvItemTroublePanel());
                        
                        // OTM 3581
                        this.setFocusTroubleCode();
                        //Invalidate Trouble Code
                        this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(false);
                    }
                }
            }
        }
        this.getHhInvItemTroubleSPageFlowBean().setIsContainerIdValidated(true);
        _logger.info("validateContainerId() End");
        return true;
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con1")) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            } else if (field.getId().equalsIgnoreCase("tro7")) {
                this.getTroubleCodeIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con1")) {
                this.getContainerIdIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            } else if (field.getId().equalsIgnoreCase("tro7")) {
                this.getTroubleCodeIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }


    public void onChangedTroubleCode(ValueChangeEvent vce) {
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredTroubleCodeValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateTroubleCode(enteredTroubleCodeValue);
        }
    }

    public boolean validateTroubleCode(String troubleCode) {
        _logger.info("validateTroubleCode() Start");
        if (!this.getTroubleCode().isDisabled()) {
            if (this.getHhInvItemTroubleSPageFlowBean().getIsError() && troubleCode != null) {
                this.setErrorStyleClass(this.getTroubleCode(), true);
                this.selectTextOnUIComponent(this.getTroubleCode());
                this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(false);
                _logger.info("validateTroubleCode() End");
                return false;
            }
            if (troubleCode == null) {
                this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(false);
                _logger.info("validateTroubleCode() End");
                return false;
            } else {
                troubleCode = troubleCode.toUpperCase();
                this.getHhInvItemTroubleSPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getTroubleCode(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateTroubleCode");
                oper.getParamsMap().put("pTroubleCode", troubleCode);
                oper.getParamsMap().put("pFacilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));

                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        String returnCode = (String) oper.getResult();
                        if (NO.equalsIgnoreCase(returnCode)) {
                            this.showMessagesPanel(ERROR,
                                                   this.getMessage("INV_TROUBLE", ERROR, this.getFacilityIdAttrValue(),
                                                                   this.getLangCodeAttrValue()));
                            this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(false);
                            this.setErrorStyleClass(this.getTroubleCode(), true);
                            this.selectTextOnUIComponent(this.getTroubleCode());
                            _logger.info("validateTroubleCode() End");
                            return false;
                        } else {
                            ADFUtils.setBoundAttributeValue(TROUBLE_CODE_ATTR, troubleCode);
                            this.getTroubleCodeIcon().setName(REQ_ICON);
                            this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
                            this.hideMessagesPanel();
                            this.removeErrorStyleToComponent(this.getTroubleCode());
                            this.refreshContentOfUIComponent(this.getInvItemTroublePanel());
                            this.setFocusContainerId();
                            this.selectTextOnUIComponent(this.getContainerId());
                        }
                    }
                }
            }
        }
        this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(true);
        _logger.info("validateTroubleCode() End");
        return true;
    }

    public void performOnChange(ClientEvent clientEvent) {
        String currentFocus = this.getHhInvItemTroubleSPageFlowBean().getIsFocusOn();
        String submittedValue = (String) clientEvent.getParameters().get("submittedValue");

        if ("ContainerId".equals(currentFocus)) {
            if (submittedValue != null && !submittedValue.isEmpty()) {
                this.setErrorStyleClass(this.getContainerId(), false);
                this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                this.setFocusContainerId();
            }
            if (this.getHhInvItemTroubleSPageFlowBean().getIsContainerIdValidated()) {
                this.setFocusTroubleCode();
                //Invalidate Trouble Code
                this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(false);
            }
        } else if ("TroubleCode".equals(currentFocus)) {
            if (submittedValue != null && !submittedValue.isEmpty()) {
                this.setErrorStyleClass(this.getTroubleCode(), false);
                this.onChangedTroubleCode(new ValueChangeEvent(this.getTroubleCode(), null, submittedValue));
            } else if (submittedValue == null || submittedValue.equals("")) {
                this.getTroubleCodeIcon().setName(null);
                this.removeErrorStyleToComponent(this.getTroubleCode());
                this.refreshContentOfUIComponent(this.getTroubleCode());
                this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
                this.hideMessagesPanel();
                this.setFocusContainerId();
                this.selectTextOnUIComponent(this.getContainerId());
                //Invalidate Container Id
                this.getHhInvItemTroubleSPageFlowBean().setIsContainerIdValidated(false);
            }
        }
    }

    public void performKey(ClientEvent clientEvent) { 
        String currentFocus = this.getHhInvItemTroubleSPageFlowBean().getIsFocusOn();
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF9Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("ContainerId".equals(currentFocus)) {
                    // OTM 3581
                    this.setErrorStyleClass(this.getContainerId(), false);
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                } else if ("TroubleCode".equals(currentFocus)) {
                    if (submittedValue != null && !submittedValue.isEmpty()) {
                        this.setErrorStyleClass(this.getTroubleCode(), false);
                        this.onChangedTroubleCode(new ValueChangeEvent(this.getTroubleCode(), null, submittedValue));
                    } else if (submittedValue == null || submittedValue.equals("")) {
                        this.getTroubleCodeIcon().setName(null);
                        this.removeErrorStyleToComponent(this.getTroubleCode());
                        this.refreshContentOfUIComponent(this.getTroubleCode());
                        this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
                        this.hideMessagesPanel();
                        this.setFocusContainerId();
                        this.selectTextOnUIComponent(this.getContainerId());
                        //Invalidate Container Id
                        this.getHhInvItemTroubleSPageFlowBean().setIsContainerIdValidated(false);
                    }
                } else if ("ContainerId".equals(currentFocus)) {
                    this.getContainerIdIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getContainerIdIcon());
                    this.refreshContentOfUIComponent(this.getContainerId());
                    this.addErrorStyleToComponent(this.getContainerId());
                    this.setFocusContainerId();
                    this.selectTextOnUIComponent(this.getContainerId());
                    this.showMessagesPanel(ERROR,
                                           this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                           this.getLangCodeAttrValue()));
                    this.clearFields(false);
                }
            }
            this.refreshContentOfUIComponent(this.getInvItemTroublePanel());
        }
    }

    private void trKeyIn(String key) {
      //  String currentFocus = this.getHhInvItemTroubleSPageFlowBean().getIsFocusOn();

        if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            this.clearFields(true);
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F4".equalsIgnoreCase(key)) {
            /* if (!this.getHhInvItemTroubleSPageFlowBean().getIsContainerIdValidated()) {
                this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null,
                                                               this.getContainerId().getValue()));
            } */
            // OTM 3581
            String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
            if (StringUtils.isEmpty(containerId)) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
                this.refreshContentOfUIComponent(this.getContainerId());
                this.addErrorStyleToComponent(this.getContainerId());
                this.setFocusContainerId();
                this.selectTextOnUIComponent(this.getContainerId());
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                this.clearFields(false);
                return;
            }
            else{
                this.validateContainerId(containerId);
            }
            if (!this.getHhInvItemTroubleSPageFlowBean().getIsTroubleCodeValidated()) {
                this.onChangedTroubleCode(new ValueChangeEvent(this.getTroubleCode(), null,
                                                               this.getTroubleCode().getValue()));
            }
            if (this.getHhInvItemTroubleSPageFlowBean().getIsContainerIdValidated()) {
                Boolean troubleCkFlg = callCTroubleCheck();
                if (troubleCkFlg.booleanValue()) {
                    callRecordItemTrouble();
                } else {
                    this.getTroubleCodeIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
                    this.refreshContentOfUIComponent(this.getTroubleCode());
                    this.addErrorStyleToComponent(this.getTroubleCode());
                    this.setFocusTroubleCode();
                    this.selectTextOnUIComponent(this.getTroubleCode());
                }
            }

        } else if ("F9".equalsIgnoreCase(key)) {
            //Navigate to page 2
            // OTM 3581
            String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
            if (StringUtils.isEmpty(containerId)) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
                this.refreshContentOfUIComponent(this.getContainerId());
                this.addErrorStyleToComponent(this.getContainerId());
                this.setFocusContainerId();
                this.selectTextOnUIComponent(this.getContainerId());
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                this.clearFields(false);
            }
            else{
                /* if (!this.getHhInvItemTroubleSPageFlowBean().getIsContainerIdValidated()) {
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null,
                                                                   this.getContainerId().getValue()));
                }
                if (this.getHhInvItemTroubleSPageFlowBean().getIsContainerIdValidated()) {
                    ADFUtils.invokeAction("Page2");
                } */
                if(this.validateContainerId(containerId)){
                    ADFUtils.invokeAction("Page2");
                }
            }
        }
    }

    private Boolean callCTroubleCheck() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCTroubleCheck");

        oper.getParamsMap().put("pFacilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));
        oper.getParamsMap().put("pTroubleCode", ADFUtils.getBoundAttributeValue(TROUBLE_CODE_ATTR));

        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                Boolean errorFlag = (Boolean) oper.getResult();
                if (errorFlag) {
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_TROUBLE", ERROR, this.getFacilityIdAttrValue(),
                                                           this.getLangCodeAttrValue()));
                    return Boolean.FALSE;
                }
            }
        }
        return Boolean.TRUE;
    }

    private void callRecordItemTrouble() {
        //Pre-Validation
        if (ADFUtils.getBoundAttributeValue(TROUBLE_CODE_ATTR) == null) {
            this.getTroubleCodeIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
            this.refreshContentOfUIComponent(this.getTroubleCode());
            this.addErrorStyleToComponent(this.getTroubleCode());
            this.setFocusTroubleCode();
            this.selectTextOnUIComponent(this.getTroubleCode());
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLangCodeAttrValue()));
        } else {
            //Call the DB Function
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("recordItemTrouble");
            oper.getParamsMap().put("pContainerId", ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR));
            oper.getParamsMap().put("pTroubleCode", ADFUtils.getBoundAttributeValue(TROUBLE_CODE_ATTR));
            oper.getParamsMap().put("pFacilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));

            oper.execute();

            //Handle Response Object
            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    //Output response
                    List<String> errorCodeList = (List<String>) oper.getResult();
                    String msg = errorCodeList.get(1);

                    if ("SUCCESS".equalsIgnoreCase(errorCodeList.get(0))) {
                        this.clearFields(true);
                        this.setFocusContainerId();
                        this.showMessagesPanel(MSGTYPE_M,
                                               this.getMessage("SUCCESS_OPER", MSGTYPE_M, this.getFacilityIdAttrValue(),
                                                               this.getLangCodeAttrValue()));
                    } else if ("FAILURE".equalsIgnoreCase(errorCodeList.get(0))) {
                        this.getTroubleCodeIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getTroubleCodeIcon());
                        this.refreshContentOfUIComponent(this.getTroubleCode());
                        this.addErrorStyleToComponent(this.getTroubleCode());
                        this.setFocusTroubleCode();
                        this.selectTextOnUIComponent(this.getTroubleCode());

                        this.showMessagesPanel(ERROR,
                                               this.getMessage(msg, ERROR, this.getFacilityIdAttrValue(),
                                                               this.getLangCodeAttrValue()));
                    } else {
                        this.showMessagesPanel(ERROR, errorCodeList.get(0));
                    }
                }
            }
        }

    }

    private void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void clearFields(boolean clearAll) {
        _logger.info("clearFields() Start");

        this.clearCurrentRow();
        if (clearAll) {
            this.getContainerId().resetValue();
            this.refreshContentOfUIComponent(this.getContainerId());
        }
        this.getTroubleCode().resetValue();
        this.getTroubleCode1().resetValue();
        this.getTroubleCode2().resetValue();
        this.getTroubleCode3().resetValue();
        this.getTroubleCode4().resetValue();
        this.getTroubleCode5().resetValue();
        this.getTroubleCode6().resetValue();
        this.refreshContentOfUIComponent(this.getTroubleCode());
        this.refreshContentOfUIComponent(this.getTroubleCode1());
        this.refreshContentOfUIComponent(this.getTroubleCode2());
        this.refreshContentOfUIComponent(this.getTroubleCode3());
        this.refreshContentOfUIComponent(this.getTroubleCode4());
        this.refreshContentOfUIComponent(this.getTroubleCode5());
        this.refreshContentOfUIComponent(this.getTroubleCode6());

        //Invalidate containerId and TroubleCode
        this.getHhInvItemTroubleSPageFlowBean().setIsContainerIdValidated(false);
        this.getHhInvItemTroubleSPageFlowBean().setIsTroubleCodeValidated(false);
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow() {
        _logger.info("clearCurrentRow() Start");
        HhInvItemTroubleSContainerViewRowImpl hhRowImpl = this.getHhInvItemTroubleSContainerViewRow();
        hhRowImpl.setContainerId1(null);
        hhRowImpl.setTroubleCode(null);
        hhRowImpl.setTroubleCode1(null);
        hhRowImpl.setTroubleCode2(null);
        hhRowImpl.setTroubleCode3(null);
        hhRowImpl.setTroubleCode4(null);
        hhRowImpl.setTroubleCode5(null);
        hhRowImpl.setTroubleCode6(null);
        _logger.info("ClearCurrentRow() End");
    }

    private HhInvItemTroubleSContainerViewRowImpl getHhInvItemTroubleSContainerViewRow() {
        return (HhInvItemTroubleSContainerViewRowImpl) ADFUtils.findIterator("HhInvItemTroubleSContainerViewIterator").getCurrentRow();
    }

    private HhInvItemTroubleSBean getHhInvItemTroubleSPageFlowBean() {
        return ((HhInvItemTroubleSBean) this.getPageFlowBean(HH_INV_ITEM_TROUBLE_FLOW_BEAN));
    }

    private void setFocusContainerId() {
        this.getHhInvItemTroubleSPageFlowBean().setIsFocusOn("ContainerId");
        this.setFocusOnUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(getTroubleCode());
    }

    private void setFocusTroubleCode() {
        this.getHhInvItemTroubleSPageFlowBean().setIsFocusOn("TroubleCode");
        this.setFocusOnUIComponent(this.getTroubleCode());
        this.refreshContentOfUIComponent(getContainerId());
    }


    public void setInvItemTroublePanel(RichPanelFormLayout InvItemTroublePanel) {
        this.InvItemTroublePanel = InvItemTroublePanel;
    }

    public RichPanelFormLayout getInvItemTroublePanel() {
        return InvItemTroublePanel;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setTroubleCode(RichInputText troubleCode) {
        this.troubleCode = troubleCode;
    }

    public RichInputText getTroubleCode() {
        return troubleCode;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF9Link(RichLink f9Link) {
        this.f9Link = f9Link;
    }

    public RichLink getF9Link() {
        return f9Link;
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_inv_item_trouble_s_CONTAINER_F4,
                                           RoleBasedAccessConstants.FORM_NAME_hh_inv_item_trouble_s)) {
            this.trKeyIn("F4");
        }else{
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    public void f9ActionListener(ActionEvent actionEvent) {
        if (callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_inv_item_trouble_s_CONTAINER_F9,
                                           RoleBasedAccessConstants.FORM_NAME_hh_inv_item_trouble_s)) {
            this.trKeyIn("F9");
        }else{
            showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setTroubleCodeIcon(RichIcon troubleCodeIcon) {
        this.troubleCodeIcon = troubleCodeIcon;
    }

    public RichIcon getTroubleCodeIcon() {
        return troubleCodeIcon;
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setTroubleCode1(RichInputText troubleCode1) {
        this.troubleCode1 = troubleCode1;
    }

    public RichInputText getTroubleCode1() {
        return troubleCode1;
    }

    public void setTroubleCode2(RichInputText troubleCode2) {
        this.troubleCode2 = troubleCode2;
    }

    public RichInputText getTroubleCode2() {
        return troubleCode2;
    }

    public void setTroubleCode3(RichInputText troubleCode3) {
        this.troubleCode3 = troubleCode3;
    }

    public RichInputText getTroubleCode3() {
        return troubleCode3;
    }

    public void setTroubleCode4(RichInputText troubleCode4) {
        this.troubleCode4 = troubleCode4;
    }

    public RichInputText getTroubleCode4() {
        return troubleCode4;
    }

    public void setTroubleCode5(RichInputText troubleCode5) {
        this.troubleCode5 = troubleCode5;
    }

    public RichInputText getTroubleCode5() {
        return troubleCode5;
    }

    public void setTroubleCode6(RichInputText troubleCode6) {
        this.troubleCode6 = troubleCode6;
    }

    public RichInputText getTroubleCode6() {
        return troubleCode6;
    }

    public void setHiddenContainerLink(RichLink hiddenContainerLink) {
        this.hiddenContainerLink = hiddenContainerLink;
    }

    public RichLink getHiddenContainerLink() {
        return hiddenContainerLink;
    }
}
