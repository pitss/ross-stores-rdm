package com.ross.rdm.inventorymgmt.hhrossbuildcontainers.view.bean;

import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;
import java.sql.Types;
import javax.faces.event.ValueChangeEvent;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.jbo.ApplicationModule;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Method.jspx
// ---    
// ---------------------------------------------------------------------
public class MethodBacking extends RDMInventoryManagementBackingBean {
  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon errorWarnInfoIcon;
  private RichOutputText errorWarnInfoMessage;

  public MethodBacking() {

  }

  public void onChangedBuildMethod(ValueChangeEvent vce) {
    //Forms  Module : hh_ross_build_container_s
    //Object        : BOILER_METHOD.BUILD_METHOD.KEY-NEXT-ITEM
    //Source Code   : 
    //
    //:BOILER_METHOD.CURRENT_BUILD_METHOD := :BOILER_METHOD.BUILD_METHOD;


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : hh_ross_build_container_s
    //Object      : BOILER_METHOD.BUILD_METHOD.KEY-NEXT-ITEM
    //
    //ApplicationModule appModule = getAppModule();
    //
    //appModule.findViewObject("HhRossBuildContainerSBoilerMethodView").getCurrentRow().setAttribute("CurrentBuildMethod",:boiler_method.build_method);
    // --------------------------------------------------------------------------------
  }

  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()  {
    return allMessagesPanel;
  }
  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()  {
    return errorWarnInfoIcon;
  }
  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()  {
    return errorWarnInfoMessage;
  }
}
