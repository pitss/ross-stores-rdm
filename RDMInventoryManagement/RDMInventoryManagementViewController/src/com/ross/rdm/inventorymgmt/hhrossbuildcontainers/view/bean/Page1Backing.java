package com.ross.rdm.inventorymgmt.hhrossbuildcontainers.view.bean;

import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;
import java.sql.Types;
import javax.faces.event.ValueChangeEvent;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.jbo.ApplicationModule;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page1.jspx
// ---    
// ---------------------------------------------------------------------
public class Page1Backing extends RDMInventoryManagementBackingBean {
  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon errorWarnInfoIcon;
  private RichOutputText errorWarnInfoMessage;

  public Page1Backing() {

  }

  public void onChangedGenericContainerId(ValueChangeEvent vce) {
    //Forms  Module : hh_ross_build_container_s
    //Object        : CONTAINER.GENERIC_CONTAINER_ID.KEY-NEXT-ITEM
    //Source Code   : 
    //
    //DECLARE 
    //	 V_SEQ_BUILT_INS     NUMBER  := 0;
    //	 V_DEST_ID 					 CONTAINER.Dest_ID%TYPE;      
    //	 V_CONTAINER_STATUS  CONTAINER.Container_Status%TYPE; 
    //	 V_LOCATION_ID  		 CONTAINER.Location_ID%TYPE;
    //	 V_GO_BLOCK          VARCHAR2(1) := 'N';
    //BEGIN
    //
    //  PKG_HOTEL_INV.V_GENERIC_CONTAINER_ID2_1(P_DISPLAY.V_DISPLAY_STRING ,:CONTAINER.CONTAINER_ID ,:CONTAINER.GENERIC_CONTAINER_ID ,:CONTAINER.ITEM_ID ,:CONTAINER.LOCATION_ID ,:CONTAINER.MARK_STATUS ,:CONTAINER.TICKET_REQUEST_STATUS ,:CONTAINER.UPS_CODE ,:WORK.FACILITY_ID ,:WORK_LOCAL.BUILD_MULTI_SKU_FLAG ,:WORK_LOCAL.CHECK_DIGIT ,:WORK_LOCAL.CONT_DEST_ID ,:WORK_LOCAL.CONT_STATUS ,:WORK_LOCAL.NEW_CONTAINER ,:WORK_LOCAL.NEW_CONTAINER_FLAG, V_DEST_ID, V_CONTAINER_STATUS, V_LOCATION_ID, V_SEQ_BUILT_INS  );
    //  IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    //   
    //  IF V_SEQ_BUILT_INS = 1 THEN 
    //  		-- Enable Location_ID field
    //      SET_ITEM_PROPERTY('CONTAINER.Location_ID',NAVIGABLE,PROPERTY_TRUE);	
    //  ELSIF V_SEQ_BUILT_INS = 2 THEN 
    //  	IF V_DEST_ID IS NULL THEN
    //      IF  Call_Msg_Window('ASSIGN_DC_DEST', 'C') = 'Y' THEN
    //      	V_DEST_ID := TO_NUMBER(g_scp(:WORK.FACILITY_ID , 'DC_dest_ID'));
    //      ELSE
    //      	CALL_MSG_WINDOW_P( 'INV_DEST_ID', 'W');
    //        RAISE FORM_TRIGGER_FAILURE;
    //      END IF;
    //    END IF;
    //    
    //    /* Start CO60071 MShah */
    //    BEGIN
    //    		IF PKG_HOTEL_INV.F_GET_MARK_STATUS(P_DISPLAY.V_DISPLAY_STRING ,:WORK.FACILITY_ID  ,:CONTAINER.GENERIC_CONTAINER_ID) = 'Y' THEN
    //	    		:CONTAINER.GENERIC_CONTAINER_ID := NULL;
    // 	    		CALL_MSG_WINDOW_P( 'MARK_MISMATCH', 'E');
    //	    		RAISE FORM_TRIGGER_FAILURE;
    //    		END IF;
    //    EXCEPTION
    //    		WHEN OTHERS THEN 
    //    				IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    //    END;		
    //    /* End CO60071 MShah */        
    //         
    //    :CONTAINER.LOCATION_ID         := V_LOCATION_ID;
    //    :WORK_LOCAL.CONT_STATUS  			 := V_CONTAINER_STATUS;
    //    :WORK_LOCAL.CONT_DEST_ID 			 := V_DEST_ID;
    //    :WORK_LOCAL.NEW_CONTAINER_FLAG := 'N';
    //
    //   /*START MJJ CO58028 - 6*/
    //		--IF (v_location = 'Y') THEN 
    //    BEGIN
    //	    IF (PKG_HOTEL_INV.V_LOCATION(P_DISPLAY.V_DISPLAY_STRING ,:WORK.FACILITY_ID, :CONTAINER.LOCATION_ID ) = 'Y') THEN 
    //	       /*END MJJ CO58028 - 6*/
    //	    	 -- Disable Location_ID field
    //	       SET_ITEM_PROPERTY('CONTAINER.Location_ID',NAVIGABLE,PROPERTY_FALSE);
    //	    ELSE
    //	    	 IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    //	    	 :CONTAINER.LOCATION_ID := NULL;
    //	       RAISE FORM_TRIGGER_FAILURE;
    //	    END IF;    
    //		EXCEPTION
    //   		WHEN FORM_TRIGGER_FAILURE THEN
    //      	RAISE;
    //   		WHEN OTHERS THEN
    //      	Log_Error_Roll(SQLCODE, 'v_location ', SQLERRM);
    //      	RAISE FORM_TRIGGER_FAILURE;
    //		END;		
    //        
    //    --PART 2 OF V_GENERIC_CONTAINER_ID2
    //    PKG_HOTEL_INV.V_GENERIC_CONTAINER_ID2_2(P_DISPLAY.V_DISPLAY_STRING ,:CONTAINER.CONTAINER_ID ,:CONTAINER.GENERIC_CONTAINER_ID ,:CONTAINER.ITEM_ID ,:CONTAINER.UPS_CODE ,:WORK.FACILITY_ID  );
    //    IF  P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    //    
    //  	
    //  END IF; 
    //  NEXT_ITEM; 
    //  
    //	PKG_HOTEL_INV.T_KEY_NEXT_ITEM1(:GLOBAL.MLD_ENABLED ,:CONTAINER.GENERIC_CONTAINER_ID ,:METHOD.BUILD_METHOD ,:METHOD.CURRENT_BUILD_METHOD ,:WORK.FACILITY_ID ,:WORK_LOCAL.NEW_CONTAINER, V_GO_BLOCK  );
    //	IF V_GO_BLOCK = 'Y' THEN 
    //			go_block('METHOD');		
    //	END IF;	
    //
    //END;	


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : hh_ross_build_container_s
    //Object      : CONTAINER.GENERIC_CONTAINER_ID.KEY-NEXT-ITEM
    //
    //ApplicationModule appModule = getAppModule();
    //
    // 
    //  oracle.jbo.domain.Number V_SEQ_BUILT_INS= 0;
    //  V_DEST_ID       CONTAINER.Dest_ID%TYPE;      
    //  V_CONTAINER_STATUS  CONTAINER.Container_Status%TYPE; 
    //  V_LOCATION_ID     CONTAINER.Location_ID%TYPE;
    //  String V_GO_BLOCK=  "N";
    //{
    //
    //  
    //    appModule.callStoredProcedure("pkg_hotel_inv.v_generic_container_id2_1",
    //                                                             Types., 
    //p_display.v_display_string,
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("ContainerId"),
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("GenericContainerId"),
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("ItemId"),
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("LocationId"),
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("MarkStatus"),
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("TicketRequestStatus"),
    //                              appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("UpsCode"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("BuildMultiSkuFlag"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("CheckDigit"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("ContDestId"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("ContStatus"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("NewContainer"),
    //                              appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("NewContainerFlag"),
    //                              v_dest_id,
    //                              v_container_status,
    //                              v_location_id,
    //                              v_seq_built_ins);
    //  if( p_display.handle_mess){ throw new JboException(); /*FORM_TRIGGER_FAILURE*/ }
    //   
    //  if( V_SEQ_BUILT_INS == 1){ 
    //    // Enable Location_ID field
    //      set_item_property("locationIdContainer",
    //                                                          navigable,
    //                                                          property_true); 
    //  } else if(  V_SEQ_BUILT_INS == 2){ 
    //   if( V_DEST_ID == null){
    //      if(  call_msg_window("ASSIGN_DC_DEST",
    //                  "C").equals( "Y")){
    //       V_DEST_ID= 
    // ( String ) appModule.callStoredFunction( "g_scp",
    //               Types.CHAR, 
    //appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //       "DC_dest_ID") 
    // ( String ) appModule.callStoredFunction( "g_scp",
    //               Types.CHAR, 
    //appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //       "DC_dest_ID");
    //      }else{
    //       call_msg_window_p("INV_DEST_ID",
    //                          "W");
    //        throw new JboException(); /*FORM_TRIGGER_FAILURE*/
    //      }
    //    }
    //    
    //    /* Start CO60071 MShah */
    //    try{
    //      if( 
    // ( String ) appModule.callStoredFunction( "pkg_hotel_inv.f_get_mark_status",
    //                                       Types.VARCHAR, 
    //p_display.v_display_string,
    //                   appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //                   appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("GenericContainerId")).equals( "Y")){
    //       appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().setAttribute("GenericContainerId",null);
    //        call_msg_window_p("MARK_MISMATCH",
    //                           "E");
    //       throw new JboException(); /*FORM_TRIGGER_FAILURE*/
    //      }
    //}catch(Exception e){
    //      // WHEN OTHERS THEN 
    //        if( p_display.handle_mess){ throw new JboException(); /*FORM_TRIGGER_FAILURE*/ }
    //
    //    }  
    //    /* End CO60071 MShah */        
    //         
    //    appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().setAttribute("LocationId",v_location_id));
    //    appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().setAttribute("ContStatus",v_container_status));
    //    appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().setAttribute("ContDestId",v_dest_id));
    //    appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().setAttribute("NewContainerFlag", "N";
    //
    //   /*START MJJ CO58028 - 6*/
    //  //IF (v_location = 'Y') THEN 
    //    try{
    //     if(
    //( String ) appModule.callStoredFunction( "pkg_hotel_inv.v_location",
    //                       Types.VARCHAR, 
    //p_display.v_display_string,
    //           appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //           appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("LocationId")).equals( "Y"){ 
    //        /*END MJJ CO58028 - 6*/
    //       // Disable Location_ID field
    //        set_item_property("locationIdContainer",
    //                                                                                                navigable,
    //                                                                                                property_false);
    //     }else{
    //       if( p_display.handle_mess){ throw new JboException(); /*FORM_TRIGGER_FAILURE*/ }
    //       appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().setAttribute("LocationId",null);
    //        throw new JboException(); /*FORM_TRIGGER_FAILURE*/
    //     }
    //}catch(Exception e){
    //     // WHEN FORM_TRIGGER_FAILURE THEN
    //       throw new JboException();
    //     // WHEN OTHERS THEN
    //       log_error_roll(SQLCODE,
    //                       "v_location ",
    //                       SQLERRM);
    //       throw new JboException(); /*FORM_TRIGGER_FAILURE*/
    //
    //  }  
    //        
    //    //PART 2 OF V_GENERIC_CONTAINER_ID2
    //    
    //                                                        appModule.callStoredProcedure("pkg_hotel_inv.v_generic_container_id2_2",
    //                                                                                                                                                                     Types., 
    //p_display.v_display_string,
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("ContainerId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("GenericContainerId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("ItemId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("UpsCode"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"));
    //    if(  p_display.handle_mess){ throw new JboException(); /*FORM_TRIGGER_FAILURE*/ }
    //    
    //   
    //  } 
    //  next_item; 
    //  
    // 
    //      appModule.callStoredProcedure("pkg_hotel_inv.t_key_next_item1",
    //                                               Types., 
    //:global.mld_enabled,
    //                       appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("GenericContainerId"),
    //                       appModule.findViewObject("HhRossBuildContainerSMethodView").getCurrentRow().getAttribute("BuildMethod"),
    //                       appModule.findViewObject("HhRossBuildContainerSMethodView").getCurrentRow().getAttribute("CurrentBuildMethod"),
    //                       appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //                       appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("NewContainer"),
    //                       v_go_block);
    // if( V_GO_BLOCK.equals( "Y")){ 
    //   go_block(HhRossBuildContainerSMethodView);  
    // }
    // 
    //
    //}
    // --------------------------------------------------------------------------------
  }

  public void onChangedLocationId(ValueChangeEvent vce) {
    //Forms  Module : hh_ross_build_container_s
    //Object        : CONTAINER.LOCATION_ID.KEY-NEXT-ITEM
    //Source Code   : 
    //
    ///*START MJJ CO58028 -1*/
    //--IF v_location = 'Y' THEN
    //	
    //BEGIN	
    //		IF PKG_HOTEL_INV.V_LOCATION(P_DISPLAY.V_DISPLAY_STRING ,:WORK.FACILITY_ID  ,:CONTAINER.location_id) = 'Y' THEN
    //		/*END MJJ CO58028 -1*/
    //    		NEXT_ITEM;
    //		ELSE 
    //			IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    //		END IF;
    //EXCEPTION
    //   WHEN FORM_TRIGGER_FAILURE THEN
    //      RAISE;
    //   WHEN OTHERS THEN
    //      Log_Error_Roll(SQLCODE, 'v_location ', SQLERRM);
    //      RAISE FORM_TRIGGER_FAILURE;
    //END;		


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : hh_ross_build_container_s
    //Object      : CONTAINER.LOCATION_ID.KEY-NEXT-ITEM
    //
    //ApplicationModule appModule = getAppModule();
    //
    ///*START MJJ CO58028 -1*/
    ////IF v_location = 'Y' THEN
    // 
    //try{ 
    //  if( 
    // ( String ) appModule.callStoredFunction( "pkg_hotel_inv.v_location",
    //                         Types.VARCHAR, 
    //p_display.v_display_string,
    //            appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //            appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("LocationId")).equals( "Y")){
    //  /*END MJJ CO58028 -1*/
    //      next_item;
    //  }else{ 
    //   if( p_display.handle_mess){ throw new JboException(); /*FORM_TRIGGER_FAILURE*/ }
    //  }
    //}catch(Exception e){
    //   // WHEN FORM_TRIGGER_FAILURE THEN
    //      throw new JboException();
    //   // WHEN OTHERS THEN
    //      log_error_roll(SQLCODE,
    //                      "v_location ",
    //                      SQLERRM);
    //      throw new JboException(); /*FORM_TRIGGER_FAILURE*/
    //
    //}
    // --------------------------------------------------------------------------------
  }

  public void onChangedContainerId(ValueChangeEvent vce) {
    //Forms  Module : hh_ross_build_container_s
    //Object        : CONTAINER.CONTAINER_ID.KEY-NEXT-ITEM
    //Source Code   : 
    //
    ///*CONTAINER.CONTAINER_ID   KEY-NEXT-ITEM    */
    //DECLARE
    //		V_GO_ITEM     VARCHAR2(1)  := 'N';
    //BEGIN
    //	--USER CONFIRMATION REQUIRED - HANDLE IT IN ADF - PITSS
    //	PKG_HOTEL_INV.ADD_LABELED_CONTAINER2(P_DISPLAY.V_DISPLAY_STRING, :GLOBAL.FACILITY_TYPE ,:GLOBAL.MLD_ENABLED ,:CONTAINER.CONTAINER_ID ,:CONTAINER.GENERIC_CONTAINER_ID ,:CONTAINER.ITEM_ID ,:CONTAINER.LOCATION_ID ,:CONTAINER.UPS_CODE ,:METHOD.BUILD_METHOD ,:WORK.FACILITY_ID ,:WORK.USER ,:WORK_LOCAL.BUILD_MULTI_SKU_FLAG ,:WORK_LOCAL.CONSOLIDATE_PEND_WIP ,:WORK_LOCAL.CONT_DEST_ID ,:WORK_LOCAL.CONT_STATUS ,:WORK_LOCAL.NEW_CONTAINER_FLAG, V_GO_ITEM );
    //	IF V_GO_ITEM = 'Y' THEN 
    //  	 CALL_MSG_WINDOW_P( 'INV_BUILD_CONT','E');
    //     p_go_item('METHOD.BUILD_METHOD');
    //     CLEAR_BLOCK(NO_VALIDATE);
    //     p_go_item('Container.Generic_Container_ID');
    //     CLEAR_BLOCK(NO_VALIDATE);
    //     RAISE FORM_TRIGGER_FAILURE;  
    //  END IF;
    //EXCEPTION 
    //  WHEN OTHERS THEN   	
    //    LOG_ERROR( SQLCODE,'add_labeled_container2 ', SQLERRM);
    //    Log_Error_Roll(SQLCODE, 'add_labeled_container2 ', SQLERRM);
    //    RAISE FORM_TRIGGER_FAILURE;
    //END;
    //    
    //    
    //


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : hh_ross_build_container_s
    //Object      : CONTAINER.CONTAINER_ID.KEY-NEXT-ITEM
    //
    //ApplicationModule appModule = getAppModule();
    //
    ///*CONTAINER.CONTAINER_ID   KEY-NEXT-ITEM    */
    //
    //  String V_GO_ITEM=  "N";
    //try{
    // //USER CONFIRMATION REQUIRED - HANDLE IT IN ADF - PITSS
    // 
    //                                                           appModule.callStoredProcedure("pkg_hotel_inv.add_labeled_container2",
    //                                                                                                                                                                     Types., 
    //p_display.v_display_string,
    //                                                                                  :global.facility_type,
    //                                                                                  :global.mld_enabled,
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("ContainerId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("GenericContainerId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("ItemId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("LocationId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("UpsCode"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSMethodView").getCurrentRow().getAttribute("BuildMethod"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("FacilityId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkView").getCurrentRow().getAttribute("User"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("BuildMultiSkuFlag"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("ConsolidatePendWip"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("ContDestId"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("ContStatus"),
    //                                                                                  appModule.findViewObject("HhRossBuildContainerSWorkLocalView").getCurrentRow().getAttribute("NewContainerFlag"),
    //                                                                                  v_go_item);
    // if( V_GO_ITEM.equals( "Y")){ 
    //    call_msg_window_p("INV_BUILD_CONT",
    //                        "E");
    //     p_go_item(appModule.findViewObject("HhRossBuildContainerSMethodView").getCurrentRow().getAttribute("BuildMethod"));
    //     clear_block(no_validate);
    //     p_go_item(appModule.findViewObject("HhRossBuildContainerSContainerView").getCurrentRow().getAttribute("GenericContainerId"));
    //     clear_block(no_validate);
    //     throw new JboException(); /*FORM_TRIGGER_FAILURE*/  
    //  }
    //}catch(Exception e){ 
    //  // WHEN OTHERS THEN    
    //    log_error(SQLCODE,
    //                   "add_labeled_container2 ",
    //                   SQLERRM);
    //    log_error_roll(SQLCODE,
    //                    "add_labeled_container2 ",
    //                    SQLERRM);
    //    throw new JboException(); /*FORM_TRIGGER_FAILURE*/
    //
    //}
    // --------------------------------------------------------------------------------
  }

  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()  {
    return allMessagesPanel;
  }
  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()  {
    return errorWarnInfoIcon;
  }
  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()  {
    return errorWarnInfoMessage;
  }
}
