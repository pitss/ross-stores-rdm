package com.ross.rdm.inventorymgmt.hhinventoryverifications.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.hhinventoryverifications.model.views.HhInventoryVerificationSInvVerificationViewRowImpl;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMInventoryManagementBackingBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhInventoryVerificationSBean.class);
    private final static String PAGE_FLOW_BEAN = "HhInventoryVerificationSBean";
    private final static String LOCATION_ID_ATTR = "LocationId";
    private final static String FLUE_SPC_FLAG_ATTR = "FlueSpcFlag";
    private final static String MLP_QTY_ATTR = "MlpQty";
    private final static String CID_QTY_ATTR = "CidQty";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private RichPanelFormLayout inventoryVerificationPanel;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichInputText locationId;
    private RichInputText flueSpcFlag;
    private RichInputText mlpQty;
    private RichInputText cidQty;
    private RichInputText containerId;
    private RichIcon locationIdIcon;
    private RichIcon flueSpcFlagIcon;
    private RichIcon mlpQtyIcon;
    private RichIcon cidQtyIcon;
    private RichIcon containerIdIcon;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f5Link;
    private RichPopup confirmHighMlpPopup;
    private RichOutputText dialogConfirmHighMlpOutputtext;
    private RichDialog confirmHighMlpDialog;

    public Page1Backing() {

    }


    /******************************************************************************************
     *                                  Validators
     ******************************************************************************************/


    public void onChangedLocationId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            _logger.info("onChangedLocationId() Start");
            this.updateModel(vce);
            if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
                String locId = ((String) vce.getNewValue());
                this.callVLoc(locId);
            } else {
                this.showErrorPanel("PARTIAL_ENTRY", ERROR);
                this.setErrorUi(this.getLocationIdIcon(), this.getLocationId());
                this.setFocusLocationId();
                this.getHhInventoryVerificationSBeanPageFlowBean().setIsLocationIdValidated(false);
            }
            _logger.info("onChangedLocationId() End");
        }
    }

    private boolean callVLoc(String locId) {
        _logger.info("callVLoc() Start");
        if (locId != null)
            locId = locId.toUpperCase();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPStartLoc");
        Map map = oper.getParamsMap();
        map.put("locationId", locId);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(codeList.get(0))) {
                    String msg = codeList.get(1);
                    this.showErrorPanel(msg);
                    this.setErrorUi(this.getLocationIdIcon(), this.getLocationId());
                    this.setFocusLocationId();
                    this.getHhInventoryVerificationSBeanPageFlowBean().setIsLocationIdValidated(false);
                    _logger.info("callVLoc() End");
                    return false;
                } else {
                    String goItem = codeList.get(0);
                    ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, locId);
                    this.getLocationIdIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getLocationIdIcon());
                    this.hideMessagesPanel();

                    this.removeErrorStyleToComponent(this.getLocationId());
                    this.refreshContentOfUIComponent(this.getInventoryVerificationPanel());

                    if (YES.equalsIgnoreCase(goItem)) {
                        this.setFocusFlueSpcFlag();
                    } else {
                        this.setFocusMlpQty();
                    }
                }
            }
        }
        _logger.info("callVLoc() End");
        return true;
    }

    public void onChangedFlueSpcFlag(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            _logger.info("onChangedFlueSpcFlag() Start");
            this.updateModel(vce);
            if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
                String flueSpcFlagIn = ((String) vce.getNewValue());
                this.callVFlueSpcFlag(flueSpcFlagIn);
            }
            _logger.info("onChangedFlueSpcFlag() End");
        }
    }

    private boolean callVFlueSpcFlag(String flueSpcFlagIn) {
        _logger.info("callVFlueSpcFlag() Start");
        String reqd = (String) ADFUtils.getBoundAttributeValue("FlueSpaceReqd");
        if (YES.equalsIgnoreCase(reqd)) {
            if (YES.equalsIgnoreCase(flueSpcFlagIn) || NO.equalsIgnoreCase(flueSpcFlagIn)) {
                this.hideMessagesPanel();
                ADFUtils.setBoundAttributeValue(FLUE_SPC_FLAG_ATTR, flueSpcFlagIn);
                this.getFlueSpcFlagIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getFlueSpcFlagIcon());
                this.removeErrorStyleToComponent(this.getFlueSpcFlag());
                this.refreshContentOfUIComponent(this.getInventoryVerificationPanel());
                this.getHhInventoryVerificationSBeanPageFlowBean().setIsFlueSpcFlagValidated(true);
                this.setFocusMlpQty();
            } else {
                this.showErrorPanel("INV_FLAG", ERROR);
                this.setErrorUi(this.getFlueSpcFlagIcon(), this.getFlueSpcFlag());
                this.setFocusFlueSpcFlag();
                this.getHhInventoryVerificationSBeanPageFlowBean().setIsFlueSpcFlagValidated(false);
                _logger.info("callVFlueSpcFlag() End");
                return false;
            }
        } else {
            this.setFocusMlpQty();
        }
        _logger.info("callVFlueSpcFlag() End");
        return true;
    }

    public void onChangedMlpQty(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            _logger.info("onChangedMlpQty() Start");
            this.updateModel(vce);
            if (null != vce.getNewValue()) {
                String temp = (String) vce.getNewValue();
                BigDecimal temp2 = null;
                try {
                    temp2 = new BigDecimal(temp);
                    this.callVMlpQty(temp2);
                } catch (Exception e) {
                    this.showErrorPanel("MUST_BE_NUMERIC", ERROR);
                    this.setErrorUi(this.getMlpQtyIcon(), this.getMlpQty());
                    this.setFocusMlpQty();
                    this.getHhInventoryVerificationSBeanPageFlowBean().setIsMlpQtyValidated(false);
                }
            }
            _logger.info("onChangedMlpQty() End");
        }
    }

    private boolean callVMlpQty(BigDecimal mlpQtyIn) {
        _logger.info("callVMlpQty() Start");
        String facilityId = getFacilityIdAttrValue();

        if (BigDecimal.valueOf(0).compareTo(mlpQtyIn) > 0) {
            this.showErrorPanel("INV_QTY", ERROR);
            this.setErrorUi(this.getMlpQtyIcon(), this.getMlpQty());
            this.setFocusMlpQty();
            this.getHhInventoryVerificationSBeanPageFlowBean().setIsMlpQtyValidated(false);
            return false;
        } else {
            this.getHhInventoryVerificationSBeanPageFlowBean().setIsMlpQtyValidated(true);
            this.getMlpQtyIcon().setName(REQ_ICON);
            this.refreshContentOfUIComponent(this.getMlpQtyIcon());
            this.hideMessagesPanel();
            this.removeErrorStyleToComponent(this.getMlpQty());
            this.refreshContentOfUIComponent(this.getInventoryVerificationPanel());
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("gScp");
            Map map = oper.getParamsMap();
            map.put("facility", facilityId);
            map.put("name", "confirm_verification");
            oper.execute();
            BigDecimal verificationQty = new BigDecimal((String) oper.getResult());
            if (mlpQtyIn.compareTo(verificationQty) > 0) {
                this.getDialogConfirmHighMlpOutputtext().setValue(this.getMessage("HIGH_MLP_QTY", "C",
                                                                                  this.getFacilityIdAttrValue(),
                                                                                  this.getLangCodeAttrValue()));
                this.disableFields();
                this.getConfirmHighMlpPopup().show(new RichPopup.PopupHints());
            } else {
                this.getMlpQty().setDisabled(true);
                this.getCidQty().setDisabled(false);
                this.refreshContentOfUIComponent(this.getInventoryVerificationPanel());
                this.setFocusCidQty();
            }
        }
        return true;
    }

    public void onChangedCidQty(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            _logger.info("onChangedCidQty() Start");
            if (null != vce.getNewValue()) {
                this.updateModel(vce);
                String temp = (String) vce.getNewValue();
                BigDecimal temp2 = null;
                try {
                    temp2 = new BigDecimal(temp);
                    this.callVCidQty(temp2);
                } catch (Exception e) {
                    this.showErrorPanel("MUST_BE_NUMERIC", ERROR);
                    this.setErrorUi(this.getCidQtyIcon(), this.getCidQty());
                    this.setFocusCidQty();
                    this.getHhInventoryVerificationSBeanPageFlowBean().setIsCidQtyValidated(false);
                }
            }
            _logger.info("onChangedCidQty() End");
        }
    }

    private boolean callVCidQty(BigDecimal cidQtyIn) {
        _logger.info("callVCidQty() Start");
        String facilityId = getFacilityIdAttrValue();

        if (BigDecimal.valueOf(0).compareTo(cidQtyIn) > 0) {
            this.showErrorPanel("INV_QTY", ERROR);
            this.setErrorUi(this.getCidQtyIcon(), this.getCidQty());
            this.setFocusCidQty();
            this.getHhInventoryVerificationSBeanPageFlowBean().setIsCidQtyValidated(false);
            return false;
        } else {

            this.getHhInventoryVerificationSBeanPageFlowBean().setIsCidQtyValidated(true);
            this.getCidQtyIcon().setName(REQ_ICON);
            this.refreshContentOfUIComponent(this.getCidQtyIcon());
            this.hideMessagesPanel();
            this.removeErrorStyleToComponent(this.getCidQty());
            this.refreshContentOfUIComponent(this.getInventoryVerificationPanel());

            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("gScp");
            Map map = oper.getParamsMap();
            map.put("facility", facilityId);
            map.put("name", "confirm_verification");
            oper.execute();
            BigDecimal verificationQty = new BigDecimal((String) oper.getResult());

            if (cidQtyIn.compareTo(verificationQty) > 0) {
                this.getDialogConfirmHighMlpOutputtext().setValue(this.getMessage("HIGH_MLP_QTY", "C",
                                                                                  this.getFacilityIdAttrValue(),
                                                                                  this.getLangCodeAttrValue()));
                this.disableFields();
                this.getConfirmHighMlpPopup().setLauncherVar("Visible");
                this.getConfirmHighMlpPopup().show(new RichPopup.PopupHints());
            }
            if (cidQtyIn.compareTo(BigDecimal.valueOf(0)) == 0) {
                BigDecimal mlpQty = new BigDecimal((String) ADFUtils.getBoundAttributeValue("MlpQty"));
                if (mlpQty != null && BigDecimal.valueOf(0).compareTo(mlpQty) != 0) { //Not equal to 0
                    this.showErrorPanel("INV_MLP_QTY", ERROR);
                    OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callPClearCounts");
                    oper2.execute();
                    List<Object> codeList = (List<Object>) oper2.getResult();
                    if (codeList != null && codeList.size() == 3) {
                        this.showErrorPanel((String) codeList.get(1));
                        this.clearQtyFields();
                        this.setErrorUi(this.getMlpQtyIcon(), this.getMlpQty());
                        this.setFocusMlpQty();
                        return false;
                    }
                    String goItem = null;
                    if (codeList != null) goItem = (String) codeList.get(0);
                    if (null != goItem) {
                        this.getCidQty().setDisabled(true);
                        this.clearQtyFields();
                        if ((YES).equalsIgnoreCase(goItem)) {
                            this.getFlueSpcFlag().setDisabled(false);
                            this.refreshContentOfUIComponent(this.getFlueSpcFlag());
                            this.setFocusFlueSpcFlag();
                            this.setErrorUi(this.getFlueSpcFlagIcon(), this.getFlueSpcFlag());
                            return false;
                        } else if ((NO).equalsIgnoreCase(goItem)) {
                            this.getMlpQty().setDisabled(false);
                            this.refreshContentOfUIComponent(this.getMlpQty());
                            this.setFocusMlpQty();
                            this.setErrorUi(this.getMlpQtyIcon(), this.getMlpQty());
                            return false;
                        }
                    }
                } else {
                    if (!"Visible".equalsIgnoreCase(this.getConfirmHighMlpPopup().getLauncherVar()))
                        checkSysCount(cidQtyIn, true);
                }
            } else {
                if (!"Visible".equalsIgnoreCase(this.getConfirmHighMlpPopup().getLauncherVar()))
                    checkSysCount(cidQtyIn, true);
            }
        }
        return true;
    }

    private void clearQtyFields() {
        ADFUtils.setBoundAttributeValue("CidQty", null);
        ADFUtils.setBoundAttributeValue("MlpQty", null);
        this.getCidQty().resetValue();
        this.getMlpQty().resetValue();
        this.refreshContentOfUIComponent(this.getCidQty());
        this.refreshContentOfUIComponent(this.getMlpQty());
    }

    private void setFocusOnActiveComponent() {
        String currentFocus = this.getHhInventoryVerificationSBeanPageFlowBean().getIsFocusOn();
        if ("MlpQtyField".equalsIgnoreCase(currentFocus)) {
            this.getMlpQty().setDisabled(false);
            this.refreshContentOfUIComponent(this.getMlpQty());
            this.setFocusMlpQty();
            return;
        } else {
            this.getCidQty().setDisabled(false);
            this.refreshContentOfUIComponent(this.getCidQty());
            this.setFocusCidQty();
            return;
        }
    }

    private void checkSysCount(BigDecimal cidQtyIn, boolean yesLink) {
        BigDecimal mlpQtyIn = new BigDecimal((String) ADFUtils.getBoundAttributeValue("MlpQty"));
        OperationBinding oper3 = (OperationBinding) ADFUtils.findOperation("callPCheckSystemCount");
        Map map3 = oper3.getParamsMap();
        map3.put("cidQtyIn", cidQtyIn);
        map3.put("mlpQtyIn", mlpQtyIn);
        oper3.execute();
        List<Object> codeList = (List<Object>) oper3.getResult();
        if (codeList != null && codeList.size() == 3) {
            String errorMsg = (String) codeList.get(1);
            this.showErrorPanel(errorMsg);
            if (yesLink) {
                this.clearQtyFields();
                if (errorMsg != null && errorMsg.contains("Must start over") &&
                    YES.equals(ADFUtils.getBoundAttributeValue("FlueSpaceReqd"))) {
                    this.setFocusFlueSpcFlag();
                    this.setErrorUi(this.getFlueSpcFlagIcon(), this.getFlueSpcFlag());
                } else {
                    this.setFocusMlpQty();
                    this.setErrorUi(this.getMlpQtyIcon(), this.getMlpQty());
                }
                return;
            } else {
                this.getCidQty().setDisabled(false);
                this.setFocusCidQty();
                this.setErrorUi(this.getCidQtyIcon(), this.getCidQty());
                return;
            }
        }
        Integer outputTmpVar = null;
        String pGoItem = null;
        if ( codeList != null) {
            outputTmpVar = ((BigDecimal) codeList.get(0)).intValueExact();
            pGoItem =    (String) codeList.get(1);
        }
        if (outputTmpVar !=null){
        if (outputTmpVar == 1 || outputTmpVar == 4 || outputTmpVar == 5) {
            this.showErrorPanel("INV_COMP", CONF);
            this.clearFields();
            this.getCidQty().setDisabled(true);
            this.refreshContentOfUIComponent(this.getCidQty());
            this.setFocusLocationId();
        } else if (outputTmpVar == 3 || outputTmpVar == 7 || outputTmpVar == 12) {
            this.showErrorPanel("START_OVER", ERROR);
            this.clearQtyFields();
            if (YES.equalsIgnoreCase(pGoItem)) {
                this.getCidQty().setDisabled(true);
                this.refreshContentOfUIComponent(this.getCidQty());
                this.setFocusFlueSpcFlag();
            } else if (NO.equalsIgnoreCase(pGoItem)) {
                this.getCidQty().setDisabled(true);
                this.refreshContentOfUIComponent(this.getCidQty());
                this.setFocusMlpQty();
            }
        } else if (outputTmpVar == 8 || outputTmpVar == 9 || outputTmpVar == 10 || outputTmpVar == 13 ||
                   outputTmpVar == 14) {
            this.showErrorPanel("SCAN_LABEL", ERROR);
            this.getCidQty().setDisabled(true);
            this.refreshContentOfUIComponent(this.getCidQty());
            this.getContainerId().setDisabled(false);
            this.refreshContentOfUIComponent(this.getContainerId());
            this.setFocusContainerId();
        } else if (outputTmpVar == 11 || outputTmpVar == 2 || outputTmpVar == 6) {
            this.showErrorPanel("RE_COUNT", ERROR);
            this.clearQtyFields();
            this.getCidQty().setDisabled(true);
            this.refreshContentOfUIComponent(this.getCidQty());
            this.setFocusMlpQty();
            this.getMlpQty().setDisabled(false);
        }
        }
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            _logger.info("onChangedContainerId() Start");
            this.updateModel(vce);
            if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
                String containerIdIn = ((String) vce.getNewValue());
                this.callVContainerId(containerIdIn);
            }
            _logger.info("onChangedContainerId() End");
        }
    }

    private boolean callVContainerId(String containerIdIn) {
        _logger.info("callVContainerId() Start");
        this.hideMessagesPanel();
        this.getContainerIdIcon().setName(REQ_ICON);
        this.refreshContentOfUIComponent(getContainerIdIcon());
        this.removeErrorStyleToComponent(getContainerId());
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPValidateContainer1");
        oper.execute();
        OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callValidateChkDigit");
        Map map2 = oper2.getParamsMap();
        map2.put("containerId", containerIdIn);
        oper2.execute();
        Integer validated = (Integer) oper2.getResult();
        if (validated == 0) {
            this.showErrorPanel("INV_CHECK_DIGIT", ERROR);
            this.setErrorUi(getContainerIdIcon(), getContainerId());
            return false;
        } else {
            OperationBinding oper3 = (OperationBinding) ADFUtils.findOperation("callPValidateContainer2");
            List codesList = (List) oper3.execute();
            if (null != codesList && codesList.size() == 3) {
                String clearBlock = (String) codesList.get(2);
                this.showErrorPanel((String) codesList.get(1), (String) codesList.get(0), false);
                if (YES.equalsIgnoreCase(clearBlock)) {
                    this.clearFields();
                    this.getContainerId().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getContainerId());
                    this.setFocusLocationId();
                }
            } else if (null != codesList && codesList.size() == 1) {
                ADFUtils.setBoundAttributeValue("ContainerId", null);
                this.getContainerId().resetValue();
                this.refreshContentOfUIComponent(this.getContainerId());
            }
        }
        return true;
    }


    /******************************************************************************************
     *                                  TrKeyIn and performKey
     ******************************************************************************************/

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        this.removeErrorOnFields();
        if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F4".equalsIgnoreCase(key)) {
            String currentFocus = this.getHhInventoryVerificationSBeanPageFlowBean().getIsFocusOn();
            if ("ContainerIdField".equalsIgnoreCase(currentFocus)) {
                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPExecuteCount");
                oper.execute();
                List<Object> codeList = (List<Object>) oper.getResult();
                BigDecimal pBuiltInsSeq = (BigDecimal) codeList.get(0);
                if ((new BigDecimal(1)).equals(pBuiltInsSeq)) {
                    this.showErrorPanel("INV_COMP", CONF);
                    this.clearFields();
                    this.setFocusLocationId();
                } else if ((new BigDecimal(2)).equals(pBuiltInsSeq)) {
                    String pClearCountsGoItem = (String) codeList.get(1);
                    if (YES.equalsIgnoreCase(pClearCountsGoItem)) {
                        this.clearBlock(YES);
                        this.setErrorUi(getFlueSpcFlagIcon(), getFlueSpcFlag());
                    } else if (NO.equalsIgnoreCase(pClearCountsGoItem)) {
                        this.clearBlock(NO);
                        this.setErrorUi(getMlpQtyIcon(), getMlpQty());
                    }
                    this.showErrorPanel((String) codeList.get(3));
                }
            } else {
                validateCurrentField();
            }
        } else if ("F5".equalsIgnoreCase(key)) {
            String currentFocus = this.getHhInventoryVerificationSBeanPageFlowBean().getIsFocusOn();
            if (!"LocationIdField".equalsIgnoreCase(currentFocus)) {
                this.callPclearCounts();
            }
        }
    }

    private void removeErrorOnFields() {
        this.removeErroIcons();
        this.removeErrorStyleToComponent(this.getLocationId());
        this.removeErrorStyleToComponent(this.getFlueSpcFlag());
        this.removeErrorStyleToComponent(this.getMlpQty());
        this.removeErrorStyleToComponent(this.getCidQty());
        this.removeErrorStyleToComponent(this.getContainerId());
    }

    private void validateCurrentField() {
        String currentFocus = this.getHhInventoryVerificationSBeanPageFlowBean().getIsFocusOn();
        if ("LocIdField".equals(currentFocus)) {
            this.onChangedLocationId(new ValueChangeEvent(this.getLocationId(), null, this.getLocationId().getValue()));
        } else if ("FlueSpcFlagField".equals(currentFocus)) {
            this.onChangedFlueSpcFlag(new ValueChangeEvent(this.getFlueSpcFlag(), null,
                                                           this.getFlueSpcFlag().getValue()));
        } else if ("MlpQtyField".equals(currentFocus)) {
            this.onChangedMlpQty(new ValueChangeEvent(this.getMlpQty(), null, this.getMlpQty().getValue()));
        } else if ("CidQtyField".equals(currentFocus)) {
            this.onChangedCidQty(new ValueChangeEvent(this.getCidQty(), null, this.getCidQty().getValue()));
        } else if ("ContainerIdField".equals(currentFocus)) {
            this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null,
                                                           this.getContainerId().getValue()));
        }
    }

    private void removeErroIcons() {
        this.getCidQtyIcon().setName(REQ_ICON);
        this.getMlpQtyIcon().setName(REQ_ICON);
        this.getFlueSpcFlagIcon().setName(REQ_ICON);
        this.getLocationIdIcon().setName(REQ_ICON);
        this.getContainerIdIcon().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getCidQtyIcon());
        this.refreshContentOfUIComponent(this.getMlpQtyIcon());
        this.refreshContentOfUIComponent(this.getFlueSpcFlagIcon());
        this.refreshContentOfUIComponent(this.getLocationIdIcon());
        this.refreshContentOfUIComponent(this.getContainerIdIcon());
    }

    private void callPclearCounts() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPClearCounts");
        oper.execute();
        List<Object> codeList = (List<Object>) oper.getResult();
        if (codeList != null && codeList.size() == 3) {
            this.showErrorPanel((String) codeList.get(1));
            return;
        }
        if (codeList !=null )
        this.clearBlock((String) codeList.get(0));
    }

    private void clearCurrentRow() {
        HhInventoryVerificationSInvVerificationViewRowImpl hhRowImpl = this.getInvVerifViewCurrentRow();
        hhRowImpl.setMlpQty(null);
        hhRowImpl.setMlpQty1(null);
        hhRowImpl.setMlpSysCnt(null);
        hhRowImpl.setCidQty(null);
        hhRowImpl.setCidQty1(null);
        hhRowImpl.setCidSysCnt(null);
        hhRowImpl.setContainerId(null);
        this.getMlpQty().resetValue();
        this.getCidQty().resetValue();
        this.getContainerId().resetValue();
        this.refreshContentOfUIComponent(this.getMlpQty());
        this.refreshContentOfUIComponent(this.getCidQty());
        this.refreshContentOfUIComponent(this.getContainerId());
    }

    private void clearBlock(String flag) {
        this.clearCurrentRow();
        this.removeErroIcons();
        this.disableFields();
        this.hideMessagesPanel();
        if (YES.equalsIgnoreCase(flag)) {
            this.getFlueSpcFlag().setDisabled(false);
            this.refreshContentOfUIComponent(this.getFlueSpcFlag());
            this.setFocusFlueSpcFlag();
        } else if (NO.equalsIgnoreCase(flag)) {
            this.getMlpQty().setDisabled(false);
            this.refreshContentOfUIComponent(this.getMlpQty());
            this.setFocusMlpQty();
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                RichInputText field = (RichInputText) clientEvent.getComponent();
                String currentFieldId = field.getId();
                if (null == submittedValue || "".equalsIgnoreCase(submittedValue))
                    return;
                if ("loc1".equals(currentFieldId)) {
                    if ((StringUtils.isEmpty((String) this.getLocationId().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getLocationId().getValue())) {
                        this.onChangedLocationId(new ValueChangeEvent(this.getLocationId(), null, submittedValue));
                    }
                } else if ("flu1".equals(currentFieldId)) {
                    this.onChangedFlueSpcFlag(new ValueChangeEvent(this.getFlueSpcFlag(), null, submittedValue));
                } else if ("mlp1".equals(currentFieldId)) {
                    this.onChangedMlpQty(new ValueChangeEvent(this.getMlpQty(), null, submittedValue));
                } else if ("cid1".equals(currentFieldId)) {
                    this.onChangedCidQty(new ValueChangeEvent(this.getCidQty(), null, submittedValue));
                } else if ("con".equals(currentFieldId)) {
                    if ((StringUtils.isEmpty((String) this.getContainerId().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equals(this.getContainerId().getValue())) {
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    }
                }
            }
        }
    }


    /******************************************************************************************
     *                                  Utility methods
     ******************************************************************************************/


    private void setErrorUi(RichIcon icon, RichInputText field) {
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        this.addErrorStyleToComponent(field);
        this.selectTextOnUIComponent(field);
        this.refreshContentOfUIComponent(field);
    }

    private void showErrorPanel(String error) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);

        this.getErrorWarnInfoMessage().setValue(error);
        this.getErrorWarnInfoIcon().setName(ERR_ICON);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void showErrorPanel(String errorName, String errorCode) {
        showErrorPanel(errorName, errorCode, true);
    }


    private void showErrorPanel(String errorName, String errorCode, boolean getDescription) {

        String error = null;
        if (getDescription) {
            error = this.getMessage(errorName, errorCode, this.getFacilityIdAttrValue(), this.getLangCodeAttrValue());
        } else {
            error = errorName;
        }

        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);

        this.getErrorWarnInfoMessage().setValue(error);
        if (errorCode.equalsIgnoreCase(INFO)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (errorCode.equalsIgnoreCase(WARN)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        }else if (errorCode.equalsIgnoreCase(CONF)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        
    if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_inventory_verification_s_INV_VERIFICATION_F4,RoleBasedAccessConstants.FORM_NAME_hh_inventory_verification_s)){
               this.trKeyIn("F4"); 
           }else{
               showErrorPanel("NOT_ALLOWED", ERROR);
           }
    }

    public void f5ActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_inventory_verification_s_INV_VERIFICATION_F5,RoleBasedAccessConstants.FORM_NAME_hh_inventory_verification_s)){
                   this.trKeyIn("F5");
               }else{
                   showErrorPanel("NOT_ALLOWED", ERROR);
               }
    }

    private void setFocusLocationId() {
        this.disableFields();
        this.getLocationId().setDisabled(false);
        this.getHhInventoryVerificationSBeanPageFlowBean().setIsFocusOn("LocationIdField");
        this.setFocusOnUIComponent(this.getLocationId());
        if (!this.isErrorWarningSuccessShowing())
            this.showErrorPanel("Enter Location ID", INFO, false);
    }

    private void setFocusFlueSpcFlag() {
        this.disableFields();
        this.getFlueSpcFlag().setDisabled(false);
        this.getHhInventoryVerificationSBeanPageFlowBean().setIsFocusOn("FlueSpcFlagField");
        this.setFocusOnUIComponent(this.getFlueSpcFlag());
        if (!this.isErrorWarningSuccessShowing())
            this.showErrorPanel("Enter Y/N", INFO, false);
    }

    private boolean isErrorWarningSuccessShowing() {
        return ERR_ICON.equalsIgnoreCase(this.getErrorWarnInfoIcon().getName()) ||
               WRN_ICON.equalsIgnoreCase(this.getErrorWarnInfoIcon().getName()) ||
               "logo".equalsIgnoreCase(this.getErrorWarnInfoIcon().getName());
    }

    private void setFocusMlpQty() {
        this.disableFields();
        this.getMlpQty().setDisabled(false);
        this.getHhInventoryVerificationSBeanPageFlowBean().setIsFocusOn("MlpQtyField");
        this.setFocusOnUIComponent(this.getMlpQty());
        if (!this.isErrorWarningSuccessShowing())
            this.showErrorPanel("Enter MLP Qty", INFO, false);
    }

    private void setFocusCidQty() {
        this.disableFields();
        this.getCidQty().setDisabled(false);
        this.getHhInventoryVerificationSBeanPageFlowBean().setIsFocusOn("CidQtyField");
        this.setFocusOnUIComponent(this.getCidQty());
        if (!this.isErrorWarningSuccessShowing())
            this.showErrorPanel("Enter CID Qty", INFO, false);
    }

    private void setFocusContainerId() {
        this.disableFields();
        this.getContainerId().setDisabled(false);
        this.getHhInventoryVerificationSBeanPageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getContainerId());
        if (!this.isErrorWarningSuccessShowing())
            this.showErrorPanel("Enter Container ID", INFO, false);
    }

    private HhInventoryVerificationSInvVerificationViewRowImpl getInvVerifViewCurrentRow() {
        return (HhInventoryVerificationSInvVerificationViewRowImpl) ADFUtils.findIterator("HhInventoryVerificationSInvVerificationViewIterator").getCurrentRow();
    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        if (getHhInventoryVerificationSBeanPageFlowBean() == null)
            return;
        Object gotoFld = getHhInventoryVerificationSBeanPageFlowBean().getIsFocusOn();
        if ("LocationIdField".equals(gotoFld)) {
            setFocusOnUIComponent(getLocationId());
            this.selectTextOnUIComponent(this.getLocationId());
            if (!this.isErrorWarningSuccessShowing())
                this.showErrorPanel("Enter Location ID", INFO, false);
        } else if ("FlueSpcFlagField".equals(gotoFld)) {
            setFocusOnUIComponent(getFlueSpcFlag());
            this.selectTextOnUIComponent(this.getFlueSpcFlag());
        } else if ("MlpQtyField".equals(gotoFld)) {
            setFocusOnUIComponent(getMlpQty());
            this.selectTextOnUIComponent(this.getMlpQty());
        } else if ("CidQtyField".equals(gotoFld)) {
            setFocusOnUIComponent(getCidQty());
            this.selectTextOnUIComponent(this.getCidQty());
        } else if ("ContainerIdField".equals(gotoFld)) {
            setFocusOnUIComponent(getContainerId());
            this.selectTextOnUIComponent(this.getContainerId());
        }
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        boolean currentFocus =
            this.getHhInventoryVerificationSBeanPageFlowBean().getIsFocusOn().equalsIgnoreCase("MlpQtyField");
        if (currentFocus) {
            if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
                this.getCidQty().setDisabled(false);
                this.refreshContentOfUIComponent(this.getCidQty());
                this.setFocusCidQty();
            } else {
                this.getMlpQty().setDisabled(false);
                this.refreshContentOfUIComponent(this.getMlpQty());
                this.setFocusMlpQty();
            }
        } else {
            if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
                Object cidQty = ADFUtils.getBoundAttributeValue("CidQty");
                 if (cidQty != null)//3838: NPE fix
                   this.checkSysCount(new BigDecimal((String)cidQty ), true); 
            } else {
                this.setFocusOnActiveComponent();
            }
        }
    }

    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmHighMlpPopup().setLauncherVar("");
        this.getConfirmHighMlpPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getConfirmHighMlpDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmHighMlpPopup().setLauncherVar("");
        this.getConfirmHighMlpPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getConfirmHighMlpDialog(), DialogEvent.Outcome.no));
    }

    private void disableFields() {
        this.getLocationId().setDisabled(true);
        this.getFlueSpcFlag().setDisabled(true);
        this.getMlpQty().setDisabled(true);
        this.getCidQty().setDisabled(true);
        this.getContainerId().setDisabled(true);
        this.refreshContentOfUIComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getFlueSpcFlag());
        this.refreshContentOfUIComponent(this.getMlpQty());
        this.refreshContentOfUIComponent(this.getCidQty());
        this.refreshContentOfUIComponent(this.getContainerId());
    }

    private void clearFields() {
        _logger.info("clearFields() Start");
        HhInventoryVerificationSInvVerificationViewRowImpl hhRowImpl = this.getInvVerifViewCurrentRow();
        hhRowImpl.setLocationId(null);
        hhRowImpl.setMlpQty(null);
        hhRowImpl.setMlpQty1(null);
        hhRowImpl.setMlpSysCnt(null);
        hhRowImpl.setCidQty(null);
        hhRowImpl.setCidQty1(null);
        hhRowImpl.setCidSysCnt(null);
        hhRowImpl.setContainerId(null);
        hhRowImpl.setFlueSpcFlag(null);//OTM3594, FLUE_SPC_FLAG was not being cleared out
        this.getLocationId().resetValue();
        this.getMlpQty().resetValue();
        this.getCidQty().resetValue();
        this.getContainerId().resetValue();
        this.getFlueSpcFlag().resetValue();
        this.refreshContentOfUIComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getMlpQty());
        this.refreshContentOfUIComponent(this.getCidQty());
        this.refreshContentOfUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(this.getFlueSpcFlag());
        _logger.info("clearFields() End");
    }


    /******************************************************************************************
     *                                  Getters and Setters
     ******************************************************************************************/


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setFlueSpcFlag(RichInputText flueSpcFlag) {
        this.flueSpcFlag = flueSpcFlag;
    }

    public RichInputText getFlueSpcFlag() {
        return flueSpcFlag;
    }

    public void setMlpQty(RichInputText mlpQty) {
        this.mlpQty = mlpQty;
    }

    public RichInputText getMlpQty() {
        return mlpQty;
    }

    public void setCidQty(RichInputText cidQty) {
        this.cidQty = cidQty;
    }

    public RichInputText getCidQty() {
        return cidQty;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void setFlueSpcFlagIcon(RichIcon flueSpcFlagIcon) {
        this.flueSpcFlagIcon = flueSpcFlagIcon;
    }

    public RichIcon getFlueSpcFlagIcon() {
        return flueSpcFlagIcon;
    }

    public void setMlpQtyIcon(RichIcon mlpQtyIcon) {
        this.mlpQtyIcon = mlpQtyIcon;
    }

    public RichIcon getMlpQtyIcon() {
        return mlpQtyIcon;
    }

    public void setCidQtyIcon(RichIcon cidQtyIcon) {
        this.cidQtyIcon = cidQtyIcon;
    }

    public RichIcon getCidQtyIcon() {
        return cidQtyIcon;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setInventoryVerificationPanel(RichPanelFormLayout inventoryVerificationPanel) {
        this.inventoryVerificationPanel = inventoryVerificationPanel;
    }

    public RichPanelFormLayout getInventoryVerificationPanel() {
        return inventoryVerificationPanel;
    }

    private HhInventoryVerificationSBean getHhInventoryVerificationSBeanPageFlowBean() {
        return ((HhInventoryVerificationSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setConfirmHighMlpPopup(RichPopup confirmIntrlvgPopup) {
        this.confirmHighMlpPopup = confirmIntrlvgPopup;
    }

    public RichPopup getConfirmHighMlpPopup() {
        return confirmHighMlpPopup;
    }

    public void setDialogConfirmHighMlpOutputtext(RichOutputText dialogConfirmIntrlvgOutputtext) {
        this.dialogConfirmHighMlpOutputtext = dialogConfirmIntrlvgOutputtext;
    }

    public RichOutputText getDialogConfirmHighMlpOutputtext() {
        return dialogConfirmHighMlpOutputtext;
    }

    public void setConfirmHighMlpDialog(RichDialog confirmIntrlvgDialog) {
        this.confirmHighMlpDialog = confirmIntrlvgDialog;
    }

    public RichDialog getConfirmHighMlpDialog() {
        return confirmHighMlpDialog;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_inventorymgmt_hhinventoryverifications_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
