package com.ross.rdm.inventorymgmt.hhinventoryverifications.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhInventoryVerificationSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhInventoryVerificationSBean.class);
    private String isFocusOn;
    private Boolean isLocationIdValidated;
    private Boolean isFlueSpcFlagValidated;
    private Boolean isMlpQtyValidated;
    private Boolean isCidQtyValidated;
    private Boolean isContainerIdValidated;
                
    public void initTaskFlow() {
        _logger.info("Inventory verification page initialization begun");
        this.initGlobalVariablesInvVerif();
        this.initInvVerifWorkVariables();
        this.initInvVerifRow();
        this.callStartupLocal1();
        this.setIsFocusOn("LocationIdField");
        _logger.info("Inventory verification page initialization completed");
    }

    private void initGlobalVariablesInvVerif() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesInvVerification");
        oper.execute();
    }

    private void initInvVerifWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setInvVerifWorkVariables");
        oper.execute();
    }
    
    private void callStartupLocal1() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callStartupLocal1");
        oper.execute();
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    private void initInvVerifRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertInvVerifRow");
        oper.execute();
    }

    public void setIsLocationIdValidated(Boolean isLocationIdValidated) {
        this.isLocationIdValidated = isLocationIdValidated;
    }

    public Boolean getIsLocationIdValidated() {
        return isLocationIdValidated;
    }

    public void setIsFlueSpcFlagValidated(Boolean isFlueSpcFlagValidated) {
        this.isFlueSpcFlagValidated = isFlueSpcFlagValidated;
    }

    public Boolean getIsFlueSpcFlagValidated() {
        return isFlueSpcFlagValidated;
    }

    public void setIsMlpQtyValidated(Boolean isMlpQtyValidated) {
        this.isMlpQtyValidated = isMlpQtyValidated;
    }

    public Boolean getIsMlpQtyValidated() {
        return isMlpQtyValidated;
    }

    public void setIsCidQtyValidated(Boolean isCidQtyValidated) {
        this.isCidQtyValidated = isCidQtyValidated;
    }

    public Boolean getIsCidQtyValidated() {
        return isCidQtyValidated;
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }
}
