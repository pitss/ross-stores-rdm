package com.ross.rdm.inventorymgmt.hhrosscontinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMInventoryManagementBackingBean {
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String MASTER_CONTAINER_ID_ATTR = "MasterContainerId";
    private final static String LPN_QTY_ATTR = "LpnQty";
    private final static String LOCATION_ID_ATTR = "LocationId";
    private final static String UNIT_QTY_ATTR = "UnitQty";
    private final static String CONTAINER_STATUS = "ContainerStatus";
    private final static String WIP_PRESENT = "WipPresent";
    private final static String HH_ROSS_CONT_INQUIRY_BEAN = "HhRossContInquirySBean";

    private static ADFLogger _logger = ADFLogger.createADFLogger(RDMHotelPickingBackingBean.class);

    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private RichIcon containerIdIcon;
    private RichInputText containerId;
    private RichPanelFormLayout rossContInquiryPanel;
    private RichLink f3Link;
    private RichLink f4Link;

   
    static final String INTRLVG = "INTRLVG";


    public Page1Backing() {

    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateContainerId(enteredContainerIdValue);
        } else {
            this.getContainerIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getContainerIdIcon());
            this.refreshContentOfUIComponent(this.getContainerId());
            this.addErrorStyleToComponent(this.getContainerId());
            this.selectTextOnUIComponent(this.getContainerId());
            this.showErrorPanel(ERROR,
                                this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                this.getLanguageCodeValue()));
        }

    }

    public boolean validateContainerId(String containerId) {
        _logger.info("validateContainerId() Start");
        if (!this.getContainerId().isDisabled()) {
            if (this.getHhRossContInquirySPageFlowBean().getIsError() && containerId != null) {
                this.setErrorStyleClass(this.getContainerId(), true);
                this.selectTextOnUIComponent(this.getContainerId());
                this.getHhRossContInquirySPageFlowBean().setIsContainerIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getHhRossContInquirySPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getContainerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("displayContainerInfo");
                Map map = oper.getParamsMap();
                map.put("facilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));
                map.put("containerId", containerId);
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showErrorPanel(ERROR, msg);
                        } else {
                            this.showErrorPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getContainerIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.selectTextOnUIComponent(this.getContainerId());
                        this.getHhRossContInquirySPageFlowBean().setIsContainerIdValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, containerId);
                        this.getContainerIdIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getContainerId());
                        this.refreshContentOfUIComponent(this.getRossContInquiryPanel());


                        _logger.info("onChangedContainerId() End");

                    }
                }
            }
        }
        this.getHhRossContInquirySPageFlowBean().setIsContainerIdValidated(true);
        _logger.info("validatedContainerId() End");
        return true;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
        String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {

                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();

            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {

                if (submittedValue != null && !submittedValue.isEmpty()) {
                    this.setErrorStyleClass(this.getContainerId(), false);
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));

                    if (!this.getHhRossContInquirySPageFlowBean().getIsContainerIdValidated()) {
                        clearBlock();
                    }
                } else {
                    this.getContainerIdIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getContainerIdIcon());
                    this.refreshContentOfUIComponent(this.getContainerId());
                    this.addErrorStyleToComponent(this.getContainerId());
                    this.selectTextOnUIComponent(this.getContainerId());
                    this.showErrorPanel(ERROR,
                                        this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                        this.getLanguageCodeValue()));
                    clearBlock();
                }
            }
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");

        if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        }
        if ("F4".equalsIgnoreCase(key)) {
            BigDecimal LpnQty = (BigDecimal) ADFUtils.getBoundAttributeValue(LPN_QTY_ATTR);

            if (this.getHhRossContInquirySPageFlowBean().getIsContainerIdValidated() && LpnQty != null) {

//                this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null,
//                                                               this.getContainerIdAttrValue()));


                if (LpnQty.intValue() > 0) {
                    HhRossContInquiryGetLpnsView();
                    getMlpHeader();
                    ADFUtils.invokeAction("Page2");

                } else {
                    hhRossContInquiryDisplayLpnsInfo();
                    getLpnHeader();
                    ADFUtils.invokeAction("Page3");
                }

            } else {
                clearBlock();
            }

        }
    }

    public void emptyContainerId() {
        this.showErrorPanel(ERROR,
                            this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                            this.getLanguageCodeValue()));
        this.getContainerIdIcon().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getContainerIdIcon());
        this.refreshContentOfUIComponent(this.getRossContInquiryPanel());
        this.addErrorStyleToComponent(this.getContainerId());
        clearBlock();

        return;
    }


    private void HhRossContInquiryGetLpnsView() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executeLpnsQuery");
        String facilityId = getFacilityIdAttrValue();
        String containerId = getContainerIdAttrValue();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("containerId", containerId);
        oper.execute();
    }

    public void hhRossContInquiryGetLpnsUnstagedView() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executeLpnsUnstagedQuery");
        String facilityId = getFacilityIdAttrValue();
        String containerId = getContainerIdAttrValue();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("containerId", containerId);
        oper.execute();
    }

    public void hhRossContInquiryDisplayLpnsInfo() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executeDisplayLpnsInfoQuery");
        String facilityId = getFacilityIdAttrValue();
        String containerId = getContainerIdAttrValue();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("containerId", containerId);
        oper.execute();
    }

    private void getMlpHeader() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("getMlpHeader");
        String facilityId = getFacilityIdAttrValue();
        String languageCode = getLanguageCodeValue();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("languageCode", languageCode);
        oper.execute();
    }

    private void getLpnHeader() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("getLpnHeader");
        String facilityId = getFacilityIdAttrValue();
        String languageCode = getLanguageCodeValue();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("languageCode", languageCode);
        oper.execute();
    }

    private void clearBlock() {
        _logger.info("clearBlock Start");
        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(MASTER_CONTAINER_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(LPN_QTY_ATTR, null);
        ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(UNIT_QTY_ATTR, null);
        ADFUtils.setBoundAttributeValue(CONTAINER_STATUS, null);
        ADFUtils.setBoundAttributeValue(WIP_PRESENT, null);

        getContainerId().resetValue();
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getContainerIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
    }

    private String getLanguageCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private void showErrorPanel(String messageType, String msg) {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }

        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    private void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    String getMessage(String msgCode, String messageType) {
        return getMessage(msgCode, messageType, getBoundAttribute(FACILITY_ID_ATTR),
                          getBoundAttribute(LANGUAGE_CODE_ATTR));
    }

    String getBoundAttribute(String attrName) {
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String) val;
        return null;
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con1")) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con1")) {
                this.getContainerIdIcon().setName("required");
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    private HhRossContInquirySBean getHhRossContInquirySPageFlowBean() {
        return ((HhRossContInquirySBean) this.getPageFlowBean(HH_ROSS_CONT_INQUIRY_BEAN));
    }

    public void setRossContInquiryPanel(RichPanelFormLayout rossContInquiryPanel) {
        this.rossContInquiryPanel = rossContInquiryPanel;
    }

    public RichPanelFormLayout getRossContInquiryPanel() {
        return rossContInquiryPanel;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ross_cont_inquiry_s_MAIN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ross_cont_inquiry_s)) {
            this.trKeyIn("F4");
        } else { 
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }
}
