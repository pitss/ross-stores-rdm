package com.ross.rdm.inventorymgmt.hhrosscontinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhRossContInquirySBean  {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhRossContInquirySBean.class);
    private Boolean isContainerIdValidated;
    private String  isFocusOn;
    private Boolean isError;
    private Boolean unstaged;
    private boolean notAllowed = false;

    public void setUnstaged(Boolean unstaged) {
        this.unstaged = unstaged;
    }

    public Boolean getUnstaged() {
        return unstaged;
    }

    public void setFirstRun(Boolean firstRun) {
        this.firstRun = firstRun;
    }

    public Boolean getFirstRun() {
        return firstRun;
    }
    private Boolean firstRun;
    
    public void init(){
        DCIteratorBinding ib = ADFUtils.findIterator("HhRossContInquiryGetLpnsViewIterator");
        ib.setCurrentRowIndexInRange(0);
    }


      public void initTaskFlowRossContInquiry() {
    _logger.info("Ross Cont Inquiry page initialization begun");
    this.initGlobalVariablesRossContInquiry();
    this.initHhRossContInquiryWorkVariables();
    this.initHhRossContInquiryRow();

    this.setFirstRun(true);
    this.setUnstaged(true);
    this.setIsError(false);
    this.setIsContainerIdValidated(false);
    this.setIsFocusOn("ContainerIdField");
    
            _logger.info("Ross Cont Inquiry page initialization completed");

}
    private void initGlobalVariablesRossContInquiry() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesRossContInquiry");
        oper.execute();
    }
    
    private void initHhRossContInquiryRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertRossContInquiry");
        oper.execute();
    }
    
    private void initHhRossContInquiryWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhRossContInquiryWorkVariables");
        oper.execute();
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }

    public void setNotAllowed(boolean notAllowed) {
        this.notAllowed = notAllowed;
    }

    public boolean isNotAllowed() {
        return notAllowed;
    }
}
