package com.ross.rdm.inventorymgmt.hhrosscontinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.view.bean.HhAddToContainerSBean;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.event.SelectionEvent;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMInventoryManagementBackingBean {

    private static final String FIRST_RECORD = "FIRST RECORD";
    private static final String LAST_RECORD = "LAST RECORD";
    private static final String HH_ROSS_CONT_INQUIRY_S_BEAN = "HhRossContInquirySBean";


    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private RichLink f3Link;
    private RichLink f7Link;
    private RichLink f8Link;
    private RichLink f9Link;

    private RichTable containerTable;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private static Page1Backing _page1 = new Page1Backing();


    public Page2Backing() {
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF9Link());
                actionEvent.queue();
            }
        }
    }

    private void trKeyIn(String key) {
        DCIteratorBinding ib;
        int crow;
        long trow;

        if (getUnstaged())
            ib = ADFUtils.findIterator("HhRossContInquiryGetLpnsViewIterator");
        else
            ib = ADFUtils.findIterator("HhRossContInquiryGetLpnsUnstagedViewIterator");

        crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        trow = ib.getEstimatedRowCount();

        if ("F3".equalsIgnoreCase(key)) {
            ADFUtils.invokeAction("Page1");
        } else if ("F7".equalsIgnoreCase(key)) {
            if (crow - 5 <= 0) {
                this.showMessagesPanel(INFO, FIRST_RECORD);
                ib.setCurrentRowIndexInRange(0);
            } else
                ib.setCurrentRowIndexInRange(crow - 5);
            this.hideMessagesPanel();
        }

        else if ("F8".equalsIgnoreCase(key)) {
            if (crow + 5 >= trow - 1) {
                this.showMessagesPanel(INFO, LAST_RECORD);
                ib.setCurrentRowIndexInRange((int) trow - 1);

            } else
                ib.setCurrentRowIndexInRange(crow + 5);
            this.hideMessagesPanel();
        }

        else if ("F9".equalsIgnoreCase(key)) {
            if (getUnstaged() && getFirstRun()) {
                _page1.hhRossContInquiryGetLpnsUnstagedView();
                this.setUnstaged(false);
                this.setFirstRun(false);
            } else if (getUnstaged() && !getFirstRun()) {
                this.setUnstaged(false);
            } else {
                this.setUnstaged(true);
            }
        }
        this.refreshContentOfUIComponent(getContainerTable());
        this.refreshContentOfUIComponent(getF9Link());
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        if (getUnstaged()) {
            JSFUtils.resolveMethodExpression("#{bindings.HhRossContInquiryGetLpnsView.collectionModel.makeCurrent}",
                                             SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                             selectionEvent });
            this.showFirstLastRecordInfo();
        }
        if (!getUnstaged()) {
            JSFUtils.resolveMethodExpression("#{bindings.HhRossContInquiryGetLpnsUnstagedView.collectionModel.makeCurrent}",
                                             SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                             selectionEvent });
            this.showFirstLastRecordInfo();
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (!this.getPageFlowBean().isNotAllowed()) {
            this.showFirstLastRecordInfo();
        }
        _logger.info("onRegionLoad End");
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f7ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_2_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ross_cont_inquiry_s)) {
            this.trKeyIn("F7");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
            this.getPageFlowBean().setNotAllowed(true);
        }
    }

    public void f8ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_2_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ross_cont_inquiry_s)) {
            this.trKeyIn("F8");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
            this.getPageFlowBean().setNotAllowed(true);
        }
    }

    public void f9ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_2_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ross_cont_inquiry_s)) {
            this.trKeyIn("F9");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
            this.getPageFlowBean().setNotAllowed(true);
        }
    }


    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib;
        int crow;
        long trow;

        if (getUnstaged())
            ib = ADFUtils.findIterator("HhRossContInquiryGetLpnsViewIterator");
        else
            ib = ADFUtils.findIterator("HhRossContInquiryGetLpnsUnstagedViewIterator");

        crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        trow = ib.getEstimatedRowCount();

        if (crow == trow - 1) {
            this.showMessagesPanel(INFO, LAST_RECORD);
        } else if (crow == 0) {
            this.showMessagesPanel(INFO, FIRST_RECORD);
        } else {
            this.hideMessagesPanel();
        }
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("hideMessagesPanel() End");
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("showMessagesPanel() End");
    }

    private HhRossContInquirySBean getPageFlowBean() {
        return (HhRossContInquirySBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhRossContInquirySBean");
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    public void setF9Link(RichLink f9Link) {
        this.f9Link = f9Link;
    }

    public RichLink getF9Link() {
        return f9Link;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setUnstaged(Boolean unstaged) {
        ((HhRossContInquirySBean) this.getPageFlowBean(HH_ROSS_CONT_INQUIRY_S_BEAN)).setUnstaged(unstaged);
    }

    public Boolean getUnstaged() {

        return ((HhRossContInquirySBean) this.getPageFlowBean(HH_ROSS_CONT_INQUIRY_S_BEAN)).getUnstaged();
    }

    public void setFirstRun(Boolean firstRun) {
        ((HhRossContInquirySBean) this.getPageFlowBean(HH_ROSS_CONT_INQUIRY_S_BEAN)).setFirstRun(firstRun);
    }

    public Boolean getFirstRun() {

        return ((HhRossContInquirySBean) this.getPageFlowBean(HH_ROSS_CONT_INQUIRY_S_BEAN)).getFirstRun();
    }

    public void setContainerTable(RichTable containerTable) {
        this.containerTable = containerTable;
    }

    public RichTable getContainerTable() {
        return containerTable;
    }
}
