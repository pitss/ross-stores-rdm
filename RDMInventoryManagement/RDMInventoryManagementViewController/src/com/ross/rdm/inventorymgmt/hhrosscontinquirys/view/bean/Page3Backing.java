package com.ross.rdm.inventorymgmt.hhrosscontinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page3.jspx
// ---    
// ---------------------------------------------------------------------
public class Page3Backing extends RDMInventoryManagementBackingBean {
  private RichPanelGroupLayout errorPanel;
  private RichIcon errorMessageIcon;
  private RichOutputText errorMessage;
  
    private RichPanelFormLayout rossContInquiryPanel;

  
    private RichLink f3Link;
    private RichLink f7Link;
    private RichLink f8Link;
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);


  public Page3Backing() {

  }
  
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            }
            else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
                        }
        }
    }
    
    private void trKeyIn(String key) {
        DCIteratorBinding ib = ADFUtils.findIterator("HhRossContInquiryDisplayLpnsInfoViewIterator");
        if ("F3".equalsIgnoreCase(key)) {
            ADFUtils.invokeAction("Page1");        
        } 
        else if ("F7".equalsIgnoreCase(key)) {
            if(ib.getRowSetIterator().hasPrevious())
                ib.getRowSetIterator().previous();
        }
        else if ("F8".equalsIgnoreCase(key)) {
            if(ib.getRowSetIterator().hasNext())
            ib.getRowSetIterator().next();
        }
    }
    
    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f7ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_3_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ross_cont_inquiry_s)) {
            this.trKeyIn("F7");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }
    
    public void f8ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_3_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ross_cont_inquiry_s)) {
            this.trKeyIn("F8");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }


    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    public void setRossContInquiryPanel(RichPanelFormLayout rossContInquiryPanel) {
        this.rossContInquiryPanel = rossContInquiryPanel;
    }

    public RichPanelFormLayout getRossContInquiryPanel() {
        return rossContInquiryPanel;
    }
}
