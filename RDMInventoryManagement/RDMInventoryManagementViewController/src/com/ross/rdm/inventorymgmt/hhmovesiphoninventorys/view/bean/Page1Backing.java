package com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;

import java.util.List;

import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMMobileBaseBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelGroupLayout globalPgl;
    private RichInputText cid;
    private RichInputText loc;
    private RichInputText ite;
    private RichInputText des;
    private RichInputText ups;
    private RichInputText toL;
    private RichInputText qty;
    private RichLink f2DetailLink;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f6WaveLink;
    private final static String FORM_NAME = "HH_MOVE_INVENTORY_S";
    private final static String F_BLOCK = "MOVE_BLOCK.";
    private RichLink validateContainerLink;
    private RichIcon iconContainer;
    private RichIcon iconToLoc;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichPanelFormLayout fieldsPanel;

    public void setIconToLoc(RichIcon iconToLoc) {
        this.iconToLoc = iconToLoc;
    }

    public RichIcon getIconToLoc() {
        return iconToLoc;
    }


    public void onChangedContainerId(ValueChangeEvent vce) {
        if (!this.isCurrentTaskFlowActive())
            return;
        this.updateModel(vce);
        String newCid = (String) vce.getNewValue();
        if (newCid != null && !newCid.isEmpty()) {
            this.callDisplayInfo();
        }
    }

    private boolean isCurrentTaskFlowActive() {
        return null != this.getMoveSiphonPageFlowBean();
    }

    public void onChangedToLocation(ValueChangeEvent vce) {
        if (!this.isCurrentTaskFlowActive())
            return;
        this.updateModel(vce);
        String newLocId = (String) vce.getNewValue();
        if (newLocId != null && !newLocId.isEmpty())
            this.callValidateToLocation();
    }


    private void callDisplayInfo() {
        OperationBinding oper = ADFUtils.findOperation("callDisplayMoveBlock");
        List result = (List) oper.execute();
        if (result != null) {
            int listSize = result.size();
            if (listSize == 3) {
                this.showMessagesPanel((String) result.get(0), (String) result.get(1));
                this.setErrorContainerId();
                this.setContainerValid(false);
            } else if (listSize == 2) {
                this.showMessagesPanel((String) result.get(0),
                                       this.getMessage((String) result.get(1), (String) result.get(0),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                this.setErrorContainerId();
                this.setContainerValid(false);
            } else {
                this.setContainerValid(true);
                this.removErrorContainerId();
                this.setFocusLocation();
            }
        }
    }


    private void callValidateToLocation() {
        OperationBinding oper = ADFUtils.findOperation("callValidateToLocation");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List result = (List) oper.getResult();
            if (result != null && result.size() == 2) {
                this.setLocationValid(false);
                this.showMessagesPanel((String) result.get(0), (String) result.get(1));
                this.setErrorLocationId();
            } else {
                this.setLocationValid(true);
                this.removErrorLocId();
            }
        }
    }

    private void setErrorOnActiveComponent() {
        HhMoveSiphonInventorySBean fbean = this.getMoveSiphonPageFlowBean();
        if (fbean != null) {
            if ("CONTAINER_ID".equalsIgnoreCase(fbean.getIsFocusOn()))
                this.setErrorContainerId();
            else
                this.setErrorLocationId();
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        HhMoveSiphonInventorySBean fbean = this.getMoveSiphonPageFlowBean();
        if (fbean != null)
            if ("CONTAINER_ID".equalsIgnoreCase(fbean.getIsFocusOn())) {
                this.setFocusOnUIComponent(this.getCid());
            } else {
                this.setFocusOnUIComponent(this.getToL());
            }
    }

    private HhMoveSiphonInventorySBean getMoveSiphonPageFlowBean() {
        return (HhMoveSiphonInventorySBean) this.getPageFlowBean("HhMoveSiphonInventorySBean");
    }

    private void setFocusLocation() {
        HhMoveSiphonInventorySBean fbean = this.getMoveSiphonPageFlowBean();

        fbean.setIsFocusOn("LOCATION_ID");
        this.getCid().setDisabled(true);
        this.getToL().setDisabled(false);
        this.refreshContentOfUIComponent(this.getCid());
        this.refreshContentOfUIComponent(this.getToL());
        this.setFocusOnUIComponent(this.getToL());
    }

    private void setErrorLocationId() {
        this.addErrorStyleToComponent(this.getToL());
        this.getIconToLoc().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconToLoc());
        this.selectTextOnUIComponent(this.getToL());
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String newValue = (String) clientEvent.getParameters().get("submittedValue");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String currentFocus = this.getMoveSiphonPageFlowBean().getIsFocusOn();
                if ("LOCATION_ID".equals(currentFocus)) {
                    this.onChangedToLocation(new ValueChangeEvent(this.getCid(), null, newValue));
                } else if ("CONTAINER_ID".equals(currentFocus)) {
                    this.onChangedContainerId(new ValueChangeEvent(this.getToL(), null, newValue));
                }
            }
        }
    }

    private void removErrorLocId() {
        this.hideMessagesPanel();
        this.removeErrorStyleToComponent(this.getToL());
        this.getIconToLoc().setName("required");
        this.refreshContentOfUIComponent(this.getIconToLoc());
    }

    private void removErrorContainerId() {
        this.hideMessagesPanel();
        this.removeErrorStyleToComponent(this.getCid());
        this.getIconContainer().setName("required");
        this.refreshContentOfUIComponent(this.getIconContainer());
    }

    private void setErrorContainerId() {
        this.addErrorStyleToComponent(this.getCid());
        this.getIconContainer().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getIconContainer());
        this.selectTextOnUIComponent(this.getCid());
    }

    private void callInsertEventCodeTrExit2() {
        ADFUtils.findOperation("callInsertEventCode").execute();
    }

    private void callSaveLaborProd2() {
        ADFUtils.findOperation("callSaveLaborProd2").execute();
    }

    public Page1Backing() {

    }

    public String f3Action() {
        return this.trKeyIn("F3");
    }

    public String f4Action() {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F4,
                                                FORM_NAME_hh_move_siphon_inventory_s)) {
            return this.trKeyIn("F4");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
            return null;
        }
    }

    public String f6Action() {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F6,
                                                FORM_NAME_hh_move_siphon_inventory_s)) {
            return this.trKeyIn("F6");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
            return null;
        }
    }

    public String f2Action() {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F2,
                                                FORM_NAME_hh_move_siphon_inventory_s)) {
            return this.trKeyIn("F2");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
            return null;
        }
    }

    private String trKeyIn(String key) {
        if ("F2".equalsIgnoreCase(key)) {
            return this.processF2();
        } else if ("F6".equalsIgnoreCase(key)) {
            return this.processF6();
        } else if ("F3".equalsIgnoreCase(key)) {
            return this.processF3();
        } else if ("F4".equalsIgnoreCase(key)) {
            if (this.checkContainerId() && this.checkLocationId() && this.isContainerValid() && this.isLocationValid())
                return this.processF4();
        }
        return "";
    }

    private String processF3() {
        this.callSaveLaborProd2();
        this.callInsertEventCodeTrExit2();
        return "home";
    }

    private String processF6() {
        String action = "";
        String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
        String facilityId = (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID);
        OperationBinding oper = this.executeFindWave(containerId, facilityId);
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            Long contHasItems = (Long) oper.getResult();
            if (contHasItems != null && contHasItems.longValue() > 0) {
                return action = "Page3";
            } else {
                String languageCode = (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE);
                this.setErrorOnActiveComponent();
                this.showMessagesPanel(WARN, this.getMessage("NO_DATA", WARN, facilityId, languageCode));
            }
        }
        return action;
    }

    private String processF2() {
        String action = "";
        if (this.checkContainerId()) {
            String facilityId = (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID);
            String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
            OperationBinding oper = this.executeFindItems(facilityId, containerId);
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                Long contHasItems = (Long) oper.getResult();
                if (contHasItems != null && contHasItems.longValue() > 0) {
                    return action = "Page2";
                } else {
                    String languageCode = (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE);
                    this.setErrorOnActiveComponent();
                    this.showMessagesPanel(WARN, this.getMessage("NO_DATA", WARN, facilityId, languageCode));
                }
            }
        }
        return action;

    }

    private String processF4() {
        List result;
        OperationBinding oper = ADFUtils.findOperation("callProcessMove");
        result = (List) oper.execute();
        if (result != null && result.size() > 1) {
            String msgCode = (String) result.get(0);
            String msg = (String) result.get(1);
            if (MSGTYPE_M.equalsIgnoreCase(msgCode)) {
                this.clear();
            }
            this.showMessagesPanel(msgCode,
                                   this.getMessage(msg, msgCode,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }
        return null;
    }

    private void clear() {
        this.clearCurrentRow();
        this.setInitialFocus();
    }

    private void clearCurrentRow() {
        (ADFUtils.findIterator("HhMoveSiphonInventorySMoveBlockViewIterator")).getCurrentRow().refresh(Row.REFRESH_WITH_DB_FORGET_CHANGES);
    }

    private void setInitialFocus() {
        this.getCid().resetValue();
        this.getLoc().resetValue();
        this.getIte().resetValue();
        this.getDes().resetValue();
        this.getUps().resetValue();
        this.getToL().resetValue();
        this.getQty().resetValue();
        this.refreshContentOfUIComponent(this.getCid());
        this.refreshContentOfUIComponent(this.getLoc());
        this.refreshContentOfUIComponent(this.getIte());
        this.refreshContentOfUIComponent(this.getDes());
        this.refreshContentOfUIComponent(this.getUps());
        this.refreshContentOfUIComponent(this.getToL());
        this.refreshContentOfUIComponent(this.getQty());
        HhMoveSiphonInventorySBean pfB = this.getMoveSiphonPageFlowBean();
        pfB.setIsFocusOn("CONTAINER_ID");
        this.getToL().setDisabled(true);
        this.getCid().setDisabled(false);
        this.refreshContentOfUIComponent(this.getToL());
        this.refreshContentOfUIComponent(this.getCid());
        this.setFocusOnUIComponent(this.getCid());
    }

    private OperationBinding executeFindItems(String facilityId, String containerId) {
        OperationBinding oper = ADFUtils.findOperation("findContainerItemsByParams");
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("containerId", containerId);
        oper.execute();
        return oper;
    }

    private OperationBinding executeFindWave(String containerId, String facilityId) {
        OperationBinding oper = ADFUtils.findOperation("executeWithParams");
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
        return oper;
    }


    public void showMessagesPanel(String messageType, String msg) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);

        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }


    private boolean checkLocationId() {
        String locationId = (String) ADFUtils.getBoundAttributeValue("ToLocation");
        if (!this.getToL().isDisabled() && locationId == null) {
            this.setErrorLocationId();
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            return false;
        }
        return true;
    }

    private boolean isErrorDisplayed() {
        return this.getErrorWarnInfoMessage() != null && this.getErrorWarnInfoMessage().getValue() != null &&
               !((String) this.getErrorWarnInfoMessage().getValue()).isEmpty();
    }

    private boolean checkContainerId() {
        String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
        if (!this.getCid().isDisabled() && containerId == null) {
            this.setErrorContainerId();
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            return false;
        }
        return true;
    }

    private boolean isContainerValid() {
        return this.getMoveSiphonPageFlowBean().isContainerValid();

    }

    private void setContainerValid(boolean is) {
        this.getMoveSiphonPageFlowBean().setContainerValid(is);
    }

    private boolean isLocationValid() {
        return this.getMoveSiphonPageFlowBean().isLocationValid();

    }

    private void setLocationValid(boolean is) {
        this.getMoveSiphonPageFlowBean().setLocationValid(is);
    }

    public void setValidateContainerLink(RichLink validateContainerLink) {
        this.validateContainerLink = validateContainerLink;
    }

    public RichLink getValidateContainerLink() {
        return validateContainerLink;
    }

    public void setIconContainer(RichIcon iconContainer) {
        this.iconContainer = iconContainer;
    }

    public RichIcon getIconContainer() {
        return iconContainer;
    }

    public void setCid(RichInputText cid) {
        this.cid = cid;
    }

    public RichInputText getCid() {
        return cid;
    }


    public void setLoc(RichInputText loc) {
        this.loc = loc;
    }

    public RichInputText getLoc() {
        return loc;
    }

    public void setIte(RichInputText ite) {
        this.ite = ite;
    }

    public RichInputText getIte() {
        return ite;
    }

    public void setDes(RichInputText des) {
        this.des = des;
    }

    public RichInputText getDes() {
        return des;
    }

    public void setUps(RichInputText ups) {
        this.ups = ups;
    }

    public RichInputText getUps() {
        return ups;
    }

    public void setToL(RichInputText toL) {
        this.toL = toL;
    }

    public RichInputText getToL() {
        return toL;
    }

    public void setQty(RichInputText qty) {
        this.qty = qty;
    }

    public RichInputText getQty() {
        return qty;
    }

    public void setF2DetailLink(RichLink f2DetailLink) {
        this.f2DetailLink = f2DetailLink;
    }

    public RichLink getF2DetailLink() {
        return f2DetailLink;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF6WaveLink(RichLink f6WaveLink) {
        this.f6WaveLink = f6WaveLink;
    }

    public RichLink getF6WaveLink() {
        return f6WaveLink;
    }


    public void setGlobalPgl(RichPanelGroupLayout globalPgl) {
        this.globalPgl = globalPgl;
    }

    public RichPanelGroupLayout getGlobalPgl() {
        return globalPgl;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.errorWarnInfoIcon = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return errorWarnInfoIcon;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.allMessagesPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return allMessagesPanel;
    }


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorWarnInfoMessage = errorMessage;
    }


    public void setFieldsPanel(RichPanelFormLayout fieldsPanel) {
        this.fieldsPanel = fieldsPanel;
    }

    public RichPanelFormLayout getFieldsPanel() {
        return fieldsPanel;
    }
}
