package com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.inventorymgmt.hhsiphonmlps.view.bean.HhSiphonMlpSBean;

import java.util.List;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhMoveSiphonInventorySBean {
    private String moveContainerId; // Item/Parameter hh_move_siphon_inventory_s.MOVE_CONTAINER_ID
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhSiphonMlpSBean.class);
    private String isFocusOn;
    private boolean containerValid;
    private boolean locationValid;


    public void initTaskFlow() {
        _logger.info("initTaskFlow start");
        ADFUtils.findOperation("initTaskFlow").execute();
        this.setIsFocusOn("CONTAINER_ID");
    }

    public void setMoveContainerId(String moveContainerId) {
        this.moveContainerId = moveContainerId;
    }

    public String getMoveContainerId() {
        return moveContainerId;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setLocationValid(boolean locationValid) {
        this.locationValid = locationValid;
    }

    public boolean isLocationValid() {
        return locationValid;
    }

    public void setContainerValid(boolean containerValid) {
        this.containerValid = containerValid;
    }

    public boolean isContainerValid() {
        return containerValid;
    }
}
