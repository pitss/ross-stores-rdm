package com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.inventorymgmt.view.framework.RDMInventoryManagementBackingBean;

import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import org.apache.myfaces.trinidad.event.SelectionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMInventoryManagementBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    static final String FIRST_RECORD = "FIRST RECORD";
    static final String LAST_RECORD = "LAST RECORD";
    private RichLink f3ExitLink;
    private RichTable itemTable;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        this.showFirstLastRecordInfo();
        _logger.info("onRegionLoad End");
    }

    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib = ADFUtils.findIterator("MoveSiphonInventorySContainerItemView1Iterator");
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == trow - 1) {
            this.showMessagesPanel(INFO, LAST_RECORD);
        } else if (crow == 0) {
            this.showMessagesPanel(INFO, FIRST_RECORD);
        } else {
            this.hideMessagesPanel();
        }
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        JSFUtils.resolveMethodExpression("#{bindings.MoveSiphonInventorySContainerItemView1.collectionModel.makeCurrent}",
                                         SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                         selectionEvent });
        this.showFirstLastRecordInfo();
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.getErrorWarnInfoIcon().setName(INF_ICON);
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel() End");
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("hideMessagesPanel() End");
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public Page2Backing() {

    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }


    public void setItemTable(RichTable itemTable) {
        this.itemTable = itemTable;
    }

    public RichTable getItemTable() {
        return itemTable;
    }
}
