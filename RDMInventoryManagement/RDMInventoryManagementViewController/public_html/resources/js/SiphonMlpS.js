function onKeyPressedOnMasterContainerId(event) {
    this.onKeyPressedSiphonMlpPage1(event, 'mas1');
}

function onKeyPressedOnLocationId(event) {
    this.onKeyPressedSiphonMlpPage1(event, 'loc1');
}

function onKeyPressedOnCurItemId(event) {
    this.onKeyPressedSiphonMlpPage1(event, 'ite');
}

function onKeyPressedOnPoNBR(event) {
    this.onKeyPressedSiphonMlpPage1(event, 'poN1');
}

function onKeyPressedOnNewItemId(event) {
    this.onKeyPressedSiphonMlpPage1(event, 'new1');
}

function onKeyPressedSiphonMlpPage1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
        event.cancel();
    }
    else {
        switch (keyPressed) {
            case AdfKeyStroke.F3_KEY:
                this.pressOnLink('f3', component);
                break;
            case AdfKeyStroke.F4_KEY:
                this.pressOnLink('f4', component);
                break;
            case AdfKeyStroke.F9_KEY:
                this.pressOnLink('f9', component);
                break;
            default :
                event.cancel();
                break;
        }
    }
    event.cancel();
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}

function onOpenPopupOnSiphonMlpView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('cnf1');
    linkComp.focus();
}

function OnOpenSiphonLoadPopup(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('lcpsm');
    linkComp.focus();
}

function onKeyPressedOnSiphonMlpViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    switch (keyPressed) {
        case AdfKeyStroke.F1_KEY:
            this.pressOnLink('yesLinkPopup', component);
            break;
        case AdfKeyStroke.F2_KEY:
            this.pressOnLink('noLinkPopup', component);
            break;
        default :
            event.cancel();
            break;
    }
    event.cancel();
}

function setSiphonMLPLpnTableFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function okSiphonMlpLoad(event) {
    if (keyPressed == AdfKeyStroke.F1_KEY)
        AdfActionEvent.queue(component.findComponent('okMlp'), true);
    event.cancel();
}

function onKeyPressedOnSelectItemId(event) {
    this.onKeyPressedSiphonMlpPage2(event, 'tbb');
}

function onKeyPressedSiphonMlpPage2(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    switch (keyPressed) {
        case AdfKeyStroke.F3_KEY:
            AdfActionEvent.queue(component.findComponent('f3'), true);
            event.cancel();
            break;
        case AdfKeyStroke.F4_KEY:
            AdfActionEvent.queue(component.findComponent('f4'), true);
            event.cancel();
            break;
        case AdfKeyStroke.F7_KEY:
            AdfActionEvent.queue(component.findComponent('f7'), true);
            event.cancel();
            break;
        case AdfKeyStroke.F8_KEY:
            AdfActionEvent.queue(component.findComponent('f8'), true);
            event.cancel();
            break;
        default :
            break;
    }
}

function inputOnBlurFocusHanlder(event) {
    SetFocusOnUIcomp(event.getSource());
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}