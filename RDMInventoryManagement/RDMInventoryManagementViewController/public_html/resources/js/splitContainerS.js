// PAGE1 VIEW JS --------------------------------------------------
function onKeyPressedOnFromContainerSplitContainer(event) {
    this.onKeyPressedSplitContainer(event, 'fro1');
}

function onKeyPressedOnToContainerSplitContainer(event) {
    this.onKeyPressedSplitContainer(event, 'toC1');
}

function onKeyPressedOnItemUpcSplitContainer(event) {
    this.onKeyPressedSplitContainer(event, 'ite2');
}

function onKeyPressedOnContainerQtySplitContainer(event) {
    this.onKeyPressedSplitContainer(event, 'con1');
}

function onKeyPressedOnUnitQtySplitContainer(event) {
    this.onKeyPressedSplitContainer(event, 'uni1');
}

function onKeyPressedSplitContainer(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
        true);
    }
    event.cancel();
}

function onKeyPressedSplitContainerPage1PopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}

function onOpenPopupOnSplitContainerPage1(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopup');
    linkComp.focus();
}
// END PAGE1 VIEW JS --------------------------------------------------

// PAGE2 VIEW JS --------------------------------------------------
function onKeyPressedOnReasonCodeSplitContainerPage2(event) {
    this.onKeyPressedSplitContainerPage2(event, 'reaCod');
}

function onKeyPressedOnUnitQtySplitContainerPage2(event) {
    this.onKeyPressedSplitContainerPage2(event, 'uni2');
}

function onKeyPressedOnMaintainSplitContainer(event) {
    this.onKeyPressedSplitContainerPage2(event, 'mai1');
}

function onKeyPressedSplitContainerPage2(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
        true);
    }
    event.cancel();
}
function onKeyPressedOnCodeLovSplitContainer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if(keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY){
        var focusOn = findLovSplitContainerReason();
        if (keyPressed == AdfKeyStroke.F6_KEY) {
            if (focusOn != null) {
                openListOfValues(focusOn);
            }
        }
        else if (keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
            AdfCustomEvent.queue(component, "customKeyEvent", 
            {
                keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
            },
            true);
        }
        event.cancel();
    }
}

function findLovSplitContainerReason() {
    var lovId = 'splitLov';
    var codeLov = ("[id*='" + lovId + "']");
    openListOfValues(codeLov);
    return $(codeLov);    
}

function onOpenSuccessPopupOnSplitContainerPage2(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('okLinkPopup');
    linkComp.focus();
}

function onKeyPressedSplitContainerPage2SuccessPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}