// JS for hHInvalidContainerS module--------------------------------------------------

// PAGE1 VIEW JS --------------------------------------------------
function onKeyPressedOnLpn(event) {
    this.onKeyPressedInvalidContainerPage1(event, 'lpn1');
}

function onKeyPressedOnPoNbr(event) {
    this.onKeyPressedInvalidContainerPage1(event, 'poN');
}

function onKeyPressedOnItemId(event) {
    this.onKeyPressedInvalidContainerPage1(event, 'ite1');
}

function onKeyPressedOnQty1(event) {
    this.onKeyPressedInvalidContainerPage1(event, 'qty1');
}

function onKeyPressedOnLocation(event) {
    this.onKeyPressedInvalidContainerPage1(event, 'loc1');
}

function onKeyPressedInvalidContainerPage1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY 
        || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY 
        || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}



//Page 2 functions
function onKeyPressedInvalidContainerPage2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    switch(keyPressed){
        case AdfKeyStroke.F3_KEY:
            AdfActionEvent.queue(component.findComponent('f3'), true); 
            event.cancel();
            break;
        case AdfKeyStroke.F4_KEY:
            AdfActionEvent.queue(component.findComponent('f4'), true); 
            event.cancel();
            break;
        case AdfKeyStroke.F9_KEY:
            AdfActionEvent.queue(component.findComponent('f9'), true); 
            event.cancel();
            break;
        case AdfKeyStroke.F10_KEY:
            AdfActionEvent.queue(component.findComponent('f10'), true); 
            event.cancel();
            break;
        default:
            break;
    }
}