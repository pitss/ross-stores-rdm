function OnOpenConfirmPopupIV(event) {
    OnOpen(event, 'ylp');
}

function noYesLinkConfirmPopupIV(event) {
    fLinksKeyHandlerIV(event, 'p1')
}

function onBlurInvVerif(event) {
    var elem = (event.getSource().getClientId());
    var comp = document.querySelector('[id="' + elem + '::content"]');
    $(comp).select();
}

function fLinksKeyHandlerIV(event, popupId) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F2_KEY) {
            fLink = component.findComponent('nlp');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);
    }
}

function setInitialFocus() {
    setTimeout(function () {
        var focusOnValue = $("input[id*='focus']").val();
        if ((focusOnValue != null)) {
            switch (focusOnValue) {
                case 'LocIdField':
                    SetFocusOnUIcomp("input[id*='loc1']");
                    break;
                case 'FlueSpcFlagField':
                    SetFocusOnUIcomp("input[id*='flu1']");
                    break;
                case 'MlpQtyField':
                    SetFocusOnUIcomp("input[id*='mlp1']");
                    break;
                case 'CidQtyField':
                    SetFocusOnUIcomp("input[id*='cid1']");
                    break;
                case 'ContainerIdField':
                    SetFocusOnUIcomp("input[id*='con']");
                    break;
                default :
                    break;
            }
        }
    },
0);
}

function onKeyPressedIV(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}