function onKeyPressedOnFromContainerXref(event) {
    this.onKeyPressedXrefContainer(event, 'fro1');
}

function onKeyPressedOnToContainerXref(event) {
    this.onKeyPressedXrefContainer(event, 'toC1');
}

function onKeyPressedXrefContainer(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
        true);
    }
    event.cancel();
}

function onOpenPopupXrefContainer(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopup');
    linkComp.focus();
}

function onKeyPressedOnXrefContainerViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}