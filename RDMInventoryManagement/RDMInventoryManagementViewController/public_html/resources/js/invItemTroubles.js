function setInvItemPage1InitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('con1');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setInvItemPage2InitialFocus() {
    setTimeout(function () {
        var tblTroubleId = 'trbl';
        var tblTroubleCode = ("[id*='" + tblTroubleId + "']");
        if ($(tblTroubleCode).size() > 0)
            $(tblTroubleCode).focus();
    },
0);
}

function onKeyPressedItemTroublesFields(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", {keyPressed : keyPressed, submittedValue : submittedValue }, true);
    }
    else if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
    else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b3'), true);
    }
    else if (keyPressed == AdfKeyStroke.F9_KEY) {
        AdfActionEvent.queue(component.findComponent('b4'), true);
    }
        event.cancel();
}


function inputOnBlurFocusHandler(event) {
    setFocusOrSelectOnUIcomp(event.getSource());
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function onKeyPressedInvItemTroubles(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    
    AdfCustomEvent.queue(component, "performKeyEvent", 
    {
        keyPressed : keyPressed, submittedValue : submittedValue
    },
true);
    event.cancel();
}

function onKeyPressedInvItemTroublesP2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode()
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
    else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b3'), true);
    }
}

function onInvItemTroChangeEvent(event) {
    var component = event.getSource();
    var submittedValue = component.getSubmittedValue();
    if (submittedValue) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : 13, submittedValue : submittedValue
        },
true);
        event.cancel();
    }
    else {
        component.focus();

    }
}