function setRossContInitialFocusP1() {
    setTimeout(function () {
        var focusOn = customFindElementByIdContainer('con1');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setRossContInitialFocusP2() {
    setTimeout(function() {
    var focusOn = customFindElementByIdContainer('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setRossContInitialFocusP3() {
    setTimeout(function () {
        var focusOn = customFindElementByIdContainer('b1');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedRossContInq(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue()

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);
        event.cancel();
    }
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function onKeyPressedRossContInq2(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.ENTER_KEY) {
        event.cancel();
    }
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     
    else if (keyPressed == AdfKeyStroke.F7_KEY) {
        AdfActionEvent.queue(component.findComponent('b3'), true);
    }
        else if (keyPressed == AdfKeyStroke.F8_KEY) {
        AdfActionEvent.queue(component.findComponent('b4'), true);
    }
}

function onKeyPressedOnRossContInqTable(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        if (keyPressed == AdfKeyStroke.F3_KEY) {
            AdfActionEvent.queue(component.findComponent('b1'), true);
        }
        if (keyPressed == AdfKeyStroke.F7_KEY) {
            AdfActionEvent.queue(component.findComponent('b3'), true);
        }
        if (keyPressed == AdfKeyStroke.F8_KEY) {
            AdfActionEvent.queue(component.findComponent('b4'), true);
        }
        if (keyPressed == AdfKeyStroke.F9_KEY) {
            AdfActionEvent.queue(component.findComponent('b5'), true);
        }
        event.cancel();
    }
}

function inputOnBlurFocusHandler(event) {
    setFocusOrSelectOnUIcomp(event.getSource());
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}

function onContainerItemIdChangeEvent(event) {
    var component = event.getSource();
    var submittedValue = component.getSubmittedValue();
    if (submittedValue) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : 13, submittedValue : submittedValue
        },
true);
        event.cancel();
    }
    else {
        component.focus();

    }
}

function customFindElementByIdContainer(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}