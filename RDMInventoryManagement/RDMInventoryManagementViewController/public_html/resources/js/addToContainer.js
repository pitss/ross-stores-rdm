function onKeyPressedAddPage1(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyPressedAddPage2(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onOpenExitPopupAddPage2(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('okLinkPopup');
    linkComp.focus();
}

function onKeyPressedFreightBillConfirmPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component, true);
    }
    event.cancel();
}

function onOpenConfirmPopupAddPage1(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup');
    linkComp.focus();
}

function onKeyPressedConfirmPopupAdd(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenLabeledPopup1AddPage1(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup1');
    linkComp.focus();
}

function setMethodPageAddContInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedOnTableMethodAdd(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        if (keyPressed == AdfKeyStroke.F3_KEY) {
            AdfActionEvent.queue(component.findComponent('f3'), true);
        }
        else if (keyPressed == AdfKeyStroke.F4_KEY) {
            AdfActionEvent.queue(component.findComponent('f4'), true);
        }
        event.cancel();
    }
}