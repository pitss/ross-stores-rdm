function onKeyOnCIdMoveInv(event) {
    this.onKeyPressedMoveInventorySiphonPage1(event);
}

function onKeyOnTo(event) {
    this.onKeyPressedMoveInventorySiphonPage1(event);
}

function onKeyPressedMoveInventorySiphonPage1(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
    else {
        switch (keyPressed) {
            case AdfKeyStroke.F2_KEY:
                this.pressOnLink('f2', component);
                break;
            case AdfKeyStroke.F3_KEY:
                this.pressOnLink('f3', component);
                break;
            case AdfKeyStroke.F4_KEY:
                this.pressOnLink('f4', component);
                break;
            case AdfKeyStroke.F6_KEY:
                this.pressOnLink('f6', component);
                break;
            default :
                event.cancel();
            break 
        }
        event.cancel();
    }
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}

function onKeyUpTablesSiphonMove(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY)
        AdfActionEvent.queue(component.findComponent('f3'), true);
    event.cancel();
}