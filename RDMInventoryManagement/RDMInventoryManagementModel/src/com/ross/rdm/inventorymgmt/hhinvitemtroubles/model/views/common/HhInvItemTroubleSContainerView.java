package com.ross.rdm.inventorymgmt.hhinvitemtroubles.model.views.common;

import java.util.List;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Mar 14 15:00:53 EDT 2016
// ---------------------------------------------------------------------
public interface HhInvItemTroubleSContainerView extends ViewObject {
    List<String> callValidateContainerId(String pContainerId, String pTroubleCode, String pFacilityId);

    List<String> recordItemTrouble(String pContainerId, String pTroubleCode, String pFacilityId);
}

