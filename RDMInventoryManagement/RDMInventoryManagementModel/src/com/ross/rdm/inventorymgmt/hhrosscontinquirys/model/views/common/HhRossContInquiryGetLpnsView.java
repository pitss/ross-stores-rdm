package com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Mar 30 07:18:16 PDT 2016
// ---------------------------------------------------------------------
public interface HhRossContInquiryGetLpnsView extends ViewObject {
    void executeLpnsQuery(String facilityId, String containerId);
}

