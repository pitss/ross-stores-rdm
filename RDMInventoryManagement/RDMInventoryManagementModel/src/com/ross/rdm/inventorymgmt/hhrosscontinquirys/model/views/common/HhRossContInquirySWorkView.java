package com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Mar 07 12:09:26 PST 2016
// ---------------------------------------------------------------------
public interface HhRossContInquirySWorkView extends ViewObject {
    void setHhRossContInquiryWorkVariables();

    void getMlpHeader(String facilityId, String languageCode);

    void getLpnHeader(String facilityId, String languageCode);
}

