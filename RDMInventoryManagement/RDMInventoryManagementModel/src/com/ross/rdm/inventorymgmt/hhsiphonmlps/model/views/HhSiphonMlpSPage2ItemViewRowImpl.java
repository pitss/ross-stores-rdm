package com.ross.rdm.inventorymgmt.hhsiphonmlps.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Mar 11 10:12:33 CST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhSiphonMlpSPage2ItemViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ItemId;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ITEMID = AttributesEnum.ItemId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhSiphonMlpSPage2ItemViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute ItemId.
     * @return the ItemId
     */
    public String getItemId() {
        return (String) getAttributeInternal(ITEMID);
    }
}

