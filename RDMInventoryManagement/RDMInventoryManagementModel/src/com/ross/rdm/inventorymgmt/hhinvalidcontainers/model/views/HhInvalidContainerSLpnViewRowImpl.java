package com.ross.rdm.inventorymgmt.hhinvalidcontainers.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.sql.Date;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Mar 15 17:11:28 CET 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhInvalidContainerSLpnViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Lpn,
        ItemId,
        Qty,
        Location,
        PoNbr,
        ReceiptDate;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int LPN = AttributesEnum.Lpn.index();
    public static final int ITEMID = AttributesEnum.ItemId.index();
    public static final int QTY = AttributesEnum.Qty.index();
    public static final int LOCATION = AttributesEnum.Location.index();
    public static final int PONBR = AttributesEnum.PoNbr.index();
    public static final int RECEIPTDATE = AttributesEnum.ReceiptDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhInvalidContainerSLpnViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Lpn.
     * @return the Lpn
     */
    public String getLpn() {
        return (String) getAttributeInternal(LPN);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Lpn.
     * @param value value to set the  Lpn
     */
    public void setLpn(String value) {
        setAttributeInternal(LPN, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemId.
     * @return the ItemId
     */
    public String getItemId() {
        return (String) getAttributeInternal(ITEMID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemId.
     * @param value value to set the  ItemId
     */
    public void setItemId(String value) {
        setAttributeInternal(ITEMID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Qty.
     * @return the Qty
     */
    public String getQty() {
        return (String) getAttributeInternal(QTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Qty.
     * @param value value to set the  Qty
     */
    public void setQty(String value) {
        setAttributeInternal(QTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Location.
     * @return the Location
     */
    public String getLocation() {
        return (String) getAttributeInternal(LOCATION);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Location.
     * @param value value to set the  Location
     */
    public void setLocation(String value) {
        setAttributeInternal(LOCATION, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PoNbr.
     * @return the PoNbr
     */
    public String getPoNbr() {
        return (String) getAttributeInternal(PONBR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PoNbr.
     * @param value value to set the  PoNbr
     */
    public void setPoNbr(String value) {
        setAttributeInternal(PONBR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ReceiptDate.
     * @return the ReceiptDate
     */
    public Date getReceiptDate() {
        return (Date) getAttributeInternal(RECEIPTDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ReceiptDate.
     * @param value value to set the  ReceiptDate
     */
    public void setReceiptDate(Date value) {
        setAttributeInternal(RECEIPTDATE, value);
    }
}

