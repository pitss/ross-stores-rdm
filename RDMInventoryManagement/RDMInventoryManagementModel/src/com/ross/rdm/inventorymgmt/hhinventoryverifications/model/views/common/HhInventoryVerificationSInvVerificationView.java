package com.ross.rdm.inventorymgmt.hhinventoryverifications.model.views.common;

import java.math.BigDecimal;

import java.util.List;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue May 03 09:47:58 EDT 2016
// ---------------------------------------------------------------------
public interface HhInventoryVerificationSInvVerificationView extends ViewObject {


    void callPValidateContainer1();

    Integer callValidateChkDigit(String containerId);

    List<String> callPValidateContainer2();

    List<Object> callPExecuteCount();

    List<Object> callPCheckSystemCount(BigDecimal cidQtyIn, BigDecimal mlpQtyIn);
}

