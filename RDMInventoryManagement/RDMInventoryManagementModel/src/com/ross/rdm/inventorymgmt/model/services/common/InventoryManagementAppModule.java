package com.ross.rdm.inventorymgmt.model.services.common;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Mar 07 14:43:03 EST 2016
// ---------------------------------------------------------------------
public interface InventoryManagementAppModule extends ApplicationModule {
    String callDisplayHeader(String facilityId, String langCode, String formName);

    void setGlobalVariablesInvItemTroubleSBean();

    void setGlobalVariablesInvVerification();

    void initXrefContainerS();

    void setGlobalVariablesRossContInquiry();

    void initTaskFlowAddToContainer();

    String gScp(String facility, String name);

    String callCheckScreenOptionPriv(String facilityId, String userId, String formName, String optionName);

    void initSplitContainerS();

    void callDoCommit();
}

