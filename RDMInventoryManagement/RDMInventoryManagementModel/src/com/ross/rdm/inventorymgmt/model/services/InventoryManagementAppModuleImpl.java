package com.ross.rdm.inventorymgmt.model.services;

import com.ross.rdm.common.hhmenuss.model.views.GetLoginDateViewImpl;
import com.ross.rdm.common.model.services.RdmCommonAppModuleImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.model.views.UserMessageViewImpl;
import com.ross.rdm.common.utils.model.base.RDMApplicationModuleImpl;
import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.ContainerViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.HhAddToContainerSContainerViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.HhAddToContainerSMethodViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.HhAddToContainerSPopupEditorViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.HhAddToContainerSWorkLocalViewRowImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.HhAddToContainerSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.HhAddToContainerSWorkViewRowImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.MasterBuildMethodViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.TransshipmentSetupViewImpl;
import com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views.TransshipmentSetupViewRowImpl;
import com.ross.rdm.inventorymgmt.hhinvalidcontainers.model.views.HhInvalidContainerSDetailViewImpl;
import com.ross.rdm.inventorymgmt.hhinvalidcontainers.model.views.HhInvalidContainerSWorkLaborProdViewRowImpl;
import com.ross.rdm.inventorymgmt.hhinvalidcontainers.model.views.HhInvalidContainerSWorkLocalViewImpl;
import com.ross.rdm.inventorymgmt.hhinvalidcontainers.model.views.HhInvalidContainerSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhinvalidcontainers.model.views.HhInvalidContainerSWorkViewRowImpl;
import com.ross.rdm.inventorymgmt.hhinventoryverifications.model.views.HhInventoryVerificationSInvVerificationViewImpl;
import com.ross.rdm.inventorymgmt.hhinventoryverifications.model.views.HhInventoryVerificationSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhinvitemtroubles.model.views.HhInvItemTroubleSItemTroubleCodeViewImpl;
import com.ross.rdm.inventorymgmt.hhinvitemtroubles.model.views.HhInvItemTroubleSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.model.views.HhMoveSiphonInventorySMoveBlockViewImpl;
import com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.model.views.HhMoveSiphonInventorySWorkLaborProdViewImpl;
import com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.model.views.HhMoveSiphonInventorySWorkLocalViewImpl;
import com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.model.views.HhMoveSiphonInventorySWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.model.views.MoveSiphonInventorySContainerItemViewImpl;
import com.ross.rdm.inventorymgmt.hhmovesiphoninventorys.model.views.MoveSiphonInventorySWaveInformationViewImpl;
import com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.HhRossContInquiryDisplayLpnsInfoViewImpl;
import com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.HhRossContInquiryGetLpnsUnstagedViewImpl;
import com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.HhRossContInquiryGetLpnsViewImpl;
import com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.HhRossContInquirySPage2ViewImpl;
import com.ross.rdm.inventorymgmt.hhrosscontinquirys.model.views.HhRossContInquirySWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhsiphonmlps.model.views.HhSiphonMlpSMainViewImpl;
import com.ross.rdm.inventorymgmt.hhsiphonmlps.model.views.HhSiphonMlpSPage2ItemViewImpl;
import com.ross.rdm.inventorymgmt.hhsiphonmlps.model.views.HhSiphonMlpSWorkLocalViewImpl;
import com.ross.rdm.inventorymgmt.hhsiphonmlps.model.views.HhSiphonMlpSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhsplitcontainers.model.views.HhSplitContainerSWorkLocalViewRowImpl;
import com.ross.rdm.inventorymgmt.hhsplitcontainers.model.views.HhSplitContainerSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhsplitcontainers.model.views.HhSplitContainerSWorkViewRowImpl;
import com.ross.rdm.inventorymgmt.hhxrefcontainers.model.views.HhXrefContainerSWorkLocalViewRowImpl;
import com.ross.rdm.inventorymgmt.hhxrefcontainers.model.views.HhXrefContainerSWorkViewImpl;
import com.ross.rdm.inventorymgmt.hhxrefcontainers.model.views.HhXrefContainerSWorkViewRowImpl;
import com.ross.rdm.inventorymgmt.model.services.common.InventoryManagementAppModule;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;
import oracle.jbo.server.ViewObjectImpl;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jan 27 14:33:08 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class InventoryManagementAppModuleImpl extends RDMApplicationModuleImpl implements InventoryManagementAppModule {
    private static ADFLogger _logger = ADFLogger.createADFLogger(InventoryManagementAppModuleImpl.class);

    @Override
    public void afterConnect() {
        super.afterConnect();
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS",
                                    facilityId, languageCode);      
    }

    /**
     * This is the default constructor (do not remove).
     */
    public InventoryManagementAppModuleImpl() {
    }

    /**
     * Container's getter for HhAddToContainerSWorkView.
     * @return HhAddToContainerSWorkView
     */
    public HhAddToContainerSWorkViewImpl getHhAddToContainerSWorkView() {
        return (HhAddToContainerSWorkViewImpl) findViewObject("HhAddToContainerSWorkView");
    }

    /**
     * Container's getter for HhAddToContainerSContainerView.
     * @return HhAddToContainerSContainerView
     */
    public HhAddToContainerSContainerViewImpl getHhAddToContainerSContainerView() {
        return (HhAddToContainerSContainerViewImpl) findViewObject("HhAddToContainerSContainerView");
    }

    /**
     * Container's getter for HhAddToContainerSPopupEditorView.
     * @return HhAddToContainerSPopupEditorView
     */
    public HhAddToContainerSPopupEditorViewImpl getHhAddToContainerSPopupEditorView() {
        return (HhAddToContainerSPopupEditorViewImpl) findViewObject("HhAddToContainerSPopupEditorView");
    }

    /**
     * Container's getter for HhAddToContainerSMethodView.
     * @return HhAddToContainerSMethodView
     */
    public HhAddToContainerSMethodViewImpl getHhAddToContainerSMethodView() {
        return (HhAddToContainerSMethodViewImpl) findViewObject("HhAddToContainerSMethodView");
    }

    /**
     * Container's getter for HhAddToContainerSWorkLocalView.
     * @return HhAddToContainerSWorkLocalView
     */
    public RDMTransientViewObjectImpl getHhAddToContainerSWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhAddToContainerSWorkLocalView");
    }


    /**
     * Container's getter for HhInvalidContainerSWorkLaborProdView.
     * @return HhInvalidContainerSWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getHhInvalidContainerSWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhInvalidContainerSWorkLaborProdView");
    }

    /**
     * Container's getter for HhInvalidContainerSLpnView.
     * @return HhInvalidContainerSLpnView
     */
    public RDMTransientViewObjectImpl getHhInvalidContainerSLpnView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhInvalidContainerSLpnView");
    }

    /**
     * Container's getter for HhInvalidContainerSDetailView.
     * @return HhInvalidContainerSDetailView
     */
    public HhInvalidContainerSDetailViewImpl getHhInvalidContainerSDetailView() {
        return (HhInvalidContainerSDetailViewImpl) findViewObject("HhInvalidContainerSDetailView");
    }

    /**
     * Container's getter for HhInvalidContainerSWorkView.
     * @return HhInvalidContainerSWorkView
     */
    public HhInvalidContainerSWorkViewImpl getHhInvalidContainerSWorkView() {
        return (HhInvalidContainerSWorkViewImpl) findViewObject("HhInvalidContainerSWorkView");
    }

    /**
     * Container's getter for HhInvalidContainerSWorkLocalView.
     * @return HhInvalidContainerSWorkLocalView
     */
    public HhInvalidContainerSWorkLocalViewImpl getHhInvalidContainerSWorkLocalView() {
        return (HhInvalidContainerSWorkLocalViewImpl) findViewObject("HhInvalidContainerSWorkLocalView");
    }

    /**
     * Container's getter for HhInventoryVerificationSInvVerificationView.
     * @return HhInventoryVerificationSInvVerificationView
     */
    public HhInventoryVerificationSInvVerificationViewImpl getHhInventoryVerificationSInvVerificationView() {
        return (HhInventoryVerificationSInvVerificationViewImpl) findViewObject("HhInventoryVerificationSInvVerificationView");
    }

    /**
     * Container's getter for HhInventoryVerificationSWorkView.
     * @return HhInventoryVerificationSWorkView
     */
    public HhInventoryVerificationSWorkViewImpl getHhInventoryVerificationSWorkView() {
        return (HhInventoryVerificationSWorkViewImpl) findViewObject("HhInventoryVerificationSWorkView");
    }

    /**
     * Container's getter for HhInventoryVerificationSBoilerView.
     * @return HhInventoryVerificationSBoilerView
     */
    public RDMTransientViewObjectImpl getHhInventoryVerificationSBoilerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhInventoryVerificationSBoilerView");
    }

    /**
     * Container's getter for HhInvItemTroubleSContainerView.
     * @return HhInvItemTroubleSContainerView
     */
    public ViewObjectImpl getHhInvItemTroubleSContainerView() {
        return (ViewObjectImpl) findViewObject("HhInvItemTroubleSContainerView");
    }


    /**
     * Container's getter for HhInvItemTroubleSWorkView.
     * @return HhInvItemTroubleSWorkView
     */
    public HhInvItemTroubleSWorkViewImpl getHhInvItemTroubleSWorkView() {
        return (HhInvItemTroubleSWorkViewImpl) findViewObject("HhInvItemTroubleSWorkView");
    }

    /**
     * Container's getter for HhInvItemTroubleSWorkLocalView.
     * @return HhInvItemTroubleSWorkLocalView
     */
    public RDMTransientViewObjectImpl getHhInvItemTroubleSWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhInvItemTroubleSWorkLocalView");
    }


    /**
     * Container's getter for HhMoveSiphonInventorySWorkLaborProdView.
     * @return HhMoveSiphonInventorySWorkLaborProdView
     */
    public HhMoveSiphonInventorySWorkLaborProdViewImpl getHhMoveSiphonInventorySWorkLaborProdView() {
        return (HhMoveSiphonInventorySWorkLaborProdViewImpl) findViewObject("HhMoveSiphonInventorySWorkLaborProdView");
    }

    /**
     * Container's getter for HhMoveSiphonInventorySMoveBlockView.
     * @return HhMoveSiphonInventorySMoveBlockView
     */
    public HhMoveSiphonInventorySMoveBlockViewImpl getHhMoveSiphonInventorySMoveBlockView() {
        return (HhMoveSiphonInventorySMoveBlockViewImpl) findViewObject("HhMoveSiphonInventorySMoveBlockView");
    }


    /**
     * Container's getter for HhMoveSiphonInventorySWorkView.
     * @return HhMoveSiphonInventorySWorkView
     */
    public HhMoveSiphonInventorySWorkViewImpl getHhMoveSiphonInventorySWorkView() {
        return (HhMoveSiphonInventorySWorkViewImpl) findViewObject("HhMoveSiphonInventorySWorkView");
    }

    /**
     * Container's getter for HhMoveSiphonInventorySWorkLocalView.
     * @return HhMoveSiphonInventorySWorkLocalView
     */
    public HhMoveSiphonInventorySWorkLocalViewImpl getHhMoveSiphonInventorySWorkLocalView() {
        return (HhMoveSiphonInventorySWorkLocalViewImpl) findViewObject("HhMoveSiphonInventorySWorkLocalView");
    }


    /**
     * Container's getter for HhMoveSiphonInventorySWaveInformationView.
     * @return HhMoveSiphonInventorySWaveInformationView
     */
    public ViewObjectImpl getHhMoveSiphonInventorySWaveInformationView() {
        return (ViewObjectImpl) findViewObject("HhMoveSiphonInventorySWaveInformationView");
    }

    /**
     * Container's getter for HhRossBuildContainerSWorkView.
     * @return HhRossBuildContainerSWorkView
     */
    public RDMTransientViewObjectImpl getHhRossBuildContainerSWorkView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossBuildContainerSWorkView");
    }

    /**
     * Container's getter for HhRossBuildContainerSContainerView.
     * @return HhRossBuildContainerSContainerView
     */
    public RDMTransientViewObjectImpl getHhRossBuildContainerSContainerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossBuildContainerSContainerView");
    }

    /**
     * Container's getter for HhRossBuildContainerSMethodView.
     * @return HhRossBuildContainerSMethodView
     */
    public RDMTransientViewObjectImpl getHhRossBuildContainerSMethodView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossBuildContainerSMethodView");
    }

    /**
     * Container's getter for HhRossBuildContainerSWorkLocalView.
     * @return HhRossBuildContainerSWorkLocalView
     */
    public RDMTransientViewObjectImpl getHhRossBuildContainerSWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossBuildContainerSWorkLocalView");
    }

    /**
     * Container's getter for HhRossBuildContainerSBoilerView.
     * @return HhRossBuildContainerSBoilerView
     */
    public RDMTransientViewObjectImpl getHhRossBuildContainerSBoilerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossBuildContainerSBoilerView");
    }

    /**
     * Container's getter for HhRossBuildContainerSBoilerMethodView.
     * @return HhRossBuildContainerSBoilerMethodView
     */
    public RDMTransientViewObjectImpl getHhRossBuildContainerSBoilerMethodView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossBuildContainerSBoilerMethodView");
    }

    /**
     * Container's getter for HhRossContInquirySWorkView.
     * @return HhRossContInquirySWorkView
     */
    public HhRossContInquirySWorkViewImpl getHhRossContInquirySWorkView() {
        return (HhRossContInquirySWorkViewImpl) findViewObject("HhRossContInquirySWorkView");
    }

    /**
     * Container's getter for HhRossContInquirySMainView.
     * @return HhRossContInquirySMainView
     */
    public RDMTransientViewObjectImpl getHhRossContInquirySMainView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossContInquirySMainView");
    }

    /**
     * Container's getter for HhRossContInquirySPage2TopView.
     * @return HhRossContInquirySPage2TopView
     */
    public RDMTransientViewObjectImpl getHhRossContInquirySPage2TopView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossContInquirySPage2TopView");
    }

    /**
     * Container's getter for HhRossContInquirySPage2View.
     * @return HhRossContInquirySPage2View
     */
    public HhRossContInquirySPage2ViewImpl getHhRossContInquirySPage2View() {
        return (HhRossContInquirySPage2ViewImpl) findViewObject("HhRossContInquirySPage2View");
    }

    /**
     * Container's getter for HhRossContInquirySPage3View.
     * @return HhRossContInquirySPage3View
     */
    public RDMTransientViewObjectImpl getHhRossContInquirySPage3View() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossContInquirySPage3View");
    }

    /**
     * Container's getter for HhRossContInquirySBoilerView.
     * @return HhRossContInquirySBoilerView
     */
    public RDMTransientViewObjectImpl getHhRossContInquirySBoilerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossContInquirySBoilerView");
    }

    /**
     * Container's getter for HhRossContInquirySBoilerPage2View.
     * @return HhRossContInquirySBoilerPage2View
     */
    public RDMTransientViewObjectImpl getHhRossContInquirySBoilerPage2View() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossContInquirySBoilerPage2View");
    }

    /**
     * Container's getter for HhRossContInquirySBoilerPage3View.
     * @return HhRossContInquirySBoilerPage3View
     */
    public RDMTransientViewObjectImpl getHhRossContInquirySBoilerPage3View() {
        return (RDMTransientViewObjectImpl) findViewObject("HhRossContInquirySBoilerPage3View");
    }

    /**
     * Container's getter for HhSiphonMlpSWorkView.
     * @return HhSiphonMlpSWorkView
     */
    public HhSiphonMlpSWorkViewImpl getHhSiphonMlpSWorkView() {
        return (HhSiphonMlpSWorkViewImpl) findViewObject("HhSiphonMlpSWorkView");
    }

    /**
     * Container's getter for HhSiphonMlpSMainView.
     * @return HhSiphonMlpSMainView
     */
    public HhSiphonMlpSMainViewImpl getHhSiphonMlpSMainView() {
        return (HhSiphonMlpSMainViewImpl) findViewObject("HhSiphonMlpSMainView");
    }


    /**
     * Container's getter for HhSiphonMlpSWorkLocalView.
     * @return HhSiphonMlpSWorkLocalView
     */
    public HhSiphonMlpSWorkLocalViewImpl getHhSiphonMlpSWorkLocalView() {
        return (HhSiphonMlpSWorkLocalViewImpl) findViewObject("HhSiphonMlpSWorkLocalView");
    }

    /**
     * Container's getter for HhXrefContainerSSplitContainerView.
     * @return HhXrefContainerSSplitContainerView
     */
    public RDMTransientViewObjectImpl getHhXrefContainerSSplitContainerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhXrefContainerSSplitContainerView");
    }

    /**
     * Container's getter for HhXrefContainerSWorkView.
     * @return HhXrefContainerSWorkView
     */
    public HhXrefContainerSWorkViewImpl getHhXrefContainerSWorkView() {
        return (HhXrefContainerSWorkViewImpl) findViewObject("HhXrefContainerSWorkView");
    }

    /**
     * Container's getter for HhXrefContainerSWorkLaborProdView.
     * @return HhXrefContainerSWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getHhXrefContainerSWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhXrefContainerSWorkLaborProdView");
    }

    /**
     * Container's getter for HhXrefContainerSWorkLocalView.
     * @return HhXrefContainerSWorkLocalView
     */
    public RDMTransientViewObjectImpl getHhXrefContainerSWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhXrefContainerSWorkLocalView");
    }

    /**
     * Helper Method to Simplify Invoking Stored Procedures.
     * @param stmt
     * @param bindVars
     */
    public void callStoredProcedure(String stmt, Object... bindVars) {
        DBUtils.callStoredProcedure(getDBTransaction(), stmt, bindVars);
    }

    /**
     * Helper Method to Simplify Invoking Stored Functions.
     * @param sqlReturnType
     * @param stmt
     * @param bindVars
     * @return
     */
    public Object callStoredFunction(int sqlReturnType, String stmt, Object... bindVars) {
        return DBUtils.callStoredFunction(getDBTransaction(), sqlReturnType, stmt, bindVars);
    }

    /**
     * Container's getter for HhSplitContainerSSplitContainerView.
     * @return HhSplitContainerSSplitContainerView
     */
    public RDMTransientViewObjectImpl getHhSplitContainerSSplitContainerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhSplitContainerSSplitContainerView");
    }


    /**
     * Container's getter for HhSplitContainerSReturnReasonView.
     * @return HhSplitContainerSReturnReasonView
     */
    public RDMTransientViewObjectImpl getHhSplitContainerSReturnReasonView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhSplitContainerSReturnReasonView");
    }


    /**
     * Container's getter for HhSplitContainerSWorkView.
     * @return HhSplitContainerSWorkView
     */
    public HhSplitContainerSWorkViewImpl getHhSplitContainerSWorkView() {
        return (HhSplitContainerSWorkViewImpl) findViewObject("HhSplitContainerSWorkView");
    }

    /**
     * Container's getter for HhSplitContainerSWorkLocalView.
     * @return HhSplitContainerSWorkLocalView
     */
    public RDMTransientViewObjectImpl getHhSplitContainerSWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhSplitContainerSWorkLocalView");
    }

    /**
     * Container's getter for HhSplitContainerSWorkLaborProdView.
     * @return HhSplitContainerSWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getHhSplitContainerSWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhSplitContainerSWorkLaborProdView");
    }

    public void setGlobalVariablesInvVerification() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        String userId = (String) currentRow.getAttribute("UserId");
        gVViewImplRowImpl.setGlobalUserId(userId);
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
    }

    /**
     * Container's getter for GlobalVariablesView1.
     * @return GlobalVariablesView1
     */
    public GlobalVariablesViewImpl getGlobalVariablesView() {
        return this.getRdmCommonAppModule().getGlobalVariablesView();
    }

    /**
     * Container's getter for UserMessageView1.
     * @return UserMessageView1
     */
    public UserMessageViewImpl getUserMessageView() {
        return this.getRdmCommonAppModule().getUserMessageView();
    }

    /**
     * Container's getter for GetLoginDateView1.
     * @return GetLoginDateView1
     */
    public GetLoginDateViewImpl getGetLoginDateView() {
        return this.getRdmCommonAppModule().getGetLoginDateView();
    }

    public void callDoCommit() {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_COMMON_ADF.doCommit");
    }

    public String callDisplayHeader(String facilityId, String langCode, String formName) {
        String header =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "PKG_COMMON_ADF.DISPLAY_HEADER",
                                                facilityId, langCode, formName.toUpperCase());
        if (header != null) {
            header = header.replace(facilityId, "").trim();
        }
        return header;
    }

    public void setGlobalVariablesInvItemTroubleSBean() {
        _logger.info("setGlobalVariablesInvItemTroubleSBean() Start");
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        String userId = (String) currentRow.getAttribute("UserId");
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }

        gVViewImplRowImpl.setGlobalUserId(userId);
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("R");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        _logger.info("setGlobalVariablesInvItemTroubleSBean() End");
    }

    /**
     * Java wrapper for the database stored function 'g_scp'
     */
    public String gScp(String facility, String name) {
        return (String) callStoredFunction(Types.VARCHAR, "g_scp", facility, name);
    }

    public boolean containsSeveralMessages(String string) {
        return StringUtils.isNotEmpty(string) && string.contains("#") &&
               StringUtils.isNotEmpty(string.replace("#", ""));
    }

    public List<String> processMessagesParams(SQLOutParam p_v_return, SQLOutParam p_msg_display) {
        List<String> errors = null;
        if (p_v_return.getWrappedData() != null && !"RAISE_FAILURE####".equals(p_v_return.getWrappedData())) {
            errors = new ArrayList<String>();
            if (this.containsSeveralMessages((String) p_v_return.getWrappedData()) &&
                this.containsSeveralMessages((String) p_msg_display.getWrappedData())) {
                String p_types = (String) p_msg_display.getWrappedData();
                String[] types = p_types.split("[#]");
                String p_msgs = (String) p_v_return.getWrappedData();
                String[] messages = p_msgs.split("[#]");
                String message = "";
                for (int i = 0; i < messages.length; i++) {
                    message += messages[i];
                    if (i > 0) {
                        message += ". ";
                    }
                }
                if (types[0] != null) {
                    errors.add(types[0]);
                } else {
                    errors.add("E");
                }
                errors.add(message);
            } else {
                errors.add((String) p_msg_display.getWrappedData());
                if (((String) p_v_return.getWrappedData()).contains("RAISE_FAILURE####")) {
                    String error = (String) p_v_return.getWrappedData();
                    error = error.replace("RAISE_FAILURE####", "");
                    errors.add(error);
                } else {
                    errors.add((String) p_v_return.getWrappedData());
                }
            }
        }
        return errors;
    }

    public void callSetAhlInfo(String facilityId, String formName) {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "AHL.SET_AHL_INFO", facilityId, formName);
    }


    /**
     * Container's getter for HhSiphonMlpSPage2ItemView1.
     * @return HhSiphonMlpSPage2ItemView1
     */
    public HhSiphonMlpSPage2ItemViewImpl getHhSiphonMlpSPage2ItemView() {
        return (HhSiphonMlpSPage2ItemViewImpl) findViewObject("HhSiphonMlpSPage2ItemView");
    }

    private void prepareViewObject(RDMViewObjectImpl vo) {
        if (vo != null) {
            if (vo.getCurrentRow() == null) {
                vo.insertRow(vo.createRow());
            } else {
                vo.getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
            }
        }
    }

    public void initInvalidContainerS() {
        this.prepareViewObject(this.getHhInvalidContainerSLpnView());
        this.prepareViewObject(this.getHhInvalidContainerSWorkLaborProdView());
        this.prepareViewObject(this.getHhInvalidContainerSWorkLocalView());
        this.prepareViewObject(this.getHhInvalidContainerSWorkView());
        this.trStartupInvalidContainer();
        callSetAhlInfo(((HhInvalidContainerSWorkViewRowImpl) this.getHhInvalidContainerSWorkView().getCurrentRow()).getFacilityId(),
                       "hh_invalid_container_s");
    }

    private String hhHeaderFooter(String currentForm, String facilityId) {
        SQLOutParam pHeader = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam pFooter1 = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam pFooter2 = new SQLOutParam("", Types.VARCHAR);

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.HH_HEADER_FOOTER", facilityId,
                                    currentForm, pHeader, pFooter1, pFooter2);

        String header = (String) pHeader.getWrappedData();
        return header.replace(facilityId, "").trim();
    }

    private void trStartupInvalidContainer() {
        GlobalVariablesViewRowImpl global = this.setGlobalVariablesInvalidContainer();
        HhInvalidContainerSWorkViewRowImpl workRow =
            (HhInvalidContainerSWorkViewRowImpl) this.getHhInvalidContainerSWorkView().getCurrentRow();
        workRow.setUser(global.getGlobalUserId());
        workRow.setFacilityId(global.getGlobalFacilityId());
        workRow.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workRow.setDevice(global.getGlobalDevice());
        workRow.setMainBlock("LPN");
        workRow.setLanguageCode(global.getGlobalLanguageCode());
        workRow.setVersionNumber("%I%");
        workRow.setHeader(this.hhHeaderFooter("hh_invalid_container_s", global.getGlobalFacilityId()));

        HhInvalidContainerSWorkLaborProdViewRowImpl workLaborProd =
            (HhInvalidContainerSWorkLaborProdViewRowImpl) this.getHhInvalidContainerSWorkLaborProdView().getCurrentRow();
        workLaborProd.setStartTime(new java.sql.Date(System.currentTimeMillis()));
        workLaborProd.setUnitsProcessed(new BigDecimal(0));
        workLaborProd.setContainersProcessed(new BigDecimal(0));
        workLaborProd.setOperationsPerformed(new BigDecimal(0));
        workLaborProd.setActivityCode("------");
        workLaborProd.setReferenceCode("---------");

        this.getHhInvalidContainerSWorkLocalView().executeF5Logic();
    }

    private GlobalVariablesViewRowImpl setGlobalVariablesInvalidContainer() {

        getRdmCommonAppModule().initCurrentUserIfNeed();

        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        String userId = (String) currentRow.getAttribute("UserId");
        variablesRow.setGlobalUserId(userId);
        variablesRow.setGlobalLanguageCode(languageCode);
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("R");
        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        gVViewImpl.insertRow(variablesRow);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        return variablesRow;
    }


    public void initXrefContainerS() {
        this.prepareViewObject(this.getHhXrefContainerSWorkView());
        this.prepareViewObject(this.getHhXrefContainerSWorkLocalView());
        this.prepareViewObject(this.getHhXrefContainerSWorkLaborProdView());
        this.prepareViewObject(this.getHhXrefContainerSSplitContainerView());

        GlobalVariablesViewRowImpl globalRow = this.setGlobalVariablesXrefContainerS();

        HhXrefContainerSWorkViewRowImpl workRow =
            (HhXrefContainerSWorkViewRowImpl) this.getHhXrefContainerSWorkView().getCurrentRow();

        workRow.setUser(globalRow.getGlobalUserId());
        workRow.setFacilityId(globalRow.getGlobalFacilityId());
        workRow.setLanguageCode(globalRow.getGlobalLanguageCode());

        String header =
            this.callDisplayHeader(globalRow.getGlobalFacilityId(), globalRow.getGlobalLanguageCode(),
                                   "HH_XREF_CONTAINER_S");
        workRow.setHeader(header);

        workRow.setMainBlock("SPLIT_CONTAINER");
        workRow.setVersionNumber("%I%");

        HhXrefContainerSWorkLocalViewRowImpl workLocalRow =
            (HhXrefContainerSWorkLocalViewRowImpl) this.getHhXrefContainerSWorkLocalView().getCurrentRow();

        workLocalRow.setDcDestId(this.gScp(globalRow.getGlobalFacilityId(), "DC_dest_ID"));
        workLocalRow.setCheckDigit(this.gScp(globalRow.getGlobalFacilityId(), "check_digit"));

        callSetAhlInfo(globalRow.getGlobalFacilityId(), "HH_XREF_CONTAINER_S");
    }

    private GlobalVariablesViewRowImpl setGlobalVariablesXrefContainerS() {
        getRdmCommonAppModule().initCurrentUserIfNeed();

        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        String userId = (String) currentRow.getAttribute("UserId");
        variablesRow.setGlobalUserId(userId);
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalLanguageCode(languageCode);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("R");
        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        gVViewImpl.insertRow(variablesRow);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        return variablesRow;
    }

    public void initSplitContainerS() {
        this.prepareViewObject(this.getHhSplitContainerSWorkView());
        this.prepareViewObject(this.getHhSplitContainerSWorkLocalView());
        this.prepareViewObject(this.getHhSplitContainerSWorkLaborProdView());
        this.prepareViewObject(this.getHhSplitContainerSSplitContainerView());
        this.prepareViewObject(this.getHhSplitContainerSReturnReasonView());

        GlobalVariablesViewRowImpl globalRow = this.setGlobalVariablesSplitContainerS();

        HhSplitContainerSWorkViewRowImpl workRow =
            (HhSplitContainerSWorkViewRowImpl) this.getHhSplitContainerSWorkView().getCurrentRow();

        workRow.setUser(globalRow.getGlobalUserId());
        workRow.setFacilityId(globalRow.getGlobalFacilityId());
        workRow.setLanguageCode(globalRow.getGlobalLanguageCode());

        String header =
            this.callDisplayHeader(globalRow.getGlobalFacilityId(), globalRow.getGlobalLanguageCode(),
                                   "HH_SPLIT_CONTAINER_S");
        workRow.setHeader(header);
        workRow.setMainBlock("SPLIT_CONTAINER");
        workRow.setVersionNumber("%I%");

        HhSplitContainerSWorkLocalViewRowImpl workLocalRow =
            (HhSplitContainerSWorkLocalViewRowImpl) this.getHhSplitContainerSWorkLocalView().getCurrentRow();

        workLocalRow.setDcDestId(this.gScp(globalRow.getGlobalFacilityId(), "DC_dest_ID"));

//        HhSplitContainerSSplitContainerViewRowImpl splitRow =
//            (HhSplitContainerSSplitContainerViewRowImpl) this.getHhSplitContainerSSplitContainerView().getCurrentRow();
        //splitRow.setContainerQty(new BigDecimal(1));

        callSetAhlInfo(globalRow.getGlobalFacilityId(), "HH_SPLIT_CONTAINER_S");
    }

    private GlobalVariablesViewRowImpl setGlobalVariablesSplitContainerS() {
        getRdmCommonAppModule().initCurrentUserIfNeed();

        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        Row loginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId =
            loginUserRow.getAttribute("FacilityId") != null ? (String) loginUserRow.getAttribute("FacilityId") : "PR";
        String languageCode =
            loginUserRow.getAttribute("LanguageCode") != null ? (String) loginUserRow.getAttribute("LanguageCode") :
            "AM";

        variablesRow.setGlobalUserId((String) loginUserRow.getAttribute("UserId"));
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalLanguageCode(languageCode);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("R");
        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        gVViewImpl.insertRow(variablesRow);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        //in AppModuleImpl.afterConnect()
        return variablesRow;
    }

    public void setGlobalVariablesRossContInquiry() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        String userId = (String) currentRow.getAttribute("UserId");
        gVViewImplRowImpl.setGlobalUserId(userId);
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("R");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
    }

    /**
     * Container's getter for TroubleCodeRgView1.
     * @return TroubleCodeRgView1
     */
    public HhInvItemTroubleSItemTroubleCodeViewImpl getTroubleCodeRgView() {
        return (HhInvItemTroubleSItemTroubleCodeViewImpl) findViewObject("TroubleCodeRgView");
    }

    /**
     * Container's getter for HhInvItemTroubleSItemTroubleCodeView1.
     * @return HhInvItemTroubleSItemTroubleCodeView1
     */
    public HhInvItemTroubleSItemTroubleCodeViewImpl getHhInvItemTroubleSItemTroubleCodeView() {
        return (HhInvItemTroubleSItemTroubleCodeViewImpl) findViewObject("HhInvItemTroubleSItemTroubleCodeView");
    }

    /**
     * Container's getter for HhRossContInquiryGetLpnsUnstagedView1.
     * @return HhRossContInquiryGetLpnsUnstagedView1
     */
    public HhRossContInquiryGetLpnsUnstagedViewImpl getHhRossContInquiryGetLpnsUnstagedView() {
        return (HhRossContInquiryGetLpnsUnstagedViewImpl) findViewObject("HhRossContInquiryGetLpnsUnstagedView");
    }

    /**
     * Container's getter for HhRossContInquiryGetLpnsView1.
     * @return HhRossContInquiryGetLpnsView1
     */
    public HhRossContInquiryGetLpnsViewImpl getHhRossContInquiryGetLpnsView() {
        return (HhRossContInquiryGetLpnsViewImpl) findViewObject("HhRossContInquiryGetLpnsView");
    }

    public void initTaskFlowAddToContainer() {
        this.prepareViewObject(this.getHhAddToContainerSContainerView());
        this.prepareViewObject(this.getHhAddToContainerSMethodView());
        this.prepareViewObject(this.getHhAddToContainerSPopupEditorView());
        this.prepareViewObject(this.getHhAddToContainerSWorkLocalView());
        this.prepareViewObject(this.getHhAddToContainerSWorkView());

        this.trStartupAddToContainer();

        callSetAhlInfo(((HhAddToContainerSWorkViewRowImpl) this.getHhAddToContainerSWorkView().getCurrentRow()).getFacilityId(),
                       "hh_add_to_container_s");
    }

    private void trStartupAddToContainer() {
        GlobalVariablesViewRowImpl global = this.setGlobalVariablesAddToContainer();
        HhAddToContainerSWorkViewRowImpl workRow =
            (HhAddToContainerSWorkViewRowImpl) this.getHhAddToContainerSWorkView().getCurrentRow();
        workRow.setUser(global.getGlobalUserId());
        workRow.setFacilityId(global.getGlobalFacilityId());
        workRow.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workRow.setDevice(global.getGlobalDevice());
        workRow.setMainBlock("CONTAINER");
        workRow.setLanguageCode(global.getGlobalLanguageCode());
        workRow.setHeader(this.callDisplayHeader(workRow.getFacilityId(), workRow.getLanguageCode(),
                                                 "hh_add_to_container_s"));
        workRow.setVersionNumber("%I%");

        facilitySetupInitialize(global);

        HhAddToContainerSWorkLocalViewRowImpl workLocal =
            (HhAddToContainerSWorkLocalViewRowImpl) this.getHhAddToContainerSWorkLocalView().getCurrentRow();
        workLocal.setContDestId(null);
        workLocal.setConsolidatePendWip(gScp(workRow.getFacilityId(), "consolidate_pend_wip"));
        workLocal.setCheckDigit(gScp(workRow.getFacilityId(), "check_digit"));
    }

    private GlobalVariablesViewRowImpl setGlobalVariablesAddToContainer() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        Row user = this.getGetLoginDateView().getCurrentRow();
        String facility = (String) user.getAttribute("FacilityId");
        String lang = (String) user.getAttribute("LanguageCode");
        String userId = (String) user.getAttribute("UserId");
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        variablesRow.setGlobalUserId(userId);
        variablesRow.setGlobalFacilityId(facility);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("R");
        variablesRow.setGlobalLanguageCode(lang);
        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        gVViewImpl.insertRow(variablesRow);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facility, lang);
        return variablesRow;
    }

    public String getUserMessagesByCode(String msgCode, String facilityId, String langCode) {
        UserMessageViewImpl userMessageView = this.getUserMessageView();
        userMessageView.setbindCodeType(msgCode);
        userMessageView.setbindFacilityId(facilityId);
        userMessageView.setbindLangCode(langCode);
        userMessageView.executeQuery();
        Row rowViewKey = this.getUserMessageView().first();
        if (rowViewKey == null) {
            return "???" + msgCode + "???";
        } else {
            return (String) rowViewKey.getAttribute("MessageText");
        }
    }


    /**
     * Container's getter for MasterBuildMethodView1.
     * @return MasterBuildMethodView1
     */
    public MasterBuildMethodViewImpl getMasterBuildMethodView() {
        return (MasterBuildMethodViewImpl) findViewObject("MasterBuildMethodView");
    }

    /**
     * Container's getter for ContainerView1.
     * @return ContainerView1
     */
    public ContainerViewImpl getContainerView() {
        return (ContainerViewImpl) findViewObject("ContainerView");
    }

    /**
     * Container's getter for TransshipmentSetupView1.
     * @return TransshipmentSetupView1
     */
    public TransshipmentSetupViewImpl getTransshipmentSetupView() {
        return (TransshipmentSetupViewImpl) findViewObject("TransshipmentSetupView");
    }

    private void facilitySetupInitialize(GlobalVariablesViewRowImpl global) {
        String facilityId = global.getGlobalFacilityId();
        String dcDestId = gScp(facilityId, "DC_DEST_ID");
        global.setGlobalMldEnabled(gScp(facilityId, "MLD_ENABLED"));
        global.setGlobalDestId(dcDestId);

        getTransshipmentSetupView().setbind_facility(facilityId);
        getTransshipmentSetupView().setbind_dest(dcDestId);
        getTransshipmentSetupView().executeQuery();
        TransshipmentSetupViewRowImpl first = (TransshipmentSetupViewRowImpl) getTransshipmentSetupView().first();
        if (first != null) {
            global.setGlobalFacilityType(first.getFacilityType());
        }
    }

    public String callCheckScreenOptionPriv(String facilityId, String userId, String formName, String optionName) {
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                   "PKG_COMMON_ADF.CHECK_SCREEN_OPTION_PRIV", facilityId, userId,
                                                   formName, optionName);
    }

    /**
     * Container's getter for RdmCommonAppModule1.
     * @return RdmCommonAppModule1
     */
    public RdmCommonAppModuleImpl getRdmCommonAppModule() {
        return (RdmCommonAppModuleImpl) findApplicationModule("RdmCommonAppModule");
    }

    /**
     * Container's getter for BuildMethodLov1.
     * @return BuildMethodLov1
     */
    public RDMViewObjectImpl getBuildMethodLov() {
        return (RDMViewObjectImpl) findViewObject("BuildMethodLov");
    }

    /**
     * Container's getter for HhRossContInquiryDisplayLpnsInfoView1.
     * @return HhRossContInquiryDisplayLpnsInfoView1
     */
    public HhRossContInquiryDisplayLpnsInfoViewImpl getHhRossContInquiryDisplayLpnsInfoView() {
        return (HhRossContInquiryDisplayLpnsInfoViewImpl) findViewObject("HhRossContInquiryDisplayLpnsInfoView");
    }


    /**
     * Container's getter for MoveSiphonInventorySContainerItemView1.
     * @return MoveSiphonInventorySContainerItemView1
     */
    public MoveSiphonInventorySContainerItemViewImpl getMoveSiphonInventorySContainerItemView1() {
        return (MoveSiphonInventorySContainerItemViewImpl) findViewObject("MoveSiphonInventorySContainerItemView1");
    }

    /**
     * Container's getter for MoveSiphonInventorySWaveInformationView1.
     * @return MoveSiphonInventorySWaveInformationView1
     */
    public MoveSiphonInventorySWaveInformationViewImpl getMoveSiphonInventorySWaveInformationView1() {
        return (MoveSiphonInventorySWaveInformationViewImpl) findViewObject("MoveSiphonInventorySWaveInformationView1");
    }
}
