package com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import oracle.jbo.RowSet;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Apr 14 09:40:10 CEST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhAddToContainerSMethodViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CurrentBuildMethod,
        BuildMethod,
        VA_MethodLov;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int CURRENTBUILDMETHOD = AttributesEnum.CurrentBuildMethod.index();
    public static final int BUILDMETHOD = AttributesEnum.BuildMethod.index();
    public static final int VA_METHODLOV = AttributesEnum.VA_MethodLov.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhAddToContainerSMethodViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute CurrentBuildMethod.
     * @return the CurrentBuildMethod
     */
    public String getCurrentBuildMethod() {
        return (String) getAttributeInternal(CURRENTBUILDMETHOD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CurrentBuildMethod.
     * @param value value to set the  CurrentBuildMethod
     */
    public void setCurrentBuildMethod(String value) {
        setAttributeInternal(CURRENTBUILDMETHOD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute BuildMethod.
     * @return the BuildMethod
     */
    public String getBuildMethod() {
        return (String) getAttributeInternal(BUILDMETHOD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute BuildMethod.
     * @param value value to set the  BuildMethod
     */
    public void setBuildMethod(String value) {
        setAttributeInternal(BUILDMETHOD, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> VA_MethodLov.
     */
    public RowSet getVA_MethodLov() {
        return (RowSet) getAttributeInternal(VA_METHODLOV);
    }
}

