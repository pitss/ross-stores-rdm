package com.ross.rdm.inventorymgmt.hhaddtocontainers.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;

import java.math.BigDecimal;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Apr 18 17:44:23 CEST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class TransshipmentSetupViewImpl extends RDMViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public TransshipmentSetupViewImpl() {
    }

    /**
     * Returns the bind variable value for bind_facility.
     * @return bind variable value for bind_facility
     */
    public String getbind_facility() {
        return (String) getNamedWhereClauseParam("bind_facility");
    }

    /**
     * Sets <code>value</code> for bind variable bind_facility.
     * @param value value to bind as bind_facility
     */
    public void setbind_facility(String value) {
        setNamedWhereClauseParam("bind_facility", value);
    }

    /**
     * Returns the bind variable value for bind_dest.
     * @return bind variable value for bind_dest
     */
    public String getbind_dest() {
        return (String) getNamedWhereClauseParam("bind_dest");
    }

    /**
     * Sets <code>value</code> for bind variable bind_dest.
     * @param value value to bind as bind_dest
     */
    public void setbind_dest(String value) {
        setNamedWhereClauseParam("bind_dest", value);
    }
}

