function setTaskQueueInitialFocusP1() {
    setTimeout(function () {
        var focusOn = customFindElementByIdTaskQueue('pequ');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setTaskQueueInitialFocusP2() {
    setTimeout(function () {
        var focusOn = customFindElementByIdContainer('b1');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function customFindElementByIdTaskQueue(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function inputOnBlurFocusHandler(event) {
    setFocusOrSelectOnUIcomp(event.getSource());
}

function onKeyPressedTaskQueue(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue()

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);
        event.cancel();
    }
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     else if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
    
}

function onKeyPressedTaskQueue2(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.ENTER_KEY){
        event.cancel();
    }
    
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     else if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
    else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b3'), true);
    }
}

function OnOpenErrorPopupTaskPage2(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('ylp');
    linkComp.focus();
}

function OnKeyUpErrorPopupTaskPage2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY){
        AdfActionEvent.queue(component.findComponent('ylp'), true);
    }
    event.cancel();
}