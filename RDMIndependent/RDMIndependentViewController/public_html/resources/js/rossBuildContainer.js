function setInitialFocusBC() {
    setTimeout(function () {
        var focusOnValue = $("input[id*='focus']").val();
        if ((focusOnValue != null)) {
            switch (focusOnValue) {
                case 'GenericContainerIdField':
                    SetFocusOnUIcomp("input[id*='gen1']");
                    break;
                case 'LocationIdField':
                    SetFocusOnUIcomp("input[id*='loc1']");
                    break;
                case 'ContainerIdField':
                    SetFocusOnUIcomp("input[id*='con1']");
                    break;
                case 'UpsCodeField':
                    SetFocusOnUIcomp("input[id*='ups1']");
                    break;
                default :
                    break;
            }
        }
    },
0);
}

function OnOpenConfirmPopupRBC(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('ylp');
    linkComp.focus();
}

function OnOpenConfirmationPopupRBC(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yes');
    linkComp.focus();
}

function noYesLinkConfirmPopupRBC(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F2_KEY) {
            fLink = component.findComponent('nlp');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);
    }
}

function OnOpenMultiSkuPopupRBC(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yeslink');
    linkComp.focus();
}

function onKeyPressedMultiSkuPopupRBC(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('yeslink'), true);
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfActionEvent.queue(component.findComponent('nolink'), true);
    }
    event.cancel();
}

function onKeyPressedConfirmationPopupRBC(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('yes'), true);
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfActionEvent.queue(component.findComponent('no'), true);
    }
    event.cancel();
}

function onKeyPressedRBC(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}

function onKeyPressedBmth(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var link = null;
    switch (keyPressed) {
        case AdfKeyStroke.F3_KEY:
            link = 'b2';
            break;
        case AdfKeyStroke.F3_KEY:
            link = 'b3';
            break;
        case AdfKeyStroke.ENTER_KEY:
            this.triggerServeListener(component, keyPressed);
            break;
        case AdfKeyStroke.TAB_KEY:
            this.triggerServeListener(component, keyPressed);
            break;
        default :
            break;
    }

    AdfActionEvent.queue(component.findComponent(link), true);
    event.cancel();
}

function triggerServeListener(component, keyPressed) {
    AdfCustomEvent.queue(component, "customKeyEvent", 
    {
        keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
    },
true);
    event.cancel();

}

function inputOnBlurFocusHandlerRBC(event) {
    SetFocusOnUIcomp(event.getSource());
}