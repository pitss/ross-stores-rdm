package com.ross.rdm.independent.hhrossbuildcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.independent.view.framework.RDMIndependentBackingBean;

import java.util.List;

import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Method.jspx
// ---
// ---------------------------------------------------------------------
public class MethodBacking extends RDMIndependentBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(MethodBacking.class);
    private RichLink f3Link;
    private RichLink f4Link;
    private RichInputText buildMethod;
    private RichIcon buildMethodIcon;

    public MethodBacking() {

    }

    public void onChangedBuildMethod(ValueChangeEvent vce) {
    }


    public String f3ExitAction() {
        return this.trKeyIn("F3");
    }

    public String f4SaveAction() {
        return this.trKeyIn("F4");
    }

    private String trKeyIn(String fkey) {
        this.getHhRossBuildContainerSBeanPageFlowBean().setIsFocusOn("LocationIdField");
        if ("F4".equalsIgnoreCase(fkey))
            return this.callValidateAndSave();
        else if ("F3".equalsIgnoreCase(fkey))
            return "backGlobalHome";
        return "";
    }

    private String callValidateAndSave() {
        String goTo = "backGlobalHome";
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateAndSave");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 3) {
                    this.showErrorPanel((String) codeList.get(1), (String) codeList.get(0));
                    this.setErrorUi(this.getBuildMethodIcon(), this.getBuildMethod());
                    this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);
                    return "";
                } else if (codeList.size() == 1) {
                    this.hideMessagesPanel();
                    return goTo;
                }
            }
        }
        return goTo;
    }

    private void setErrorUi(RichIcon icon, RichInputText field) {
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        this.addErrorStyleToComponent(field);
        this.selectTextOnUIComponent(field);
        this.refreshContentOfUIComponent(field);
    }

    private void showErrorPanel(String errorName, String errorCode) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.getErrorWarnInfoMessage().setValue(errorName);
        if (errorCode.equalsIgnoreCase(INFO)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (errorCode.equalsIgnoreCase(WARN)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (errorCode.equalsIgnoreCase(MSGTYPE_M)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }


    private HhRossBuildContainerSBean getHhRossBuildContainerSBeanPageFlowBean() {
        return ((HhRossBuildContainerSBean) this.getPageFlowBean("HhRossBuildContainerSBean"));
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        this.setFocusOnUIComponent(this.getBuildMethod());
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                RichInputText field = (RichInputText) clientEvent.getComponent();
                String currentFieldId = field.getId();
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedValue)) ||
                    submittedValue.equals(field.getValue())) {
                    if ("it1".equals(currentFieldId)) {
                        this.onChangedBuildMethod(new ValueChangeEvent(this.getBuildMethod(), null, submittedValue));
                    }
                }
            }
        }
    }

    public void setBuildMethod(RichInputText buildMethod) {
        this.buildMethod = buildMethod;
    }

    public RichInputText getBuildMethod() {
        return buildMethod;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }
    public void setBuildMethodIcon(RichIcon buildMethodIcon) {
        this.buildMethodIcon = buildMethodIcon;
    }

    public RichIcon getBuildMethodIcon() {
        return buildMethodIcon;
    }
}
