package com.ross.rdm.independent.hhrossbuildcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.HashMap;
import java.util.Map;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhRossBuildContainerSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhRossBuildContainerSBean.class);
    private String isFocusOn;
    private Integer addLabeledOperCount;
    private Boolean isGenericContainerIdValidated;
    private Boolean isLocationIdValidated;
    private Boolean isContainerIdValidated;
    private Boolean isUpsCodeValidated;
    private Boolean showMldPopup;

    public void initTaskFlow() {
        _logger.info("Build container page initialization begun");
        this.initGlobalVariablesBuildContainer();
        this.initWorkLocalRow();
        this.initWorkVariablesBuildContainer();
        this.initFacilitySetup();
        this.initContainerRow();
        this.initMethodRow();
        this.setIsFocusOn("FOCUS_POPUP");
        this.setShowMldPopup(true);
        _logger.info("Build container page initialization completed");
    }
    
    private void initGlobalVariablesBuildContainer() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesBuildContainer");
        oper.execute();
    }

    private void initWorkVariablesBuildContainer() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setWorkVariablesBuildContainer");
        oper.execute();
    }
    
    private void initFacilitySetup() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callFacilitySetupInitialize");
        oper.execute();
    }
    
    private void initContainerRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertContainerRow");
        oper.execute();
    }
    
    private void initWorkLocalRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertWorkLocalRow");
        oper.execute();
    }
    
    private void initMethodRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertMethodRow");
        oper.execute();
    }
    
    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsGenericContainerIdValidated(Boolean isGenericContainerIdValidated) {
        this.isGenericContainerIdValidated = isGenericContainerIdValidated;
    }

    public Boolean getIsGenericContainerIdValidated() {
        return isGenericContainerIdValidated;
    }

    public void setIsLocationIdValidated(Boolean isLocationIdValidated) {
        this.isLocationIdValidated = isLocationIdValidated;
    }

    public Boolean getIsLocationIdValidated() {
        return isLocationIdValidated;
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsUpsCodeValidated(Boolean isUpsCodeValidated) {
        this.isUpsCodeValidated = isUpsCodeValidated;
    }

    public Boolean getIsUpsCodeValidated() {
        return isUpsCodeValidated;
    }

    public void setShowMldPopup(Boolean showMldPopup) {
        this.showMldPopup = showMldPopup;
    }

    public Boolean getShowMldPopup() {
        return showMldPopup;
    }

   
    public void setAddLabeledOperCount(Integer confirmationLocation) {
        this.addLabeledOperCount = confirmationLocation;
    }

    public Integer getAddLabeledOperCount() {
        return addLabeledOperCount;
    }
}
