package com.ross.rdm.independent.hhrossbuildcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.independent.hhrossbuildcontainers.model.views.HhRossBuildContainerSContainerViewRowImpl;
import com.ross.rdm.independent.hhrossbuildcontainers.model.views.HhRossBuildContainerSWorkViewRowImpl;
import com.ross.rdm.independent.view.framework.RDMIndependentBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMIndependentBackingBean {
    private final static String PAGE_FLOW_BEAN = "HhRossBuildContainerSBean";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String GENERIC_CONTAINER_ID_ATTR = "GenericContainerId";
    private final static String LOCATION_ID_ATTR = "LocationId";
    private RichPanelFormLayout buildContainerPanel;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichInputText genericContainerId;
    private RichInputText locationId;
    private RichInputText containerId;
    private RichInputText upsCode;
    private RichInputText storageCat;
    private RichInputText lpnCount;
    private RichIcon genericContainerIcon;
    private RichIcon locationIdIcon;
    private RichIcon containerIdIcon;
    private RichIcon upsCodeIcon;
    private RichIcon storageCatIcon;
    private RichIcon lpnCountIcon;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichPopup multiSkuPopup;
    private RichOutputText multiSkuDialogOutputtext;
    private RichPopup confirmationPopup;
    private RichOutputText confirmationDialogOutputtext;
    private RichPopup assignDcDestPopup;
    private RichOutputText assignDcDestDialogOutputtext;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    public Page1Backing() {

    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        Boolean showPopup = this.getHhRossBuildContainerSBeanPageFlowBean().getShowMldPopup();
        if (showPopup) {
            this.getMultiSkuDialogOutputtext().setValue(this.getMessage("BUILD_MULTI_SKU", "C",
                                                                        this.getFacilityIdAttrValue(),
                                                                        this.getLangCodeAttrValue()));
            this.getMultiSkuPopup().show(new RichPopup.PopupHints());
        }
    }


    /******************************************************************************************
     *                                  Validators
     ******************************************************************************************/


    public void onChangedGenericContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedGenericContainerId() Start");
        if (!isNavigationExecuted()) {
            this.updateModel(vce);
            this.hideMessagesPanel();
            if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
                String gCid = ((String) vce.getNewValue());
                if (this.callVGenericContainerId21(gCid)) {
                    this.callUpdateLpnCount();
                    this.callKeyNextItem1();
                }

            } else {
                this.showErrorPanel(this.getMessage("PARTIAL_ENTRY", "E", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()), "E");
                this.setErrorUi(this.getGenericContainerIcon(), this.getGenericContainerId());
                this.setFocusGenericContainerId();
                this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);
            }
        }
        _logger.info("onChangedGenericContainerId() End");
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_independent_hhrossbuildcontainers_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    private boolean callVGenericContainerId21(String gCid) {
        _logger.info("callVgcid() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVGenericContainerId21");
        Map map = oper.getParamsMap();
        map.put("genericContainerId", gCid);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 2) {
                    this.showErrorPanel((String) codeList.get(1), (String) codeList.get(0));
                    this.setErrorUi(this.getGenericContainerIcon(), this.getGenericContainerId());
                    this.setFocusGenericContainerId();
                    this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);
                    _logger.info("callVgcid() End");
                    return false;
                } else {
                    BigDecimal builtIns = (BigDecimal) codeList.get(0);
                    ADFUtils.setBoundAttributeValue(GENERIC_CONTAINER_ID_ATTR, gCid);
                    if (builtIns != null && builtIns.equals(new BigDecimal(1))) {
                        this.getGenericContainerIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getGenericContainerIcon());
                        this.hideMessagesPanel();
                        this.removeErrorOfAllFields();
                        this.setFocusLocationId();
                        this.refreshContentOfUIComponent(this.getBuildContainerPanel());
                        this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(true);
                        _logger.info("callVgcid() End");
                    } else if (builtIns != null && builtIns.equals(new BigDecimal(2))) {
                        if (null == ADFUtils.getBoundAttributeValue("ReturnedDestId")) {
                            this.getHhRossBuildContainerSBeanPageFlowBean().setIsFocusOn(null);
                            this.refreshContentOfUIComponent(getGenericContainerId());
                            this.hideMessagesPanel();
                            this.removeErrorOfAllFields();
                            this.getAssignDcDestDialogOutputtext().setValue(this.getMessage("ASSIGN_DC_DEST", "C",
                                                                                            this.getFacilityIdAttrValue(),
                                                                                            this.getLangCodeAttrValue()));
                            _logger.info("callVgcid() End");
                            this.getAssignDcDestPopup().show(new RichPopup.PopupHints());
                        } else if (this.callFGetMarkStatus()) {
                            return this.callVLocation();
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private void callUpdateLpnCount() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callUpdateLpnCount");
        oper.execute();
        this.hideMessagesPanel();
        this.removeErrorOfAllFields();
    }

    private void callKeyNextItem1() {
        this.hideMessagesPanel();
        this.removeErrorOfAllFields();
        OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callKeyNextItem1");
        oper2.execute();
        if (null != oper2.getResult()) {
            if ("Y".equalsIgnoreCase(((String) oper2.getResult()))) {
                _logger.info("callVgcid2() end");
                ADFUtils.invokeAction("Method");
            }
        }
    }


    private boolean callFGetMarkStatus() {
        OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callFGetMarkStatus");
        oper2.execute();
        if (oper2.getErrors().isEmpty()) {
            if (null != oper2.getResult()) {
                List<String> codeList = (List<String>) oper2.getResult();
                if (codeList.get(0).equalsIgnoreCase("Y")) {
                    this.showErrorPanel(getMessage("MARK_MISMATCH"), "E");
                    this.setErrorUi(this.getGenericContainerIcon(), this.getGenericContainerId());
                    this.setFocusGenericContainerId();
                    this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);
                    return false;
                }
            }
        }
        ADFUtils.setBoundAttributeValue("LocationId", ADFUtils.getBoundAttributeValue("ReturnedLocId"));
        ADFUtils.setBoundAttributeValue("ContDestId", ADFUtils.getBoundAttributeValue("ReturnedDestId"));
        ADFUtils.setBoundAttributeValue("ContStatus", ADFUtils.getBoundAttributeValue("ReturnedContStatus"));
        ADFUtils.setBoundAttributeValue("NewContainerFlag", "N");
        return true;
    }

    private boolean callVLocation() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVLocation");
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if ("Y".equalsIgnoreCase(codeList.get(0))) {
                    this.setFocusContainerId();
                    return this.callVGenericContainerId22();
                } else {
                    this.showErrorPanel(codeList.get(1), codeList.get(0));
                    ADFUtils.setBoundAttributeValue("LocationId", null);
                    this.setErrorUi(this.getGenericContainerIcon(), this.getGenericContainerId());
                    this.setFocusGenericContainerId();
                    this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);

                }
            }
        }
        return false;
    }


    private boolean callVGenericContainerId22() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVGenericContainerId22");
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            List<String> codeList = (List<String>) oper.getResult();
            if (null != codeList && codeList.size() > 1) {
                this.showErrorPanel(codeList.get(1), codeList.get(0));
                this.setErrorUi(this.getGenericContainerIcon(), this.getGenericContainerId());
                this.setFocusGenericContainerId();
                this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);
                _logger.info("yesLinkConfirmPopupActionListener() End");
                return false;
            }
        }
        return this.callUpdateStorageCategory();
    }

    private boolean callUpdateStorageCategory() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callUpdateStorageCat");
        oper.execute();
        return oper.getErrors() == null || oper.getErrors().isEmpty();
    }

    public void onChangedLocationId(ValueChangeEvent vce) {
        _logger.info("onChangedLocationId() Start");
        if (!isNavigationExecuted()) {
            this.updateModel(vce);
            if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
                String locId = ((String) vce.getNewValue());
                this.callVLocation(locId);
            } else {
                this.showErrorPanel(this.getMessage("INV_LOCATION", "E", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()), "E");
                this.setErrorUi(this.getLocationIdIcon(), this.getLocationId());
                this.setFocusLocationId();
                this.getHhRossBuildContainerSBeanPageFlowBean().setIsLocationIdValidated(false);
            }
        }
        _logger.info("onChangedLocationId() End");
    }

    private void callVLocation(String locId) {
        _logger.info("callVLocation() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVLocation");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if (codeList.size() == 2) {
                    this.showErrorPanel(codeList.get(1), codeList.get(0));
                    this.setErrorUi(this.getLocationIdIcon(), this.getLocationId());
                    this.setFocusLocationId();
                    this.getHhRossBuildContainerSBeanPageFlowBean().setIsLocationIdValidated(false);
                    _logger.info("callVLocation() End");
                    return;
                } else if (codeList.get(0).equalsIgnoreCase("Y")) {
                    ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, locId);
                    this.getLocationIdIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getLocationIdIcon());
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getLocationId());
                    this.refreshContentOfUIComponent(this.getBuildContainerPanel());
                    this.setFocusContainerId();
                    this.getHhRossBuildContainerSBeanPageFlowBean().setIsLocationIdValidated(true);
                    _logger.info("callVLocation() End");
                }
            }
        }
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        if (!isNavigationExecuted()) {
            this.updateModel(vce);
            if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
                this.setAddLabeledOperCount(Integer.valueOf(1));
                this.callVContainerId(null);
            } else {
                this.showErrorPanel(this.getMessage("INV_CONTAINER", "E", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()), "E");
                this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
                this.setFocusContainerId();
                this.getHhRossBuildContainerSBeanPageFlowBean().setIsContainerIdValidated(false);
            }
        }
        _logger.info("onChangedContainerId() End");
    }

    private void callVContainerId(String userChoice) {
        if (this.getAddLabeledOperCount() <= 7) {
            String operName = "callAddLabeledContainer" + this.getAddLabeledOperCount().toString();
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(operName);
            if (userChoice != null)
                oper.getParamsMap().put("userChoice", userChoice);

            oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                List result = (List) oper.getResult();
                if (result != null && result.size() > 0) {
                    if (result.size() >= 2) {
                        String messageType = (String) result.get(0);
                        String message = (String) result.get(1);
                        String popupName = "";
                        if (result.size() == 3)
                            popupName = (String) result.get(2);
                        if ("E".equals(messageType) || "W".equals(messageType)) {
                            this.showErrorPanel(message, messageType);
                            this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
                            this.setFocusContainerId();
                            this.getHhRossBuildContainerSBeanPageFlowBean().setIsContainerIdValidated(false);
                            this.setAddLabeledOperCount(Integer.valueOf(1));
                        } else if ("C".equals(messageType)) {
                            this.showConfirmationPopup(message, popupName);
                        } else {
                            this.removeErrorOfAllFields();
                            this.showErrorPanel(message, messageType);
                            this.setFocusContainerId();
                        }
                    }

                } else {
                    this.setAddLabeledOperCount(this.getAddLabeledOperCount() + Integer.valueOf(1));
                    this.callVContainerId(null);
                }
            }
        }
    }

    public void yesConfirmationPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmationPopup().hide();
        this.getContainerId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getContainerId());
        this.setAddLabeledOperCount(this.getAddLabeledOperCount() + Integer.valueOf(1));
        this.setFocusContainerId();
        this.callVContainerId("Y");
    }

    public void noConfirmationPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmationPopup().hide();
        this.getContainerId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getContainerId());
        this.setAddLabeledOperCount(this.getAddLabeledOperCount() + Integer.valueOf(1));
        this.setFocusContainerId();
        this.callVContainerId("N");
    }

    private void showConfirmationPopup(String msg, String popupName) {
        this.getContainerId().setDisabled(true);
        this.refreshContentOfUIComponent(this.getContainerId());
        this.removeErrorOfAllFields();
        this.hideMessagesPanel();
        this.getConfirmationDialogOutputtext().setValue(msg);
        this.getConfirmationPopup().setLauncherVar(popupName);
        this.getConfirmationPopup().show(new RichPopup.PopupHints());
    }

    /******************************************************************************************
     *                                  TrKeyIn and performKey
     ******************************************************************************************/


    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");

        if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F4".equalsIgnoreCase(key)) {
            this.clearFields();
            this.setFocusGenericContainerId();
            this.refreshContentOfUIComponent(this.getBuildContainerPanel());
            this.hideMessagesPanel();
            this.removeErrorOfAllFields();
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                RichInputText field = (RichInputText) clientEvent.getComponent();
                String currentFieldId = field.getId();
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedValue)) ||
                    submittedValue.equals(field.getValue())) {
                    if ("gen1".equals(currentFieldId)) {
                        this.onChangedGenericContainerId(new ValueChangeEvent(this.getGenericContainerId(), null,
                                                                              submittedValue));
                    } else if ("loc1".equals(currentFieldId)) {
                        this.onChangedLocationId(new ValueChangeEvent(this.getLocationId(), null, submittedValue));
                    } else if ("con1".equals(currentFieldId)) {
                        this.getHhRossBuildContainerSBeanPageFlowBean().setAddLabeledOperCount(1);
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    }
                }
            }
        }
    }


    /******************************************************************************************
     *                                  Popup window methods
     ******************************************************************************************/


    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        _logger.info("yesLinkConfirmPopupActionListener() Start");
        this.getAssignDcDestPopup().hide();
        this.setFocusContainerId();
        this.refreshContentOfUIComponent(this.getBuildContainerPanel());
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("gScp");
        Map map = oper.getParamsMap();
        map.put("facility", ADFUtils.getBoundAttributeValue("FacilityId"));
        map.put("name", "DC_dest_ID");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                BigDecimal rdi = new BigDecimal((String) oper.getResult());
                ADFUtils.setBoundAttributeValue("ReturnedDestId", rdi);
            }
        }
        this.callFGetMarkStatus();
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getAssignDcDestPopup().hide();
        this.showErrorPanel(getMessage("INV_DEST_ID"), "W");
        this.setErrorUi(this.getGenericContainerIcon(), this.getGenericContainerId());
        this.setFocusGenericContainerId();
        this.getHhRossBuildContainerSBeanPageFlowBean().setIsGenericContainerIdValidated(false);
        this.getHhRossBuildContainerSBeanPageFlowBean().setAddLabeledOperCount(1);
    }


    public void yesMultiSkuPopupActionListener(ActionEvent actionEvent) {
        _logger.info("yesLinkConfirmPopupActionListener() Start");
        this.setFocusGenericContainerId();
        this.getMultiSkuPopup().hide();
        ADFUtils.setBoundAttributeValue("SingleSku16Flag", "N");
        ADFUtils.setBoundAttributeValue("SingleSku12Flag", "Y");
        this.getHhRossBuildContainerSBeanPageFlowBean().setShowMldPopup(false);
    }

    public void noMultiSkuPopupActionListener(ActionEvent actionEvent) {
        this.setFocusGenericContainerId();
        ADFUtils.setBoundAttributeValue(GENERIC_CONTAINER_ID_ATTR, null);
        this.getMultiSkuPopup().hide();
        ADFUtils.setBoundAttributeValue("SingleSku16Flag", "Y");
        ADFUtils.setBoundAttributeValue("SingleSku12Flag", "N");
        this.getHhRossBuildContainerSBeanPageFlowBean().setShowMldPopup(false);
    }


    /******************************************************************************************
     *                                  Utility methods
     ******************************************************************************************/


    private void setErrorUi(RichIcon icon, RichInputText field) {
        icon.setName("error");
        this.refreshContentOfUIComponent(icon);
        this.addErrorStyleToComponent(field);
        this.selectTextOnUIComponent(field);
        this.refreshContentOfUIComponent(field);
    }

    private void showErrorPanel(String errorName, String errorCode) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.getErrorWarnInfoMessage().setValue(errorName);
        if (errorCode.equalsIgnoreCase("I")) {
            this.getErrorWarnInfoIcon().setName("info");
        } else if (errorCode.equalsIgnoreCase("W")) {
            this.getErrorWarnInfoIcon().setName("warning");
        } else if (errorCode.equalsIgnoreCase("M")) {
            this.getErrorWarnInfoIcon().setName("logo");
        } else {
            this.getErrorWarnInfoIcon().setName("error");
        }
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }


    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void setFocusGenericContainerId() {
        this.getHhRossBuildContainerSBeanPageFlowBean().setIsFocusOn("GenericContainerIdField");
        this.setFocusOnUIComponent(this.getGenericContainerId());
    }

    private void setFocusLocationId() {
        this.getHhRossBuildContainerSBeanPageFlowBean().setIsFocusOn("LocationIdField");
        this.setFocusOnUIComponent(this.getLocationId());
    }

    private void setFocusContainerId() {
        this.getHhRossBuildContainerSBeanPageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusUpsCode() {
        this.getHhRossBuildContainerSBeanPageFlowBean().setIsFocusOn("UpsCodeField");
        this.setFocusOnUIComponent(this.getUpsCode());
    }

    private HhRossBuildContainerSContainerViewRowImpl getContainerViewCurrentRow() {
        return (HhRossBuildContainerSContainerViewRowImpl) ADFUtils.findIterator("HhRossBuildContainerSContainerViewIterator").getCurrentRow();
    }

    private HhRossBuildContainerSWorkViewRowImpl getWorkViewCurrentRow() {
        return (HhRossBuildContainerSWorkViewRowImpl) ADFUtils.findIterator("HhRossBuildContainerSWorkViewIterator").getCurrentRow();
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_ross_build_container_s_CONTAINER_F4,
                                                FORM_NAME_hh_ross_build_container_s)) {
            this.trKeyIn("F4");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    private void clearFields() {
        _logger.info("clearFields() Start");
        HhRossBuildContainerSContainerViewRowImpl hhRowImpl = this.getContainerViewCurrentRow();
        HhRossBuildContainerSWorkViewRowImpl hhWorkRowImpl = this.getWorkViewCurrentRow();
        hhRowImpl.setGenericContainerId(null);
        hhRowImpl.setLocationId(null);
        hhRowImpl.setContainerId(null);
        hhRowImpl.setUpsCode(null);
        hhWorkRowImpl.setStorageCat(null);
        this.getGenericContainerId().resetValue();
        this.getLocationId().resetValue();
        this.getContainerId().resetValue();
        this.getUpsCode().resetValue();
        this.getStorageCat().resetValue();
        this.refreshContentOfUIComponent(this.getGenericContainerId());
        this.refreshContentOfUIComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(this.getUpsCode());
        this.refreshContentOfUIComponent(this.getStorageCat());
        this.refreshContentOfUIComponent(this.getLpnCount());
        _logger.info("clearFields() End");
    }


    /******************************************************************************************
     *                                  Getters and Setters
     ******************************************************************************************/


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setGenericContainerId(RichInputText genericContainerId) {
        this.genericContainerId = genericContainerId;
    }

    public RichInputText getGenericContainerId() {
        return genericContainerId;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setUpsCode(RichInputText upsCode) {
        this.upsCode = upsCode;
    }

    public RichInputText getUpsCode() {
        return upsCode;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setGenericContainerIcon(RichIcon genericContainerIcon) {
        this.genericContainerIcon = genericContainerIcon;
    }

    public RichIcon getGenericContainerIcon() {
        return genericContainerIcon;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setUpsCodeIcon(RichIcon upsCodeIcon) {
        this.upsCodeIcon = upsCodeIcon;
    }

    public RichIcon getUpsCodeIcon() {
        return upsCodeIcon;
    }

    private HhRossBuildContainerSBean getHhRossBuildContainerSBeanPageFlowBean() {
        return ((HhRossBuildContainerSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }

    private void setAddLabeledOperCount(Integer count) {
        if (null != this.getHhRossBuildContainerSBeanPageFlowBean())
            this.getHhRossBuildContainerSBeanPageFlowBean().setAddLabeledOperCount(count);
    }

    private Integer getAddLabeledOperCount() {
        return this.getHhRossBuildContainerSBeanPageFlowBean().getAddLabeledOperCount();
    }

    public void setBuildContainerPanel(RichPanelFormLayout buildContainerPanel) {
        this.buildContainerPanel = buildContainerPanel;
    }

    public RichPanelFormLayout getBuildContainerPanel() {
        return buildContainerPanel;
    }

    public void setAssignDcDestPopup(RichPopup assignDcDestPopup) {
        this.assignDcDestPopup = assignDcDestPopup;
    }

    public RichPopup getAssignDcDestPopup() {
        return assignDcDestPopup;
    }

    public void setAssignDcDestDialogOutputtext(RichOutputText assignDcDestDialogOutputtext) {
        this.assignDcDestDialogOutputtext = assignDcDestDialogOutputtext;
    }

    public RichOutputText getAssignDcDestDialogOutputtext() {
        return assignDcDestDialogOutputtext;
    }

    public void setMultiSkuPopup(RichPopup multiSkuPopup) {
        this.multiSkuPopup = multiSkuPopup;
    }

    public RichPopup getMultiSkuPopup() {
        return multiSkuPopup;
    }

    public void setMultiSkuDialogOutputtext(RichOutputText multiSkuDialogOutputtext) {
        this.multiSkuDialogOutputtext = multiSkuDialogOutputtext;
    }

    public RichOutputText getMultiSkuDialogOutputtext() {
        return multiSkuDialogOutputtext;
    }

    private void removeErrorOfAllFields() {
        this.removeErrorStyleToComponent(getGenericContainerId());
        this.removeErrorStyleToComponent(getLocationId());
        this.removeErrorStyleToComponent(getContainerId());
        this.removeErrorStyleToComponent(getUpsCode());
        this.removeErrorStyleToComponent(getStorageCat());
        this.removeErrorStyleToComponent(getLpnCount());
        this.getGenericContainerIcon().setName("required");
        this.getLocationIdIcon().setName("required");
        this.getContainerIdIcon().setName("required");
        this.refreshContentOfUIComponent(getGenericContainerIcon());
        this.refreshContentOfUIComponent(getLocationIdIcon());
        this.refreshContentOfUIComponent(getContainerIdIcon());
    }

    public String getMessage(String messageCode) {
        return this.getMessage(messageCode, "E", this.getFacilityIdAttrValue(), this.getLangCodeAttrValue());
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setConfirmationDialogOutputtext(RichOutputText confirmationDialogOutputtext) {
        this.confirmationDialogOutputtext = confirmationDialogOutputtext;
    }

    public RichOutputText getConfirmationDialogOutputtext() {
        return confirmationDialogOutputtext;
    }

    public void setStorageCat(RichInputText storageCat) {
        this.storageCat = storageCat;
    }

    public RichInputText getStorageCat() {
        return storageCat;
    }

    public void setLpnCount(RichInputText lpnCount) {
        this.lpnCount = lpnCount;
    }

    public RichInputText getLpnCount() {
        return lpnCount;
    }

    public void setStorageCatIcon(RichIcon storageCatIcon) {
        this.storageCatIcon = storageCatIcon;
    }

    public RichIcon getStorageCatIcon() {
        return storageCatIcon;
    }

    public void setLpnCountIcon(RichIcon lpnCountIcon) {
        this.lpnCountIcon = lpnCountIcon;
    }

    public RichIcon getLpnCountIcon() {
        return lpnCountIcon;
    }
}
