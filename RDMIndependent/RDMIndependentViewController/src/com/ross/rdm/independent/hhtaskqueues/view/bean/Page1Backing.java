package com.ross.rdm.independent.hhtaskqueues.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.independent.view.framework.RDMIndependentBackingBean;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMIndependentBackingBean {
    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String EQUIPMENT_TYPE_ATTR = "Equipment";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String LOCATION_ID_ATTR = "Location";
    private final static String USER = "User";

    private final static String FORM_NAME = "HH_TASK_QUEUE_S";
    private final static String HH_TASK_QUEUE_BEAN = "HhTaskQueueSBean";
    private final static String SEARCH_BLOCK = "B_SEARCH";

    private RichIcon equipmentTypeIcon;
    private RichInputText equipmentType;
    private RichIcon locationIcon;
    private RichInputText location;
    private RichPanelFormLayout taskQueuePanel;
    private RichLink f1Link;
    private RichLink f3Link;

    static final String INTRLVG = "INTRLVG";


    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    public Page1Backing() {

    }

    public void onChangedTiEquipmentType(ValueChangeEvent vce) {
        _logger.info("onChangedTiEquipmentType() Start");
        this.updateModel(vce);
        if (null != vce && null != vce.getNewValue()) {
            String enteredEquipmentType = (vce.getNewValue().toString());
            Integer equipmentType;
            if (isInteger(enteredEquipmentType)) {
                equipmentType = Integer.parseInt(enteredEquipmentType);
                this.validateEquipmentType(equipmentType);
            } else {
                this.showErrorPanel(ERROR,
                                    this.getMessage("MUST_BE_NUMERIC", ERROR, this.getFacilityIdAttrValue(),
                                                    this.getLanguageCodeValue()));
                this.getEquipmentTypeIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
                this.refreshContentOfUIComponent(this.getEquipmentType());
                this.addErrorStyleToComponent(this.getEquipmentType());
                this.selectTextOnUIComponent(this.getEquipmentType());
                this.getHhTaskQueueSPageFlowBean().setIsEquipmentTypeValidated(false);
            }
        } else {
            setFocusLocation();
        }
    }

    public static boolean isInteger(String s) {
        return isInteger(s, 10);
    }

    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty())
            return false;
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '_') {
                if (s.length() == 1)
                    return false;
                else
                    continue;
            }
            if (Character.digit(s.charAt(i), radix) < 0)
                return false;
        }
        return true;
    }

    public void onChangedLocation(ValueChangeEvent vce) {
        _logger.info("onChangedLocation() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue()) {
            String enteredLocation = (vce.getNewValue().toString());
            this.validateLocation(enteredLocation);
        } else {
            setFocusEquipment();
        }
    }

    public boolean validateEquipmentType(Integer equipmentType) {
        _logger.info("validateContainerId() Start");
        if (!this.getEquipmentType().isDisabled()) {
            this.getHhTaskQueueSPageFlowBean().setIsError(false);
            this.setErrorStyleClass(this.getEquipmentType(), false);

            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("pEquipTypeExists");
            Map map = oper.getParamsMap();
            map.put("facilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));
            map.put("ti_equipment_type", equipmentType);
            oper.execute();

            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    List<String> errorCodeList = (List<String>) oper.getResult();

                    this.getEquipmentTypeIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
                    this.refreshContentOfUIComponent(this.getEquipmentType());
                    this.addErrorStyleToComponent(this.getEquipmentType());
                    this.selectTextOnUIComponent(this.getEquipmentType());
                    this.getHhTaskQueueSPageFlowBean().setIsEquipmentTypeValidated(false);
                    this.showErrorPanel(ERROR,
                                        this.getMessage(errorCodeList.get(0), ERROR, this.getFacilityIdAttrValue(),
                                                        this.getLanguageCodeValue()));
                    _logger.info("onChangedContainerId() End");
                    return false;
                } else {
                    ADFUtils.setBoundAttributeValue(EQUIPMENT_TYPE_ATTR, equipmentType);
                    this.getEquipmentTypeIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getEquipmentType());
                    this.refreshContentOfUIComponent(this.getTaskQueuePanel());

                    _logger.info("onChangedContainerId() End");

                }
            }
        }
        this.getHhTaskQueueSPageFlowBean().setIsEquipmentTypeValidated(true);
        _logger.info("validatedContainerId() End");
        return true;
    }

    public boolean validateLocation(String location) {
        if (!this.location.isDisabled())
            _logger.info("validateContainerId() Start");
        this.getHhTaskQueueSPageFlowBean().setIsError(false);
        this.setErrorStyleClass(this.getLocation(), false);
        ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, location);
        this.hideMessagesPanel();
        this.removeErrorStyleToComponent(this.getLocation());
        this.refreshContentOfUIComponent(this.getTaskQueuePanel());

        this.getHhTaskQueueSPageFlowBean().setIsLocationValidated(true);
        _logger.info("validatedContainerId() End");
        return true;
    }

    private void showErrorPanel(String messageType, String msg) {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }

        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("equ")) {
                this.getEquipmentTypeIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
            } else if (field.getId().equalsIgnoreCase("loc")) {
                this.getLocationIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getLocationIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("equ")) {
                this.getEquipmentTypeIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
            } else if (field.getId().equalsIgnoreCase("loc")) {
                this.getLocationIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getLocationIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    public void performKey(ClientEvent clientEvent) {

        String currentFocus = this.getHhTaskQueueSPageFlowBean().getIsFocusOn();
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1Link());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {

                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();

            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("Equipment".equals(currentFocus)) {
                    if (submittedValue != null && !submittedValue.isEmpty()) {
                        this.setErrorStyleClass(this.getEquipmentType(), false);
                        this.onChangedTiEquipmentType(new ValueChangeEvent(this.getEquipmentType(), null,
                                                                           submittedValue));
                        this.setErrorStyleClass(this.getLocation(), false);
                        this.removeErrorStyleToComponent(this.getLocation());
                        this.refreshContentOfUIComponent(this.getTaskQueuePanel());
                        if (this.getHhTaskQueueSPageFlowBean().getIsEquipmentTypeValidated())
                            this.setFocusLocation();

                    } else {
                        setFocusLocation();
                        this.hideMessagesPanel();
                        this.setErrorStyleClass(this.getEquipmentType(), false);
                        this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
                        this.refreshContentOfUIComponent(this.getEquipmentType());
                    }
                } else if ("Location".equals(currentFocus)) {
                    if (submittedValue != null && !submittedValue.isEmpty()) {
                        this.setErrorStyleClass(this.getLocation(), false);
                        this.onChangedLocation(new ValueChangeEvent(this.getLocation(), null, submittedValue));
                        if (this.getHhTaskQueueSPageFlowBean().getIsLocationValidated())
                            this.setFocusEquipment();

                    } else {
                        setFocusEquipment();
                        this.hideMessagesPanel();
                        this.setErrorStyleClass(this.getLocation(), false);
                        this.refreshContentOfUIComponent(this.getLocationIcon());
                        this.refreshContentOfUIComponent(this.getLocation());
                    }
                }

            }
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");

        if ("F3".equalsIgnoreCase(key)) {
            ADFUtils.invokeAction("backGlobalHome");
            _logger.info("trKeyIn() End");

        }
        if ("F1".equalsIgnoreCase(key)) {
            if (null == getEquipmentTypeValue()) {
                return;
            }

            if (null == getLocationId()) {
                this.setErrorStyleClass(this.getEquipmentType(), false);
                this.getEquipmentTypeIcon().setName(REQ_ICON);
                this.hideMessagesPanel();
                this.removeErrorStyleToComponent(this.getEquipmentType());
                this.refreshContentOfUIComponent(this.getTaskQueuePanel());
                this.refreshContentOfUIComponent(this.getEquipmentType());
                return;
            } else {
                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("pLocationExists");
                Map map = oper.getParamsMap();
                map.put("facilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));
                map.put("ti_location", getLocationId());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();

                        this.getEquipmentTypeIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getEquipmentType());
                        this.refreshContentOfUIComponent(this.getTaskQueuePanel());

                        this.setFocusLocation();
                        this.setErrorStyleClass(this.getLocation(), true);
                        this.refreshContentOfUIComponent(this.getLocation());
                        this.refreshContentOfUIComponent(this.getTaskQueuePanel());
                        this.refreshContentOfUIComponent(this.getEquipmentType());
                        this.addErrorStyleToComponent(this.getLocation());
                        this.selectTextOnUIComponent(this.getLocation());
                        this.showErrorPanel(ERROR,
                                            this.getMessage(errorCodeList.get(0), ERROR, this.getFacilityIdAttrValue(),
                                                            this.getLanguageCodeValue()));

                        return;
                    }

                    if (!this.getHhTaskQueueSPageFlowBean().getIsEquipmentTypeValidated()) {
                        this.getEquipmentTypeIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getEquipmentTypeIcon());
                        this.refreshContentOfUIComponent(this.getEquipmentType());
                        this.addErrorStyleToComponent(this.getEquipmentType());
                        this.selectTextOnUIComponent(this.getEquipmentType());
                        this.getHhTaskQueueSPageFlowBean().setIsEquipmentTypeValidated(false);
                        this.showErrorPanel(ERROR,
                                            this.getMessage("INV_EQUIP_TYPE", ERROR, this.getFacilityIdAttrValue(),
                                                            this.getLanguageCodeValue()));
                        this.setFocusEquipment();
                        this.clearEquipment();
                        this.clearLocation();
                        return;
                    }

                    OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("pGetOrderBy");
                    Map map2 = oper2.getParamsMap();
                    map2.put("facilityId", ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR));
                    map2.put("location", getLocationId());
                    map2.put("user", ADFUtils.getBoundAttributeValue(USER));
                    oper2.execute();

                    if (null != oper2.getResult()) {
                        List<String> errorCodeList = (List<String>) oper2.getResult();

                        if ("PROCEDURE_ERROR".equals(errorCodeList.get(0))) {

                            this.showErrorPanel(ERROR,
                                                this.getMessage(errorCodeList.get(0), ERROR,
                                                                this.getFacilityIdAttrValue(),
                                                                this.getLanguageCodeValue()));
                            return;
                        }

                        if ("NO_WH_ID".equals(errorCodeList.get(0))) {
                            this.showErrorPanel(ERROR,
                                                this.getMessage(errorCodeList.get(0), ERROR,
                                                                this.getFacilityIdAttrValue(),
                                                                this.getLanguageCodeValue()));
                            return;
                        }

                        if ("NO_LOCATION_ID".equals(errorCodeList.get(0))) {
                            HhPopulateTaskQueue();
                        }

                    } else {
                        if (!HhTaskQueuePopulateTaskQueue())
                            return;
                    }
                    if (this.getHhTaskQueueSPageFlowBean().getIsEquipmentTypeValidated() &&
                        this.getHhTaskQueueSPageFlowBean().getIsLocationValidated())
                        this.setFocusEquipment();
                    ADFUtils.invokeAction("Page2");
                }
            }
        }
    }

    private boolean HhTaskQueuePopulateTaskQueue() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executePopulateTaskQueueQuery");
        String facilityId = getFacilityIdAttrValue();
        Integer equipmentType = getEquipmentTypeValue();
        String locationId = getLocationId();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("equipment", equipmentType);
        map.put("location", locationId);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> errorCodeList = (List<String>) oper.getResult();
                this.setFocusLocation();
                this.setErrorStyleClass(this.getLocation(), true);
                this.refreshContentOfUIComponent(this.getLocation());
                this.refreshContentOfUIComponent(this.getTaskQueuePanel());
                this.refreshContentOfUIComponent(this.getEquipmentType());
                this.addErrorStyleToComponent(this.getLocation());
                this.selectTextOnUIComponent(this.getLocation());
                this.getHhTaskQueueSPageFlowBean().setIsLocationValidated(false);
                this.showErrorPanel(ERROR,
                                    this.getMessage(errorCodeList.get(0), ERROR, this.getFacilityIdAttrValue(),
                                                    this.getLanguageCodeValue()));
                return false;
            }
        }
        return true;
    }

    private void HhPopulateTaskQueue() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executeTaskQueueViewQuery");
        String facilityId = getFacilityIdAttrValue();
        Integer equipmentType = getEquipmentTypeValue();
        String locationId = getLocationId();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("equipment", equipmentType);
        map.put("location", locationId);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> errorCodeList = (List<String>) oper.getResult();
                this.getHhTaskQueueSPageFlowBean().setIsLocationValidated(false);
                this.setFocusLocation();
                this.setErrorStyleClass(this.getLocation(), true);
                this.refreshContentOfUIComponent(this.getLocation());
                this.refreshContentOfUIComponent(this.getTaskQueuePanel());
                this.refreshContentOfUIComponent(this.getEquipmentType());
                this.addErrorStyleToComponent(this.getLocation());
                this.selectTextOnUIComponent(this.getLocation());
                this.showErrorPanel(ERROR,
                                    this.getMessage(errorCodeList.get(0), ERROR, this.getFacilityIdAttrValue(),
                                                    this.getLanguageCodeValue()));
            }
        }
    }

    private void clearEquipment() {
        _logger.info("clearBlock Start");
        ADFUtils.setBoundAttributeValue(EQUIPMENT_TYPE_ATTR, null);
        _logger.info("clearBlock end");
    }

    private void clearLocation() {
        _logger.info("clearBlock Start");
        ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, null);
        _logger.info("clearBlock end");
    }

    public void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    String getMessage(String msgCode, String messageType) {
        return getMessage(msgCode, messageType, getBoundAttribute(FACILITY_ID_ATTR),
                          getBoundAttribute(LANGUAGE_CODE_ATTR));
    }

    String getBoundAttribute(String attrName) {
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String) val;
        return null;
    }

    private void setFocusEquipment() {
        this.getHhTaskQueueSPageFlowBean().setIsFocusOn("Equipment");
        this.setFocusOnUIComponent(this.getEquipmentType());
    }

    private void setFocusLocation() {
        this.getHhTaskQueueSPageFlowBean().setIsFocusOn("Location");
        this.setFocusOnUIComponent(this.getLocation());
    }

    private HhTaskQueueSBean getHhTaskQueueSPageFlowBean() {
        return ((HhTaskQueueSBean) this.getPageFlowBean(HH_TASK_QUEUE_BEAN));
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLanguageCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private Integer getEquipmentTypeValue() {
        return (Integer) ADFUtils.getBoundAttributeValue(EQUIPMENT_TYPE_ATTR);
    }

    private String getLocationId() {
        return (String) ADFUtils.getBoundAttributeValue(LOCATION_ID_ATTR);
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }


    public void setEquipmentTypeIcon(RichIcon equipmentTypeIcon) {
        this.equipmentTypeIcon = equipmentTypeIcon;
    }

    public RichIcon getEquipmentTypeIcon() {
        return equipmentTypeIcon;
    }

    public void setEquipmentType(RichInputText equipmentType) {
        this.equipmentType = equipmentType;
    }

    public RichInputText getEquipmentType() {
        return equipmentType;
    }


    public void setLocationIcon(RichIcon locationIcon) {
        this.locationIcon = locationIcon;
    }

    public RichIcon getLocationIcon() {
        return locationIcon;
    }

    public void setLocation(RichInputText location) {
        this.location = location;
    }

    public RichInputText getLocation() {
        return location;
    }

    public void setTaskQueuePanel(RichPanelFormLayout taskQueuePanel) {
        this.taskQueuePanel = taskQueuePanel;
    }

    public RichPanelFormLayout getTaskQueuePanel() {
        return taskQueuePanel;
    }

    public void setF1Link(RichLink f1Link) {
        this.f1Link = f1Link;
    }

    public RichLink getF1Link() {
        return f1Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void f1ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_task_queue_s_B_SEARCH_F1,
                                                FORM_NAME_hh_task_queue_s)) {
            this.trKeyIn("F1");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
            // CLEAR PREVIOUS MESSAGES
            this.getErrorMessage().setValue(null);
            this.getErrorMessageIcon().setName(null);
            this.getErrorMessageIcon().setVisible(false);

            if (WARN.equals(messageType)) {
                this.getErrorMessageIcon().setName(WRN_ICON);
            } else if (INFO.equals(messageType)) {
                this.getErrorMessageIcon().setName(INF_ICON);
            } else if (MSGTYPE_M.equals(messageType)) {
                this.getErrorMessageIcon().setName(LOGO_ICON);
            } else {
                this.getErrorMessageIcon().setName(ERR_ICON);
            }
            this.getErrorMessage().setValue(msg);
            this.getErrorMessageIcon().setVisible(true);
            this.refreshContentOfUIComponent(this.getErrorPanel());
        }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public static void setLogger(ADFLogger _logger) {
        Page1Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }
}
