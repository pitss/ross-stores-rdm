package com.ross.rdm.independent.hhtaskqueues.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.common.NameMapping;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhTaskQueueSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhTaskQueueSBean.class);

    private String formName;
    private Boolean isEquipmentTypeValidated;
    private String isFocusOn;
    private Boolean isError;
    private Boolean isLocationValidated;
    private String containerId;    
    private final static String CONTAINER_ID = "ContainerId";
    private String parameterValue = "";
    private String pickInd = "";
    private String rowId = "";
    private String toLocationId = "";
    private String trailerId = "";

    public void setToLocationId(String toLocationId) {
        this.toLocationId = toLocationId;
    }

    public String getToLocationId() {
        return toLocationId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerId() {
        return trailerId;
    }


    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setPickInd(String pickInd) {
        this.pickInd = pickInd;
    }

    public String getPickInd() {
        return pickInd;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getRowId() {
        return rowId;
    }

    public void initTaskFlowTaskQueue() {
        _logger.info("TaskFlowTaskQueue page initialization begun");
        this.initGlobalVariablesTaskQueue();
        this.initHhTaskQueueWorkVariables();
        this.initHhTaskQueueRow();

        this.setIsError(false);
        this.setIsFocusOn("Equipment");

        _logger.info("Task Queue page initialization completed");

    }

    public TaskFlowId getPutawayInventoryTaskFlow() {
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_putaway_inventory_s", "hotelpicking/"));
    }

    public TaskFlowId getMoveInventoryTaskFlow() {
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_move_inventory_s", "hotelpicking/"));
    }

    public TaskFlowId getMoveTrailerTaskFlow() {
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_move_trailer_s", "trailermgmt/"));
    }

    public TaskFlowId getBulkPickTaskFlow() {
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_bulk_pk_across_wv_s", "hotelpicking/"));
    }

    public TaskFlowId getContainerPickTaskFlow() {
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_container_pick_s", "hotelpicking/"));
    }

    private void initGlobalVariablesTaskQueue() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesTaskQueue");
        oper.execute();
    }

    private void initHhTaskQueueRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertTaskQueue");
        oper.execute();
    }

    private void initHhTaskQueueWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhTaskQueueWorkVariables");
        oper.execute();
    }
    
    public void setContainerId (String containerId) {
        this.containerId = (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID); 
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return formName;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }

    public void setIsEquipmentTypeValidated(Boolean isEquipmentTypeValidated) {
        this.isEquipmentTypeValidated = isEquipmentTypeValidated;
    }

    public Boolean getIsEquipmentTypeValidated() {
        return isEquipmentTypeValidated;
    }

    public void setIsLocationValidated(Boolean isLocationValidated) {
        this.isLocationValidated = isLocationValidated;
    }

    public Boolean getIsLocationValidated() {
        return isLocationValidated;
    }
}
