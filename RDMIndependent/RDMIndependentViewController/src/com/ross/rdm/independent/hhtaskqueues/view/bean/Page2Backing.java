package com.ross.rdm.independent.hhtaskqueues.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.independent.view.framework.RDMIndependentBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMIndependentBackingBean {
    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private final static String TASK_QUEUE_BLOCK = "B_TASK_QUEUE";
    private final static String FORM_NAME = "HH_TASK_QUEUE_S";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";


    private RichIcon activityCodeIcon;
    private RichInputText activityCode;
    private RichIcon loctionIcon;
    private RichInputText location;
    private RichPanelFormLayout taskQueuePanel;
    private RichLink f1Link;
    private RichLink f3Link;
    private RichLink f4Link;

    private RichPopup taskQueuePopup;
    private RichOutputText taskQueuePopupOutputtext;

    private final static String HH_TASK_QUEUE_BEAN = "HhTaskQueueSBean";
    private final static String ACTIVITY_CODE_ATTR = "ActivityCode";
    private final static String LOCATION_ID_ATTR = "LocationId";
    private final static String TASK_ID_ATTR = "TaskId";
    private final static String USER_ATTR = "User";
    private final static String CURRENT_FORM = "CurrentForm";
    private final static String GLOBAL_LOCATION_ID = "GlobalLocationId";
    private final static String USER_PRIVILEGE = "UserPrivilege";
    private final static String CONTAINER_ID = "ContainerId";
    private final static String ROW_ID = "RowId1";
    private final static String TO_LOCATION_ID = "ToLocationId";
    private final static String TRAILER_ID = "TrailerId"; 
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichPopup errorPopup;
    private RichOutputText errorPopupText;

    public Page2Backing() {

    }

    public void performKey(ClientEvent clientEvent) {

        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1Link());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            }
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        DCIteratorBinding ib = ADFUtils.findIterator("HhTaskQueueSBPopulateTaskQueueViewIterator");

        String result = this.callCheckScreenOptionPriv(TASK_QUEUE_BLOCK + key, null);

        if (!"OFF".equals(result)) {
            if ("F3".equalsIgnoreCase(key)) {
                _logger.info("trKeyIn() End");
                ADFUtils.invokeAction("backGlobalHome");
            }
            if ("F1".equalsIgnoreCase(key)) {

                if (null != getActivityCodeId() || null != getLocationId()) {
                    if (getActivityCodeId().equals("C_PICK") || getActivityCodeId().equals("B_PICK") ||
                        getActivityCodeId().equals("U_PICK") || getActivityCodeId().equals("PU_PCK")) {
                        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("getLabelPickInd");
                        String facilityId = getFacilityIdAttrValue();
                        Map map = oper.getParamsMap();
                        map.put("facilityId", facilityId);
                        oper.execute();

                        if (oper.getErrors().isEmpty()) {
                            if (null != oper.getResult()) {
                                List<String> errorCodeList = (List<String>) oper.getResult();
                                String errorCode = errorCodeList.get(0);
                                if ("NO_PICKPACK".equals(errorCode)) {
                                    getErrorPopupText().setValue(this.getMessage(errorCode, ERROR,
                                                                                 this.getFacilityIdAttrValue(),
                                                                                 this.getLanguageCodeValue()));
                                    getErrorPopup().show(new RichPopup.PopupHints());
                                    return;
                                } else {
                                    this.showErrorPanel(ERROR,
                                                        this.getMessage(errorCode, ERROR, this.getFacilityIdAttrValue(),
                                                                        this.getLanguageCodeValue()));
                                    return;
                                }
                            } 
                        }
                    }
                }
                acceptSecondPart();
            }
            
        }
        if ("F4".equalsIgnoreCase(key))
            if (ib.getRowSetIterator().hasNext())
                ib.getRowSetIterator().next();

            else if (ib.getRowSetIterator().getCurrentRowIndex() == (ib.getEstimatedRowCount() - 1))
                ib.setCurrentRowIndexInRange(0);
    }

    private String callCheckScreenOptionPriv(String optionName, String formName) {
        _logger.info("callCheckScreenOptionPriv() Start");
        _logger.fine("optionName value: " + optionName + "\n" + "formName value: " + formName);
        String result = "OFF";
        String userId = (String) ADFUtils.getBoundAttributeValue("User");
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String languageCode = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
        oracle.binding.OperationBinding oper = ADFUtils.findOperation("callCheckScreenOptionPriv");
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("userId", userId);
        oper.getParamsMap().put("formName", FORM_NAME);
        oper.getParamsMap().put("optionName", optionName);
        result = (String) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if ("OFF".equals(result)) {
                this.showErrorPanel(WARN, this.getMessage("NOT_ALLOWED", WARN, facilityId, languageCode));
            }
        }

        _logger.fine("result value: " + result);
        _logger.info("callCheckScreenOptionPriv() End");
        return result;
    }

    private void HhTaskQueueGetFormPriv(String formName) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executeGetFormPrivQuery");
        String facilityId = getFacilityIdAttrValue();
        String iFormName = formName;
        Integer userPrivilege = getUserPrivilege().intValue();

        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("formName", iFormName);
        map.put("userPrivilege", userPrivilege);
        oper.execute();
    }

    private void setAhlInfo(String facilityId) {
        OperationBinding oper3 = (OperationBinding) ADFUtils.findOperation("callSetAhlInfo");
        Map map3 = oper3.getParamsMap();
        map3.put("facilityId", facilityId);
        map3.put("formName", "HH_TASK_QUEUE_S");
        oper3.execute();
    }

    private void showErrorPanel(String messageType, String msg) {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }

        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void f1ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_task_queue_s_B_TASK_QUEUE_F1,
                                                FORM_NAME_hh_task_queue_s)) {
            this.trKeyIn("F1");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_task_queue_s_B_TASK_QUEUE_F4,
                                                FORM_NAME_hh_task_queue_s)) {
            this.trKeyIn("F4");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
            // CLEAR PREVIOUS MESSAGES
            this.getErrorMessage().setValue(null);
            this.getErrorMessageIcon().setName(null);
            this.getErrorMessageIcon().setVisible(false);

            if (WARN.equals(messageType)) {
                this.getErrorMessageIcon().setName(WRN_ICON);
            } else if (INFO.equals(messageType)) {
                this.getErrorMessageIcon().setName(INF_ICON);
            } else if (MSGTYPE_M.equals(messageType)) {
                this.getErrorMessageIcon().setName(LOGO_ICON);
            } else {
                this.getErrorMessageIcon().setName(ERR_ICON);
            }
            this.getErrorMessage().setValue(msg);
            this.getErrorMessageIcon().setVisible(true);
            this.refreshContentOfUIComponent(this.getErrorPanel());
        }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private HhTaskQueueSBean getHhTaskQueueSPageFlowBean() {
        return ((HhTaskQueueSBean) this.getPageFlowBean(HH_TASK_QUEUE_BEAN));
    }

    private String getActivityCodeId() {
        return (String) ADFUtils.getBoundAttributeValue(ACTIVITY_CODE_ATTR);
    }

    private String getLocationId() {
        return (String) ADFUtils.getBoundAttributeValue(LOCATION_ID_ATTR);
    }

    private String getLanguageCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private BigDecimal getTaskIdAttrValue() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue(TASK_ID_ATTR);
    }

    private String getUserAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(USER_ATTR);
    }

    private String getCurrentForm() {
        return (String) ADFUtils.getBoundAttributeValue(CURRENT_FORM);
    }

    private String getGlobalLocationId() {
        return (String) ADFUtils.getBoundAttributeValue(GLOBAL_LOCATION_ID);
    }

    private String getContainerId() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID);
    }

    private String getTQRowId() {
        return (String) ADFUtils.getBoundAttributeValue(ROW_ID);
    }

    private String getToLocationId() {
        return (String) ADFUtils.getBoundAttributeValue(TO_LOCATION_ID);
    }

    private String getTrailerId() {
        return (String) ADFUtils.getBoundAttributeValue(TRAILER_ID);
    }

    private BigDecimal getUserPrivilege() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue(USER_PRIVILEGE);
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public static void setLogger(ADFLogger _logger) {
        Page2Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setActivityCodeIcon(RichIcon activityCodeIcon) {
        this.activityCodeIcon = activityCodeIcon;
    }

    public RichIcon getActivityCodeIcon() {
        return activityCodeIcon;
    }

    public void setActivityCode(RichInputText activityCode) {
        this.activityCode = activityCode;
    }

    public RichInputText getActivityCode() {
        return activityCode;
    }

    public void setLoctionIcon(RichIcon loctionIcon) {
        this.loctionIcon = loctionIcon;
    }

    public RichIcon getLoctionIcon() {
        return loctionIcon;
    }

    public void setLocation(RichInputText location) {
        this.location = location;
    }

    public RichInputText getLocation() {
        return location;
    }

    public void setTaskQueuePanel(RichPanelFormLayout taskQueuePanel) {
        this.taskQueuePanel = taskQueuePanel;
    }

    public RichPanelFormLayout getTaskQueuePanel() {
        return taskQueuePanel;
    }

    public void setF1Link(RichLink f1Link) {
        this.f1Link = f1Link;
    }

    public RichLink getF1Link() {
        return f1Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setTaskQueuePopup(RichPopup taskQueuePopup) {
        this.taskQueuePopup = taskQueuePopup;
    }

    public RichPopup getTaskQueuePopup() {
        return taskQueuePopup;
    }

    public void setTaskQueuePopupOutputtext(RichOutputText taskQueuePopupOutputtext) {
        this.taskQueuePopupOutputtext = taskQueuePopupOutputtext;
    }

    public RichOutputText getTaskQueuePopupOutputtext() {
        return taskQueuePopupOutputtext;
    } 
 
    private void acceptSecondPart() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("pAssignTaskToUser");
        String facilityId = getFacilityIdAttrValue();
        BigDecimal taskId = getTaskIdAttrValue();

        String user = getUserAttrValue();
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("taskId", taskId);
        map.put("user", user);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> errorCodeList = (List<String>) oper.getResult();
                this.showErrorPanel(ERROR,
                                    this.getMessage(errorCodeList.get(0), ERROR, this.getFacilityIdAttrValue(),
                                                    this.getLanguageCodeValue()));
            } else {
                String formName = "";
                String parameterName = null;
                String activityCode = getActivityCodeId();

                if ("C_ROP".equals(activityCode)) {
                    formName = "hh_start_replenishment_pick_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(YES); 
                    setAhlInfo(facilityId);
                } else if ("B_PICK".equals(activityCode)) {
                    formName = "hh_bulk_pick_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(YES);
                    getHhTaskQueueSPageFlowBean().setRowId(getTQRowId()); 
                    setAhlInfo(facilityId);
                    ADFUtils.invokeAction("goBulkPick");
                } else if ("C_PICK".equals(activityCode)) {
                    formName = "hh_container_pick_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(YES);
                    getHhTaskQueueSPageFlowBean().setRowId(getTQRowId()); 
                    setAhlInfo(facilityId);
                    ADFUtils.invokeAction("goContainerPick");
                } else if ("B_ROP".equals(activityCode)) {
                    formName = "hh_bulk_replen_pick_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(YES);
                    setAhlInfo(facilityId);
                } else if ("U_PICK".equals(activityCode)) {
                    formName = "hh_begin_unit_picking_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(YES);
                    setAhlInfo(facilityId);
                } else if ("PU_PCK".equals(activityCode)) {
                    formName = "hh_prime_unit_pick_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(YES);
                    setAhlInfo(facilityId);
                } else if ("CYCCNT".equals(activityCode)) {
                    //SET GLOBAL LOCATION ID = getLocationId();
                    //CALL NAUTILUS FORM PASSING "hh_count_location_s";
                    OperationBinding oper3 = (OperationBinding) ADFUtils.findOperation("callSetAhlInfo");
                    Map map3 = oper3.getParamsMap();
                    map3.put("facilityId", facilityId);
                    map3.put("formName", "HH_TASK_QUEUE_S");
                    oper3.execute();
                    ADFUtils.invokeAction("Page1");
                    return;
                } else if ("MOVE".equals(activityCode) || "BUFMVE".equals(activityCode)) {
                    formName = "hh_move_inventory_s";
                    parameterName = "MOVE_CONTAINER_ID";
                    getHhTaskQueueSPageFlowBean().setParameterValue(getContainerId()); 
                    getHhTaskQueueSPageFlowBean().setPickInd(NO);
                    setAhlInfo(facilityId);
                    ADFUtils.invokeAction("goMoveInventory");
                } else if ("PUTWAY".equals(activityCode)) {
                    formName = "hh_putaway_inventory_s";
                    parameterName = "PUTAWAY_CONTAINER_ID";
                    getHhTaskQueueSPageFlowBean().setParameterValue(getContainerId()); 
                    getHhTaskQueueSPageFlowBean().setPickInd(NO);
                    setAhlInfo(facilityId);
                    ADFUtils.invokeAction("goPutAway");
                } else if ("TRMOVI".equals(getActivityCodeId()) || "SHMOVI".equals(getActivityCodeId())) {
                    formName = "hh_move_trailer_s";
                    getHhTaskQueueSPageFlowBean().setPickInd(NO);
                    getHhTaskQueueSPageFlowBean().setTrailerId(getTrailerId());
                    getHhTaskQueueSPageFlowBean().setToLocationId(getToLocationId()); 
                    setAhlInfo(facilityId);
                    ADFUtils.invokeAction("goMoveTrailer");
                } else {
                    this.showErrorPanel(ERROR,
                                        this.getMessage("UNKNOWN_ACT", ERROR, this.getFacilityIdAttrValue(),
                                                        this.getLanguageCodeValue()));
                    return;
                }

                HhTaskQueueGetFormPriv(formName);
            }
        }
    }

    public void setErrorPopup(RichPopup errorPopup) {
        this.errorPopup = errorPopup;
    }

    public RichPopup getErrorPopup() {
        return errorPopup;
    }

    public void setErrorPopupText(RichOutputText errorPopupText) {
        this.errorPopupText = errorPopupText;
    }

    public RichOutputText getErrorPopupText() {
        return errorPopupText;
    }

    public String errorPopupOkAction() {
        getErrorPopup().hide();
        acceptSecondPart();
        return null;
    }
}
