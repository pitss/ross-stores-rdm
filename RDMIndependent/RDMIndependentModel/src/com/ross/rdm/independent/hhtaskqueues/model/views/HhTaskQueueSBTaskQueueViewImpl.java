package com.ross.rdm.independent.hhtaskqueues.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.independent.hhtaskqueues.model.views.common.HhTaskQueueSBTaskQueueView;

import java.util.ArrayList;
import java.util.List;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon May 30 20:01:23 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhTaskQueueSBTaskQueueViewImpl extends RDMViewObjectImpl implements HhTaskQueueSBTaskQueueView {
    /**
     * This is the default constructor (do not remove).
     */
    public HhTaskQueueSBTaskQueueViewImpl() {
    }

    /**
     * Returns the bind variable value for location.
     * @return bind variable value for location
     */
    public String getlocation() {
        return (String) getNamedWhereClauseParam("location");
    }

    /**
     * Sets <code>value</code> for bind variable location.
     * @param value value to bind as location
     */
    public void setlocation(String value) {
        setNamedWhereClauseParam("location", value);
    }

    /**
     * Returns the bind variable value for ti_equipment_type.
     * @return bind variable value for ti_equipment_type
     */
    public Integer getti_equipment_type() {
        return (Integer) getNamedWhereClauseParam("ti_equipment_type");
    }

    /**
     * Sets <code>value</code> for bind variable ti_equipment_type.
     * @param value value to bind as ti_equipment_type
     */
    public void setti_equipment_type(Integer value) {
        setNamedWhereClauseParam("ti_equipment_type", value);
    }

    /**
     * Returns the bind variable value for facility_id.
     * @return bind variable value for facility_id
     */
    public String getfacility_id() {
        return (String) getNamedWhereClauseParam("facility_id");
    }

    /**
     * Sets <code>value</code> for bind variable facility_id.
     * @param value value to bind as facility_id
     */
    public void setfacility_id(String value) {
        setNamedWhereClauseParam("facility_id", value);
    }
    public List executeTaskQueueViewQuery(String facilityId, Integer equipment, String location) {
        
        List<String> codesList = null;    
        
        this.setfacility_id(facilityId);
        this.setti_equipment_type(equipment);
        this.setlocation(location);
        this.executeQuery();
    if (!this.hasNext()) {
        codesList = new ArrayList <String>();
        String errorMsg = "NO_DATA";
        codesList.add(errorMsg);
    }
        return codesList;
    }
    
}

