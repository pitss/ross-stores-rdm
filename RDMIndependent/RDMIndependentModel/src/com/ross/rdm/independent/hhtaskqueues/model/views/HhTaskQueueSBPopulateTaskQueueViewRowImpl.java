package com.ross.rdm.independent.hhtaskqueues.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue May 17 16:34:31 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhTaskQueueSBPopulateTaskQueueViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActivityCode {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getActivityCode();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setActivityCode((String) value);
            }
        }
        ,
        LocationId {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getLocationId();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setLocationId((String) value);
            }
        }
        ,
        TaskId {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getTaskId();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setTaskId((BigDecimal) value);
            }
        }
        ,
        ContainerId {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getContainerId();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setContainerId((String) value);
            }
        }
        ,
        RowId1 {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getRowId1();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setRowId1((String) value);
            }
        }
        ,
        TrailerId {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getTrailerId();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setTrailerId((String) value);
            }
        }
        ,
        ToLocationId {
            public Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj) {
                return obj.getToLocationId();
            }

            public void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl obj, Object value) {
                obj.setToLocationId((String) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhTaskQueueSBPopulateTaskQueueViewRowImpl object);

        public abstract void put(HhTaskQueueSBPopulateTaskQueueViewRowImpl object, Object value);

        public int index() {
            return HhTaskQueueSBPopulateTaskQueueViewRowImpl.AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return HhTaskQueueSBPopulateTaskQueueViewRowImpl.AttributesEnum.firstIndex() +
                   HhTaskQueueSBPopulateTaskQueueViewRowImpl.AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = HhTaskQueueSBPopulateTaskQueueViewRowImpl.AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACTIVITYCODE = AttributesEnum.ActivityCode.index();
    public static final int LOCATIONID = AttributesEnum.LocationId.index();
    public static final int TASKID = AttributesEnum.TaskId.index();
    public static final int CONTAINERID = AttributesEnum.ContainerId.index();
    public static final int ROWID1 = AttributesEnum.RowId1.index();
    public static final int TRAILERID = AttributesEnum.TrailerId.index();
    public static final int TOLOCATIONID = AttributesEnum.ToLocationId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhTaskQueueSBPopulateTaskQueueViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute ActivityCode.
     * @return the ActivityCode
     */
    public String getActivityCode() {
        return (String) getAttributeInternal(ACTIVITYCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ActivityCode.
     * @param value value to set the  ActivityCode
     */
    public void setActivityCode(String value) {
        setAttributeInternal(ACTIVITYCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute LocationId.
     * @return the LocationId
     */
    public String getLocationId() {
        return (String) getAttributeInternal(LOCATIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LocationId.
     * @param value value to set the  LocationId
     */
    public void setLocationId(String value) {
        setAttributeInternal(LOCATIONID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TaskId.
     * @return the TaskId
     */
    public BigDecimal getTaskId() {
        return (BigDecimal) getAttributeInternal(TASKID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TaskId.
     * @param value value to set the  TaskId
     */
    public void setTaskId(BigDecimal value) {
        setAttributeInternal(TASKID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContainerId.
     * @return the ContainerId
     */
    public String getContainerId() {
        return (String) getAttributeInternal(CONTAINERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContainerId.
     * @param value value to set the  ContainerId
     */
    public void setContainerId(String value) {
        setAttributeInternal(CONTAINERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RowId1.
     * @return the RowId1
     */
    public String getRowId1() {
        return (String) getAttributeInternal(ROWID1);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RowId1.
     * @param value value to set the  RowId1
     */
    public void setRowId1(String value) {
        setAttributeInternal(ROWID1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TrailerId.
     * @return the TrailerId
     */
    public String getTrailerId() {
        return (String) getAttributeInternal(TRAILERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TrailerId.
     * @param value value to set the  TrailerId
     */
    public void setTrailerId(String value) {
        setAttributeInternal(TRAILERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ToLocationId.
     * @return the ToLocationId
     */
    public String getToLocationId() {
        return (String) getAttributeInternal(TOLOCATIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ToLocationId.
     * @param value value to set the  ToLocationId
     */
    public void setToLocationId(String value) {
        setAttributeInternal(TOLOCATIONID, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

