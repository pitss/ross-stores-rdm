package com.ross.rdm.independent.hhtaskqueues.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.independent.hhtaskqueues.model.views.common.HhTaskQueueSBGetFormPrivView;

import java.util.ArrayList;
import java.util.List;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon May 23 12:07:26 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhTaskQueueSBGetFormPrivViewImpl extends RDMViewObjectImpl implements HhTaskQueueSBGetFormPrivView {
    /**
     * This is the default constructor (do not remove).
     */
    public HhTaskQueueSBGetFormPrivViewImpl() {
    }

    /**
     * Returns the bind variable value for facility_id.
     * @return bind variable value for facility_id
     */
    public String getfacility_id() {
        return (String) getNamedWhereClauseParam("facility_id");
    }

    /**
     * Sets <code>value</code> for bind variable facility_id.
     * @param value value to bind as facility_id
     */
    public void setfacility_id(String value) {
        setNamedWhereClauseParam("facility_id", value);
    }

    /**
     * Returns the bind variable value for form_name.
     * @return bind variable value for form_name
     */
    public String getform_name() {
        return (String) getNamedWhereClauseParam("form_name");
    }

    /**
     * Sets <code>value</code> for bind variable form_name.
     * @param value value to bind as form_name
     */
    public void setform_name(String value) {
        setNamedWhereClauseParam("form_name", value);
    }
    
    public List executeGetFormPrivQuery(String facilityId, String formName, Integer userPrivilege) {
        
        List<String> codesList = null;
        this.setfacility_id(facilityId);
        this.setform_name(formName);
        this.executeQuery();
                
        if (!this.hasNext()) {
            codesList = new ArrayList<String>();
            String errorMsg = "ACCESS DENIED";
            codesList.add(errorMsg);
        }
        
        if(this.hasNext()){
            this.next();
            HhTaskQueueSBGetFormPrivViewRowImpl formPrivViewRow = (HhTaskQueueSBGetFormPrivViewRowImpl) getCurrentRow();
                
            Integer privilegeViewUserPrivilege = formPrivViewRow.getUserPrivilege();
            
            if(userPrivilege < privilegeViewUserPrivilege){
            codesList = new ArrayList<String>();
            String errorMsg = "ACCESS DENIED";
            codesList.add(errorMsg);
        }
        }
        return codesList;
    }

}

