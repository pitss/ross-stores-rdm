create or replace PACKAGE CONVEYABLE_CARTON_LOAD AS
/***********************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* ----------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Initial Version
*                                                  This is the main package for Conveyable Carton
*                                                  Load mobile screen.
***********************************************************************************************/
   FUNCTION container_is_valid (i_facility_id          IN facility.facility_id%TYPE,
                                i_container_id         IN container.container_id%TYPE,
                                i_door_id              IN door.door_id%TYPE,
                                i_user_id              IN dms_user.user_id%TYPE,
                                i_action_type          IN VARCHAR2,
                                o_error_msg_code       IN OUT user_message.message_code%TYPE,
                                o_error_msg_typ        IN OUT user_message.message_type%TYPE) RETURN VARCHAR2;

   FUNCTION user_is_valid     (i_facility_id           IN facility.facility_id%TYPE,
                               i_user_id               IN dms_user.user_id%TYPE,
                               i_action_type           IN VARCHAR2,
                               o_error_msg_code        IN OUT user_message.message_code%TYPE,
                               o_error_msg_typ         IN OUT user_message.message_type%TYPE) RETURN VARCHAR2;
                                                       
   PROCEDURE get_carton_info (i_facility_id            IN facility.facility_id%TYPE,
                              i_user_id                IN dms_user.user_id%TYPE,
                              i_container_id           IN container.container_id%TYPE,
                              o_door_id                IN OUT door.door_id%TYPE,
                              o_trailer_id             IN OUT trailer.trailer_id%TYPE,
                              o_route                  IN OUT route.route%TYPE,
                              o_num_users_at_door      IN OUT NUMBER,
                              o_num_users_joining      IN OUT NUMBER,
                              o_error_msg_code         IN OUT user_message.message_code%TYPE,
                              o_error_msg_typ          IN OUT user_message.message_type%TYPE);

   PROCEDURE get_user_info (i_facility_id              IN facility.facility_id%TYPE,
                            i_user_id                  IN dms_user.user_id%TYPE,
                            o_container_id             IN OUT container.container_id%TYPE,
                            o_door_id                  IN OUT door.door_id%TYPE,
                            o_trailer_id               IN OUT trailer.trailer_id%TYPE,
                            o_route                    IN OUT route.route%TYPE,
                            o_team_ind                 IN OUT VARCHAR2,
                            o_team_id                  IN OUT dms_team_user.team_id%TYPE,
                            o_num_users                IN OUT NUMBER,
                            o_error_msg_code           IN OUT user_message.message_code%TYPE,
                            o_error_msg_typ            IN OUT user_message.message_type%TYPE);
                                                       
   FUNCTION assign_first_cid (i_facility_id            IN facility.facility_id%TYPE,
                              i_user_id                IN dms_user.user_id%TYPE,
                              i_remaining_users        IN OUT VARCHAR2,
                              i_container_id           IN container.container_id%TYPE,
                              i_door_id                IN door.door_id%TYPE,
                              i_trailer_id             IN trailer.trailer_id%TYPE,
                              i_route                  IN route.route%TYPE,
                              i_team_ind               IN VARCHAR2,
                              i_num_users              IN NUMBER,
                              o_error_msg_code         IN OUT user_message.message_code%TYPE,
                              o_error_msg_typ          IN OUT user_message.message_type%TYPE) RETURN VARCHAR2;

   FUNCTION assign_last_cid (i_facility_id             IN facility.facility_id%TYPE,
                             i_user_id                 IN dms_user.user_id%TYPE,
                             i_user_to_end             IN dms_user.user_id%TYPE,
                             i_scanned_users           IN OUT VARCHAR2, 
                             i_unscanned_users         IN OUT VARCHAR2, 
                             i_door_id                 IN door.door_id%TYPE,
                             i_first_container_id      IN container.container_id%TYPE,
                             i_last_container_id       IN container.container_id%TYPE,
                             i_action_type             IN VARCHAR2, 
                             o_count_remaining_users   IN OUT NUMBER,
                             o_remaining_users         IN OUT VARCHAR2,
                             o_next_container_id       IN OUT container.container_id%TYPE,
                             o_error_msg_code          IN OUT user_message.message_code%TYPE,
                             o_error_msg_typ           IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 ;

   PROCEDURE populate_team (i_facility_id              IN facility.facility_id%TYPE,
                            i_team_ind                 IN VARCHAR2,
                            i_user_id                  IN dms_user.user_id%TYPE,
                            o_team_id                  IN OUT dms_team_user.team_id%TYPE,
                            o_open_xsaction            IN OUT VARCHAR2,
                            o_error_msg_code           IN OUT user_message.message_code%TYPE,
                            o_error_msg_typ            IN OUT user_message.message_type%TYPE);
                                                       
   PROCEDURE add_del_user (i_facility_id               IN facility.facility_id%TYPE,
                           i_user_id                   IN dms_user.user_id%TYPE,
                           i_user_added_del            IN dms_user.user_id%TYPE,
                           i_team_id                   IN dms_team_user.team_id%TYPE,
                           i_action_type               IN VARCHAR2,
                           o_error_msg_code            IN OUT user_message.message_code%TYPE,
                           o_error_msg_typ             IN OUT user_message.message_type%TYPE);
                                                       
   FUNCTION scan_complete (i_facility_id               IN facility.facility_id%TYPE,
                           i_user_id                   IN dms_user.user_id%TYPE,
                           i_scanned_user              IN VARCHAR2,
                           i_unscanned_user            IN VARCHAR2,
                           o_error_msg_code            IN OUT user_message.message_code%TYPE,
                           o_error_msg_typ             IN OUT user_message.message_type%TYPE) RETURN VARCHAR2;
                                                       
   PROCEDURE cancel_session (i_facility_id             IN facility.facility_id%TYPE,
                             i_user_id                 IN dms_user.user_id%TYPE,
                             i_door_id                 IN door.door_id%TYPE,
                             i_team_ind                IN VARCHAR2);
                             
   FUNCTION user_open_session (i_facility_id           IN facility.facility_id%TYPE,
                               i_user_id               IN VARCHAR2,
                               o_error_msg_code        IN OUT user_message.message_code%TYPE,
                               o_error_msg_typ         IN OUT user_message.message_type%TYPE) RETURN VARCHAR2;
                               
   FUNCTION supervisor_done (i_facility_id               IN facility.facility_id%TYPE,
                             i_user_id                   IN dms_user.user_id%TYPE,
                             i_scanned_user              IN VARCHAR2,
                             i_unscanned_user            IN VARCHAR2,
                             o_screen_code               IN OUT VARCHAR2,
                             o_reopen_user               IN OUT VARCHAR2,
                             o_end_user                  IN OUT VARCHAR2,
                             o_error_msg_code            IN OUT user_message.message_code%TYPE,
                             o_error_msg_typ             IN OUT user_message.message_type%TYPE,
                             i_door_id                   IN door.door_id%TYPE,
                             i_first_container_id        IN container.container_id%TYPE,
                             i_trailer_id                IN trailer.trailer_id%TYPE,
                             i_route                     IN route.route%TYPE,
                             i_team_ind                  IN VARCHAR2) RETURN VARCHAR2;

   

END CONVEYABLE_CARTON_LOAD; 