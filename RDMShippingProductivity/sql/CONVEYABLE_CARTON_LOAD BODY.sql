create or replace PACKAGE BODY CONVEYABLE_CARTON_LOAD AS
   FUNCTION container_is_valid (i_facility_id      IN facility.facility_id%TYPE,
                                i_container_id     IN container.container_id%TYPE,
                                i_door_id          IN door.door_id%TYPE,
                                i_user_id          IN dms_user.user_id%TYPE,
                                i_action_type      IN VARCHAR2,
                                o_error_msg_code   IN OUT user_message.message_code%TYPE,
                                o_error_msg_typ    IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 is
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Function created to validate if container scanned is valid.
*                                                  Call this function when a First CID or Last CID is scanned.
* PARAMETERS:
* i_facility_id     - Pass the facility_id of the session
* i_container_id    - Pass the First CID or the Last CID
* i_door_id         - Pass the Door ID of the Last CID. Pass NULL when validating the First CID.
* i_user_id         - Pass the user who scans the carton ID
* i_action_type     - Pass SCAN_IN for First CID or SCAN_OUT for Last CID
* o_error_msg_code  - The error code will be returned back to the calling program
* o_error_msg_typ   - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/

   e_inv_container_id       EXCEPTION;
   e_inv_container_status   EXCEPTION;
   e_container_non_conv     EXCEPTION;
   e_door_not_conv          EXCEPTION;
   e_cont_out_seq           EXCEPTION;
   e_inv_door_cid           EXCEPTION;
   l_container_id_in        work_activity_audit.container_id%TYPE;
   l_prev_status            work_activity_audit.status%TYPE;
   l_container_status       container.container_status%TYPE;
   l_divert_ts              container.divert_ts%TYPE;
   l_sort_order             container.sort_order%TYPE;
   l_location_id            container.location_id%TYPE;
   l_logical_dest           location.logical_dest_id%TYPE;
   l_prev_cid_divert_ts     container.divert_ts%TYPE;
   l_prev_cid_sort_order    container.sort_order%TYPE;
   l_last_xsaction          BOOLEAN;
   l_door_not_found         BOOLEAN;
   TYPE l_loaded_by_t IS TABLE OF dms_team_user.user_id%TYPE INDEX BY BINARY_INTEGER;
   l_loaded_by              l_loaded_by_t;
   l_user_found             BOOLEAN;
   l_data                   VARCHAR2(150);
   l_max_sort_order_found   BOOLEAN;
   l_max_sort_order         scp.scp_current_val%TYPE;
   l_scp_max_sort_order     scp.scp_current_val%TYPE;
   l_sorter_id              scp_wh.scp_current_val%TYPE;
   l_divert_flag            wh.create_divert_flag%TYPE;
   l_wh_found               VARCHAR2(1);
   l_wh_id                  wh.wh_id%TYPE;

   CURSOR c_get_container_info IS
   SELECT container_status, divert_ts, sort_order, location_id
     FROM container c
    WHERE c.facility_id = i_facility_id
      AND c.container_id = i_container_id;

   CURSOR c_door_is_conveyable (l_location_id IN container.location_id%TYPE) IS
   SELECT logical_dest_id
     FROM location l, door d
    WHERE l.facility_id = i_facility_id
      AND l.facility_id = d.facility_id
      AND l.location_id = d.location_id
      AND l.location_id = l_location_id;

   CURSOR c_get_last_xsaction (l_location_id IN container.location_id%TYPE) IS
   SELECT w.container_id, w.status
     FROM work_activity_audit w
    WHERE w.facility_id = i_facility_id
      AND w.location_id = l_location_id
      AND w.activity_code = 'CNTLDG'
      AND w.status <> 'X'
      AND w.action_ts = (SELECT max(action_ts)
                           FROM work_activity_audit w2
                          WHERE w2.facility_id = i_facility_id
                            AND w2.location_id = l_location_id
                            AND w2.activity_code = 'CNTLDG'
                            AND w2.status <> 'X') 
    ORDER BY start_ts desc;

   CURSOR c_get_cid_divert_ts (l_container_id_in IN container.container_id%TYPE)IS
   SELECT divert_ts, sort_order
     FROM container_event ce
    WHERE ce.facility_id = i_facility_id
      AND ce.container_id = l_container_id_in;

   CURSOR c_get_actual_user (l_divert_ts IN container.divert_ts%TYPE,
                             l_location_id IN container.location_id%TYPE) IS 
   SELECT distinct operator
  FROM (SELECT wau.facility_id, wau.first_container_id, wau.OPERATOR,
               wau.first_scan_ts, wau.last_container_id, wau.last_scan_ts,
               wau.first_cid_sort_order, wau.last_cid_sort_order, ce.container_id,
               ce.divert_ts
          FROM container_event ce, (SELECT b.facility_id, first_container_id, OPERATOR,
                                          first_scan_ts, last_container_id, last_scan_ts,
                                          MAX(DECODE (first_container_id, ce.container_id, sort_order, NULL)) first_cid_sort_order,
                                          MAX(DECODE (last_container_id,ce.container_id, sort_order,NULL)) last_cid_sort_order
                                     FROM (SELECT st.facility_id, st.container_id first_container_id,
                                                  st.OPERATOR, st.action_ts first_scan_ts,co.container_id last_container_id,
                                                  co.action_ts last_scan_ts
                                             FROM (SELECT facility_id, w.container_id,location_id, OPERATOR, start_ts,
                                                          w.action_ts,w.activity_code
                                                     FROM work_activity_audit w
                                                    WHERE w.facility_id = i_facility_id
                                                      AND w.activity_code = 'CNTLDG'
                                                      AND location_id = l_location_id
                                                      AND start_ts >= l_divert_ts
                                                      AND action_ts >=l_divert_ts
                                                      AND status = 'S') st,
                                                  (SELECT facility_id, container_id,location_id, OPERATOR, start_ts,
                                                          w.action_ts, w.activity_code
                                                     FROM work_activity_audit w
                                                    WHERE w.facility_id = i_facility_id
                                                      AND w.activity_code = 'CNTLDG'
                                                      AND location_id = l_location_id
                                                      AND start_ts >= l_divert_ts
                                                      AND action_ts >=l_divert_ts
                                                      AND w.status = 'C') co
                                            WHERE st.OPERATOR = co.OPERATOR
                                              AND st.location_id =co.location_id
                                              AND st.facility_id =co.facility_id
                                              AND st.start_ts = co.start_ts
                                              AND st.activity_code = co.activity_code
                                            ORDER BY first_scan_ts, first_container_id) b,
                                                                        container_event ce
                                    WHERE (b.first_container_id = ce.container_id OR b.last_container_id = ce.container_id)
                                      AND b.facility_id = i_facility_id
                                      AND b.facility_id = ce.facility_id
                                      AND ce.user_id = '3RD PARTY'
                                      AND ce.event_code in ('72','75')
                                    GROUP BY b.facility_id,first_container_id,OPERATOR,first_scan_ts,last_container_id,last_scan_ts) wau
         WHERE ce.facility_Id = i_facility_id
           AND wau.facility_id = ce.facility_id
           AND ce.user_id = '3RD PARTY'
           AND ce.event_code in ('72','75')
           AND ce.sort_order BETWEEN wau.first_cid_sort_order AND wau.last_cid_sort_order)
 WHERE facility_id = i_facility_id
   AND container_id = i_container_id;
   
   CURSOR c_get_max_sort_order IS
   SELECT MAX (sort_order)
     FROM container_event ce, LOCATION l, ZONE z
    WHERE ce.facility_id = i_facility_id
      AND ce.event_code in ('72','75')
      AND ce.user_id = '3RD PARTY'
      AND ce.facility_id = l.facility_id
      AND ce.location_id = l.location_id
      AND l.facility_id = z.facility_id
      AND l.ZONE = z.ZONE
      AND z.wh_id IN (SELECT wh_id
                        FROM scp_wh
                       WHERE facility_id = i_facility_id
                         AND scp_name = 'sorter_id'
                         AND scp_current_val = l_sorter_id);
                         
   CURSOR c_get_max_divert_ts IS
   SELECT divert_ts 
     FROM container_event ce
    WHERE ce.facility_id = i_facility_id
      AND ce.event_code in ('72','75')
      AND ce.user_id = '3RD PARTY'
      AND ce.sort_order = l_max_sort_order;



      BEGIN
         -- Validate the container if it exists in RDM
         IF v_container_ID ( i_container_id, i_facility_id) = 'N' THEN
            raise e_inv_container_id;
         END IF;
         
         -- Get the details of the scanned carton
         OPEN c_get_container_info;
         FETCH c_get_container_info INTO l_container_status, l_divert_ts, l_sort_order, l_location_id;
         CLOSE c_get_container_info;
         -- Make sure the status = M
         IF l_container_status <> 'M' THEN
            raise e_inv_container_status;
         END IF;
         -- Make sure the carton is conveyable (divert_ts is not null)
         IF l_divert_ts IS NULL THEN
            raise e_container_non_conv;
         END IF;
         
         -- Check if scanned carton is in a conveyable door location (logical_dest_id is not null)
         OPEN c_door_is_conveyable (l_location_id);
         FETCH c_door_is_conveyable INTO l_logical_dest;
         l_door_not_found := c_door_is_conveyable%NOTFOUND;
         CLOSE c_door_is_conveyable;

         IF l_door_not_found OR l_logical_dest IS NULL THEN
            raise e_door_not_conv;
         END IF;
         -- Get the most recent transaction at the door. Do not include cancelled (X) status
         OPEN c_get_last_xsaction (l_location_id);
         FETCH c_get_last_xsaction INTO l_container_id_in, l_prev_status;
         l_last_xsaction := c_get_last_xsaction%FOUND;
         CLOSE c_get_last_xsaction;
         
         -- Get the divert_ts and sort_order of the most recent carton scanned at door
         OPEN c_get_cid_divert_ts (l_container_id_in);
         FETCH c_get_cid_divert_ts INTO l_prev_cid_divert_ts, l_prev_cid_sort_order;
         CLOSE c_get_cid_divert_ts;
         
         -- Get the wh_id of the door
         get_wh_id (i_facility_id, i_door_id, l_wh_id, l_divert_flag, l_wh_found);
         
         -- Get the max sort_order that will be sent by Shipping sorter
         l_scp_max_sort_order := g_scp (i_facility_id, 'max_sort_order');
         l_sorter_id := g_scp_wh (i_facility_id, l_wh_id, 'sorter_id');
         
         -- Get the max sort_order that was sent by shipping sorter
         OPEN c_get_max_sort_order;
         FETCH c_get_max_sort_order INTO l_max_sort_order;
         l_max_sort_order_found := c_get_max_sort_order%FOUND;
         CLOSE c_get_max_sort_order;
         
         
         -- Check if the most recent carton scanned at door had a later divert_ts and later sort_order
         IF l_last_xsaction THEN 

            IF NOT ((l_prev_status = 'S' AND i_action_type = 'SCAN_IN' 
               AND l_divert_ts >= l_prev_cid_divert_ts 
               AND ((l_sort_order >= l_prev_cid_sort_order) OR (l_sort_order < l_prev_cid_sort_order AND l_max_sort_order = l_scp_max_sort_order AND l_divert_ts > l_prev_cid_divert_ts)))
               OR (l_prev_status = 'C' AND i_action_type = 'SCAN_IN' 
                  AND l_divert_ts >= l_prev_cid_divert_ts 
                  AND ((l_sort_order > l_prev_cid_sort_order)OR (l_sort_order < l_prev_cid_sort_order AND l_max_sort_order = l_scp_max_sort_order AND l_divert_ts > l_prev_cid_divert_ts)))
               OR (l_prev_status = 'C' AND i_action_type = 'SCAN_OUT' 
                  AND l_divert_ts >= l_prev_cid_divert_ts 
                  AND ((l_sort_order >= l_prev_cid_sort_order) OR (l_sort_order < l_prev_cid_sort_order AND l_max_sort_order = l_scp_max_sort_order AND l_divert_ts > l_prev_cid_divert_ts)))
               OR (l_prev_status = 'S' AND i_action_type = 'SCAN_OUT' 
                  AND l_divert_ts >= l_prev_cid_divert_ts 
                  AND ((l_sort_order >= l_prev_cid_sort_order) OR (l_sort_order < l_prev_cid_sort_order AND l_max_sort_order = l_scp_max_sort_order AND l_divert_ts > l_prev_cid_divert_ts))))
            THEN
               -- if carton scanned was out of sequnce, get the user who actually loaded the carton
               OPEN c_get_actual_user (l_divert_ts, l_location_id);
               FETCH c_get_actual_user BULK COLLECT INTO l_loaded_by;
               CLOSE c_get_actual_user;
               -- If a user was found, log the user in audit trail. Otherwise, log the carton with reason code SKIPPD
               IF l_loaded_by.COUNT > 0 THEN
                  FOR i IN l_loaded_by.FIRST .. l_loaded_by.LAST LOOP
                     EXIT WHEN i > 15;
                     l_data := l_data || ',' || l_loaded_by(i);
                     
                  END LOOP;

                  log_audit_trail (i_facility_id,
                                   'CONVEYABLE_CARTON_LOAD',
                                   i_user_id, 
                                   l_location_id,
                                   i_container_id,
                                   'CONV',
                                   'OUTSEQ',
                                   'ACTUAL_USER',
                                   TRIM(BOTH ',' FROM substr(substr(l_data,1,30),1,instr(substr(l_data,1,30),',',-1))),
                                   'ACTION',
                                   i_action_type);
               ELSE
                  log_audit_trail (i_facility_id,
                                   'CONVEYABLE_CARTON_LOAD',
                                   i_user_id, 
                                   l_location_id,
                                   i_container_id,
                                   'CONV',
                                   'SKIPPD',
                                   'ACTION',
                                   i_action_type);
               END IF;

               raise e_cont_out_seq;
            END IF;
         END IF;
         -- If the action type <> SCAN IN and the carton is from a diff. door, raise an error
         IF i_action_type <> 'SCAN_IN' THEN
            IF l_location_id <> i_door_id THEN
               raise e_inv_door_cid;
            END IF;
         END IF;

         RETURN ('Y');

      EXCEPTION
         WHEN e_inv_container_id THEN
            o_error_msg_code := 'INV_CID';
            o_error_msg_typ := 'E';
            RETURN ('N');
         WHEN e_inv_container_status THEN
            o_error_msg_code := 'INV_CONT_STAT';
            o_error_msg_typ := 'E';
            RETURN ('N');
         WHEN e_container_non_conv THEN
            o_error_msg_code := 'CONT_NO_DVRT';
            o_error_msg_typ := 'E';
            RETURN ('N');
         WHEN e_door_not_conv THEN
            o_error_msg_code := 'INV_DOOR';
            o_error_msg_typ := 'E';
            RETURN ('N');
         WHEN e_cont_out_seq THEN
            o_error_msg_code := 'CONT_OUT_SEQ';
            o_error_msg_typ := 'E';
            RETURN ('N');
         WHEN e_inv_door_cid THEN
            o_error_msg_code := 'INV_DOOR_CID';
            o_error_msg_typ := 'E';
            RETURN ('N');
      END container_is_valid;

   FUNCTION user_is_valid (i_facility_id      IN facility.facility_id%TYPE,
                           i_user_id          IN dms_user.user_id%TYPE,
                           i_action_type      IN VARCHAR2,
                           o_error_msg_code   IN OUT user_message.message_code%TYPE,
                           o_error_msg_typ    IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 is

/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Function created to validate if user is allowed to scan in/out.
*                                                  Call this function when a user tries to scan a First or Last CID.
* PARAMETERS:
* i_facility_id     - Pass the facility_id of the session
* i_user_id         - Pass the user who scans the carton ID
* i_action_type     - Pass SCAN_IN for First CID or SCAN_OUT for Last CID
* o_error_msg_code  - The error code will be returned back to the calling program
* o_error_msg_typ   - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/

   CURSOR c_get_user_current_status IS
   SELECT status
     FROM work_activity_audit w
    WHERE w.facility_id = i_facility_id
      AND w.operator = i_user_id
      AND w.activity_code = 'CNTLDG'
      AND w.action_ts = (SELECT max(action_ts)
                           FROM work_activity_audit w
                          WHERE w.facility_id = i_facility_id
                            AND w.operator = i_user_id
                            AND w.activity_code = 'CNTLDG')
      AND w.start_ts = (SELECT max(start_ts)
                           FROM work_activity_audit w
                          WHERE w.facility_id = i_facility_id
                            AND w.operator = i_user_id
                            AND w.activity_code = 'CNTLDG');


   l_user_current_status   work_activity_audit.status%TYPE;
   user_logged_in          EXCEPTION;
   user_not_logged_in      EXCEPTION;

      BEGIN
         -- Get the status of the user's current session 
         OPEN c_get_user_current_status;
         FETCH c_get_user_current_status INTO l_user_current_status;
         CLOSE c_get_user_current_status;
         -- if user was scanning in, don't allow to scan in if the user's most recent session is not C or X
         IF i_action_type = 'SCAN_IN' THEN
            IF l_user_current_status NOT IN ('C', 'X') THEN
               raise user_logged_in;
            END IF;
         -- if user was scanning out, don't allow to scan out if the user's most recent session is not S
         ELSIF i_action_type = 'SCAN_OUT' THEN
            IF l_user_current_status <> 'S' THEN
               raise user_not_logged_in;
            END IF;
         END IF;

         RETURN ('Y');

      EXCEPTION
         WHEN user_logged_in THEN
            o_error_msg_code := 'USER_LOGGED_IN';
            o_error_msg_typ := 'E';
            RETURN ('N');
         WHEN user_not_logged_in THEN
            o_error_msg_code := 'NOT_LOGGED_ON';
            o_error_msg_typ := 'E';
            RETURN ('N');
      END user_is_valid;

   PROCEDURE get_carton_info (i_facility_id       IN facility.facility_id%TYPE,
                              i_user_id           IN dms_user.user_id%TYPE,
                              i_container_id      IN container.container_id%TYPE,
                              o_door_id           IN OUT door.door_id%TYPE,
                              o_trailer_id        IN OUT trailer.trailer_id%TYPE,
                              o_route             IN OUT route.route%TYPE,
                              o_num_users_at_door IN OUT NUMBER,
                              o_num_users_joining IN OUT NUMBER,
                              o_error_msg_code    IN OUT user_message.message_code%TYPE,
                              o_error_msg_typ     IN OUT user_message.message_type%TYPE) is

/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Procedure created to get all the details associated to the
*                                                  carton scanned by the user.
*                                                  Call this function when a First CID is scanned.
* PARAMETERS:
* i_facility_id          - Pass the facility_id of the session
* i_user_id              - Pass the user who scans the carton ID
* i_container_id         - Pass the First CID 
* o_door_id              - Door/Location ID of the CID will be returned 
* o_trailer_id           - Trailer ID associated to the door will be returned 
* o_route                - Route associated to the door will be returned 
* o_num_users_at_door    - The number of users at the door will be returned
* o_num_users_joining    - The number of users joining the door will be returned (1 or > 1 if user is part of a team)
* o_error_msg_code       - The error code will be returned back to the calling program
* o_error_msg_typ        - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/

   CURSOR c_get_door_trailer IS
   SELECT c.location_id, m.trailer_id, c.route, c.divert_ts
     FROM container c, manifest m
    WHERE c.facility_id = i_facility_id
      AND m.facility_id = i_facility_id
      AND c.container_id = i_container_id
      AND c.location_id = m.door_id
      AND c.bol_nbr = m.bol_nbr
      AND rownum < 2;

   CURSOR c_get_num_users_door IS
   SELECT distinct user_sharing
     FROM work_activity_audit w
    WHERE w.facility_id = i_facility_id
      AND w.activity_code = 'CNTLDG'
      AND w.location_id = o_door_id
      AND w.status = 'S'
      AND NOT EXISTS (SELECT 'x'
                        FROM work_activity_audit w2
                       WHERE w2.facility_id = i_facility_id
                         AND w2.activity_code = 'CNTLDG'
                         AND w2.location_id = o_door_id
                         AND w2.status <> 'S'
                         AND w.start_ts = w2.start_ts
                         AND w.operator = w2.operator);

   CURSOR c_user_is_individual IS
   SELECT team_id
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.user_id = i_user_id
      AND dtu.mstr_bol_id = '0'
      AND dtu.create_date IS NOT NULL
      AND dtu.end_date IS NULL;

   CURSOR c_count_users_in_team (l_team_id_in IN dms_team_user.team_id%TYPE) IS
   SELECT count(user_id)
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.team_id = l_team_id_in
      AND dtu.mstr_bol_id = '0'
      AND dtu.create_date IS NOT NULL
      AND dtu.end_date IS NULL;


      l_users_found        BOOLEAN;
      l_user_not_in_team   BOOLEAN;
      l_users_in_team      NUMBER;
      l_team_id            dms_team_user.team_id%TYPE;
      l_cid_divert_ts      container.divert_ts%TYPE;
      e_inv_container_id   EXCEPTION;

      BEGIN
         -- Get the door, trailer, route and divert_ts of the carton scanned by the user
         OPEN c_get_door_trailer;
         FETCH c_get_door_trailer INTO o_door_id, o_trailer_id, o_route, l_cid_divert_ts;
         CLOSE c_get_door_trailer;
         -- make sure there's a trailer associated to the carton
         IF o_trailer_id IS NULL THEN
            raise e_inv_container_id;
         ELSE
            -- Get the current number of users at the door (users with S session)
            OPEN c_get_num_users_door;
            FETCH c_get_num_users_door INTO o_num_users_at_door;
            l_users_found := c_get_num_users_door%FOUND;
            CLOSE c_get_num_users_door;

            IF NOT l_users_found THEN
               o_num_users_at_door := 0;
            END IF;
         END IF;
         -- Get the user's team ID
         OPEN c_user_is_individual;
         FETCH c_user_is_individual INTO l_team_id;
         l_user_not_in_team := c_user_is_individual%NOTFOUND;
         CLOSE c_user_is_individual;
         -- If user is not on team then default number of users joining to 1. Otherwise, count the number of users in the team
         IF l_user_not_in_team THEN
            o_num_users_joining := 1;
         ELSE
            OPEN c_count_users_in_team (l_team_id);
            FETCH c_count_users_in_team INTO o_num_users_joining;
            CLOSE c_count_users_in_team;

         END IF;
         -- Return a confirmation message if there are users at the door
         IF o_num_users_at_door > 0 THEN
            o_error_msg_code := 'USER_AT_DOOR';
            o_error_msg_typ := 'C';
         END IF;

      EXCEPTION
         WHEN e_inv_container_id THEN
            o_error_msg_code := 'INV_CID';
            o_error_msg_typ := 'E';
         WHEN OTHERS THEN
            log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.get_carton_info', SQLERRM);
      END get_carton_info;

   PROCEDURE get_user_info (i_facility_id        IN facility.facility_id%TYPE,
                            i_user_id            IN dms_user.user_id%TYPE,
                            o_container_id       IN OUT container.container_id%TYPE,
                            o_door_id            IN OUT door.door_id%TYPE,
                            o_trailer_id         IN OUT trailer.trailer_id%TYPE,
                            o_route              IN OUT route.route%TYPE,
                            o_team_ind           IN OUT VARCHAR2,
                            o_team_id            IN OUT dms_team_user.team_id%TYPE,
                            o_num_users          IN OUT NUMBER,
                            o_error_msg_code     IN OUT user_message.message_code%TYPE,
                            o_error_msg_typ      IN OUT user_message.message_type%TYPE) is
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Procedure created to get all the details associated to the
*                                                  user who accessed the screen.
*                                                  Call this function when the user accesses the screen.
* PARAMETERS:
* i_facility_id          - Pass the facility_id of the session
* i_user_id              - Pass the user who accesses the screen
* o_container_id         - The First CID of the user if he/she had an open session will be returned.
* o_door_id              - Door/Location ID of the CID will be returned 
* o_trailer_id           - Trailer ID associated to the door will be returned 
* o_route                - Route associated to the door will be returned 
* o_team_ind             - Will return Y if the user is part of a team and N if user is an individual
* o_team_id              - Will return the team ID of the user.
* o_num_users            - The number of users at the door will be returned
* o_error_msg_code       - The error code will be returned back to the calling program
* o_error_msg_typ        - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/
   CURSOR c_get_user_info IS
   SELECT container_id, location_id, trailer_id, route, user_sharing
     FROM work_activity_audit w
    WHERE w.facility_id = i_facility_id
      AND w.operator = i_user_id
      AND w.activity_code = 'CNTLDG'
      AND w.status = 'S'
      AND NOT EXISTS (SELECT 'x'
                        FROM work_activity_audit w2
                       WHERE w2.facility_id = i_facility_id
                         AND w2.activity_code = 'CNTLDG'
                         AND w2.operator = i_user_id
                         AND w2.location_id = w.location_id
                         AND w2.status <> 'S'
                         AND w.start_ts = w2.start_ts);

   CURSOR c_user_is_individual IS
   SELECT team_id
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.user_id = i_user_id
      AND dtu.mstr_bol_id = '0'
      AND dtu.create_date IS NOT NULL
      AND dtu.end_date IS NULL;


    l_value_found         BOOLEAN;
    l_team_id             dms_team_user.team_id%TYPE;
    l_user_not_in_team     BOOLEAN;

      BEGIN
         -- Get the current session (S) of the user
         OPEN c_get_user_info;
         FETCH c_get_user_info INTO o_container_id, o_door_id, o_trailer_id, o_route, o_num_users;
         l_value_found := c_get_user_info%FOUND;
         CLOSE c_get_user_info;
         -- Get the user's team ID
         OPEN c_user_is_individual;
         FETCH c_user_is_individual INTO l_team_id;
         l_user_not_in_team := c_user_is_individual%NOTFOUND;
         CLOSE c_user_is_individual;
         -- Return Y if user is part of a team (team_ind) else return N
         IF l_user_not_in_team THEN
            o_team_ind := 'N';
            o_team_id := null;
         ELSE
            o_team_ind := 'Y';
            o_team_id := l_team_id;
         END IF;

      EXCEPTION
         WHEN OTHERS THEN
            log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.get_user_info', SQLERRM);
      END get_user_info;

   FUNCTION assign_first_cid (i_facility_id          IN facility.facility_id%TYPE,
                              i_user_id              IN dms_user.user_id%TYPE,
                              i_remaining_users      IN OUT VARCHAR2,
                              i_container_id         IN container.container_id%TYPE,
                              i_door_id              IN door.door_id%TYPE,
                              i_trailer_id           IN trailer.trailer_id%TYPE,
                              i_route                IN route.route%TYPE,
                              i_team_ind             IN VARCHAR2,
                              i_num_users            IN NUMBER,
                              o_error_msg_code       IN OUT user_message.message_code%TYPE,
                              o_error_msg_typ        IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 IS
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Call this function when assigning a First CID to the user
*                                                  scanning at the door.
*                                                  This function should also be invoked after the assign_last_cid
*                                                  is called to assign a First CID to the user scanning at the door 
*                                                  and the scanned badges.
*                                                  And lastly, the function should be invoked to assign a last
*                                                  carton to the remaining users after a user/team scans out
*                                                  of a door.
*                                                  
* PARAMETERS:
* i_facility_id          - Pass the facility_id of the session
* i_user_id              - Pass the user scanning in. This should be NULL when being called after assign_last_cid (SCAN_OUT)
* i_remaining_users      - Pass the scanned badges or the remaining users at the door.
* i_container_id         - Pass the container ID scanned by the user or the container ID returned by assign_last_cid (SCAN_OUT)
* i_door_id              - Pass the door ID of the container scanned by the user. This can be NULL when i_remaining_users
*                          is not NULL
* i_trailer_id           - Pass the trailer ID of the container scanned by the user. This can be NULL when i_remaining_users
*                          is not NULL
* i_route                - Pass the route of the container scanned by the user. This can be NULL when i_remaining_users
*                          is not NULL
* i_team_ind             - Pass Y or N if the scanning user is part of a team or not.
* i_num_users            - Pass the count of users that will be sharing credits at the door
* o_error_msg_code       - The error code will be returned back to the calling program
* o_error_msg_typ        - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/

      l_dc_id           wh.dc_id%TYPE;
      l_wh_id           wh.wh_id%TYPE;
      l_zone            zone.zone%TYPE;
      l_rec_not_found   BOOLEAN;
      l_team_id         dms_team_user.team_id%TYPE;
      l_start_date      DATE := sysdate;
      l_start_ts        DATE := l_start_date;
      TYPE l_team_users_t IS TABLE OF dms_team_user.user_id%TYPE INDEX BY BINARY_INTEGER;
      l_team_users      l_team_users_t;
      l_statement       VARCHAR2(2000);
      l_comma_count     NUMBER(3);
      l_trailer_id      trailer.trailer_id%TYPE;
      l_route           route.route%TYPE;
      l_door_id         door.door_id%TYPE;
      l_trlr_found      BOOLEAN;
      l_start           NUMBER(3);
      l_occurence       NUMBER(3);
      l_length          NUMBER(3);
      l_comma_pos       NUMBER(3);
      l_user_id         dms_user.user_id%TYPE;
      TYPE l_facility_id_t IS TABLE OF work_activity_audit.facility_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_container_id_t IS TABLE OF work_activity_audit.container_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_activity_code_t IS TABLE OF work_activity_audit.activity_code%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_start_ts_t IS TABLE OF work_activity_audit.start_ts%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_action_ts_t IS TABLE OF work_activity_audit.action_ts%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_status_t IS TABLE OF work_activity_audit.status%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_operator_t IS TABLE OF work_activity_audit.operator%TYPE INDEX BY BINARY_INTEGER;
      l_facility_id     l_facility_id_t;
      l_container_id    l_container_id_t;
      l_activity_code   l_activity_code_t;
      l_start_ts_b      l_start_ts_t;
      l_action_ts       l_action_ts_t;
      l_status          l_status_t;
      l_operator        l_operator_t;

      CURSOR c_get_wh_zone_dc IS
      SELECT w.dc_id, w.wh_id, z.zone
        FROM wh w, zone z, location l
       WHERE l.facility_id = i_facility_id
         AND l.location_id = i_door_id
         AND l.facility_id = z.facility_id
         AND l.zone = z.zone
         AND z.facility_id = w.facility_id
         AND z.wh_id = w.wh_id;
         
      CURSOR c_get_door_trailer (container_id_in IN container.container_id%TYPE) IS
      SELECT m.trailer_id, c.route, c.location_id
        FROM container c, manifest m
       WHERE c.facility_id = i_facility_id
         AND m.facility_id = i_facility_id
         AND c.container_id = container_id_in
         AND c.location_id = m.door_id
         AND c.bol_nbr = m.bol_nbr
         AND rownum < 2;

      CURSOR c_get_team_id(user_id_in IN dms_team_user.user_id%TYPE) IS
      SELECT team_id
        FROM dms_team_user dtu
       WHERE dtu.facility_id = i_facility_id
         AND dtu.user_id = user_id_in
         AND dtu.mstr_bol_id = '0'
         AND dtu.create_date IS NOT NULL
         AND dtu.end_date IS NULL;

      CURSOR c_get_team_users (l_team_id_in IN dms_team_user.team_id%TYPE) IS
      SELECT user_id
        FROM dms_team_user dtu
       WHERE dtu.facility_id = i_facility_id
         AND dtu.team_id = l_team_id_in
         AND dtu.mstr_bol_id = '0'
         AND dtu.create_date IS NOT NULL
         AND dtu.end_date IS NULL;

      CURSOR c_get_all_open_xsaction IS
      SELECT w.facility_id, w.container_id, w.activity_code, w.start_ts, w.action_ts, w.status, w.operator
           FROM work_activity_audit w
          WHERE w.facility_id = i_facility_id
            AND w.location_id = i_door_id
            AND w.activity_code = 'CNTLDG'
            AND w.status = 'S'
            AND NOT EXISTS (SELECT 'x'
                              FROM work_activity_audit w2
                             WHERE w2.facility_id = i_facility_id
                               AND w2.activity_code = 'CNTLDG'
                               AND w2.location_id = i_door_id
                               AND w2.status <> 'S'
                               AND w.start_ts = w2.start_ts
                               AND w.operator = w2.operator);

      BEGIN
         -- Get the dc, wh and zone of the door where the user scanned
         OPEN c_get_wh_zone_dc;
         FETCH c_get_wh_zone_dc into l_dc_id,
                                     l_wh_id,
                                     l_zone;
         l_rec_not_found := c_get_wh_zone_dc%NOTFOUND;
         CLOSE c_get_wh_zone_dc;
         -- Get the team ID of the user if he's part of a team. Then get the users who are part of the team
         IF i_team_ind = 'Y' THEN
            OPEN c_get_team_id(i_user_id);
            FETCH c_get_team_id INTO l_team_id;
            CLOSE c_get_team_id;

            OPEN c_get_team_users(l_team_id);
            FETCH c_get_team_users BULK COLLECT INTO l_team_users;
            CLOSE c_get_team_users;
         END IF;
         -- NULL remaining_users mean, we are only assigning a First CID to the scanning-in user
         IF i_remaining_users IS NULL THEN
            OPEN c_get_all_open_xsaction;
            FETCH c_get_all_open_xsaction BULK COLLECT INTO l_facility_id, l_container_id, l_activity_code, l_start_ts_b, l_action_ts, l_status, l_operator;
            CLOSE c_get_all_open_xsaction;
            
            IF l_facility_id.COUNT > 0 THEN
               FORALL i IN l_facility_id.FIRST .. l_facility_id.LAST
                 DELETE work_activity_audit
                  WHERE facility_id = l_facility_id(i)
                    AND container_id = l_container_id(i)
                    AND activity_code = l_activity_code(i)
                    AND start_ts = l_start_ts_b(i)
                    AND action_ts = l_action_ts(i)
                    AND status = l_status(i)
                    AND operator = l_operator(i);
                 COMMIT;
            END IF;
            
            IF i_team_ind = 'N' THEN
               INSERT INTO work_activity_audit (facility_id,
                                                container_id,
                                                activity_code,
                                                start_ts,
                                                action_ts,
                                                status,
                                                dc_id,
                                                wh_id,
                                                zone,
                                                location_id,
                                                operator,
                                                total_unit_qty,
                                                num_lpn,
                                                units_worked,
                                                container_worked,
                                                units_remaining,
                                                container_remaining,
                                                item_id,
                                                prepack_qty,
                                                nested_qty,
                                                supervisor,
                                                pay_variable_codes,
                                                route,
                                                trailer_id,
                                                terminated_by,
                                                user_sharing,
                                                team_id)
                                        VALUES (i_facility_id,
                                                i_container_id,
                                                'CNTLDG',
                                                l_start_ts,
                                                l_start_ts,
                                                'S',
                                                l_dc_id,
                                                l_wh_id,
                                                l_zone,
                                                i_door_id,
                                                i_user_id,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                i_route,
                                                i_trailer_id,
                                                NULL,
                                                i_num_users,
                                                NULL);
               COMMIT;
            ELSE
               IF l_team_users.COUNT > 0 THEN
                  FORALL i IN l_team_users.FIRST .. l_team_users.LAST
                     INSERT INTO work_activity_audit (facility_id,
                                                      container_id,
                                                      activity_code,
                                                      start_ts,
                                                      action_ts,
                                                      status,
                                                      dc_id,
                                                      wh_id,
                                                      zone,
                                                      location_id,
                                                      operator,
                                                      total_unit_qty,
                                                      num_lpn,
                                                      units_worked,
                                                      container_worked,
                                                      units_remaining,
                                                      container_remaining,
                                                      item_id,
                                                      prepack_qty,
                                                      nested_qty,
                                                      supervisor,
                                                      pay_variable_codes,
                                                      route,
                                                      trailer_id,
                                                      terminated_by,
                                                      user_sharing,
                                                      team_id)
                                              VALUES (i_facility_id,
                                                      i_container_id,
                                                      'CNTLDG',
                                                      l_start_ts,
                                                      l_start_ts,
                                                      'S',
                                                      l_dc_id,
                                                      l_wh_id,
                                                      l_zone,
                                                      i_door_id,
                                                      l_team_users(i),
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      i_route,
                                                      i_trailer_id,
                                                      NULL,
                                                      i_num_users,
                                                      l_team_id);
                  COMMIT;
               END IF;
            END IF;
         -- If remaining_users is not null, this means we need to assign First CID to the remaining users and to the scanning-in user
         ELSIF i_remaining_users IS NOT NULL THEN 
            OPEN c_get_all_open_xsaction;
            FETCH c_get_all_open_xsaction BULK COLLECT INTO l_facility_id, l_container_id, l_activity_code, l_start_ts_b, l_action_ts, l_status, l_operator;
            CLOSE c_get_all_open_xsaction;
            
            IF l_facility_id.COUNT > 0 THEN
               FORALL i IN l_facility_id.FIRST .. l_facility_id.LAST
                 DELETE work_activity_audit
                  WHERE facility_id = l_facility_id(i)
                    AND container_id = l_container_id(i)
                    AND activity_code = l_activity_code(i)
                    AND start_ts = l_start_ts_b(i)
                    AND action_ts = l_action_ts(i)
                    AND status = l_status(i)
                    AND operator = l_operator(i);
                 COMMIT;
            END IF;
            
            OPEN c_get_door_trailer(i_container_id);
            FETCH c_get_door_trailer INTO l_trailer_id,
                                          l_route,
                                          l_door_id;
            l_trlr_found := c_get_door_trailer%FOUND;
            CLOSE c_get_door_trailer;
            -- Assign First CID to the scanning-in user/team
            IF i_team_ind = 'N' THEN
               INSERT INTO work_activity_audit (facility_id,
                                                container_id,
                                                activity_code,
                                                start_ts,
                                                action_ts,
                                                status,
                                                dc_id,
                                                wh_id,
                                                zone,
                                                location_id,
                                                operator,
                                                total_unit_qty,
                                                num_lpn,
                                                units_worked,
                                                container_worked,
                                                units_remaining,
                                                container_remaining,
                                                item_id,
                                                prepack_qty,
                                                nested_qty,
                                                supervisor,
                                                pay_variable_codes,
                                                route,
                                                trailer_id,
                                                terminated_by,
                                                user_sharing,
                                                team_id)
                                        VALUES (i_facility_id,
                                                i_container_id,
                                                'CNTLDG',
                                                l_start_ts,
                                                l_start_ts,
                                                'S',
                                                l_dc_id,
                                                l_wh_id,
                                                l_zone,
                                                l_door_id,
                                                i_user_id,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                l_route,
                                                l_trailer_id,
                                                NULL,
                                                i_num_users,
                                                NULL);
               COMMIT;
            ELSE
               IF l_team_users.COUNT > 0 THEN
                  FORALL i IN l_team_users.FIRST .. l_team_users.LAST
                     INSERT INTO work_activity_audit (facility_id,
                                                      container_id,
                                                      activity_code,
                                                      start_ts,
                                                      action_ts,
                                                      status,
                                                      dc_id,
                                                      wh_id,
                                                      zone,
                                                      location_id,
                                                      operator,
                                                      total_unit_qty,
                                                      num_lpn,
                                                      units_worked,
                                                      container_worked,
                                                      units_remaining,
                                                      container_remaining,
                                                      item_id,
                                                      prepack_qty,
                                                      nested_qty,
                                                      supervisor,
                                                      pay_variable_codes,
                                                      route,
                                                      trailer_id,
                                                      terminated_by,
                                                      user_sharing,
                                                      team_id)
                                              VALUES (i_facility_id,
                                                      i_container_id,
                                                      'CNTLDG',
                                                      l_start_ts,
                                                      l_start_ts,
                                                      'S',
                                                      l_dc_id,
                                                      l_wh_id,
                                                      l_zone,
                                                      i_door_id,
                                                      l_team_users(i),
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      l_route,
                                                      l_trailer_id,
                                                      NULL,
                                                      i_num_users,
                                                      l_team_id);
                  COMMIT;
               END IF;
            END IF;
            -- Get the User ID of the remaining users
            l_team_id := NULL;
            
            -- The count of comma corresponds to the number of remaining users + 1 (ex. '999878,911217,994578)
            SELECT LENGTH(NVL(i_remaining_users,0)) - LENGTH(REPLACE(NVL(i_remaining_users,0),',',NULL)) INTO l_comma_count
              FROM DUAL;
            
            l_start := 1;
            l_occurence := 1;
            l_length :=1;
            FOR i IN 1 .. l_comma_count LOOP
               l_team_id := NULL;
               l_comma_pos := INSTR(i_remaining_users, ',',l_start, l_occurence);
               l_user_id := SUBSTR(i_remaining_users, l_start, l_comma_pos - l_length);
               
               l_start := l_comma_pos + 1;
               l_length := l_comma_pos + 1;
               
               OPEN c_get_team_id(l_user_id);
               FETCH c_get_team_id INTO l_team_id;
               CLOSE c_get_team_id;
               -- Assign First CID to each remaining user
               INSERT INTO work_activity_audit (facility_id,
                                                container_id,
                                                activity_code,
                                                start_ts,
                                                action_ts,
                                                status,
                                                dc_id,
                                                wh_id,
                                                zone,
                                                location_id,
                                                operator,
                                                total_unit_qty,
                                                num_lpn,
                                                units_worked,
                                                container_worked,
                                                units_remaining,
                                                container_remaining,
                                                item_id,
                                                prepack_qty,
                                                nested_qty,
                                                supervisor,
                                                pay_variable_codes,
                                                route,
                                                trailer_id,
                                                terminated_by,
                                                user_sharing,
                                                team_id)
                                        VALUES (i_facility_id,
                                                i_container_id,
                                                'CNTLDG',
                                                l_start_ts,
                                                l_start_ts,
                                                'S',
                                                l_dc_id,
                                                l_wh_id,
                                                l_zone,
                                                i_door_id,
                                                l_user_id,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                l_route,
                                                l_trailer_id,
                                                NULL,
                                                i_num_users,
                                                l_team_id);
               COMMIT;
            END LOOP;
            -- Assign a First CID to the last remaining user
            l_user_id := TRIM(SUBSTR(i_remaining_users,l_start));
            l_team_id := NULL;
            OPEN c_get_team_id(l_user_id);
            FETCH c_get_team_id INTO l_team_id;
            CLOSE c_get_team_id;
            
            INSERT INTO work_activity_audit (facility_id,
                                             container_id,
                                             activity_code,
                                             start_ts,
                                             action_ts,
                                             status,
                                             dc_id,
                                             wh_id,
                                             zone,
                                             location_id,
                                             operator,
                                             total_unit_qty,
                                             num_lpn,
                                             units_worked,
                                             container_worked,
                                             units_remaining,
                                             container_remaining,
                                             item_id,
                                             prepack_qty,
                                             nested_qty,
                                             supervisor,
                                             pay_variable_codes,
                                             route,
                                             trailer_id,
                                             terminated_by,
                                             user_sharing,
                                             team_id)
                                     VALUES (i_facility_id,
                                             i_container_id,
                                             'CNTLDG',
                                             l_start_ts,
                                             l_start_ts,
                                             'S',
                                             l_dc_id,
                                             l_wh_id,
                                             l_zone,
                                             i_door_id,
                                             l_user_id,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             l_route,
                                             l_trailer_id,
                                             NULL,
                                             i_num_users,
                                             l_team_id);
            COMMIT;
         END IF;
         RETURN ('Y');

      EXCEPTION
         WHEN OTHERS THEN
            log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.assign_first_cid', SQLERRM);
            RETURN ('N');
      END assign_first_cid;
      
   FUNCTION assign_last_cid (i_facility_id            IN facility.facility_id%TYPE,
                             i_user_id                IN dms_user.user_id%TYPE,
                             i_user_to_end            IN dms_user.user_id%TYPE,
                             i_scanned_users          IN OUT VARCHAR2,
                             i_unscanned_users        IN OUT VARCHAR2,
                             i_door_id                IN door.door_id%TYPE,
                             i_first_container_id     IN container.container_id%TYPE,
                             i_last_container_id      IN container.container_id%TYPE,
                             i_action_type            IN VARCHAR2,
                             o_count_remaining_users  IN OUT NUMBER,
                             o_remaining_users        IN OUT VARCHAR2,
                             o_next_container_id      IN OUT container.container_id%TYPE,
                             o_error_msg_code         IN OUT user_message.message_code%TYPE,
                             o_error_msg_typ          IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 IS
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Call this function when assigning a Last CID to the user scanning
*                                                  out of the door. This should also be invoked to end the sessions
*                                                  of the remaining users at the door.Once the remaining users' 
*                                                  sessions are ended, it should be reopened through assign_first_cid.
*                                                  This function should also be invoked when a user scans in to 
*                                                  a door and say all the users that the system expects are not there. 
*                                                  Or when the badges that the system expects to be scanned were not
*                                                  scanned.
* PARAMETERS:
* i_facility_id          - Pass the facility_id of the session
* i_user_id              - Pass the user scanning in. This will also be used to populate the Terminated_by column
* i_user_to_end          - Pass ALL to end all the open sessions at the door. Pass ONE to check if the user scanning in 
*                          has open session and if conveyor cutoff was performed.
* i_scanned_users        - Pass all the user_IDs of the scanned badges, delimited by comma. Pass NULL if no scanned badges.
* i_unscanned_users      - Pass all the user_IDs of the unscanned badges, delimited by comma. Pass NULL if no unscanned badges.
* i_door_id              - Pass the door ID where the user is scanning in or out.
* i_first_container_id   - Pass the container_ID scanned by the user during Scan In.
* i_last_container_id    - Pass the container_ID scanned by the user during Scan Out.
* i_action_type          - Pass SCAN_IN or SCAN_OUT
* o_count_remaining_users - The number of remaining users will be returned back by the procedure. This will be the count of all
*                         users at the door minus the user scanning out.
* o_remaining_users      - The remaining User IDs, delimited by comma, will be returned back by the procedure. This will be all
*                         users at the door minus the user scanning out.
* o_next_container_id    - The First CID that will be used to reopen the remaining users' session will be returned back by the procedure.
* o_error_msg_code       - The error code will be returned back to the calling program
* o_error_msg_typ        - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/

      CURSOR c_get_open_xsaction (user_id_in IN dms_user.user_id%TYPE) IS
      SELECT status, start_ts, dc_id, wh_id, zone, location_id, operator, user_sharing, team_id
        FROM work_activity_audit w
       WHERE w.facility_id = i_facility_id
         AND w.operator = user_id_in
         AND w.activity_code = 'CNTLDG'
         AND w.status = 'S'
         AND NOT EXISTS (SELECT 'x'
                           FROM work_activity_audit w2
                          WHERE w2.facility_id = i_facility_id
                            AND w2.activity_code = 'CNTLDG'
                            AND w2.operator = user_id_in
                            AND w2.status <> 'S'
                            AND w2.location_id = w.location_id
                            AND w.start_ts = w2.start_ts);
      -- joined container to make sure the last CID that will be fetched is M-status
      CURSOR c_get_last_cid (container_id_in IN container.container_id%TYPE,
                             location_id_in  IN location.location_id%TYPE) IS
      SELECT a.container_id
        FROM (SELECT *
                FROM container_event
               WHERE facility_id = i_facility_id
                 AND event_code in ('72','75')
                 AND user_id = '3RD PARTY'
                 AND location_id = location_id_in
                 AND divert_ts <= (SELECT divert_ts
                                     FROM container_event
                                    WHERE facility_id = i_facility_id
                                      AND event_code in ('72','75')
                                      AND user_id = '3RD PARTY'
                                      AND container_id = container_id_in
                                      AND location_id = location_id_in)
                 AND sort_order < (SELECT sort_order
                                     FROM container_event
                                    WHERE facility_id = i_facility_id
                                      AND event_code in ('72','75')
                                      AND user_id = '3RD PARTY'
                                      AND container_id = container_id_in
                                      AND location_id = location_id_in)
                 ORDER BY divert_ts desc, sort_order desc) a, container b
       WHERE a.facility_id = b.facility_id
         AND a.container_id = b.container_id
         AND b.container_status = 'M'
         AND a.divert_ts = b.divert_ts
         AND a.location_id = b.location_id
         AND a.sort_order = b.sort_order
         AND ROWNUM < 2;
       
      CURSOR c_get_next_cid (container_id_in IN container.container_id%TYPE,
                             location_id_in  IN location.location_id%TYPE) IS
       SELECT  a.container_id
        FROM (SELECT *
                FROM container_event
               WHERE facility_id = i_facility_id
                 AND event_code in ('72','75')
                 AND user_id = '3RD PARTY'
                 AND location_id = location_id_in
                 AND divert_ts >= (SELECT divert_ts
                                     FROM container_event
                                    WHERE facility_id = i_facility_id
                                      AND event_code in ('72','75')
                                      AND user_id = '3RD PARTY'
                                      AND container_id = container_id_in
                                      AND location_id = location_id_in)
                 AND sort_order > (SELECT sort_order
                                     FROM container_event
                                    WHERE facility_id = i_facility_id
                                      AND event_code in ('72','75')
                                      AND user_id = '3RD PARTY'
                                      AND container_id = container_id_in
                                      AND location_id = location_id_in)
                 ORDER BY divert_ts, sort_order) a, container b
       WHERE a.facility_id = b.facility_id
         AND a.container_id = b.container_id
         AND b.container_status = 'M'
         AND a.divert_ts = b.divert_ts
         AND a.location_id = b.location_id
         AND a.sort_order = b.sort_order
         AND ROWNUM < 2;
      
      CURSOR c_get_door_trailer (container_id_in IN container.container_id%TYPE) IS
      SELECT m.trailer_id, c.route, c.location_id
        FROM container c, manifest m
       WHERE c.facility_id = i_facility_id
         AND m.facility_id = i_facility_id
         AND c.container_id = container_id_in
         AND c.location_id = m.door_id
         AND c.bol_nbr = m.bol_nbr
         AND rownum < 2;
      
      CURSOR c_check_cutoff (l_start_ts IN work_activity_audit.start_ts%TYPE) IS
      SELECT * 
        FROM (SELECT container_id
                FROM container_event
               WHERE facility_id = i_facility_id
                 AND event_code = '28'
                 AND location_id = i_door_id
                 AND action_ts >= l_start_ts
                 ORDER BY sort_order, action_ts)
       WHERE ROWNUM < 2;
             
      CURSOR c_get_all_open_users (door_id_in IN door.door_id%TYPE) IS
         SELECT status, start_ts, dc_id, wh_id, zone, location_id, operator, user_sharing, team_id
           FROM work_activity_audit w
          WHERE w.facility_id = i_facility_id
            AND w.location_id = door_id_in
            AND w.activity_code = 'CNTLDG'
            AND w.status = 'S'
            AND NOT EXISTS (SELECT 'x'
                              FROM work_activity_audit w2
                             WHERE w2.facility_id = i_facility_id
                               AND w2.activity_code = 'CNTLDG'
                               AND w2.location_id = door_id_in
                               AND w2.status <> 'S'
                               AND w.start_ts = w2.start_ts
                               AND w.operator = w2.operator);
      
      CURSOR c_get_team_users (l_team_id_in IN dms_team_user.team_id%TYPE)IS
      SELECT user_id
        FROM dms_team_user dtu
       WHERE dtu.facility_id = i_facility_id
         AND dtu.team_id = l_team_id_in
         AND dtu.mstr_bol_id = '0'
         AND dtu.create_date IS NOT NULL
         AND dtu.end_date IS NULL;
         
      CURSOR c_get_team_id (l_user_id_in IN dms_team_user.user_id%TYPE) IS
      SELECT team_id
        FROM dms_team_user dtu
       WHERE dtu.facility_id = i_facility_id
         AND dtu.user_id = l_user_id_in
         AND dtu.mstr_bol_id = '0'
         AND dtu.create_date IS NOT NULL
         AND dtu.end_date IS NULL;
         
      l_start_ts                work_activity_audit.start_ts%TYPE;
      l_dc_id                   work_activity_audit.dc_id%TYPE;
      l_wh_id                   work_activity_audit.wh_id%TYPE;
      l_zone                    work_activity_audit.zone%TYPE;
      l_location_id             work_activity_audit.location_id%TYPE;
      l_operator                work_activity_audit.operator%TYPE;
      l_user_sharing            work_activity_audit.user_sharing%TYPE;
      l_team_id                 dms_team_user.team_id%TYPE;
      l_trailer_id              work_activity_audit.trailer_id%TYPE;
      l_route                   work_activity_audit.route%TYPE;
      l_door_id                 work_activity_audit.location_id%TYPE;
      l_cutoff_carton           container.container_id%TYPE;
      l_status                  work_activity_audit.status%TYPE;
      l_last_cid                container.container_id%TYPE;
      l_trlr_found              BOOLEAN;
      l_last_cid_found          BOOLEAN;
      l_cutoff_performed        BOOLEAN;
      l_ret_val                 VARCHAR2(1);
      
      TYPE l_status_t IS TABLE OF work_activity_audit.status%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_start_Ts_t IS TABLE OF work_activity_audit.start_Ts%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_dc_id_t IS TABLE OF work_activity_audit.dc_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_wh_id_t IS TABLE OF work_activity_audit.wh_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_zone_t IS TABLE OF work_activity_audit.zone%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_location_id_t IS TABLE OF work_activity_audit.location_Id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_operator_t IS TABLE OF work_activity_audit.operator%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_user_sharing_t IS TABLE OF work_activity_audit.user_sharing%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_team_id_t IS TABLE OF work_activity_audit.team_id%TYPE INDEX BY BINARY_INTEGER;
      lt_status         l_status_t;
      lt_start_Ts       l_start_Ts_t;
      lt_dc_id          l_dc_id_t;
      lt_wh_id          l_wh_id_t;
      lt_zone           l_zone_t;
      lt_location_id    l_location_id_t;
      lt_operator       l_operator_t;
      lt_user_sharing   l_user_sharing_t;
      lt_team_id        l_team_id_t;
      l_next_cid_found          BOOLEAN;
      l_comma_count             NUMBER;
      l_open_found              BOOLEAN;
      l_statement               VARCHAR2(2000);
      l_user_id_not_on_team     BOOLEAN;
      l_end_session             VARCHAR2(1);
      l_count_remaining_users   NUMBER := 0;
      l_remaining_users         VARCHAR2(2000) := NULL;
      l_scanned_users           VARCHAR2(2000);
      l_unscanned_users         VARCHAR2(2000);
       

      BEGIN
         -- ALL means to assign last carton to all the users at the door
         IF i_user_to_end = 'ALL' THEN 
            -- Get the Last CID to be assigned to the remaining users at the door (container scanned  minus 1)
            OPEN c_get_last_cid(i_first_container_id, i_door_id);
            FETCH c_get_last_cid INTO l_last_cid;
            l_last_cid_found := c_get_last_cid%FOUND;
            CLOSE c_get_last_cid;
            -- Get the trailer, route and door of the carton from previous query
            OPEN c_get_door_trailer(l_last_cid);
            FETCH c_get_door_trailer INTO l_trailer_id,
                                          l_route,
                                          l_door_id;
            l_trlr_found := c_get_door_trailer%FOUND;
            CLOSE c_get_door_trailer;
            -- If a user is scanning in and both scanned and unscanned users are NULL (this means all the users' sessions at the door will be ended)
            IF i_action_type = 'SCAN_IN' AND i_scanned_users IS NULL AND i_unscanned_users IS NULL THEN
               
               IF l_last_cid_found THEN
                  FOR i IN c_get_all_open_users(i_door_id) LOOP
                     INSERT INTO work_activity_audit (facility_id,
                                                      container_id,
                                                      activity_code,
                                                      start_ts,
                                                      action_ts,
                                                      status,
                                                      dc_id,
                                                      wh_id,
                                                      zone,
                                                      location_id,
                                                      operator,
                                                      total_unit_qty,
                                                      num_lpn,
                                                      units_worked,
                                                      container_worked,
                                                      units_remaining,
                                                      container_remaining,
                                                      item_id,
                                                      prepack_qty,
                                                      nested_qty,
                                                      supervisor,
                                                      pay_variable_codes,
                                                      route,
                                                      trailer_id,
                                                      terminated_by,
                                                      user_sharing,
                                                      team_id)
                                              VALUES (i_facility_id,
                                                      l_last_cid,
                                                      'CNTLDG',
                                                      i.start_ts,
                                                      sysdate,
                                                      'C',
                                                      i.dc_id,
                                                      i.wh_id,
                                                      i.zone,
                                                      i_door_id,
                                                      i.operator,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      l_route,
                                                      l_trailer_id,
                                                      i_user_id,
                                                      i.user_sharing,
                                                      i.team_id);
                     COMMIT;
                  END LOOP;
               END IF;
               RETURN ('Y');
            -- If the user is scanning in and he scanned some of all of the users at the door, we need to assign a last CID to all users
            ELSIF i_action_type = 'SCAN_IN' AND (i_scanned_users IS NOT NULL OR i_unscanned_users IS NOT NULL) THEN

               IF i_scanned_users IS NOT NULL THEN
                  -- Get the scanned users and add single quotes to each user ID (ex. '999878, 911217' to '999878', '911217')
                  l_scanned_users := REPADMIN.fn_add_quotes (i_scanned_users, 1);
                  
                  -- Get most recent open session of the scanned users
                  BEGIN
                     l_statement := 'SELECT status, start_ts, dc_id, wh_id, zone, location_id, operator, user_sharing, team_id ' ||
                                      'FROM work_activity_audit w ' ||
                                     'WHERE w.facility_id = ''' || i_facility_id ||''' ' ||
                                       'AND w.operator IN ( ' || l_scanned_users || ') ' ||
                                       'AND w.activity_code = ''CNTLDG'' ' ||
                                       'AND w.status = ''S'' ' ||
                                       'AND NOT EXISTS (SELECT ''x'' ' ||
                                                         'FROM work_activity_audit w2 ' ||
                                                        'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                          'AND w2.activity_code = ''CNTLDG'' ' ||
                                                          'AND w2.operator IN ( ' || l_scanned_users || ') ' ||
                                                          'AND w2.status <> ''S'' ' ||
                                                          'AND w.start_ts = w2.start_ts)';

                     EXECUTE IMMEDIATE l_statement BULK COLLECT INTO lt_status,
                                                                     lt_start_Ts,
                                                                     lt_dc_id,
                                                                     lt_wh_id,
                                                                     lt_zone,
                                                                     lt_location_id,
                                                                     lt_operator,
                                                                     lt_user_sharing,
                                                                     lt_team_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
                  END;
                  -- If the scanned users had open session, assign a Last CID using their previous trailer, route, user_sharing, etc.
                  IF lt_status.COUNT > 0 AND l_last_cid_found THEN
                     FORALL i IN lt_status.FIRST .. lt_status.LAST
                        INSERT INTO work_activity_audit (facility_id,
                                                         container_id,
                                                         activity_code,
                                                         start_ts,
                                                         action_ts,
                                                         status,
                                                         dc_id,
                                                         wh_id,
                                                         zone,
                                                         location_id,
                                                         operator,
                                                         total_unit_qty,
                                                         num_lpn,
                                                         units_worked,
                                                         container_worked,
                                                         units_remaining,
                                                         container_remaining,
                                                         item_id,
                                                         prepack_qty,
                                                         nested_qty,
                                                         supervisor,
                                                         pay_variable_codes,
                                                         route,
                                                         trailer_id,
                                                         terminated_by,
                                                         user_sharing,
                                                         team_id)
                                                 VALUES (i_facility_id,
                                                         l_last_cid,
                                                         'CNTLDG',
                                                         lt_start_ts(i),
                                                         sysdate,
                                                         'C',
                                                         lt_dc_id(i),
                                                         lt_wh_id(i),
                                                         lt_zone(i),
                                                         i_door_id,
                                                         lt_operator(i),
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         l_route,
                                                         l_trailer_id,
                                                         'SYSTEM',
                                                         lt_user_sharing(i),
                                                         lt_team_id(i));
                     COMMIT;
                  END IF; 
               END IF;
               
               IF i_unscanned_users IS NOT NULL THEN
                  -- Get the unscanned users and add single quotes to each user ID (ex. '999878, 911217' to '999878', '911217')
                  l_unscanned_users := REPADMIN.fn_add_quotes (NVL(i_unscanned_users,0), 1);
                  -- Get most recent open session of the unscanned users
                  BEGIN
                     l_statement := 'SELECT status, start_ts, dc_id, wh_id, zone, location_id, operator, user_sharing, team_id ' ||
                                      'FROM work_activity_audit w ' ||
                                     'WHERE w.facility_id = ''' || i_facility_id ||''' ' ||
                                       'AND w.operator IN ( ' || l_unscanned_users || ') ' ||
                                       'AND w.activity_code = ''CNTLDG'' ' ||
                                       'AND w.status = ''S'' ' ||
                                       'AND NOT EXISTS (SELECT ''x'' ' ||
                                                         'FROM work_activity_audit w2 ' ||
                                                        'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                          'AND w2.activity_code = ''CNTLDG'' ' ||
                                                          'AND w2.operator IN ( ' || l_unscanned_users || ') ' ||
                                                          'AND w2.status <> ''S'' ' ||
                                                          'AND w.start_ts = w2.start_ts)';
                     EXECUTE IMMEDIATE l_statement BULK COLLECT INTO lt_status,
                                                                     lt_start_Ts,
                                                                     lt_dc_id,
                                                                     lt_wh_id,
                                                                     lt_zone,
                                                                     lt_location_id,
                                                                     lt_operator,
                                                                     lt_user_sharing,
                                                                     lt_team_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN                       
                     NULL;
                  END;
                  -- If the unscanned users had open session, assign a Last CID using their previous trailer, route, user_sharing, etc.
                  IF lt_status.COUNT > 0 AND l_last_cid_found THEN
                     FORALL i IN lt_status.FIRST .. lt_status.LAST
                        INSERT INTO work_activity_audit (facility_id,
                                                         container_id,
                                                         activity_code,
                                                         start_ts,
                                                         action_ts,
                                                         status,
                                                         dc_id,
                                                         wh_id,
                                                         zone,
                                                         location_id,
                                                         operator,
                                                         total_unit_qty,
                                                         num_lpn,
                                                         units_worked,
                                                         container_worked,
                                                         units_remaining,
                                                         container_remaining,
                                                         item_id,
                                                         prepack_qty,
                                                         nested_qty,
                                                         supervisor,
                                                         pay_variable_codes,
                                                         route,
                                                         trailer_id,
                                                         terminated_by,
                                                         user_sharing,
                                                         team_id)
                                                 VALUES (i_facility_id,
                                                         l_last_cid,
                                                         'CNTLDG',
                                                         lt_start_ts(i),
                                                         sysdate,
                                                         'C',
                                                         lt_dc_id(i),
                                                         lt_wh_id(i),
                                                         lt_zone(i),
                                                         i_door_id,
                                                         lt_operator(i),
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         l_route,
                                                         l_trailer_id,
                                                         i_user_id,
                                                         lt_user_sharing(i),
                                                         lt_team_id(i));
                     COMMIT;
                  END IF; 
               END IF;
               RETURN ('Y');
            -- If the user is scanning out, we need to end the session of all the users at the door
            ELSIF i_action_type = 'SCAN_OUT' THEN
               -- Get the trailer, route and door of the carton scanned by the user
               OPEN c_get_door_trailer(i_last_container_id);
               FETCH c_get_door_trailer INTO l_trailer_id,
                                             l_route,
                                             l_door_id;
               l_trlr_found := c_get_door_trailer%FOUND;
               CLOSE c_get_door_trailer;
               
               IF i_last_container_id IS NOT NULL THEN
                  -- Get the team ID of the scanning-out user
                  OPEN c_get_team_id (i_user_id);
                  FETCH c_get_team_id INTO l_team_id;
                  l_user_id_not_on_team := c_get_team_id%NOTFOUND;
                  CLOSE c_get_team_id;
                  
                  IF l_user_id_not_on_team THEN
                     -- Assign a last CID to all current users at the door
                     FOR i IN c_get_all_open_users(i_door_id) LOOP
                        INSERT INTO work_activity_audit (facility_id,
                                                         container_id,
                                                         activity_code,
                                                         start_ts,
                                                         action_ts,
                                                         status,
                                                         dc_id,
                                                         wh_id,
                                                         zone,
                                                         location_id,
                                                         operator,
                                                         total_unit_qty,
                                                         num_lpn,
                                                         units_worked,
                                                         container_worked,
                                                         units_remaining,
                                                         container_remaining,
                                                         item_id,
                                                         prepack_qty,
                                                         nested_qty,
                                                         supervisor,
                                                         pay_variable_codes,
                                                         route,
                                                         trailer_id,
                                                         terminated_by,
                                                         user_sharing,
                                                         team_id)
                                                 VALUES (i_facility_id,
                                                         i_last_container_id,
                                                         'CNTLDG',
                                                         i.start_ts,
                                                         sysdate,
                                                         'C',
                                                         i.dc_id,
                                                         i.wh_id,
                                                         i.zone,
                                                         i_door_id,
                                                         i.operator,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         l_route,
                                                         l_trailer_id,
                                                         DECODE(i.operator, i_user_id, i_user_id, 'SYSTEM'),
                                                         i.user_sharing,
                                                         i.team_id);
                        IF i.operator <> i_user_id THEN
                           l_count_remaining_users := l_count_remaining_users + 1;
                           l_remaining_users := l_remaining_users || ',' || i.operator;
                        END IF;
                        COMMIT;
                     END LOOP;
                  ELSE
                     FOR i IN c_get_team_users(l_team_id) LOOP
                        OPEN c_get_open_xsaction (i.user_id);
                        FETCH c_get_open_xsaction INTO l_status,
                                                       l_start_ts,
                                                       l_dc_id,
                                                       l_wh_id,
                                                       l_zone,
                                                       l_location_id,
                                                       l_operator,
                                                       l_user_sharing,
                                                       l_team_id;
                        l_open_found := c_get_open_xsaction%FOUND;
                        CLOSE c_get_open_xsaction;
                        
                        IF l_open_found THEN
                        -- Assign a last CID to all team members of the scanning-out user at the door
                           INSERT INTO work_activity_audit (facility_id,
                                                            container_id,
                                                            activity_code,
                                                            start_ts,
                                                            action_ts,
                                                            status,
                                                            dc_id,
                                                            wh_id,
                                                            zone,
                                                            location_id,
                                                            operator,
                                                            total_unit_qty,
                                                            num_lpn,
                                                            units_worked,
                                                            container_worked,
                                                            units_remaining,
                                                            container_remaining,
                                                            item_id,
                                                            prepack_qty,
                                                            nested_qty,
                                                            supervisor,
                                                            pay_variable_codes,
                                                            route,
                                                            trailer_id,
                                                            terminated_by,
                                                            user_sharing,
                                                            team_id)
                                                    VALUES (i_facility_id,
                                                            i_last_container_id,
                                                            'CNTLDG',
                                                            l_start_ts,
                                                            sysdate,
                                                            'C',
                                                            l_dc_id,
                                                            l_wh_id,
                                                            l_zone,
                                                            i_door_id,
                                                            l_operator,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            l_route,
                                                            l_trailer_id,
                                                            i_user_Id,
                                                            l_user_sharing,
                                                            l_team_id);
                           COMMIT;
                        END IF;
                     END LOOP;
                     
                     FOR i IN c_get_all_open_users(i_door_id) LOOP
                     -- Assign a last CID to all remaining user at the door
                        INSERT INTO work_activity_audit (facility_id,
                                                         container_id,
                                                         activity_code,
                                                         start_ts,
                                                         action_ts,
                                                         status,
                                                         dc_id,
                                                         wh_id,
                                                         zone,
                                                         location_id,
                                                         operator,
                                                         total_unit_qty,
                                                         num_lpn,
                                                         units_worked,
                                                         container_worked,
                                                         units_remaining,
                                                         container_remaining,
                                                         item_id,
                                                         prepack_qty,
                                                         nested_qty,
                                                         supervisor,
                                                         pay_variable_codes,
                                                         route,
                                                         trailer_id,
                                                         terminated_by,
                                                         user_sharing,
                                                         team_id)
                                                 VALUES (i_facility_id,
                                                         i_last_container_id,
                                                         'CNTLDG',
                                                         i.start_ts,
                                                         sysdate,
                                                         'C',
                                                         i.dc_id,
                                                         i.wh_id,
                                                         i.zone,
                                                         i_door_id,
                                                         i.operator,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         l_route,
                                                         l_trailer_id,
                                                         'SYSTEM',
                                                         i.user_sharing,
                                                         i.team_id);
                        --Count the remaining users at the door as we need to return this back to the form
                        l_count_remaining_users := l_count_remaining_users + 1;
                        --Get the user IDs of the remaining users at the door as we need to return this back to the form
                        l_remaining_users := l_remaining_users || ',' || i.operator;
                        COMMIT;
                     END LOOP;
                  END IF;
               END IF;
               -- Get the next CID that will be used to reopen the sessions of the remaining users at the door.
               OPEN c_get_next_cid (i_last_container_id, i_door_id);
               FETCH c_get_next_cid INTO o_next_container_id;
               l_next_cid_found := c_get_next_cid%FOUND;
               CLOSE c_get_next_cid;
               
               o_count_remaining_users := l_count_remaining_users;
               o_remaining_users := trim(BOTH ',' FROM l_remaining_users);
            
               RETURN ('Y');
            END IF;
         -- 'ONE' means the procedure is invoked when the user first accessed the screen. The procedure will check
         -- if conveyor cutoff was done at the door after the user has scanned in and did not scan out.
         ELSIF i_user_to_end = 'ONE' THEN 
            -- Get the team ID of the user
            OPEN c_get_team_id (i_user_id);
            FETCH c_get_team_id INTO l_team_id;
            l_user_id_not_on_team := c_get_team_id%NOTFOUND;
            CLOSE c_get_team_id;
            
            -- Get the user's open session (S)
            IF l_user_id_not_on_team THEN
               OPEN c_get_open_xsaction(i_user_id);
               FETCH c_get_open_xsaction INTO l_status,
                                              l_start_ts,
                                              l_dc_id,
                                              l_wh_id,
                                              l_zone,
                                              l_location_id,
                                              l_operator,
                                              l_user_sharing,
                                              l_team_id;
               l_open_found :=    c_get_open_xsaction%FOUND;
               CLOSE c_get_open_xsaction;
               
               -- If user doesn't have an open session then return Y
               IF NOT l_open_found THEN
                  RETURN ('Y');
               END IF;
               
               -- Check if conveyor cutoff was done at the door after the user's scan-in
               OPEN c_check_cutoff (l_start_ts);
               FETCH c_check_cutoff INTO l_cutoff_carton;
               l_cutoff_performed := c_check_cutoff%FOUND;
               CLOSE c_check_cutoff;
               -- If cutoff was not done, return Y
               IF NOT l_cutoff_performed THEN
                  RETURN ('Y');
               END IF;
               -- If there was a cutoff at the door, get the last CID that will be used to assign to the user
               OPEN c_get_last_cid (l_cutoff_carton, l_location_id);
               FETCH c_get_last_cid INTO l_last_cid;
               l_last_cid_found := c_get_last_cid%FOUND;
               CLOSE c_get_last_cid;
               -- Get the trailer, route and door ID
               OPEN c_get_door_trailer(l_last_cid);
               FETCH c_get_door_trailer INTO l_trailer_id,
                                             l_route,
                                             l_door_id;
               l_trlr_found := c_get_door_trailer%FOUND;
               CLOSE c_get_door_trailer;
               -- Assign a last CID to the user because conveyor cutoff was done after his/her scan-in and he did not scan out.
               IF l_last_cid_found THEN
                  INSERT INTO work_activity_audit (facility_id,
                                                   container_id,
                                                   activity_code,
                                                   start_ts,
                                                   action_ts,
                                                   status,
                                                   dc_id,
                                                   wh_id,
                                                   zone,
                                                   location_id,
                                                   operator,
                                                   total_unit_qty,
                                                   num_lpn,
                                                   units_worked,
                                                   container_worked,
                                                   units_remaining,
                                                   container_remaining,
                                                   item_id,
                                                   prepack_qty,
                                                   nested_qty,
                                                   supervisor,
                                                   pay_variable_codes,
                                                   route,
                                                   trailer_id,
                                                   terminated_by,
                                                   user_sharing,
                                                   team_id)
                                           VALUES (i_facility_id,
                                                   l_last_cid,
                                                   'CNTLDG',
                                                   l_start_ts,
                                                   sysdate,
                                                   'C',
                                                   l_dc_id,
                                                   l_wh_id,
                                                   l_zone,
                                                   l_location_id,
                                                   i_user_id,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   l_route,
                                                   l_trailer_id,
                                                   'SYSTEM',
                                                   l_user_sharing,
                                                   NULL);
                                           COMMIT;
                                           l_ret_val := 'C'; 
               END IF;
            ELSE
               OPEN c_get_open_xsaction(i_user_id);
               FETCH c_get_open_xsaction INTO l_status,
                                              l_start_ts,
                                              l_dc_id,
                                              l_wh_id,
                                              l_zone,
                                              l_location_id,
                                              l_operator,
                                              l_user_sharing,
                                              l_team_id;
               l_open_found :=    c_get_open_xsaction%FOUND;
               CLOSE c_get_open_xsaction;
               
               OPEN c_check_cutoff (l_start_ts);
               FETCH c_check_cutoff INTO l_cutoff_carton;
               l_cutoff_performed := c_check_cutoff%FOUND;
               CLOSE c_check_cutoff;
               
               IF NOT l_cutoff_performed THEN
                  RETURN ('Y');
               END IF;
               
               OPEN c_get_last_cid (l_cutoff_carton, l_location_id);
               FETCH c_get_last_cid INTO l_last_cid;
               l_last_cid_found := c_get_last_cid%FOUND;
               CLOSE c_get_last_cid;
               
               OPEN c_get_door_trailer(l_last_cid);
               FETCH c_get_door_trailer INTO l_trailer_id,
                                             l_route,
                                             l_door_id;
               l_trlr_found := c_get_door_trailer%FOUND;
               CLOSE c_get_door_trailer;
                  
               FOR i IN c_get_team_users(l_team_id) LOOP
                  OPEN c_get_open_xsaction (i.user_id);
                  FETCH c_get_open_xsaction INTO l_status,
                                                 l_start_ts,
                                                 l_dc_id,
                                                 l_wh_id,
                                                 l_zone,
                                                 l_location_id,
                                                 l_operator,
                                                 l_user_sharing,
                                                 l_team_id;
                  l_open_found := c_get_open_xsaction%FOUND;
                  CLOSE c_get_open_xsaction;
                  
                  IF NOT l_open_found THEN
                     l_ret_val := 'Y'; 
                     EXIT;
                  END IF;
                  
                  IF l_last_cid_found THEN
                     INSERT INTO work_activity_audit (facility_id,
                                                      container_id,
                                                      activity_code,
                                                      start_ts,
                                                      action_ts,
                                                      status,
                                                      dc_id,
                                                      wh_id,
                                                      zone,
                                                      location_id,
                                                      operator,
                                                      total_unit_qty,
                                                      num_lpn,
                                                      units_worked,
                                                      container_worked,
                                                      units_remaining,
                                                      container_remaining,
                                                      item_id,
                                                      prepack_qty,
                                                      nested_qty,
                                                      supervisor,
                                                      pay_variable_codes,
                                                      route,
                                                      trailer_id,
                                                      terminated_by,
                                                      user_sharing,
                                                      team_id)
                                              VALUES (i_facility_id,
                                                      l_last_cid,
                                                      'CNTLDG',
                                                      l_start_ts,
                                                      sysdate,
                                                      'C',
                                                      l_dc_id,
                                                      l_wh_id,
                                                      l_zone,
                                                      i_door_id,
                                                      l_operator,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      l_route,
                                                      l_trailer_id,
                                                      'SYSTEM',
                                                      l_user_sharing,
                                                      l_team_id);
                                                      l_ret_val := 'C'; 
                     COMMIT;
                  END IF;
               END LOOP;
            END IF;
         END IF;

         RETURN l_ret_val;
         
      EXCEPTION
         WHEN OTHERS THEN
            ROLLBACK;
            log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.assign_last_cid', SQLERRM);
            RETURN ('N');
      END assign_last_cid;

   PROCEDURE populate_team (i_facility_id      IN facility.facility_id%TYPE,
                            i_team_ind         IN VARCHAR2,
                            i_user_id          IN dms_user.user_id%TYPE,
                            o_team_id          IN OUT dms_team_user.team_id%TYPE,
                            o_open_xsaction    IN OUT VARCHAR2,
                            o_error_msg_code   IN OUT user_message.message_code%TYPE,
                            o_error_msg_typ    IN OUT user_message.message_type%TYPE) IS
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Procedure created to generate the team ID if user is not yet
*                                                  part of the team or to pull the team details of the user.
* i_facility_id           - Pass facility_id of the session
* i_user_id               - Pass the user ID of the owner of the session.
* o_team_id               - If the user is currently part of a team, the team_ID of the user will be returned by the procedure
* o_open_xsaction         - 'Y' will be returned if the user has open transaction. Otherwise, N will be returned.
* o_error_msg_code        - The error code will be returned back to the calling program
* o_error_msg_typ         - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/
   CURSOR c_get_team_id IS
   SELECT team_id
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.user_id = i_user_id
      AND dtu.mstr_bol_id = '0'
      AND dtu.create_date IS NOT NULL
      AND dtu.end_date IS NULL;

   CURSOR c_generate_team_id IS
   SELECT nvl(max(to_number(team_id)),0)  + 1 team_id
     FROM dms_team dt
    WHERE dt.facility_id = i_facility_id
      AND dt.mstr_bol_id = '0'
      AND dt.trailer_id = '0'
      AND dt.carton_unload_qty = '0';
      
   CURSOR c_get_bulk_team_id IS
   SELECT to_number(team_id), end_date
     FROM dms_team dt
    WHERE dt.facility_id = i_facility_id
      AND dt.mstr_bol_id = '0'
      AND dt.trailer_id = '0'
      AND dt.carton_unload_qty = '0'
    ORDER BY to_number(team_id);

      CURSOR c_get_open_xsaction (user_id_in IN dms_user.user_id%TYPE) IS
      SELECT 'Y'
        FROM work_activity_audit w
       WHERE w.facility_id = i_facility_id
         AND w.operator = user_id_in
         AND w.activity_code = 'CNTLDG'
         AND w.status = 'S'
         AND NOT EXISTS (SELECT 'x'
                           FROM work_activity_audit w2
                          WHERE w2.facility_id = i_facility_id
                            AND w2.activity_code = 'CNTLDG'
                            AND w2.operator = user_id_in
                            AND w2.location_id = w.location_id
                            AND w2.status <> 'S'
                            AND w.start_ts = w2.start_ts);


   l_user_not_on_team   BOOLEAN;
   l_team_id            NUMBER(13);
   l_prev_team_id       NUMBER(13);
   l_no_next_team_id    BOOLEAN;
   l_user_has_open      BOOLEAN;
   l_open_xsaction      VARCHAR2(1);
   e_no_data_found      EXCEPTION;
   e_open_xsaction      EXCEPTION;
   TYPE l_bulk_team_id_t IS TABLE OF dms_team.team_id%TYPE INDEX BY BINARY_INTEGER;
   TYPE l_end_date_t IS TABLE OF dms_team.end_date%TYPE INDEX BY BINARY_INTEGER;
   l_bulk_team_id       l_bulk_team_id_t;
   l_end_date           l_end_date_t;
   

   BEGIN
      -- Get the user's current team ID
      OPEN c_get_team_id;
      FETCH c_get_team_id INTO o_team_id;
      l_user_not_on_team := c_get_team_id%NOTFOUND;
      CLOSE c_get_team_id;
      -- Get the user's open session 
      OPEN c_get_open_xsaction(i_user_id);
      FETCH c_get_open_xsaction INTO l_open_xsaction;
      l_user_has_open := c_get_open_xsaction%FOUND;
      CLOSE c_get_open_xsaction;
      -- If user has an open session and the user is not part of a team, raise an error. User should not be able to join a team if he/she had open session
      IF l_user_has_open THEN
         IF i_team_ind = 'N' THEN
            o_open_xsaction := l_open_xsaction;
            raise e_open_xsaction;
         END IF;
      ELSE
         l_open_xsaction := 'N';
         o_open_xsaction := l_open_xsaction;
      END IF;
      -- If user is not part of a team and doesn't have an open session, generate a team ID
      IF l_user_not_on_team AND l_open_xsaction = 'N' THEN
         -- Generate a team ID
         OPEN c_generate_team_id;
         FETCH c_generate_team_id INTO l_team_id;
         CLOSE c_generate_team_id;

         -- IF the team ID has reached 999999999999, we need to go back to the first available team ID
         IF l_team_id > 999999999999 THEN

            l_prev_team_id := 1;
            -- Get all team IDs from dms_team table
            OPEN c_get_bulk_team_id;
            LOOP
               FETCH c_get_bulk_team_id BULK COLLECT INTO l_bulk_team_id, l_end_date LIMIT 100;
               
               FOR i IN l_bulk_team_id.FIRST .. l_bulk_team_id.LAST LOOP
                  -- Check the end_date of the team ID. Exit out of the loop as soon as we got the first available team ID to use
                  IF l_end_date(i) IS NOT NULL OR to_number(l_bulk_team_id(i)) <> l_prev_team_id THEN
                     l_team_id := l_prev_team_id;
                     EXIT;
                  ELSE
                     l_team_id := NULL;
                  END IF;
                  l_prev_team_id := l_prev_team_id + 1;
               END LOOP;
               
               IF l_team_id IS NOT NULL THEN
                  EXIT;
               END IF;
               
               EXIT WHEN c_get_bulk_team_id%NOTFOUND;
            END LOOP;
            -- raise No Data found if there's no available team ID to use
            IF l_team_id IS NULL THEN
               raise e_no_data_found;
            ELSE
               o_team_id := l_team_id;
               
               DELETE FROM dms_team
                WHERE facility_id = i_facility_id
                  AND mstr_bol_id = '0'
                  AND trailer_id = '0'
                  AND carton_unload_qty = '0'
                  AND team_id = l_team_id
                  AND end_date IS NOT NULL;

               INSERT INTO dms_team (facility_id,
                                     mstr_bol_id,
                                     team_id,
                                     trailer_id,
                                     carton_unload_qty,
                                     create_date,
                                     created_by,
                                     modified_by,
                                     end_date)
               VALUES (i_facility_id,
                      0,
                      o_team_id,
                     '0',
                      0,
                      sysdate,
                      i_user_id,
                      NULL,
                      NULL);

               COMMIT;
            END IF;
         ELSE
            o_team_id := l_team_id;

            INSERT INTO dms_team (facility_id,
                                  mstr_bol_id,
                                  team_id,
                                  trailer_id,
                                  carton_unload_qty,
                                  create_date,
                                  created_by,
                                  modified_by,
                                  end_date)
            VALUES (i_facility_id,
                   0,
                   o_team_id,
                  '0',
                   0,
                   sysdate,
                   i_user_id,
                   NULL,
                   NULL);

            COMMIT;
         END IF;
      END IF;


      EXCEPTION
         WHEN e_no_data_found THEN
            o_error_msg_code := 'NO_DATA';
            o_error_msg_typ := 'E';
         WHEN e_open_xsaction THEN
            o_error_msg_code := 'INV_USER_TEAM';
            o_error_msg_typ := 'E';
         WHEN OTHERS THEN
            log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.populate_team', SQLERRM);
      END populate_team;

   PROCEDURE add_del_user (i_facility_id       IN facility.facility_id%TYPE,
                           i_user_id           IN dms_user.user_id%TYPE,
                           i_user_added_del    IN dms_user.user_id%TYPE,
                           i_team_id           IN dms_team_user.team_id%TYPE,
                           i_action_type       IN VARCHAR2,
                           o_error_msg_code    IN OUT user_message.message_code%TYPE,
                           o_error_msg_typ     IN OUT user_message.message_type%TYPE) is
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Procedure created to start or end a user from a team.
* i_facility_id           - Pass facility_id of the session
* i_user_id               - Pass the user ID of the owner of the session.
* i_user_added_del        - Pass the user ID that is being added/deleted from the team
* i_team_id               - Pass the team ID of the i_user_id
* i_action_type           - Pass ADD or DELETE
* o_error_msg_code        - The error code will be returned back to the calling program
* o_error_msg_typ         - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/
   CURSOR c_get_team_id IS
   SELECT team_id
   FROM dms_team_user dtu
   WHERE dtu.facility_id = i_facility_id
   AND dtu.user_id = i_user_added_del
   AND dtu.mstr_bol_id = '0'
   AND dtu.create_date IS NOT NULL
   AND dtu.end_date IS NULL;

   CURSOR c_get_closed_team IS
   SELECT team_id
   FROM dms_team_user dtu
   WHERE dtu.facility_id = i_facility_id
   AND dtu.user_id = i_user_added_del
   AND dtu.mstr_bol_id = '0'
   AND dtu.team_id = i_team_id
   AND dtu.create_date IS NOT NULL
   AND dtu.end_date IS NOT NULL;

   CURSOR c_get_open_xsaction IS
   SELECT 'x'
     FROM work_activity_audit w
    WHERE w.facility_id = i_facility_id
      AND w.operator = i_user_added_del
      AND w.activity_code = 'CNTLDG'
      AND w.status = 'S'
      AND NOT EXISTS (SELECT 'x'
                        FROM work_activity_audit w2
                       WHERE w2.facility_id = i_facility_id
                         AND w2.activity_code = 'CNTLDG'
                         AND w2.operator = i_user_added_del
                         AND w2.location_id = w.location_id
                         AND w2.status <> 'S'
                         AND w.start_ts = w2.start_ts);

   CURSOR c_check_user_id IS
   SELECT 'x'
     FROM dms_user du
    WHERE du.facility_id = i_facility_id
      AND du.user_id = i_user_added_del;
      
   CURSOR c_get_team_user_count IS
   SELECT count(1)
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.mstr_bol_id = '0'
      AND dtu.team_id = i_team_id
      AND dtu.create_date IS NOT NULL
      AND dtu.end_date IS NULL;


   l_inv_user              VARCHAR2(1);
   l_new_user_team         dms_team_user.team_id%TYPE;
   l_user_prev_team        dms_team_user.team_id%TYPE;
   l_open_xsaction_found   BOOLEAN;
   l_user_is_in_team       BOOLEAN;
   l_user_was_on_team      BOOLEAN;
   e_partial_entry         EXCEPTION;
   e_inv_user_team         EXCEPTION;
   e_user_team_exist       EXCEPTION;
   l_user_id               VARCHAR2(1);
   l_user_notfound         BOOLEAN;
   e_inv_user_id           EXCEPTION;
   l_no_more_user          BOOLEAN;
   l_remaining_team_user   NUMBER;

   BEGIN
      IF i_user_added_del IS NULL THEN
         raise e_partial_entry;
      ELSE
         OPEN c_check_user_id;
         FETCH c_check_user_id INTO l_user_id;
         l_user_notfound := c_check_user_id%NOTFOUND;
         CLOSE c_check_user_id;

         IF l_user_notfound THEN
            raise e_inv_user_id;
         END if;

         OPEN c_get_open_xsaction;
         FETCH c_get_open_xsaction INTO l_inv_user;
         l_open_xsaction_found := c_get_open_xsaction%FOUND;
         CLOSE c_get_open_xsaction;

         IF l_open_xsaction_found THEN
            raise e_inv_user_team;
         ELSE
            IF i_action_type = 'ADD' THEN
               OPEN c_get_team_id;
               FETCH c_get_team_id INTO l_new_user_team;
               l_user_is_in_team := c_get_team_id%FOUND;
               CLOSE c_get_team_id;

               IF l_user_is_in_team THEN
                  --IF l_new_user_team <> i_team_id THEN
                     raise e_user_team_exist;
                 --END IF;
               ELSE
                  OPEN c_get_closed_team;
                  FETCH c_get_closed_team INTO l_user_prev_team;
                  l_user_was_on_team := c_get_closed_team%FOUND;
                  CLOSE c_get_closed_team;

                  IF l_user_was_on_team THEN
                     UPDATE dms_team_user
                        SET modified_by = i_user_id,
                            end_date = NULL,
                            create_date = sysdate
                      WHERE facility_id = i_facility_id
                        AND team_id = i_team_id
                        AND user_id = i_user_added_del
                        AND mstr_bol_id = '0'
                        AND create_date IS NOT NULL
                        AND end_date IS NOT NULL;
                     COMMIT;
                  ELSE
                     INSERT INTO dms_team_user (facility_id,
                                                mstr_bol_id,
                                                team_id,
                                                user_id,
                                                create_date,
                                                added_by,
                                                modified_by,
                                                end_date)
                     VALUES (i_facility_id,
                            '0',
                             i_team_id,
                             i_user_added_del,
                             sysdate,
                             i_user_id,
                             NULL,
                             NULL);
                     COMMIT;
                  END IF;
               END IF;
            ELSE
               UPDATE dms_team_user
                  SET modified_by = i_user_id,
                      end_date = sysdate
                WHERE facility_id = i_facility_id
                  AND team_id = i_team_id
                  AND user_id = i_user_added_del
                  AND mstr_bol_id = '0'
                  AND create_date IS NOT NULL
                  AND end_date IS NULL;
               COMMIT;
               
               OPEN c_get_team_user_count;
               FETCH c_get_team_user_count INTO l_remaining_team_user;
               l_no_more_user := c_get_team_user_count%NOTFOUND;
               CLOSE c_get_team_user_count;
               
               IF l_no_more_user OR l_remaining_team_user = 0 THEN
                  UPDATE dms_team
                  SET modified_by = i_user_id,
                      end_date = sysdate
                WHERE facility_id = i_facility_id
                  AND team_id = i_team_id
                  AND mstr_bol_id = '0'
                  AND trailer_id = '0'
                  AND carton_unload_qty = '0';
                  
                  COMMIT;
               END IF;
            END IF;
         END IF;
      END IF;

      EXCEPTION
         WHEN e_partial_entry THEN
            o_error_msg_code := 'PARTIAL_ENTRY';
            o_error_msg_typ := 'E';
         WHEN e_inv_user_team THEN
            o_error_msg_code := 'INV_USER_TEAM';
            o_error_msg_typ := 'E';
         WHEN e_user_team_exist THEN
            o_error_msg_code := 'USER_TEAM_EXIST';
            o_error_msg_typ := 'E';
         WHEN e_inv_user_id THEN
            o_error_msg_code := 'INV_USER_ID';
            o_error_msg_typ := 'E';
         WHEN OTHERS THEN
            log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.add_del_user', SQLERRM);
      END add_del_user;

--   PROCEDURE user_to_scan (i_facility_id            IN facility.facility_id%TYPE,
--                           i_door_id                IN door.door_id%TYPE,
--                           i_user_id                IN dms_user.user_id%TYPE) IS
--/************************************************************************************************************
--* Change History:
--*
--* Name            CO/CR#          Date                Description
--* -----------------------------------------------------------------------------------------------------------
--* L.Galliguez    CHG0036741      10/10/2016        Procedure created to get all the users at the door. These
--*                                                  users will be inserted into the global temp table and will
--*                                                  be used by the system to validate if all the users have been
--*                                                  scanned or not.
--************************************************************************************************************/
--   BEGIN
--      DELETE FROM ross_data_lookup_gtt;
--
--      INSERT INTO ross_data_lookup_gtt (sessionid,
--                                        datadef,
--                                        datavalue)
--         SELECT i_user_id, operator, 'N'
--           FROM work_activity_audit w
--          WHERE w.facility_id = i_facility_id
--            AND w.activity_code = 'CNTLDG'
--            AND w.location_id = i_door_id
--            AND w.status = 'S'
--            AND NOT EXISTS (SELECT 'x'
--                              FROM work_activity_audit w2
--                             WHERE w2.facility_id = i_facility_id
--                               AND w2.activity_code = 'CNTLDG'
--                               AND w2.location_id = i_door_id
--                               AND w2.status <> 'S'
--                               AND w.start_ts = w2.start_ts);
--
--      COMMIT;
--      EXCEPTION
--         WHEN OTHERS THEN
--         log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.user_to_scan', SQLERRM);
--   END user_to_scan;
--
--   PROCEDURE update_scan_user (i_facility_id        IN facility.facility_id%TYPE,
--                               i_user_id            IN dms_user.user_id%TYPE,
--                               i_scanned_user_id    IN dms_user.user_id%TYPE,
--                               o_error_msg_code     IN OUT user_message.message_code%TYPE,
--                               o_error_msg_typ      IN OUT user_message.message_type%TYPE) IS
--/************************************************************************************************************
--* Change History:
--*
--* Name            CO/CR#          Date                Description
--* -----------------------------------------------------------------------------------------------------------
--* L.Galliguez    CHG0036741      10/10/2016        Procedure created to update the user at the door that was
--*                                                  scanned by the user joining in.
--************************************************************************************************************/
--   CURSOR c_is_user_valid IS
--   SELECT datavalue
--     FROM ross_data_lookup_gtt
--    WHERE sessionid = i_user_id
--      AND datadef = i_scanned_user_id;
--
--   l_user_valid       ross_data_lookup_gtt.datavalue%TYPE;
--   l_user_not_found   BOOLEAN;
--   e_inv_user         EXCEPTION;
--
--   BEGIN
--      OPEN c_is_user_valid;
--      FETCH c_is_user_valid INTO l_user_valid;
--      l_user_not_found := c_is_user_valid%NOTFOUND;
--      CLOSE c_is_user_valid;
--
--      IF l_user_not_found THEN
--         raise e_inv_user;
--      ELSIF l_user_valid = 'N' THEN
--         UPDATE ross_data_lookup_gtt
--            SET datavalue = 'Y'
--          WHERE sessionid = i_user_id
--            AND datadef = i_scanned_user_id
--            AND datavalue = 'N';
--
--         COMMIT;
--      END IF;
--
--      EXCEPTION
--      WHEN e_inv_user THEN
--         o_error_msg_code := 'INV_USER_ID';
--         o_error_msg_typ := 'E';
--      WHEN OTHERS THEN
--         log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.update_scan_user', SQLERRM);
--      END update_scan_user;

   FUNCTION scan_complete (i_facility_id             IN facility.facility_id%TYPE,
                           i_user_id                 IN dms_user.user_id%TYPE,
                           i_scanned_user            IN VARCHAR2,
                           i_unscanned_user          IN VARCHAR2,
                           o_error_msg_code          IN OUT user_message.message_code%TYPE,
                           o_error_msg_typ           IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 IS
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Function created to validate if all the expected users have
*                                                  been scanned. Return the appropriate confirmation or error
*                                                  message depending on the user that was not scanned.
* PARAMETERS:
* i_facility_id           - Pass facility_id of the session
* i_user_id               - Pass the user ID of the owner of the session.
* i_scanned_user          - Pass the scanned users delimited by a comma
* i_unscanned_user        - Pass the unscanned users delimited by a comma
* o_error_msg_code        - The error code will be returned back to the calling program
* o_error_msg_typ         - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/


   CURSOR c_get_team_id (user_id_in IN dms_team_user.user_id%TYPE) IS
   SELECT team_id
     FROM dms_team_user
    WHERE facility_id = i_facility_id
      AND user_id = user_id_in
      AND mstr_bol_id = '0'
      AND create_date IS NOT NULL
      AND end_date IS NULL;


   e_end_user_session           EXCEPTION;
   e_cant_end_tm_session        EXCEPTION;
   l_team_id                    dms_team_user.team_id%TYPE;
   l_comma_count                NUMBER;
   l_statement                  VARCHAR2(2000);
   l_unscanned_ind              VARCHAR2(1);
   TYPE unscanned_tm_users_t IS TABLE OF dms_team_user.user_id%TYPE INDEX BY BINARY_INTEGER;
   l_unscanned_tm               unscanned_tm_users_t;
   --TYPE l_tm_members_not_scanned_t IS TABLE OF VARCHAR2(1) INDEX BY BINARY_INTEGER;
   l_tm_members_not_scanned     VARCHAR2(1);
   l_end_user_session           VARCHAR2(1) := 'X';
   l_unscanned_user             VARCHAR2(2000);
   l_scanned_user               VARCHAR2(2000);


   BEGIN

      IF i_unscanned_user IS NOT NULL THEN 
         l_unscanned_user := REPADMIN.fn_add_quotes (i_unscanned_user, 1); -- 999878
      END IF;
      
      IF i_scanned_user IS NOT NULL THEN  -- '10991058,10991408,10990865,944944'
         l_scanned_user := REPADMIN.fn_add_quotes (i_scanned_user, 1);
      END IF;

      -- Check for any unscanned individual users
      IF i_unscanned_user IS NOT NULL THEN 
         BEGIN
            l_statement := 'SELECT ''x'' ' ||
                             'FROM dms_user du ' ||
                              'WHERE du.facility_id = ''' || i_facility_id || ''' ' ||
                                'AND du.user_id in ('|| l_unscanned_user||') ' ||
                               'AND NOT EXISTS (SELECT ''x'' ' ||
                                                'FROM DMS_TEAM_USER dtu ' ||
                                               'WHERE mstr_bol_id = ''0'' ' ||
                                                 'AND facility_id = ''' || i_facility_id || ''' ' ||
                                                 'AND create_date IS NOT NULL ' ||
                                                 'AND end_date IS NULL ' ||
                                                 'AND user_id IN ('|| l_unscanned_user||') ' ||
                                                 'AND du.user_id = dtu.user_id) ' ||
                                                 'AND EXISTS (SELECT ''x'' ' ||
                                                               'FROM work_activity_audit w ' ||
                                                               'WHERE w.facility_id = ''' || i_facility_id || ''' ' ||
                                                               'AND w.operator IN ('||l_unscanned_user||') ' ||
                                                               'AND w.activity_code = ''CNTLDG'' ' ||
                                                               'AND w.status = ''S'' ' ||
                                                               'AND NOT EXISTS (SELECT ''x'' ' ||
                                                                                 'FROM work_activity_audit w2 ' ||
                                                                                 'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                                                 'AND w2.activity_code = ''CNTLDG'' ' ||
                                                                                 'AND w2.operator IN ('||l_unscanned_user||') ' ||
                                                                                 'AND w2.location_id = w.location_id ' ||
                                                                                 'AND w2.status <> ''S'' ' ||
                                                                                 'AND w.start_ts = w2.start_ts)) ' ||
                               'AND rownum < 2';
            
            EXECUTE IMMEDIATE l_statement INTO l_unscanned_ind;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_unscanned_ind := NULL;
         END;
      END IF;

      IF i_unscanned_user IS NOT NULL THEN
         -- Check for any unscanned team users
         BEGIN
            l_statement := 'SELECT dtu.user_id ' ||
                             'FROM DMS_TEAM_USER dtu ' ||
                            'WHERE mstr_bol_id = ''0'' ' ||
                              'AND facility_id = ''' || i_facility_id || ''' ' ||
                              'AND create_date IS NOT NULL ' ||
                              'AND end_date IS NULL ' ||
                              'AND user_id IN ('||l_unscanned_user||') ' ||
                              'AND EXISTS (SELECT ''x'' ' ||
                                            'FROM work_activity_audit w ' ||
                                            'WHERE w.facility_id = ''' || i_facility_id || ''' ' ||
                                            'AND w.operator IN ('||l_unscanned_user||') ' ||
                                            'AND w.activity_code = ''CNTLDG'' ' ||
                                            'AND w.status = ''S'' ' ||
                                            'AND NOT EXISTS (SELECT ''x'' ' ||
                                                              'FROM work_activity_audit w2 ' ||
                                                              'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                              'AND w2.activity_code = ''CNTLDG'' ' ||
                                                              'AND w2.operator IN ('||l_unscanned_user||') ' ||
                                                              'AND w2.location_id = w.location_id ' ||
                                                              'AND w2.status <> ''S'' ' ||
                                                              'AND w.start_ts = w2.start_ts)) ' ||
                              'AND rownum < 2';
            
            EXECUTE IMMEDIATE l_statement BULK COLLECT INTO l_unscanned_tm;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
         END;
      END IF;

      -- Ask the user if unscanned individual users' sessions will be ended
      IF l_unscanned_ind IS NOT NULL THEN
         raise e_end_user_session;
      -- If the unscanned users belong to a team, check if all team members of the team were scanned or not
      ELSIF l_unscanned_tm.COUNT > 0 THEN
         FOR i IN l_unscanned_tm.FIRST .. l_unscanned_tm.LAST LOOP
            OPEN c_get_team_id (l_unscanned_tm(i));
            FETCH c_get_team_id INTO l_team_id;
            CLOSE c_get_team_id;

            IF i_scanned_user IS NOT NULL THEN 
               BEGIN
                  l_statement := 'SELECT ''x'' ' ||
                                   'FROM dms_team_user d ' ||
                                  'WHERE facility_id = ''' || i_facility_id || ''' ' ||
                                    'AND team_id = ''' || l_team_id || ''' ' ||
                                    'AND mstr_bol_id = ''0'' ' ||
                                    'AND create_date IS NOT NULL ' ||
                                    'AND end_date IS NULL ' ||
                                    'AND user_id IN (' || l_scanned_user||') ' ||
                                    'AND EXISTS (SELECT ''x'' ' ||
                                                  'FROM work_activity_audit w ' ||
                                                  'WHERE w.facility_id = ''' || i_facility_id || ''' ' ||
                                                  'AND w.operator IN ('||l_scanned_user||') ' ||
                                                  'AND w.activity_code = ''CNTLDG'' ' ||
                                                  'AND w.status = ''S'' ' ||
                                                  'AND NOT EXISTS (SELECT ''x'' ' ||
                                                                    'FROM work_activity_audit w2 ' ||
                                                                    'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                                    'AND w2.activity_code = ''CNTLDG'' ' ||
                                                                    'AND w2.operator IN ('||l_scanned_user||') ' ||
                                                                    'AND w2.location_id = w.location_id ' ||
                                                                    'AND w2.status <> ''S'' ' ||
                                                                    'AND w.start_ts = w2.start_ts)) ' ||
                              'AND rownum < 2';
                  
                  EXECUTE IMMEDIATE l_statement INTO l_tm_members_not_scanned;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  l_tm_members_not_scanned := NULL;
               END;

               --If all team members were not scanned then ask the user if unscanned team members' sessions will be ended
               IF l_tm_members_not_scanned IS NULL THEN
                  l_end_user_session := 'Y';
                  EXIT;
               ELSE
                  --If at least 1 team member was scanned but not the other, raise an error that we can't end the team's session
                  l_end_user_session := 'N';
               END IF;
            ELSE
               l_end_user_session := 'Y';
               EXIT;
            END IF;
         END LOOP;

         IF l_end_user_session = 'Y' THEN
            raise e_end_user_session;
         ELSE
            raise e_cant_end_tm_session;
         END IF;
      END IF;

      RETURN ('Y');

      EXCEPTION
      WHEN e_end_user_session THEN
         o_error_msg_code := 'END_UNSCANNED';
         o_error_msg_typ := 'C';
         RETURN ('N');
      WHEN e_cant_end_tm_session THEN
         o_error_msg_code := 'INC_SCANNED';
         o_error_msg_typ := 'E';
         RETURN ('N');
      WHEN OTHERS THEN
         log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.scan_complete', SQLERRM);
         RETURN ('N');
      END scan_complete;

   PROCEDURE cancel_session (i_facility_id           IN facility.facility_id%TYPE,
                             i_user_id               IN dms_user.user_id%TYPE,
                             i_door_id               IN door.door_id%TYPE,
                             i_team_ind              IN VARCHAR2) IS
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Procedure created to update the 'S' record of the user to 'X'
*                                                  when F9=Cancel is pressed. 'X' status will indicate that the
*                                                  user has cancelled.
* PARAMETERS:
* i_facility_id           - Pass facility_id of the session
* i_user_id               - Pass the user ID of the owner of the session.
* i_door_id               - Pass the user's door ID 
************************************************************************************************************/

      CURSOR c_get_team_users (l_team_id_in IN dms_team_user.team_id%TYPE)IS
      SELECT user_id
        FROM dms_team_user dtu
       WHERE dtu.facility_id = i_facility_id
         AND dtu.team_id = l_team_id_in
         AND dtu.mstr_bol_id = '0'
         AND dtu.create_date IS NOT NULL
         AND dtu.end_date IS NULL;
         
      CURSOR c_get_team_id IS
      SELECT team_id
        FROM dms_team_user
       WHERE facility_id = i_facility_id
         AND user_id = i_user_id
         AND mstr_bol_id = '0'
         AND create_date IS NOT NULL
         AND end_date IS NULL;
         
      CURSOR c_get_all_open_users IS
         SELECT w.facility_id, w.container_id, w.activity_code, w.start_ts, w.action_ts, w.status, w.operator
           FROM work_activity_audit w
          WHERE w.facility_id = i_facility_id
            AND w.location_id = i_door_id
            AND w.activity_code = 'CNTLDG'
            AND w.status = 'S'
            AND NOT EXISTS (SELECT 'x'
                              FROM work_activity_audit w2
                             WHERE w2.facility_id = i_facility_id
                               AND w2.activity_code = 'CNTLDG'
                               AND w2.location_id = i_door_id
                               AND w2.status <> 'S'
                               AND w.start_ts = w2.start_ts
                               AND w.operator = w2.operator);
         
      l_team_id      dms_team_user.team_id%TYPE;
      TYPE l_team_users_t IS TABLE OF dms_team_user.user_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_facility_id_t IS TABLE OF work_activity_audit.facility_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_container_id_t IS TABLE OF work_activity_audit.container_id%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_activity_code_t IS TABLE OF work_activity_audit.activity_code%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_start_ts_t IS TABLE OF work_activity_audit.start_ts%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_action_ts_t IS TABLE OF work_activity_audit.action_ts%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_status_t IS TABLE OF work_activity_audit.status%TYPE INDEX BY BINARY_INTEGER;
      TYPE l_operator_t IS TABLE OF work_activity_audit.operator%TYPE INDEX BY BINARY_INTEGER;
      l_team_users              l_team_users_t;
      l_facility_id             l_facility_id_t;
      l_container_id            l_container_id_t;
      l_activity_code           l_activity_code_t;
      l_start_ts                l_start_ts_t;
      l_action_ts               l_action_ts_t;
      l_status                  l_status_t;
      l_operator                l_operator_t;
      l_count_remaining_users   NUMBER(3);
      
      BEGIN
         IF i_team_ind = 'Y' THEN
            OPEN c_get_team_id;
            FETCH c_get_team_id INTO l_team_id;
            CLOSE c_get_team_id;
            
            OPEN c_get_team_users(l_team_id);
            FETCH c_get_team_users BULK COLLECT INTO l_team_users;
            CLOSE c_get_team_users;
         END IF;
         
         IF l_team_users.COUNT > 0 THEN
            FORALL i IN l_team_users.FIRST.. l_team_users.LAST
               UPDATE work_activity_audit w
                  SET w.status = 'X',
                      w.action_ts = sysdate
                WHERE w.facility_id = i_facility_id
                  AND w.operator = l_team_users(i)
                  AND w.location_id = i_door_id
                  AND w.activity_code = 'CNTLDG'
                  AND w.status = 'S'
                  AND NOT EXISTS (SELECT 'x'
                                    FROM work_activity_audit w2
                                   WHERE w2.facility_id = i_facility_id
                                     AND w2.activity_code = 'CNTLDG'
                                     AND w2.operator = l_team_users(i)
                                     AND w2.location_id = i_door_id
                                     AND w2.status <> 'S'
                                     AND w.start_ts = w2.start_ts);
         ELSE
            UPDATE work_activity_audit w
                  SET w.status = 'X',
                      w.action_ts = sysdate
                WHERE w.facility_id = i_facility_id
                  AND w.operator = i_user_id
                  AND w.location_id = i_door_id
                  AND w.activity_code = 'CNTLDG'
                  AND w.status = 'S'
                  AND NOT EXISTS (SELECT 'x'
                                    FROM work_activity_audit w2
                                   WHERE w2.facility_id = i_facility_id
                                     AND w2.activity_code = 'CNTLDG'
                                     AND w2.operator = i_user_id
                                     AND w2.location_id = i_door_id
                                     AND w2.status <> 'S'
                                     AND w.start_ts = w2.start_ts);
         END IF;
         
         log_audit_trail (i_facility_id,
                          'CONVEYABLE_CARTON_LOAD',
                          i_user_id, 
                          i_door_id,
                          NULL,
                          'CONV',
                          'CANCEL',
                          'TEAM_ID',
                          NVL(l_team_id,'N/A'));
                          
         -- Get count of all remaining users at door
         OPEN c_get_all_open_users;
         FETCH c_get_all_open_users BULK COLLECT INTO l_facility_id, l_container_id, l_activity_code, l_start_ts, l_action_ts, l_status, l_operator;
         CLOSE c_get_all_open_users;
         
         IF l_facility_id.COUNT > 0 THEN
            l_count_remaining_users := l_facility_id.COUNT;
            
            FORALL i IN l_facility_id.FIRST .. l_facility_id.LAST
               UPDATE work_activity_audit w
                  SET w.user_sharing = l_count_remaining_users
                WHERE w.facility_id = l_facility_id(i)
                  AND w.container_id  = l_container_id(i)
                  AND w.activity_code = l_activity_code(i)
                  AND w.start_ts = l_start_ts(i)
                  AND w.action_ts = l_action_ts(i)
                  AND w.status = l_status(i)
                  AND w.operator = l_operator(i);
         END IF;
         
         
         COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
         log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.cancel_session', SQLERRM);
      END cancel_session;
      
   FUNCTION user_open_session (i_facility_id         IN facility.facility_id%TYPE,
                               i_user_id             IN VARCHAR2,
                               o_error_msg_code      IN OUT user_message.message_code%TYPE,
                               o_error_msg_typ       IN OUT user_message.message_type%TYPE) RETURN VARCHAR2 IS
   /************************************************************************************************************
   * Change History:
   *
   * Name            CO/CR#          Date                Description
   * -----------------------------------------------------------------------------------------------------------
   * L.Galliguez    CHG0036741      10/10/2016        Procedure created to check if user has open transaction.
   * PARAMETERS:
   * i_facility_id           - Pass facility_id of the session
   * i_user_id               - Pass the user ID of the owner of the session.
   * o_open_xsaction         - Will return Y if user had open transaction or N if user did not have open transaction.
   ************************************************************************************************************/
   
      CURSOR c_get_open_xsaction IS
      SELECT 'x'
        FROM work_activity_audit w
       WHERE w.facility_id = i_facility_id
         AND w.operator = i_user_id
         AND w.activity_code = 'CNTLDG'
         AND w.status = 'S'
         AND NOT EXISTS (SELECT 'x'
                           FROM work_activity_audit w2
                          WHERE w2.facility_id = i_facility_id
                            AND w2.activity_code = 'CNTLDG'
                            AND w2.operator = i_user_id
                            AND w2.location_id = w.location_id
                            AND w2.status <> 'S'
                            AND w.start_ts = w2.start_ts);
                            
      l_return_val              VARCHAR2(1);
      l_transaction_found       BOOLEAN;
      e_has_open_xsaction       EXCEPTION;
      TYPE l_user_open_t IS TABLE OF VARCHAR2(1) INDEX BY BINARY_INTEGER;
      l_user_open               l_user_open_t;
      l_statement               VARCHAR2(2000);

      BEGIN
         BEGIN
            l_statement := 'SELECT ''x'' ' ||
                             'FROM work_activity_audit w ' ||
                            'WHERE w.facility_id = ''' || i_facility_id ||''' ' ||
                              'AND w.operator IN ( ' || i_user_id || ') ' ||
                              'AND w.activity_code = ''CNTLDG'' ' ||
                              'AND w.status = ''S'' ' ||
                              'AND NOT EXISTS (SELECT ''x'' ' ||
                                                'FROM work_activity_audit w2 ' ||
                                               'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                 'AND w2.activity_code = ''CNTLDG'' ' ||
                                                 'AND w2.operator IN ( ' || i_user_id || ') ' ||
                                                 'AND w2.status <> ''S'' ' ||
                                                 'AND w.start_ts = w2.start_ts)';

            EXECUTE IMMEDIATE l_statement BULK COLLECT INTO l_user_open;
         
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_return_val := 'Y';
         END;
         
         IF l_user_open.COUNT > 0 THEN
            raise e_has_open_xsaction;
         ELSE
            l_return_val := 'Y';
         END IF;
         
         RETURN l_return_val;
         
      EXCEPTION
      WHEN e_has_open_xsaction THEN
         o_error_msg_code := 'INV_USER_TEAM';
         o_error_msg_typ := 'E';
         RETURN ('N');
      WHEN OTHERS THEN
         log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.user_open_session', SQLERRM);
         RETURN ('N');
      END user_open_session;
      
   FUNCTION supervisor_done (i_facility_id               IN facility.facility_id%TYPE,
                             i_user_id                   IN dms_user.user_id%TYPE,
                             i_scanned_user              IN VARCHAR2,
                             i_unscanned_user            IN VARCHAR2,
                             o_screen_code               IN OUT VARCHAR2,
                             o_reopen_user               IN OUT VARCHAR2,
                             o_end_user                  IN OUT VARCHAR2,
                             o_error_msg_code            IN OUT user_message.message_code%TYPE,
                             o_error_msg_typ             IN OUT user_message.message_type%TYPE,
                             i_door_id                   IN door.door_id%TYPE,
                             i_first_container_id        IN container.container_id%TYPE,
                             i_trailer_id                IN trailer.trailer_id%TYPE,
                             i_route                     IN route.route%TYPE,
                             i_team_ind                  IN VARCHAR2) RETURN VARCHAR2 IS
/************************************************************************************************************
* Change History:
*
* Name            CO/CR#          Date                Description
* -----------------------------------------------------------------------------------------------------------
* L.Galliguez    CHG0036741      10/10/2016        Function created to validate if all the expected users have
*                                                  been scanned. Return the appropriate confirmation or error
*                                                  message depending on the user that was not scanned.
* PARAMETERS:
* i_facility_id           - Pass facility_id of the session
* i_user_id               - Pass the user ID of the owner of the session.
* i_scanned_user          - Pass the scanned users delimited by a comma
* i_unscanned_user        - Pass the unscanned users delimited by a comma
* o_screen_code           - Return the screen where the system should go back
* o_reopen_user           - Return the users to reopen
* o_end_user              - Return the users to end
* o_error_msg_code        - The error code will be returned back to the calling program
* o_error_msg_typ         - The error type will be returned back to the calling program (C - confirmation, E - Error)
************************************************************************************************************/


   e_end_user_session           EXCEPTION;
   e_cant_end_tm_session        EXCEPTION;
   l_comma_count                NUMBER;
   l_statement                  VARCHAR2(2000);
   l_unscanned_ind              VARCHAR2(1);
   TYPE unscanned_tm_users_t IS TABLE OF dms_team_user.user_id%TYPE INDEX BY BINARY_INTEGER;
   TYPE l_unscanned_user_id_t IS TABLE OF dms_team_user.user_id%TYPE INDEX BY BINARY_INTEGER;
   TYPE l_unscanned_team_id_t IS TABLE OF dms_team_user.team_id%TYPE INDEX BY BINARY_INTEGER;
   TYPE l_unscanned_user_type_t IS TABLE OF VARCHAR2(5) INDEX BY BINARY_INTEGER;
   l_unscanned_tm               unscanned_tm_users_t;
   l_unscanned_user_id          l_unscanned_user_id_t;
   l_unscanned_team_id          l_unscanned_team_id_t;
   l_unscanned_user_type        l_unscanned_user_type_t;
   l_scanned_user_id            l_unscanned_user_id_t;
   l_scanned_team_id            l_unscanned_team_id_t;
   l_scanned_user_type          l_unscanned_user_type_t;
   --TYPE l_tm_members_not_scanned_t IS TABLE OF VARCHAR2(1) INDEX BY BINARY_INTEGER;
   l_tm_members_not_scanned     VARCHAR2(1);
   l_end_user_session           VARCHAR2(1) := 'X';
   l_unscanned_user             VARCHAR2(2000);
   l_scanned_user               VARCHAR2(2000);
   l_broken_team                l_unscanned_team_id_t;
   l_prev_unscanned_team_id     dms_team_user.team_id%TYPE;
   k                            NUMBER(3);
   l_error_msg_code             user_message.message_code%TYPE;
   l_error_msg_typ              user_message.message_type%TYPE;
   l_count_remaining_users      NUMBER(3);
   l_remaining_users            VARCHAR2(2000);
   l_next_container_id          container.container_id%TYPE;
   l_assign_last                VARCHAR2(1);
   l_assign_first               VARCHAR2(1);
   l_num_users                  NUMBER(3) := 0;
   l_team_id                    dms_team_user.team_id%TYPE;
   l_count_users                NUMBER(3);
   
   CURSOR c_get_team_id IS
   SELECT team_id
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.mstr_bol_id = '0'
      AND dtu.user_id = i_user_id
      AND dtu.end_date IS NULL;
   
   CURSOR c_get_count_users (l_team_id_in IN dms_team_user.team_id%TYPE) IS
   SELECT count(user_id)
     FROM dms_team_user dtu
    WHERE dtu.facility_id = i_facility_id
      AND dtu.team_id = l_team_id_in
      AND dtu.mstr_bol_id = '0'
      AND dtu.create_date IS NOT NULL
      AND dtu.end_date IS NULL;

   BEGIN

      IF i_unscanned_user IS NOT NULL THEN 
         l_unscanned_user := REPADMIN.fn_add_quotes (i_unscanned_user, 1); 
      END IF;
      
      IF i_scanned_user IS NOT NULL THEN
         l_scanned_user := REPADMIN.fn_add_quotes (i_scanned_user, 1);
      END IF;

      -- Get the type of each unscanned user, whether TEAM or IND
      IF i_unscanned_user IS NOT NULL THEN 
         BEGIN
            l_statement := 'SELECT du.user_id, dtu.team_id, ' ||
                                  'DECODE (team_id, NULL, ''IND'', ''TEAM'') user_type ' ||
                             'FROM dms_user du, dms_team_user dtu ' ||
                            'WHERE du.user_id IN ('|| l_unscanned_user||') ' ||
                              'AND du.user_id = dtu.user_id(+) ' ||
                              'AND du.facility_id = dtu.facility_id(+) ' ||
                              'AND mstr_bol_id (+) = ''0'' ' ||
                              'AND create_date(+) IS NOT NULL ' ||
                              'AND end_date(+) IS NULL ' ||
                              'AND du.facility_id = ''' || i_facility_id || ''' ' ||
                              'AND EXISTS (SELECT ''x'' ' ||
                                            'FROM work_activity_audit w ' ||
                                            'WHERE w.facility_id = ''' || i_facility_id || ''' ' ||
                                            'AND w.operator IN ('||l_unscanned_user||') ' ||
                                            'AND w.activity_code = ''CNTLDG'' ' ||
                                            'AND w.status = ''S'' ' ||
                                            'AND NOT EXISTS (SELECT ''x'' ' ||
                                                              'FROM work_activity_audit w2 ' ||
                                                              'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                                              'AND w2.activity_code = ''CNTLDG'' ' ||
                                                              'AND w2.operator IN ('||l_unscanned_user||') ' ||
                                                              'AND w2.location_id = w.location_id ' ||
                                                              'AND w2.status <> ''S'' ' ||
                                                              'AND w.start_ts = w2.start_ts)) ';
            
            EXECUTE IMMEDIATE l_statement BULK COLLECT INTO l_unscanned_user_id, l_unscanned_team_id, l_unscanned_user_type;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
         END;
      END IF;
      
      -- Get the type of each scanned user, whether TEAM or IND
      IF i_scanned_user IS NOT NULL THEN 
         BEGIN
            l_statement := 'SELECT w.OPERATOR, x.team_id, user_type ' ||
                             'FROM work_activity_audit w, ' ||
                                  '(SELECT du.facility_id facility_id, du.user_id, dtu.team_id team_id, ' ||
                                          'DECODE (team_id, NULL, ''IND'', ''TEAM'') user_type ' ||
                                     'FROM dms_user du, dms_team_user dtu ' ||
                                    'WHERE du.user_id IN ('|| l_scanned_user||') ' ||
                                      'AND du.user_id = dtu.user_id(+)' ||
                                      'AND du.facility_id = dtu.facility_id(+) ' ||
                                      'AND mstr_bol_id(+) = ''0'' ' ||
                                      'AND create_date(+) IS NOT NULL ' ||
                                      'AND end_date(+) IS NULL ' ||
                                      'AND du.facility_id = ''' || i_facility_id || ''') x ' ||
                            'WHERE w.facility_id = ''' || i_facility_id || ''' ' ||
                              'AND w.OPERATOR IN ('|| l_scanned_user||') ' ||
                              'AND w.activity_code = ''CNTLDG'' ' ||
                              'AND w.status = ''S'' ' ||
                              'AND NOT EXISTS ( ' ||
                                     'SELECT ''x'' ' ||
                                       'FROM work_activity_audit w2 ' ||
                                      'WHERE w2.facility_id = ''' || i_facility_id || ''' ' ||
                                        'AND w2.activity_code = ''CNTLDG'' ' ||
                                        'AND w2.OPERATOR IN ('|| l_scanned_user||') ' ||
                                        'AND w2.location_id = w.location_id ' ||
                                        'AND w2.status <> ''S'' ' ||
                                        'AND w.start_ts = w2.start_ts) ' ||
                              'AND w.OPERATOR = x.user_id ' ||
                              'AND w.facility_id = x.facility_id ';
            
            EXECUTE IMMEDIATE l_statement BULK COLLECT INTO l_scanned_user_id, l_scanned_team_id, l_scanned_user_type;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
         END;
      END IF;
      
      -- Loop through unscanned then compare if there's similar team id in scanned. Save all broken teams.
      l_prev_unscanned_team_id := NULL;
      k := 1;
      IF l_unscanned_user_id.COUNT > 0 THEN
         FOR i IN l_unscanned_user_id.FIRST .. l_unscanned_user_id.LAST LOOP
            IF l_unscanned_team_id(i) IS NOT NULL AND l_unscanned_user_type(i) = 'TEAM' AND (l_prev_unscanned_team_id IS NULL OR l_unscanned_team_id(i) <> l_prev_unscanned_team_id) THEN
               IF l_scanned_user_id.COUNT > 0 THEN
                  FOR j IN l_scanned_user_id.FIRST .. l_scanned_user_id.LAST LOOP
                     IF l_scanned_team_id(j) IS NOT NULL AND l_scanned_user_type(j) = 'TEAM' THEN
                        IF l_unscanned_team_id(i) = l_scanned_team_id(j) THEN
                           l_broken_team(k) := l_unscanned_team_id(i);
                           k := k + 1;
                           EXIT;
                        END IF;
                     END IF;
                  END LOOP;
               ELSE
                  o_reopen_user := NULL;
               END IF;
            END IF;
            l_prev_unscanned_team_id :=  l_unscanned_team_id(i);
         END LOOP;
      ELSE
         IF l_scanned_user_id.COUNT = 0 THEN
            o_reopen_user := NULL;
         END IF;
         
         o_end_user := NULL; 
      END IF;
      
      -- Get all unscanned INDIVIDUAL and WHOLE TEAMS to end
      IF k > 1 THEN 
         IF l_unscanned_user_id.COUNT > 0 THEN
            FOR i IN l_unscanned_user_id.FIRST .. l_unscanned_user_id.LAST LOOP
               IF l_unscanned_user_type(i) = 'IND' THEN
                  o_end_user := o_end_user || ',' || l_unscanned_user_id(i);
               END IF;
            END LOOP;
            
            l_prev_unscanned_team_id := NULL;
            FOR i IN l_unscanned_user_id.FIRST .. l_unscanned_user_id.LAST LOOP
               IF l_unscanned_team_id(i) IS NOT NULL AND l_unscanned_user_type(i) = 'TEAM' THEN
                  FOR k IN l_broken_team.FIRST .. l_broken_team.LAST LOOP
                     IF l_unscanned_team_id(i) <> l_broken_team(k) THEN
                        o_end_user := o_end_user || ',' || l_unscanned_user_id(i);
                     END IF;
                  END LOOP;
               END IF;
               l_prev_unscanned_team_id :=  l_unscanned_team_id(i);
            END LOOP;
         ELSE
            o_end_user := NULL;
         END IF;
         o_screen_code := 'SCAN_USER';
         o_end_user := TRIM(BOTH ',' FROM o_end_user);
      ELSE
      -- IF there's no broken teams, accept the scanning user and end all scanned/unscanned users then reopen the scanned users.
         IF l_scanned_user_id.COUNT > 0 THEN
            FOR i IN l_scanned_user_id.FIRST .. l_scanned_user_id.LAST LOOP
               o_reopen_user := o_reopen_user || ',' || l_scanned_user_id(i) ;
            END LOOP;
            o_reopen_user := TRIM(BOTH ',' FROM o_reopen_user);
         END IF;
         
         IF l_unscanned_user_id.COUNT > 0 THEN
            FOR i IN l_unscanned_user_id.FIRST .. l_unscanned_user_id.LAST LOOP
               o_end_user := o_end_user || ',' || l_unscanned_user_id(i) ;
            END LOOP;
            o_end_user := TRIM(BOTH ',' FROM o_end_user);
         END IF;
         
         o_screen_code := 'SCAN_LAST';
      END IF;
      
      IF o_reopen_user IS NOT NULL THEN
         SELECT LENGTH(NVL(o_reopen_user,0)) - LENGTH(REPLACE(NVL(o_reopen_user,0),',',NULL)) + 1 INTO l_num_users
           FROM DUAL;
      END IF;
        
      IF i_team_ind = 'Y' THEN
         OPEN c_get_team_id;
         FETCH c_get_team_id INTO l_team_id;
         CLOSE c_get_team_id;
         
         OPEN c_get_count_users(l_team_id);
         FETCH c_get_count_users INTO l_count_users;
         CLOSE c_get_count_users;
         
         l_num_users := l_num_users + l_count_users;
         
      ELSE
         l_num_users := l_num_users + 1;
      END IF;
         
      
      IF o_screen_code = 'SCAN_LAST' THEN
         l_assign_last := assign_last_cid (i_facility_id,
                                           i_user_id,
                                           'ALL',
                                           o_reopen_user,
                                           o_end_user,
                                           i_door_id,
                                           i_first_container_id,
                                           NULL,
                                           'SCAN_IN', 
                                           l_count_remaining_users,
                                           l_remaining_users,
                                           l_next_container_id,
                                           l_error_msg_code,
                                           l_error_msg_typ);
                                           
         l_assign_first := assign_first_cid (i_facility_id,
                              i_user_id,
                              o_reopen_user,
                              i_first_container_id,
                              i_door_id,
                              i_trailer_id,
                              i_route,
                              i_team_ind,
                              l_num_users,
                              l_error_msg_code,
                              l_error_msg_typ);
      ELSE
         l_assign_last := assign_last_cid (i_facility_id,
                                           i_user_id,
                                           'ALL',
                                           o_reopen_user,
                                           o_end_user,
                                           i_door_id,
                                           i_first_container_id,
                                           NULL,
                                           'SCAN_IN', 
                                           l_count_remaining_users,
                                           l_remaining_users,
                                           l_next_container_id,
                                           l_error_msg_code,
                                           l_error_msg_typ);
      END IF;
      RETURN ('Y');

      EXCEPTION
      WHEN OTHERS THEN
         log_oracle_error (i_facility_id, 'CONVEYABLE_CARTON_LOAD.supervisor_done', SQLERRM);
         RETURN ('N');
      END supervisor_done;
END CONVEYABLE_CARTON_LOAD; 