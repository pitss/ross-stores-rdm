function setTeamPageInitialFocus() {
    setTimeout(function () {
        setFocusOrSelectOnUIcomp("input[id*='tuid']");

    },
0);
}

function setConveyableCartonInitialFocus() {
    setTimeout(function () {
        setFocusOrSelectOnUIcomp("input[id*='shipcont']");

    },
0);
}

function setSupervisorLoginInitialFocus() {
    setTimeout(function () {
        setFocusOrSelectOnUIcomp("input[id*='use2']");

    },
0);
}

function setScanBadgeInitialFocus() {
    setTimeout(function () {
        setFocusOrSelectOnUIcomp("input[id*='userId']");

    },
0);
}

function initFocusDeleteTable() {
    setTimeout(function () {
   SetFocusOnUIcomp(customFindElementById('delTab'));

    },
0);
}

function setFocusonExitLink(){
     setTimeout(function () {
   SetFocusOnUIcomp(customFindElementById('delf3'));

    },
0);
}
function OnBlurDeleteTeamHandler() {
    setTimeout(function () {
       SetFocusOnUIcomp(customFindElementById('delTab'));

    },
    0);
}

function OnBlurSupViewHandler() {
    setTimeout(function () {
       SetFocusOnUIcomp(customFindElementById('supview'));

    },
    0);
}

function onKeyPressedDeleteTeamTable(event) {
    var keyPressed = event.getKeyCode();
    var fLink;
      var  srcComponent = event.getSource();
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        if (keyPressed == AdfKeyStroke.F3_KEY) {
            fLink = srcComponent.findComponent('delf3');
        }
        else if (keyPressed == AdfKeyStroke.F7_KEY) {
            fLink = srcComponent.findComponent('f7');

        }
        if (fLink != null) {
            //fLink.focus();
            AdfActionEvent.queue(fLink, true);
        }
    }
    else if(keyPressed == AdfKeyStroke.ARROWDOWN_KEY || keyPressed == AdfKeyStroke.ARROWUP_KEY){
    $("[id*='testButn']").click();
        return;
    }

}

function onKeyF7PressedDeleteTeamTable(event) {
    var fLink;
    var  srcComponent = event.getSource();
    fLink = srcComponent.findComponent('delTab');
    fLink.focus();
}
function onKeyPressedOnContainer(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        this.onKeyPressedConveyableCartonLoad(event, 'shipcont');
    }
}

function onKeyPressedOnUserId(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    var clientId = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F5_KEY) && clientId == 'userId') {
        event.cancel();
    }

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : 'userId'
        },
true);
        event.cancel();
    }
}

function onKeyPressedOnTeamUSer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    var clientId = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F5_KEY) && clientId == 'tuid') {
        event.cancel();
    }

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : 'tuid'
        },
true);
        event.cancel();
    }
}

function onKeyPressedSuperVisorUserName(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    var clientId = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F5_KEY) && clientId == 'use1') {
        event.cancel();
    }

    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : 'use1'
        },
true);
        event.cancel();
    }
}

function custFind(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function onKeyPressedSuperVisorPassword(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    var clientId = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F5_KEY) && clientId == 'pas1') {
        event.cancel();
    }

    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : 'pas1'
        },
true);
        event.cancel();
    }
}

function onKeyPressedConveyableCartonLoad(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    var clientId = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F8_KEY) && clientId == 'shipcont') {
        event.cancel();
    }

    else if (keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : item
        },
true);
        event.cancel();
    }
}

function OnOpenUseratDoorPopup(event) {
    OnOpen(event, 'nel');
}

function OnOpenLastCartonConfirmPopup(event) {
    OnOpen(event, 'nlc');
}

function OnOpenRemainingUserPopup(event) {
    OnOpen(event, 'remmb');
}
function OnOpenConfirmDoorPopup(event) {
    OnOpen(event, 'ncdp');
}

function OnOpenEndSessionPopup(event) {
    OnOpen(event, 'nces');
}

function OnOpenCancelSessionPopup(event) {
//alert('inside cancel Session popup');
    OnOpen(event, 'ncs');
}

function OnOpenUserScanValidationPopup(event) {
    OnOpen(event, 'nusp');
}

function OnOpenTeamExitPopup(event) {
    OnOpen(event, 'ntep');
}

function OnOpenTeamDeletePopup(event) {
    OnOpen(event, 'ntdp');

}

function onOpenSupervisorViewPopup(event) {
  setTimeout(function () {
   SetFocusOnUIcomp(customFindElementById('supview'));

    },
0);
}

function OnOpen(event, linkId) {
    var component = event.getSource();
    var linkComp = component.findComponent(linkId);
    linkComp.focus();
}

function noYesUserAtDoorPopup(event) {
    fLinksKeyHandler(event, 'cudp', 'yel')
}

function noYesRemUserPopup(event) {
    fLinksKeyHandler(event, 'remm', 'remmb')
}

function noYesConfirmLastCartonPopup(event) {
    fLinksKeyHandler(event, 'clp', 'ylc')
}

function noYesConfirmDoorPopup(event) {
    fLinksKeyHandler(event, 'cdp', 'ycdp');
}

function noYesEndSessionPopup(event) {
    fLinksKeyHandler(event, 'cesp', 'yces');
}

function noYesCancelSessionPopup(event) {
    fLinksKeyHandler(event, 'csp', 'ycs');
}

function noYesUserScanPopup(event) {
    fLinksKeyHandler(event, 'usp', 'yusp');
}

function noYesTeamDeletePopup(event) {
    fLinksKeyHandler(event, 'tdp', 'ytdp');
}

function noYesTeamExitPopup(event) {
    fLinksKeyHandler(event, 'tep', 'ytep');
}

function exitSupsView(event) {
 var keyPressed = event.getKeyCode();
    var fLink;
      var  srcComponent = event.getSource();
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        if (keyPressed == AdfKeyStroke.F3_KEY) {
            fLink = srcComponent.findComponent('svpe');
        }else{
            event.cancel();
        }
        if (fLink != null) {
            //fLink.focus();
            AdfActionEvent.queue(fLink, true);
        }
    }
    else if(keyPressed == AdfKeyStroke.ARROWDOWN_KEY || keyPressed == AdfKeyStroke.ARROWUP_KEY){
    $("[id*='supbtn']").click();
        return;
    }
event.cancel();

}

function fLinksKeyHandler(event, popupId, yesLink) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var item = component.getClientId().split(':')[3];
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }

    if ((keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY)) {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F1_KEY) {
            fLink = component.findComponent(yesLink);
            fLink.focus();
        }
        AdfCustomEvent.queue(fLink, "customKeyEvent", 
        {
            keyPressed : keyPressed, item : item
        },
true);
    }
event.cancel();
}