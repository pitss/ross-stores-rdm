package com.ross.rdm.productivity.hhconveyablecartonload.view.bean;
import oracle.adf.share.logging.ADFLogger;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.context.AdfFacesContext;
public class HhConveyorCartonLoadBean {
    private static ADFLogger log = ADFLogger.createADFLogger(HhConveyorCartonLoadBean.class);
    public HhConveyorCartonLoadBean() {
    }
    private String focus = null;
    private final static String CONTAINER_ID = "shipcont";
    private String isFocusOn;
    private static final String HHCONVEYORCARTONLOAD="hhconveyorcartonload";
    private static final String REFRESHACTION="refreshAction";
    private static final String CREATEINSERTCONVEYABLECARTONLOADROW="CreateInsertConveyableCartonLoadRow";
    private static final String ACTION_TYPE_="ACTION_TYPE";
    private static final String SETGLOBALVARIABLESCONVEYORCUTOFF="setGlobalVariablesConveyorCutOff";
    private static final String SCAN_OUT_="SCAN_OUT";
    private static final String USE2="use2";
    private static final String SETCONVEYORCARTONLOADWORKVARIABLES="setConveyorCartonLoadWorkVariables";
    private String isConveyorCatornPanelVisible;
    private String isScanPanelVisible;
    private String isTeamPanelVisible;

    public void setIsTeamPanelVisible(String isTeamPanelVisible) {
        this.isTeamPanelVisible = isTeamPanelVisible;
    }

    public String getIsTeamPanelVisible() {
        return isTeamPanelVisible;
    }

    public void setIsDeleteTeamPanelVisible(String isDeleteTeamPanelVisible) {
        this.isDeleteTeamPanelVisible = isDeleteTeamPanelVisible;
    }

    public String getIsDeleteTeamPanelVisible() {
        return isDeleteTeamPanelVisible;
    }
    private String isDeleteTeamPanelVisible;

    public void setIsConveyorCatornPanelVisible(String isConveyorCatornPanelVisible) {
        this.isConveyorCatornPanelVisible = isConveyorCatornPanelVisible;
    }

    public String getIsConveyorCatornPanelVisible() {
        return isConveyorCatornPanelVisible;
    }

    public void setIsScanPanelVisible(String isScanPanelVisible) {
        this.isScanPanelVisible = isScanPanelVisible;
    }

    public String getIsScanPanelVisible() {
        return isScanPanelVisible;
    }

    public void setIsSuperVisoranelVisible(String isSuperVisoranelVisible) {
        this.isSuperVisoranelVisible = isSuperVisoranelVisible;
    }

    public String getIsSuperVisoranelVisible() {
        return isSuperVisoranelVisible;
    }
    private String isSuperVisoranelVisible;

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }
    public String getIsFocusOn() {
        return isFocusOn;
    }
    public String initTaskFlow() {
        log.info("Conveyable carton load page initialization begun");
        log.info("Set the Global Variables for the form");
        initTaskFlowmethods(SETGLOBALVARIABLESCONVEYORCUTOFF);
        initTaskFlowmethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        initTaskFlowmethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        log.info("Set the Work Variables for the form");
        this.setFocus(CONTAINER_ID);
        setIsFocusOn(USE2);
        setIsConveyorCatornPanelVisible("YES");
        setIsScanPanelVisible("YES");
        setIsSuperVisoranelVisible("YES");
        setIsDeleteTeamPanelVisible("YES");
        setIsTeamPanelVisible("YES");
        log.info("Conveyable carton load page initialization completed");
        return HHCONVEYORCARTONLOAD;
    }
    public void initTaskFlowmethods(String opName) {
        log.info("Start of initTaskFlowmethods Method");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(opName);
        if (SETCONVEYORCARTONLOADWORKVARIABLES.equalsIgnoreCase(opName)) {
            oper.getParamsMap().put(REFRESHACTION, false);
            Boolean isScanOut = (Boolean) oper.execute();
            if(Boolean.TRUE.equals(isScanOut)){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
            }
        }
        else{
        oper.execute();
        }
        log.info("End of initTaskFlowmethods Method");
    }
    public void setFocus(String focus) {
        log.info("Start of setFocus Method");
        this.focus = focus;
        log.info("End of setFocus Method");
    }
    public String getFocus() {
        return focus;
    }
}
