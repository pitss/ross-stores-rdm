package com.ross.rdm.productivity.hhconveyablecartonload.view.framework;



import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.jbo.ApplicationModule;
import oracle.jbo.server.DBTransaction;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class RDMShippingBackingBean extends RDMMobileBaseBackingBean {

    private static String _appModuleName = "ProductivityAppModule";
    private static String _appModuleDataControlName = "ProductivityAppModuleDataControl";

    public RDMShippingBackingBean() {
    }

    /**
     * Return name of the Application Module
     * set in static class attribute _appModuleName .
     *
     * @return _appModuleName
     */
    public String getAppModuleName() {
        return _appModuleName;
    }

    /**
     * Return name of the Application Module Data Control Name
     * set in static class attribute _appModuleDataControlName .
     *
     * @return _appModuleDataControlName
     */
    public String getAppModuleDataControlName() {
        return _appModuleDataControlName;
    }

    /**
     * Return Application Module
     *
     * @return application module
     */
    public ApplicationModule getAppModule() {
        return ADFUtils.getApplicationModuleForDataControl(getAppModuleDataControlName());
    }

    /**
     * Return BindingContainer of page
     *
     * @return current page binding container
     */
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    /**
     * sets the cursor to the given component id
     *
     * @param  componentId of item on page
     */
    public void setFocusOnUIComponent(String componentId) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);

        UIComponent uiComponent = facesContext.getViewRoot().findComponent(componentId);
        service.addScript(facesContext,
                          " var t=document.getElementById('" + uiComponent.getClientId(facesContext) + "::content'); " +
                          " t.focus();");
    }

    /**
     * sends a partial page submit for the given component
     *
     * @param  component on page
     */
    public void refreshContentOfUIComponent(UIComponent component) {
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(component);
    }

    /**
     * Return current DBTransaction
     *
     * @return current DBTransaction
     */
    public DBTransaction getDBTransaction() {
        return (DBTransaction) getAppModule().getTransaction();
    }

    public String logoutExitBTF() {
        // PITSS.CON NOTE : If you want to navigate to the calling Task Flow/Page Flow,
        //   then please comment out the following code and replace with
        //   return "backGlobalHome";
        return "backGlobalHome";
        //ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
        //HttpSession session = (HttpSession)ectx.getSession(false);
        //String temp = ControllerContext.getInstance().getGlobalViewActivityURL("exit");
        //try {
        //   ectx.redirect(temp);
        //   session.invalidate();
        //} catch (Exception ex) {
        //  ex.printStackTrace();
        //}
        //return null;
    }
    
    public void selectTextOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("if(textInput != null){textInput.select();}");

        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }
}

