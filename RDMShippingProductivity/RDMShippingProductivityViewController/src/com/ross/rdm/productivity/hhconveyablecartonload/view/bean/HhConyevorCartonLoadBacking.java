package com.ross.rdm.productivity.hhconveyablecartonload.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.productivity.hhconveyablecartonload.model.views.HhConveyableCartonLoadViewRowImpl;
import com.ross.rdm.productivity.hhconveyablecartonload.view.framework.RDMShippingBackingBean;

import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

@SuppressWarnings("oracle.jdeveloper.java.serialversionuid-field-missing")
public class HhConyevorCartonLoadBacking extends RDMShippingBackingBean {
    private static ADFLogger log = ADFLogger.createADFLogger(HhConyevorCartonLoadBacking.class);
    private final static String LANGUAGE_CODE_ATTR = "AM";
    private RichPanelFormLayout conveyorCartolLoadPanel;
    private RichPanelGroupLayout allMessagesPanel;
    private RichPopup confirmationPopup;
    private RichInputText containerId;
    private RichIcon containerIdIcon;
    private RichPopup confirmDoorPopup;
    private RichPopup confirmEndSessionPopup;
    private RichPanelLabelAndMessage containerPLM;
    private RichInputText scanUserId;
    private RichPanelGroupLayout supervisorMessagePanel;
    private RichPopup supervisorViewPopup;
    private RichPopup unScannBadgeValidatonPopup;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichIcon scanUserIdIcon;
    private RichIcon scanBadgeIcon;
    private String focus = null;
    private RichPopup confirmCancelSessionPopup;
    private RichTable scanUserBadgeTable;
    private RichLink f3ExitLink;
    private RichLink f5TeamLink;
    private RichLink f6Refresh;
    private RichLink f9CancelLink;
    private RichLink f3BadgeScanExitLink;
    private RichLink f4BadgeScanDoneLink;
    private RichLink f1BadgeScanYesLink;
    private RichLink f2BadgeScanNoLink;
    private RichLink f2SupsViewLink;
    private RichLink f3SupsExitLink;
    private RichLink f4SupsDoneLink;
    private RichInputText supervisorPassword;
    private RichLink f1UserAtDoorLink;
    private RichLink f2UserAtDoorLink;
    private RichLink f1ConfirmDoorLink;
    private RichLink f2ConfirmDoorLink;
    private RichLink f1ConfirmCancel;
    private RichLink f2ConfirmCancel;
    private RichLink f1ConfirmEndSession;
    private RichLink f2ConfirmEndSession;
    private RichLink f3SupsViewExitLink;
    private RichPanelGroupLayout iconPanelMessage;
    private RichPanelGroupLayout supervisorIconMessagePanel;
    private RichPanelLabelAndMessage userName;
    private RichInputText supervisorUserId;
    private String userAuthFocus = null;
    private String remainingUserPopupText;
    private static final String MULT_USER_DOOR_ = "MULT_USER_DOOR";
    private static final String CREATEINSERTCONVEYABLECARTONLOADROW = "CreateInsertConveyableCartonLoadRow";
    private static final String CONFIRMASSIGNLASTCARTON = "confirmAssginLastCarton";
    private static final String SCREENCODE = "ScreenCode";
    private static final String SUPERVISORLOGIN = "superVisorLogin";
    private static final String CREATEINSERTCONVEYABLECARTONROW = "CreateInsertConveyableCartonRow";
    private static final String SCAN_USER_ = "SCAN_USER";
    private static final String USER_LOGGED_IN_ = "USER_LOGGED_IN";
    private static final String SCP = "SCP";
    private static final String SING_USER_DOOR_ = "SING_USER_DOOR";
    private static final String USERNAME = "userName";
    private static final String SCAN_OUT_ = "SCAN_OUT";
    private static final String SCAN_IN_ = "SCAN_IN";
    private static final String PACTION = "pAction";
    private static final String BACKGLOBALHOME = "backGlobalHome";
    private static final String GETTEAMDETAILS = "getTeamDetails";
    private static final String INC_SCANNED_ = "INC_SCANNED";
    private static final String KEYPRESSED = "keyPressed";
    private static final String BACKTOCONVEYABLECARTONLOAD = "backToConveyableCartonLoad";
    private static final String PCONTAINERID = "pContainerId";
    private static final String BACKTOSCANUSERBADGE = "backToScanUserBadge";
    private static final String ACTIONTYPE = "actionType";
    private static final String SCANNEDUSER = "ScannedUser";
    private static final String SCANUSER = "scanUser";
    private static final String SVPE = "svpe";
    private static final String ALL = "ALL";
    private static final String ASSIGNLASTCARTON = "assignLastCarton";
    private static final String HHCONVEYABLECARTONLOADVIEWITERATOR = "HhConveyableCartonLoadViewIterator";
    private static final String ENDUSERSSESSION = "endUsersSession";
    private static final String ACTION_TYPE_ = "ACTION_TYPE";
    private static final String LAST_CARTON_ = "LAST_CARTON";
    private static final String USE2 = "use2";
    private static final String PUSERID = "pUserId";
    private static final String END_UNSCANNED_ = "END_UNSCANNED";
    private static final String ERRORCODE = "ErrorCode";
    private static final String PARTIAL_ENTRY_ = "PARTIAL_ENTRY";
    private static final String E = "E";
    private static final String REFRESHACTION = "refreshAction";
    private static final String CREATECONVEYABLECARTONSUPLOGINROW = "CreateConveyableCartonSupLoginRow";
    private static final String INV_USER_ID_ = "INV_USER_ID";
    private static final String HHCONVEYORCARTONLOADBEAN = "HhConveyorCartonLoadBean";
    private static final String N = "N";
    private static final String PASSWORD_1 = "password";
    private static final String ON = "ON";
    private static final String ASSIGNFIRSTCARTON = "assignFirstCarton";
    private static final String END_SESSION_ = "END_SESSION";
    private static final String SCAN_LAST_ = "SCAN_LAST";
    private static final String CREATEINSERTCONVEYABLECARTONTEAMROW = "CreateInsertConveyableCartonTeamRow";
    private static final String SETCONVEYORCARTONLOADWORKVARIABLES = "setConveyorCartonLoadWorkVariables";
    private static final String EMPTY = "";
    private static final String VALIDATEFIRSTCONTAINER = "validateFirstContainer";
    private static final String BACKTOCONVEYABLECARTON = "backToConveyableCarton";
    private static final String USERTOEND = "userToEnd";
    private static final String ERROR = "error";
    private static final String CARTON_LOAD_ = "CARTON_LOAD";
    private static final String SCANBADGECOMPLETE = "scanBadgeComplete";
    private static final String TEAMCOUNT = "TeamCount";
    private static final String VALIDATESUPERVISORLOGIN = "validateSupervisorLogin";
    private static final String NUM_USERS_ = "NUM_USERS";
    private static final String RESETCONVEYABLECARTONLOADPAGE = "resetConveyableCartonLoadPage";
    private static final String STATUS = "Status";
    private static final String SAVEUSERSCANDEATILS = "saveUserScanDeatils";
    private static final String SUBMITTEDVALUE = "submittedValue";
    private static final String ITEM = "item";
    private static final String SCANUSERID = "scanUserId";
    private static final String SUPERVISORDONEACTION = "supervisorDoneAction";
    private RichPanelGroupLayout conveyorCartonMesagePanel;
    private RichButton supViewButton;
    private RichTable supViewTableBinding;
    private RichPopup lastCartonConfirmPopup;
    private RichLink f1Confirmlastcarton;
    private RichLink f2ConfirmLastcarton;
    private RichPopup remUsersPopup;
    private RichLink f1RemUsersPopup;
    private RichPopup remainingUsersConfirmPopup;
    private RichLink f1RemainingUserPopupLink;
    private RichOutputText remainingUsersTextBiding;

    public void f3ActionListener(ActionEvent actionEvent) {
        log.info("Start of f3ActionListener Method");
        ADFUtils.invokeAction(BACKGLOBALHOME);
        log.info("End of f3ActionListener Method" + actionEvent.getSource());
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        log.info("Start of onChangedContainerId Method");
        this.updateModel(vce);
        removeErrorOfField(getContainerId(), getContainerIdIcon());
        hideMessagesPanel();
        getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            if(isPageDefAvailable()){

            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            String actionType = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(ACTION_TYPE_);
            actionType = actionType == null ? SCAN_IN_ : actionType;
            this.getTrailerDoorValue(enteredContainerIdValue, actionType);
            }
        } else {
            resetConveyableCartonPage(PARTIAL_ENTRY_);
        }
        log.info("End of onChangedContainerId Method");
    }

    public void getTrailerDoorValue(String containerId, String actionType) {
        log.info("Start of getTrailerDoorValue Method");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(VALIDATEFIRSTCONTAINER);
        oper.getParamsMap().put(PCONTAINERID, containerId);
        oper.getParamsMap().put(PACTION, actionType);
        Map<String, String> responseList = (Map<String, String>) oper.execute();
        String status = responseList.get(STATUS);
        if (LAST_CARTON_.equalsIgnoreCase(status)) {
            this.getLastCartonConfirmPopup().show(new RichPopup.PopupHints());
            log.info("User has an open transcation");
//            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
//            executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
//            executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
//            changeCartonLabel();
//            resetConveyableCartonPage(EMPTY);
        } else if (MULT_USER_DOOR_.equals(status)) {
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(SCP, responseList.get(SCP));
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(NUM_USERS_, responseList.get(NUM_USERS_));
            this.getConfirmationPopup().show(new RichPopup.PopupHints());
        } else if (SING_USER_DOOR_.equals(status)) {
            OperationBinding assignFirstOp = (OperationBinding) ADFUtils.findOperation(ASSIGNLASTCARTON);
            assignFirstOp.getParamsMap().put(USERTOEND, ALL);
            assignFirstOp.getParamsMap().put(ACTIONTYPE, SCAN_OUT_);
            assignFirstOp.execute();
            executeOpMethods(ASSIGNFIRSTCARTON);
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_IN_);
            changeCartonLabel();
            resetConveyableCartonPage(EMPTY);
        } else {
            resetConveyableCartonPage(responseList.get(ERRORCODE).toString());
        }
        log.info("End of getTrailerDoorValue Method");
    }

    private void changeCartonLabel() {
        log.info("Start of changeCartonLabel Method");
        String actionType = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(ACTION_TYPE_);
        actionType = actionType == null ? SCAN_IN_ : actionType;
        if (SCAN_IN_.equals(actionType)) {
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        } else {
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_IN_);
        }
        if (null != getContainerIdIcon()) {
            refreshContentOfUIComponent(this.getContainerIdIcon());
        }
        if (null != getContainerPLM()) {
            refreshContentOfUIComponent(this.getContainerPLM());
        }
        if (null != getConveyorCartolLoadPanel()) {
            refreshContentOfUIComponent(this.getConveyorCartolLoadPanel());
        }
        log.info("End of changeCartonLabel Method");
    }

    public void f5ActionListener(ActionEvent actionEvent) {
        removeErrorOfField(getContainerId(), getContainerIdIcon());
        hideMessagesPanel();
        log.info("Start of f5ActionListener Method");
        executeOpMethods(CREATEINSERTCONVEYABLECARTONTEAMROW);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GETTEAMDETAILS);
        Map<String, String> responseList = (Map<String, String>) oper.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(TEAMCOUNT, (String) responseList.get(TEAMCOUNT));
        if (responseList.get(ERRORCODE) != null) {
            resetConveyableCartonPage(responseList.get(ERRORCODE).toString());
        } else {
            getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("NO");
            getHhConveyorCartonLoadBean().setIsTeamPanelVisible("YES");
            
            ADFUtils.invokeAction(GETTEAMDETAILS);
        }
        log.info("End of f5ActionListener Method");
    }

    public void f6ActionListener(ActionEvent actionEvent) {
        log.info("Start of f6ActionListener Method");
        ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).executeQuery();
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        resetConveyableCartonPage(EMPTY);
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
        log.info("End of f6ActionListener Method");
    }

    public void resetConveyableCartonPage(String errorCode) {
        if (EMPTY.equalsIgnoreCase(errorCode)) {
            HhConveyableCartonLoadViewRowImpl hhConveyableCartonRow = this.getConveyableCartonRow();
            if (hhConveyableCartonRow.getFirstContainer() != null) {
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);

            } else {
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_IN_);

            }
            hhConveyableCartonRow.setContainerId(null);
            getContainerId().setValue(null);
            getContainerId().resetValue();
            hideMessagesPanel();
            removeErrorOfField(getContainerId(), getContainerIdIcon());
        } else {
            getContainerIdIcon().setName(ERROR);
            addErrorStyleToComponent(this.getContainerId());
            showMessagesPanel(E, this.getMessage(errorCode, E, null, this.getLangCodeAttrValue()));
        }
        if (!(USER_LOGGED_IN_.equalsIgnoreCase(errorCode))) {
            executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        }
        refreshContentOfUIComponent(this.getContainerIdIcon());
        refreshContentOfUIComponent(this.getContainerPLM());
        refreshContentOfUIComponent(this.getConveyorCartolLoadPanel());
    }
    
    public String executeKeyPrivilege(String keyName){
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("checkKeyPrivilege");
            oper.getParamsMap().put("optionName", keyName);
        String result = (String) oper.execute();
        return result;
    }

    public void f9ActionListener(ActionEvent actionEvent) {
        log.info("Start of f9ActionListener Method");
        String keyPrivilege=executeKeyPrivilege("F9");
        if(!("OFF".equalsIgnoreCase(keyPrivilege))){
        HhConveyableCartonLoadViewRowImpl hhConveyableCartonRow = this.getConveyableCartonRow();
        if (hhConveyableCartonRow != null) {
            if (hhConveyableCartonRow.getFirstContainer() != null) {
                this.getConfirmCancelSessionPopup().show(new RichPopup.PopupHints());
            } else {
                getContainerIdIcon().setName(ERROR);
                addErrorStyleToComponent(this.getContainerId());
                showMessagesPanel(E, this.getMessage(PARTIAL_ENTRY_, E, null, this.getLangCodeAttrValue()));
                refreshContentOfUIComponent(this.getContainerIdIcon());
                refreshContentOfUIComponent(this.getContainerPLM());
                refreshContentOfUIComponent(this.getConveyorCartolLoadPanel());
            }
        }
        }
        else{
            getContainerIdIcon().setName(ERROR);
            addErrorStyleToComponent(this.getContainerId());
            showMessagesPanel(E, this.getMessage("NOT_ALLOWED", E, null, this.getLangCodeAttrValue()));
            refreshContentOfUIComponent(this.getContainerIdIcon());
            refreshContentOfUIComponent(this.getContainerPLM());
            refreshContentOfUIComponent(this.getConveyorCartolLoadPanel());
        }
        log.info("End of f9ActionListener Method");
    }

    private HhConveyableCartonLoadViewRowImpl getConveyableCartonRow() {
        if (log.isInfo()) {
            log.info("Start of getConveyableCartonUserListRow Method");
        }
        return (HhConveyableCartonLoadViewRowImpl) ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).getCurrentRow();
    }

    public void executeOpMethods(String opName) {
        log.info("Start of executeOpMethods Method- Genric method to call opBinding");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(opName);
        if (SETCONVEYORCARTONLOADWORKVARIABLES.equalsIgnoreCase(opName)) {
            oper.getParamsMap().put(REFRESHACTION, false);
        }
        oper.execute();
        log.info("End of executeOpMethods Method");
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void yesConfirmUserAtDoorActionListener(ActionEvent actionEvent) {
        log.info("This method will be call when user press Yes for Are there users at door popup");
        this.getConfirmationPopup().hide();
        this.getConfirmDoorPopup().show(new RichPopup.PopupHints());
        log.info("End of yesConfirmUserAtDoorActionListener Method");
    }

    public void noConfirmUserAtDoorActionListener(ActionEvent actionEvent) {
        log.info("Start of noConfirmUserAtDoorActionListener Method");
        this.getConfirmationPopup().hide();
        this.getConfirmEndSessionPopup().show(new RichPopup.PopupHints());
        log.info("End of noConfirmUserAtDoorActionListener Method");
    }

    public void yesConfirmExitLinkActionListener(ActionEvent actionEvent) {
        log.info("Start of yesConfirmExitLinkActionListener Method");
        this.getConfirmDoorPopup().hide();
        executeOpMethods(SAVEUSERSCANDEATILS);
        executeOpMethods(CREATEINSERTCONVEYABLECARTONTEAMROW);
        ADFUtils.invokeAction(SCANUSERID);
        log.info("End of yesConfirmExitLinkActionListener Method");
    }

    public void noConfirmExitLinkActionListener(ActionEvent actionEvent) {
        log.info("Start of noConfirmExitLinkActionListener Method");
        this.getConfirmDoorPopup().hide();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(RESETCONVEYABLECARTONLOADPAGE);
        oper.execute();
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        changeCartonLabel();
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
        log.info("End of noConfirmExitLinkActionListener Method");
    }

    public void yesConfirmEndSession(ActionEvent actionEvent) {
        String scp = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(SCP);
        log.info("Start of yesConfirmUnscannedBadgeSession Method");
        this.getConfirmEndSessionPopup().hide();
        if (N.equalsIgnoreCase(scp)) {
            executeOpMethods(SUPERVISORDONEACTION);
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
            ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).executeQuery();
            executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
            executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
            if (this.getContainerId() != null) {
                setFocusOnUIComponent(this.getContainerId());
                refreshContentOfUIComponent(this.getContainerId());
                if(this.getContainerPLM()!=null){
                refreshContentOfUIComponent(this.getContainerPLM());
                }
            }
        } else {
            hideMessagesPanel();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(END_SESSION_, CARTON_LOAD_);
            executeOpMethods(SAVEUSERSCANDEATILS);
            executeOpMethods(CREATECONVEYABLECARTONSUPLOGINROW);
            ADFUtils.invokeAction(SUPERVISORLOGIN);
            getHhConveyorCartonLoadBean().setIsFocusOn("use2");
            getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("NO");
            getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("YES");
            refreshContentOfUIComponent(getConveyorCartonMesagePanel());
        }
        log.info("Start of yesConfirmEndSession Method");
        log.info("End of yesConfirmEndSession Method");
    }

    public String noConfirmEndSession() {
        log.info("Start of noConfirmEndSession Method");
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        this.getConfirmEndSessionPopup().hide();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        changeCartonLabel();
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
        log.info("End of noConfirmEndSession Method");
        return null;
    }

    public void scanUserId(ValueChangeEvent valueChangeEvent) {
        log.info("Start of scanUserId Method");
        removeErrorOfField(getScanUserId(), getScanUserIdIcon());
        hideMessagesPanel();
        getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");
        refreshContentOfUIComponent(this.getIconPanelMessage());
        String userId = (String) valueChangeEvent.getNewValue();
        if (null != userId && !EMPTY.equals(userId)) {
            if(isPageDefAvailable()){

            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(SCANUSER);
            oper.getParamsMap().put(PUSERID, userId);
            Map<String, String> responseList = (Map<String, String>) oper.execute();
            if (null != responseList && responseList.size() > 0) {
                String status = responseList.get(SCANNEDUSER);
                if (INV_USER_ID_.equalsIgnoreCase(status)) {
                    getScanUserIdIcon().setName(ERROR);
                    refreshContentOfUIComponent(this.getScanUserIdIcon());
                    addErrorStyleToComponent(this.getScanUserId());
                    executeOpMethods(CREATEINSERTCONVEYABLECARTONTEAMROW);
                    refreshContentOfUIComponent(this.getScanUserId());
                    showMessagesPanel(E, this.getMessage(INV_USER_ID_, E, null, this.getLangCodeAttrValue()));
                } else {
                    executeOpMethods(CREATEINSERTCONVEYABLECARTONTEAMROW);
                    getScanUserId().setValue(null);
                    getScanUserId().resetValue();
                    refreshContentOfUIComponent(this.getScanUserId());
                    refreshContentOfUIComponent(this.getScanUserBadgeTable());
                }
            }
            //            else {
            //                executeOpMethods("CreateInsertConveyableCartonTeamRow");
            //                getScanUserId().setValue(null);
            //                refreshContentOfUIComponent(this.getScanUserId());
            //            }
        } else {
            getScanUserIdIcon().setName(ERROR);
            refreshContentOfUIComponent(this.getScanUserIdIcon());
            addErrorStyleToComponent(this.getScanUserId());
            executeOpMethods(CREATEINSERTCONVEYABLECARTONTEAMROW);
            refreshContentOfUIComponent(this.getScanUserId());
            showMessagesPanel(E, this.getMessage(PARTIAL_ENTRY_, E, null, this.getLangCodeAttrValue()));
        }}
        log.info("End of scanUserId Method");
    }

    public void setScanUserId(RichInputText scanUserId) {
        this.scanUserId = scanUserId;
    }

    public RichInputText getScanUserId() {
        return scanUserId;
    }

    public String f4DoneScanBadge() {
        log.info("Start of f4DoneScanBadge Method");
        hideMessagesPanel();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(SCANBADGECOMPLETE);
        Map<String, String> response = (Map<String, String>) oper.execute();
        if (response.size() > 0) {
            if (response.get(ERRORCODE) != null) {
                if (END_UNSCANNED_.equalsIgnoreCase(response.get(ERRORCODE))) {
                    this.getUnScannBadgeValidatonPopup().show(new RichPopup.PopupHints());
                } else if (INC_SCANNED_.equalsIgnoreCase(response.get(ERRORCODE))) {
                    showMessagesPanel(E,
                                      this.getMessage(response.get(ERRORCODE), E, null, this.getLangCodeAttrValue()));
                }
            }
        } else {
            getHhConveyorCartonLoadBean().setIsScanPanelVisible("NO");
            getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
            refreshContentOfUIComponent(this.getIconPanelMessage());
            executeOpMethods(SUPERVISORDONEACTION);
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
            ADFUtils.invokeAction(BACKTOCONVEYABLECARTON);
            ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).executeQuery();
            executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
            executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
            if (this.getContainerId() != null) {
                setFocusOnUIComponent(this.getContainerId());
            }

        }
        return EMPTY;
    }
    
    private boolean isPageDefAvailable(){
            boolean navigation = false;
            BindingContext bindingContext = BindingContext.getCurrent();        
            if(bindingContext.getCurrentBindingsEntry()!=null){
                navigation = true;
            }
            return navigation;
        } 

    public void onChangeSupervisorPassword(ValueChangeEvent valueChangeEvent) {
        log.info("Start of onChangeSupervisorPassword Method");
        this.updateModel(valueChangeEvent);
        if (valueChangeEvent.getNewValue() != null) {
            String password = valueChangeEvent.getNewValue().toString().replace("*", EMPTY);
            if (null != password && password.length() > 0) {
                if(isPageDefAvailable()){
                hideMessagesPanel();
                OperationBinding oper = (OperationBinding) ADFUtils.findOperation(VALIDATESUPERVISORLOGIN);
                oper.getParamsMap().put(USERNAME, EMPTY);
                oper.getParamsMap().put(PASSWORD_1, valueChangeEvent.getNewValue().toString());
                String result = (String) oper.execute();
                if (null != result && !ON.equals(result)) {
                    showMessagesPanel(E, this.getMessage(result, E, null, this.getLangCodeAttrValue()));
                }
                
                }
                //setFocusOnUIComponent(this.getSupervisorUserId());
            }
        } else {
            getHhConveyorCartonLoadBean().setIsFocusOn(getSupervisorUserId().getId());
            setFocusOnUIComponent(this.getSupervisorUserId());
            refreshContentOfUIComponent(getSupervisorUserId());
            refreshContentOfUIComponent(getSupervisorPassword());
        }
        log.info("End of onChangeSupervisorPassword Method");
    }

    public String f3SupervisorViewExitAction() {
        log.info("Start of f3SupervisorViewExitAction Method");
        this.getSupervisorViewPopup().hide();
        getHhConveyorCartonLoadBean().setIsFocusOn(this.getSupervisorPassword().getId());
        this.setFocusOnUIComponent(this.getSupervisorPassword());
        log.info("End of f3SupervisorViewExitAction Method");
        return null;
    }

    public String f2SupervisorViewAction() {
        log.info("Start of f2SupervisorViewAction Method");
        String keyPrivilege=executeKeyPrivilege("F2");
        if(!("OFF".equalsIgnoreCase(keyPrivilege))){
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(VALIDATESUPERVISORLOGIN);
        oper.getParamsMap().put(USERNAME, EMPTY);
        oper.getParamsMap().put(PASSWORD_1, EMPTY);
        String result = (String) oper.execute();
        if (null != result && !ON.equals(result)) {
            showMessagesPanel(E, this.getMessage(result, E, null, this.getLangCodeAttrValue()));
        } else {
            getHhConveyorCartonLoadBean().setIsFocusOn("supview");
            if(this.getSupervisorPassword()!=null){
            refreshContentOfUIComponent(this.getSupervisorPassword());
            }
            this.getSupervisorViewPopup().show(new RichPopup.PopupHints());
            this.writeJavaScriptToClient("SetFocusOnUIcomp(customFindElementById('supview'));");
        }
        }
        else{
            showMessagesPanel(E, this.getMessage("NOT_ALLOWED", E, null, this.getLangCodeAttrValue()));
 
        }
        log.info("End of f2SupervisorViewAction Method");
        return null;
    }

    public void yesConfirmUnscannedBadgeSession(ActionEvent actionEvent) {
        this.getUnScannBadgeValidatonPopup().hide();
        String scp = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(SCP);
        log.info("Start of yesConfirmUnscannedBadgeSession Method");
        if (N.equalsIgnoreCase(scp)) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(SUPERVISORDONEACTION);
            @SuppressWarnings("unchecked")
            Map<String, String> response = (Map<String, String>) oper.execute();
            if (response.size() > 0) {
                if (SCAN_LAST_.equalsIgnoreCase(response.get(SCREENCODE))) {
                    getHhConveyorCartonLoadBean().setIsScanPanelVisible("NO");
                    getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
                    refreshContentOfUIComponent(this.getIconPanelMessage());
                    AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
                    ADFUtils.invokeAction(BACKTOCONVEYABLECARTON);
                    ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).executeQuery();
                    executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
                    executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
                    if (this.getContainerId() != null) {
                        setFocusOnUIComponent(this.getContainerId());
                    }
                } else {
                    executeOpMethods(SAVEUSERSCANDEATILS);
                    getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");

                }
            } else {
                executeOpMethods(SAVEUSERSCANDEATILS);
                getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");

            }
        } else {
            getHhConveyorCartonLoadBean().setIsScanPanelVisible("NO");
            getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("YES");
            refreshContentOfUIComponent(this.getIconPanelMessage());
            hideMessagesPanel();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put(END_SESSION_, "SCAN_BADGE");
            executeOpMethods(CREATECONVEYABLECARTONSUPLOGINROW);
            ADFUtils.invokeAction("login");
            getHhConveyorCartonLoadBean().setIsFocusOn(USE2);
            if (this.getSupervisorUserId() != null) {
                this.setFocusOnUIComponent(this.getSupervisorUserId());
            }
        }
    }

    public void noConfirmUnscannedBadgesSession(ActionEvent actionEvent) {
        log.info("Start of noConfirmUnscannedBadgesSession Method");
        this.getUnScannBadgeValidatonPopup().hide();
        getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");
        if (this.getScanUserId() != null) {
            this.setFocusOnUIComponent(this.getScanUserId());
        }
        log.info("End of noConfirmUnscannedBadgesSession Method");
    }

    public void setSupervisorMessagePanel(RichPanelGroupLayout supervisorMessagePanel) {
        this.supervisorMessagePanel = supervisorMessagePanel;
    }

    public RichPanelGroupLayout getSupervisorMessagePanel() {
        return supervisorMessagePanel;
    }

    public String f3superVisorExitAction() {
        log.info("Start of f3superVisorExitAction Method");
        refreshContentOfUIComponent(getSupervisorIconMessagePanel());
        if (CARTON_LOAD_.equalsIgnoreCase((String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(END_SESSION_))) {
            ADFUtils.invokeAction(BACKTOCONVEYABLECARTONLOAD);
            if (ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR) != null) {
                ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).executeQuery();
            }
            executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
            executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
            getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
            getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("NO");


        } else {
            ADFUtils.invokeAction(BACKTOSCANUSERBADGE);
            getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");
            getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("NO");
        }
        log.info("End of f3superVisorExitAction Method");
        return null;
    }

    public String f4SupervisorDoneAction() {
        String keyPrivilege=executeKeyPrivilege("F4");
        if(!("OFF".equalsIgnoreCase(keyPrivilege))){
        hideMessagesPanel();
        log.info("Start of f4SupervisorDoneAction Method");
        OperationBinding oper1 = (OperationBinding) ADFUtils.findOperation(VALIDATESUPERVISORLOGIN);
        oper1.getParamsMap().put(USERNAME, EMPTY);
        oper1.getParamsMap().put(PASSWORD_1, EMPTY);
        String result = (String) oper1.execute();
        if (null != result && !ON.equals(result)) {
            showMessagesPanel(E, this.getMessage(result, E, null, this.getLangCodeAttrValue()));
        } else {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(SUPERVISORDONEACTION);
            @SuppressWarnings("unchecked")
            Map<String, String> response = (Map<String, String>) oper.execute();
            if (response.size() > 0) {
                if (SCAN_LAST_.equalsIgnoreCase(response.get(SCREENCODE))) {
                    getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("NO");
                    getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
                    refreshContentOfUIComponent(this.getSupervisorIconMessagePanel());
                    AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
                    ADFUtils.invokeAction(BACKTOCONVEYABLECARTONLOAD);
                    ADFUtils.findIterator(HHCONVEYABLECARTONLOADVIEWITERATOR).executeQuery();
                    executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
                    executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
                    if (this.getContainerId() != null) {
                        setFocusOnUIComponent(this.getContainerId());
                    }
                    
                } else if (SCAN_USER_.equalsIgnoreCase(response.get(SCREENCODE))) {
                    getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("NO");
                    getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");
                    executeOpMethods(SAVEUSERSCANDEATILS);
                    ADFUtils.invokeAction(BACKTOSCANUSERBADGE);
                }
            } else {
                getHhConveyorCartonLoadBean().setIsSuperVisoranelVisible("NO");
                getHhConveyorCartonLoadBean().setIsScanPanelVisible("YES");
                ADFUtils.invokeAction(BACKTOSCANUSERBADGE);
            }
        }
        }
        else{
            showMessagesPanel(E, this.getMessage("NOT_ALLOWED", E, null, this.getLangCodeAttrValue()));

        }
        //Added here to test super visor login creadential with static values
        log.info("End of f4SupervisorDoneAction Method");
        return null;
    }

    public String f3ScanBadgeExitAction() {
        log.info("Start of f3ScanBadgeExitAction Method");
        executeOpMethods(CREATEINSERTCONVEYABLECARTONROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_IN_);
        ADFUtils.invokeAction(BACKTOCONVEYABLECARTON);
        getHhConveyorCartonLoadBean().setIsScanPanelVisible("NO");
        getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
        refreshContentOfUIComponent(this.getIconPanelMessage());
        hideMessagesPanel();
        log.info("End of f3ScanBadgeExitAction Method");
        return null;
    }

    public void setSupervisorViewPopup(RichPopup supervisorViewPopup) {
        this.supervisorViewPopup = supervisorViewPopup;
    }

    public RichPopup getSupervisorViewPopup() {
        return supervisorViewPopup;
    }

    public void setUnScannBadgeValidatonPopup(RichPopup unScannBadgeValidatonPopup) {
        this.unScannBadgeValidatonPopup = unScannBadgeValidatonPopup;
    }

    public RichPopup getUnScannBadgeValidatonPopup() {
        return unScannBadgeValidatonPopup;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setConfirmDoorPopup(RichPopup confirmDoorPopup) {
        this.confirmDoorPopup = confirmDoorPopup;
    }

    public RichPopup getConfirmDoorPopup() {
        return confirmDoorPopup;
    }

    public void setConfirmEndSessionPopup(RichPopup confirmEndSessionPopup) {
        this.confirmEndSessionPopup = confirmEndSessionPopup;
    }

    public RichPopup getConfirmEndSessionPopup() {
        return confirmEndSessionPopup;
    }

    public void setContainerPLM(RichPanelLabelAndMessage containerPLM) {
        this.containerPLM = containerPLM;
    }

    public RichPanelLabelAndMessage getContainerPLM() {
        return containerPLM;
    }

    public void setConveyorCartolLoadPanel(RichPanelFormLayout conveyorCartolLoadPanel) {
        this.conveyorCartolLoadPanel = conveyorCartolLoadPanel;
    }

    public RichPanelFormLayout getConveyorCartolLoadPanel() {
        return conveyorCartolLoadPanel;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setScanUserIdIcon(RichIcon scanUserIdIcon) {
        this.scanUserIdIcon = scanUserIdIcon;
    }

    public RichIcon getScanUserIdIcon() {
        return scanUserIdIcon;
    }

    public void setScanBadgeIcon(RichIcon scanBadgeIcon) {
        this.scanBadgeIcon = scanBadgeIcon;
    }

    public RichIcon getScanBadgeIcon() {
        return scanBadgeIcon;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getFocus() {
        return focus;
    }

    public void setConfirmCancelSessionPopup(RichPopup confirmCancelSessionPopup) {
        this.confirmCancelSessionPopup = confirmCancelSessionPopup;
    }

    public RichPopup getConfirmCancelSessionPopup() {
        return confirmCancelSessionPopup;
    }

    public void yesConfirmCancelSession(ActionEvent actionEvent) {
        removeErrorOfField(getContainerId(), getContainerIdIcon());
        hideMessagesPanel();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        executeOpMethods(RESETCONVEYABLECARTONLOADPAGE);
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        changeCartonLabel();
        this.getConfirmCancelSessionPopup().hide();
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
        log.info(actionEvent.getSource() + "event triggered from");
    }

    public void noConfirmCancelSession(ActionEvent actionEvent) {
        this.getConfirmCancelSessionPopup().hide();
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
        log.info(actionEvent.getSource() + "event triggered from");
    }

    public void setScanUserBadgeTable(RichTable scanUserBadgeTable) {
        this.scanUserBadgeTable = scanUserBadgeTable;
    }

    public RichTable getScanUserBadgeTable() {
        return scanUserBadgeTable;
    }

    public void performKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            String item = (String) clientEvent.getParameters().get(ITEM);
            String submittedValue = (String) clientEvent.getParameters().get(SUBMITTEDVALUE);
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5TeamLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Refresh());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF9CancelLink());
                actionEvent.queue();
            } else if (F1_KEY_CODE.equals(keyPressed)) {
                if (getF2UserAtDoorLink() != null) {
                    if (getF2UserAtDoorLink().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF1UserAtDoorLink());
                        actionEvent.queue();
                    }
                }
                if (getF2ConfirmDoorLink() != null) {
                    if (getF2ConfirmDoorLink().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF1ConfirmDoorLink());
                        actionEvent.queue();
                    }
                }
                if (getF2ConfirmEndSession() != null) {
                    if (getF2ConfirmEndSession().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF1ConfirmEndSession());
                        actionEvent.queue();
                    }
                }
                if (getF2ConfirmCancel() != null) {
                    if (getF2ConfirmCancel().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF1ConfirmCancel());
                        actionEvent.queue();
                    }
                }
                if(getF2ConfirmLastcarton()!=null){
                    if(getF2ConfirmLastcarton().getId().equalsIgnoreCase(item)){
                        ActionEvent actionEvent = new ActionEvent(this.getF1Confirmlastcarton());
                        actionEvent.queue();
                    }
                }
                if(getF1RemainingUserPopupLink()!=null){
                    if(getF1RemainingUserPopupLink().getId().equalsIgnoreCase(item)){
                        ActionEvent actionEvent = new ActionEvent(this.getF1RemainingUserPopupLink());
                        actionEvent.queue();
                    }
                }
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                if (getF2UserAtDoorLink() != null) {
                    if (getF2UserAtDoorLink().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF2UserAtDoorLink());
                        actionEvent.queue();
                    }
                }
                if (getF2ConfirmDoorLink() != null) {
                    if (getF2ConfirmDoorLink().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF2ConfirmDoorLink());
                        actionEvent.queue();
                    }
                }
                if (getF2ConfirmEndSession() != null) {
                    if (getF2ConfirmEndSession().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF2ConfirmEndSession());
                        actionEvent.queue();
                    }
                }
                if (getF2ConfirmCancel() != null) {
                    if (getF2ConfirmCancel().getId().equalsIgnoreCase(item)) {
                        ActionEvent actionEvent = new ActionEvent(this.getF2ConfirmCancel());
                        actionEvent.queue();
                    }
                }
                if(getF2ConfirmLastcarton()!=null){
                    if(getF2ConfirmLastcarton().getId().equalsIgnoreCase(item)){
                        ActionEvent actionEvent = new ActionEvent(this.getF2ConfirmLastcarton());
                        actionEvent.queue();
                    }
                }
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (getContainerId().getId().equalsIgnoreCase(item) && submittedValue != null &&
                    !submittedValue.isEmpty()) {
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                } else if (getContainerId().getId().equalsIgnoreCase(item) &&
                           (submittedValue == null || submittedValue.isEmpty())) {
                    resetConveyableCartonPage(PARTIAL_ENTRY_);
                }
            }
        }
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF5TeamLink(RichLink f5TeamLink) {
        this.f5TeamLink = f5TeamLink;
    }

    public RichLink getF5TeamLink() {
        return f5TeamLink;
    }

    public void setF6Refresh(RichLink f6Refresh) {
        this.f6Refresh = f6Refresh;
    }

    public RichLink getF6Refresh() {
        return f6Refresh;
    }

    public void setF9CancelLink(RichLink f9CancelLink) {
        this.f9CancelLink = f9CancelLink;
    }

    public RichLink getF9CancelLink() {
        return f9CancelLink;
    }

    public void performBadgeScanKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            String item = (String) clientEvent.getParameters().get(ITEM);
            String submittedValue = (String) clientEvent.getParameters().get(SUBMITTEDVALUE);
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3BadgeScanExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4BadgeScanDoneLink());
                actionEvent.queue();
            } else if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1BadgeScanYesLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2BadgeScanNoLink());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (getScanUserId().getId().equalsIgnoreCase(item) && submittedValue != null &&
                    !submittedValue.isEmpty()) {
                    this.scanUserId(new ValueChangeEvent(this.getScanUserId(), null, submittedValue));
                } else if ((getScanUserId().getId().equalsIgnoreCase(item) || null == item) &&
                           (submittedValue == null || submittedValue.isEmpty())) {
                    getScanUserIdIcon().setName(ERROR);
                    refreshContentOfUIComponent(this.getScanUserIdIcon());
                    addErrorStyleToComponent(this.getScanUserId());
                    executeOpMethods(CREATEINSERTCONVEYABLECARTONTEAMROW);
                    refreshContentOfUIComponent(this.getScanUserId());
                    showMessagesPanel(E, this.getMessage(PARTIAL_ENTRY_, E, null, this.getLangCodeAttrValue()));
                }
            }
        }
    }

    public void setF3BadgeScanExitLink(RichLink f3BadgeScanExitLink) {
        this.f3BadgeScanExitLink = f3BadgeScanExitLink;
    }

    public RichLink getF3BadgeScanExitLink() {
        return f3BadgeScanExitLink;
    }

    public void setF4BadgeScanDoneLink(RichLink f4BadgeScanDoneLink) {
        this.f4BadgeScanDoneLink = f4BadgeScanDoneLink;
    }

    public RichLink getF4BadgeScanDoneLink() {
        return f4BadgeScanDoneLink;
    }

    public void setF1BadgeScanYesLink(RichLink f1BadgeScanYesLink) {
        this.f1BadgeScanYesLink = f1BadgeScanYesLink;
    }

    public RichLink getF1BadgeScanYesLink() {
        return f1BadgeScanYesLink;
    }

    public void setF2BadgeScanNoLink(RichLink f2BadgeScanNoLink) {
        this.f2BadgeScanNoLink = f2BadgeScanNoLink;
    }

    public RichLink getF2BadgeScanNoLink() {
        return f2BadgeScanNoLink;
    }

    public void performSupervisorKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            String currentFocus = this.getHhConveyorCartonLoadBean().getIsFocusOn();
            String item = (String) clientEvent.getParameters().get(ITEM);
            String submittedValue = (String) clientEvent.getParameters().get(SUBMITTEDVALUE);
            if (F3_KEY_CODE.equals(keyPressed)) {
                if (SVPE.equalsIgnoreCase(item)) {
                    ActionEvent actionEvent = new ActionEvent(this.getF3SupsViewExitLink());
                    actionEvent.queue();
                } else {
                    ActionEvent actionEvent = new ActionEvent(this.getF3SupsExitLink());
                    actionEvent.queue();
                }
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4SupsDoneLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2SupsViewLink());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (getSupervisorUserId().getId().equals(currentFocus)) {
                    this.onChangeSupervisorPassowrd(new ValueChangeEvent(this.getSupervisorUserId(), null,
                                                                         submittedValue));
                    getHhConveyorCartonLoadBean().setIsFocusOn(getSupervisorPassword().getId());
                } else {
                    if (submittedValue != null && !submittedValue.isEmpty()) {
                        this.onChangeSupervisorPassword(new ValueChangeEvent(this.getSupervisorPassword(), null,
                                                                             submittedValue));
                    } else {
                        getHhConveyorCartonLoadBean().setIsFocusOn(getSupervisorUserId().getId());
                        setFocusOnUIComponent(this.getSupervisorUserId());
                        refreshContentOfUIComponent(getSupervisorUserId());
                        refreshContentOfUIComponent(getSupervisorPassword());
                        //                        showMessagesPanel("E",
                        //                                          this.getMessage("PARTIAL_ENTRY", "E", null, this.getLangCodeAttrValue()));
                    }
                }
            }
        }
    }

    public void performSupervisorIdUserKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            String item = (String) clientEvent.getParameters().get(ITEM);
            String submittedValue = (String) clientEvent.getParameters().get(SUBMITTEDVALUE);
            if (F3_KEY_CODE.equals(keyPressed)) {
                if (SVPE.equalsIgnoreCase(item)) {
                    ActionEvent actionEvent = new ActionEvent(this.getF3SupsViewExitLink());
                    actionEvent.queue();
                } else {
                    ActionEvent actionEvent = new ActionEvent(this.getF3SupsExitLink());
                    actionEvent.queue();
                }
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4SupsDoneLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2SupsViewLink());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (submittedValue != null && !submittedValue.isEmpty()) {
                    this.onChangeSupervisorPassowrd(new ValueChangeEvent(this.getSupervisorPassword(), null,
                                                                         submittedValue));
                    getHhConveyorCartonLoadBean().setIsFocusOn(getSupervisorPassword().getId());
                    refreshContentOfUIComponent(this.getSupervisorUserId());
                } else {
                    showMessagesPanel(E, this.getMessage(PARTIAL_ENTRY_, E, null, this.getLangCodeAttrValue()));
                }
            }
        }
    }

    public void setF2SupsViewLink(RichLink f2SupsViewLink) {
        this.f2SupsViewLink = f2SupsViewLink;
    }

    public RichLink getF2SupsViewLink() {
        return f2SupsViewLink;
    }

    public void setF3SupsExitLink(RichLink f3SupsExitLink) {
        this.f3SupsExitLink = f3SupsExitLink;
    }

    public RichLink getF3SupsExitLink() {
        return f3SupsExitLink;
    }

    public void setF4SupsDoneLink(RichLink f4SupsDoneLink) {
        this.f4SupsDoneLink = f4SupsDoneLink;
    }

    public RichLink getF4SupsDoneLink() {
        return f4SupsDoneLink;
    }

    public void setSupervisorPassword(RichInputText supervisorPassword) {
        this.supervisorPassword = supervisorPassword;
    }

    public RichInputText getSupervisorPassword() {
        return supervisorPassword;
    }

    public void setF1UserAtDoorLink(RichLink f1UserAtDoorLink) {
        this.f1UserAtDoorLink = f1UserAtDoorLink;
    }

    public RichLink getF1UserAtDoorLink() {
        return f1UserAtDoorLink;
    }

    public void setF2UserAtDoorLink(RichLink f2UserAtDoorLink) {
        this.f2UserAtDoorLink = f2UserAtDoorLink;
    }

    public RichLink getF2UserAtDoorLink() {
        return f2UserAtDoorLink;
    }

    public void setF1ConfirmDoorLink(RichLink f1ConfirmDoorLink) {
        this.f1ConfirmDoorLink = f1ConfirmDoorLink;
    }

    public RichLink getF1ConfirmDoorLink() {
        return f1ConfirmDoorLink;
    }

    public void setF2ConfirmDoorLink(RichLink f2ConfirmDoorLink) {
        this.f2ConfirmDoorLink = f2ConfirmDoorLink;
    }

    public RichLink getF2ConfirmDoorLink() {
        return f2ConfirmDoorLink;
    }

    public void setF1ConfirmCancel(RichLink f1ConfirmCancel) {
        this.f1ConfirmCancel = f1ConfirmCancel;
    }

    public RichLink getF1ConfirmCancel() {
        return f1ConfirmCancel;
    }

    public void setF2ConfirmCancel(RichLink f2ConfirmCancel) {
        this.f2ConfirmCancel = f2ConfirmCancel;
    }

    public RichLink getF2ConfirmCancel() {
        return f2ConfirmCancel;
    }

    public void setF1ConfirmEndSession(RichLink f1ConfirmEndSession) {
        this.f1ConfirmEndSession = f1ConfirmEndSession;
    }

    public RichLink getF1ConfirmEndSession() {
        return f1ConfirmEndSession;
    }

    public void setF2ConfirmEndSession(RichLink f2ConfirmEndSession) {
        this.f2ConfirmEndSession = f2ConfirmEndSession;
    }

    public RichLink getF2ConfirmEndSession() {
        return f2ConfirmEndSession;
    }

    public void setF3SupsViewExitLink(RichLink f3SupsViewExitLink) {
        this.f3SupsViewExitLink = f3SupsViewExitLink;
    }

    public RichLink getF3SupsViewExitLink() {
        return f3SupsViewExitLink;
    }

    public void setIconPanelMessage(RichPanelGroupLayout iconPanelMessage) {
        this.iconPanelMessage = iconPanelMessage;
    }

    public RichPanelGroupLayout getIconPanelMessage() {
        return iconPanelMessage;
    }

    public void setSupervisorIconMessagePanel(RichPanelGroupLayout supervisorIconMessagePanel) {
        this.supervisorIconMessagePanel = supervisorIconMessagePanel;
    }

    public RichPanelGroupLayout getSupervisorIconMessagePanel() {
        return supervisorIconMessagePanel;
    }

    private HhConveyorCartonLoadBean getHhConveyorCartonLoadBean() {
        return ((HhConveyorCartonLoadBean) this.getPageFlowBean(HHCONVEYORCARTONLOADBEAN));
    }

    public void setUserName(RichPanelLabelAndMessage userName) {
        this.userName = userName;
    }

    public RichPanelLabelAndMessage getUserName() {
        return userName;
    }

    @SuppressWarnings("unchecked")
    public void onChangeSupervisorPassowrd(ValueChangeEvent valueChangeEvent) {
        hideMessagesPanel();
        log.info("Start of onChangeSupervisorPassword Method");
        this.updateModel(valueChangeEvent);
        if (valueChangeEvent.getNewValue() != null) {
            if(isPageDefAvailable()){

            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(VALIDATESUPERVISORLOGIN);
            oper.getParamsMap().put(USERNAME, valueChangeEvent.getNewValue().toString());
            oper.getParamsMap().put(PASSWORD_1, EMPTY);
            oper.execute();
            setFocusOnUIComponent(this.getSupervisorPassword());
            
            }
        }
    }
    
    public Boolean getConveyorCartonPanelVisible(){
        return "YES".equals(getHhConveyorCartonLoadBean().getIsConveyorCatornPanelVisible());

    }
    
    public Boolean getScanPanelVisible(){
        return "YES".equals(getHhConveyorCartonLoadBean().getIsScanPanelVisible());
    }
    
    public Boolean getSupervisorPanelVisible(){
        return "YES".equals(getHhConveyorCartonLoadBean().getIsSuperVisoranelVisible());
    }

    public Boolean getDisabledSupervisor() {
        return !getSupervisorUserId().getId().equals(getHhConveyorCartonLoadBean().getIsFocusOn());
    }

    public Boolean getDisabledPassword() {
        return !getSupervisorPassword().getId().equals(getHhConveyorCartonLoadBean().getIsFocusOn());
    }

    public void setUserAuthFocus(String userAuthFocus) {
        this.userAuthFocus = userAuthFocus;
    }

    public String getUserAuthFocus() {
        return userAuthFocus;
    }

    public void setSupervisorUserId(RichInputText supervisorUserId) {
        this.supervisorUserId = supervisorUserId;
    }

    public RichInputText getSupervisorUserId() {
        return supervisorUserId;
    }


    public void setConveyorCartonMesagePanel(RichPanelGroupLayout conveyorCartonMesagePanel) {
        this.conveyorCartonMesagePanel = conveyorCartonMesagePanel;
    }

    public RichPanelGroupLayout getConveyorCartonMesagePanel() {
        return conveyorCartonMesagePanel;
    }

    public void setSupViewButton(RichButton supViewButton) {
        this.writeJavaScriptToClient("SetFocusOnUIcomp(customFindElementById('supview'));");
        this.supViewButton = supViewButton;
    }

    public RichButton getSupViewButton() {
        return supViewButton;
    }

    public void setSupViewTableBinding(RichTable supViewTableBinding) {
        this.supViewTableBinding = supViewTableBinding;
    }

    public RichTable getSupViewTableBinding() {
        return supViewTableBinding;
    }

    public void setLastCartonConfirmPopup(RichPopup lastCartonConfirmPopup) {
        this.lastCartonConfirmPopup = lastCartonConfirmPopup;
    }

    public RichPopup getLastCartonConfirmPopup() {
        return lastCartonConfirmPopup;
    }

    public void setF1Confirmlastcarton(RichLink f1Confirmlastcarton) {
        this.f1Confirmlastcarton = f1Confirmlastcarton;
    }

    public RichLink getF1Confirmlastcarton() {
        return f1Confirmlastcarton;
    }

    public void setF2ConfirmLastcarton(RichLink f2ConfirmLastcarton) {
        this.f2ConfirmLastcarton = f2ConfirmLastcarton;
    }

    public RichLink getF2ConfirmLastcarton() {
        return f2ConfirmLastcarton;
    }

    public void yesConfirmLastCartonAction(ActionEvent actionEvent) {
        this.getLastCartonConfirmPopup().hide();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CONFIRMASSIGNLASTCARTON);
        String numUser = (String) oper.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(NUM_USERS_, numUser);
        if(Integer.valueOf(numUser)>0){
            this.getRemainingUsersTextBiding().setValue(numUser+" "+this.getMessage("REM_USERS").toString());
            this.getRemainingUsersConfirmPopup().show(new RichPopup.PopupHints());
            
        }
        else{
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(NUM_USERS_, numUser);
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        changeCartonLabel();
        resetConveyableCartonPage(EMPTY);
            if (this.getContainerId() != null) {
                this.setFocusOnUIComponent(this.getContainerId());
            }
        }
    }

    public void noConfirmLastCarton(ActionEvent actionEvent) {
        this.getLastCartonConfirmPopup().hide();
        resetConveyableCartonPage(EMPTY);
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
    }

    public void setRemUsersPopup(RichPopup remUsersPopup) {
        this.remUsersPopup = remUsersPopup;
    }

    public RichPopup getRemUsersPopup() {
        return remUsersPopup;
    }

    public void setF1RemUsersPopup(RichLink f1RemUsersPopup) {
        this.f1RemUsersPopup = f1RemUsersPopup;
    }

    public RichLink getF1RemUsersPopup() {
        return f1RemUsersPopup;
    }

    public void okRemUsersPopupAction(ActionEvent actionEvent) {
        this.getRemUsersPopup().hide();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        changeCartonLabel();
        resetConveyableCartonPage(EMPTY);
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
    }

    public String remainingUsersConfirmAction() {
        this.getRemainingUsersConfirmPopup().hide();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(ACTION_TYPE_, SCAN_OUT_);
        executeOpMethods(CREATEINSERTCONVEYABLECARTONLOADROW);
        executeOpMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
        changeCartonLabel();
        resetConveyableCartonPage(EMPTY);
        if (this.getContainerId() != null) {
            this.setFocusOnUIComponent(this.getContainerId());
        }
        return null;
    }
    
    protected String getMessage(String messageCode) {
        return this.getMessage(messageCode, "W", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                               (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
    }

    public void setRemainingUsersConfirmPopup(RichPopup remainingUsersConfirmPopup) {
        this.remainingUsersConfirmPopup = remainingUsersConfirmPopup;
    }

    public RichPopup getRemainingUsersConfirmPopup() {
        return remainingUsersConfirmPopup;
    }

    public void setF1RemainingUserPopupLink(RichLink f1RemainingUserPopupLink) {
        this.f1RemainingUserPopupLink = f1RemainingUserPopupLink;
    }

    public RichLink getF1RemainingUserPopupLink() {
        return f1RemainingUserPopupLink;
    }

    public void setRemainingUserPopupText(String remainingUserPopupText) {
        this.remainingUserPopupText = remainingUserPopupText;
    }

    public String getRemainingUserPopupText() {
        return remainingUserPopupText;
    }

    public void setRemainingUsersTextBiding(RichOutputText remainingUsersTextBiding) {
        this.remainingUsersTextBiding = remainingUsersTextBiding;
    }

    public RichOutputText getRemainingUsersTextBiding() {
        return remainingUsersTextBiding;
    }
}
