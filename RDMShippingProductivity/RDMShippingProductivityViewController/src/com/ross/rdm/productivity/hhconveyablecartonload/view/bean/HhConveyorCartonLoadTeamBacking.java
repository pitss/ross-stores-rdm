package com.ross.rdm.productivity.hhconveyablecartonload.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.productivity.hhconveyablecartonload.model.views.HhConveyableCartonTeamViewRowImpl;
import com.ross.rdm.productivity.hhconveyablecartonload.model.views.HhConveyableCartonUserListViewRowImpl;
import com.ross.rdm.productivity.hhconveyablecartonload.view.framework.RDMShippingBackingBean;

import java.io.Serializable;

import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

public class HhConveyorCartonLoadTeamBacking extends RDMShippingBackingBean implements Serializable {
    private static ADFLogger log = ADFLogger.createADFLogger(HhConveyorCartonLoadTeamBacking.class);
    private RichInputText teamId;
    private RichIcon teamIdIcon;
    private RichInputText userId;
    private RichIcon userIdIcon;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private final static String LANGUAGE_CODE_ATTR = "AM";
    private RichPopup f3TeamExitPopup;
    private RichPopup f7TeamDeletePopup;
    private RichLink f3TeamExitBinding;
    private RichLink f7TeamDeleteBinding;
    private RichLink f1TeamDeleteYesBinding;
    private RichLink f1TeamDeleteNoBinding;
    private RichLink f1TeamExitBinding;
    private RichLink f2TeamExitBinding;
    private RichLink f3DeleteTeamExitBinding;
    private RichLink f7DeleteTeamButoon;
    private RichLink f7DeleteTeamButton;
    private RichPanelGroupLayout iconPanelMessage;
    private RichTable deleteTeamTableBinding;
    private static final String E = "E";
    private static final String HHCONVEYORCARTONLOAD = "hhconveyorcartonload";
    private static final String ERROR = "error";
    private static final String KEYPRESSED = "keyPressed";
    private static final String INV_USER_ID_ = "INV_USER_ID";
    private static final String TEAMCOUNT = "TeamCount";
    private static final String _SETFOCUSONUICOMP_CUSTOMFINDELEMENTBYIDDELTAB_ =
        " SetFocusOnUIcomp(customFindElementById('delTab'));";
    private static final String HHCONVEYABLECARTONTEAMVIEWITERATOR = "HhConveyableCartonTeamViewIterator";
    private static final String HHCONVEYABLECARTONUSERLISTVIEWITERATOR = "HhConveyableCartonUserListViewIterator";
    private static final String STATUS = "Status";
    private static final String GETUSEROPENTRANSCATION = "getUserOpenTranscation";
    private static final String DELETEUSER_0 = "deleteUSer";
    private static final String SUBMITTEDVALUE = "submittedValue";
    private static final String ITEM = "item";
    private static final String PUSERID = "pUserId";
    private static final String BACKTOTEAMDETAILS = "backToTeamDetails";
    private static final String DELETEUSER = "deleteUser";
    private static final String ADDNEWUSER = "addNewUser";
    private static final String ERRORCODE = "ErrorCode";
    private static final String PARTIAL_ENTRY_ = "PARTIAL_ENTRY";
    private static final String EMPTY = "";
    private static final String CREATEINSERTCONVEYABLECARTONROW = "CreateInsertConveyableCartonRow";
    private static final String SETCONVEYORCARTONLOADWORKVARIABLES = "setConveyorCartonLoadWorkVariables";
    private static final String REFRESHACTION = "refreshAction";
    private RichPanelGroupLayout deleteTablePgLayout;
    private RichOutputText teamUserIdBinding;
    private RichButton testButtonBinding;
    private RichLink teamUserId;
    private RichTable addTeamTableBinding;
    private RichPanelGroupLayout teamMessagePanel;

    public String f3ExitAction() {
        if (log.isInfo()) {
            log.info("Start of f3ExitAction Method");
        }
        String teamCount = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(TEAMCOUNT);
        if (teamCount != null) {
            int teamMembers = Integer.parseInt(teamCount);
            if (teamMembers < 2) {
                this.getF3TeamExitPopup().show(new RichPopup.PopupHints());
            } else {
                getHhConveyorCartonLoadBean().setIsTeamPanelVisible("NO");
                getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
                executeOperationMethods(CREATEINSERTCONVEYABLECARTONROW);
                executeOperationMethods(SETCONVEYORCARTONLOADWORKVARIABLES);
                ADFUtils.invokeAction(HHCONVEYORCARTONLOAD);
            }
        } else {
            this.getF3TeamExitPopup().show(new RichPopup.PopupHints());
        }
        if (log.isInfo()) {
            log.info("End of f3ExitAction Method");
        }
        return null;
    }

    public String f7DeleteAction() {
        if (log.isInfo()) {
            log.info("Start of f7DeleteAction Method");
        }
        HhConveyableCartonUserListViewRowImpl hhConveyableCartonUserListViewRowImpl =
            this.getConveyableCartonUserListRow();
        if (null != hhConveyableCartonUserListViewRowImpl) {
            String userId = hhConveyableCartonUserListViewRowImpl.getUserId();
            Map<String, String> result = executeOpMethods(DELETEUSER, userId);
        }
        if (this.getF7TeamDeletePopup() != null) {
            this.getF7TeamDeletePopup().hide();
            if (ADFUtils.findIterator(HHCONVEYABLECARTONUSERLISTVIEWITERATOR).getEstimatedRowCount() < 1) {
                StringBuilder script = new StringBuilder("");
                script.append("setFocusonExitLink();");
                this.writeJavaScriptToClient(script.toString());
            } else {
                StringBuilder script = new StringBuilder("");
                script.append("initFocusDeleteTable();");
                this.writeJavaScriptToClient(script.toString());
            }
        }
        if (null != this.getDeleteTeamTableBinding()) {
            refreshContentOfUIComponent(this.getDeleteTeamTableBinding());
        }
        if (log.isInfo()) {
            log.info("End of f7DeleteAction Method");
        }
        return null;
    }

    private HhConveyableCartonUserListViewRowImpl getConveyableCartonUserListRow() {
        if (log.isInfo()) {
            log.info("Start of getConveyableCartonUserListRow Method");
        }
        if (log.isInfo()) {
            log.info("End of getConveyableCartonUserListRow Method");
        }
        return (HhConveyableCartonUserListViewRowImpl) ADFUtils.findIterator(HHCONVEYABLECARTONUSERLISTVIEWITERATOR).getCurrentRow();
    }

    public void onAddNewUser(ValueChangeEvent valueChangeEvent) {
        if (log.isInfo()) {
            log.info("Start of onAddNewUser Method");
        }
        removeErrorOfField(getUserId(), getUserIdIcon());
        hideMessagesPanel();
        String userId = (String) valueChangeEvent.getNewValue();
        if (null != userId && !EMPTY.equals(userId)) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GETUSEROPENTRANSCATION);
            Map<String, String> result = (Map<String, String>) oper.execute();
            if (result.size() > 0) {
                if (null != result.get(ERRORCODE)) {
                    String code = result.get(ERRORCODE);
                    resetTeamDetailsPage(code);
                }
            } else {
                executeOpMethods(ADDNEWUSER, userId);
            }
        } else {
            resetTeamDetailsPage(INV_USER_ID_);
        }
        if (null != this.getAddTeamTableBinding()) {
            refreshContentOfUIComponent(this.getAddTeamTableBinding());
        }
        if (log.isInfo()) {
            log.info("End of onAddNewUser Method");
        }
    }

    public Map<String, String> executeOpMethods(String methodName, String userId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(methodName);
        oper.getParamsMap().put(PUSERID, userId);
        Map<String, String> result = (Map<String, String>) oper.execute();
        String TeamCount = result.get(TEAMCOUNT);
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(TEAMCOUNT, TeamCount);
        if (null != result.get(STATUS)) {
            String code = result.get(ERRORCODE);
            resetTeamDetailsPage(code);
        } else if (!DELETEUSER.equalsIgnoreCase(methodName)) {
            resetTeamDetailsPage(EMPTY);
        }

        return result;
    }

    public void resetTeamDetailsPage(String errorCode) {
        if (EMPTY.equalsIgnoreCase(errorCode)) {
            HhConveyableCartonTeamViewRowImpl hhConveyableCartonRow = this.getConveyableCartonTeamRow();
            if (hhConveyableCartonRow != null) {
                hhConveyableCartonRow.setUserId(null);
                if (getUserId() != null) {
                    getUserId().setValue(null);
                    getUserId().resetValue();
                    getUserIdIcon().setName(EMPTY);
                    removeErrorOfField(getUserId(), getUserIdIcon());
                }
            }
            hideMessagesPanel();
        } else {
            if (getUserId() != null) {
                getUserIdIcon().setName(ERROR);
                addErrorStyleToComponent(this.getUserId());
            }
            showMessagesPanel(E, this.getMessage(errorCode, E, null, this.getLangCodeAttrValue()));
        }
        if (getUserId() != null) {
            refreshContentOfUIComponent(this.getUserId());
        }
        if (getUserIdIcon() != null) {
            refreshContentOfUIComponent(this.getUserIdIcon());
        }
        refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private HhConveyableCartonTeamViewRowImpl getConveyableCartonTeamRow() {
        if (log.isInfo()) {
            log.info("Start of getConveyableCartonUserListRow Method");
        }
        if (log.isInfo()) {
            log.info("End of getConveyableCartonUserListRow Method");
        }
        return (HhConveyableCartonTeamViewRowImpl) ADFUtils.findIterator(HHCONVEYABLECARTONTEAMVIEWITERATOR).getCurrentRow();
    }

    public void setTeamId(RichInputText teamId) {
        this.teamId = teamId;
    }

    public RichInputText getTeamId() {
        return teamId;
    }

    public void setTeamIdIcon(RichIcon teamIdIcon) {
        this.teamIdIcon = teamIdIcon;
    }

    public RichIcon getTeamIdIcon() {
        return teamIdIcon;
    }

    public void setUserId(RichInputText userId) {
        this.userId = userId;
    }

    public RichInputText getUserId() {
        return userId;
    }

    public void setUserIdIcon(RichIcon userIdIcon) {
        this.userIdIcon = userIdIcon;
    }

    public RichIcon getUserIdIcon() {
        return userIdIcon;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    private String getLangCodeAttrValue() {
        log.info("Start of getLangCodeAttrValue Method");
        log.info("End of getLangCodeAttrValue Method");
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setF3TeamExitPopup(RichPopup f3TeamExitPopup) {
        this.f3TeamExitPopup = f3TeamExitPopup;
    }

    public RichPopup getF3TeamExitPopup() {
        return f3TeamExitPopup;
    }

    public void f1TeamExitConfirmYesAction(ActionEvent actionEvent) {
        f7DeleteAction();
        getHhConveyorCartonLoadBean().setIsTeamPanelVisible("NO");
        getHhConveyorCartonLoadBean().setIsConveyorCatornPanelVisible("YES");
        ADFUtils.invokeAction(HHCONVEYORCARTONLOAD);
    }

    public void f1TeamExitConfirmNoAction(ActionEvent actionEvent) {
        this.getF3TeamExitPopup().hide();
        this.setFocusOnUIComponent(this.getUserId());
    }

    public String f7DeleteConfirmNoAction() {
        this.getF7TeamDeletePopup().hide();
        this.writeJavaScriptToClient(_SETFOCUSONUICOMP_CUSTOMFINDELEMENTBYIDDELTAB_);
        return null;
    }

    public void setF7TeamDeletePopup(RichPopup f7TeamDeletePopup) {
        this.f7TeamDeletePopup = f7TeamDeletePopup;
    }

    public RichPopup getF7TeamDeletePopup() {
        return f7TeamDeletePopup;
    }

    public void performTeamKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            String item = (String) clientEvent.getParameters().get(ITEM);
            String submittedValue = (String) clientEvent.getParameters().get(SUBMITTEDVALUE);
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3TeamExitBinding());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7TeamDeleteBinding());
                actionEvent.queue();
            } else if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1TeamDeleteYesBinding());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1TeamDeleteNoBinding());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (submittedValue != null && !submittedValue.isEmpty()) {
                    this.onAddNewUser(new ValueChangeEvent(this.getUserId(), null, submittedValue));
                } else {
                    resetTeamDetailsPage(PARTIAL_ENTRY_);
                }
            }
        }
    }

    public void performTeamDeleteKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3DeleteTeamExitBinding());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7DeleteTeamButoon());
                actionEvent.queue();
            } else if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1TeamDeleteYesBinding());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1TeamDeleteNoBinding());
                actionEvent.queue();
            }
        }
    }

    public void setF3TeamExitBinding(RichLink f3TeamExitBinding) {
        this.f3TeamExitBinding = f3TeamExitBinding;
    }

    public RichLink getF3TeamExitBinding() {
        return f3TeamExitBinding;
    }

    public void setF7TeamDeleteBinding(RichLink f7TeamDeleteBinding) {
        this.f7TeamDeleteBinding = f7TeamDeleteBinding;
    }

    public RichLink getF7TeamDeleteBinding() {
        return f7TeamDeleteBinding;
    }

    public String executeKeyPrivilege(String keyName) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("checkKeyPrivilege");
        oper.getParamsMap().put("optionName", keyName);
        String result = (String) oper.execute();
        return result;
    }

    public void f7TeamDeleteAction(ActionEvent actionEvent) {
        String keyPrivilege = executeKeyPrivilege("F7");
        if (!("OFF".equalsIgnoreCase(keyPrivilege))) {
            hideMessagesPanel();

            OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GETUSEROPENTRANSCATION);
            Map<String, String> result = (Map<String, String>) oper.execute();
            if (result.size() > 0) {
                if (null != result.get(ERRORCODE)) {
                    String code = result.get(ERRORCODE);
                    resetTeamDetailsPage(code);
                }
            } else if (ADFUtils.findIterator(HHCONVEYABLECARTONUSERLISTVIEWITERATOR).getEstimatedRowCount() < 1) {
                resetTeamDetailsPage("CANT_DLT_NE_REC");
            } else {
                getHhConveyorCartonLoadBean().setIsDeleteTeamPanelVisible("YES");
                getHhConveyorCartonLoadBean().setIsTeamPanelVisible("NO");
                ADFUtils.invokeAction(DELETEUSER_0);
            }
        } else {
            resetTeamDetailsPage("NOT_ALLOWED");

        }

    }

    public void setF1TeamDeleteYesBinding(RichLink f1TeamDeleteYesBinding) {
        this.f1TeamDeleteYesBinding = f1TeamDeleteYesBinding;
    }

    public RichLink getF1TeamDeleteYesBinding() {
        return f1TeamDeleteYesBinding;
    }

    public void setF1TeamDeleteNoBinding(RichLink f1TeamDeleteNoBinding) {
        this.f1TeamDeleteNoBinding = f1TeamDeleteNoBinding;
    }

    public RichLink getF1TeamDeleteNoBinding() {
        return f1TeamDeleteNoBinding;
    }

    public void setF1TeamExitBinding(RichLink f1TeamExitBinding) {
        this.f1TeamExitBinding = f1TeamExitBinding;
    }

    public RichLink getF1TeamExitBinding() {
        return f1TeamExitBinding;
    }

    public void setF2TeamExitBinding(RichLink f2TeamExitBinding) {
        this.f2TeamExitBinding = f2TeamExitBinding;
    }

    public RichLink getF2TeamExitBinding() {
        return f2TeamExitBinding;
    }

    public void executeOperationMethods(String opName) {
        log.info("Start of executeOpMethods Method- Genric method to call opBinding");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(opName);
        if (SETCONVEYORCARTONLOADWORKVARIABLES.equalsIgnoreCase(opName)) {
            oper.getParamsMap().put(REFRESHACTION, false);
        }
        oper.execute();
        log.info("End of executeOpMethods Method");
    }

    public void performTeamExitKey(ClientEvent clientEvent) {
        log.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEYPRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEYPRESSED);
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1TeamExitBinding());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2TeamExitBinding());
                actionEvent.queue();
            }
        }
    }
    private HhConveyorCartonLoadBean getHhConveyorCartonLoadBean() {
        return ((HhConveyorCartonLoadBean) this.getPageFlowBean("HhConveyorCartonLoadBean"));
    }

    
    public Boolean getDeleteTeamPanelVisible(){
        return "YES".equals(getHhConveyorCartonLoadBean().getIsDeleteTeamPanelVisible());
    }
    
    public Boolean getTeamDetailsPanelVisible(){
        return "YES".equals(getHhConveyorCartonLoadBean().getIsTeamPanelVisible());
    }

    public void setF3DeleteTeamExitBinding(RichLink f3DeleteTeamExitBinding) {
        this.f3DeleteTeamExitBinding = f3DeleteTeamExitBinding;
    }

    public RichLink getF3DeleteTeamExitBinding() {
        return f3DeleteTeamExitBinding;
    }

    public String f3DeleteTeamExitAction() {
        getIconPanelMessage().setVisible(false);
        getHhConveyorCartonLoadBean().setIsDeleteTeamPanelVisible("NO");
        getHhConveyorCartonLoadBean().setIsTeamPanelVisible("YES");
        refreshContentOfUIComponent(this.getIconPanelMessage());
        hideMessagesPanel();
        ADFUtils.invokeAction(BACKTOTEAMDETAILS);
        if (getUserId() != null) {
            this.setFocusOnUIComponent(this.getUserId());
        }
        return null;
    }

    public void setF7DeleteTeamButoon(RichLink f7DeleteTeamButoon) {
        this.f7DeleteTeamButoon = f7DeleteTeamButoon;
    }

    public RichLink getF7DeleteTeamButoon() {
        return f7DeleteTeamButoon;
    }

    public void setF7DeleteTeamButton(RichLink f7DeleteTeamButton) {
        this.f7DeleteTeamButton = f7DeleteTeamButton;
    }

    public RichLink getF7DeleteTeamButton() {
        return f7DeleteTeamButton;
    }

    public void f7DeleteTeamButtonAction(ActionEvent actionEvent) {
        String teamCount = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(TEAMCOUNT);
        if (teamCount != null) {
            int teamMembers = Integer.parseInt(teamCount);
            if (teamMembers > 0) {
                this.getF7TeamDeletePopup().show(new RichPopup.PopupHints());
                if (this.getF1TeamDeleteNoBinding() != null) {
                    this.setFocusOnUIComponent(this.getF1TeamDeleteNoBinding());
                }
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        refreshContentOfUIComponent(this.getDeleteTeamTableBinding());
        this.writeJavaScriptToClient(_SETFOCUSONUICOMP_CUSTOMFINDELEMENTBYIDDELTAB_);
    }

    public void setIconPanelMessage(RichPanelGroupLayout iconPanelMessage) {
        this.iconPanelMessage = iconPanelMessage;
    }

    public RichPanelGroupLayout getIconPanelMessage() {
        return iconPanelMessage;
    }

    public void setDeleteTeamTableBinding(RichTable deleteTeamTableBinding) {
        this.deleteTeamTableBinding = deleteTeamTableBinding;
    }

    public RichTable getDeleteTeamTableBinding() {
        return deleteTeamTableBinding;
    }

    public void onTeamDetailsRegionLoad(ComponentSystemEvent componentSystemEvent) {
        refreshContentOfUIComponent(this.getUserId());
    }

    public void setDeleteTablePgLayout(RichPanelGroupLayout deleteTablePgLayout) {
        this.deleteTablePgLayout = deleteTablePgLayout;
    }

    public RichPanelGroupLayout getDeleteTablePgLayout() {
        return deleteTablePgLayout;
    }

    public void setTeamUserIdBinding(RichOutputText teamUserIdBinding) {
        this.writeJavaScriptToClient(" SetFocusOnUIcomp(customFindElementById('deltab'));");
        this.teamUserIdBinding = teamUserIdBinding;
    }

    public RichOutputText getTeamUserIdBinding() {
        return teamUserIdBinding;
    }

    public void setTestButtonBinding(RichButton testButtonBinding) {
        this.writeJavaScriptToClient(" SetFocusOnUIcomp(customFindElementById('deltab'));");
        this.testButtonBinding = testButtonBinding;
    }

    public RichButton getTestButtonBinding() {
        return testButtonBinding;
    }

    public void setTeamUserId(RichLink teamUserId) {
        this.teamUserId = teamUserId;
    }

    public RichLink getTeamUserId() {
        this.writeJavaScriptToClient(" SetFocusOnUIcomp(customFindElementById('deltab'));");
        return teamUserId;
    }

    public void setAddTeamTableBinding(RichTable addTeamTableBinding) {
        this.addTeamTableBinding = addTeamTableBinding;
    }

    public RichTable getAddTeamTableBinding() {
        return addTeamTableBinding;
    }

    public void setTeamMessagePanel(RichPanelGroupLayout teamMessagePanel) {
        this.teamMessagePanel = teamMessagePanel;
    }

    public RichPanelGroupLayout getTeamMessagePanel() {
        return teamMessagePanel;
    }
}
