package com.ross.rdm.productivity.hhconveyablecartonload.model.views;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;

import com.ross.rdm.productivity.hhconveyablecartonload.model.views.common.HhConveyableCartonWorkView;
import com.ross.rdm.productivity.hhconveyablecartonload.model.services.ProductivityAppModuleImpl;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;
import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Oct 18 16:07:40 PDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhConveyableCartonWorkViewImpl extends ViewObjectImpl implements HhConveyableCartonWorkView {
    private static ADFLogger log = ADFLogger.createADFLogger(HhConveyableCartonWorkViewImpl.class);


    /**
     * This is the default constructor (do not remove).
     */
    public HhConveyableCartonWorkViewImpl() {

        log.info("Start of HhConveyableCartonWorkViewImpl Method");

        log.info("End of HhConveyableCartonWorkViewImpl Method");
    }

    public Boolean setConveyorCartonLoadWorkVariables(Boolean refreshAction) {

        log.info("Start of setConveyorCartonLoadWorkVariables Method");

        HhConveyableCartonWorkViewRowImpl ConveyorCutOffWorkVRowImpl =
            (HhConveyableCartonWorkViewRowImpl) this.createRow();
        ProductivityAppModuleImpl applicationModule = (ProductivityAppModuleImpl) this.getApplicationModule();
        GlobalVariablesViewRowImpl gVViewRowImpl =
            (GlobalVariablesViewRowImpl) applicationModule.getGlobalVariablesView().getCurrentRow();
        HhConveyableCartonLoadViewRowImpl conveyableCartonRow =
            (HhConveyableCartonLoadViewRowImpl) applicationModule.getHhConveyableCartonLoadView().getCurrentRow();
        String pFacilityId = gVViewRowImpl.getGlobalFacilityId();
        String pUser = gVViewRowImpl.getGlobalUserId();
        SQLOutParam pTrailer = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pDoor = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pVReturn = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pMsgDisplay = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pRoute = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pTeamIn = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pTeamId = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pNum_users = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pFirstcid = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pFirstcontd = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pLastcid = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pNewUserSharing = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pNextContainerId = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pActionType = new SQLOutParam("SCAN_IN", Types.VARCHAR);
        SQLOutParam pScannedUser = new SQLOutParam(conveyableCartonRow.getScannedUser(), Types.VARCHAR);
        SQLOutParam pUnscannedUSer = new SQLOutParam(conveyableCartonRow.getUnscannedUser(), Types.VARCHAR);
        SQLOutParam pCountRemianingUser = new SQLOutParam(null, Types.VARCHAR);
        String pUserToEnd = "ONE";
        String screen_name_in = "HH_CONVEYABLE_CARTON_LOAD_S";

        try {
            if (!(Boolean.TRUE.equals(refreshAction))) {
                ConveyorCutOffWorkVRowImpl.setUserName(gVViewRowImpl.getGlobalUserId());
                ConveyorCutOffWorkVRowImpl.setHeader(gVViewRowImpl.getGlobalHeader());
                ConveyorCutOffWorkVRowImpl.setFacilityId(gVViewRowImpl.getGlobalFacilityId());
                ConveyorCutOffWorkVRowImpl.setDevice(gVViewRowImpl.getGlobalDevice());
                ConveyorCutOffWorkVRowImpl.setMainBlock("CONVEYOR");
                ConveyorCutOffWorkVRowImpl.setLanguageCode(gVViewRowImpl.getGlobalLanguageCode());
                ConveyorCutOffWorkVRowImpl.setVersionNumber("%I%");
                String displayHeader =
                    applicationModule.callDisplayHeader(ConveyorCutOffWorkVRowImpl.getFacilityId(),
                                                        ConveyorCutOffWorkVRowImpl.getLanguageCode(),
                                                        "HH_CONVEYABLE_CARTON_LOAD_S");
                ConveyorCutOffWorkVRowImpl.setHeader(displayHeader);
                //ConveyorCutOffWorkVRowImpl.setHeader("Conveyable Carton Load");

                this.insertRow(ConveyorCutOffWorkVRowImpl);
                this.setCurrentRow(ConveyorCutOffWorkVRowImpl);

                applicationModule.callSetAhlInfo(ConveyorCutOffWorkVRowImpl.getFacilityId(),
                                                 "HH_CONVEYABLE_CARTON_LOAD_S");
                DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_COMMON_ADF.CHECK_USER_PRIVILEGE", pFacilityId,
                                            screen_name_in, pUser, null);

            }


            log.info("Call get_user_info to get user details for" + pUser);
            DBUtils.callStoredProcedure(this.getDBTransaction(), "CONVEYABLE_CARTON_LOAD.get_user_info", pFacilityId,
                                        pUser, pFirstcid, pDoor, pTrailer, pRoute, pTeamIn, pTeamId, pNum_users,
                                        pVReturn, pMsgDisplay);
            String doorVal = (String) pDoor.getWrappedData();

            String retVal =
                (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                    "CONVEYABLE_CARTON_LOAD.assign_last_cid", pFacilityId, pUser,
                                                    pUserToEnd, pScannedUser, pUnscannedUSer, pDoor, pFirstcontd,
                                                    pLastcid, pActionType, pCountRemianingUser, pNewUserSharing,
                                                    pNextContainerId, pVReturn, pMsgDisplay);


            if ("C".equalsIgnoreCase(retVal)) {
                if (pTeamIn.getWrappedData() != null) {
                    conveyableCartonRow.setTeamIndicator(pTeamIn.getWrappedData().toString());
                }
                if (pTeamId.getWrappedData() != null) {
                    conveyableCartonRow.setTeamId(pTeamId.getWrappedData().toString());
                    return false;
                }
            } else {
                if (pFirstcid.getWrappedData() != null) {
                    log.info(pUser + "has an open transaction at Door" + pDoor.getWrappedData());
                    conveyableCartonRow.setTrailer((String) pTrailer.getWrappedData());
                    conveyableCartonRow.setDoor(doorVal);
                    conveyableCartonRow.setFirstContainer((String) pFirstcid.getWrappedData());
                    conveyableCartonRow.setTeamIndicator((String) pTeamIn.getWrappedData());
                    conveyableCartonRow.setRoute((String) pRoute.getWrappedData());
                    if (pTeamId.getWrappedData() != null) {
                        conveyableCartonRow.setTeamId(pTeamId.getWrappedData().toString());
                    }
                    pFirstcid = new SQLOutParam(null, Types.VARCHAR);
                    conveyableCartonRow.setNumUsers(pNum_users.getWrappedData().toString());

                    log.info("End of setConveyorCartonLoadWorkVariables Method");
                    return true;
                } else {
                    if (pTeamIn.getWrappedData() != null) {
                        conveyableCartonRow.setTeamIndicator(pTeamIn.getWrappedData().toString());
                    }
                    if (pTeamId.getWrappedData() != null) {
                        conveyableCartonRow.setTeamId(pTeamId.getWrappedData().toString());
                        return false;
                    }
                }
            }
        } catch (Exception ex) {
            log.severe(ex.getMessage());
        }
        return false;
    }

    public String checkKeyPrivilege(String optionName) {
        ProductivityAppModuleImpl applicationModule = (ProductivityAppModuleImpl) this.getApplicationModule();
        GlobalVariablesViewRowImpl gVViewRowImpl =
            (GlobalVariablesViewRowImpl) applicationModule.getGlobalVariablesView().getCurrentRow();
        String pFacilityId = gVViewRowImpl.getGlobalFacilityId();
        String pUser = gVViewRowImpl.getGlobalUserId();
        String screen_name_in = "HH_CONVEYABLE_CARTON_LOAD_S";

        String retVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_COMMON_ADF.CHECK_SCREEN_OPTION_PRIV", pFacilityId, pUser,
                                                screen_name_in, optionName);
        return retVal;
    }
}

