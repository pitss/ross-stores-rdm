package com.ross.rdm.productivity.hhconveyablecartonload.model.views;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Oct 18 16:05:03 PDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhConveyableCartonTeamViewRowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        TeamId,
        UserId;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int TEAMID = AttributesEnum.TeamId.index();
    public static final int USERID = AttributesEnum.UserId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhConveyableCartonTeamViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute TeamId.
     * @return the TeamId
     */
    public String getTeamId() {
        return (String) getAttributeInternal(TEAMID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TeamId.
     * @param value value to set the  TeamId
     */
    public void setTeamId(String value) {
        setAttributeInternal(TEAMID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UserId.
     * @return the UserId
     */
    public String getUserId() {
        return (String) getAttributeInternal(USERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UserId.
     * @param value value to set the  UserId
     */
    public void setUserId(String value) {
        setAttributeInternal(USERID, value);
    }
}

