package com.ross.rdm.productivity.hhconveyablecartonload.model.views;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Oct 18 16:05:12 PDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhConveyableCartonUserListViewRowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        UserId,
        TeamId,
        FacilityId,
        UserName;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int USERID = AttributesEnum.UserId.index();
    public static final int TEAMID = AttributesEnum.TeamId.index();
    public static final int FACILITYID = AttributesEnum.FacilityId.index();
    public static final int USERNAME = AttributesEnum.UserName.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhConveyableCartonUserListViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute UserId.
     * @return the UserId
     */
    public String getUserId() {
        return (String) getAttributeInternal(USERID);
    }

    /**
     * Gets the attribute value for the calculated attribute TeamId.
     * @return the TeamId
     */
    public String getTeamId() {
        return (String) getAttributeInternal(TEAMID);
    }

    /**
     * Gets the attribute value for the calculated attribute FacilityId.
     * @return the FacilityId
     */
    public String getFacilityId() {
        return (String) getAttributeInternal(FACILITYID);
    }

    /**
     * Gets the attribute value for the calculated attribute UserName.
     * @return the UserName
     */
    public String getUserName() {
        return (String) getAttributeInternal(USERNAME);
    }
}

