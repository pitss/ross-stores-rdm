:: Building of all Jars Libraries.
ojdeploy -clean -buildfile C:\Project\Ross\SVN\RDM_R3\ojdeploy-build.xml
:: copy Clusters Jars Libraries inside the RDMCylinder\lib 
copy/Y RDMCommon\dist\ Cylinder\lib
copy/Y RDMHotelPicking\dist\ Cylinder\lib
copy/Y RDMCommonUtils\dist\ Cylinder\lib
copy/Y RDMIndependent\dist\ Cylinder\lib
copy/Y RDMInventoryManagement\dist\ Cylinder\lib
copy/Y RDMProcessing\dist\ Cylinder\lib
copy/Y RDMPTSPicking\dist\ Cylinder\lib
copy/Y RDMReceiving\dist\ Cylinder\lib
copy/Y RDMShipping\dist\ Cylinder\lib
copy/Y RDMStagerOptions\dist\ Cylinder\lib
copy/Y RDMTrailerManagement\dist\ Cylinder\lib 
copy/Y RDMShippingProductivity\dist\ Cylinder\lib 
:: copy RDMCommon Jars Libraries inside the each Cluster
copy/Y RDMCommon\dist\ RDMHotelPicking\lib
copy/Y RDMCommon\dist\ RDMIndependent\lib
copy/Y RDMCommon\dist\ RDMInventoryManagement\lib
copy/Y RDMCommon\dist\ RDMProcessing\lib
copy/Y RDMCommon\dist\ RDMPTSPicking\lib
copy/Y RDMCommon\dist\ RDMReceiving\lib
copy/Y RDMCommon\dist\ RDMShipping\lib
copy/Y RDMCommon\dist\ RDMStagerOptions\lib
copy/Y RDMCommon\dist\ RDMTrailerManagement\lib
copy/Y RDMCommon\dist\ RDMShippingProductivity\lib

:: copy RDMCommonUtils Jars Libraries inside the each Cluster
copy/Y RDMCommonUtils\dist\ RDMCommon\lib
copy/Y RDMCommonUtils\dist\ RDMHotelPicking\lib
copy/Y RDMCommonUtils\dist\ RDMIndependent\lib
copy/Y RDMCommonUtils\dist\ RDMInventoryManagement\lib
copy/Y RDMCommonUtils\dist\ RDMProcessing\lib
copy/Y RDMCommonUtils\dist\ RDMPTSPicking\lib
copy/Y RDMCommonUtils\dist\ RDMReceiving\lib
copy/Y RDMCommonUtils\dist\ RDMShipping\lib
copy/Y RDMCommonUtils\dist\ RDMStagerOptions\lib
copy/Y RDMCommonUtils\dist\ RDMTrailerManagement\lib
copy/Y RDMCommonUtils\dist\ RDMShippingProductivity\lib

copy/Y Cylinder\dist\ Cylinder\lib