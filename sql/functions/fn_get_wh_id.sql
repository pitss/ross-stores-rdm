CREATE OR REPLACE FUNCTION FN_GET_WH_ID (facility_id_in IN VARCHAR2,
                                         location_id_in IN VARCHAR2) RETURN VARCHAR2 IS
/*******************************************************************************
* Function: This function gets the WH_ID of an input location_id               *
* Change History:                                                              *
*                                                                              *
* Author          Date          CO#      Change Description                    *
********************************************************************************
* R.Macero      08/20/2009     58745     Initial Version.                      *
*******************************************************************************/
   t_wh_id   PAR3214.ZONE.WH_ID%TYPE;
BEGIN

   SELECT NVL(z.wh_id, '99') INTO t_wh_id
     FROM par3214.location l,
          par3214.zone z
    WHERE l.facility_id = facility_id_in
      AND l.facility_id = z.facility_id
      AND l.zone = z.zone
      AND l.location_id = location_id_in;

   RETURN t_wh_id;

EXCEPTION
   WHEN no_data_found THEN
      RETURN ('99');
END;
/