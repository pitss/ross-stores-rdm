--------------------------------------------------------
--  DDL for Type REC_REPORTS
--------------------------------------------------------

CREATE OR REPLACE TYPE "REC_REPORTS" 
IS
  OBJECT
  (
    t_bol_nbr NUMBER(9,0),
    t_queue VARCHAR2(20 BYTE),
    t_type VARCHAR2(100) );

--------------------------------------------------------
--  DDL for Type T_REPORT
--------------------------------------------------------
CREATE OR REPLACE TYPE "T_REPORT" AS TABLE OF REC_REPORTS;
