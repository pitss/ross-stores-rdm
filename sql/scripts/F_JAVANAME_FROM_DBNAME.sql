create or replace function f_javaname_from_dbname(p_string in varchar2) return varchar2 is    
      type t_table  is TABLE OF VARCHAR2(100);
      l_string       VARCHAR2(100) := p_String || '_';
      l_comma_index  PLS_INTEGER;
      l_index        PLS_INTEGER := 1;
      v_table          t_table := t_table();
      v_FinalString VARCHAR2(100);
   v_TabLen BINARY_INTEGER;
   v_returnstring varchar2(100);
       BEGIN
   if l_string like 'O\_%'  ESCAPE '\' 
      or l_string like 'I\_%'  ESCAPE '\'
      or l_string like 'P\_%'  ESCAPE '\'
   then
     l_string := substr(l_string, 3);
   end if;

      LOOP
       l_comma_index := INSTR(l_string, '_', l_index);
       EXIT WHEN l_comma_index = 0;
       v_table.EXTEND;
       v_table(v_table.COUNT) := SUBSTR(l_string, l_index, l_comma_index - l_index);
       l_index := l_comma_index + 1;
     END LOOP;
     FOR v_Count IN 1..v_Table.COUNT
  LOOP
   --DBMS_OUTPUT.PUT_LINE(v_Table(v_Count));
   v_returnstring := v_returnstring || initcap(v_table(v_count));
   --DBMS_OUTPUT.PUT_LINE(v_returnstring);
   END LOOP;
   return v_returnstring;
   END;
/