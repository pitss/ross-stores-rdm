CREATE TABLE "PAR3214"."FORMS_BLOCK_ITEMS" 
   (	"FORM_NAME" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"BLOCK_NAME" VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	"ITEM_NAME" VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "FORMS_BLOCK_ITEMS_PK" PRIMARY KEY ("FORM_NAME", "BLOCK_NAME", "ITEM_NAME");



/* Statement to create insert script from PITSS.com
select blks.form_name form_name,
       blks.name_object block_name,
       items.name_object item_name
  from all_obj blks,
       all_obj items
  where 1=1 
   --and blks.form_name = 'hh_container_pick_s'
   and blks.api_obj_id = 3
   and items.parent_id = blks.pk_object
   and items.api_obj_id = 13
   and blks.owner = 'PITSS18'; */

