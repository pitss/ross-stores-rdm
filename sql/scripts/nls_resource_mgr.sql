CREATE OR REPLACE PACKAGE nls_resource_mgr
IS
  FUNCTION get_resource_bundle(
      p_locale IN VARCHAR2)
    RETURN sys_refcursor;
  FUNCTION get_message_bundle(
      p_locale IN VARCHAR2)
    RETURN sys_refcursor ;
END nls_resource_mgr;
/


CREATE OR REPLACE PACKAGE BODY nls_resource_mgr
IS
FUNCTION get_resource_bundle(
    p_locale IN VARCHAR2)
  RETURN sys_refcursor
IS
  l_cur sys_refcursor;
BEGIN
  OPEN l_cur FOR SELECT data_base_value, display_value FROM translator WHERE language_code = p_locale;
  RETURN l_cur;
END;
FUNCTION get_message_bundle(
    p_locale IN VARCHAR2)
  RETURN sys_refcursor
IS
  l_cur sys_refcursor;
BEGIN
  OPEN l_cur FOR SELECT message_code, message_text FROM user_language_message WHERE language_code = p_locale;
  RETURN l_cur;
END;
END nls_resource_mgr;
/
