CREATE OR REPLACE PACKAGE PKG_PICKING
AS

  v_debug varchar2(2) := 'N';
  PROCEDURE v_from_container(
      V_RETURN               IN OUT VARCHAR2,
      Pick_From_Container_Id IN VARCHAR2,
      P_FACILITY_ID          IN VARCHAR2,
      From_Container_Id_In   IN VARCHAR2,
      Location_Id_In         IN VARCHAR2);
  --
  PROCEDURE GET_PICK_DIRECTIVE(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,    
    I_CONF_CONTAINER_ID        IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID         IN OUT VARCHAR2 ,
    I_CONTAINER_QTY            IN OUT NUMBER ,
    I_DESCRIPTION              IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID         IN OUT VARCHAR2 ,
    I_ITEM_ID                  IN OUT VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_REFERENCE_CODE           IN OUT VARCHAR2 ,
    I_BULK_GROUP               IN OUT VARCHAR2 ,
    I_CONTAINER_QTY2           IN OUT NUMBER ,
    I_DEST_ID                  IN OUT NUMBER ,
    I_DISTRO_NBR               IN OUT VARCHAR2 ,
    I_EMPTY_PRESSED            IN OUT VARCHAR2 ,
    I_FIRST_PICK_FLAG          IN OUT VARCHAR2 ,
    I_OLD_TO_CID               IN OUT VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN OUT NUMBER ,
    I_PICK_CONTAINER_QTY       IN OUT NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID01   IN OUT VARCHAR2 ,
    I_PICK_TYPE                IN OUT VARCHAR2 ,
    I_TO_LOC_ENTERED           IN OUT VARCHAR2 ,
    I_WAVE_NBR                 IN OUT NUMBER ,
    I_ZONE                     IN OUT VARCHAR2 ,  
    P_PM_NO_PICK_FLAG          IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG          IN OUT VARCHAR2 ,    
    I_Global_Intrlvg           IN OUT VARCHAR2,        
    I_GOTO_FIELD               IN OUT VARCHAR2,  
    I_Operations_Performed     IN NUMBER,    
    I_Start_Time               IN DATE,  
    I_Units_Processed          IN NUMBER,
    I_Containers_Processed     IN NUMBER,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_DISTRO_TS                IN DATE ,
    I_LABELED_RESERVE          IN VARCHAR2 ,    
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PROCESS_BY_AISLE         IN VARCHAR2 ,
    P_PM_PICK_TYPE             IN VARCHAR2 ,
    P_PM_START_LOCATION        IN VARCHAR2,
    P_PM_ALLOW_INTRLVG         IN VARCHAR2  );
  --
  PROCEDURE GET_GENERIC_PICK_DIRECTIVE(
    p_facility_id            IN VARCHAR2,
    P_PM_wave_date           IN DATE,
    p_PROCESS_BY_AISLE       IN VARCHAR2,
	p_PM_Pick_Type_C         IN VARCHAR2,
	p_PM_Pick_Type_CP        IN VARCHAR2,
    p_PM_Pick_Type           IN VARCHAR2,
    p_PM_START_LOCATION      IN VARCHAR2,
    p_PM_UPS_code            IN VARCHAR2,
    p_Labeled_Picking        IN VARCHAR2,
    p_USER                   IN VARCHAR2,
    P_PM_ALLOW_INTRLVG       IN VARCHAR2,
    p_Operations_Performed   IN NUMBER,
    p_Start_Time             IN DATE,
    p_Units_Processed        IN NUMBER,
    p_Containers_Processed   IN NUMBER,
    p_Pick_From_Container_ID IN OUT VARCHAR2,
    p_Pick_From_Container_ID2 IN OUT VARCHAR2,
    p_PM_Ups_Group_Ind       IN OUT VARCHAR2,
    p_pick_type              IN OUT VARCHAR2,
    p_Generic_To_Container   IN OUT VARCHAR2,
    p_first_pick_flag        IN OUT VARCHAR2,
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    p_pick_to_container_id   IN OUT VARCHAR2,
    p_pick_to_container_id2   IN OUT VARCHAR2,
    p_zone                   IN OUT VARCHAR2,
    p_container_qty          IN OUT NUMBER,
    p_reference_code         IN OUT VARCHAR2,
    p_Container_Qty2         IN OUT NUMBER,
    p_Conf_Location_ID       IN OUT VARCHAR2,
    p_Conf_Container_ID      IN OUT VARCHAR2,
    p_Empty_Pressed          IN OUT VARCHAR2,
    p_To_Loc_Entered         IN OUT VARCHAR2,
    p_old_to_cid             IN OUT VARCHAR2,
    p_wave_nbr               IN OUT NUMBER,
    p_Bulk_Group             IN OUT VARCHAR2,
    p_original_pick_qty      IN OUT VARCHAR2,
    p_Distro_Nbr             IN OUT VARCHAR2,
    p_Pick_Container_Qty     IN OUT NUMBER,
    p_Description            IN OUT VARCHAR2,
    p_From_Location_Id       IN OUT VARCHAR2,
    p_goto_field             IN OUT VARCHAR2,
    P_ITEM_ID                IN OUT VARCHAR2 ,
    P_DEST_ID                IN OUT NUMBER  ,   
    P_PM_NO_PICK_FLAG        IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG        IN OUT VARCHAR2 ,
    P_Global_Intrlvg         IN OUT VARCHAR2);
  --
  PROCEDURE GET_GENERIC_PICK_DIRECTIVE2 (
    p_facility_id            IN VARCHAR2,
    P_PM_TASK_QUEUE_IND      IN VARCHAR2 ,
    P_PM_WAVE_NBR            IN NUMBER, 
    P_PM_ROWID               IN VARCHAR2 ,
    P_DISTRO_TS              IN DATE  ,   
    p_PROCESS_BY_AISLE       IN VARCHAR2,
	p_PM_Pick_Type_C         IN VARCHAR2,
	p_PM_Pick_Type_CP        IN VARCHAR2,
    p_PM_Pick_Type           IN VARCHAR2,
    p_PM_START_LOCATION      IN OUT VARCHAR2,
    p_Labeled_Picking        IN VARCHAR2,
    p_USER                   IN VARCHAR2,   
    P_PM_ALLOW_INTRLVG       IN VARCHAR2,        
    p_Operations_Performed   IN NUMBER,
    p_Start_Time             IN DATE,
    p_Units_Processed        IN NUMBER,      
    p_Containers_Processed   IN NUMBER,
    p_Pick_From_Container_ID IN OUT VARCHAR2,
    p_Pick_From_Container_ID2 IN OUT VARCHAR2,
    p_PM_Ups_Group_Ind       IN OUT VARCHAR2,
    p_pick_type              IN OUT VARCHAR2,
    p_Generic_To_Container   IN OUT VARCHAR2,
    p_first_pick_flag        IN OUT VARCHAR2,
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    p_pick_to_container_id   IN OUT VARCHAR2,
    p_pick_to_container_id2   IN OUT VARCHAR2,
    p_zone                   IN OUT VARCHAR2,
    p_container_qty          IN OUT NUMBER,
    p_reference_code         IN OUT VARCHAR2,
    p_Container_Qty2         IN OUT NUMBER,
    p_Conf_Location_ID       IN OUT VARCHAR2,
    p_Conf_Container_ID      IN OUT VARCHAR2,
    p_Empty_Pressed          IN OUT VARCHAR2,
    p_To_Loc_Entered         IN OUT VARCHAR2,
    p_old_to_cid             IN OUT VARCHAR2,
    p_wave_nbr               IN OUT NUMBER,
    p_Bulk_Group             IN OUT VARCHAR2,
    p_original_pick_qty      IN OUT VARCHAR2,
    p_Distro_Nbr             IN OUT VARCHAR2,
    p_Pick_Container_Qty     IN OUT NUMBER,
    p_Description            IN OUT VARCHAR2,
    p_From_Location_Id       IN OUT VARCHAR2,
    p_goto_field             IN OUT VARCHAR2,
    P_ITEM_ID                IN OUT VARCHAR2 ,
    P_DEST_ID                IN OUT NUMBER  ,    
    P_PM_NO_PICK_FLAG        IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG        IN OUT VARCHAR2 ,   
    P_Global_Intrlvg         IN OUT VARCHAR2 );
  --
 
    
  PROCEDURE Validate_Qty(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER,
    I_UNITS_PROCESSED          IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER,
    I_OLD_TO_CID               IN OUT VARCHAR2,
    I_ITEM_ID                  IN OUT VARCHAR2,
    I_TO_LOCATION_ID           IN OUT VARCHAR2,
    I_TOTAL_WEIGHT             IN OUT NUMBER,
    I_UNITS_PICKED             IN OUT NUMBER,
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2,
    I_SUGGESTED_LOC            IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_CONTAINER_QTY01          IN OUT NUMBER ,
    I_MASTER_TO_CONTAINER      IN OUT VARCHAR2 ,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,    
    I_GOTO_FIELD               IN OUT VARCHAR2,
    I_FIRST_WEIGHT_FLAG        IN OUT VARCHAR2 ,
    I_EVENT_CODE               IN OUT NUMBER ,
    P_VERSION                  IN NUMBER,
    I_BULK_GROUP               IN VARCHAR2,
    I_DISTRO_NBR               IN VARCHAR2,
    I_DISTRO_TS                IN DATE,
    I_ZONE                     IN VARCHAR2,
    I_PM_ALLOW_INTRLVG         IN VARCHAR2,
    I_USER                     IN VARCHAR2,
    I_DEST_ID                  IN NUMBER,
    I_WAVE_NBR                 IN NUMBER,
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY2           IN NUMBER ,
    I_EMPTY_PRESSED            IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG          IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_TYPE                IN VARCHAR2 ,
    SYSTEM_CURSOR_BLOCK        IN VARCHAR2 );
  --
  PROCEDURE unlock_bulk_type_picks(
      p_facility_id VARCHAR2,
      p_user        VARCHAR2 );
  --
  FUNCTION check_wave_status(
      p_facility_id VARCHAR2,
      p_wave_nbr    VARCHAR2)
    RETURN VARCHAR2;
  --
  
  PROCEDURE CONF_CONTAINER_KEY_NEXT (
    V_RETURN                 IN OUT VARCHAR2, 
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CASEPACK               IN OUT NUMBER ,
    I_CONF_CONTAINER_ID      IN VARCHAR2 ,
    I_CONTAINER_QTY          IN NUMBER ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_CONTAINER_QTY2         IN NUMBER ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER );
    
  --
  PROCEDURE bypass_pick(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2, 
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_CODE                     IN OUT VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_AUTH_USER_ID             IN VARCHAR2 ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER ,   
    I_GOTO_FIELD               IN OUT VARCHAR2,
    P_VERSION                  IN NUMBER );
    --
  PROCEDURE BYPASS_KEY_PRESS(
    V_RETURN                   IN OUT VARCHAR2,
    V_CLEAR_BLOCK              IN OUT VARCHAR2 ,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_LOG_BYPASS               IN OUT VARCHAR2,
    I_REASON_CODE              IN OUT VARCHAR2,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,
    I_GOTO_FIELD               IN OUT VARCHAR2,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_AUTH_USER_ID             IN VARCHAR2 ,
    I_DISTRO_NBR               IN VARCHAR2,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER,
    I_USER                     IN VARCHAR2,
    P_VERSION                  IN NUMBER);
    --
  FUNCTION V_REASON_CODE(
      V_RETURN         IN OUT VARCHAR2,
      I_MSG_DISPLAY    IN OUT VARCHAR2,
      I_CODE           IN VARCHAR2 )
    RETURN INT;
  --
  PROCEDURE Process_Remaining_C(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CONTAINER_QTY          IN OUT NUMBER ,
    I_CONTAINER_QTY01        IN OUT NUMBER ,
    I_MASTER_TO_CONTAINER    IN OUT VARCHAR2 ,
    I_CALLING_BLOCK          IN OUT VARCHAR2 ,    
    I_GOTO_FIELD             IN OUT VARCHAR2,
    I_FIRST_WEIGHT_FLAG      IN OUT VARCHAR2 ,
    I_NBR_CHILD              IN OUT NUMBER ,
    I_CONTAINERS_PROCESSED   IN OUT NUMBER,
    I_OPERATIONS_PERFORMED   IN OUT NUMBER,
    I_UNITS_PROCESSED        IN OUT NUMBER,
    I_CASEPACK               IN OUT NUMBER,
    I_OLD_TO_CID             IN OUT VARCHAR2,  
    I_TO_LOCATION_ID         IN OUT VARCHAR2,
    I_TOTAL_WEIGHT           IN OUT NUMBER,
    I_PICK_CONTAINER_QTY     IN OUT NUMBER ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2,
    I_SUGGESTED_LOC          IN OUT VARCHAR2,
    I_EVENT_CODE             IN OUT NUMBER ,
    I_UNITS_PICKED           IN OUT NUMBER,
    I_PICK_FROM_CONTAINER_ID IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID       IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 , 
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG        IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_BULK_GROUP             IN VARCHAR2, 
    I_DISTRO_TS              IN DATE,
    I_ZONE                   IN VARCHAR2,
    I_ORIGINAL_PICK_QTY      IN NUMBER,
    I_PM_ALLOW_INTRLVG       IN VARCHAR2, 
    I_DEST_ID                IN NUMBER,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,   
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    P_VERSION                IN NUMBER  );
    --
  PROCEDURE Process_Remaining_CIDS(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CONTAINER_QTY          IN OUT NUMBER ,
    I_CONTAINER_QTY01        IN OUT NUMBER ,
    I_MASTER_TO_CONTAINER    IN OUT VARCHAR2 ,
    I_CALLING_BLOCK          IN OUT VARCHAR2 ,    
    I_GOTO_FIELD             IN OUT VARCHAR2,
    I_FIRST_WEIGHT_FLAG      IN OUT VARCHAR2 ,
    I_NBR_CHILD              IN OUT NUMBER ,
    I_CONTAINERS_PROCESSED   IN OUT NUMBER,
    I_OPERATIONS_PERFORMED   IN OUT NUMBER,
    I_UNITS_PROCESSED        IN OUT NUMBER,
    I_CASEPACK               IN OUT NUMBER,
    I_OLD_TO_CID             IN OUT VARCHAR2,  
    I_TO_LOCATION_ID         IN OUT VARCHAR2,
    I_TOTAL_WEIGHT           IN OUT NUMBER,
    I_PICK_CONTAINER_QTY     IN OUT NUMBER ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2,
    I_SUGGESTED_LOC          IN OUT VARCHAR2,
    I_EVENT_CODE             IN OUT NUMBER ,
    I_UNITS_PICKED           IN OUT NUMBER,
    I_PICK_FROM_CONTAINER_ID IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID       IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_GENERIC_TO_CHILD       IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG        IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_BULK_GROUP             IN VARCHAR2, 
    I_DISTRO_TS              IN DATE,
    I_ZONE                   IN VARCHAR2,
    I_ORIGINAL_PICK_QTY      IN NUMBER,
    I_PM_ALLOW_INTRLVG       IN VARCHAR2, 
    I_DEST_ID                IN NUMBER,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,   
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    P_VERSION                IN NUMBER  );
  --
  
  PROCEDURE TO_LOCATION_DONE_KEY (
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2 ,
    I_SUGGESTED_LOC            IN OUT VARCHAR2,
    I_TOTAL_WEIGHT             IN OUT NUMBER ,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,
    I_GOTO_FIELD               IN OUT VARCHAR2,
    I_UNITS_PICKED             IN OUT NUMBER,
    I_UNITS_PROCESSED          IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER,
    I_OLD_TO_CID               IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 , 
    I_MASTER_TO_CONTAINER      IN OUT VARCHAR2 ,
    I_FIRST_WEIGHT_FLAG        IN OUT VARCHAR2 ,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER,
    P_PM_NO_PICK_FLAG          IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG          IN OUT VARCHAR2 , 
    I_FIRST_SCAN_FLAG          IN OUT VARCHAR2 ,
    P_Global_Intrlvg           IN OUT VARCHAR2,
    I_TO_LOCATION_ID           IN VARCHAR2 ,
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_DEST_ID                  IN NUMBER ,
    I_EVENT_CODE               IN NUMBER ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER,
    P_VERSION                  IN NUMBER,
    I_BULK_GROUP               IN VARCHAR2,
    I_DISTRO_NBR               IN VARCHAR2,
    I_DISTRO_TS                IN DATE,
    I_ZONE                     IN VARCHAR2,
    I_PM_ALLOW_INTRLVG         IN VARCHAR2,  
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY2           IN NUMBER ,
    I_EMPTY_PRESSED            IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,  
    SYSTEM_CURRENT_FORM        IN VARCHAR2);
    
    --
     PROCEDURE TR_EXIT_BP (    
    V_RETURN                   IN OUT VARCHAR2,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2,      
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_OPERATIONS_PERFORMED     IN NUMBER,
    I_Units_Processed          IN NUMBER,
    I_Containers_Processed     IN NUMBER,
    I_REFERENCE_CODE           IN VARCHAR2 ,
    I_Start_Time               IN DATE 
    ) ;
    --
    PROCEDURE CONT_CONTAINER_ID_KEY_NEXT (
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CONTAINER_ID           IN OUT VARCHAR2 ,    
    I_NBR_CHILD              IN OUT NUMBER ,
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_EVENT_CODE             IN NUMBER ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 );
    --
    PROCEDURE LC_GENERIC_TO_CHILD_KEY_NEXT(
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_CONTAINER_QTY        IN OUT NUMBER ,
    I_GENERIC_TO_CHILD     IN OUT VARCHAR2 ,    
    I_FIRST_SCAN_FLAG      IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID     IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID IN VARCHAR2 ,
    I_FACILITY_ID          IN VARCHAR2 ,
    I_USER                 IN VARCHAR2 ,
    I_EVENT_CODE           IN NUMBER ,
    I_PICK_TYPE            IN VARCHAR2 );
    --
    PROCEDURE LC_PICK_TO_CHILD_KEY_NEXT(
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_CONTAINER_QTY        IN OUT NUMBER ,
    I_PICK_TO_CHILD        IN OUT VARCHAR2 ,
    I_FIRST_SCAN_FLAG      IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID     IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID IN VARCHAR2 ,
    I_FACILITY_ID          IN VARCHAR2 ,
    I_USER                 IN VARCHAR2 ,
    I_EVENT_CODE           IN NUMBER ,
    I_PICK_TYPE            IN VARCHAR2 );
    --
    PROCEDURE LC_F4_KEY(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_GENERIC_TO_CHILD       IN OUT VARCHAR2 ,
    I_PICK_TO_CHILD          IN OUT VARCHAR2 ,
    I_OLD_TO_CID             IN VARCHAR2 ,
    I_PICK_CONTAINER_QTY     IN NUMBER ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_DISTRO_NBR             IN VARCHAR2,
    I_WAVE_NBR               IN NUMBER ,
    I_DEST_ID                IN NUMBER ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_BP_CONTAINER_QTY       IN NUMBER ,
    I_LC_CONTAINER_QTY       IN NUMBER ,
    I_FIRST_SCAN_FLAG        IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_EVENT_CODE             IN NUMBER ,
    I_PICK_TYPE              IN VARCHAR2 );
    
PROCEDURE Run_Purge_Directive(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_BULK_GROUP               IN VARCHAR2 ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER );
    
PROCEDURE CHECK_CATCH_WEIGHT(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,   
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    P_VERSION                IN NUMBER,    
    I_BULK_GROUP             IN VARCHAR2,
    I_ORIGINAL_PICK_QTY      IN NUMBER,
    I_CONTAINER_QTY          IN NUMBER,
    I_FROM_LOCATION_ID       IN VARCHAR2,
    I_CONTAINERS_PROCESSED   IN OUT NUMBER,
    I_OPERATIONS_PERFORMED   IN OUT NUMBER,
    I_UNITS_PROCESSED        IN OUT NUMBER,
    I_CASEPACK               IN OUT NUMBER,
    I_DISTRO_NBR             IN VARCHAR2,
    I_DISTRO_TS              IN DATE,
    I_FIRST_SCAN_FLAG        IN VARCHAR2,
    I_OLD_TO_CID             IN OUT VARCHAR2,
    I_PICK_CONTAINER_QTY     IN NUMBER,
    I_ZONE                   IN VARCHAR2, 
    I_PM_ALLOW_INTRLVG       IN VARCHAR2,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2 ,
    I_SUGGESTED_LOC          IN OUT VARCHAR2 ,
    I_TO_LOCATION_ID         IN VARCHAR2 ,
    I_TOTAL_WEIGHT           IN NUMBER ,
    I_CALLING_BLOCK          IN OUT VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_DEST_ID                IN NUMBER ,
    I_EVENT_CODE             IN NUMBER ,
    I_FIRST_WEIGHT_FLAG      IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_UNITS_PICKED           IN OUT NUMBER ,
    I_WAVE_NBR               IN NUMBER ,    
    I_GOTO_FIELD             IN OUT VARCHAR2,
    calling_block_in         IN VARCHAR2);
    
    PROCEDURE Validate_Qty2( 
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,       
    I_GOTO_FIELD               IN OUT VARCHAR2 ,  
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2 ,  
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT  VARCHAR2 ,
    I_TO_LOCATION_ID           IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2,
    I_BULK_GROUP               IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER ,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER ,
    I_UNITS_PROCESSED          IN OUT NUMBER ,    
    I_UNITS_PICKED             IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_DISTRO_TS                IN DATE ,
    I_EVENT_CODE               IN NUMBER ,
    I_FIRST_SCAN_FLAG          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_OLD_TO_CID               IN OUT VARCHAR2 ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID01   IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_SUGGESTED_LOC            IN OUT VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER ,
    I_ZONE                     IN VARCHAR2 ,
    P_PM_ALLOW_INTRLVG         IN VARCHAR2,
    P_VERSION                  IN NUMBER);
END PKG_PICKING;
/


CREATE OR REPLACE PACKAGE BODY PKG_PICKING
AS
  FRM_TRIGGER_FAILURE EXCEPTION;
  PRAGMA EXCEPTION_INIT(FRM_TRIGGER_FAILURE, -20777);
 procedure debug1(msg varchar2) is
 begin
 if (v_debug = 'Y') then
    dbms_output.put_line(msg);
 end if;
 end;
--
PROCEDURE Get_Error_Message(
    V_RETURN           IN OUT VARCHAR2,
    I_MSG_DISPLAY      IN OUT VARCHAR2,
    I_FACILITY_ID      IN VARCHAR2 ,
    I_USER             IN VARCHAR2 ,
    I_PROCESS_BY_AISLE IN VARCHAR2 ,
    P_PM_ALLOW_INTRLVG IN VARCHAR2 ,
    P_PM_NO_PICK_FLAG  IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG  IN OUT VARCHAR2 ,
    P_Global_Intrlvg   IN OUT VARCHAR2)
IS
/*******************************************************************************
*  Change History:                                                                
*    
*   Author           Date         Defect/CR#    Change Description      
*   --------------------------------------------------------------------------
*   L. Coronel       04/08/2008   CO43433       MVDC - High Bay Hotel 
*                                               Capabilities: If interleaving is
*                                               on and there are no more
*                                               available putaway or picking
*                                               task, display an error message.
*   L.Coronel        06/16/2008   CO43433       2. Reset all global variables
*                                               to their default values when both
*                                               pick and putaway tasks are exhausted
*                                               then exit interleaving/picking.
*   L.Coronel        06/16/2008   CO43433       3. Set all global variables accordingly
*                                               then call putaway screen after displaying
*                                               the appropriate message.
*******************************************************************************/

BEGIN
      IF P_PM_NO_TASK_FLAG = 'Y' THEN 
         IF I_PROCESS_BY_AISLE = 'Y' THEN
             I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_MORE_TASK', 'E');
              /*� Call_Msg_Window_P('NO_MORE_TASK') �*/
         ELSE
          I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_TASK_ZONE', 'E');
         	   /*� Call_Msg_Window_P('NO_TASK_ZONE') �*/
         END IF;
         /* Start CO43433 LCoronel - 2 */
         P_Global_Intrlvg := 'EXIT_INTRLVG';
          /*� Set_Global_Intrlvg('EXIT_INTRLVG') �*/
          /*� Tr_Exit �*/
         /* End CO43433 LCoronel - 2 */
      ELSE
         IF P_PM_NO_PICK_FLAG = 'N' THEN
         /* Start CO43433 LCoronel - 3 */
            P_PM_NO_PICK_FLAG := 'Y';
            IF I_PROCESS_BY_AISLE = 'Y' THEN
                I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_PICK_AISLE', 'E');
                /*� Call_Msg_Window_P('NO_PICK_AISLE') �*/
            ELSE
               I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_PICKS', 'E');
            	   /*� Call_Msg_Window_P('NO_PICKS') �*/
            END IF;
         END IF;
      END IF;
      IF P_PM_ALLOW_INTRLVG = 'Y' THEN
				 P_PM_NO_TASK_FLAG := 'Y';
				 PKG_HOTEL_PICKING_ADF.PREPARE_EXIT_PICKING_INTRLVG(I_FACILITY_ID ,I_USER  );
         P_Global_Intrlvg := 'INTRLVG';
				  /*� Set_Global_Intrlvg('INTRLVG') �*/
				  /*� NEW_FORM( lower('hh_putaway_inventory_s')) �*/
         /* End CO43433 LCoronel - 3 */
      ELSE
         /* Start CO43433 LCoronel - 2 */
         P_Global_Intrlvg := 'EXIT_INTRLVG';
          /*� Set_Global_Intrlvg('EXIT_INTRLVG') �*/
          /*� Tr_Exit �*/
         /* End CO43433 LCoronel - 2 */
      END IF;
 
END; 

FUNCTION Check_Pick_Readyness ( 
           P_FACILITY_ID  IN VARCHAR2,
           From_Container_Id_In IN VARCHAR2,
           Location_Id_In  IN VARCHAR2 )
RETURN VARCHAR2 IS
/*
||  This is referenced in HH_Container_Pick_S
*/
--*Declarations*--
t_cycle_count_status   Location.Cycle_Count_Status%TYPE;
t_location_status      Location.Location_Status%TYPE;
t_container_status     Container.Container_Status%TYPE;
-------------------------------------------------
CURSOR Cycle_Count_Status_Cursor IS
SELECT Cycle_Count_Status,
       Location_Status
  FROM Location
 WHERE Facility_Id = P_FACILITY_ID
   AND Location_Id = Location_Id_in;
------------------------------------------------
CURSOR Container_Status_Cursor IS
SELECT Container_Status
  FROM Container
 WHERE Facility_Id   = P_FACILITY_ID
   AND Container_Id  = From_Container_Id_In;
------------------------------------------------
BEGIN
  OPEN Cycle_Count_Status_Cursor;
    FETCH Cycle_Count_Status_Cursor INTO t_cycle_count_status,
                                         t_location_status;
  CLOSE Cycle_Count_Status_Cursor;
--
  OPEN Container_Status_Cursor;
    FETCH Container_Status_Cursor INTO t_container_status;
  CLOSE Container_Status_Cursor;
----------------------------------------------------
-- Check if location has been manually marked
-- for cycle count or if the location status
-- is out of service. If OK then check container
-- status, if it is (T)roubled then return invalid
-- status.
---------------------------------------------------
  IF NVL (t_cycle_count_status, 'MM') = 'MM' OR
     NVL (t_location_status, 'OUT-SERVICE') = 'OUT-SERVICE'
  THEN
      RETURN('INV_LOCATION');
  ELSIF t_container_Status = 'T' THEN
      RETURN('INV_CONT_STATUS');
  ELSIF t_container_status IS NULL THEN
      RETURN('INV_CONTAINER');
  ELSE
      RETURN('SUCCESS');
  END IF;
RETURN NULL; END Check_Pick_Readyness;

   
  FUNCTION mlp_in_shuttle_loc( 
      facility_id_in IN location.facility_id%TYPE,
      location_id_in IN location.location_id%TYPE)
    RETURN VARCHAR2
  IS
    ---
    l_dummy       VARCHAR2(1);
    t_shuttle_loc BOOLEAN;
    ---
    CURSOR check_shuttle_loc
    IS
      SELECT 'x'
      FROM location
      WHERE facility_id = facility_id_in
      AND location_id   = location_id_in
      AND location_type = g_scp (facility_id_in, 'shuttle_loc_type');
    ---
  BEGIN
    ---
    OPEN check_shuttle_loc;
    FETCH check_shuttle_loc INTO l_dummy;
    t_shuttle_loc := check_shuttle_loc%FOUND;
    CLOSE check_shuttle_loc;
    ---
    IF NOT t_shuttle_loc THEN
      RETURN ('N');
    END IF;
    ---
    RETURN ('Y');
    ---
  EXCEPTION
    ---
  WHEN OTHERS THEN
    ---
    IF check_shuttle_loc%ISOPEN THEN
      CLOSE check_shuttle_loc;
    END IF;
    ---
    RETURN ('N');
    ---
  END mlp_in_shuttle_loc;
  FUNCTION check_mlp_validity(
      container_id_in container.container_id%TYPE,
      p_facility_id VARCHAR2)
    RETURN BOOLEAN
  IS
    t_dummy VARCHAR2(1);
    t_sys_wip container_wip.wip_code%TYPE;
    t_no_wip BOOLEAN;
    CURSOR check_mlp_active_wip
    IS
      SELECT 'X'
      FROM container_wip cw
      WHERE facility_id = p_facility_Id
      AND wip_code     <> t_sys_wip
      AND end_ts       IS NULL
      AND container_id IN
        (SELECT container_id
        FROM container c
        WHERE facility_id        = p_facility_Id
        AND container_id         = cw.container_id
        AND (c.container_id      = container_id_in
        OR c.master_container_id = container_id_in)
        );
  BEGIN
    t_sys_wip := g_scp(p_facility_Id,'release_wip');
    OPEN check_mlp_active_wip;
    FETCH check_mlp_active_wip INTO t_dummy;
    t_no_wip := check_mlp_active_wip%NOTFOUND;
    CLOSE check_mlp_active_wip;
    IF t_no_wip THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END;
  PROCEDURE Save_Labor_Prod(
      p_facility_id          VARCHAR2,
      p_labeled_picking      VARCHAR2,
      p_user                 VARCHAR2,
      p_Operations_Performed VARCHAR2,
      p_Start_Time           VARCHAR2,
      p_Units_Processed      VARCHAR2,
      p_Containers_Processed VARCHAR2,
      p_Reference_Code       VARCHAR2,
      V_RETURN IN OUT VARCHAR2 )
  IS
    /*
    || Insert User Productivity in Labor_Productivity Table only
    || if the user processed at least one unit
    || USAGE : REFERENCE
    */
  BEGIN
    IF p_Operations_Performed <> 0 THEN
      INSERT
      INTO Labor_Productivity
        (
          Facility_ID,
          Start_Time,
          User_ID,
          Activity_Code,
          End_Time,
          Units_Processed,
          Reference_Code,
          Containers_Processed,
          Operations_Performed
        )
        VALUES
        (
          p_Facility_ID,
          p_Start_Time,
          p_User,
          'B_PICK',
          SYSDATE,
          p_Units_Processed,
          p_Reference_Code,
          p_Containers_Processed,
          p_Operations_Performed
        );
      COMMIT;
    END IF;
  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    NULL;
  WHEN OTHERS THEN
    PKG_COMMON_ADF.LOG_ERROR( V_RETURN, SQLCODE, 'Save_Labor_Prod', SQLERRM);
    --Log_Error(SQLCODE,'Save_Labor_Prod', SQLERRM);
  END Save_Labor_Prod;
  
  -- --------------  
  -- Validate if FROM CONTAINER is ready for picking
  -- --------------
  PROCEDURE v_from_container(
    V_RETURN               IN OUT VARCHAR2,
    Pick_From_Container_Id IN VARCHAR2,
    P_FACILITY_ID          IN VARCHAR2,
    From_Container_Id_In   IN VARCHAR2,
    Location_Id_In         IN VARCHAR2)
IS
-------------------------------------------
/* Checks to see if the from container is */
/* ready for picking.                     */
-------------------------------------------
purge_failure   EXCEPTION;
t_status        User_Message.Message_Code%TYPE;
t_purge_status  VARCHAR2(200);
l_log_bypass    VARCHAR2(3);
--
BEGIN
  IF Pick_From_Container_Id IS NOT NULL THEN
     t_status := Check_Pick_Readyness(P_FACILITY_ID,
                                 From_Container_Id_In,
                                 Location_Id_In );
     IF t_status <> 'SUCCESS' THEN
--
             -- BEGIN CR560
             /*t_purge_status := purge_directive( :Work.Facility_Id,
                                 :Work_Local.Wave_Nbr,
                                 :Work_Local.Distro_Nbr,
                                 :Work_Local.Pick_From_Container_Id,
                                 :Work_Local.Dest_Id,
                                 :Bulk_Pick.Pick_To_Container_Id );
             IF t_purge_status <> 'SUCCESS' THEN
                 RAISE purge_failure;
             END IF;
             COMMIT;
             Call_Msg_Window_P(t_status);             
             Get_Pick_Directive;*/
             l_log_bypass := 'N';
             -- call the TR_KEY_IN procedure to prevent the inserting of record in error_log table at this point
             -- TR_KEY_IN('F7'); 
             -- END CR560

     END IF;
  END IF;
EXCEPTION	
WHEN OTHERS THEN  
  PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'v_from_container', SQLERRM);
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
END v_from_container;
 
 PROCEDURE GET_GENERIC_PICK_DIRECTIVE(
    p_facility_id            IN VARCHAR2,
    P_PM_wave_date           IN DATE,
    p_PROCESS_BY_AISLE       IN VARCHAR2,
	p_PM_Pick_Type_C         IN VARCHAR2,
	p_PM_Pick_Type_CP        IN VARCHAR2,
    p_PM_Pick_Type           IN VARCHAR2,
    p_PM_START_LOCATION      IN VARCHAR2,
    p_PM_UPS_code            IN VARCHAR2,
    p_Labeled_Picking        IN VARCHAR2,
    p_USER                   IN VARCHAR2,
    P_PM_ALLOW_INTRLVG       IN VARCHAR2,
    p_Operations_Performed   IN NUMBER,
    p_Start_Time             IN DATE,
    p_Units_Processed        IN NUMBER,
    p_Containers_Processed   IN NUMBER,
    p_Pick_From_Container_ID IN OUT VARCHAR2,
    p_Pick_From_Container_ID2 IN OUT VARCHAR2,
    p_PM_Ups_Group_Ind       IN OUT VARCHAR2,
    p_pick_type              IN OUT VARCHAR2,
    p_Generic_To_Container   IN OUT VARCHAR2,
    p_first_pick_flag        IN OUT VARCHAR2,
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    p_pick_to_container_id   IN OUT VARCHAR2,
    p_pick_to_container_id2   IN OUT VARCHAR2,
    p_zone                   IN OUT VARCHAR2,
    p_container_qty          IN OUT NUMBER,
    p_reference_code         IN OUT VARCHAR2,
    p_Container_Qty2         IN OUT NUMBER,
    p_Conf_Location_ID       IN OUT VARCHAR2,
    p_Conf_Container_ID      IN OUT VARCHAR2,
    p_Empty_Pressed          IN OUT VARCHAR2,
    p_To_Loc_Entered         IN OUT VARCHAR2,
    p_old_to_cid             IN OUT VARCHAR2,
    p_wave_nbr               IN OUT NUMBER,
    p_Bulk_Group             IN OUT VARCHAR2,
    p_original_pick_qty      IN OUT VARCHAR2,
    p_Distro_Nbr             IN OUT VARCHAR2,
    p_Pick_Container_Qty     IN OUT NUMBER,
    p_Description            IN OUT VARCHAR2,
    p_From_Location_Id       IN OUT VARCHAR2,
    p_goto_field             IN OUT VARCHAR2,
    P_ITEM_ID                IN OUT VARCHAR2 ,
    P_DEST_ID                IN OUT NUMBER  ,   
    P_PM_NO_PICK_FLAG        IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG        IN OUT VARCHAR2 ,
    P_Global_Intrlvg         IN OUT VARCHAR2)
  IS
  
    value_found BOOLEAN := FALSE;
    --
    t_dummy VARCHAR2(1);
    t_pick_order Pick_Directive.Pick_Order%TYPE;
    t_purge_status    VARCHAR2(200);
    t_container_count NUMBER(10);
    t_pick_to_container_id Container.Container_Id%TYPE;
    container_found BOOLEAN;
    t_location_id Container.Location_Id%TYPE;
    t_Zone Zone.Zone%TYPE;
    purge_failure EXCEPTION;
    inv_container EXCEPTION;
    inv_pick_type EXCEPTION;
    no_picks      EXCEPTION;
    -----------------------------------------------------------------------
    -- Start CR35261 - LCC
    t_mlp_in_location BOOLEAN;
    t_loc location.location_id%TYPE;
    t_mlp_check BOOLEAN;
    -- End CR35261
    ---------------------------------------------------------------------------
    -- variable declarations.
    ---------------------------------------------------------------------------
    t_pick_found BOOLEAN;
    l_item_id pick_directive.item_id%TYPE;
    l_item_qty NUMBER;
    l_dest_id  NUMBER;
    CURSOR V_Generic_Cntr_Cursor
    IS
      SELECT 'X'
      FROM SYS.DUAL
      WHERE EXISTS
        (SELECT container_id
        FROM Container
        WHERE Facility_ID = p_facility_id
        AND Container_Id  = p_Generic_To_Container
        );
    -- ---------------------------------------
    CURSOR Pick_Container_Cursor
    IS
      SELECT
        /*+ rule */
        pd.Pick_To_Container_Id,
        pd.Wave_Nbr,
        pd.Zone,
        pd.Pick_Type,
        pd.Pick_From_Container_ID,
        c.Location_ID,
        SUM(Pick_Container_Qty)    AS total_cont_qty,
        COUNT(Pick_To_Container_ID)AS total_cont
      FROM container c,
        pick_directive pd
      WHERE pd.facility_id    = p_facility_id
      AND pd.Pick_Type       IN (p_PM_Pick_Type_CP, p_PM_Pick_Type_C, p_PM_Pick_Type)
      AND pd.zone             = t_zone
      AND (p_PROCESS_BY_AISLE = 'N'
      OR (p_PROCESS_BY_AISLE  = 'Y'
      AND C.location_id LIKE SUBSTR(p_PM_START_LOCATION,1,5)
        || '%'))
      AND TRUNC(pd.distro_ts)           <= TRUNC(NVL(p_PM_wave_date, pd.distro_ts))
      AND NVL(pd.pick_in_progress, 'N') <> 'Y'
      AND pd.facility_id                 = c.facility_id
      AND pd.pick_from_container_id      = c.container_id
      AND c.location_id                 >= p_PM_START_LOCATION
      AND (pd.pick_type                  = 'C'
      OR EXISTS
        (SELECT 'X'
        FROM unit_pick_zone upz
        WHERE upz.facility_id     = pd.facility_id
        AND upz.dest_id           = pd.dest_id
        AND unit_pick_system_code = p_PM_UPS_code
        ))
      AND EXISTS
        (SELECT 'X'
        FROM wave w
        WHERE w.facility_id = pd.facility_id
        AND w.wave_nbr      = pd.wave_nbr
        AND w.wave_status  IN ('OPEN', 'PRINTED')
        )
      AND EXISTS
        (SELECT 'X'
        FROM location l
        WHERE c.facility_id       = l.facility_id
        AND c.location_id         = l.location_id
        AND l.cycle_count_status <> 'MM'
        AND l.location_status    <> 'OUT-SERVICE'
        )
      GROUP BY pd.Pick_Type,
        pd.Wave_Nbr,
        pd.Zone,
        pd.Pick_From_Container_ID,
        pd.Pick_To_Container_ID,
        c.Location_Id,
        pd.zone
      ORDER BY DECODE(pd.Zone, t_Zone, 1, 2),
        c.Location_Id;
      -- ---------------------------------------
      CURSOR Pick_Directive_Cursor
      IS
        SELECT
          /*+ rule */
          pd.Pick_To_Container_ID,
          pd.Pick_From_Container_ID,
          pd.Wave_Nbr,
          Zone,
          pd.Pick_Type,
          c.Location_ID,
          SUM(Pick_Container_Qty)    AS total_cont_qty,
          COUNT(Pick_To_Container_ID)AS total_cont
        FROM Pick_Directive pd,
          Container c
        WHERE pd.Facility_Id    = p_facility_id
        AND pd.zone             = t_zone
        AND (p_PROCESS_BY_AISLE = 'N'
        OR (p_PROCESS_BY_AISLE  = 'Y'
        AND C.location_id LIKE SUBSTR(p_PM_START_LOCATION,1,5)
          || '%'))
        AND pd.Pick_Type             IN (p_PM_Pick_Type_CP, p_PM_Pick_Type_C, p_PM_Pick_Type)
        AND pd.Pick_From_Container_Id = c.Container_id
        AND TRUNC(pd.distro_ts)      <= TRUNC(NVL(p_PM_wave_date, pd.distro_ts))
 --     AND NVL(pd.pick_in_progress, 'N') <> 'Y'
      AND pd.facility_id                 = c.facility_id 
      AND c.location_id                 >= p_PM_START_LOCATION
   AND (pd.pick_type             = 'C'
        OR EXISTS
          (SELECT 'X'
          FROM unit_pick_zone upz
          WHERE upz.facility_id         = pd.facility_id
          AND upz.dest_id               = pd.dest_id
          AND upz.unit_pick_system_code = p_PM_UPS_code
          )) 
        AND EXISTS
          (SELECT 'X'
          FROM Location l
          WHERE l.Facility_Id       = c.Facility_Id
          AND l.Location_Id         = c.Location_Id
          AND l.Cycle_Count_Status <> 'MM'
          AND l.Location_Status    <> 'OUT-SERVICE'
          )
        AND EXISTS
          (SELECT 'X'
          FROM wave w
          WHERE pd.facility_id = w.facility_id
          AND pd.wave_nbr      = w.wave_nbr
          AND w.wave_status   IN ('OPEN', 'PRINTED')
          )
        GROUP BY pd.Pick_Type,
          pd.Wave_Nbr,
          pd.Zone,
          pd.Pick_From_Container_ID,
          pd.Pick_To_Container_ID,
          c.Location_Id
        ORDER BY c.Location_Id;
        -- ---------------------------------------
        CURSOR Container_Dest_Id_Cursor
        IS
          SELECT Distro_Nbr,
            Dest_ID
          FROM Pick_Directive
          WHERE Facility_ID          = p_facility_id
          AND Pick_From_Container_ID = p_Pick_From_Container_ID
          AND Pick_Type             IN (p_Pick_Type, 'CP', 'C')
          GROUP BY pick_from_container_id,
            dest_id,
            distro_nbr;
        -- ---------------------------------------
        CURSOR Cntr_Location_Generic_Cursor
        IS
          SELECT Location_Id
          FROM Container
          WHERE Facility_Id = p_facility_id
          AND Container_Id  = p_Pick_From_Container_ID;
        -- ---------------------------------------
        CURSOR Cntr_Qty_Generic_Cursor
        IS
          SELECT SUM(CI.Container_Qty)
          FROM Container_Item CI,
            Container C
          WHERE C.Facility_Id        = p_facility_id
          AND CI.Facility_Id         = p_facility_id
          AND (C.Master_Container_Id = p_Pick_From_Container_ID
          OR C.Container_Id          = p_Pick_From_Container_ID)
          AND container_status NOT  IN ('P','D')
          AND CI.Container_Id        = C.Container_Id;
        -- ---------------------------------------
        CURSOR Item_Master_Description_Cursor
        IS
          SELECT Description
          FROM Item_Master
          WHERE Facility_Id = p_facility_id
          AND Item_Id       = l_item_id;
        -----------------------------------------
        
        CURSOR Get_Pick_Zone
        IS
          SELECT Zone
          FROM Location
          WHERE Facility_Id = p_facility_id
          AND Location_Id   = p_PM_START_LOCATION;
        CURSOR Get_Pick_Grp_Ind
        IS
          SELECT Group_Ind
          FROM Unit_Pick_System
          WHERE facility_id         = p_facility_id
          AND unit_pick_system_code = p_PM_UPS_code;
        -- ---------------------------------------
        CURSOR PG_Pick_Container_Cursor
        IS
          SELECT
            /*+ rule */
            pd.Pick_To_Container_Id pick_to_container_id,
            pd.Wave_Nbr,
            pd.Zone,
            pd.Pick_Type,
            pd.Pick_From_Container_ID,
            c.Location_ID,
            SUM(pd.Pick_Container_Qty)    AS total_cont_qty,
            COUNT(pd.Pick_To_Container_ID)AS total_cont
          FROM container c,
            pick_directive pd
          WHERE pd.facility_id    = p_facility_id
          AND pd.Pick_Type       IN (p_PM_Pick_Type_CP, p_PM_Pick_Type_C, p_PM_Pick_Type)
          AND pd.zone             = t_zone
          AND (p_PROCESS_BY_AISLE = 'N'
          OR (p_PROCESS_BY_AISLE  = 'Y'
          AND C.location_id LIKE SUBSTR(p_PM_START_LOCATION,1,5)
            || '%'))
          AND TRUNC(pd.distro_ts)           <= TRUNC(NVL(p_PM_wave_date, pd.distro_ts))
          AND NVL(pd.pick_in_progress, 'N') <> 'Y'
          AND pd.facility_id                 = c.facility_id
          AND pd.pick_from_container_id      = c.container_id
          AND c.location_id                 >= p_PM_START_LOCATION
          AND (pd.pick_type                  = 'C'
          OR EXISTS
            (SELECT 'X'
            FROM unit_pick_zone upz,
              unit_pick_system_group ug
            WHERE ug.facility_id          = p_facility_id
            AND ug.unit_pick_system_group = p_PM_UPS_code
            AND ug.facility_id            = upz.facility_id
            AND ug.unit_pick_system_code  = upz.unit_pick_system_code
            AND upz.facility_id           = pd.facility_id
            AND upz.dest_id               = pd.dest_id
            ))
          AND EXISTS
            (SELECT 'X'
            FROM wave w
            WHERE w.facility_id = pd.facility_id
            AND w.wave_nbr      = pd.wave_nbr
            AND w.wave_status  IN ('OPEN', 'PRINTED')
            )
          AND EXISTS
            (SELECT 'X'
            FROM location l
            WHERE c.facility_id       = l.facility_id
            AND c.location_id         = l.location_id
            AND l.cycle_count_status <> 'MM'
            AND l.location_status    <> 'OUT-SERVICE'
            )
          GROUP BY pd.Pick_Type,
            pd.Wave_Nbr,
            pd.Zone,
            pd.Pick_From_Container_ID,
            pd.Pick_To_Container_ID,
            c.Location_Id,
            pd.zone
          ORDER BY DECODE(pd.Zone, t_Zone, 1, 2),
            c.Location_Id;
            
          ---------------------------------------------------  
          CURSOR PG_Pick_Directive_Cursor
          IS
            SELECT
              /*+ rule */
              pd.Pick_To_Container_ID,
              pd.Pick_From_Container_ID,
              pd.Wave_Nbr,
              Zone,
              pd.Pick_Type,
              c.Location_ID,
              SUM(pd.Pick_Container_Qty)    AS total_cont_qty,
              COUNT(pd.Pick_To_Container_ID)AS total_cont
            FROM Pick_Directive pd,
              Container c
            WHERE pd.Facility_Id    = p_facility_id
            AND pd.zone             = t_zone
            AND (p_PROCESS_BY_AISLE = 'N'
            OR (p_PROCESS_BY_AISLE  = 'Y'
            AND C.location_id LIKE SUBSTR(p_PM_START_LOCATION,1,5)
              || '%'))
            AND NVL(pd.pick_in_progress, 'N') <> 'Y'
            AND pd.Pick_Type                  IN (p_PM_Pick_Type_CP, p_PM_Pick_Type_C, p_PM_Pick_Type)
            AND pd.Pick_From_Container_Id      = c.Container_id
            AND TRUNC(pd.distro_ts)           <= TRUNC(NVL(p_PM_wave_date, pd.distro_ts))
            AND (pd.pick_type                  = 'C'
            OR EXISTS
              (SELECT 'X'
              FROM unit_pick_zone upz,
                unit_pick_system_group ug
              WHERE ug.facility_id          = p_facility_id
              AND ug.unit_pick_system_group = p_PM_UPS_code
              AND ug.facility_id            = upz.facility_id
              AND ug.unit_pick_system_code  = upz.unit_pick_system_code
              AND upz.facility_id           = pd.facility_id
              AND upz.dest_id               = pd.dest_id
              ))
            AND pd.Facility_Id = c.Facility_id
            AND c.Location_Id >= p_PM_START_LOCATION
            AND EXISTS
              (SELECT 'X'
              FROM Location l
              WHERE l.Facility_Id       = c.Facility_Id
              AND l.Location_Id         = c.Location_Id
              AND l.Cycle_Count_Status <> 'MM'
              AND l.Location_Status    <> 'OUT-SERVICE'
              )
            AND EXISTS
              (SELECT 'X'
              FROM wave w
              WHERE pd.facility_id = w.facility_id
              AND pd.wave_nbr      = w.wave_nbr
              AND w.wave_status   IN ('OPEN', 'PRINTED')
              )
            GROUP BY pd.Pick_Type,
              pd.Wave_Nbr,
              pd.Zone,
              pd.Pick_From_Container_ID,
              pd.Pick_To_Container_ID,
              pd.Pick_Container_Qty,
              c.Location_Id
            ORDER BY c.Location_Id;
            -- ---------------------------------------
            CURSOR get_item_id
            IS
              SELECT Item_id
              FROM pick_directive
              WHERE facility_id          = p_facility_id
              AND pick_from_container_id = p_Pick_From_Container_ID
              AND pick_type             IN (p_Pick_Type, 'CP', 'C');
            --
            -- CR35260 - New cursor to fetch no. of distinct item id in the pallet
            CURSOR get_item_qty
            IS
              SELECT COUNT (DISTINCT (item_id)) AS total_item_qty
              FROM pick_directive
              WHERE facility_id          = p_facility_id
              AND pick_from_container_id = p_Pick_From_Container_ID
              AND pick_type             IN (p_Pick_Type, 'CP', 'C');
            --
            -- CR35260 - New cursr to fetch no. of distinct destinations the pallet is going to
            CURSOR c_get_dest
            IS
              SELECT COUNT(DISTINCT dest_id)
              FROM Pick_Directive
              WHERE Facility_id          = p_facility_id
              AND Pick_From_Container_ID = p_Pick_From_Container_ID
              AND Pick_Type             IN (p_Pick_Type, 'CP', 'C');
            -- ---------------------------------------
            -- Start CR35261
            CURSOR get_location (cid_in container.container_id%TYPE)
            IS
              SELECT location_id
              FROM container
              WHERE facility_id = p_facility_id
              AND container_id  = cid_in;
            -- ---------------------------------------
            CURSOR check_multi_mlp_loc(cid_in IN container.container_id%TYPE, loc_in IN location.location_id%TYPE)
            IS
              SELECT
                /*+ RULE */
                'X'
              FROM container
              WHERE facility_id        = p_facility_id
              AND (master_container_id = 'NONE'
              AND container_id        <> cid_in
              OR (master_container_id <> 'NONE'
              AND master_container_id <> cid_in))
              AND location_id          = loc_in;
          BEGIN
     --     I_MSG_DISPLAY := 'lab: '||p_Labeled_Picking||' fac '||p_facility_id||' start loc: '||p_PM_START_LOCATION;  
            -- Get the probable zone, a picker would like to pick from. This can be determined
            -- by starting location scanned in Pick wothout a Pick Package screen. For labeled
            -- Picking 'N'o try to display the first pick from that zone.
            t_Zone              := NULL;
            
            IF p_Labeled_Picking = 'N' THEN
              OPEN Get_Pick_Zone;
              FETCH Get_Pick_Zone INTO t_Zone;
              CLOSE Get_Pick_Zone;
            END IF;
            debug1('Zone: '||t_zone);
     --   I_MSG_DISPLAY := I_MSG_DISPLAY||' \n zone '||t_zone;
            ---------------------------------------------------------
            -- Get wave, zone, pick_order, pick type, and distro time
            -- for the pick to container that was entered.
            -- Use for further picks.
            ---------------------------------------------------------
            -- BEGIN CR575
            -- Open and fetch the Get_Pick_Grp_Ind cursor
            OPEN Get_Pick_Grp_Ind;
            FETCH Get_Pick_Grp_Ind INTO p_PM_Ups_Group_Ind;
            CLOSE Get_Pick_Grp_Ind;
            --
            debug1('p_PM_Ups_Group_Ind: '||p_PM_Ups_Group_Ind);
      --      I_MSG_DISPLAY := I_MSG_DISPLAY||' \n Ups Group '||p_PM_Ups_Group_Ind||' First Pick: '||p_first_pick_flag;
            IF p_first_pick_flag    = 'Y' THEN
              IF p_PM_Ups_Group_Ind = 'Y' THEN
                FOR c_get_rec IN PG_Pick_Container_Cursor
                LOOP
                  t_pick_found := FALSE;
                  
                  IF c_get_rec.pick_type IN ('CP','C') THEN
                    t_pick_found := consolidate_picks( p_facility_id, NULL, c_get_rec.pick_type, c_get_rec.pick_from_container_id, 'hh_bulk_pk_across_wv_s', NULL );
                  ELSE
                    t_pick_found := TRUE;
                  END IF;
                  
                  IF c_get_rec.pick_type = 'C' THEN
                    t_mlp_check         := check_mlp_validity(c_get_rec.pick_from_container_id, p_facility_id);
                    
                    OPEN get_location (c_get_rec.pick_from_container_id);
                    FETCH get_location INTO t_loc;
                    CLOSE get_location;
                    
                    OPEN check_multi_mlp_loc (c_get_rec.pick_from_container_id,t_loc);
                    FETCH check_multi_mlp_loc INTO t_dummy;
                    t_mlp_in_location := check_multi_mlp_loc%FOUND;
                    CLOSE check_multi_mlp_loc;
                    
                  END IF;
                  --IF t_pick_found THEN
                  /* Start R.Macero CO95087 */
                  IF (t_pick_found AND c_get_rec.pick_type <> 'C') OR (t_pick_found AND c_get_rec.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND mlp_in_shuttle_loc (p_facility_id,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
                    /* End R.Macero CO95087 */
                    IF c_get_rec.pick_type = 'C' THEN
                      p_Container_Qty     := '';
                      p_Container_Qty2    := c_get_rec.total_cont_qty;
                    ELSE
                      p_Container_Qty := c_get_rec.total_cont_qty;
                    END IF;
                    
                    value_found              := TRUE;
                    p_pick_to_container_id   := c_get_rec.pick_to_container_id;
                    p_wave_nbr               := c_get_rec.wave_nbr;
                    p_zone                   := c_get_rec.zone;
                    p_Pick_Type              := c_get_rec.pick_type;
                    p_Pick_From_Container_ID := c_get_rec.pick_from_container_id;
                    t_location_id            := c_get_rec.location_id;
                    t_container_count        := c_get_rec.total_cont;
                    
                    OPEN get_item_id;
                    FETCH get_item_id INTO l_item_id;
                    CLOSE get_item_id;
                    
                    OPEN get_item_qty;
                    FETCH get_item_qty INTO l_item_qty;
                    CLOSE get_item_qty;
                    
                    IF l_item_qty > 1 THEN
                      p_item_id  := 'MULTI - SKU';
                    ELSE
                      p_item_id := l_item_id;
                    END IF;
                    EXIT;
                  END IF;
                  
                END LOOP;
                --
              ELSE -- p_PM_Ups_Group_Ind = 'N'
                -- END CR575
                -- Start CR35261: LCC - loop through the tasks to find C picks. Do not include ups code since C picks does not have these.
                -- End CR35261
                -- CR35260 - Fetch CP picks along with other pick
                --         - In case of CP pick, determine if it can be processed as BP
                --         - If not then bypass it.
                /*OPEN Pick_Container_Cursor;
                FETCH Pick_Container_Cursor INTO l_pick_to_container_id,
                l_Wave_Nbr,
                l_zone,
                p_Pick_Type,
                p_Pick_From_Container_ID,
                l_item_id,
                t_location_id,
                l_Container_Qty,
                t_container_count;
                value_found := Pick_Container_Cursor%FOUND;
                CLOSE Pick_Container_Cursor;*/
                FOR c_get_rec IN Pick_Container_Cursor
                LOOP
                  t_pick_found := FALSE;
                  IF c_get_rec.pick_type IN ('CP', 'C') THEN
                    t_pick_found := consolidate_picks( p_facility_id, NULL, c_get_rec.pick_type, c_get_rec.pick_from_container_id, 'hh_bulk_pk_across_wv_s', NULL);
                  ELSE
                    t_pick_found := TRUE;
                  END IF;
                  
                  IF c_get_rec.pick_type = 'C' THEN
                    t_mlp_check         := check_mlp_validity(c_get_rec.pick_from_container_id, p_facility_id);
                    
                    OPEN get_location (c_get_rec.pick_from_container_id);
                    FETCH get_location INTO t_loc;
                    CLOSE get_location;
                    
                    OPEN check_multi_mlp_loc (c_get_rec.pick_from_container_id,t_loc);
                    FETCH check_multi_mlp_loc INTO t_dummy;
                    t_mlp_in_location := check_multi_mlp_loc%FOUND;                    
                    CLOSE check_multi_mlp_loc;
                    
                  END IF;
                  --IF t_pick_found THEN
                  /* Start R.Macero CO95087 */
                  IF (t_pick_found AND c_get_rec.pick_type <> 'C') OR (t_pick_found AND c_get_rec.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND mlp_in_shuttle_loc (p_facility_id,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
                    /* End R.Macero CO95087 */
                    IF c_get_rec.pick_type = 'C' THEN
                      p_Container_Qty     := '';
                      p_Container_Qty2    := c_get_rec.total_cont_qty;
                    ELSE
                      p_Container_Qty := c_get_rec.total_cont_qty;
                    END IF;
                    
                    value_found              := TRUE;
                    p_pick_to_container_id   := c_get_rec.pick_to_container_id;
                    p_wave_nbr               := c_get_rec.wave_nbr;
                    p_zone                   := c_get_rec.zone;
                    p_Pick_Type              := c_get_rec.pick_type;
                    p_Pick_From_Container_ID := c_get_rec.pick_from_container_id;
                    t_location_id            := c_get_rec.location_id;
                    t_container_count        := c_get_rec.total_cont;
                    
                    OPEN get_item_id;
                    FETCH get_item_id INTO l_item_id;
                    CLOSE get_item_id;
                    
                    OPEN get_item_qty;
                    FETCH get_item_qty INTO l_item_qty;
                    CLOSE get_item_qty;
                    
                    IF l_item_qty > 1 THEN
                      p_item_id  := 'MULTI - SKU';
                    ELSE
                      p_item_id := l_item_id;
                    END IF;
                    EXIT;
                  END IF;
                END LOOP;
                --
              END IF; -- CR575 p_PM_Ups_Group_Ind = 'Y'
              /* Start CO43433 LCoronel - 2 */
              -- No rows returned from Pick_Container_Cursor
              IF NOT(value_found) THEN
                /*RAISE no_picks;*/
                Get_Error_Message(
                   V_RETURN,  I_MSG_DISPLAY, p_facility_id , p_user ,
                  p_process_by_aisle , p_pm_allow_intrlvg , P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG ,P_Global_Intrlvg);
				RETURN;
                /* End CO43433 LCoronel - 2 */
                
              ELSE
                p_Generic_To_Container := p_Pick_From_Container_ID;
                -- p_go_item('Bulk_Pick.Conf_Location_id');
                p_goto_field := 'BULK_PICK.CONF_LOCATION_ID';
              END IF;
              --
              -- CR35260 - Consider CP as a valid pick type
              /*IF p_Pick_Type NOT IN ('B','BR','BP') THEN*/
              -- Start CR35261 - LCC
              /*IF p_Pick_Type NOT IN ('B','BR','BP','CP') THEN*/
              IF p_Pick_Type NOT IN ('B','BR','BP','CP', 'C') THEN
                --
                p_Pick_From_Container_ID := '';
                p_item_id                := '';
                p_Container_Qty          := '';
                RAISE inv_pick_type;
                -- End CR35261
              END IF;
              ---------------------------------------------------------
              -- Set the reference code in the labor productivity table
              -- to equal the wave number and the zone.
              ---------------------------------------------------------
              p_Reference_Code := TO_CHAR(p_Wave_Nbr) || LPAD(p_zone, 4, ' ');
              --
              p_first_pick_flag := 'N';
            ELSE -- first_pick_flag (N)
             debug1('first_pick_flag: N. p_PM_Ups_Group_Ind: '|| p_PM_Ups_Group_Ind);
              --    GO_BLOCK('BULK_PICK'N);
              p_Conf_Location_ID  := '';
              p_Conf_Container_ID := '';
              p_Empty_Pressed     := 'N';
              p_To_Loc_Entered    := 'N';
              p_old_to_cid        := '';
              --
              -- BEGIN CR575
              -- Open the Get_Pick_Grp_Ind and PG_Pick_Directive_Cursor cursors
              IF p_PM_Ups_Group_Ind = 'Y' THEN
                -- CR35260 - Fetch CP picks along with other pick
                --         - In case of CP pick, determine if it can be processed as BP
                --         - If not then bypass it.
                /*OPEN PG_Pick_Directive_Cursor;
                FETCH PG_Pick_Directive_Cursor INTO l_Pick_To_Container_Id,
                p_Pick_From_Container_ID,
                l_Wave_Nbr,
                l_zone,
                p_Pick_Type,
                l_item_id,
                t_location_id,
                l_Container_Qty,
                t_container_count;
                value_found := PG_Pick_Directive_Cursor%FOUND;
                CLOSE PG_Pick_Directive_Cursor;*/
                FOR c_get_rec IN PG_Pick_Directive_Cursor
                LOOP
                  t_pick_found := FALSE;
                  --IF c_get_rec.pick_type = 'CP' THEN
                  IF c_get_rec.pick_type IN ('CP', 'C') THEN
                    t_pick_found := consolidate_picks( p_facility_id, NULL, c_get_rec.pick_type, c_get_rec.pick_from_container_id, 'hh_bulk_pk_across_wv_s', NULL );
                  ELSE
                    t_pick_found := TRUE;
                  END IF;
                  -- CR35261 LCC: change #3
                  IF c_get_rec.pick_type = 'C' THEN
                    t_mlp_check         := check_mlp_validity(c_get_rec.pick_from_container_id,p_facility_id);
                    OPEN get_location (c_get_rec.pick_from_container_id);
                    FETCH get_location INTO t_loc;
                    CLOSE get_location;
                    OPEN check_multi_mlp_loc (c_get_rec.pick_from_container_id,t_loc);
                    FETCH check_multi_mlp_loc INTO t_dummy;
                    t_mlp_in_location := check_multi_mlp_loc%FOUND;
                    CLOSE check_multi_mlp_loc;
                  END IF;
                  --IF t_pick_found THEN
                  /* Start R.Macero CO95087 */
                  IF (t_pick_found AND c_get_rec.pick_type <> 'C') OR (t_pick_found AND c_get_rec.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND mlp_in_shuttle_loc (p_facility_id,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
                    /* End R.Macero CO95087 */
                    IF c_get_rec.pick_type = 'C' THEN
                      p_Container_Qty     := '';
                      p_Container_Qty2    := c_get_rec.total_cont_qty;
                    ELSE
                      p_Container_Qty := c_get_rec.total_cont_qty;
                    END IF;
                    value_found              := TRUE;
                    p_pick_to_container_id   := c_get_rec.pick_to_container_id;
                    p_Pick_From_Container_ID := c_get_rec.pick_from_container_id;
                    p_Wave_Nbr               := c_get_rec.wave_nbr;
                    p_zone                   := c_get_rec.zone;
                    p_Pick_Type              := c_get_rec.pick_type;
                    t_location_id            := c_get_rec.location_id;
                    --l_Container_Qty := c_get_rec.total_cont_qty;
                    t_container_count := c_get_rec.total_cont;
                    -- End CR35261
                    OPEN get_item_id;
                    FETCH get_item_id INTO l_item_id;
                    CLOSE get_item_id;
                    
                    OPEN get_item_qty;
                    FETCH get_item_qty INTO l_item_qty;
                    CLOSE get_item_qty;
                    
                    IF l_item_qty > 1 THEN
                      p_item_id  := 'MULTI - SKU';
                    ELSE
                      p_item_id := l_item_id;
                    END IF;
                    
                    EXIT;
                  END IF;
                END LOOP;
                --
              ELSE -- p_PM_Ups_Group_Ind = 'N'
              debug1('p_PM_Ups_Group_Ind: N.  ');
                -- END CR575
                -- CR35260 - Fetch CP picks along with other pick
                --         - In case of CP pick, determine if it can be processed as BP
                --         - If not then bypass it.
                /* OPEN Pick_Directive_Cursor;
                FETCH Pick_Directive_Cursor INTO l_Pick_To_Container_Id,
                p_Pick_From_Container_ID,
                l_Wave_Nbr,
                l_zone,
                p_Pick_Type,
                l_item_id,
                t_location_id,
                l_Container_Qty,
                t_container_count;
                value_found := Pick_Directive_Cursor%FOUND;
                CLOSE Pick_Directive_Cursor;*/
                debug1('Before Loop Pick_Directive_Cursor ');
                FOR c_get_rec IN Pick_Directive_Cursor
                LOOP
                  t_pick_found := FALSE;
                  /*IF c_get_rec.pick_type = 'CP' THEN*/
                  IF c_get_rec.pick_type IN ('CP', 'C') THEN
                    t_pick_found := consolidate_picks( p_facility_id, NULL, c_get_rec.pick_type, c_get_rec.pick_from_container_id, 'hh_bulk_pk_across_wv_s', NULL );
                  ELSE
                    t_pick_found := TRUE;
                  END IF;
                  -- CR35261 LCC: change #3
                  IF c_get_rec.pick_type = 'C' THEN
                    t_mlp_check         := check_mlp_validity(c_get_rec.pick_from_container_id,p_facility_id);
                    
                    OPEN get_location (c_get_rec.pick_from_container_id);
                    FETCH get_location INTO t_loc;
                    CLOSE get_location;
                   debug1('t_loc: '|| t_loc); 
                    OPEN check_multi_mlp_loc (c_get_rec.pick_from_container_id,t_loc);
                    FETCH check_multi_mlp_loc INTO t_dummy;
                    t_mlp_in_location := check_multi_mlp_loc%FOUND;
                    CLOSE check_multi_mlp_loc;
                      debug1('t_dummy: '|| t_dummy); 
                  END IF;
                  /* Start R.Macero CO95087 */
                  IF (t_pick_found AND c_get_rec.pick_type <> 'C') OR (t_pick_found AND c_get_rec.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND mlp_in_shuttle_loc (p_facility_id,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
                    /* End R.Macero CO95087 */
                    IF c_get_rec.pick_type = 'C' THEN
                      p_Container_Qty     := '';
                      p_Container_Qty2    := c_get_rec.total_cont_qty;
                    ELSE
                      p_Container_Qty := c_get_rec.total_cont_qty;
                       debug1('p_Container_Qty:   '||p_Container_Qty);
                    END IF;
                    debug1('c_get_rec.pick_from_container_id:   '||c_get_rec.pick_from_container_id);
                    value_found              := TRUE;
                    p_pick_to_container_id   := c_get_rec.pick_to_container_id;
                    p_Pick_From_Container_ID := c_get_rec.pick_from_container_id;
                    p_Wave_Nbr               := c_get_rec.wave_nbr;
                    p_zone                   := c_get_rec.zone;
                    p_Pick_Type              := c_get_rec.pick_type;
                    t_location_id            := c_get_rec.location_id;
                    t_container_count        := c_get_rec.total_cont;
                    
                    OPEN get_item_id;
                    FETCH get_item_id INTO l_item_id;
                    CLOSE get_item_id;
                     debug1('l_item_id: '|| l_item_id);  
                    OPEN get_item_qty;
                    FETCH get_item_qty INTO l_item_qty;
                    CLOSE get_item_qty;
                     debug1('l_item_qty: '|| l_item_qty);  
                    IF l_item_qty > 1 THEN
                      p_item_id  := 'MULTI - SKU';
                    ELSE
                      p_item_id := l_item_id;
                    END IF;
                    EXIT;
                  END IF;
                END LOOP;
                --
                  debug1('After Loop Pick_Directive_Cursor ');
              END IF; -- p_PM_Ups_Group_Ind = 'Y'
              -- No rows returned from Pick_Directive_Cursor
              IF NOT(value_found) THEN
                Save_Labor_Prod( p_facility_id => p_facility_id, p_labeled_picking => p_labeled_picking, p_user => p_user, p_Operations_Performed => p_operations_performed, 
                                 p_Start_Time => p_start_time, p_Units_Processed => p_units_processed, p_Containers_Processed => p_containers_processed, 
                                 p_Reference_Code => p_reference_code, V_RETURN => v_return);
                -- PC3_Go_Field('BULK_PICK.generic_to_container');
                p_goto_field := 'BULK_PICK.GENERIC_TO_CONTAINER';
                Get_Error_Message(
                  V_RETURN           => V_RETURN,
                  I_MSG_DISPLAY      => I_MSG_DISPLAY,
                  I_FACILITY_ID      => p_facility_id ,
                  I_USER             => p_user ,
                  I_PROCESS_BY_AISLE => p_process_by_aisle ,
                  P_PM_ALLOW_INTRLVG => p_pm_allow_intrlvg ,
                  P_PM_NO_PICK_FLAG  => P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG  => P_PM_NO_TASK_FLAG ,
                  P_Global_Intrlvg   => P_Global_Intrlvg);
                 RETURN;
              ELSE
                -- Start CR35261 - LCC
                IF p_Pick_Type         = 'C' THEN
                  p_original_pick_qty := p_Container_Qty2;
                ELSE
                  p_original_pick_qty := p_Container_Qty;
                   debug1('p_Container_Qty: '|| p_Container_Qty||'  -  p_original_pick_qty: '||p_original_pick_qty);  
                END IF;
                -- End CR35261
                p_Generic_To_Container := p_Pick_From_Container_ID;
                --  p_go_item('BULK_PICK.CONF_LOCATION_ID');
                p_goto_field := 'BULK_PICK.CONF_LOCATION_ID';
              END IF; -- No value found Pick_Directive_Cursor
            END IF;   --first flag
            -- CR35260 - Multi-dest logic for full CP pallet
             debug1('p_Pick_From_Container_ID: '|| p_Pick_From_Container_ID); 
            OPEN c_get_dest;
            FETCH c_get_dest INTO l_dest_id;
            CLOSE c_get_dest;
            
            debug1('l_dest_id: '|| l_dest_id);  
        --     I_MSG_DISPLAY := I_MSG_DISPLAY||' Dest '||l_dest_id;
            IF (t_container_count > 1 AND p_Pick_Type NOT IN ('CP', 'C')) OR (l_dest_id > 1 AND p_Pick_Type IN ('CP', 'C')) THEN
              -----------------------------------------------
              -- The bulk pick is a BulkGroup Pick
              -----------------------------------------------
              p_Dest_ID    := g_scp(p_facility_id, 'mixed_dest_id');
              p_Bulk_Group := 'TRUE';
            ELSE
              -----------------------------------------------
              -- It is a single Bulk Pick
              -----------------------------------------------
              OPEN Container_Dest_Id_Cursor;
              FETCH Container_Dest_Id_Cursor INTO p_Distro_Nbr, p_Dest_Id;
              CLOSE Container_Dest_Id_Cursor;
              
              p_Bulk_Group := 'FALSE';
            END IF;
          debug1('l_dest_id: '|| l_dest_id||'  p_Distro_Nbr: '||p_Distro_Nbr );    
            ----------------------------------------------------------------------
            -- Fetch the container location and the pick container qty
            ----------------------------------------------------------------------
            OPEN Cntr_Location_Generic_Cursor;
            FETCH Cntr_Location_Generic_Cursor INTO p_From_Location_Id;
            container_found := Cntr_Location_Generic_Cursor%FOUND;
            CLOSE Cntr_Location_Generic_Cursor;
             debug1('p_From_Location_Id: '|| p_From_Location_Id); 
   -- I_MSG_DISPLAY := I_MSG_DISPLAY||' Loc '||p_From_Location_Id;            
            IF (container_found = TRUE) THEN
              OPEN Cntr_Qty_Generic_Cursor;
              FETCH Cntr_Qty_Generic_Cursor INTO p_Pick_Container_Qty;
              container_found := Cntr_Qty_Generic_Cursor%FOUND;
         --     I_MSG_DISPLAY := I_MSG_DISPLAY||'Found 2   container_found';
              CLOSE Cntr_Qty_Generic_Cursor;
              debug1('p_Pick_Container_Qty: '|| p_Pick_Container_Qty); 
            END IF;
 
            ----------------------------------------------
            IF NOT ( container_found ) THEN
              ----------------------------------------------
              I_MSG_DISPLAY := I_MSG_DISPLAY||' - '|| p_facility_id||' - '|| p_Wave_Nbr||' - '||p_Distro_Nbr ||' - '||p_Pick_From_Container_ID ||' - '||l_Dest_Id ||' - '|| p_Pick_To_Container_Id ;
              debug1('I_MSG_DISPLAY: '|| I_MSG_DISPLAY);
              t_purge_status  :=     purge_directive (
                      facility_id_in            => p_facility_id,
                      wave_nbr_in               => p_Wave_Nbr,
                      distro_nbr_in             => p_Distro_Nbr,
                      pick_from_container_id_in => p_Pick_From_Container_ID,
                      dest_id_in                => p_Dest_Id,
                      pick_to_container_id_in   => p_Pick_To_Container_Id);  
              --purge_directive ( p_facility_id, p_Wave_Nbr, p_Distro_Nbr, p_Pick_From_Container_ID, l_Dest_Id, p_Pick_To_Container_Id );
                  debug1('t_purge_status: '|| t_purge_status);
              IF t_purge_status <> 'SUCCESS' THEN
                RAISE purge_failure;
              END IF;

              -------------------------------------------
            COMMIT;
            END IF;
            IF l_item_qty    > 1 AND p_Pick_Type IN ('CP', 'C') THEN
              p_Description := 'MULTI - SKU';
            ELSE
              -- End CR35261
              ----------------------------------------------
              OPEN Item_Master_Description_Cursor;
              FETCH Item_Master_Description_Cursor INTO p_Description;
              CLOSE Item_Master_Description_Cursor;
              ----------------------------------------------
            END IF;
            --
            -- CR35260 - If value_found and container_found then initialize
            --          l_Container_Qty = l_Pick_Container_Qty
            IF value_found AND container_found AND p_Pick_Type   <> 'C' THEN
              p_Container_Qty                                    := p_Pick_Container_Qty;
            ELSIF value_found AND container_found AND p_Pick_Type = 'C' THEN
              p_Container_Qty2                                   := p_Pick_Container_Qty;
            END IF;
            
         
            p_original_pick_qty := p_Pick_Container_Qty;
            --
             debug1('p_original_pick_qty 2 : '|| p_original_pick_qty); 
            p_pick_to_container_id2   := p_pick_to_container_id;
            p_pick_from_container_id2 := p_Pick_From_Container_ID;
         
            -----------------------------------------------------------------
            --UPDATE PICK_IN_PROGRESS FLAG TO 'Y' FOR PICK_FROM_CONTAINER_ID
            -----------------------------------------------------------------
            UPDATE PICK_DIRECTIVE
            SET PICK_IN_PROGRESS       = 'Y',
              USER_ID                  = p_USER
            WHERE FACILITY_ID          = p_facility_id
            AND PICK_FROM_CONTAINER_ID = p_Pick_From_Container_ID
            AND PICK_TYPE             IN ('B', 'BP','CP', 'C');
            COMMIT;
            ------------------------------------------------
          EXCEPTION
          WHEN inv_container THEN
            I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER', 'E');
            --I_MSG_DISPLAY := 'NSERR4: '||I_MSG_DISPLAY;
            -- Call_Msg_Window_P('INV_CONTAINER');
            -- RAISE  FORM_TRIGGER_FAILURE;
          WHEN inv_pick_type THEN
            I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NOT_IN_PICK', 'E');
            --I_MSG_DISPLAY := 'NSERR3: '||I_MSG_DISPLAY;
            -- Call_Msg_Window_P('NOT_IN_PICK');
            -- RAISE  FORM_TRIGGER_FAILURE;
          WHEN no_picks THEN
            I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'PICK_COMPLETE', 'E');
            --I_MSG_DISPLAY := 'NSERR2: '||I_MSG_DISPLAY;
            --Call_Msg_Window_P('PICK_COMPLETE');
            /* IF l_calling_block = 'TO_LOCATION' THEN
            Tr_Exit;
            ELSE
            RAISE FORM_TRIGGER_FAILURE;
            END IF;
            raise;*/
          WHEN purge_failure THEN
            I_MSG_DISPLAY :=   
                PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE');
            --I_MSG_DISPLAY := 'NSERR1: '|| I_MSG_DISPLAY||' 1 '||p_PM_START_LOCATION||' 2 '|| p_Wave_Nbr||' 3 '|| p_Distro_Nbr||' 4 '|| p_Pick_From_Container_ID||' 5 '||  l_Dest_Id||' 6 '||  p_Pick_To_Container_Id;
            PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Load_Screen',t_purge_status);
            --Call_Msg_Window_P('OPER_FAILURE');
            -- Log_Error_Roll(9999, 'Load_Screen ', t_purge_status);
            -- RAISE FORM_TRIGGER_FAILURE;
          WHEN OTHERS THEN
            PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Get_Pick_Directive', 'Pick To CID: ' || p_Pick_To_Container_ID || ' Wv: ' || TO_CHAR(p_Wave_Nbr, '90') || ' Zn: ' || p_zone || ' Typ: ' || p_Pick_Type ||
            /*
            ' D TS: ' || TO_CHAR(l_distro_ts, 'MM-DD HH24:MI:SS')|| */
            SQLERRM);
            PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
            I_MSG_DISPLAY :=  SQLERRM;
          END;

 -- --------------------------------
 -- unlock bulk type picks for the user.
 -- --------------------------------
 
 PROCEDURE unlock_bulk_type_picks (p_facility_id VARCHAR2, p_user VARCHAR2 ) IS
 BEGIN
 	-- CR35260 - Also unlock CP picks
	
	/*UPDATE pick_directive
	   SET pick_in_progress = 'N',
	       user_id = NULL
	 WHERE facility_id = :work.facility_id
	   AND user_id = :work.user
	   AND pick_type like 'B%';*/

	-- Start CR35261 - LCC
	UPDATE pick_directive
	   SET pick_in_progress = 'N',
	       user_id = NULL
	 WHERE facility_id = p_facility_id
	   AND user_id = p_user
	   AND (pick_type like 'B%' OR pick_type IN ('C', 'CP'))
	   AND pick_in_progress = 'Y';
	-- End CR35261 
	COMMIT;
 END;
 
  
 -- -------------------------
 --  Get Pick Directive
 -- -------------------------
 PROCEDURE GET_PICK_DIRECTIVE(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,    
    I_CONF_CONTAINER_ID        IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID         IN OUT VARCHAR2 ,
    I_CONTAINER_QTY            IN OUT NUMBER ,
    I_DESCRIPTION              IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID         IN OUT VARCHAR2 ,
    I_ITEM_ID                  IN OUT VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_REFERENCE_CODE           IN OUT VARCHAR2 ,
    I_BULK_GROUP               IN OUT VARCHAR2 ,
    I_CONTAINER_QTY2           IN OUT NUMBER ,
    I_DEST_ID                  IN OUT NUMBER ,
    I_DISTRO_NBR               IN OUT VARCHAR2 ,
    I_EMPTY_PRESSED            IN OUT VARCHAR2 ,
    I_FIRST_PICK_FLAG          IN OUT VARCHAR2 ,
    I_OLD_TO_CID               IN OUT VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN OUT NUMBER ,
    I_PICK_CONTAINER_QTY       IN OUT NUMBER ,    
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID01   IN OUT VARCHAR2 ,
    I_PICK_TYPE                IN OUT VARCHAR2 ,
    I_TO_LOC_ENTERED           IN OUT VARCHAR2 ,
    I_WAVE_NBR                 IN OUT NUMBER ,
    I_ZONE                     IN OUT VARCHAR2 ,  
    P_PM_NO_PICK_FLAG          IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG          IN OUT VARCHAR2 ,    
    I_Global_Intrlvg           IN OUT VARCHAR2,   
    I_GOTO_FIELD               IN OUT VARCHAR2 ,  
    I_Operations_Performed     IN NUMBER,    
    I_Start_Time               IN DATE,  
    I_Units_Processed          IN NUMBER,
    I_Containers_Processed     IN NUMBER,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_DISTRO_TS                IN DATE ,
    I_LABELED_RESERVE          IN VARCHAR2 ,    
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PROCESS_BY_AISLE         IN VARCHAR2 ,
    P_PM_PICK_TYPE             IN VARCHAR2 ,
    P_PM_START_LOCATION        IN VARCHAR2,
    P_PM_ALLOW_INTRLVG         IN VARCHAR2  )
IS
  /*****************************************************************************************
  * Author       Date         Defect#         Change Description
  *--------------------------------------------------------------------------
  *  L. Coronel  07/06/2007   CR35260         WMS Splitting: Accenture Change
  *                                           R3.1 - Process full pallet CPs as BP.
  *   L. Coronel       08/24/2007   CR35261       WMS Splitting: Accenture
  *                                               Change R3.2 Process Full pallet Cs
  *                                               as BPs
  *                                               1. Added 'C' to also fetch
  *                                               C picks from pick directive
  *                                               table.
  *                                               2. Fetch the C picks and
  *                                               determine if
  *                                               the C pick can be processed
  *                                               as BP. If not then bypass.
  *                                               3. Do not display LPN qty for
  *                                               full pallet C picks.
  * R.Macero     04/01/2008      CO43433          1 - Added a filter on
  *                                               location_id to
  *                                               ensure record fetched is on
  *                                               the same aisle as the
  *                                               starting location
  *                                               and disable the zone filter
  *                                               when process_by_aisle is Y.
  *                                               Otherwise zone filter is
  *                                               active and the location_id
  *                                               filter is disabled.
  *                                               Table alias on
  *                                               pick_directive
  *                                               necessary to optimize sub
  *                                               query.
  *   L. Coronel     04/08/2008   CO43433         2. MVDC - High Bay Hotel
  *                                               Capabilities: If
  *                                               interleaving is
  *                                               on and there are no more
  *                                               available putaway or picking
  *                                               task, display a new message.
  *   R.Macero         10/05/2012   CO95087       Modified code to allow
  *                                               full-pallet C picks to be
  *                                               processed in a multi-MLP
  *                                               location only if the location
  *                                               is a Shuttle Trailer location.
  *****************************************************************************************/
  --*Declarations*--
  -- CR35260 - Initializing value_found variable
  /*value_found               BOOLEAN;*/
  value_found BOOLEAN := FALSE;
  --
  dummy VARCHAR2(1);
  t_pick_order Pick_Directive.Pick_Order%TYPE;
  t_purge_status    VARCHAR2(200);
  t_container_count NUMBER(10);
  container_found   BOOLEAN;
  purge_failure     EXCEPTION;
  inv_container     EXCEPTION;
  inv_pick_type     EXCEPTION;
  no_picks          EXCEPTION;
  ------------------------------------------------------------------------------
  -- declare new variable.
  ------------------------------------------------------------------------------
  t_pick_found BOOLEAN;
  l_item_qty   NUMBER;
  l_item_id Pick_Directive.item_id%TYPE;
  l_dest_qty NUMBER;
  ------------------------------------------------------------
  -- Start CR35261
  t_mlp_in_location BOOLEAN;
  t_loc location.location_id%TYPE;
  t_dummy     VARCHAR2(1);
  t_mlp_check BOOLEAN;
  -- End CR35261
  -- CR35260 - Fetch CP picks along with other pick types.
  --         - Roll full pallet CPs to BP irrespective of item id.
  /* CURSOR Pick_Container_Cursor IS
  SELECT Wave_Nbr,
  Zone,
  Pick_Type,
  Pick_From_Container_ID,
  Item_ID,
  sum(Pick_Container_Qty,
  count(Pick_To_Container_ID)
  FROM Pick_Directive
  WHERE Facility_Id           = I_FACILITY_ID
  AND Pick_To_Container_Id  = I_PICK_TO_CONTAINER_ID
  AND NVL(pick_in_progress, 'N') <> 'Y'
  GROUP BY Pick_Type,
  Wave_Nbr,
  Zone,
  Pick_From_Container_ID
  Item_ID; */
  /* Start CO43433 LCoronel - 1 */
  CURSOR Pick_Container_Cursor
  IS
    SELECT PD.Wave_Nbr,
      PD.Zone,
      PD.Pick_Type,
      PD.Pick_From_Container_ID,
      SUM(PD.Pick_Container_Qty)    AS total_cont_qty,
      COUNT(PD.Pick_To_Container_ID)AS total_cont
    FROM Pick_Directive PD
    WHERE PD.Facility_Id               = I_FACILITY_ID
    AND PD.Pick_To_Container_Id        = I_PICK_TO_CONTAINER_ID
    AND NVL(PD.pick_in_progress, 'N') <> 'Y'
    AND ((I_PROCESS_BY_AISLE           = 'N'
    AND PD.zone                        = I_ZONE)
    OR (I_PROCESS_BY_AISLE             = 'Y'
    AND EXISTS
      (SELECT 'X'
      FROM container C
      WHERE C.facility_id = PD.facility_id
      AND C.container_id  = PD.pick_from_container_id
      AND C.location_id LIKE SUBSTR(P_PM_START_LOCATION,1,5)
        || '%'
      )))
    GROUP BY PD.Pick_Type,
      PD.Wave_Nbr,
      PD.Zone,
      PD.Pick_From_Container_ID;
    /* End CO43433 LCoronel - 1 */
    ------------------------------------------------------------
    -- CR35260 - Fetch CP picks along with other pick types.
    --         - Roll full pallet CPs to BP irrespective of item id.
    /* CURSOR Pick_Directive_Cursor IS
    SELECT Wave_Nbr,
    Zone,
    Pick_Type,
    Pick_To_Container_ID,
    Pick_From_Container_ID,
    Item_ID,
    sum(Pick_Container_Qty,
    count(Pick_To_Container_ID)
    FROM Pick_Directive
    WHERE Facility_Id    =  I_FACILITY_ID
    AND Wave_Nbr       =  I_WAVE_NBR
    AND Zone           =  I_ZONE
    AND Pick_Type      =  I_PICK_TYPE
    AND NVL(pick_in_progress, 'N') <> 'Y'
    GROUP BY Pick_Type,
    Wave_Nbr,
    Zone,
    Pick_From_Container_ID,
    Pick_To_Container_ID; */
    -- Start CR35261: LCC - added pick type C to the following cursors wherever applicable
    CURSOR Pick_Directive_Cursor
    IS
      SELECT PD.Wave_Nbr,
        PD.Zone,
        PD.Pick_Type,
        PD.Pick_To_Container_ID,
        PD.Pick_From_Container_ID,
        SUM(PD.Pick_Container_Qty)    AS total_cont_qty,
        COUNT(PD.Pick_To_Container_ID)AS total_cont
      FROM Pick_Directive PD
      WHERE PD.Facility_Id = I_FACILITY_ID
      AND PD.Wave_Nbr      = I_WAVE_NBR
        /* Start CO43433 LCoronel - 1 */
        /*AND Zone           =  I_ZONE*/
      AND ((I_PROCESS_BY_AISLE = 'N'
      AND PD.zone              = I_ZONE)
      OR (I_PROCESS_BY_AISLE   = 'Y'
      AND EXISTS
        (SELECT 'X'
        FROM container C
        WHERE C.facility_id = PD.facility_id
        AND C.container_id  = PD.pick_from_container_id
        AND C.location_id LIKE SUBSTR(P_PM_START_LOCATION,1,5)
          || '%'
        )))
        /* End CO43433 LCoronel - 1 */
        --AND Pick_Type      IN (P_PM_PICK_TYPE, 'CP')
      AND PD.Pick_Type                  IN (P_PM_PICK_TYPE, 'CP', 'C' )
      AND NVL(PD.pick_in_progress, 'N') <> 'Y'
      GROUP BY PD.Pick_Type,
        PD.Wave_Nbr,
        PD.Zone,
        PD.Pick_From_Container_ID,
        PD.Pick_To_Container_ID;
      -----------------------------------------------------------------
      -- CR35260 - Also fetch for CP picks
      /*CURSOR Container_Dest_Id_Cursor IS
      SELECT Distro_Nbr,
      Dest_ID
      FROM Pick_Directive
      WHERE Facility_ID            = I_FACILITY_ID
      AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
      AND Pick_Type      =  I_PICK_TYPE
      GROUP BY pick_from_container_id,
      dest_id,
      distro_nbr; */
      CURSOR Container_Dest_Id_Cursor
      IS
        SELECT Distro_Nbr,
          Dest_ID
        FROM Pick_Directive
        WHERE Facility_ID          = I_FACILITY_ID
        AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
          --AND Pick_Type      IN (I_PICK_TYPE, 'CP')
        AND Pick_Type IN (I_PICK_TYPE, 'CP', 'C')
        GROUP BY pick_from_container_id,
          dest_id,
          distro_nbr;
      -----------------------------------------------------------------
      CURSOR Container_Location_Cursor
      IS
        SELECT C.Location_Id,
          SUM(CI.Container_Qty)
        FROM Container_Item CI,
          Container C
        WHERE C.Facility_Id = I_FACILITY_ID
        AND CI.Facility_Id  = I_FACILITY_ID
        AND C.Container_Id  = I_PICK_FROM_CONTAINER_ID
        AND CI.Container_Id = C.Container_Id
        GROUP BY C.Location_Id,
          CI.Container_Qty;
      -----------------------------------------------------------------
      CURSOR Cntr_Location_Generic_Cursor
      IS
        SELECT Location_Id
        FROM Container
        WHERE Facility_Id = I_FACILITY_ID
        AND Container_Id  = I_PICK_FROM_CONTAINER_ID;
      -----------------------------------------------------------------
      -- CR35260 - Filter picked CP conts.
      /* CURSOR Cntr_Qty_Generic_Cursor IS
      SELECT SUM(CI.Container_Qty)
      FROM Container_Item CI, Container C
      WHERE C.Facility_Id          = I_FACILITY_ID
      AND CI.Facility_Id         = I_FACILITY_ID
      AND (C.Master_Container_Id  = I_PICK_FROM_CONTAINER_ID
      OR  C.Container_Id  = I_PICK_FROM_CONTAINER_ID)
      AND CI.Container_Id = C.Container_Id; */
      CURSOR Cntr_Qty_Generic_Cursor
      IS
        SELECT SUM(CI.Container_Qty)
        FROM Container_Item CI,
          Container C
        WHERE C.Facility_Id        = I_FACILITY_ID
        AND CI.Facility_Id         = I_FACILITY_ID
        AND (C.Master_Container_Id = I_PICK_FROM_CONTAINER_ID
        OR C.Container_Id          = I_PICK_FROM_CONTAINER_ID)
        AND container_status NOT  IN ('P','D')
        AND CI.Container_Id        = C.Container_Id;
      -----------------------------------------------------------------
      -- CR35260 - For multi-sku CP pallet, I_ITEM_ID will be "MULTI - SKU"
      /* CURSOR Item_Master_Description_Cursor IS
      SELECT Description
      FROM Item_Master
      WHERE Facility_Id = I_FACILITY_ID
      AND Item_Id     = I_ITEM_ID; */
      CURSOR Item_Master_Description_Cursor
      IS
        SELECT Description
        FROM Item_Master
        WHERE Facility_Id = I_FACILITY_ID
        AND Item_Id       = l_item_id;
      -----------------------------------------------------------------
      -- CR35260 - New cursor to fetch item id
      CURSOR get_item_id
      IS
        SELECT Item_id
        FROM pick_directive
        WHERE facility_id          = I_FACILITY_ID
        AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
          /*AND pick_type IN (I_PICK_TYPE, 'CP')*/
        AND pick_type IN (I_PICK_TYPE, 'CP', 'C');
      --
      -- CR35260 - New cursor to fetch no. of distinct item id in the pallet
      CURSOR get_item_qty
      IS
        SELECT COUNT (DISTINCT (item_id)) AS total_item_qty
        FROM pick_directive
        WHERE facility_id          = I_FACILITY_ID
        AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
          /*AND pick_type IN (I_PICK_TYPE, 'CP');*/
        AND pick_type IN (I_PICK_TYPE, 'CP', 'C');
      --
      -- CR35260 - New cursr to fetch no. of distinct destinations the pallet is going to
      CURSOR c_get_dest
      IS
        SELECT COUNT(DISTINCT dest_id)
        FROM Pick_Directive
        WHERE Facility_id          = I_FACILITY_ID
        AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
          /*AND Pick_Type IN (I_PICK_TYPE, 'CP')*/
        AND Pick_Type IN (I_PICK_TYPE, 'CP', 'C');
      --
      -- Start CR35261 - LCC
      CURSOR get_location (cid_in container.container_id%TYPE)
      IS
        SELECT location_id
        FROM container
        WHERE facility_id = I_FACILITY_ID
        AND container_id  = cid_in;
      CURSOR check_multi_mlp_loc(cid_in IN container.container_id%TYPE, loc_in IN location.location_id%TYPE)
      IS
        SELECT
          /*+ RULE */
          'X'
        FROM container
        WHERE facility_id        = I_FACILITY_ID
        AND (master_container_id = 'NONE'
        AND container_id        <> cid_in
        OR (master_container_id <> 'NONE'
        AND master_container_id <> cid_in))
        AND location_id          = loc_in;
      -- End CR35261
      -----------------------------------------------------------------
    BEGIN
      ---------------------------------------------------------
      -- Get wave, zone, pick_order, pick type, and distro time
      -- for the pick to container that was entered.
      -- Use for further picks.
      ---------------------------------------------------------
      IF I_FIRST_PICK_FLAG = 'Y' THEN
        -- CR35260 - Fetch CP picks along with other pick
        --         - In case of CP pick, determine if it can be processed as BP
        --         - If not then bypass it.
        /*OPEN Pick_Container_Cursor;
        FETCH Pick_Container_Cursor INTO I_WAVE_NBR,
        I_ZONE,
        I_PICK_TYPE,
        I_PICK_FROM_CONTAINER_ID,
        I_ITEM_ID,
        I_CONTAINER_QTY,
        t_container_count;
        value_found := Pick_Container_Cursor%FOUND;
        CLOSE Pick_Container_Cursor;*/
        FOR pick_cur_first IN Pick_Container_Cursor
        LOOP
          t_pick_found := FALSE;
          -- IF pick_cur_first.pick_type = 'CP' THEN
          IF pick_cur_first.pick_type IN ('CP', 'C') THEN
            t_pick_found := consolidate_picks( I_FACILITY_ID, NULL, pick_cur_first.pick_type, pick_cur_first.pick_from_container_id, 'hh_bulk_pk_across_wv_s', NULL );
          ELSE
            t_pick_found := TRUE;
          END IF;
          -- Start CR35261 - LCC
          t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(I_FACILITY_ID ,pick_cur_first.pick_from_container_id);
          
          OPEN get_location (pick_cur_first.pick_from_container_id);
          FETCH get_location INTO t_loc;
          CLOSE get_location;
          
          OPEN check_multi_mlp_loc (pick_cur_first.pick_from_container_id, t_loc);
          FETCH check_multi_mlp_loc INTO t_dummy;
          t_mlp_in_location := check_multi_mlp_loc%FOUND;
          CLOSE check_multi_mlp_loc;
          
          --IF t_pick_found THEN
          /* Start R.Macero CO95087 */
          IF (t_pick_found AND pick_cur_first.pick_type <> 'C') OR (t_pick_found AND pick_cur_first.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC( I_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
            /* End R.Macero CO95087 */
            IF pick_cur_first.pick_type = 'C' THEN
              I_CONTAINER_QTY          := '';
              I_CONTAINER_QTY2         := pick_cur_first.total_cont_qty;
            ELSE
              I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            END IF;
            value_found              := TRUE;
            I_WAVE_NBR               := pick_cur_first.wave_nbr;
            I_ZONE                   := pick_cur_first.zone;
            I_PICK_TYPE              := pick_cur_first.pick_type;
            I_PICK_FROM_CONTAINER_ID := pick_cur_first.pick_from_container_id;
            --I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            t_container_count := pick_cur_first.total_cont;
            
            OPEN get_item_id;
            FETCH get_item_id INTO l_item_id;
            CLOSE get_item_id;
            
            OPEN get_item_qty;
            FETCH get_item_qty INTO l_item_qty;
            CLOSE get_item_qty;
            
            IF l_item_qty > 1 THEN
              I_ITEM_ID  := 'MULTI - SKU';
            ELSE
              I_ITEM_ID := l_item_id;
            END IF;
            EXIT;
          END IF;
        END LOOP;
        --
        IF NOT(value_found) THEN
          RAISE inv_container;
          -- CR35260 - Consider CP as valid pick type.
          /*ELSIF I_PICK_TYPE NOT IN ('B','BR','BP') THEN*/
        ELSIF I_PICK_TYPE NOT IN ('B','BR','BP', 'CP', 'C') THEN
          --
          I_PICK_FROM_CONTAINER_ID := '';
          I_ITEM_ID                := '';
          I_CONTAINER_QTY          := '';
          RAISE inv_pick_type;
        END IF;
        ---------------------------------------------------------
        -- Set the reference code in the labor productivity table
        -- to equal the wave number and the zone.
        ---------------------------------------------------------
        I_REFERENCE_CODE := TO_CHAR(I_WAVE_NBR) || LPAD(I_ZONE, 4, ' ');
        --
        I_FIRST_PICK_FLAG := 'N';
      ELSE
         
        /*� GO_BLOCK('BULK_PICK') �*/
        I_GOTO_FIELD :=  'BULK_PICK';
        
        I_CONF_LOCATION_ID  := '';
        I_CONF_CONTAINER_ID := '';
        I_EMPTY_PRESSED     := 'N';
        I_TO_LOC_ENTERED    := 'N';
        I_OLD_TO_CID        := '';
        -- CR35260 - Fetch CP picks along with other pick
        --         - In case of CP pick, determine if it can be processed as BP
        --         - If not then bypass it.
        /* OPEN Pick_Directive_Cursor;
        FETCH Pick_Directive_Cursor INTO I_WAVE_NBR,
        I_ZONE,
        I_PICK_TYPE,
        I_PICK_TO_CONTAINER_ID,
        I_PICK_FROM_CONTAINER_ID,
        I_ITEM_ID,
        I_CONTAINER_QTY,
        t_container_count;
        value_found := Pick_Directive_Cursor%FOUND;
        CLOSE Pick_Directive_Cursor;*/
        FOR pick_cur_first IN Pick_Directive_Cursor
        LOOP
          t_pick_found := FALSE;
          -- IF pick_cur_first.pick_type = 'CP' THEN
          IF pick_cur_first.pick_type IN ('CP', 'C') THEN
            t_pick_found := consolidate_picks( I_FACILITY_ID, NULL, pick_cur_first.pick_type, pick_cur_first.pick_from_container_id, 'hh_bulk_pk_across_wv_s', NULL );
          ELSE
            t_pick_found := TRUE;
          END IF;
          -- Start CR35261 - LCC
          t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(I_FACILITY_ID ,pick_cur_first.pick_from_container_id);
          OPEN get_location (pick_cur_first.pick_from_container_id);
          FETCH get_location INTO t_loc;
          CLOSE get_location;
          
          OPEN check_multi_mlp_loc (pick_cur_first.pick_from_container_id,t_loc);
          FETCH check_multi_mlp_loc INTO t_dummy;
          t_mlp_in_location := check_multi_mlp_loc%FOUND;
          CLOSE check_multi_mlp_loc;
          
          --IF t_pick_found THEN
          /* Start R.Macero CO95087 */
          IF (t_pick_found AND pick_cur_first.pick_type <> 'C')OR( t_pick_found AND pick_cur_first.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC( I_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
            /* End R.Macero CO95087 */
            IF pick_cur_first.pick_type = 'C' THEN
              I_CONTAINER_QTY          := '';
              I_CONTAINER_QTY2         := pick_cur_first.total_cont_qty;
            ELSE
              I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            END IF;
            value_found              := TRUE;
            I_WAVE_NBR               := pick_cur_first.wave_nbr;
            I_ZONE                   := pick_cur_first.zone;
            I_PICK_TYPE              := pick_cur_first.pick_type;
            I_PICK_TO_CONTAINER_ID   := pick_cur_first.pick_to_container_id;
            I_PICK_FROM_CONTAINER_ID := pick_cur_first.pick_from_container_id;
            --I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            t_container_count := pick_cur_first.total_cont;
            
            OPEN get_item_id;
            FETCH get_item_id INTO l_item_id;
            CLOSE get_item_id;
            
            OPEN get_item_qty;
            FETCH get_item_qty INTO l_item_qty;
            CLOSE get_item_qty;
            
            IF l_item_qty > 1 THEN
              I_ITEM_ID  := 'MULTI - SKU';
            ELSE
              I_ITEM_ID := l_item_id;
            END IF;
            EXIT;
          END IF;
        END LOOP;
        --
        /* Start CO43433 LCoronel - 2 */
        -- No rows returned from Pick_Directive_Cursor
        IF NOT(value_found) THEN
          /*� Save_Labor_Prod �*/
          Save_Labor_Prod( p_facility_id => i_facility_id, p_labeled_picking => i_labeled_picking, p_user => I_user, p_Operations_Performed => I_operations_performed, 
            p_Start_Time => I_start_time, p_Units_Processed => I_units_processed, p_Containers_Processed => I_containers_processed, p_Reference_Code => I_reference_code, V_RETURN => v_return);
         
          /*� CLEAR_BLOCK(NO_VALIDATE) �*/          
          /*� PC3_Go_Field('BULK_PICK.PICK_TO_CONTAINER_ID') �*/
          I_GOTO_FIELD := 'BULK_PICK.PICK_TO_CONTAINER_ID';
          /*RAISE no_picks;*/
         Get_Error_Message(
                  V_RETURN           => V_RETURN,
                  I_MSG_DISPLAY      => I_MSG_DISPLAY,
                  I_FACILITY_ID      => I_FACILITY_ID ,
                  I_USER             => I_USER ,
                  I_PROCESS_BY_AISLE => I_PROCESS_BY_AISLE ,
                  P_PM_ALLOW_INTRLVG => p_pm_allow_intrlvg ,
                  P_PM_NO_PICK_FLAG  => P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG  => P_PM_NO_TASK_FLAG ,
                  P_Global_Intrlvg   => I_Global_Intrlvg);
		 RETURN;
          /* End CO43433 LCoronel - 2 */
        ELSE
          -- Start CR35261 - LCC
          IF I_PICK_TYPE         = 'C' THEN
            I_ORIGINAL_PICK_QTY := I_CONTAINER_QTY2;
          ELSE
            I_ORIGINAL_PICK_QTY := I_CONTAINER_QTY;
          END IF;
          -- End CR35261
          
          /*� PC3_Go_Field('BULK_PICK.CONF_LOCATION_ID') �*/
          I_GOTO_FIELD := 'BULK_PICK.CONF_LOCATION_ID';
        END IF; -- No value found Pick_Directive_Cursor
      END IF;   -- first flag
      -- CR35260 - Multi-dest logic for full pallet CP pick
      OPEN c_get_dest;
      FETCH c_get_dest INTO l_dest_qty;
      CLOSE c_get_dest;
      /*IF (t_container_count > 1) THEN*/
      -- Start CR35261 - LCC
      IF (t_container_count > 1 AND I_PICK_TYPE NOT IN ('CP','C')) OR (l_dest_qty > 1 AND I_PICK_TYPE IN ('CP', 'C')) THEN
        /*IF (t_container_count > 1 AND I_PICK_TYPE <> 'CP')
        OR (l_dest_qty > 1 AND I_PICK_TYPE = 'CP') THEN*/
        -- End CR35261
        -----------------------------------------------
        -- The bulk pick is a BulkGroup Pick
        -----------------------------------------------
        I_DEST_ID    := g_scp(I_FACILITY_ID, 'mixed_dest_id');
        I_BULK_GROUP := 'TRUE';
      ELSE
        -----------------------------------------------
        -- It is a single Bulk Pick
        -----------------------------------------------
        OPEN Container_Dest_Id_Cursor;
        FETCH Container_Dest_Id_Cursor INTO I_DISTRO_NBR, I_DEST_ID;
        CLOSE Container_Dest_Id_Cursor;
        I_BULK_GROUP := 'FALSE';
      END IF;
      -------------------------------------------------------------
      -- Fetch the container location and the pick container qty
      -------------------------------------------------------------
      IF (I_LABELED_RESERVE = 'N') THEN
        OPEN Container_Location_Cursor;
        FETCH Container_Location_Cursor INTO I_FROM_LOCATION_ID, I_PICK_CONTAINER_QTY;
        container_found := Container_Location_Cursor%FOUND;
        CLOSE Container_Location_Cursor;
      ELSE
        OPEN Cntr_Location_Generic_Cursor;
        FETCH Cntr_Location_Generic_Cursor INTO I_FROM_LOCATION_ID;
        container_found := Cntr_Location_Generic_Cursor%FOUND;
        CLOSE Cntr_Location_Generic_Cursor;
        
        IF (container_found = TRUE) THEN
          OPEN Cntr_Qty_Generic_Cursor;
          FETCH Cntr_Qty_Generic_Cursor INTO I_PICK_CONTAINER_QTY;
          container_found := Cntr_Qty_Generic_Cursor%FOUND;
          CLOSE Cntr_Qty_Generic_Cursor;
        END IF;
      END IF;
      ----------------------------------------------
      IF NOT ( container_found ) THEN
        -------------------------------------------
      --  I_MSG_DISPLAY := 'Fac: '|| I_FACILITY_ID || ' -> '||I_WAVE_NBR|| ' -> '|| I_DISTRO_NBR  || ' -> '|| I_PICK_FROM_CONTAINER_ID  || ' -> '|| I_DEST_ID  || ' -> '||  I_PICK_TO_CONTAINER_ID || ' -> ';
        t_purge_status    := purge_directive ( I_FACILITY_ID, I_WAVE_NBR, I_DISTRO_NBR, I_PICK_FROM_CONTAINER_ID, I_DEST_ID, I_PICK_TO_CONTAINER_ID );
        IF t_purge_status <> 'SUCCESS' THEN
          RAISE purge_failure;
        END IF;
        -------------------------------------------
        COMMIT;
      END IF;
      -- CR35260 - If CP pallet has multiple SKU then display
      --           "MULTI - SKU" on the item description field.
      /* OPEN Item_Master_Description_Cursor;
      FETCH Item_Master_Description_Cursor INTO I_DESCRIPTION;
      CLOSE Item_Master_Description_Cursor; */
      -- Start CR35261 - LCC
      /*IF l_item_qty > 1 AND I_PICK_TYPE = 'CP' THEN
      I_DESCRIPTION := 'MULTI - SKU';
      ELSE*/
      IF l_item_qty    > 1 AND I_PICK_TYPE IN ('CP', 'C') THEN
        I_DESCRIPTION := 'MULTI - SKU';
      ELSE
        -- End CR35261
        ----------------------------------------------
        OPEN Item_Master_Description_Cursor;
        FETCH Item_Master_Description_Cursor INTO I_DESCRIPTION;
        CLOSE Item_Master_Description_Cursor;
        ----------------------------------------------
      END IF;
      --
      -- CR35260 - If value_found and container_found then initialize
      --          I_CONTAINER_QTY = I_PICK_CONTAINER_QTY
      IF value_found AND container_found AND I_PICK_TYPE   <> 'C' THEN
        I_CONTAINER_QTY  := I_PICK_CONTAINER_QTY;
      ELSIF value_found AND container_found AND I_PICK_TYPE = 'C' THEN
        I_CONTAINER_QTY2 := I_PICK_CONTAINER_QTY;
      END IF;
      I_ORIGINAL_PICK_QTY := I_PICK_CONTAINER_QTY;
      --
      I_PICK_TO_CONTAINER_ID01   := I_PICK_TO_CONTAINER_ID;
      I_PICK_FROM_CONTAINER_ID01 := I_PICK_FROM_CONTAINER_ID;
      -----------------------------------------------------------------
      --UPDATE PICK_IN_PROGRESS FLAG TO 'Y' FOR PICK_FROM_CONTAINER_ID
      -----------------------------------------------------------------
      -- CR35260 - Also lock eligible full pallet CP pick
      /* UPDATE PICK_DIRECTIVE
      SET PICK_IN_PROGRESS = 'Y',
      USER_ID = I_USER
      WHERE FACILITY_ID = I_FACILITY_ID
      AND PICK_FROM_CONTAINER_ID = I_PICK_FROM_CONTAINER_ID
      AND PICK_TYPE IN ('B', 'BP'); */
      -- Start CR35261 - LCC
      UPDATE PICK_DIRECTIVE
      SET PICK_IN_PROGRESS       = 'Y',
        USER_ID                  = I_USER
      WHERE FACILITY_ID          = I_FACILITY_ID
      AND PICK_FROM_CONTAINER_ID = I_PICK_FROM_CONTAINER_ID
        --AND PICK_TYPE IN ('B', 'BP','CP');
      AND PICK_TYPE IN ('B', 'BP','CP', 'C');
      -- End CR35261
      COMMIT;
      ------------------------------------------------
    EXCEPTION
    WHEN inv_container THEN
      I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER');
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
      /*� Call_Msg_Window_P('INV_CONTAINER') �*/ 
     -- RAISE FORM_TRIGGER_FAILURE;
    WHEN inv_pick_type THEN
      I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NOT_IN_PICK');
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
      /*� Call_Msg_Window_P('NOT_IN_PICK') �       
          RAISE FORM_TRIGGER_FAILURE;*/
    WHEN no_picks THEN
      I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'PICK_COMPLETE');
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
      /*� Call_Msg_Window_P('PICK_COMPLETE') �*/       
     -- RAISE FORM_TRIGGER_FAILURE;
      
      -- CLEAR_BLOCK('NO_VALIDATE');
      /* Start CO43433 LCoronel - Added error handling for the new procedure
      get_error_message*/
   -- WHEN FORM_TRIGGER_FAILURE THEN
   --   RAISE;
      /* End CO43433 LCoronel */
    WHEN purge_failure THEN
      I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE');
      PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Load_Screen', t_purge_status);
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
      /*� Call_Msg_Window_P('OPER_FAILURE') �*/ 
      /*� Log_Error_Roll(9999, 'Load_Screen ', t_purge_status) �*/       
     -- RAISE FORM_TRIGGER_FAILURE;
    WHEN OTHERS THEN
      PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Get_Pick_Directive', 'Pick To CID: ' ||
      I_PICK_TO_CONTAINER_ID || ' Wv: ' || TO_CHAR(I_WAVE_NBR, '90') ||' Zn: ' || I_ZONE ||
      ' Typ: ' || I_PICK_TYPE || ' D TS: ' || TO_CHAR(I_DISTRO_TS, 'MM-DD HH24:MI:SS')||SQLERRM);
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
      /*� Log_Error(SQLCODE, 'Get_Pick_Directive', 'Pick To CID: ' ||
      I_PICK_TO_CONTAINER_ID ||
      ' Wv: ' ||
      TO_CHAR(I_WAVE_NBR, '90') ||
      ' Zn: ' || I_ZONE ||
      ' Typ: ' || I_PICK_TYPE ||
      ' D TS: ' ||
      TO_CHAR(I_DISTRO_TS, 'MM-DD HH24:MI:SS')||
      SQLERRM) �*/
       
 --     RAISE FORM_TRIGGER_FAILURE;
    END; 
 
 
 --------------
 PROCEDURE GET_PICK_DIRECTIVE2(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,      
    I_CONF_CONTAINER_ID        IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID         IN OUT VARCHAR2 ,
    I_CONTAINER_QTY            IN OUT NUMBER ,
    I_DESCRIPTION              IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID         IN OUT VARCHAR2 ,
    I_ITEM_ID                  IN OUT VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_REFERENCE_CODE           IN OUT VARCHAR2 ,
    I_BULK_GROUP               IN OUT VARCHAR2 ,
    I_CONTAINER_QTY2           IN OUT NUMBER ,
    I_DEST_ID                  IN OUT NUMBER ,
    I_DISTRO_NBR               IN OUT VARCHAR2 ,
    I_EMPTY_PRESSED            IN OUT VARCHAR2 ,
    I_FIRST_PICK_FLAG          IN OUT VARCHAR2 ,
    I_OLD_TO_CID               IN OUT VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN OUT NUMBER ,
    I_PICK_CONTAINER_QTY       IN OUT NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID01   IN OUT VARCHAR2 ,
    I_PICK_TYPE                IN OUT VARCHAR2 ,
    I_TO_LOC_ENTERED           IN OUT VARCHAR2 ,
    I_WAVE_NBR                 IN OUT NUMBER ,
    I_ZONE                     IN OUT VARCHAR2 ,     
    P_PM_NO_PICK_FLAG          IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG          IN OUT VARCHAR2,   
    I_Global_Intrlvg           IN OUT VARCHAR2 ,    
    I_GOTO_FIELD               IN OUT VARCHAR2 ,  
    I_Operations_Performed     IN NUMBER,
    I_Start_Time               IN DATE,  
    I_Units_Processed          IN NUMBER,
    I_Containers_Processed     IN NUMBER,  
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_DISTRO_TS                IN DATE ,
    I_LABELED_RESERVE          IN VARCHAR2 , 
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PROCESS_BY_AISLE         IN VARCHAR2 ,
    P_PM_PICK_TYPE             IN VARCHAR2 ,
    P_PM_PICK_TYPE_CP          IN VARCHAR2 ,
    P_PM_START_LOCATION        IN VARCHAR2 ,
    P_PM_ALLOW_INTRLVG         IN VARCHAR2, 
    P_PM_WAVE_NBR              IN NUMBER  )
IS
/*********************************************************************************************
*  Change History:                                                                
*    
*   Author           Date         Defect/CR#    Change Description      
*   -----------------------------------------------------------------------------------------
*   L. Coronel       07/06/2007   CR35260       WMS Splitting: Accenture Change
*																							  R3.1 Process Full pallet CPs as BPs
*   L. Coronel       08/24/2007   CR35261       WMS Splitting: Accenture
*                                               Change R3.2 Process Full pallet Cs
*                                               as BPs
*                                               1. Added 'C' to also fetch
*                                               C picks from pick directive
*                                               table.
*                                               2. Fetch the C picks and
*                                               determine if 
*                                               the C pick can be processed
*                                               as BP. If not then bypass.
*                                               3. Do not display LPN qty for
*                                               full pallet C picks.
* R.Macero     04/01/2008      CO43433          1 - Added a filter on
*                                               location_id to
*                                               ensure record fetched is on
*                                               the same aisle as the
*                                               starting location
*                                               and disable the zone filter
*                                               when process_by_aisle is Y.
*                                               Otherwise zone filter is
*                                               active and the location_id
*                                               filter is disabled.
*                                               Table alias on
*                                               pick_directive
*                                               necessary to optimize sub
*                                               query.
*   L. Coronel     04/08/2008   CO43433         2. MVDC - High Bay Hotel 
*                                               Capabilities: If
*                                               interleaving is
*                                               on and there are no more
*                                               available putaway or picking
*                                               task, display a new message.
*   R.Macero         10/05/2012   CO95087       Modified code to allow
*                                               full-pallet C picks to be
*                                               processed in a multi-MLP
*                                               location only if the location
*                                               is a Shuttle Trailer location.
*********************************************************************************************/
 

-- CR35260 - Initializing value_found variable
/*value_found               BOOLEAN;*/
value_found               BOOLEAN := FALSE;
--
dummy                     VARCHAR2(1);
t_pick_order              Pick_Directive.Pick_Order%TYPE;
t_purge_status            VARCHAR2(200);
t_container_count         NUMBER(10);
container_found           BOOLEAN;
purge_failure             EXCEPTION;     
inv_container             EXCEPTION;
inv_pick_type             EXCEPTION;
no_picks                  EXCEPTION;
	
---------------------------------------------------------------------------
-- variable declarations.
---------------------------------------------------------------------------

t_pick_found              BOOLEAN; 
l_item_id                 pick_directive.item_id%TYPE;
l_item_qty                NUMBER;
l_dest_qty                NUMBER;
	
------------------------------------------------------------
-- Start CR35261 
t_mlp_in_location         BOOLEAN;
t_loc                     location.location_id%TYPE;
t_dummy                   VARCHAR2(1);
t_mlp_check               BOOLEAN;
-- End CR35261


-- CR35260 - Fetch CP picks along with other picks.
--         - Roll Full pallet Cps to BP irrespective of item id.

/*CURSOR Pick_Container_Cursor IS
SELECT Wave_Nbr,
       Zone,
       Pick_Type,
       Pick_From_Container_ID,
       Item_ID,
       sum(Pick_Container_Qty,
       count(Pick_To_Container_ID) 
  FROM Pick_Directive
 WHERE Facility_Id           = I_FACILITY_ID
   AND Pick_To_Container_Id  = I_PICK_TO_CONTAINER_ID
   AND NVL(pick_in_progress, 'N') <> 'Y' 
GROUP BY Pick_Type,
       Wave_Nbr,
       Zone,
       Pick_From_Container_ID
       Item_ID; */

/* Start CO43433 LCoronel - 1 */
CURSOR Pick_Container_Cursor IS
SELECT PD.Wave_Nbr,
       PD.Zone,
       PD.Pick_Type,
       PD.Pick_From_Container_ID,
       sum(PD.Pick_Container_Qty) as total_cont_qty,
       count(PD.Pick_To_Container_ID)as total_cont
  FROM Pick_Directive PD
 WHERE PD.Facility_Id           = I_FACILITY_ID
   AND PD.Pick_To_Container_Id  = I_PICK_TO_CONTAINER_ID
   AND NVL(PD.pick_in_progress, 'N') <> 'Y' 
   AND ((I_PROCESS_BY_AISLE = 'N' AND
         PD.zone = I_ZONE) OR
        (I_PROCESS_BY_AISLE = 'Y' AND
         EXISTS (SELECT 'X'
                   FROM container C
                  WHERE C.facility_id = PD.facility_id
                    AND C.container_id = PD.pick_from_container_id
                    AND C.location_id LIKE
                        SUBSTR(P_PM_START_LOCATION,1,5) || '%')))
GROUP BY PD.Pick_Type,
       PD.Wave_Nbr,
       PD.Zone,
       PD.Pick_From_Container_ID;
/* End CO43433 LCoronel - 1 */
       
------------------------------------------------------------

-- CR35260 - Fetch CP picks along with other picks.
--         - Roll Full pallet Cps to BP irrespective of item id.

/*CURSOR Pick_Directive_Cursor IS
SELECT Wave_Nbr,
       Zone,
       Pick_Type,
       Pick_To_Container_ID,
       Pick_From_Container_ID,
       Item_ID,
       sum(Pick_Container_Qty,
       count(Pick_To_Container_ID) 
  FROM Pick_Directive
 WHERE Facility_Id    =  I_FACILITY_ID 
   AND Wave_Nbr       =  P_PM_WAVE_NBR
   AND Zone           =  I_ZONE
   AND Pick_Type      =  I_PICK_TYPE
   AND NVL(pick_in_progress, 'N') <> 'Y' 
GROUP BY Pick_Type,
       Wave_Nbr,
       Zone,
       Pick_From_Container_ID,
       Pick_To_Container_ID
       Item_ID; */

-- Start CR35261: LCC - added pick type C to the following cursors wherever applicable
CURSOR Pick_Directive_Cursor IS
SELECT PD.Wave_Nbr,
       PD.Zone,
       PD.Pick_Type,
       PD.Pick_To_Container_ID,
       PD.Pick_From_Container_ID,
       sum(PD.Pick_Container_Qty) as total_cont_qty,
       count(PD.Pick_To_Container_ID)as total_cont
  FROM Pick_Directive PD
 WHERE PD.Facility_Id    =  I_FACILITY_ID 
   AND PD.Wave_Nbr       =  P_PM_WAVE_NBR
   /* Start CO43433 LCoronel - 1 */
   /*AND Zone           =  I_ZONE*/
   AND ((I_PROCESS_BY_AISLE = 'N' AND
         PD.zone = I_ZONE) OR
        (I_PROCESS_BY_AISLE = 'Y' AND
         EXISTS (SELECT 'X'
                   FROM container C
                  WHERE C.facility_id = PD.facility_id
                    AND C.container_id = PD.pick_from_container_id
                    AND C.location_id LIKE
                        SUBSTR(P_PM_START_LOCATION,1,5) || '%')))
   /* End CO43433 LCoronel - 1 */
   --AND Pick_Type      IN (P_PM_PICK_TYPE, 'CP')
   AND PD.Pick_Type      IN (P_PM_PICK_TYPE_CP, P_PM_PICK_TYPE_CP, P_PM_PICK_TYPE)
   AND NVL(PD.pick_in_progress, 'N') <> 'Y' 
GROUP BY PD.Pick_Type,
       PD.Wave_Nbr,
       PD.Zone,
       PD.Pick_From_Container_ID,
       PD.Pick_To_Container_ID;
-----------------------------------------------------------------

-- CR35260 - Also look for CP picks

/*CURSOR Container_Dest_Id_Cursor IS
SELECT Distro_Nbr,
       Dest_ID
  FROM Pick_Directive
 WHERE Facility_ID            = I_FACILITY_ID
   AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
   AND Pick_Type      =  I_PICK_TYPE
GROUP BY pick_from_container_id,
       dest_id,
       distro_nbr;*/

CURSOR Container_Dest_Id_Cursor IS
SELECT Distro_Nbr,
       Dest_ID
  FROM Pick_Directive
 WHERE Facility_ID            = I_FACILITY_ID
   AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
   --AND Pick_Type      IN (I_PICK_TYPE, 'CP')
   AND Pick_Type      IN (I_PICK_TYPE, 'CP', 'C')
GROUP BY pick_from_container_id,
       dest_id,
       distro_nbr;
-----------------------------------------------------------------
CURSOR Container_Location_Cursor IS
SELECT C.Location_Id,
       SUM(CI.Container_Qty)
  FROM Container_Item CI, Container C
 WHERE C.Facility_Id   = I_FACILITY_ID
   AND CI.Facility_Id  = I_FACILITY_ID
   AND C.Container_Id  = I_PICK_FROM_CONTAINER_ID
   AND CI.Container_Id = C.Container_Id
GROUP BY C.Location_Id,
      CI.Container_Qty;
-----------------------------------------------------------------
CURSOR Cntr_Location_Generic_Cursor IS                                     
SELECT Location_Id
  FROM Container
 WHERE Facility_Id     = I_FACILITY_ID
   AND Container_Id    = I_PICK_FROM_CONTAINER_ID;
-----------------------------------------------------------------

-- CR35260 - Filter picked CP conts

/*CURSOR Cntr_Qty_Generic_Cursor IS
SELECT SUM(CI.Container_Qty)
  FROM Container_Item CI, Container C
 WHERE C.Facility_Id          = I_FACILITY_ID
   AND CI.Facility_Id         = I_FACILITY_ID
   AND (C.Master_Container_Id  = I_PICK_FROM_CONTAINER_ID
    OR  C.Container_Id  = I_PICK_FROM_CONTAINER_ID)
   AND CI.Container_Id = C.Container_Id; */

CURSOR Cntr_Qty_Generic_Cursor IS
SELECT SUM(CI.Container_Qty)
  FROM Container_Item CI, Container C
 WHERE C.Facility_Id          = I_FACILITY_ID
   AND CI.Facility_Id         = I_FACILITY_ID
   AND (C.Master_Container_Id  = I_PICK_FROM_CONTAINER_ID
    OR  C.Container_Id  = I_PICK_FROM_CONTAINER_ID)
   AND container_status NOT IN ('P','D')
   AND CI.Container_Id = C.Container_Id;
-----------------------------------------------------------------

-- CR35260 - In case of Multi-Sku CP pallet, 
--           I_ITEM_ID is "MULTI - SKU".

/*CURSOR Item_Master_Description_Cursor IS
SELECT Description
  FROM Item_Master
 WHERE Facility_Id = I_FACILITY_ID
   AND Item_Id     = I_ITEM_ID; */

CURSOR Item_Master_Description_Cursor IS
SELECT Description
  FROM Item_Master
 WHERE Facility_Id = I_FACILITY_ID
   AND Item_Id     = l_item_id;
-----------------------------------------------------------------
   
-- CR35260 - New cursor fetch item_id for the eligible pick
CURSOR get_item_id IS
SELECT item_id
 FROM pick_directive
WHERE facility_id = I_FACILITY_ID
  AND wave_nbr = I_WAVE_NBR
  AND pick_from_container_id =
      I_PICK_FROM_CONTAINER_ID
  /*AND pick_type IN (I_PICK_TYPE, 'CP')*/
  AND pick_type IN (I_PICK_TYPE, 'CP', 'C')
GROUP BY item_id;
--

-- CR35260 - New cursor to fetch distinct no of items in CP pallet.
CURSOR get_item_qty IS
SELECT COUNT (DISTINCT (item_id)) AS total_item_qty
FROM   pick_directive
WHERE facility_id = I_FACILITY_ID
  AND wave_nbr = I_WAVE_NBR
  AND pick_from_container_id =
      I_PICK_FROM_CONTAINER_ID
  /*AND pick_type IN (I_PICK_TYPE, 'CP')*/
  AND pick_type IN (I_PICK_TYPE, 'CP', 'C');
--  
 
-- CR35260 - New cursor to fetch distinct no of destinations CP pallet is going to.
CURSOR c_get_dest IS
SELECT COUNT(distinct dest_id)
  FROM Pick_Directive
 WHERE Facility_id = I_FACILITY_ID
   AND Wave_Nbr    = I_WAVE_NBR
   AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
   /*AND Pick_Type IN (I_PICK_TYPE, 'CP')*/
   AND Pick_Type IN (I_PICK_TYPE, 'CP', 'C');  
--      

-- Start CR35261 - LCC 
CURSOR get_location (cid_in container.container_id%TYPE) IS 
SELECT location_id
  FROM container
 WHERE facility_id = I_FACILITY_ID
   AND container_id = cid_in;

CURSOR check_multi_mlp_loc(cid_in IN container.container_id%TYPE, loc_in IN location.location_id%TYPE) IS
SELECT /*+ RULE */ 'X'
  FROM container
 WHERE facility_id = I_FACILITY_ID
   AND (master_container_id = 'NONE' AND container_id <> cid_in
        OR (master_container_id <> 'NONE' AND master_container_id <> cid_in))
   AND location_id = loc_in;
-- End CR35261   

-----------------------------------------------------------------
BEGIN
---------------------------------------------------------
-- Get wave, zone, pick_order, pick type, and distro time
-- for the pick to container that was entered.
-- Use for further picks.
---------------------------------------------------------

  IF I_FIRST_PICK_FLAG = 'Y' THEN
      
			 -- CR35260 - Also fetching the CP picks and determining if 
			 --           the CP pick can be processed as BP. If not then bypass.
      
      /*OPEN Pick_Container_Cursor;
        FETCH Pick_Container_Cursor INTO I_WAVE_NBR,
                                         I_ZONE,
                                         I_PICK_TYPE,
                                         I_PICK_FROM_CONTAINER_ID,
                                         I_ITEM_ID,
                                         I_CONTAINER_QTY,
                                         t_container_count;
        value_found := Pick_Container_Cursor%FOUND;

      CLOSE Pick_Container_Cursor;*/

     	FOR pick_cur_first IN Pick_Container_Cursor
     	LOOP
         t_pick_found := FALSE;
         -- IF pick_cur_first.pick_type = 'CP' THEN
         IF pick_cur_first.pick_type IN ('CP', 'C') THEN
		        t_pick_found := consolidate_picks(
		                                          I_FACILITY_ID,
			                                        pick_cur_first.wave_nbr,
		                                          pick_cur_first.pick_type,
		                                          pick_cur_first.pick_from_container_id,
		                                          'hh_bulk_pick_s',
		                                          NULL
		                                          );
		     ELSE
		        t_pick_found := TRUE;
		     END IF;
		      
         -- Start CR35261 - LCC 
         t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(I_FACILITY_ID  ,pick_cur_first.pick_from_container_id);
         
         OPEN get_location (pick_cur_first.pick_from_container_id);
         FETCH get_location INTO t_loc;
         CLOSE get_location;

         OPEN check_multi_mlp_loc (pick_cur_first.pick_from_container_id, t_loc);
         FETCH check_multi_mlp_loc INTO t_dummy;
         t_mlp_in_location := check_multi_mlp_loc%FOUND;
         CLOSE check_multi_mlp_loc;

         --IF t_pick_found THEN
         /* Start R.Macero CO95087 */
         IF (t_pick_found AND pick_cur_first.pick_type <> 'C') OR (t_pick_found AND pick_cur_first.pick_type = 'C' 
         AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC( I_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
         /* End R.Macero CO95087 */
 
            IF pick_cur_first.pick_type = 'C' THEN   
               I_CONTAINER_QTY := '';
	             I_CONTAINER_QTY2 := pick_cur_first.total_cont_qty;
            ELSE
	             I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            END IF;
              
            I_WAVE_NBR := pick_cur_first.wave_nbr;
            I_ZONE := pick_cur_first.zone;
            I_PICK_TYPE := pick_cur_first.pick_type;
            I_PICK_FROM_CONTAINER_ID :=
             pick_cur_first.pick_from_container_id;
            --I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            t_container_count := pick_cur_first.total_cont;  

				
				    OPEN get_item_id;
				    FETCH get_item_id INTO l_item_id;
				    CLOSE get_item_id;
				    
				    OPEN get_item_qty;
				    FETCH get_item_qty INTO l_item_qty;
				    CLOSE get_item_qty;
				    
				    IF l_item_qty > 1 THEN
				       I_ITEM_ID:= 'MULTI - SKU';
				    ELSE
				       I_ITEM_ID := l_item_id;
				    END IF;            
			      value_found := TRUE;
			      EXIT;	       		
		     END IF;
      END LOOP;
			-- End CR35261

      IF NOT(value_found) THEN
        RAISE inv_container;      
      -- CR36260 - Consider CP as a valid pick
      /*ELSIF I_PICK_TYPE NOT IN ('B','BR','BP', 'CP') THEN*/
      -- Start CR35261
      ELSIF I_PICK_TYPE NOT IN ('B','BR','BP', 'CP', 'C') THEN
      -- End CR35261
        I_PICK_FROM_CONTAINER_ID := '';
        I_ITEM_ID := '';
        I_CONTAINER_QTY := '';
        RAISE inv_pick_type;
      END IF;

      ---------------------------------------------------------
      -- Set the reference code in the labor productivity table
      -- to equal the wave number and the zone.
      ---------------------------------------------------------

     I_REFERENCE_CODE := TO_CHAR(I_WAVE_NBR) ||
                                        LPAD(I_ZONE, 4, ' ');
--
     I_FIRST_PICK_FLAG := 'N';

  ELSE -- first_pick_flag (N)
     I_GOTO_FIELD := 'BULK_PICK';
      /*� GO_BLOCK('BULK_PICK') �*/
      
     I_CONF_LOCATION_ID := '';
     I_CONF_CONTAINER_ID := '';
     I_EMPTY_PRESSED  := 'N';
     I_TO_LOC_ENTERED := 'N';
     I_OLD_TO_CID := '';

	 	 -- CR35260 - Also fetching the CP picks and determining if 
	   --           the CP pick can be processed as BP. If not then bypass.

     /*OPEN Pick_Directive_Cursor;
        FETCH Pick_Directive_Cursor INTO I_WAVE_NBR,
                                         I_ZONE,
                                         I_PICK_TYPE,
                                         I_PICK_TO_CONTAINER_ID,
                                         I_PICK_FROM_CONTAINER_ID,
                                         I_ITEM_ID,
                                         I_CONTAINER_QTY,
                                         t_container_count;
        value_found := Pick_Directive_Cursor%FOUND;
     CLOSE Pick_Directive_Cursor;*/

     FOR pick_cur_first IN Pick_Directive_Cursor
     LOOP

        t_pick_found := FALSE;
        -- Start CR35261 - LCC
        -- IF pick_cur_first.pick_type = 'CP' THEN
        IF pick_cur_first.pick_type IN ('CP', 'C') THEN
	         t_pick_found := consolidate_picks(
	                                           I_FACILITY_ID,
	                                           pick_cur_first.wave_nbr,
	  	                                       pick_cur_first.pick_type,
	                                           pick_cur_first.pick_from_container_id,
		                                         'hh_bulk_pick_s',
		                                         NULL);
	       ELSE
	          t_pick_found := TRUE;
	       END IF;
        
         -- Start CR35261 - LCC
           t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(I_FACILITY_ID  ,pick_cur_first.pick_from_container_id);
 
           OPEN get_location (pick_cur_first.pick_from_container_id);
           FETCH get_location INTO t_loc;
           CLOSE get_location;

           OPEN check_multi_mlp_loc (pick_cur_first.pick_from_container_id,t_loc);
           FETCH check_multi_mlp_loc INTO t_dummy;
           t_mlp_in_location := check_multi_mlp_loc%FOUND;
           CLOSE check_multi_mlp_loc;

           --IF t_pick_found THEN
           /* Start R.Macero CO95087 */
           IF (t_pick_found AND pick_cur_first.pick_type <> 'C')OR( t_pick_found AND pick_cur_first.pick_type = 'C'
           AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC( I_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
           /* End R.Macero CO95087 */
 
            IF pick_cur_first.pick_type = 'C' THEN   
               I_CONTAINER_QTY := '';
	             I_CONTAINER_QTY2 := pick_cur_first.total_cont_qty;
            ELSE
	             I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            END IF;
              
            I_WAVE_NBR := pick_cur_first.wave_nbr;
            I_ZONE := pick_cur_first.zone;
            I_PICK_TYPE := pick_cur_first.pick_type;
            I_PICK_FROM_CONTAINER_ID :=
             pick_cur_first.pick_from_container_id;
            --I_CONTAINER_QTY := pick_cur_first.total_cont_qty;
            t_container_count := pick_cur_first.total_cont;  
				 
				    OPEN get_item_id;
				    FETCH get_item_id INTO l_item_id;
				    CLOSE get_item_id;
				    
				    OPEN get_item_qty;
				    FETCH get_item_qty INTO l_item_qty;
				    CLOSE get_item_qty;
				          
				    IF l_item_qty > 1 THEN
				       I_ITEM_ID:= 'MULTI - SKU';
				    ELSE
				       I_ITEM_ID := l_item_id;
				    END IF;
	         	
	          value_found := TRUE;
	          EXIT;
         END IF;
      END LOOP;
     --
/* Start CO43433 LCoronel - 2 */
-- No rows returned from Pick_Directive_Cursor     
     IF NOT(value_found) THEN
         /*� Save_Labor_Prod �*/
        Save_Labor_Prod( p_facility_id => i_facility_id, p_labeled_picking => i_labeled_picking, p_user => I_user, p_Operations_Performed => I_operations_performed, 
            p_Start_Time => I_start_time, p_Units_Processed => I_units_processed, p_Containers_Processed => I_containers_processed, p_Reference_Code => I_reference_code, V_RETURN => v_return);
         
         /*� CLEAR_BLOCK(NO_VALIDATE) �*/
         /*� PC3_Go_Field('BULK_PICK.PICK_TO_CONTAINER_ID') �*/
         I_GOTO_FIELD := 'BULK_PICK.PICK_TO_CONTAINER_ID';
        /*RAISE no_picks;*/
        
         /*� Get_Error_Message �*/
          Get_Error_Message(
                  V_RETURN           => V_RETURN,
                  I_MSG_DISPLAY      => I_MSG_DISPLAY,
                  I_FACILITY_ID      => I_FACILITY_ID ,
                  I_USER             => I_USER ,
                  I_PROCESS_BY_AISLE => I_PROCESS_BY_AISLE ,
                  P_PM_ALLOW_INTRLVG => p_pm_allow_intrlvg ,
                  P_PM_NO_PICK_FLAG  => P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG  => P_PM_NO_TASK_FLAG ,
                  P_Global_Intrlvg   => I_Global_Intrlvg);
          RETURN;
        /* End CO43433 LCoronel - 2 */
     ELSE
        -- Start CR35261 - LCC
        IF I_PICK_TYPE = 'C' THEN
           I_ORIGINAL_PICK_QTY := I_CONTAINER_QTY2;
        ELSE
           I_ORIGINAL_PICK_QTY := I_CONTAINER_QTY;
        END IF;
        -- End CR35261
         /*� PC3_Go_Field('BULK_PICK.CONF_LOCATION_ID') �*/
         I_GOTO_FIELD := 'BULK_PICK.CONF_LOCATION_ID';
     END IF;  -- No value found Pick_Directive_Cursor 
  END IF; -- first flag

  -- CR35260 - Multi dest logic for Full pallet CP picks

  OPEN c_get_dest;
  FETCH c_get_dest INTO l_dest_qty;
  CLOSE c_get_dest;


  /*IF (t_container_count > 1) THEN*/
  -- Start CR35261 - LCC
  IF (t_container_count > 1 AND I_PICK_TYPE NOT IN ('CP','C')) OR (l_dest_qty > 1 AND I_PICK_TYPE IN ('CP', 'C')) THEN
  /*IF (t_container_count > 1 AND I_PICK_TYPE <> 'CP')  	
  OR (l_dest_qty > 1 AND I_PICK_TYPE = 'CP') THEN*/
  -- End CR35261
     -----------------------------------------------
     -- The bulk pick is a BulkGroup Pick
     -----------------------------------------------
     I_DEST_ID := g_scp(I_FACILITY_ID, 'mixed_dest_id');
     I_BULK_GROUP := 'TRUE';
  ELSE
     -----------------------------------------------
     -- It is a single Bulk Pick
     -----------------------------------------------
     OPEN Container_Dest_Id_Cursor;
     FETCH Container_Dest_Id_Cursor INTO I_DISTRO_NBR,
                                         I_DEST_ID;
     CLOSE Container_Dest_Id_Cursor;
     I_BULK_GROUP := 'FALSE';
  END IF;

  -------------------------------------------------------------
  -- Fetch the container location and the pick container qty
  -------------------------------------------------------------
  IF (I_LABELED_RESERVE = 'N') THEN                                
     OPEN Container_Location_Cursor;
       FETCH Container_Location_Cursor INTO I_FROM_LOCATION_ID,
                                            I_PICK_CONTAINER_QTY;
       container_found := Container_Location_Cursor%FOUND;
     CLOSE Container_Location_Cursor;
  ELSE
     OPEN Cntr_Location_Generic_Cursor;
     FETCH Cntr_Location_Generic_Cursor INTO I_FROM_LOCATION_ID;
     container_found := Cntr_Location_Generic_Cursor%FOUND;
     CLOSE Cntr_Location_Generic_Cursor;

     IF (container_found = TRUE) THEN
        OPEN Cntr_Qty_Generic_Cursor;
          FETCH Cntr_Qty_Generic_Cursor INTO I_PICK_CONTAINER_QTY;
          container_found := Cntr_Qty_Generic_Cursor%FOUND;
        CLOSE Cntr_Qty_Generic_Cursor;
     END IF;
  END IF;
  ----------------------------------------------
  IF NOT ( container_found ) THEN
     -------------------------------------------

     t_purge_status := purge_directive
                      ( I_FACILITY_ID,
                        I_WAVE_NBR,
                        I_DISTRO_NBR,
                        I_PICK_FROM_CONTAINER_ID,
                        I_DEST_ID,
                        I_PICK_TO_CONTAINER_ID );
     IF t_purge_status <> 'SUCCESS' THEN
        RAISE purge_failure;
     END IF;
     -------------------------------------------
     COMMIT;
  END IF;

 -- CR35260 - If Full CP pallet has multiple SKus then 
 --           display "MULTI - SKU" in the item description field.

  -- Start CR35261 - LCC
  /*IF l_item_qty > 1 AND I_PICK_TYPE = 'CP' THEN 
     I_DESCRIPTION := 'MULTI - SKU';
  ELSE*/
  IF l_item_qty > 1 AND I_PICK_TYPE IN ('CP', 'C') THEN 
     I_DESCRIPTION := 'MULTI - SKU';
  ELSE
  -- End CR35261
  
		  OPEN Item_Master_Description_Cursor;
		  FETCH Item_Master_Description_Cursor INTO I_DESCRIPTION;
		  CLOSE Item_Master_Description_Cursor;
  END IF;
  I_PICK_TO_CONTAINER_ID01 := I_PICK_TO_CONTAINER_ID;
  I_PICK_FROM_CONTAINER_ID01 := I_PICK_FROM_CONTAINER_ID;


  -----------------------------------------------------------------
  --UPDATE PICK_IN_PROGRESS FLAG TO 'Y' FOR PICK_FROM_CONTAINER_ID
  -----------------------------------------------------------------
 
 -- CR35260 - Also lock eligible Full pallet CP picks
 
 /*UPDATE PICK_DIRECTIVE
    SET PICK_IN_PROGRESS = 'Y',
        USER_ID = I_USER
  WHERE FACILITY_ID = I_FACILITY_ID
    AND PICK_FROM_CONTAINER_ID = I_PICK_FROM_CONTAINER_ID
    AND PICK_TYPE IN ('B', 'BP'); */

 -- Start CR35261 - LCC
 UPDATE PICK_DIRECTIVE
    SET PICK_IN_PROGRESS = 'Y',
        USER_ID = I_USER
  WHERE FACILITY_ID = I_FACILITY_ID
    AND PICK_FROM_CONTAINER_ID = I_PICK_FROM_CONTAINER_ID
    --AND PICK_TYPE IN ('B', 'BP','CP');
    AND PICK_TYPE IN ('B', 'BP','CP', 'C');
 -- End CR35261

  COMMIT;
  
------------------------------------------------
EXCEPTION
WHEN inv_container THEN
 /*� Call_Msg_Window_P('INV_CONTAINER') �*/ 
  I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN); 
--  RAISE FRM_TRIGGER_FAILURE;
WHEN inv_pick_type THEN
  /*� Call_Msg_Window_P('NOT_IN_PICK') �*/
  I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NOT_IN_PICK');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
 -- RAISE FRM_TRIGGER_FAILURE;
WHEN no_picks THEN
  /*� Call_Msg_Window_P('PICK_COMPLETE') �*/
  I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'PICK_COMPLETE');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
-- RAISE FRM_TRIGGER_FAILURE;
/* Start CO43433 LCoronel - Added error handling for the new procedure
 get_error_message*/
--WHEN FRM_TRIGGER_FAILURE THEN
-- RAISE;
/* End CO43433 LCoronel */
WHEN purge_failure THEN
   /*� Call_Msg_Window_P('OPER_FAILURE') �*/ 
   /*� Log_Error_Roll(9999, 'Load_Screen ', t_purge_status) �*/ 
  I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE');
  PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Load_Screen', t_purge_status);
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);  
-- RAISE FRM_TRIGGER_FAILURE;
WHEN OTHERS THEN 
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Get_Pick_Directive2', 'Pick To CID: ' ||
      I_PICK_TO_CONTAINER_ID || ' Wv: ' || TO_CHAR(I_WAVE_NBR, '90') ||' Zn: ' || I_ZONE ||
      ' Typ: ' || I_PICK_TYPE || ' D TS: ' || TO_CHAR(I_DISTRO_TS, 'MM-DD HH24:MI:SS')||SQLERRM);
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);                                
-- RAISE FRM_TRIGGER_FAILURE;
END;
 --------------
 
 
 FUNCTION check_wave_status (p_facility_id varchar2, p_wave_nbr varchar2) RETURN VARCHAR2 IS
Cursor C_CHECK_WAVE IS
    SELECT wave_status
      FROM Wave
     WHERE facility_id = p_facility_id
       AND wave_nbr    = p_Wave_Nbr;
       
 t_wave_status        	Wave.wave_status%TYPE;
 BEGIN
 OPEN C_CHECK_WAVE;
   FETCH C_CHECK_WAVE INTO t_wave_status;
   CLOSE C_CHECK_WAVE;
   
   RETURN t_wave_status;
 END; 
 -- 
PROCEDURE GET_GENERIC_PICK_DIRECTIVE2 (
    p_facility_id            IN VARCHAR2,
    P_PM_TASK_QUEUE_IND      IN VARCHAR2 ,
    P_PM_WAVE_NBR            IN NUMBER,
    P_PM_ROWID               IN VARCHAR2 ,
    P_DISTRO_TS              IN DATE  ,   
    p_PROCESS_BY_AISLE       IN VARCHAR2,
	p_PM_Pick_Type_C         IN VARCHAR2,
	p_PM_Pick_Type_CP        IN VARCHAR2,
    p_PM_Pick_Type           IN VARCHAR2,
    p_PM_START_LOCATION      IN OUT VARCHAR2,
    p_Labeled_Picking        IN VARCHAR2,
    p_USER                   IN VARCHAR2, 
    P_PM_ALLOW_INTRLVG       IN VARCHAR2,     
    p_Operations_Performed   IN  NUMBER,
    p_Start_Time             IN DATE,
    p_Units_Processed        IN NUMBER,    
    p_Containers_Processed   IN NUMBER,
    p_Pick_From_Container_ID IN OUT VARCHAR2,
    p_Pick_From_Container_ID2 IN OUT VARCHAR2,
    p_PM_Ups_Group_Ind       IN OUT VARCHAR2,
    p_pick_type              IN OUT VARCHAR2,
    p_Generic_To_Container   IN OUT VARCHAR2,
    p_first_pick_flag        IN OUT VARCHAR2,
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    p_pick_to_container_id   IN OUT VARCHAR2,
    p_pick_to_container_id2   IN OUT VARCHAR2,
    p_zone                   IN OUT VARCHAR2,
    p_container_qty          IN OUT NUMBER,
    p_reference_code         IN OUT VARCHAR2,
    p_Container_Qty2         IN OUT NUMBER,
    p_Conf_Location_ID       IN OUT VARCHAR2,
    p_Conf_Container_ID      IN OUT VARCHAR2,
    p_Empty_Pressed          IN OUT VARCHAR2,
    p_To_Loc_Entered         IN OUT VARCHAR2,
    p_old_to_cid             IN OUT VARCHAR2,
    p_wave_nbr               IN OUT NUMBER,
    p_Bulk_Group             IN OUT VARCHAR2,
    p_original_pick_qty      IN OUT VARCHAR2,
    p_Distro_Nbr             IN OUT VARCHAR2,
    p_Pick_Container_Qty     IN OUT NUMBER,
    p_Description            IN OUT VARCHAR2,
    p_From_Location_Id       IN OUT VARCHAR2,
    p_goto_field             IN OUT VARCHAR2,
    P_ITEM_ID                IN OUT VARCHAR2 ,
    P_DEST_ID                IN OUT NUMBER  ,     
    P_PM_NO_PICK_FLAG        IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG        IN OUT VARCHAR2 ,   
    P_Global_Intrlvg         IN OUT VARCHAR2
 )
IS
  /*********************************************************************************************
  *  Change History:
  *
  *   Author           Date         Defect/CR#    Change Description
  *   ------------------------------------------------------------------------------------------
  *   L. Coronel       07/06/2007   CR35260       WMS Splitting: Accenture Change
  *                         R3.1 Process Full pallet CPs as BPs
  *   L. Coronel       08/28/2007   CR35261       WMS Splitting: Accenture
  *                                               Change R3.2 Process Full pallet Cs
  *                                               as BPs
  *                                               1. Added 'C' to also fetch
  *                                               C picks from pick directive
  *                                               table.
  *                                               2. Fetch the C picks and
  *                                               determine if the C pick can
  *                                               be processed as BP. If not
  *                                               then bypass.
  *                                               3. Do not display LPN qty for
  *                                               full pallet C picks.
  * R.Macero     04/01/2008      CO43433          1 - Added a filter on
  *                                               location_id to ensure record
  *                                               fetched is on the same aisle
  *                                               as the starting location
  *                                               and disable the zone filter
  *                                               when process_by_aisle is Y.
  *                                               Otherwise zone filter is
  *                                               active and the location_id
  *                                               filter is disabled.
  *   L.Coronel        04/08/2008   CO43433       2. MVDC - High Bay Hotel
  *                                               Capabilities: If
  *                                               interleaving is
  *                                               on and there are no more
  *                                               available putaway or picking
  *                                               task, display a new message.
  *   R.Macero         12/26/2011   CO80290       Modified Pick_Container_TQ_Cursor cursor.
  *                                               Removed RULE hint which no longer
  *                                               improves performance in Oracle 11g.
  *   R.Macero         10/05/2012   CO95087       Modified code to allow
  *                                               full-pallet C picks to be
  *                                               processed in a multi-MLP
  *                                               location only if the location
  *                                               is a Shuttle Trailer location.
  *********************************************************************************************/
  -- CR35260 - Initializing value_found variable
  /*value_found               BOOLEAN;*/
 value_found BOOLEAN := FALSE;
t_dummy     VARCHAR2(1);
t_pick_order Pick_Directive.Pick_Order%TYPE;
t_purge_status    VARCHAR2(200);
t_container_count NUMBER(10);
t_pick_to_container_id Container.Container_Id%TYPE;
container_found BOOLEAN;
t_location_id Container.Location_Id%TYPE;
t_Zone Zone.Zone%TYPE;
purge_failure EXCEPTION;
inv_container EXCEPTION;
inv_pick_type EXCEPTION;
no_picks      EXCEPTION;
  ---------------------------------------------------------------------------
  -- variable declarations.
  ---------------------------------------------------------------------------
  t_pick_found BOOLEAN;
  l_item_id pick_directive.item_id%TYPE;
  l_item_qty NUMBER;
  l_dest_qty NUMBER;
  -- Start CR35261 - LCC
  t_mlp_in_location BOOLEAN;
  t_loc location.location_id%TYPE;
  t_mlp_check BOOLEAN;
  -- End CR35261
  -----------------------------------------------------------------------
  CURSOR V_Generic_Cntr_Cursor
  IS
    SELECT 'X'
    FROM SYS.DUAL
    WHERE EXISTS
      (SELECT container_id
      FROM Container
      WHERE Facility_ID = P_FACILITY_ID
      AND Container_Id  = P_GENERIC_TO_CONTAINER
      );
  ------------------------------------------------------------------------
  -- CR35260 - Fetch CP picks along with other picks.
  --         - Roll Full pallet Cps to BP irrespective of item id.
  /* CURSOR Pick_Container_Cursor IS
  SELECT pd.Pick_To_Container_Id,
  pd.Wave_Nbr,
  pd.Zone,
  pd.Pick_Type,
  pd.Pick_From_Container_ID,
  Item_ID,
  c.Location_ID,
  sum(Pick_Container_Qty,
  count(Pick_To_Container_ID)
  FROM Pick_Directive pd, Container c, Location L
  WHERE pd.Facility_Id              = P_FACILITY_ID
  AND pd.Wave_Nbr                 = P_PM_WAVE_NBR
  AND pd.Pick_Type                = P_PM_PICK_TYPE
  AND pd.Pick_From_Container_Id   = c.Container_Id
  AND pd.Facility_Id              = c.Facility_Id
  AND c.Location_Id              >= P_PM_START_LOCATION
  AND NVL(pd.pick_in_progress, 'N') <> 'Y'
  AND L.Facility_Id               = C.Facility_Id
  AND L.Location_Id               = C.Location_Id
  -- BEGIN DEFECT000394496
  AND EXISTS (SELECT 'X'
  FROM Location l
  WHERE l.Facility_Id = c.Facility_Id
  AND l.Location_Id = c.Location_Id
  AND l.Cycle_Count_Status <> 'MM'
  AND l.Location_Status <> 'OUT-SERVICE')
  -- END DEFECT000394496
  GROUP BY pd.Pick_Type,
  pd.Wave_Nbr,
  pd.Zone,
  pd.Pick_From_Container_ID,
  pd.Pick_To_Container_ID,
  pd.Item_ID,
  c.Location_Id,
  l.zone
  ORDER BY Decode(L.Zone, t_Zone, 1, 2), c.Location_Id; */
  -- Start CR35261 - LCC
  CURSOR Pick_Container_Cursor
  IS
    SELECT
      /*+ rule */
      pd.Pick_To_Container_Id,
      pd.Wave_Nbr,
      pd.Zone,
      pd.Pick_Type,
      pd.Pick_From_Container_ID,
      c.Location_ID,
      SUM(Pick_Container_Qty)    AS total_cont_qty,
      COUNT(Pick_To_Container_ID)AS total_cont
    FROM Pick_Directive pd,
      Container c,
      Location L
    WHERE pd.Facility_Id = P_FACILITY_ID
    AND pd.Wave_Nbr      = P_PM_WAVE_NBR
      /*AND pd.Pick_Type                IN ('CP', P_PM_PICK_TYPE)*/
    AND pd.Pick_Type             IN (p_PM_Pick_Type_CP, p_PM_Pick_Type_C, p_PM_Pick_Type)
    AND pd.Pick_From_Container_Id = c.Container_Id
    AND pd.Facility_Id            = c.Facility_Id
    AND c.Location_Id            >= P_PM_START_LOCATION
      /* Start CO43433 LCoronel - 1 */
    AND ((P_PROCESS_BY_AISLE = 'N')
    OR (P_PROCESS_BY_AISLE   = 'Y'
    AND C.location_id LIKE SUBSTR(P_PM_START_LOCATION,1,5)      || '%'))
      /* End CO43433 LCoronel - 1 */
    AND NVL(pd.pick_in_progress, 'N') <> 'Y'
    AND L.Facility_Id                  = C.Facility_Id
    AND L.Location_Id                  = C.Location_Id
    AND EXISTS
      (SELECT 'X'
      FROM Location l
      WHERE l.Facility_Id       = c.Facility_Id
      AND l.Location_Id         = c.Location_Id
      AND l.Cycle_Count_Status <> 'MM'
      AND l.Location_Status    <> 'OUT-SERVICE'
      )
  GROUP BY pd.Pick_Type,
    pd.Wave_Nbr,
    pd.Zone,
    pd.Pick_From_Container_ID,
    pd.Pick_To_Container_ID,
    c.Location_Id,
    l.zone
  ORDER BY DECODE(L.Zone, t_Zone, 1, 2),
    c.Location_Id;
  -- End CR35261
  ------------------------------------------------------------------------
  -- CR35260 - Fetch CP picks along with other picks.
  --         - Roll Full pallet Cps to BP irrespective of item id.
  /*CURSOR Pick_Container_TQ_Cursor IS
  SELECT Pick_To_Container_Id,
  pd.Wave_Nbr,
  Zone,
  pd.Pick_Type,
  pd.Pick_From_Container_ID,
  pd.Item_ID,
  sum(Pick_Container_Qty,
  count(Pick_To_Container_ID)
  c.location_id
  FROM Pick_Directive pd, Container c
  WHERE pd.Rowid = P_PM_ROWID
  AND pd.Facility_Id = P_FACILITY_ID
  AND NVL(pd.pick_in_progress, 'N') <> 'Y'
  AND pd.Pick_From_Container_Id   = c.Container_Id
  AND pd.Facility_Id              = c.Facility_Id
  GROUP BY pd.Pick_Type,
  pd.Wave_Nbr,
  pd.Zone,
  pd.Pick_From_Container_ID,
  pd.Pick_To_Container_ID,
  pd.Item_ID,
  c.location_id; */
  CURSOR Pick_Container_TQ_Cursor
  IS
    /* Start CO80290 R.Macero */
    SELECT Pick_To_Container_Id,
      /* End CO80290 R.Macero */
      pd.Wave_Nbr,
      Zone,
      pd.Pick_Type,
      pd.Pick_From_Container_ID,
      SUM(Pick_Container_Qty)    AS total_cont_qty,
      COUNT(Pick_To_Container_ID)AS total_cont,
      c.location_id
    FROM Pick_Directive pd,
      Container c
    WHERE pd.Rowid                     = P_PM_ROWID
    AND pd.Facility_Id                 = P_FACILITY_ID
    AND NVL(pd.pick_in_progress, 'N') <> 'Y'
    AND pd.Pick_From_Container_Id      = c.Container_Id
    AND pd.Facility_Id                 = c.Facility_Id
    GROUP BY pd.Pick_Type,
      pd.Wave_Nbr,
      pd.Zone,
      pd.Pick_From_Container_ID,
      pd.Pick_To_Container_ID,
      c.location_id;
  ------------------------------------------------------------------------
  -- CR35260 - Fetch CP picks along with other picks.
  --         - Roll Full pallet Cps to BP irrespective of item id.
  /*CURSOR Pick_Directive_Cursor IS
  SELECT pd.Pick_To_Container_ID,
  pd.Pick_From_Container_ID,
  pd.Wave_Nbr,
  Zone,
  pd.Pick_Type,
  Item_ID,
  c.Location_ID,
  sum(Pick_Container_Qty,
  count(Pick_To_Container_ID)
  FROM Pick_Directive pd, Container c
  WHERE pd.Facility_Id                            = P_FACILITY_ID
  AND pd.Wave_Nbr                               = P_WAVE_NBR
  AND pd.Zone                                   = P_ZONE
  AND NVL(pd.pick_in_progress, 'N')             <> 'Y'
  AND pd.Pick_Type                        = :Work_Local_Pick_Type
  AND pd.Pick_From_Container_Id                 = c.Container_id
  AND pd.Pick_From_Container_Id                 = c.Container_id
  AND pd.Facility_Id                            = c.Facility_id
  AND c.Location_Id                            >= P_PM_START_LOCATION
  -- BEGIN DEFECT000394496
  AND EXISTS (SELECT 'X'
  FROM Location l
  WHERE l.Facility_Id = c.Facility_Id
  AND l.Location_Id = c.Location_Id
  AND l.Cycle_Count_Status <> 'MM'
  AND l.Location_Status <> 'OUT-SERVICE')
  -- END DEFECT000394496
  GROUP BY pd.Pick_Type,
  pd.Wave_Nbr,
  pd.Zone,
  pd.Pick_From_Container_ID,
  pd.Pick_To_Container_ID,
  pd.Item_ID,
  pd.Pick_Container_Qty,
  c.Location_Id
  ORDER BY c.Location_Id; */
  -- Start CR35261 - LCC
  CURSOR Pick_Directive_Cursor
  IS
    SELECT
      /*+ rule */
      pd.Pick_To_Container_ID,
      pd.Pick_From_Container_ID,
      pd.Wave_Nbr,
      Zone,
      pd.Pick_Type,
      c.Location_ID,
      SUM(pd.Pick_Container_Qty)    AS total_cont_qty,
      COUNT(pd.Pick_To_Container_ID)AS total_cont
    FROM Pick_Directive pd,
      Container c
    WHERE pd.Facility_Id = P_FACILITY_ID
    AND pd.Wave_Nbr      = P_PM_WAVE_NBR
      /* Start CO43433 LCoronel - 1 */
      /*AND pd.Zone                                   = P_ZONE*/
    AND ((P_PROCESS_BY_AISLE = 'N'    AND pd.zone              = P_ZONE)
      OR (P_PROCESS_BY_AISLE = 'Y'    AND C.location_id LIKE SUBSTR(P_PM_START_LOCATION,1,5)      || '%'))
      /* End CO43433 LCoronel - 1 */
    AND NVL(pd.pick_in_progress, 'N') <> 'Y'
      /*AND pd.Pick_Type                              IN ('CP', P_PM_PICK_TYPE)*/
    AND pd.Pick_Type             IN (p_PM_Pick_Type_CP, p_PM_Pick_Type_C, p_PM_Pick_Type)
    AND pd.Pick_From_Container_Id = c.Container_id
    AND pd.Pick_From_Container_Id = c.Container_id
    AND pd.Facility_Id            = c.Facility_id
    AND c.Location_Id            >= P_PM_START_LOCATION
    AND EXISTS
      (SELECT 'X'
      FROM Location l
      WHERE l.Facility_Id       = c.Facility_Id
      AND l.Location_Id         = c.Location_Id
      AND l.Cycle_Count_Status <> 'MM'
      AND l.Location_Status    <> 'OUT-SERVICE'
      )
  GROUP BY pd.Pick_Type,
    pd.Wave_Nbr,
    pd.Zone,
    pd.Pick_From_Container_ID,
    pd.Pick_To_Container_ID,
    c.Location_Id
  ORDER BY c.Location_Id;
  -----------------------------------------
  -- CR35260 - Also look for CP picks
  /*CURSOR Container_Dest_Id_Cursor IS
  SELECT Distro_Nbr,
  Dest_ID
  FROM Pick_Directive
  WHERE Facility_ID            = P_FACILITY_ID
  AND Pick_From_Container_ID = P_PICK_FROM_CONTAINER_ID
  AND Pick_Type              = P_PICK_TYPE
  GROUP BY pick_from_container_id,
  dest_id,
  distro_nbr; */
  CURSOR Container_Dest_Id_Cursor
  IS
    SELECT Distro_Nbr,
      Dest_ID
    FROM Pick_Directive
    WHERE Facility_ID          = P_FACILITY_ID
    AND Pick_From_Container_ID = P_PICK_FROM_CONTAINER_ID
      /*AND Pick_Type              IN (P_PICK_TYPE, 'CP')*/
    AND Pick_Type IN (P_PICK_TYPE, 'CP', 'C')
    GROUP BY pick_from_container_id,
      dest_id,
      distro_nbr;
  -- End CR35261
  -----------------------------------------
  CURSOR Cntr_Location_Generic_Cursor
  IS
    SELECT Location_Id
    FROM Container
    WHERE Facility_Id = P_FACILITY_ID
    AND Container_Id  = P_PICK_FROM_CONTAINER_ID;
  -----------------------------------------
  --CR35260 - Filter the picked CP conts
  /*CURSOR Cntr_Qty_Generic_Cursor IS
  SELECT SUM(CI.Container_Qty)
  FROM Container_Item CI, Container C
  WHERE C.Facility_Id          = P_FACILITY_ID
  AND CI.Facility_Id         = P_FACILITY_ID
  AND (C.Master_Container_Id  = P_PICK_FROM_CONTAINER_ID
  OR  C.Container_Id  = P_PICK_FROM_CONTAINER_ID)
  AND CI.Container_Id = C.Container_Id; */
  CURSOR Cntr_Qty_Generic_Cursor
  IS
    SELECT SUM(CI.Container_Qty)
    FROM Container_Item CI,
      Container C
    WHERE C.Facility_Id        = P_FACILITY_ID
    AND CI.Facility_Id         = P_FACILITY_ID
    AND (C.Master_Container_Id = P_PICK_FROM_CONTAINER_ID
    OR C.Container_Id          = P_PICK_FROM_CONTAINER_ID)
    AND container_status NOT  IN ('P','D')
    AND CI.Container_Id        = C.Container_Id;
  -----------------------------------------
  -- CR35260 - In case of Multi-Sku CP pallet,
  --           P_ITEM_ID is "MULTI - SKU".
  /*CURSOR Item_Master_Description_Cursor IS
  SELECT Description
  FROM Item_Master
  WHERE Facility_Id = P_FACILITY_ID
  AND Item_Id     = P_ITEM_ID; */
  CURSOR Item_Master_Description_Cursor
  IS
    SELECT Description
    FROM Item_Master
    WHERE Facility_Id = P_FACILITY_ID
    AND Item_Id       = l_item_id;
  -----------------------------------------
  CURSOR Get_Pick_Zone
  IS
    SELECT Zone
    FROM Location
    WHERE Facility_Id = P_FACILITY_ID
    AND Location_Id   = P_PM_START_LOCATION;
  -- CR35260 - New cursor fetch item_id for the eligible pick
  -- Start CR35261 - LCC
  CURSOR get_item_id
  IS
    SELECT item_id
    FROM pick_directive
    WHERE facility_id          = P_FACILITY_ID
    AND wave_nbr               = P_WAVE_NBR
    AND pick_from_container_id = P_PICK_FROM_CONTAINER_ID
      /*AND pick_type IN (P_PICK_TYPE, 'CP')*/
    AND pick_type IN (P_PICK_TYPE, 'CP', 'C')
    GROUP BY item_id;
  --
  -- CR35260 - New cursor to fetch distinct no of items in CP pallet.
  CURSOR get_item_qty
  IS
    SELECT COUNT (DISTINCT (item_id)) AS total_item_qty
    FROM pick_directive
    WHERE facility_id          = P_FACILITY_ID
    AND wave_nbr               = P_WAVE_NBR
    AND pick_from_container_id = P_PICK_FROM_CONTAINER_ID
      /*AND pick_type IN (P_PICK_TYPE, 'CP');*/
    AND pick_type IN (P_PICK_TYPE, 'CP', 'C');
  --
  -- CR35260 - New cursor to fetch distinct no of destinations CP pallet is going to.
  CURSOR c_get_dest
  IS
    SELECT COUNT(DISTINCT dest_id)
    FROM Pick_Directive
    WHERE Facility_id          = P_FACILITY_ID
    AND Wave_Nbr               = P_WAVE_NBR
    AND Pick_From_Container_ID = P_PICK_FROM_CONTAINER_ID
      /*AND Pick_Type IN (P_PICK_TYPE, 'CP');*/
    AND Pick_Type IN (P_PICK_TYPE, 'CP', 'C');
  -- ---------------------------------------
  CURSOR get_location (cid_in container.container_id%TYPE)
  IS
    SELECT location_id
    FROM container
    WHERE facility_id = P_FACILITY_ID
    AND container_id  = cid_in;
  -- ---------------------------------------
  CURSOR check_multi_mlp_loc(cid_in IN container.container_id%TYPE, loc_in IN location.location_id%TYPE)
  IS
    SELECT
      /*+ RULE */
      'X'
    FROM container
    WHERE facility_id        = P_FACILITY_ID
    AND (master_container_id = 'NONE'
    AND container_id        <> cid_in
    OR (master_container_id <> 'NONE'
    AND master_container_id <> cid_in))
    AND location_id          = loc_in;
  -- End CR35261
 tcnt int :=0;
BEGIN

--I_MSG_DISPLAY := I_MSG_DISPLAY||'wave: '||p_wave_nbr;

  -- Get the probable zone, a picker would like to pick from. This can be determined
  -- by starting location scanned in Pick wothout a Pick Package screen. For labeled
  -- Picking 'N'o try to display the first pick from that zone.
  t_Zone              := NULL;
  IF P_LABELED_PICKING = 'N' THEN
    OPEN Get_Pick_Zone;
    FETCH Get_Pick_Zone INTO t_Zone;
    CLOSE Get_Pick_Zone;
    debug1('Zone: '||t_zone);
  END IF;
  ---------------------------------------------------------
  -- Get wave, zone, pick_order, pick type, and distro time
  -- for the pick to container that was entered.
  -- Use for further picks.
  ---------------------------------------------------------
  IF P_FIRST_PICK_FLAG     = 'Y' THEN
    IF P_PM_TASK_QUEUE_IND = 'Y' THEN
      -- CR35260 - Also fetching the CP picks and determining if
      --           the CP pick can be processed as BP. If not then bypass.
      /*OPEN Pick_Container_TQ_Cursor;
      FETCH Pick_Container_TQ_Cursor INTO P_PICK_TO_CONTAINER_ID,
      P_WAVE_NBR,
      P_ZONE,
      P_PICK_TYPE,
      P_PICK_FROM_CONTAINER_ID,
      P_ITEM_ID,
      P_CONTAINER_QTY,
      t_container_count,
      P_PM_START_LOCATION;
      value_found := Pick_Container_TQ_Cursor%FOUND;
      CLOSE Pick_Container_TQ_Cursor;*/
      FOR pick_cur_first IN Pick_Container_TQ_Cursor
      LOOP
        t_pick_found := FALSE;
        --IF pick_cur_first.pick_type = 'CP' THEN
        IF pick_cur_first.pick_type IN ('CP', 'C') THEN
          t_pick_found := consolidate_picks( P_FACILITY_ID, pick_cur_first.wave_nbr, pick_cur_first.pick_type, pick_cur_first.pick_from_container_id, 'hh_bulk_pick_s', NULL );
        ELSE
          t_pick_found := TRUE;
        END IF;
        -- Start CR35261 - LCC
        t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(P_FACILITY_ID ,pick_cur_first.pick_from_container_id);
        OPEN get_location (pick_cur_first.pick_from_container_id);
        FETCH get_location INTO t_loc;
        CLOSE get_location;
        OPEN check_multi_mlp_loc (pick_cur_first.pick_from_container_id,t_loc);
        FETCH check_multi_mlp_loc INTO t_dummy;
        t_mlp_in_location := check_multi_mlp_loc%FOUND;
        CLOSE check_multi_mlp_loc;
        --IF t_pick_found THEN
        /* Start R.Macero CO95087 */
        IF (t_pick_found AND pick_cur_first.pick_type <> 'C') OR (t_pick_found AND pick_cur_first.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC(P_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
          /* End R.Macero CO95087 */
          IF pick_cur_first.pick_type = 'C' THEN
            P_CONTAINER_QTY          := '';
            P_CONTAINER_QTY2         := pick_cur_first.total_cont_qty;
          ELSE
            P_CONTAINER_QTY := pick_cur_first.total_cont_qty;
          END IF;
          value_found              := TRUE;
          P_PICK_TO_CONTAINER_ID   := pick_cur_first.pick_to_container_id;
          P_WAVE_NBR               := pick_cur_first.Wave_nbr;
          P_ZONE                   := pick_cur_first.zone;
          P_PICK_TYPE              := pick_cur_first.pick_type;
          P_PICK_FROM_CONTAINER_ID := pick_cur_first.pick_from_container_id;
          --P_CONTAINER_QTY := pick_cur_first.total_cont_qty;
          t_container_count   := pick_cur_first.total_cont;
          P_PM_START_LOCATION := pick_cur_first.location_id;
          -- End CR35261
          OPEN get_item_id;
          FETCH get_item_id INTO l_item_id;
          CLOSE get_item_id;
          OPEN get_item_qty;
          FETCH get_item_qty INTO l_item_qty;
          CLOSE get_item_qty;
          IF l_item_qty > 1 THEN
            P_ITEM_ID  := 'MULTI - SKU';
          ELSE
            P_ITEM_ID := l_item_id;
          END IF;
          EXIT;
        END IF;
      END LOOP;
      --
      /* Start CO43433 LCoronel - 2 */
      -- No rows returned from Pick_Container_TQ_Cursor
      IF NOT(value_found) THEN
        /*RAISE no_picks;*/
        Get_Error_Message(
                  V_RETURN           => V_RETURN,
                  I_MSG_DISPLAY      => I_MSG_DISPLAY,
                  I_FACILITY_ID      => p_facility_id ,
                  I_USER             => p_user ,
                  I_PROCESS_BY_AISLE => p_process_by_aisle ,
                  P_PM_ALLOW_INTRLVG => p_pm_allow_intrlvg ,
                  P_PM_NO_PICK_FLAG  => P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG  => P_PM_NO_TASK_FLAG ,
                  P_Global_Intrlvg   => P_Global_Intrlvg);
		RETURN;
        /* End CO43433 LCoronel - 2 */
      ELSE
        P_GENERIC_TO_CONTAINER := P_PICK_FROM_CONTAINER_ID;
        p_goto_field := 'BULK_PICK.CONF_LOCATION_ID';
        /*� GO_ITEM('Bulk_Pick.Conf_Location_id') �*/
         
      END IF;
    ELSE --P_PM_TASK_QUEUE_IND
      -- CR35260 - Also fetching the CP picks and determining if
      --           the CP pick can be processed as BP. If not then bypass.
      /*OPEN Pick_Container_Cursor;
      FETCH Pick_Container_Cursor INTO P_PICK_TO_CONTAINER_ID,
      P_WAVE_NBR,
      P_ZONE,
      P_PICK_TYPE,
      P_PICK_FROM_CONTAINER_ID,
      P_ITEM_ID,
      t_location_id,
      P_CONTAINER_QTY,
      t_container_count;
      value_found := Pick_Container_Cursor%FOUND;
      CLOSE Pick_Container_Cursor;*/
      FOR pick_cur_first IN Pick_Container_Cursor
      LOOP
        t_pick_found := FALSE;
        --IF pick_cur_first.pick_type = 'CP' THEN
        IF pick_cur_first.pick_type IN ('CP', 'C') THEN
          t_pick_found := consolidate_picks( P_FACILITY_ID, pick_cur_first.wave_nbr, pick_cur_first.pick_type, pick_cur_first.pick_from_container_id, 'hh_bulk_pick_s', NULL );
        ELSE
          t_pick_found := TRUE;
        END IF;
        -- Start CR35261
        t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(P_FACILITY_ID ,pick_cur_first.pick_from_container_id);
        OPEN get_location (pick_cur_first.pick_from_container_id);
        FETCH get_location INTO t_loc;
        CLOSE get_location;
        OPEN check_multi_mlp_loc (pick_cur_first.pick_from_container_id,t_loc);
        FETCH check_multi_mlp_loc INTO t_dummy;
        t_mlp_in_location := check_multi_mlp_loc%FOUND;
        CLOSE check_multi_mlp_loc;
        --IF t_pick_found THEN 
        /* Start R.Macero CO95087 */
        IF (t_pick_found AND pick_cur_first.pick_type <> 'C') OR ( t_pick_found AND pick_cur_first.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC( P_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
          /* End R.Macero CO95087 */
          IF pick_cur_first.pick_type = 'C' THEN
            P_CONTAINER_QTY          := '';
            P_CONTAINER_QTY2         := pick_cur_first.total_cont_qty;
          ELSE
            P_CONTAINER_QTY := pick_cur_first.total_cont_qty;
          END IF;
          P_PICK_TO_CONTAINER_ID   := pick_cur_first.pick_to_container_id;
          P_WAVE_NBR               := pick_cur_first.wave_nbr;
          P_ZONE                   := pick_cur_first.zone;
          P_PICK_TYPE              := pick_cur_first.pick_type;
          P_PICK_FROM_CONTAINER_ID := pick_cur_first.pick_from_Container_id;
          t_location_id            := pick_cur_first.location_id;
          --P_CONTAINER_QTY := pick_cur_first.total_cont_qty;
          t_container_count := pick_cur_first.total_cont;
          -- End CR35261
          OPEN get_item_id;
          FETCH get_item_id INTO l_item_id;
          CLOSE get_item_id;
          OPEN get_item_qty;
          FETCH get_item_qty INTO l_item_qty;
          CLOSE get_item_qty;
          IF l_item_qty > 1 THEN
            P_ITEM_ID  := 'MULTI - SKU';
          ELSE
            P_ITEM_ID := l_item_id;
          END IF;
          value_found := TRUE;
          EXIT;
        END IF;
      END LOOP;
      --
      /* Start CO43433 LCoronel - 2 */
      -- No rows returned from Pick_Container_Cursor
      IF NOT(value_found) THEN
        /*RAISE no_picks;*/
       Get_Error_Message(
                  V_RETURN           => V_RETURN,
                  I_MSG_DISPLAY      => I_MSG_DISPLAY,
                  I_FACILITY_ID      => p_facility_id ,
                  I_USER             => p_user ,
                  I_PROCESS_BY_AISLE => p_process_by_aisle ,
                  P_PM_ALLOW_INTRLVG => p_pm_allow_intrlvg ,
                  P_PM_NO_PICK_FLAG  => P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG  => P_PM_NO_TASK_FLAG ,
                  P_Global_Intrlvg   => P_Global_Intrlvg);
	   RETURN;
        /* End CO43433 LCoronel - 2 */
      ELSE
        P_GENERIC_TO_CONTAINER := P_PICK_FROM_CONTAINER_ID;
        p_goto_field := 'BULK_PICK.CONF_LOCATION_ID';
        /*� GO_ITEM('Bulk_Pick.Conf_Location_id') �*/
         
      END IF;
    END IF;
    --
    -- CR35260 - Also consider CP as a valid pick type.
    /*IF P_PICK_TYPE NOT IN ('B','BR','BP') THEN*/
    --
    -- Start CR35261 - LCC
    /*IF P_PICK_TYPE NOT IN ('B','BR','BP','CP') THEN*/
    IF P_PICK_TYPE NOT IN ('B','BR','BP','CP', 'C') THEN
      P_PICK_FROM_CONTAINER_ID := '';
      P_ITEM_ID                := '';
      P_CONTAINER_QTY          := '';
      RAISE inv_pick_type;
      -- End CR35261
    END IF;
    --
    ---------------------------------------------------------
    -- Set the reference code in the labor productivity table
    -- to equal the wave number and the zone.
    ---------------------------------------------------------
    P_REFERENCE_CODE := TO_CHAR(P_WAVE_NBR) || LPAD(P_ZONE, 4, ' ');
    --
    P_FIRST_PICK_FLAG := 'N';
  ELSE --P_FIRST_PICK_FLAG (N)
    debug1('In Else: ');
    /*� GO_BLOCK('BULK_PICK') �*/
    p_goto_field := 'BULK_PICK';
    
    P_CONF_LOCATION_ID  := '';
    P_CONF_CONTAINER_ID := '';
    P_EMPTY_PRESSED     := 'N';
    P_TO_LOC_ENTERED    := 'N';
    P_OLD_TO_CID        := '';
    -- CR35260 - Also fetching the CP picks and determining if
    --           the CP pick can be processed as BP. If not then bypass.
    /*OPEN Pick_Directive_Cursor;
    FETCH Pick_Directive_Cursor INTO P_PICK_TO_CONTAINER_ID,
    P_PICK_FROM_CONTAINER_ID,
    P_WAVE_NBR,
    P_ZONE,
    P_PICK_TYPE,
    P_ITEM_ID,
    t_location_id,
    P_CONTAINER_QTY,
    t_container_count;
    value_found := Pick_Directive_Cursor%FOUND;
    CLOSE Pick_Directive_Cursor;*/
    debug1(P_FACILITY_ID||' -> '||P_PM_WAVE_NBR||' -> '||P_PROCESS_BY_AISLE||' -> '||P_ZONE||' -> '||P_PM_START_LOCATION||' -> '||P_PM_PICK_TYPE);
    FOR pick_cur IN Pick_Directive_Cursor
    LOOP
    tcnt := tcnt +1 ;
      t_pick_found := FALSE;
      --IF pick_cur.pick_type = 'CP' THEN
      IF pick_cur.pick_type IN ('CP', 'C') THEN
        t_pick_found := consolidate_picks( P_FACILITY_ID, pick_cur.wave_nbr, pick_cur.pick_type, pick_cur.pick_from_container_id, 'hh_bulk_pick_s', NULL );
      ELSE
        t_pick_found := TRUE;
      END IF;
      -- CR35261 LCC: change #3
      t_mlp_check := PKG_HOTEL_PICKING_ADF.CHECK_MLP_VALIDITY2(P_FACILITY_ID ,pick_cur.pick_from_container_id);
      OPEN get_location (pick_cur.pick_from_container_id);
      FETCH get_location INTO t_loc;
      CLOSE get_location;
      debug1('t_loc: '||t_loc);
      OPEN check_multi_mlp_loc (pick_cur.pick_from_container_id, t_loc);
      FETCH check_multi_mlp_loc INTO t_dummy;
      debug1('t_dummy: '||t_dummy);
      t_mlp_in_location := check_multi_mlp_loc%FOUND;
      CLOSE check_multi_mlp_loc;
      --IF t_pick_found THEN
      /* Start R.Macero CO95087 */
      IF (t_pick_found AND pick_cur.pick_type <> 'C') OR (t_pick_found AND pick_cur.pick_type = 'C' AND t_mlp_check AND ((t_mlp_in_location AND PKG_HOTEL_PICKING_ADF.MLP_IN_SHUTTLE_LOC( P_FACILITY_ID,t_loc) = 'Y') OR NOT t_mlp_in_location)) THEN
        /* End R.Macero CO95087 */
        IF pick_cur.pick_type = 'C' THEN
          P_CONTAINER_QTY    := '';
          P_CONTAINER_QTY2   := pick_cur.total_cont_qty;
        ELSE
          P_CONTAINER_QTY := pick_cur.total_cont_qty;
        END IF;
        P_PICK_TO_CONTAINER_ID   := pick_cur.pick_to_container_id;
        P_PICK_FROM_CONTAINER_ID :=pick_cur.pick_from_Container_id;
        P_WAVE_NBR               := pick_cur.wave_nbr;
        P_ZONE                   := pick_cur.zone;
        P_PICK_TYPE              := pick_cur.pick_type;
        t_location_id            := pick_cur.location_id;
        --P_CONTAINER_QTY := pick_cur.total_cont_qty;
        t_container_count := pick_cur.total_cont;
        -- End Cr35261
        OPEN get_item_id;
        FETCH get_item_id INTO l_item_id;
        CLOSE get_item_id;
        OPEN get_item_qty;
        FETCH get_item_qty INTO l_item_qty;
        CLOSE get_item_qty;
        IF l_item_qty > 1 THEN
          P_ITEM_ID  := 'MULTI - SKU';
        ELSE
          P_ITEM_ID := l_item_id;
        END IF;
        value_found := TRUE;
        EXIT;
      END IF;
    END LOOP;
    --
     debug1('tcnt: '||tcnt);
    /* Start CO43433 LCoronel - 2 */
    -- No rows returned from Pick_Directive_Cursor
    IF NOT(value_found) THEN
      /*� Save_Labor_Prod �*/
      Save_Labor_Prod( p_facility_id => p_facility_id, p_labeled_picking => p_labeled_picking, p_user => p_user, p_Operations_Performed => p_operations_performed, p_Start_Time => p_start_time, p_Units_Processed => p_units_processed, p_Containers_Processed => p_containers_processed, p_Reference_Code => p_reference_code, V_RETURN => v_return);
      /*� PC3_Go_Field('BULK_PICK.generic_to_container') �*/
      p_goto_field := 'BULK_PICK.GENERIC_TO_CONTAINER';
      /*RAISE no_picks;*/
      Get_Error_Message(
                  V_RETURN           => V_RETURN,
                  I_MSG_DISPLAY      => I_MSG_DISPLAY,
                  I_FACILITY_ID      => p_facility_id ,
                  I_USER             => p_user ,
                  I_PROCESS_BY_AISLE => p_process_by_aisle ,
                  P_PM_ALLOW_INTRLVG => p_pm_allow_intrlvg ,
                  P_PM_NO_PICK_FLAG  => P_PM_NO_PICK_FLAG ,
                  P_PM_NO_TASK_FLAG  => P_PM_NO_TASK_FLAG ,
                  P_Global_Intrlvg   => P_Global_Intrlvg);
	  RETURN;
      /* End CO43433 LCoronel - 2 */
    ELSE
      -- Start CR35261 - LCC
      IF P_PICK_TYPE         = 'C' THEN
        P_ORIGINAL_PICK_QTY := P_CONTAINER_QTY2;
      ELSE
        P_ORIGINAL_PICK_QTY := P_CONTAINER_QTY;
      END IF;
      -- End CR35261
      P_GENERIC_TO_CONTAINER := P_PICK_FROM_CONTAINER_ID;
      p_goto_field := 'BULK_PICK.CONF_LOCATION_ID';
      /*� GO_ITEM('BULK_PICK.CONF_LOCATION_ID') �*/
       
    END IF; -- No value found Pick_Directive_Cursor
  END IF;   --first flag
  -- CR35260 - Multi dest logic for Full pallet CP picks
  OPEN c_get_dest;
  FETCH c_get_dest INTO l_dest_qty;
  CLOSE c_get_dest;
  
     debug1('l_dest_qty: '||l_dest_qty);
  /*IF (t_container_count > 1) THEN*/
  -- Start CR35261 - LCC
  /*IF (t_container_count > 1 AND P_PICK_TYPE <> 'CP') OR (l_dest_qty > 1 AND P_PICK_TYPE = 'CP') THEN*/
  IF (t_container_count > 1 AND P_PICK_TYPE NOT IN ('CP', 'C')) OR (l_dest_qty > 1 AND P_PICK_TYPE IN ('CP', 'C')) THEN
    -- End CR35261
    -----------------------------------------------
    -- The bulk pick is a BulkGroup Pick
    -----------------------------------------------
    P_DEST_ID    := g_scp(P_FACILITY_ID, 'mixed_dest_id');
    P_BULK_GROUP := 'TRUE';
    
        debug1('P_DEST_ID2: '||P_DEST_ID||' P_BULK_GROUP2: '||P_BULK_GROUP);
  ELSE
    -----------------------------------------------
    -- It is a single Bulk Pick
    -----------------------------------------------
    OPEN Container_Dest_Id_Cursor;
    FETCH Container_Dest_Id_Cursor INTO P_DISTRO_NBR, P_DEST_ID;
    CLOSE Container_Dest_Id_Cursor;
    P_BULK_GROUP := 'FALSE';
    debug1('P_DEST_ID: '||P_DEST_ID||' P_BULK_GROUP: '||P_BULK_GROUP);
  END IF;
  ----------------------------------------------------------------------
  -- Fetch the container location and the pick container qty
  ----------------------------------------------------------------------
  OPEN Cntr_Location_Generic_Cursor;
  FETCH Cntr_Location_Generic_Cursor INTO P_FROM_LOCATION_ID;
  container_found := Cntr_Location_Generic_Cursor%FOUND;
  CLOSE Cntr_Location_Generic_Cursor;
  
  debug1('P_FROM_LOCATION_ID: '||P_FROM_LOCATION_ID);
  IF (container_found = TRUE) THEN
    OPEN Cntr_Qty_Generic_Cursor;
    FETCH Cntr_Qty_Generic_Cursor INTO P_PICK_CONTAINER_QTY;
    container_found := Cntr_Qty_Generic_Cursor%FOUND;
  debug1('P_PICK_CONTAINER_QTY: '||P_PICK_CONTAINER_QTY);
    CLOSE Cntr_Qty_Generic_Cursor;
  END IF;
  ----------------------------------------------
  IF NOT ( container_found ) THEN
  debug1('container_found: No');
    ----------------------------------------------
   --  I_MSG_DISPLAY := I_MSG_DISPLAY||' - '|| p_facility_id||' - '|| p_Wave_Nbr||' - '||p_Distro_Nbr ||' - '||p_Pick_From_Container_ID ||' - '||p_Dest_Id ||' - '|| p_Pick_To_Container_Id ;
    t_purge_status    := purge_directive ( P_FACILITY_ID, P_WAVE_NBR, P_DISTRO_NBR, P_PICK_FROM_CONTAINER_ID, P_DEST_ID, P_PICK_TO_CONTAINER_ID );
 
  debug1('t_purge_status: '||t_purge_status);
  IF t_purge_status <> 'SUCCESS' THEN
      RAISE purge_failure;
    END IF;
    -------------------------------------------
    COMMIT;
  END IF;
  -- CR35260 - If Full CP pallet has multiple SKus then
  --           display "MULTI - SKU" in the item description field.
  -- Start CR35261 - LCC
  /*IF l_item_qty > 1 AND P_PICK_TYPE = 'CP' THEN*/
  IF l_item_qty    > 1 AND P_PICK_TYPE IN ('CP', 'C') THEN
    P_DESCRIPTION := 'MULTI - SKU';
  ELSE
    -- End CR35261
    OPEN Item_Master_Description_Cursor;
    FETCH Item_Master_Description_Cursor INTO P_DESCRIPTION;
    CLOSE Item_Master_Description_Cursor;
  END IF; 
  
    --
    -- CR35260 - If value_found and container_found then initialize
    --          l_Container_Qty = l_Pick_Container_Qty
    IF value_found AND container_found AND p_Pick_Type   <> 'C' THEN
      p_Container_Qty                                    := p_Pick_Container_Qty;
    ELSIF value_found AND container_found AND p_Pick_Type = 'C' THEN
      p_Container_Qty2                                   := p_Pick_Container_Qty;
    END IF;
    
 
    p_original_pick_qty := p_Pick_Container_Qty;
    --
     debug1('p_original_pick_qty 2 : '|| p_original_pick_qty); 
    p_pick_to_container_id2   := p_pick_to_container_id;
    p_pick_from_container_id2 := p_Pick_From_Container_ID;
    
  -----------------------------------------------------------------
  --UPDATE PICK_IN_PROGRESS FLAG TO 'Y' FOR PICK_FROM_CONTAINER_ID
  -----------------------------------------------------------------
  -- CR35260 - Also lock eligible Full pallet CP picks
  /*UPDATE PICK_DIRECTIVE
  SET PICK_IN_PROGRESS = 'Y',
  USER_ID = P_USER
  WHERE FACILITY_ID = P_FACILITY_ID
  AND PICK_FROM_CONTAINER_ID = P_PICK_FROM_CONTAINER_ID
  AND PICK_TYPE IN ('B', 'BP'); */
  -- Start CR35261 - LCC
  UPDATE PICK_DIRECTIVE
  SET PICK_IN_PROGRESS       = 'Y',
    USER_ID                  = P_USER
  WHERE FACILITY_ID          = P_FACILITY_ID
  AND PICK_FROM_CONTAINER_ID = P_PICK_FROM_CONTAINER_ID
    /*AND PICK_TYPE IN ('B', 'BP','CP');*/
  AND PICK_TYPE IN ('B', 'BP','CP', 'C');
  COMMIT;
  ------------------------------------------------
EXCEPTION
WHEN inv_container THEN
   I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE', t_purge_status);
    PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
    
  /*� Call_Msg_Window_P('INV_CONTAINER') �*/  
  --RAISE FORM_TRIGGER_FAILURE;
WHEN inv_pick_type THEN
  I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE', t_purge_status);
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);         
  /*� Call_Msg_Window_P('NOT_IN_PICK') �*/  
 -- RAISE FORM_TRIGGER_FAILURE;
WHEN no_picks THEN
  I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE', t_purge_status);
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  /*� Call_Msg_Window_P('PICK_COMPLETE') �*/  
 -- RAISE FORM_TRIGGER_FAILURE;
  /*� CLEAR_BLOCK('NO_VALIDATE') �*/  
  /* Start CO43433 LCoronel - Added error handling for the new procedure
  get_error_message. */
--WHEN FORM_TRIGGER_FAILURE THEN
 -- RAISE;
  /* End CO43433 LCoronel */
WHEN purge_failure THEN
 I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE');
  PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Load_Screen',t_purge_status);
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  /*� Call_Msg_Window_P('OPER_FAILURE') �*/
  /*� Log_Error_Roll(9999, 'Load_Screen ', t_purge_status) �*/
  
 -- RAISE FORM_TRIGGER_FAILURE;
WHEN OTHERS THEN
   I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE', t_purge_status);
    PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Get_Pick_Directive','Pick To CID: ' ||
         P_PICK_TO_CONTAINER_ID ||
        ' Wv: '  ||  TO_CHAR(P_WAVE_NBR, '90') ||
        ' Zn: '  || P_ZONE ||
        ' Typ: ' || P_PICK_TYPE ||
        ' D TS: '||  TO_CHAR(P_DISTRO_TS, 'MM-DD HH24:MI:SS')||  SQLERRM);
    PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
    
  /*� Log_Error_Roll(SQLCODE, 'Get_Pick_Directive', 'Pick To CID: ' ||
  P_PICK_TO_CONTAINER_ID ||
  ' Wv: ' ||
  TO_CHAR(P_WAVE_NBR, '90') ||
  ' Zn: ' || P_ZONE ||
  ' Typ: ' || P_PICK_TYPE ||
  ' D TS: ' ||
  TO_CHAR(I_DISTRO_TS, 'MM-DD HH24:MI:SS')||
  SQLERRM) �*/

 -- RAISE FORM_TRIGGER_FAILURE;
END;
--
PROCEDURE Run_Purge_Directive(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_BULK_GROUP               IN VARCHAR2 ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER )
IS
t_purge_status VARCHAR2(200);
purge_failure   EXCEPTION;
	
CURSOR Cancel_Cursor IS
SELECT Distro_Nbr,
       Dest_ID    
  FROM Pick_Directive
 WHERE Facility_ID            = I_FACILITY_ID
   AND Pick_From_Container_ID = I_PICK_FROM_CONTAINER_ID
   AND Pick_Type              = I_PICK_TYPE
GROUP BY pick_from_container_id,
       dest_id,
       distro_nbr;
------------------------------------------------------------------
BEGIN
	-- Cancel the pick
	-- Issue #370 - SJS - 7 August 2001
	-- If pick is a bulk group pick, then loop through all poicks for the pick from container
	-- and delete all asociated directives.

  IF I_BULK_GROUP  = 'TRUE' THEN
    FOR pdrec IN cancel_cursor 
    LOOP
      t_purge_status := purge_directive(I_FACILITY_ID,
                        							  I_WAVE_NBR,
                        							  pdrec.Distro_Nbr,
                        					      I_PICK_FROM_CONTAINER_ID01,
                                        pdrec.Dest_Id,
                                        I_PICK_TO_CONTAINER_ID );
      IF t_purge_status <> 'SUCCESS' THEN
        RAISE purge_failure;
      END IF;
    END LOOP;
  ELSE
    -- Cancel the pick When F8 pressed  
    t_purge_status := purge_directive( I_FACILITY_ID,
                                       I_WAVE_NBR,
                                       I_DISTRO_NBR,
                                       I_PICK_FROM_CONTAINER_ID01,
                                       I_DEST_ID,
                                       I_PICK_TO_CONTAINER_ID );
    IF t_purge_status <> 'SUCCESS' THEN
      RAISE purge_failure;
    END IF;
  END IF;
  
  -- Commit Purge_Directive Transactions
  COMMIT;
     
EXCEPTION
WHEN purge_failure THEN
  /*� Log_Error_Roll(9999,'Run_Purge_Directive', t_purge_status) �*/
  /*� Call_Msg_Window_P('OPER_FAILURE') �*/
  PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Run_Purge_Directive',t_purge_status);
  I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
-- RAISE FRM_TRIGGER_FAILURE;
WHEN OTHERS THEN
   /*� Log_Error_Roll(SQLCODE, 'Run_Purge_Directive ', SQLERRM) �*/
 PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Run_Purge_Directive',SQLERRM);
 PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
-- RAISE FRM_TRIGGER_FAILURE;
END Run_Purge_Directive;
--

PROCEDURE Check_Empty(
    V_RETURN             IN OUT VARCHAR2,
    I_MSG_DISPLAY        IN OUT VARCHAR2,
    I_FACILITY_ID        IN VARCHAR2 ,
    From_Location_Id_in  IN VARCHAR2,
    From_Container_Id_in IN VARCHAR2,    
    I_BULK_GROUP               IN VARCHAR2 ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER ,
    Empty_Flag_in        IN VARCHAR2 )
IS
/*
||  If empty flag and container qty does not correspond mark for cycle
||  count.
||  This is referenced in HH_Container_Pick_S
*/
--------------------------------------------------------------------
CURSOR Container_Qty_Cursor IS    /* Count num of containers in from */
SELECT COUNT(*)
  FROM Container
 WHERE Facility_Id   = I_FACILITY_ID
   AND Location_ID   = From_Location_Id_in
   AND Container_ID <> From_Container_Id_in;
t_container_qty   NUMBER(6);
loc_found         BOOLEAN;
-------------------------------------------------------------------
BEGIN

  OPEN Container_Qty_Cursor;
      FETCH Container_Qty_Cursor INTO t_container_qty;
  CLOSE Container_Qty_Cursor;
--
   IF Empty_Flag_in = 'M' THEN
      IF mark_location( From_Location_Id_In, I_FACILITY_ID ) = 'SUCCESS' THEN
          /*� Run_Purge_Directive �*/
          Run_Purge_Directive(
            V_RETURN                   => V_RETURN,
            I_MSG_DISPLAY              => I_MSG_DISPLAY,
            I_PICK_FROM_CONTAINER_ID   => From_Container_Id_in,
            I_FACILITY_ID              => I_FACILITY_ID ,
            I_BULK_GROUP               => I_FACILITY_ID ,
            I_DEST_ID                  => I_DEST_ID ,
            I_DISTRO_NBR               => I_DISTRO_NBR ,
            I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 ,
            I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID ,
            I_PICK_TYPE                => I_PICK_TYPE ,
            I_WAVE_NBR                 => I_WAVE_NBR );
            
          /*� CLEAR_BLOCK(NO_VALIDATE) �*/
         COMMIT;
      END IF; 
   ELSIF Empty_Flag_in = 'Y' AND t_container_qty > 0 THEN
      IF  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CONFIRM_EMPTY') /*� Call_Msg_Window('CONFIRM_EMPTY') �*/ = 'Y' THEN
         /* Manually Mark location for cycle count */
         IF mark_location( From_Location_Id_In, I_FACILITY_ID ) = 'SUCCESS' THEN
            COMMIT;
         END IF;
      END IF;
--
   ELSIF Empty_Flag_in = 'N' AND t_container_qty = 0 THEN
      IF PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CONFIRM_NOT_EMP')  /*� Call_Msg_Window('CONFIRM_NOT_EMP') �*/ = 'Y' THEN
         /* Manually Mark location for cycle count */
         IF mark_location( From_Location_Id_In, I_FACILITY_ID ) = 'SUCCESS' THEN
            COMMIT;
         END IF;
      END IF;
   END IF;
EXCEPTION
WHEN OTHERS THEN
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Check_Empty',SQLERRM);
     PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  /*� Log_Error_Roll(SQLCODE, 'Check_Empty ', SQLERRM) �*/
-- RAISE FRM_TRIGGER_FAILURE;
END Check_Empty;
--
PROCEDURE update_master_cont_dest_loc(
    V_RETURN                 IN OUT VARCHAR2,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_DEST_ID                IN NUMBER ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER )
IS
t_status             VARCHAR2(60);
update_master_error  EXCEPTION;
BEGIN
  t_status := Update_Master_Container_To_Loc (I_FACILITY_ID, 
              I_PICK_TO_CONTAINER_ID,
              I_GENERIC_TO_CONTAINER);
  IF t_status <> 'SUCCESS' THEN
   RAISE update_master_error;
  END IF;

EXCEPTION
 WHEN update_master_error THEN
    PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Load_Screen',t_status||
                        ' Wave ' || TO_CHAR(I_WAVE_NBR) ||
                        ' Distr ' || I_DISTRO_NBR ||
                        ' Pck Frm ' ||
                             I_PICK_FROM_CONTAINER_ID ||
                        ' Dest ' || TO_CHAR(I_DEST_ID) ||
                        ' Pck To ' ||
                             I_PICK_TO_CONTAINER_ID);
    PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);

   /*� Log_Error  (SQLCODE, 'Load_Screen', t_status||
                        ' Wave ' || TO_CHAR(I_WAVE_NBR) ||
                        ' Distr ' || I_DISTRO_NBR ||
                        ' Pck Frm ' ||
                             I_PICK_FROM_CONTAINER_ID ||
                        ' Dest ' || TO_CHAR(I_DEST_ID) ||
                        ' Pck To ' ||
                             I_PICK_TO_CONTAINER_ID) �*/
--  RAISE FRM_TRIGGER_FAILURE;
END update_master_cont_dest_loc;

--
--

PROCEDURE transfer_attributes(
    V_RETURN                 IN OUT VARCHAR2,
    I_DEST_ID                IN NUMBER ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,
    P_VERSION                IN NUMBER )
IS

l_generic_child     Container.container_id%TYPE;
l_container_id      Container.container_id%TYPE;
l_distro_nbr        Container_Item.distro_nbr%TYPE;
l_location_id       Container.location_id%TYPE;
l_to_location_id    Container.to_location_id%TYPE;
l_final_location_id Container.final_location_id%TYPE;
l_wave_nbr          Container.wave_nbr%TYPE;
l_container_status  Container.container_status%TYPE;
l_user_id           Container.user_id%TYPE;
l_dest_id           Container.dest_id%TYPE;
l_appt_nbr          Container.appt_nbr%TYPE;
l_appt_line         Container.appt_line%TYPE;
l_po_nbr            Container.po_nbr%TYPE;
l_pick_from_container_id  Container.pick_from_container_id%TYPE;
l_pick_type         Container.pick_type%TYPE;
l_carrier_code      Container.carrier_code%TYPE;
l_service_code      Container.service_code%TYPE;
l_route             Container.route%TYPE;
l_count_children    NUMBER;
l_container_length  Container.container_length%TYPE;
l_container_width   Container.container_width%TYPE;
l_container_height  Container.container_height%TYPE;
l_container_weight  Container.container_weight%TYPE;
l_container_cube    Container.container_cube%TYPE;
l_receipt_date      Container.receipt_date%TYPE;
l_putaway_date      Container.putaway_date%TYPE;
l_rma_nbr						Container.rma_nbr%TYPE;
distro_found        BOOLEAN;
l_item_id           Container_item.item_id%type;


CURSOR container_cursor IS
SELECT container_type, location_id,
       appt_nbr, appt_line, po_nbr, container_length, Container_Width,
       Container_Height, Container_Weight, Container_Cube, User_Id, Receipt_Date,
       putaway_Date,  container_status,
       dest_id,  Bol_nbr,Divert_Ts,Sort_Order,pick_from_container_id,
       wave_nbr, pick_not_after_date, lot_nbr, asn_nbr, ucc_label_id,
       unload_check_flag, address_source, to_location_id,
       break_by_distro, pick_type, final_location_id,
       carrier_code, service_code, route, tracking_id, expedite_flag,
       trouble_marked_cont_status, open_date, close_date, rma_nbr
  FROM Container
 WHERE Facility_ID  = I_FACILITY_ID
   AND Container_ID = I_PICK_TO_CONTAINER_ID;

CURSOR get_generic_cont_children IS
SELECT container_id
  FROM Container
 WHERE Facility_id = I_FACILITY_ID
   AND Master_Container_Id = I_GENERIC_TO_CONTAINER;

CURSOR get_distro_nbr1 (cid_in IN VARCHAR)IS
SELECT distro_nbr
  FROM Container_Item
 WHERE Facility_id = I_FACILITY_ID
   AND Container_Id = cid_in;
   
CURSOR get_distro_nbr2 (cid_in IN VARCHAR)IS
SELECT distro_nbr, item_id
  FROM Container_Item
 WHERE Facility_id = I_FACILITY_ID
   AND Container_Id = cid_in;
   
CURSOR child_container_cursor IS
SELECT container_id,location_id, appt_nbr, appt_line, 
       container_length, Container_Width,
       Container_Height, Container_Weight, Container_Cube,
       Receipt_date, Putaway_date,
       po_nbr, User_Id,  container_status,
       dest_id, pick_from_container_id,
       wave_nbr, to_location_id,
       pick_type, final_location_id,
       carrier_code, service_code, route, rma_nbr
  FROM Container
 WHERE Facility_ID  = I_FACILITY_ID
 AND master_Container_ID = I_PICK_TO_CONTAINER_ID;

CURSOR pick_distro is
SELECT distro_nbr
  FROM pick_directive
 WHERE Facility_ID  = I_FACILITY_ID
   AND wave_nbr = I_WAVE_NBR
   AND item_id  = I_ITEM_ID
   AND pick_to_container_id = I_PICK_TO_CONTAINER_ID;

BEGIN
  -- Check if to_container has children (ticketing) --
  SELECT count(*)
    INTO l_count_children
    FROM Container
   WHERE Master_Container_ID = I_PICK_TO_CONTAINER_ID
     AND facility_id = I_FACILITY_ID;

 -----------------------------------------------------------------
 -- Build the pick_to container record..only for labeled picking N
 -- transfer the attributes from the pick to to the real pallet id
 -----------------------------------------------------------------
 IF I_FIRST_SCAN_FLAG = 'Y' THEN -- check if child already transferred --
       FOR ei_rec IN Container_Cursor  LOOP
        UPDATE Container
           SET Container_Type = ei_rec.container_type,                 
               Location_Id = ei_rec.location_id,                    
               Appt_Nbr = ei_rec.appt_nbr,                       
               Appt_Line = ei_rec.appt_line,                      
--               Po_Nbr = ei_rec.po_nbr, -- Commented to retain po_nbr
               Container_Length = ei_rec.container_length,               
               Container_Width = ei_rec.container_width,                
               Container_Height = ei_rec.container_height,               
               Container_Weight = ei_rec.container_weight,               
               Container_Cube = ei_rec.container_cube,                 
               User_Id = ei_rec.user_id,                        
               Receipt_Date = ei_rec.receipt_date,                  
               Putaway_Date = ei_rec.putaway_date,                   
               Container_Status = ei_rec.container_status,
               Dest_Id = ei_rec.dest_id,                        
               Bol_Nbr = ei_rec.bol_nbr,                        
               Divert_Ts = ei_rec.divert_ts,                      
               Sort_Order = ei_rec.sort_order,                     
               Pick_From_Container_Id = ei_rec.pick_from_container_id,         
               Wave_Nbr = ei_rec.wave_nbr,                       
               Pick_Not_After_Date = ei_rec.pick_not_after_date,            
               Lot_Nbr = ei_rec.lot_nbr,                        
               Asn_Nbr = ei_rec.asn_nbr,                        
               Ucc_Label_Id = ei_rec.ucc_label_id,                   
               Unload_Check_Flag = ei_rec.unload_check_flag,              
               Address_Source = ei_rec.address_source,                 
               To_Location_Id = ei_rec.to_location_id,                 
               Break_By_Distro = ei_rec.break_by_distro,                
               Pick_Type = ei_rec.pick_type,                      
               Final_Location_Id = ei_rec.final_location_id,              
               Carrier_Code = ei_rec.carrier_code,                   
               Service_Code = ei_rec.service_code,                   
               Route = ei_rec.route,                          
               Tracking_Id = ei_rec.tracking_id,                 
               Expedite_Flag = ei_rec.expedite_flag,     
               Trouble_Marked_Cont_Status = ei_rec.trouble_marked_cont_status,
               Open_Date = ei_rec.open_date,                     
               Close_Date = ei_rec.close_date,
               Rma_nbr = ei_rec.rma_nbr
         WHERE Facility_ID    = I_FACILITY_ID
           AND Container_Id         = I_GENERIC_TO_CONTAINER;

    IF l_count_children = 0 THEN 
        --This update is for the generic to container's children
        UPDATE Container
           SET Container_Type = ei_rec.container_type,                 
               Location_Id = ei_rec.location_id,                    
               Appt_Nbr = ei_rec.appt_nbr,                       
               Appt_Line = ei_rec.appt_line,                      
--               Po_Nbr = ei_rec.po_nbr, -- Commented to retain po_nbr
               Container_Status = ei_rec.container_status,
               Dest_Id = ei_rec.dest_id,                        
               Bol_Nbr = ei_rec.bol_nbr,                        
               Divert_Ts = ei_rec.divert_ts,                      
               Sort_Order = ei_rec.sort_order,                     
               Pick_From_Container_Id = ei_rec.pick_from_container_id,         
               Wave_Nbr = ei_rec.wave_nbr,                       
               Pick_Not_After_Date = ei_rec.pick_not_after_date,            
               Lot_Nbr = ei_rec.lot_nbr,                        
               Asn_Nbr = ei_rec.asn_nbr,                        
               Ucc_Label_Id = ei_rec.ucc_label_id,                   
               Unload_Check_Flag = ei_rec.unload_check_flag,              
               Address_Source = ei_rec.address_source,                 
               To_Location_Id = ei_rec.to_location_id,                 
               Break_By_Distro = ei_rec.break_by_distro,                
               Pick_Type = ei_rec.pick_type,                      
               Final_Location_Id = ei_rec.final_location_id,              
               Carrier_Code = ei_rec.carrier_code,                   
               Service_Code = ei_rec.service_code,                   
               Route = ei_rec.route,                          
               Tracking_Id = ei_rec.tracking_id,                 
               Expedite_Flag = ei_rec.expedite_flag,     
               Trouble_Marked_Cont_Status = ei_rec.trouble_marked_cont_status,
               Open_Date = ei_rec.open_date,                     
               Close_Date = ei_rec.close_date,
               Rma_nbr = ei_rec.rma_nbr
         WHERE Facility_ID    = I_FACILITY_ID
           AND Master_Container_Id  = I_GENERIC_TO_CONTAINER;

        END IF;
        END LOOP;
 END IF;
 -- Defect 166180 - SJS - removed the commit statement.

  IF I_PICK_TYPE = 'BP' THEN
     /*� Update_Master_Cont_Dest_Loc �*/
     update_master_cont_dest_loc(
        V_RETURN                 => V_RETURN,
        I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER,
        I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
        I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
        I_FACILITY_ID            =>  I_FACILITY_ID,
        I_DEST_ID                =>  I_DEST_ID,
        I_DISTRO_NBR             =>  I_DISTRO_NBR,
        I_WAVE_NBR               =>  I_WAVE_NBR);
  END IF;

  -- labeled_reserve = Y .. resulting pick_to_container has children --
  IF I_LABELED_RESERVE = 'Y' AND l_count_children > 0 THEN
    OPEN get_generic_cont_children;
    LOOP
      FETCH get_generic_cont_children INTO l_generic_child;
      EXIT WHEN get_generic_cont_children%NOTFOUND;
      OPEN child_container_cursor;
      FETCH child_container_cursor INTO l_container_id, 
            l_location_id, l_appt_nbr,l_appt_line, 
            l_container_length, l_container_width, l_container_height,
            l_container_weight, l_container_cube,
            l_receipt_date, l_putaway_date,
            l_po_nbr, l_User_Id,  l_container_status,
            l_dest_id, l_pick_from_container_id,
            l_wave_nbr, l_to_location_id,
            l_pick_type, l_final_location_id,
            l_carrier_code, l_service_code, l_route, l_rma_nbr;
      IF child_container_cursor%NOTFOUND THEN
        CLOSE child_container_cursor;
        EXIT;
      ELSE
       UPDATE Container
       SET Location_Id = l_location_id,                    
           Appt_Nbr = l_appt_nbr,                       
           Appt_Line = l_appt_line,                      
--           Po_Nbr = l_po_nbr, -- Commented to retain po_nbr
           User_Id = l_user_id,                        
           Container_Status = l_container_status,
           Dest_Id = l_dest_id,                        
           Pick_From_Container_Id = l_pick_from_container_id,         
           Wave_Nbr = l_wave_nbr,                       
           To_Location_Id = l_to_location_id,                 
           Pick_Type = l_pick_type,                      
           Final_Location_Id = l_final_location_id,              
           Carrier_Code = l_carrier_code,                   
           Service_Code = l_service_code,                   
           Route = l_route,
           Rma_nbr = l_rma_nbr
     WHERE Facility_ID    = I_FACILITY_ID
       AND Container_Id   = l_generic_child;
     
     IF P_VERSION = 1 THEN 
     		IF I_PICK_TYPE IN ('B','BP', 'CP') THEN -- Defect 348044 appended 'BP
       		OPEN get_distro_nbr1 (l_container_id);
		      FETCH get_distro_nbr1 INTO l_distro_nbr;
          distro_found := get_distro_nbr1%FOUND;
       		CLOSE get_distro_nbr1;
       		IF distro_found THEN
		          UPDATE Container_Item
             		SET distro_nbr = l_distro_nbr
           		WHERE Facility_ID    = I_FACILITY_ID
             		AND Container_Id   = l_generic_child;
       		END IF;
     		END IF;
     	ELSE
		    IF I_PICK_TYPE IN ('B','BP') THEN -- Defect 348044   -- appended 'BP
       		OPEN get_distro_nbr2 (l_container_id);
          FETCH get_distro_nbr2 INTO l_distro_nbr, l_item_id;
          distro_found := get_distro_nbr2%FOUND;
       		CLOSE get_distro_nbr2;
       		IF distro_found THEN
          	UPDATE Container_Item
             	SET distro_nbr = l_distro_nbr
           	WHERE Facility_ID    = I_FACILITY_ID
             	AND Container_Id   = l_generic_child;
       		END IF;
     		END IF;    	
     	END IF;		

     DELETE Container
      WHERE Container_Id = l_container_id
        AND Facility_id = I_FACILITY_ID;

      CLOSE child_container_cursor;

      l_distro_nbr        := NULL;
      l_location_id       := NULL;
      l_to_location_id    := NULL;
      l_final_location_id := NULL;
      l_wave_nbr          := NULL;
      l_container_status  := NULL;
      l_user_id           := NULL;
      l_dest_id           := NULL;
      l_appt_nbr          := NULL;
      l_appt_line         := NULL;
      l_po_nbr            := NULL;
      l_pick_from_container_id := NULL;
      l_pick_type         := NULL;
      l_carrier_code      := NULL;
      l_service_code      := NULL;
      l_route             := NULL;
      l_rma_nbr						:= NULL;

     END IF;

    END LOOP;
    CLOSE get_generic_cont_children;

    DELETE Container
     WHERE Master_Container_ID = I_PICK_TO_CONTAINER_ID
       AND Facility_id = I_FACILITY_ID;
     
    -- Delete container_item record of master if to_children records exist --
    -- and labeled_reserve = 'Y'                                           --
    DELETE Container_item
     WHERE Facility_id  = I_FACILITY_ID
       AND Container_id = I_PICK_FROM_CONTAINER_ID;

  ELSIF (I_LABELED_RESERVE = 'Y' AND l_count_children = 0) THEN

      IF I_PICK_TYPE IN('B','BR','BP') THEN -- Defect 348044 appended 'BP'        	 
        	 IF P_VERSION = 1 THEN 
        	 		OPEN get_distro_nbr1 (I_PICK_TO_CONTAINER_ID);
           		FETCH get_distro_nbr1 INTO l_distro_nbr;
           		distro_found := get_distro_nbr1%FOUND;
        	 		CLOSE get_distro_nbr1;
        	 ELSE		
    	        OPEN get_distro_nbr2 (I_PICK_TO_CONTAINER_ID);
           		FETCH get_distro_nbr2 INTO l_distro_nbr, l_item_id;
           		distro_found := get_distro_nbr2%FOUND;
        			CLOSE get_distro_nbr2;
        	 END IF;        	 
        IF distro_found THEN
           UPDATE Container_Item
              SET distro_nbr = l_distro_nbr
            WHERE Facility_ID    = I_FACILITY_ID
              AND Container_Id IN
                                  (SELECT Container_Id
                                     FROM Container
                                    WHERE facility_id = I_FACILITY_ID
                                      AND master_container_id = I_GENERIC_TO_CONTAINER);                                     
        END IF;
      END IF; 
 
      IF  I_PICK_FROM_CONTAINER_ID !=I_GENERIC_TO_CONTAINER  THEN  
      	DELETE Container_Item
     		 WHERE Container_ID = I_GENERIC_TO_CONTAINER
           AND Facility_id = I_FACILITY_ID;
      END IF;

  ELSIF I_LABELED_RESERVE = 'N' THEN
    IF I_PICK_TYPE = 'B' THEN
       DELETE Container_Item
        WHERE Container_ID = I_PICK_TO_CONTAINER_ID
          AND Facility_id = I_FACILITY_ID;
   ELSIF I_PICK_TYPE = 'BP' THEN
       DELETE Container_Item
        WHERE Container_ID = I_GENERIC_TO_CONTAINER
          AND Facility_id = I_FACILITY_ID;
    END IF;
  END IF;
  -- Defect 166180 - SJS - removed the commit statement.

EXCEPTION
  WHEN OTHERS THEN
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Transfer Attributes',SQLERRM);
     PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
     /*� Log_Error_Roll(SQLCODE, 'Transfer Attributes', SQLERRM) �*/
--    RAISE FRM_TRIGGER_FAILURE;
END;
--
--
PROCEDURE transfer_attributes_cp(
    V_RETURN                 IN OUT VARCHAR2,
    I_DEST_ID                IN NUMBER ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 )
IS
/****************************************************************************
*  Change History:                                                                
*    
*   Author           Date         Defect/CR#    Change Description      
*   -------------------------------------------------------------------------
*  M. Shah                        CR35260       Accenture Change � WMS
*                                               Splitting
*                                               This procedure will be
*                                               called when processing CP
*                                               picks. It will transfer the
*                                               attributes of P status
*                                               containers to LPNs
*   L. Coronel       08/28/2007   CR35261       WMS Splitting: Accenture
*                                               Change
*			                                          R3.2 Process Full pallet CPs
*                                               as BPs
*                                               1. Added C pick_type along
*                                               with other picks.
****************************************************************************/

l_generic_child     Container.container_id%TYPE;
l_container_id      Container.container_id%TYPE;
l_distro_nbr        Container_Item.distro_nbr%TYPE;
l_location_id       Container.location_id%TYPE;
l_to_location_id    Container.to_location_id%TYPE;
l_final_location_id Container.final_location_id%TYPE;
l_wave_nbr          Container.wave_nbr%TYPE;
l_container_status  Container.container_status%TYPE;
l_user_id           Container.user_id%TYPE;
l_dest_id           Container.dest_id%TYPE;
l_appt_nbr          Container.appt_nbr%TYPE;
l_appt_line         Container.appt_line%TYPE;
l_po_nbr            Container.po_nbr%TYPE;
l_pick_from_container_id  Container.pick_from_container_id%TYPE;
l_pick_type         Container.pick_type%TYPE;
l_carrier_code      Container.carrier_code%TYPE;
l_service_code      Container.service_code%TYPE;
l_route             Container.route%TYPE;
l_count_children    NUMBER;
l_container_length  Container.container_length%TYPE;
l_container_width   Container.container_width%TYPE;
l_container_height  Container.container_height%TYPE;
l_container_weight  Container.container_weight%TYPE;
l_container_cube    Container.container_cube%TYPE;
l_receipt_date      Container.receipt_date%TYPE;
l_putaway_date      Container.putaway_date%TYPE;
l_rma_nbr						Container.rma_nbr%TYPE;
distro_found        BOOLEAN;

t_item_id	    Container_Item.item_id%TYPE;
t_unit_qty	    Container_Item.unit_qty%TYPE;
t_distro_nbr	    Container_Item.distro_nbr%TYPE;
t_p_stat_child	    Container.container_Id%TYPE;
t_dummy		    VARCHAR(1);
value_found	    BOOLEAN;
l_sort_order				Container.sort_order%TYPE;

-- Start CR35261 - LCC
CURSOR get_pick_to IS
SELECT distinct pick_to_container_id
  FROM pick_directive
 WHERE facility_id = I_FACILITY_ID
/*   AND pick_type = 'CP'*/
   AND pick_type IN ('CP', 'C')
   AND pick_from_container_id = I_GENERIC_TO_CONTAINER;
-- End CR35261

CURSOR container_cursor IS
SELECT container_type, location_id,
       appt_nbr, appt_line, po_nbr, container_length, Container_Width,
       Container_Height, Container_Weight, Container_Cube, User_Id, Receipt_Date,
       putaway_Date,  container_status,
       dest_id,  Bol_nbr,Divert_Ts,Sort_Order,pick_from_container_id,
       wave_nbr, pick_not_after_date, lot_nbr, asn_nbr, ucc_label_id,
       unload_check_flag, address_source, to_location_id,
       break_by_distro, pick_type, final_location_id,
       carrier_code, service_code, route, tracking_id, expedite_flag,
       trouble_marked_cont_status, open_date, close_date, rma_nbr
  FROM Container
 WHERE Facility_ID  = I_FACILITY_ID
   AND Container_ID = I_PICK_TO_CONTAINER_ID;

CURSOR get_generic_cont_children IS
SELECT container_id
  FROM Container
 WHERE Facility_id = I_FACILITY_ID
   AND Master_Container_Id = I_GENERIC_TO_CONTAINER;

CURSOR get_distro_nbr (cid_in IN VARCHAR)IS
SELECT distro_nbr
  FROM Container_Item
 WHERE Facility_id = I_FACILITY_ID
   AND Container_Id = cid_in;
   
CURSOR get_child_dts (container_id_in IN container.container_id%TYPE) IS
SELECT item_id, unit_qty
  FROM container_item
 WHERE facility_Id = I_FACILITY_ID
   AND container_id = container_id_in;
   
CURSOR get_p_status_child (container_id_in IN container.container_id%TYPE) IS
SELECT c.container_id, ci.distro_nbr
  FROM container c, container_item ci
 WHERE c.facility_id = I_FACILITY_ID
   AND c.facility_id = ci.facility_id
   AND c.container_id = ci.container_id
   AND c.container_status = 'P'
   AND c.master_container_id = container_id_in
   AND c.pick_from_container_id = I_GENERIC_TO_CONTAINER
   AND ci.item_id = t_item_id
   AND ci.unit_qty = t_unit_qty;

CURSOR child_container_cursor (container_id_in IN container.container_id%TYPE) IS
SELECT container_id,location_id, appt_nbr, appt_line, 
       container_length, Container_Width,
       Container_Height, Container_Weight, Container_Cube,
       Receipt_date, Putaway_date,
       po_nbr, User_Id,  container_status,
       dest_id, pick_from_container_id,
       wave_nbr, to_location_id,
       pick_type, final_location_id,
       carrier_code, service_code, route, rma_nbr, sort_order
  FROM Container
 WHERE Facility_ID  = I_FACILITY_ID
   AND container_id = container_id_in;
   
CURSOR check_p_master_exists (container_id_in IN container.container_id%TYPE) IS
SELECT 'X'
  FROM Sys.dual
 WHERE EXISTS
       (SELECT pick_to_containeR_id
          FROM pick_directive
         WHERE facility_id = I_FACILITY_ID
           AND pick_to_container_id = container_id_in
           AND pick_from_container_id <> I_GENERIC_TO_CONTAINER);
   
BEGIN

	  -- Check if to_container has children (ticketing) --
  SELECT count(*)
    INTO l_count_children
    FROM Container
   WHERE Master_Container_ID = I_PICK_TO_CONTAINER_ID
     AND facility_id = I_FACILITY_ID;


 IF I_FIRST_SCAN_FLAG = 'Y' THEN -- check if child already transferred --
       FOR ei_rec IN Container_Cursor  LOOP
           -- 
						UPDATE Container
						   SET Container_Type = ei_rec.container_type,                 
						       Location_Id = ei_rec.location_id,                    
						       Appt_Nbr = ei_rec.appt_nbr,                       
						       Appt_Line = ei_rec.appt_line,                      
					--               Po_Nbr = ei_rec.po_nbr, -- Commented to retain po_nbr
						       Container_Length = ei_rec.container_length,               
						       Container_Width = ei_rec.container_width,                
						       Container_Height = ei_rec.container_height,               
						       Container_Weight = ei_rec.container_weight,               
						       Container_Cube = ei_rec.container_cube,                 
						       User_Id = ei_rec.user_id,                        
						       Receipt_Date = ei_rec.receipt_date,                  
						       Putaway_Date = ei_rec.putaway_date,                   
						       Container_Status = ei_rec.container_status,
						       Dest_Id = ei_rec.dest_id,                        
						       Bol_Nbr = ei_rec.bol_nbr,                        
						       Divert_Ts = ei_rec.divert_ts,                      
						       Sort_Order = ei_rec.sort_order,                     
						       Pick_From_Container_Id = ei_rec.pick_from_container_id,         
						       Wave_Nbr = ei_rec.wave_nbr,                       
						       Pick_Not_After_Date = ei_rec.pick_not_after_date,            
						       Lot_Nbr = ei_rec.lot_nbr,                        
						       Asn_Nbr = ei_rec.asn_nbr,                        
						       Ucc_Label_Id = ei_rec.ucc_label_id,                   
						       Unload_Check_Flag = ei_rec.unload_check_flag,              
						       Address_Source = ei_rec.address_source,                 
						       To_Location_Id = ei_rec.to_location_id,                 
						       Break_By_Distro = ei_rec.break_by_distro,                
						       Pick_Type = ei_rec.pick_type,                      
						       Final_Location_Id = ei_rec.final_location_id,              
						       Carrier_Code = ei_rec.carrier_code,                   
						       Service_Code = ei_rec.service_code,                   
						       Route = ei_rec.route,                          
						       Tracking_Id = ei_rec.tracking_id,                 
						       Expedite_Flag = ei_rec.expedite_flag,     
						       Trouble_Marked_Cont_Status = ei_rec.trouble_marked_cont_status,
						       Open_Date = ei_rec.open_date,                     
						       Close_Date = ei_rec.close_date,
						       Rma_nbr = ei_rec.rma_nbr
						 WHERE Facility_ID    = I_FACILITY_ID
						   AND Container_Id         = I_GENERIC_TO_CONTAINER;
				
						    IF l_count_children = 0 THEN 
										--This update is for the generic to container's children
										UPDATE Container
										   SET Container_Type = ei_rec.container_type,                 
										       Location_Id = ei_rec.location_id,                    
										       Appt_Nbr = ei_rec.appt_nbr,                       
										       Appt_Line = ei_rec.appt_line,                      
									--               Po_Nbr = ei_rec.po_nbr, -- Commented to retain po_nbr
										       Container_Status = ei_rec.container_status,
										       Dest_Id = ei_rec.dest_id,                        
										       Bol_Nbr = ei_rec.bol_nbr,                        
										       Divert_Ts = ei_rec.divert_ts,                      
										       Sort_Order = ei_rec.sort_order,                     
										       Pick_From_Container_Id = ei_rec.pick_from_container_id,         
										       Wave_Nbr = ei_rec.wave_nbr,                       
										       Pick_Not_After_Date = ei_rec.pick_not_after_date,            
										       Lot_Nbr = ei_rec.lot_nbr,                        
										       Asn_Nbr = ei_rec.asn_nbr,                        
										       Ucc_Label_Id = ei_rec.ucc_label_id,                   
										       Unload_Check_Flag = ei_rec.unload_check_flag,              
										       Address_Source = ei_rec.address_source,                 
										       To_Location_Id = ei_rec.to_location_id,                 
										       Break_By_Distro = ei_rec.break_by_distro,                
										       Pick_Type = ei_rec.pick_type,                      
										       Final_Location_Id = ei_rec.final_location_id,              
										       Carrier_Code = ei_rec.carrier_code,                   
										       Service_Code = ei_rec.service_code,                   
										       Route = ei_rec.route,                          
										       Tracking_Id = ei_rec.tracking_id,                 
										       Expedite_Flag = ei_rec.expedite_flag,     
										       Trouble_Marked_Cont_Status = ei_rec.trouble_marked_cont_status,
										       Open_Date = ei_rec.open_date,                     
										       Close_Date = ei_rec.close_date,
										       Rma_nbr = ei_rec.rma_nbr
										 WHERE Facility_ID    = I_FACILITY_ID
										   AND Master_Container_Id  = I_GENERIC_TO_CONTAINER;
				
						    END IF;
       END LOOP;       
 END IF;

t_distro_nbr := NULL;
t_unit_qty := 0;
t_item_id := NULL;
t_p_stat_child := NULL;

-- Start CR35261 - LCC
/*IF I_PICK_TYPE IN ('BP', 'CP') THEN*/
IF I_PICK_TYPE IN ('BP', 'CP', 'C') THEN
    /*� Update_Master_Cont_Dest_Loc �*/
    update_master_cont_dest_loc(
        V_RETURN                 => V_RETURN,
        I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER,
        I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
        I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
        I_FACILITY_ID            =>  I_FACILITY_ID,
        I_DEST_ID                =>  I_DEST_ID,
        I_DISTRO_NBR             =>  I_DISTRO_NBR,
        I_WAVE_NBR               =>  I_WAVE_NBR);
END IF;
-- End CR35261

IF I_LABELED_RESERVE = 'Y' AND l_count_children > 0 THEN
  OPEN get_generic_cont_children;
  LOOP
    FETCH get_generic_cont_children INTO l_generic_child;
    EXIT WHEN get_generic_cont_children%NOTFOUND;
    
    OPEN get_child_dts (l_generic_child);
    FETCH get_child_dts INTO t_item_id, t_unit_qty;
    IF get_child_dts%NOTFOUND THEN				
				CLOSE get_child_dts;
				EXIT;
    END IF;
    IF get_child_dts%ISOPEN THEN
    		CLOSE get_child_dts;
    END IF;		
    
    FOR pick_to_rec IN get_pick_to
    LOOP
    
		    OPEN get_p_status_child (pick_to_rec.pick_to_container_id);
		    FETCH get_p_status_child INTO t_p_stat_child, t_distro_nbr;
		    
		    IF get_p_status_child%FOUND THEN
						EXIT;
		    END IF;
		    CLOSE get_p_status_child;
    END LOOP; 
    
    IF get_p_status_child%ISOPEN THEN
    		CLOSE get_p_status_child;
    END IF;
    
    IF t_p_stat_child IS NULL THEN
    		EXIT;    		
    END IF;
    	      	      
    -- start - CR35261 - MShah
    -- Modified to fetch sort order	      	      
    OPEN child_container_cursor (t_p_stat_child);
    FETCH child_container_cursor INTO l_container_id, 
    																	l_location_id, l_appt_nbr,l_appt_line, 
    																	l_container_length, l_container_width, l_container_height,
    																	l_container_weight, l_container_cube,
    																	l_receipt_date, l_putaway_date,
    																	l_po_nbr, l_User_Id,  l_container_status,
    																	l_dest_id, l_pick_from_container_id,
    																	l_wave_nbr, l_to_location_id,
    																	l_pick_type, l_final_location_id,
    																	l_carrier_code, l_service_code, l_route, l_rma_nbr, l_sort_order;
    -- End CR35261 - MShah
    IF child_container_cursor%NOTFOUND THEN
				CLOSE child_container_cursor;
				EXIT;
    ELSE
    	  -- Start - CR35261 - MShah
     		-- Modified to update receipt, putaway dates and sort order
     		UPDATE Container
     		SET Location_Id = l_location_id,                    
   												Appt_Nbr = l_appt_nbr,                       
   												Appt_Line = l_appt_line,                      	
   												User_Id = l_user_id,                        
   												Container_Status = l_container_status,
   												Dest_Id = l_dest_id,                        
   												Pick_From_Container_Id = l_pick_from_container_id,         
   												Wave_Nbr = l_wave_nbr,                       
   												To_Location_Id = l_to_location_id,                 
   												Pick_Type = l_pick_type,                      
   												Final_Location_Id = l_final_location_id,              
   												Carrier_Code = l_carrier_code,                   
   												Service_Code = l_service_code,                   
   												Route = l_route,
   												Rma_nbr = l_rma_nbr,
   												receipt_date = l_receipt_date,
   												putaway_date = l_putaway_date,
   												sort_order = l_sort_order
   			WHERE Facility_ID    = I_FACILITY_ID
     			AND Container_Id   = l_generic_child;
        -- End - CR35261 - MShah
    
   			UPDATE Container_Item
      		 SET distro_nbr = t_distro_nbr
         WHERE Facility_ID    = I_FACILITY_ID
					 AND Container_Id   = l_generic_child;
     
   			DELETE Container
    		 WHERE Container_Id = l_container_id
					 AND Facility_id = I_FACILITY_ID;

    		CLOSE child_container_cursor;

	      l_distro_nbr        := NULL;
	      l_location_id       := NULL;
	      l_to_location_id    := NULL;
	      l_final_location_id := NULL;
	      l_wave_nbr          := NULL;
	      l_container_status  := NULL;
	      l_user_id           := NULL;
	      l_dest_id           := NULL;
	      l_appt_nbr          := NULL;
	      l_appt_line         := NULL;
	      l_po_nbr            := NULL;
	      l_pick_from_container_id := NULL;
	      l_pick_type         := NULL;
	      l_carrier_code      := NULL;
	      l_service_code      := NULL;
	      l_route             := NULL;
	      l_rma_nbr		  := NULL;

    END IF;
    
  END LOOP;
  CLOSE get_generic_cont_children;
    
  FOR pick_to_rec IN get_pick_to
  LOOP

		  OPEN check_p_master_exists (pick_to_rec.pick_to_container_Id);
		  FETCH check_p_master_exists INTO t_dummy;
		  value_found := check_p_master_exists%FOUND;
		  CLOSE check_p_master_exists;
		  
		  IF NOT value_found THEN
		    DELETE Container
		     WHERE Container_ID = pick_to_rec.pick_to_container_id
		       AND Facility_id = I_FACILITY_ID;
		  END IF;
  END LOOP;
  
  -- Delete container_item record of master if to_children records exist --
  -- and labeled_reserve = 'Y'                                           --
  DELETE Container_item
   WHERE Facility_id  = I_FACILITY_ID
     AND Container_id = I_PICK_FROM_CONTAINER_ID;

ELSIF (I_LABELED_RESERVE = 'Y' AND l_count_children = 0) THEN

    -- Start CR35261 - LCC
    /*IF I_PICK_TYPE IN('B','BR','BP', 'CP') THEN -- Defect 348044 appended 'BP'*/
    IF I_PICK_TYPE IN('B','BR','BP', 'CP', 'C') THEN
    -- End CR35261
				OPEN get_distro_nbr (I_PICK_TO_CONTAINER_ID);
				FETCH get_distro_nbr INTO l_distro_nbr;
				distro_found := get_distro_nbr%FOUND;
				CLOSE get_distro_nbr;
				IF distro_found THEN
				   UPDATE Container_Item
				      SET distro_nbr = l_distro_nbr
				    WHERE Facility_ID    = I_FACILITY_ID
				      AND Container_Id IN
							  (SELECT Container_Id
							     FROM Container
							    WHERE facility_id = I_FACILITY_ID
							      AND master_container_id = I_GENERIC_TO_CONTAINER);                                     
				END IF;
    END IF; 

    IF  I_PICK_FROM_CONTAINER_ID !=I_GENERIC_TO_CONTAINER  THEN  
			  DELETE Container_Item
				 WHERE Container_ID = I_GENERIC_TO_CONTAINER
			   AND Facility_id = I_FACILITY_ID;
		      END IF;

		ELSIF I_LABELED_RESERVE = 'N' THEN
  			IF I_PICK_TYPE = 'B' THEN
     				DELETE Container_Item
						WHERE Container_ID = I_PICK_TO_CONTAINER_ID
						  AND Facility_id = I_FACILITY_ID;
    -- Start CR35261 - LCC	
 		/*ELSIF I_PICK_TYPE IN ('BP', 'CP') THEN*/
 		ELSIF I_PICK_TYPE IN ('BP', 'CP', 'C') THEN
    -- End CR35261
     				DELETE Container_Item
						WHERE Container_ID = I_GENERIC_TO_CONTAINER
						  AND Facility_id = I_FACILITY_ID;
  	END IF;
END IF;
	  
EXCEPTION
  WHEN OTHERS THEN
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Transfer Attributes CP',SQLERRM);
     PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
     /*� Log_Error_Roll(SQLCODE, 'Transfer Attributes CP', SQLERRM) �*/
 --   RAISE FRM_TRIGGER_FAILURE;
END;

--


FUNCTION Process_Bulk_Pick(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,  
    I_CONTAINER_QTY            IN NUMBER ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_BULK_GROUP               IN VARCHAR2 ,
    I_CASEPACK                 IN OUT NUMBER ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    P_VERSION                  IN NUMBER ,
    Pick_From_Container_Id_In  IN VARCHAR2,
    Container_Qty_In           IN NUMBER )
  RETURN VARCHAR2
IS
-------------------------------------------------
-- Declarations
-------------------------------------------------
t_in_transit_loc     Location.Location_ID%TYPE;
t_purge_status       VARCHAR2(200);
dummy                container.container_id%TYPE;
t_picked_child       container.container_id%TYPE;
t_cont_nbr           NUMBER;
mld_enabled          VARCHAR2(1);
t_ret_code           VARCHAR2(1);
inv_container_route  EXCEPTION;
-------------------------------------------------

CURSOR casepack_cursor IS
SELECT ROUND(NVL(unit_qty, 0) / NVL(container_qty, 1))
  FROM Container_Item
 WHERE Facility_Id         = I_FACILITY_ID
   AND item_id             = I_ITEM_ID
   AND (Container_Id        = I_PICK_FROM_CONTAINER_ID
        OR Container_Id in (SELECT container_id 
                              FROM container
                             WHERE facility_id = I_FACILITY_ID
                               AND master_container_id = I_PICK_FROM_CONTAINER_ID));
-------------------------------------------------
CURSOR Child_Container_Cursor IS
SELECT Container_Id
  FROM Container
 WHERE Facility_Id         = I_FACILITY_ID
   AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
ORDER BY Sort_Order;
-------------------------------------------------
CURSOR To_Child_Container_Cursor IS
SELECT Container_Id
  FROM Container
 WHERE Facility_Id         = I_FACILITY_ID
   AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
   AND Container_Status    = 'P'
ORDER BY Sort_Order;
-------------------------------------------------

BEGIN
  -----------------------------------------------------------------
  -- check to see if to_container has children                   --
  -- (stock order with ticket type )                             --
  -----------------------------------------------------------------
  OPEN Child_Container_Cursor;
  FETCH Child_Container_Cursor INTO dummy;
  CLOSE Child_Container_Cursor;
  -----------------------------------------------------------------
  OPEN casepack_cursor;
    FETCH casepack_cursor INTO I_CASEPACK;
  CLOSE casepack_cursor;
  ---------------------------------------------------------------
  -- Update the Pick to Container and its children if any
  ---------------------------------------------------------------
  IF I_CONTAINER_QTY = I_PICK_CONTAINER_QTY THEN
     UPDATE Container
        SET Container_Status       = 'D',
            USER_ID              = I_USER
      WHERE Facility_Id          = I_FACILITY_ID
        AND (Container_Id        = I_PICK_TO_CONTAINER_ID OR
             Master_Container_Id = I_PICK_TO_CONTAINER_ID);
  ELSE  -- short pick.  Delete unpicked to containers. --
     IF I_LABELED_PICKING = 'Y' AND I_LABELED_RESERVE = 'Y' THEN
           DELETE FROM Container
            WHERE Facility_Id          = I_FACILITY_ID
              AND Container_Status     = 'P'
              AND Master_Container_Id  = I_PICK_TO_CONTAINER_ID;

     ELSIF I_LABELED_PICKING = 'Y' AND I_LABELED_RESERVE = 'N' THEN
/* 
           AND dummy is NOT NULL THEN
        t_cont_nbr := 0;
        WHILE t_cont_nbr < I_CONTAINER_QTY LOOP
           OPEN To_Child_Container_Cursor;
           FETCH To_Child_Container_Cursor into t_picked_child;
           CLOSE To_Child_Container_Cursor;
           --
           IF t_picked_child IS NOT NULL THEN
              UPDATE Container
                 SET Container_Status     = 'D',
                     USER_ID              = I_USER
               WHERE Facility_Id          = I_FACILITY_ID
                 AND Container_Id         = t_picked_child;
              --
              t_cont_nbr := t_cont_nbr + 1;
           END IF;
        END LOOP;
*/
           DELETE FROM Container
            WHERE Facility_Id          = I_FACILITY_ID
              AND Container_Status     = 'P'
              AND Master_Container_Id  = I_PICK_TO_CONTAINER_ID;
     ELSIF I_LABELED_RESERVE = 'Y' AND I_LABELED_PICKING = 'N' THEN
        -- labeled_picking = 'N' and labeled_reserve = 'Y' --
        -- generic to_child containers are not scanned in  --
        UPDATE Container
           SET Container_Status       = 'D',
               USER_ID              = I_USER
         WHERE Facility_Id          = I_FACILITY_ID
           AND (Container_Id        = I_PICK_TO_CONTAINER_ID OR
                Master_Container_Id = I_PICK_TO_CONTAINER_ID);
     END IF;
  END IF;
  mld_enabled := g_scp (I_FACILITY_ID, 'mld_enabled');
  IF mld_enabled = 'Y' THEN
   	t_ret_code := Assign_Picked_Container_Route (I_FACILITY_ID, I_PICK_TO_CONTAINER_ID, 'Y');
  END IF;
  IF t_ret_code = 'N' THEN
  	RAISE inv_container_route;
  END IF;
  -----------------------------------------------------------------
  IF I_CONTAINER_QTY < I_PICK_CONTAINER_QTY THEN
       -----------------------------------------------------------------
       -- If bulk pick is a bulkgroup pick then adjust the bulkgroup qty's
       -- by calling adjust_bulkgroup, else bulk pick is a regular bulk pick
       -----------------------------------------------------------------
       IF I_BULK_GROUP = 'TRUE' AND I_LABELED_RESERVE = 'N' THEN
          IF P_VERSION = 1 THEN       
            PKG_HOTEL_PICKING_ADF.ADJUST_BULKGROUP( V_RETURN, I_MSG_DISPLAY,I_CONTAINER_QTY ,I_PICK_FROM_CONTAINER_ID ,I_FACILITY_ID ,I_CASEPACK ,I_ORIGINAL_PICK_QTY ,I_PICK_TYPE  );
          ELSE
          	PKG_HOTEL_PICKING_ADF.ADJUST_BULKGROUP2( V_RETURN,I_MSG_DISPLAY,I_CONTAINER_QTY ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_FACILITY_ID ,I_CASEPACK ,I_ORIGINAL_PICK_QTY ,I_PICK_TYPE  );
          /*ns
          		
          		IF P_DISPLAY.HANDLE_MESS THEN 
          				 null/*� Log_Error_Roll(SQLCODE, 'Adjust_Bulkgroup ', SQLERRM) �*
          				RAISE FORM_TRIGGER_FAILURE; 
          		END IF;          						
          ELSE
          	PKG_HOTEL_PICKING_ADF.ADJUST_BULKGROUP2(P_DISPLAY.V_DISPLAY_STRING,I_CONTAINER_QTY ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_FACILITY_ID ,I_CASEPACK ,I_ORIGINAL_PICK_QTY ,I_PICK_TYPE  );
          	IF P_DISPLAY.HANDLE_MESS THEN 
          			 /*� Log_Error_Roll(SQLCODE, 'Adjust_Bulkgroup ', SQLERRM) �*
          			RAISE FORM_TRIGGER_FAILURE; 
          	END IF;*/
            
            
          END IF;
		  IF (V_RETURN IS NOT NULL)  THEN
		    PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Adjust_Bulkgroup',SQLERRM);
		    RETURN ('N');
		  END IF;
		
          UPDATE Container_Item
             SET unit_qty             = unit_qty -
                                        ((I_PICK_CONTAINER_QTY -
                                          I_CONTAINER_QTY) *
                                          I_CASEPACK),
                 distributed_unit_qty = distributed_unit_qty -
                                        ((I_PICK_CONTAINER_QTY -
                                          I_CONTAINER_QTY) *
                                          I_CASEPACK),
                 container_qty        = container_qty -
                                        (I_PICK_CONTAINER_QTY -
                                          I_CONTAINER_QTY)
           WHERE facility_id          = I_FACILITY_ID
             AND container_id         = I_PICK_TO_CONTAINER_ID
             AND item_id              = I_ITEM_ID;
          -------------------------------------------------------
       ELSIF I_BULK_GROUP = 'FALSE' THEN
          IF dummy is NULL THEN
             UPDATE Container_Item
                SET unit_qty             = unit_qty -
                                           ((I_PICK_CONTAINER_QTY -
                                             I_CONTAINER_QTY) *
                                             I_CASEPACK),
                    distributed_unit_qty = distributed_unit_qty -
                                           ((I_PICK_CONTAINER_QTY -
                                             I_CONTAINER_QTY) *
                                             I_CASEPACK),
                    container_qty        = container_qty -
                                           (I_PICK_CONTAINER_QTY -
                                             I_CONTAINER_QTY)
              WHERE facility_id          = I_FACILITY_ID
                AND container_id         = I_PICK_TO_CONTAINER_ID
                AND item_id              = I_ITEM_ID;
          END IF;
          -------------------------------------------------------
          IF I_LABELED_RESERVE = 'N' THEN
             UPDATE Stock_Allocation
                SET distributed_unit_qty = distributed_unit_qty -
                                           ((I_PICK_CONTAINER_QTY -
                                             I_CONTAINER_QTY) *
                                             I_CASEPACK)
              WHERE facility_id = I_FACILITY_ID
                AND distro_nbr  = I_DISTRO_NBR
                AND dest_id     = I_DEST_ID
                AND item_id     = I_ITEM_ID;
          END IF;
       END IF;
       -------------------------------------------------------
    IF I_LABELED_RESERVE = 'N' THEN 
       INSERT INTO inv_adjustment_to_upload
                   ( facility_id,
                     item_id,
                     unit_qty,
                     adjustment_reason_code)
            VALUES (I_FACILITY_ID,
                    I_ITEM_ID,
                    ((I_CONTAINER_QTY -
                      I_PICK_CONTAINER_QTY) *
                      I_CASEPACK),
                    99);
     END IF;
  ELSE -- full pick --
   IF dummy is NOT NULL THEN
   -- Update the distributed unit qty for the children containers on the master container.
      Update Container_Item
         SET distributed_unit_qty = unit_qty
       WHERE facility_id = I_FACILITY_ID
         AND container_id in (SELECT container_id
                                FROM container
                               WHERE facility_id = I_FACILITY_ID
                                 AND master_container_id = I_PICK_TO_CONTAINER_ID);
   ELSIF dummy is NULL THEN
      Update Container_Item
         SET distributed_unit_qty = unit_qty
       WHERE facility_id = I_FACILITY_ID
         AND container_id = I_PICK_TO_CONTAINER_ID;
   END IF;
 END IF;
  -------------------------------------------------------
  -- Delete the Pick Directive record
  -------------------------------------------------------
  DELETE Pick_Directive
   WHERE Facility_Id            = I_FACILITY_ID
     AND Pick_From_Container_Id = I_PICK_FROM_CONTAINER_ID01;
  -----------------------------------------------------------------
  -- Update the To_Container
  -----------------------------------------------------------------
  t_in_transit_loc := g_scp( I_FACILITY_ID, 'in_transit_loc' );
  --
  UPDATE Container
     SET Location_Id      = t_in_transit_loc,
         Container_Status = 'D'
   WHERE Facility_Id      = I_FACILITY_ID
     AND container_id     = I_PICK_TO_CONTAINER_ID;
  -----------------------------------------------------------------
  -- Delete the From_Containers for labeled_picking = Y 
  -----------------------------------------------------------------
   IF I_LABELED_PICKING = 'Y' THEN   
      DELETE Container
       WHERE facility_id           = I_FACILITY_ID
         AND Master_Container_Id   = I_PICK_FROM_CONTAINER_ID;
      ---
      DELETE Container
       WHERE Facility_Id  = I_FACILITY_ID
         AND Container_Id = I_PICK_FROM_CONTAINER_ID01;
   END IF;
   -----------------------------------------------------------------
RETURN('Y');
 
EXCEPTION
WHEN inv_container_route THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONT_ROUTE','E');
    /*� Call_Msg_Window_P ('INV_CONT_ROUTE','E') �*/
   RETURN ('N');
WHEN OTHERS THEN
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_Bulk_Pick',SQLERRM);
    /*� Log_Error_Roll(SQLCODE, 'Process_Bulk_Pick ', SQLERRM) �*/
   RETURN('N');
END Process_Bulk_Pick;
--


--

PROCEDURE Process_Bulk_Replen_Pick(
    V_RETURN                   IN OUT VARCHAR2, 
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_CASEPACK                 IN OUT NUMBER ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_UNITS_PICKED             IN OUT NUMBER ,
    I_WAVE_NBR                 IN NUMBER )
IS
/*
|| Note: A Bulk Replenishment container must be a solid SKU bulk
|| container.
*/
------------------------------------------------------------------
t_replen_qty         Container_Item.Unit_Qty%TYPE;
t_in_transit_loc     Scp.Scp_Current_Val%TYPE;
t_units_picked       NUMBER(9);
oper_failed          EXCEPTION;
t_dummy              VARCHAR2(1);
------------------------------------------------------------------
CURSOR Child_Exist_Cursor IS
SELECT 'x'
  FROM Container
 WHERE Facility_Id         = I_FACILITY_ID
   AND Master_Container_Id = I_PICK_FROM_CONTAINER_ID;
------------------------------------------------------------------
CURSOR Get_Unit_Qty_Cursor IS
SELECT (ci.Unit_Qty/ci.Container_qty), ci.unit_qty
  FROM Container_Item  ci
 WHERE ci.facility_id              = I_FACILITY_ID
   AND ci.container_id             = I_PICK_FROM_CONTAINER_ID;
------------------------------------------------------------------
CURSOR Get_Child_Qty_Cursor IS
SELECT (ci.Unit_Qty/ci.Container_qty), SUM(NVL(ci.unit_qty,0))
  FROM Container_Item  ci, Container c
 WHERE ci.facility_id              = I_FACILITY_ID
   AND c.facility_id               = ci.facility_id
   AND c.container_id              = ci.container_id
   AND c.master_container_id       = I_PICK_FROM_CONTAINER_ID
GROUP BY (ci.unit_qty/ci.container_qty);
------------------------------------------------------------------
CURSOR Child_Container_Cursor IS
SELECT Container_Id
  FROM Container
 WHERE Facility_Id         = I_FACILITY_ID
   AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
ORDER BY Sort_Order;
------------------------------------------------------------------
BEGIN
--------------------------------------------------
  OPEN Child_Exist_Cursor;
  FETCH Child_Exist_Cursor INTO t_dummy;
  CLOSE Child_Exist_Cursor;
  --
  IF t_dummy = 'x' THEN
     OPEN Get_Child_Qty_Cursor;
     FETCH Get_Child_Qty_Cursor INTO I_CASEPACK, t_replen_qty;
     CLOSE Get_Child_Qty_Cursor;
  ELSE
     OPEN Get_Unit_Qty_Cursor;
     FETCH Get_Unit_Qty_Cursor INTO I_CASEPACK, t_replen_qty;
     CLOSE Get_Unit_Qty_Cursor;
  END IF;
  -----------------------------------------------------------------
  -- Check if the user is picking the bulk with all its containers
  -----------------------------------------------------------------
  IF I_PICK_CONTAINER_QTY = I_CONTAINER_QTY THEN
     t_units_picked := t_replen_qty;
     I_UNITS_PICKED := t_units_picked;
  ELSE -- The pick has been adjusted down (shorted).
     t_units_picked := I_CONTAINER_QTY *
                       I_CASEPACK;
     --------------------------------------------------------------
       UPDATE Stock_Allocation
          SET distributed_unit_qty = distributed_unit_qty -
                                     ((I_PICK_CONTAINER_QTY -
                                       I_CONTAINER_QTY) *
                                       I_CASEPACK)
        WHERE facility_id = I_FACILITY_ID
          AND distro_nbr  = I_DISTRO_NBR
          AND dest_id     = I_DEST_ID
          AND item_id     = I_ITEM_ID;
      -------------------------------------------------------
      -- RDM 9.2 Issue #206 - SJS - 17 May 2001. 
      --  Only send inventory adjustment if labeled reserve = 'N'.
       IF I_LABELED_RESERVE = 'N' THEN 
         INSERT INTO inv_adjustment_to_upload
                     ( facility_id,
                       item_id,
                       unit_qty,
                       adjustment_reason_code)
              VALUES (I_FACILITY_ID,
                      I_ITEM_ID,
                      ((I_CONTAINER_QTY -
                        I_PICK_CONTAINER_QTY) *
                        I_CASEPACK),
                      99);
       END IF;
  --------------------------------------------------------------
  END IF;
  I_UNITS_PICKED := t_units_picked;
     -----------------------------------------------------------------
     -- Delete the From_Container.
     -----------------------------------------------------------------
     IF I_LABELED_RESERVE = 'Y' THEN   
      IF  I_PICK_FROM_CONTAINER_ID !=I_GENERIC_TO_CONTAINER  THEN  
        DELETE Container
         WHERE facility_id           = I_FACILITY_ID
           AND Master_Container_Id   = I_PICK_FROM_CONTAINER_ID;
      END IF;
     ELSE
     	  DELETE Container
         WHERE Facility_Id  = I_FACILITY_ID
           AND Container_Id = I_PICK_FROM_CONTAINER_ID01;
     END IF;
     -----------------------------------------------------------------
     -- Update the Pick to Container and its children if any
     ---------------------------------------------------------------
     UPDATE Container
        SET Container_Status       = 'D'
      WHERE Facility_Id            = I_FACILITY_ID
        AND (Container_Id        = I_PICK_TO_CONTAINER_ID OR
             Master_Container_Id = I_PICK_TO_CONTAINER_ID);
     --------------------------------------------------------------
     -- Delete the Pick Directive
     --------------------------------------------------------------
     DELETE Pick_Directive
      WHERE Facility_Id            = I_FACILITY_ID
        AND Pick_From_Container_Id = I_PICK_FROM_CONTAINER_ID01
        AND Wave_Nbr               = I_WAVE_NBR
        AND Distro_Nbr             = I_DISTRO_NBR
        AND Dest_Id                = I_DEST_ID;
     --------------------------------------------------------------
EXCEPTION
WHEN oper_failed THEN
     -- Must roll back work
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_Bulk_Replen_Pick Mark Location',' Units: '  || TO_CHAR(t_units_picked) ||
                          ' Pck To ' ||
                             I_PICK_TO_CONTAINER_ID ||
                          ' From Loc: ' || I_FROM_LOCATION_ID);
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);                    
     /*� Log_Error_Roll (SQLCODE, 'Process_Bulk_Replen_Pick Mark Location',
                     ' Units: '  || TO_CHAR(t_units_picked) ||
                          ' Pck To ' ||
                             I_PICK_TO_CONTAINER_ID ||
                          ' From Loc: ' || I_FROM_LOCATION_ID) �*/
 --  RAISE FRM_TRIGGER_FAILURE;
WHEN OTHERS THEN
     -- Must roll back work
    
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_Bulk_Replen_Pick',SQLERRM ||
                             ' Units: '  || TO_CHAR(t_units_picked) ||
                             ' Pck To ' ||
                               I_PICK_TO_CONTAINER_ID ||
                          ' From Loc: ' || I_FROM_LOCATION_ID);
     PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
    /*� Log_Error_Roll  (SQLCODE, 'Process_Bulk_Replen_Pick', SQLERRM ||
                             ' Units: '  || TO_CHAR(t_units_picked) ||
                             ' Pck To ' ||
                               I_PICK_TO_CONTAINER_ID ||
                          ' From Loc: ' || I_FROM_LOCATION_ID) �*/
--   RAISE FRM_TRIGGER_FAILURE;
END Process_Bulk_Replen_Pick;
--

--
PROCEDURE Go_To_Location(
    V_RETURN                 IN OUT VARCHAR2, 
    I_GOTO_FIELD             IN OUT VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_DEST_ID                IN NUMBER ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_OLD_TO_CID             IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_SUGGESTED_LOC          IN OUT VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER )
IS
/*
|| Move to the To Location block and set up
|| the suggested location field.
*/

/*****************************************************************************************
* Author     	 Date         Defect#         Change Description
*--------------------------------------------------------------------------
*  L. Coronel  07/06/2007   CR35260         WMS Splitting: Accenture Change
*                                           R3.1 - Process full pallet CPs as BP. 
*  L. Coronel               CR35261         WMS Splitting: Accenture Change
*			                                      R3.2 Process Full pallet Cs as BPs

*****************************************************************************************/

CURSOR container_cursor (cid_in IN VARCHAR2) IS
SELECT To_Location_ID
  FROM Container
 WHERE Facility_ID  = I_FACILITY_ID
   AND Container_ID = cid_in;
--
CURSOR print_and_apply_cursor IS
SELECT Scp_Current_Val
  FROM Scp
 WHERE Scp_Name = 'print_and_apply'
   AND Facility_ID = I_FACILITY_ID;

BEGIN
   /*� GO_BLOCK ('TO_LOCATION') �*/
   I_GOTO_FIELD := 'TO_LOCATION';
   /*� CLEAR_BLOCK(NO_VALIDATE) �*/
   
  IF I_LABELED_PICKING = 'Y' THEN
     IF I_SUGGESTED_LOC IS NULL THEN
        OPEN container_cursor (I_PICK_TO_CONTAINER_ID);
        FETCH container_cursor INTO I_SUGGESTED_LOCATION;
        CLOSE container_cursor;
     ELSE
     	  I_SUGGESTED_LOCATION := I_SUGGESTED_LOC;
     END IF;
  ELSE -- labeled picking = 'N'   
     IF I_PICK_TYPE = 'B' THEN
     
        OPEN print_and_apply_cursor;
        FETCH print_and_apply_cursor INTO I_SUGGESTED_LOCATION;
        CLOSE print_and_apply_cursor;
        
     ELSIF I_SUGGESTED_LOC IS NULL THEN
     
        OPEN container_cursor(I_OLD_TO_CID);
        FETCH container_cursor INTO I_SUGGESTED_LOCATION;
        CLOSE container_cursor;
        
     ELSE
     	  I_SUGGESTED_LOCATION := I_SUGGESTED_LOC;
     END IF;
    
     -- Start - CR35261 - MShah
     IF I_PICK_TYPE = 'C' THEN
		 		I_SUGGESTED_LOCATION := g_scp(I_FACILITY_ID, 'ship_sort_loc');     
     END IF;		
     -- End - CR35261 - MShah

     --CR35260 - Deleting P status master only if pick type is not CP
     --          since the ones associated with CPs are already deleted
     --          by transfer_attributes_cp procedure.
     /*IF I_PICK_TYPE <> 'CP' THEN*/
     IF I_PICK_TYPE NOT IN ('CP', 'C') THEN
     -- CR 35261
     	  -- Delete system generated master pick_to_cid --
        DELETE FROM Container
         WHERE Container_Id = I_OLD_TO_CID
           AND Facility_id = I_FACILITY_ID;
     END IF;
     	  
        /*DELETE FROM Container
         WHERE Container_Id = I_OLD_TO_CID
           AND Facility_id = I_FACILITY_ID;*/
    --           
  END IF;
  I_SUGGESTED_LOC := NULL;
EXCEPTION
 WHEN OTHERS THEN
     /*� Log_Error(SQLCODE, 'Go_To_Location', SQLERRM ||
                      ' Wave ' || TO_CHAR(I_WAVE_NBR) ||
                      ' Distr ' || I_DISTRO_NBR ||
                      ' Pck Frm ' ||
                      I_PICK_FROM_CONTAINER_ID ||
                      ' Dest ' || TO_CHAR(I_DEST_ID) ||
                      ' Pck To ' ||
                      I_PICK_TO_CONTAINER_ID) �*/
   PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'v',SQLERRM||
                      ' Wave ' || TO_CHAR(I_WAVE_NBR) ||
                      ' Distr ' || I_DISTRO_NBR ||
                      ' Pck Frm ' ||
                      I_PICK_FROM_CONTAINER_ID ||
                      ' Dest ' || TO_CHAR(I_DEST_ID) ||
                      ' Pck To ' ||
                      I_PICK_TO_CONTAINER_ID);
   PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
                  
END Go_To_Location;
--
--
FUNCTION Process_Bulk_Pick_C(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2, 
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    I_CONTAINER_QTY          IN NUMBER ,
    I_FROM_LOCATION_ID       IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_CASEPACK               IN OUT NUMBER ,
    I_EVENT_CODE             IN NUMBER ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_PICK_CONTAINER_QTY     IN NUMBER ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,
    P_PM_ALLOW_INTRLVG       IN VARCHAR2 ,
    P_VERSION              IN NUMBER )
  RETURN VARCHAR2
IS
/****************************************************************************
* Author     	Date      Defect#    Version	Change Description
*----------------------------------------------------------------------------
* L. Coronel  09/14/2007   CR35261          WMS Splitting: Accenture Change
*	                                          R3.2 Process Full pallet Cs as
*                                           BPs
* L.Coronel            04/16/2008           1. MVDC � High Bay Hotel 
*                                           Capabilities: Modify the
*                                           call to insert_pick
*                                           _history to log a
*                                           flag for interleaving.
****************************************************************************/

-------------------------------------------------
-- Declarations
-------------------------------------------------
t_in_transit_loc     Location.Location_ID%TYPE;
dummy                container.container_id%TYPE;
t_ret_code           VARCHAR2(1);
inv_container_route  EXCEPTION;
l_item_id            pick_directive.item_id%TYPE;
l_count              NUMBER(8);	
-------------------------------------------------
CURSOR casepack_cursor(item_id_in IN container_item.item_id%TYPE) IS
SELECT ROUND(NVL(unit_qty, 0) / NVL(container_qty, 1))
  FROM Container_Item
 WHERE Facility_Id         = I_FACILITY_ID
   AND item_id             = item_id_in
   AND (Container_Id        = I_PICK_FROM_CONTAINER_ID
        OR Container_Id in (SELECT container_id 
                              FROM container
                             WHERE facility_id = I_FACILITY_ID
                               AND master_container_id = I_PICK_FROM_CONTAINER_ID));
-------------------------------------------------
CURSOR Child_Container_Cursor IS
SELECT Container_Id
  FROM Container
 WHERE Facility_Id         = I_FACILITY_ID
   AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
ORDER BY Sort_Order;
-------------------------------------------------

CURSOR get_pd_c_recs1 IS
   SELECT *
     FROM pick_directive
    WHERE facility_id = I_FACILITY_ID 
      AND pick_from_container_id =
          I_PICK_FROM_CONTAINER_ID
      AND pick_type = 'C';
      
CURSOR get_pd_c_recs2 IS
   SELECT *
     FROM pick_directive
    WHERE facility_id = I_FACILITY_ID 
      AND pick_from_container_id =
          I_PICK_FROM_CONTAINER_ID
      AND wave_nbr = I_WAVE_NBR
      AND pick_type = 'C';
      

CURSOR get_item_id1 IS
   SELECT item_id
     FROM pick_directive
    WHERE facility_id = I_FACILITY_ID
      AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID;

CURSOR get_item_id2 IS
   SELECT item_id
     FROM pick_directive
    WHERE facility_id = I_FACILITY_ID
      AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
      AND wave_nbr = I_WAVE_NBR;
      

-------------------------------------------------------------------
BEGIN
	
	-- Start CR35261 - LCC: issue# 88 Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
   --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
     DELETE from ROSS_EVENTCODE_USERID_TMP; 
     COMMIT;

  IF I_PICK_TYPE IN ('CP','C','BP') THEN
     INSERT INTO ROSS_EVENTCODE_USERID_TMP
     (
      EVENT_CD,
      USER_ID,
      LOCATION_ID
     )
     VALUES (I_EVENT_CODE, I_USER, I_CONF_LOCATION_ID);
  END IF;
-- End CR35261 - LCC: issue# 88
	
	-----------------------------------------------------------------
  -- check to see if to_container has children                   --
  -- (stock order with ticket type )                             --
  -----------------------------------------------------------------
  OPEN Child_Container_Cursor;
  FETCH Child_Container_Cursor INTO dummy;
  CLOSE Child_Container_Cursor;
  -----------------------------------------------------------------
  IF P_VERSION = 1 THEN 
  		OPEN get_item_id1;
  		FETCH get_item_id1 INTO l_item_id;
  		CLOSE get_item_id1;
  ELSE
  		OPEN get_item_id2;
  		FETCH get_item_id2 INTO l_item_id;
  		CLOSE get_item_id2;	
  END IF;		

  OPEN casepack_cursor(l_item_id);
  FETCH casepack_cursor INTO I_CASEPACK;
  CLOSE casepack_cursor;
  ---------------------------------------------------------------
  -- Update the Pick to Container and its children if any
  ---------------------------------------------------------------
  IF I_CONTAINER_QTY = I_PICK_CONTAINER_QTY THEN
     UPDATE Container
        SET Container_Status       = 'D',
            USER_ID              = I_USER
      WHERE Facility_Id          = I_FACILITY_ID
        AND (Container_Id        = I_PICK_TO_CONTAINER_ID OR
             Master_Container_Id = I_PICK_TO_CONTAINER_ID);
  ELSE  -- short pick.  Delete unpicked to containers. --
     IF I_LABELED_PICKING = 'Y' THEN
           DELETE FROM Container
            WHERE Facility_Id          = I_FACILITY_ID
              AND Container_Status     = 'P'
              AND Master_Container_Id  = I_PICK_TO_CONTAINER_ID;
                
     ELSIF I_LABELED_RESERVE = 'Y' AND I_LABELED_PICKING = 'N' THEN
        -- labeled_picking = 'N' and labeled_reserve = 'Y' --
        -- generic to_child containers are not scanned in  --

        UPDATE Container
           SET Container_Status       = 'D',
               USER_ID              = I_USER
         WHERE Facility_Id          = I_FACILITY_ID
           AND (Container_Id        = I_PICK_TO_CONTAINER_ID OR
                Master_Container_Id = I_PICK_TO_CONTAINER_ID);
     END IF;

  END IF;
  
 IF dummy is NOT NULL THEN
 -- Update the distributed unit qty for the children containers on the master container.
    Update Container_Item
       SET distributed_unit_qty = unit_qty
     WHERE facility_id = I_FACILITY_ID
       AND container_id in (SELECT container_id
                              FROM container
                             WHERE facility_id = I_FACILITY_ID
                               AND master_container_id = I_PICK_TO_CONTAINER_ID);
 ELSIF dummy is NULL THEN
    Update Container_Item
       SET distributed_unit_qty = unit_qty
     WHERE facility_id = I_FACILITY_ID
       AND container_id = I_PICK_TO_CONTAINER_ID;
 END IF;
 
 IF P_VERSION = 1 THEN 
 		FOR pd_rec IN get_pd_c_recs1
 		LOOP	  
	  		Insert_Pick_History (I_FACILITY_ID,
	      		                 pd_rec.distro_nbr,
	                          		pd_rec.item_id,
	                          		pd_rec.dest_id,
	                                 		sysdate,
	                              		I_USER,
	                        		pd_rec.pick_type,
	                         		pd_rec.unit_qty,
	             		pd_rec.pick_to_container_id,
	           		pd_rec.pick_from_container_id,
	                             		pd_rec.zone,
	             		I_FROM_LOCATION_ID,
	               		pd_rec.pick_container_qty,
	                         		pd_rec.wave_nbr,
	    		/* Start CO43433 LCoronel - 1 */
	    		/*I_EVENT_CODE || ':' || 'HH_BULK_PK_ACROSS_WV_S.process_bulk_pick_c'); */
        		P_PM_ALLOW_INTRLVG || ':' ||
           		I_EVENT_CODE || ':' ||
    		'HH_BULK_PIK_ACROSS_WV_S.process_bulk_pick_c');
     		/* End CO43433 LCoronel - 1 */	
		END LOOP;
 ELSE
 		FOR pd_rec IN get_pd_c_recs2
 		LOOP	  
	  		Insert_Pick_History (I_FACILITY_ID,
	      		                 pd_rec.distro_nbr,
	                          		pd_rec.item_id,
	                          		pd_rec.dest_id,
	                                 		sysdate,
	                              		I_USER,
	                        		pd_rec.pick_type,
	                         		pd_rec.unit_qty,
	             		pd_rec.pick_to_container_id,
	           		pd_rec.pick_from_container_id,
	                             		pd_rec.zone,
	             		I_FROM_LOCATION_ID,
	               		pd_rec.pick_container_qty,
	                         		pd_rec.wave_nbr,
	    		/* Start CO43433 LCoronel - 1 */
	    		/*I_EVENT_CODE || ':' || 'HH_BULK_PICK_S.process_bulk_pick_c'); */
        		P_PM_ALLOW_INTRLVG || ':' ||
           		I_EVENT_CODE || ':' ||
    		'HH_BULK_PICK_S.process_bulk_pick_c');
     		/* End CO43433 LCoronel - 1 */	
		END LOOP;			
	END IF;
	
  DELETE Pick_Directive
   WHERE facility_id = I_FACILITY_ID 
     AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
     AND pick_type = 'C';
  

-------------------------------------------------------------
  -- Update the To_Container
  -----------------------------------------------------------------
  t_in_transit_loc := g_scp( I_FACILITY_ID, 'in_transit_loc' );
  --
  UPDATE Container
     SET Location_Id      = t_in_transit_loc,
         Container_Status = 'D'
   WHERE Facility_Id      = I_FACILITY_ID
     AND container_id     = I_PICK_TO_CONTAINER_ID;
   -----------------------------------------------------------------
RETURN('Y');
 
EXCEPTION
WHEN inv_container_route THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONT_ROUTE','E');
    /*� Call_Msg_Window_P ('INV_CONT_ROUTE','E') �*/
   RETURN ('N');
WHEN OTHERS THEN
   PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_Bulk_Pick_C',SQLERRM);
  /*� Log_Error_Roll(SQLCODE, 'Process_Bulk_Pick_C ', SQLERRM) �*/
   RETURN('N');
END Process_Bulk_Pick_C; 
--
PROCEDURE User_Done_Proc(
/*****************************************************************************************
* Author     	Date         Defect#    Version	 Change Description
*--------------------------------------------------------------------------
* Abhishek     09/22/2006  N/A        1.1      Captured Location_ID, Event_Code and
* Dutta.                                       User_ID, as a part of requirement 
*                                              Infosys technologies R3 
*																						   'BP pick completed' -  Container History
*
* L. Coronel   07/06/2007  CR35260             WMS Splitting: Accenture Change
*                                              R3.1 - Process full pallet CPs as BP. 
* L. Coronel   08/28/2007  CR35261             WMS Splitting: Accenture Change
*                                               R3.2 Process Full pallet Cs
*                                               as BPs
*                                               1. Includes C picks along
*                                               with other picks.
*****************************************************************************************/
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,    
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT  VARCHAR2 ,
    I_TO_LOCATION_ID           IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2,
    I_BULK_GROUP               IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER ,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER ,
    I_UNITS_PROCESSED          IN OUT NUMBER ,    
    I_UNITS_PICKED             IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_DISTRO_TS                IN DATE ,
    I_EVENT_CODE               IN NUMBER ,
    I_FIRST_SCAN_FLAG          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_OLD_TO_CID               IN OUT VARCHAR2 ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID01   IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_SUGGESTED_LOC            IN OUT VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER ,
    I_ZONE                     IN VARCHAR2 ,
    P_PM_ALLOW_INTRLVG         IN VARCHAR2,
    P_VERSION                  IN NUMBER)
IS

	status       						VARCHAR2(60);
	l_child_nbr  						NUMBER;
	l_final_loc  						container.final_location_id %TYPE;
	t_mast_dest_id					Container.dest_id%TYPE;
	t_mast_to_loc_id				Container.to_location_id%TYPE;
	t_mast_final_loc_id			Container.final_location_id%TYPE;
	oper_failure 						EXCEPTION;
	--CR439 Ross Stores
	l_ret_status						USER_MESSAGE.Message_Text%TYPE;
	l_to_location_id_out		WIP_Codes.location_id%TYPE;
	return_code_out					VARCHAR2(200);
	program_name_out				VARCHAR2(200);
	t_temp_final						container.location_id%TYPE;
	t_temp_sug							container.location_id%TYPE;
	l_out_message1          VARCHAR2(500);
	l_out_message2          VARCHAR2(2000);
	
	/* Begin Change Infosys technologies R3 'BP pick completed' -  Container History     09/22/2006*/
  l_loc_id						 ROSS_EVENTCODE_USERID_TMP.location_id%TYPE;	  
  /* End Change Infosys technologies R3 'BP pick completed' -  Container History     09/22/2006*/

	CURSOR count_from_child IS
  SELECT COUNT(container_id), final_location_id
    FROM Container
   WHERE Facility_id = I_FACILITY_ID
     AND Master_Container_id = I_GENERIC_TO_CONTAINER
   GROUP BY final_location_id;

	CURSOR master_container_cursor IS
	SELECT dest_id, to_location_id, final_location_id 
	  FROM container
	 WHERE Facility_ID  = I_FACILITY_ID
	   AND Container_id = I_PICK_TO_CONTAINER_ID;
   
	--CR439 Ross Stores
	CURSOR get_children IS
	SELECT container_id, final_location_id
	  FROM container
	 WHERE facility_id = I_FACILITY_ID
	   AND master_container_id = I_GENERIC_TO_CONTAINER;

BEGIN
/* Begin Change Infosys technologies R3 'BP pick completed' -  Container History     09/22/2006*/   		
/* Code Added */
/* Call the proc to get the location id*/
  IF I_TO_LOCATION_ID IS NULL THEN
    get_container_location (I_FACILITY_ID, I_GENERIC_TO_CONTAINER, l_loc_id);
  ELSE
  	l_loc_id := I_TO_LOCATION_ID;
  END IF;	
   --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
     DELETE from ROSS_EVENTCODE_USERID_TMP; 
     COMMIT;

   IF I_PICK_TYPE IN ('CP','C','BP') THEN
      
      INSERT INTO ROSS_EVENTCODE_USERID_TMP
     (
      EVENT_CD,
      USER_ID,
      LOCATION_ID
     )
     VALUES (I_EVENT_CODE, I_USER, l_loc_id);

   END IF;

  IF I_LABELED_PICKING = 'N' THEN
     --------------------------------------------------------------------
     -- Transfer information from the system generated to the generic,   
     -- then remove the system generated container.
     --------------------------------------------------------------------
     
     -- CR35260 - Calling new procedure to transfer attributes for full CP pallet
     /* Transfer_Attributes; */
     
     -- Start CR35261
     /*IF I_PICK_TYPE <> 'CP' THEN*/
     IF I_PICK_TYPE NOT IN ('CP', 'C') THEN
     		 /*� Transfer_Attributes �*/
         transfer_attributes(
            V_RETURN                 ,
            I_DEST_ID                 ,
            I_DISTRO_NBR              ,
            I_GENERIC_TO_CONTAINER    ,
            I_ITEM_ID                 ,
            I_PICK_FROM_CONTAINER_ID  ,
            I_PICK_TO_CONTAINER_ID    ,
            I_FACILITY_ID             ,
            I_FIRST_SCAN_FLAG         ,
            I_LABELED_RESERVE         ,
            I_PICK_TYPE               ,
            I_WAVE_NBR                ,
            P_VERSION               );
     ELSE		
     		 /*� Transfer_Attributes_CP �*/
         transfer_attributes_cp(
            V_RETURN                 ,
            I_DEST_ID                 ,
            I_DISTRO_NBR              ,
            I_WAVE_NBR                ,
            I_GENERIC_TO_CONTAINER    ,
            I_PICK_FROM_CONTAINER_ID  ,
            I_PICK_TO_CONTAINER_ID    ,
            I_FACILITY_ID             ,
            I_FIRST_SCAN_FLAG         ,
            I_LABELED_RESERVE         ,
            I_PICK_TYPE               );
     END IF;
     -- End CR35261
     
     -- update container_item record only if there are no children  --
     -- in the generic container                                    --
     -- otherwise, the master container_item record will be deleted --
     -- as the pick_to_container records are deleted                --
     OPEN count_from_child;
     FETCH count_from_child INTO l_child_nbr, l_final_loc;
     CLOSE count_from_child;
     --
     IF l_child_nbr = 0 THEN
        UPDATE container_item
           SET Container_Id = I_GENERIC_TO_CONTAINER
         WHERE Container_Id = I_PICK_TO_CONTAINER_ID
           AND Facility_id = I_FACILITY_ID;
     ELSIF l_child_nbr > 0 THEN
     	  -- 03/05/02 SWS Added logic to copy attributes from
     	  -- system generated pick to master for B and BP pick type.
    	  /*IF I_PICK_TYPE IN ('B', 'BP') AND */
        IF I_PICK_TYPE IN ('B', 'BP', 'CP', 'C') AND
    	  	 I_FIRST_SCAN_FLAG = 'N' THEN
     	    OPEN master_container_cursor;
 	   			FETCH master_container_cursor INTO t_mast_dest_id,
 	   						t_mast_to_loc_id,
 	   						t_mast_final_loc_id; 	   		
 	   			IF master_container_cursor%FOUND THEN
 	     			UPDATE Container
 	        		 SET dest_id 				  	= t_mast_dest_id,
 	            		 to_location_id 		= t_mast_to_loc_id,
 	            		 final_location_id  = t_mast_final_loc_id
 	      		 WHERE Facility_id  = I_FACILITY_ID
 	       		   AND Container_ID = I_GENERIC_TO_CONTAINER;
 	   			END IF; 
 	   			CLOSE master_container_cursor;	
     	  ELSE 

          UPDATE container
             SET final_location_id = l_final_loc
           WHERE Facility_id = I_FACILITY_ID
             AND Container_id = I_GENERIC_TO_CONTAINER;
        END IF;
     END IF;
  
  	 --CR439 Ross Stores
     t_temp_sug := NULL;
     t_temp_final := NULL;
     FOR child_rec IN get_children LOOP
     	 --
     	 IF t_temp_final IS NULL THEN
     	 		t_temp_final := child_rec.final_location_id;
     	 END IF;
     	 --
     	 l_ret_status := determine_container_to_loc (I_FACILITY_ID,
					  			   	  													 child_rec.container_id, 
					  			   	  													 child_rec.final_location_id,
					    																		 NULL, 
					    																	 	 l_to_location_id_out,
																									 return_code_out, 
																									 program_name_out);
			 --
			 IF t_temp_sug IS NULL OR t_temp_final <> l_to_location_id_out THEN
     	 		t_temp_sug := l_to_location_id_out;
			 END IF;
		   --
     END LOOP;
 		 --
     I_SUGGESTED_LOC := t_temp_sug;

     -- CR35260 - CP pick to can be associated to multiple generic_to_container.
     
     
     /* UPDATE Container
           SET Master_Container_Id = I_GENERIC_TO_CONTAINER
         WHERE Master_Container_Id = I_PICK_TO_CONTAINER_ID
           AND Facility_id = I_FACILITY_ID; */
          
     -- Start CR35261
     /*IF I_PICK_TYPE <> 'CP' THEN*/
     IF I_PICK_TYPE NOT IN ('CP', 'C') THEN
        -- Set master cid of system generated pick_to_cid to generic_cid -- 
        UPDATE Container
           SET Master_Container_Id = I_GENERIC_TO_CONTAINER
         WHERE Master_Container_Id = I_PICK_TO_CONTAINER_ID
           AND Facility_id = I_FACILITY_ID;
     END IF;
     -- End CR35261
     I_OLD_TO_CID := I_PICK_TO_CONTAINER_ID;
     I_PICK_TO_CONTAINER_ID := I_GENERIC_TO_CONTAINER;

  END IF;
  --
  IF I_PICK_TYPE = 'B' THEN
    IF   /*� Process_Bulk_Pick( I_PICK_FROM_CONTAINER_ID01, I_PICK_CONTAINER_QTY ) �*/ 
      Process_Bulk_Pick(
          V_RETURN                   => V_RETURN,
          I_MSG_DISPLAY              => I_MSG_DISPLAY,  
          I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
          I_ITEM_ID                  =>  I_ITEM_ID,
          I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
          I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
          I_FACILITY_ID              =>  I_FACILITY_ID,
          I_USER                     =>  I_USER,
          I_BULK_GROUP               =>  I_BULK_GROUP,
          I_CASEPACK                 =>  I_CASEPACK,
          I_DEST_ID                  =>  I_DEST_ID,
          I_DISTRO_NBR               =>  I_DISTRO_NBR,
          I_LABELED_PICKING          =>  I_LABELED_PICKING,
          I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
          I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
          I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
          I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID01,
          I_PICK_TYPE                =>  I_PICK_TYPE,
          P_VERSION                  =>  P_VERSION,
          Pick_From_Container_Id_In  =>  I_PICK_FROM_CONTAINER_ID,
          Container_Qty_In           =>  I_CONTAINER_QTY )      = 'N' THEN
               RAISE oper_failure;
    END IF;
  -- Start CR35261 - LCC
  ELSIF I_PICK_TYPE = 'C' THEN
     IF  /*� Process_Bulk_Pick_C �*/
         Process_Bulk_Pick_C(
            V_RETURN                 => V_RETURN,
            I_MSG_DISPLAY            => I_MSG_DISPLAY, 
            I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID ,
            I_CONTAINER_QTY          => I_CONTAINER_QTY ,
            I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID ,
            I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID,
            I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID,
            I_FACILITY_ID            => I_FACILITY_ID,
            I_USER                   => I_USER,
            I_CASEPACK               => I_CASEPACK,
            I_EVENT_CODE             => I_EVENT_CODE,
            I_LABELED_PICKING        => I_LABELED_PICKING,
            I_LABELED_RESERVE        => I_LABELED_RESERVE,
            I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
            I_PICK_TYPE              => I_PICK_TYPE,
            I_WAVE_NBR               => I_WAVE_NBR,
            P_PM_ALLOW_INTRLVG       => P_PM_ALLOW_INTRLVG,
            P_VERSION                => P_VERSION)= 'N' THEN
     	 RAISE oper_failure;
     END IF;                    
  -- End Cr35261 
  -- CR35260 - Process full pallet CP pick as BP.
  /*  ELSIF I_PICK_TYPE = 'BP' THEN*/
  ELSIF I_PICK_TYPE IN ('BP', 'CP') THEN
  --
  
      IF P_VERSION = 1 THEN 
      		PKG_HOTEL_PICKING_ADF.PROCESS_UPS_BULK_PICK(V_RETURN, I_MSG_DISPLAY ,I_CONTAINER_QTY ,I_FROM_LOCATION_ID ,I_GENERIC_TO_CONTAINER ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_PICK_TO_CONTAINER_ID ,I_FACILITY_ID ,I_CASEPACK ,I_DEST_ID ,I_DISTRO_NBR ,I_DISTRO_TS ,I_EVENT_CODE ,I_PICK_CONTAINER_QTY ,I_PICK_FROM_CONTAINER_ID01 ,I_PICK_TO_CONTAINER_ID01 ,I_PICK_TYPE ,I_WAVE_NBR ,I_ZONE ,I_USER ,P_PM_ALLOW_INTRLVG, l_out_message1, l_out_message2  );

--      		IF P_DISPLAY.HANDLE_MESS THEN     			
--      			 null/*� Log_Error_Roll(SQLCODE, l_out_message1,l_out_message2) �*/     			
--      			RAISE FORM_TRIGGER_FAILURE; 
--      	  END IF;
      		
      ELSE
      		PKG_HOTEL_PICKING_ADF.PROCESS_UPS_BULK_PICK2(  V_RETURN, I_MSG_DISPLAY ,I_CONTAINER_QTY ,I_FROM_LOCATION_ID ,I_GENERIC_TO_CONTAINER ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_PICK_TO_CONTAINER_ID ,I_FACILITY_ID ,I_CASEPACK ,I_DEST_ID ,I_DISTRO_NBR ,I_DISTRO_TS ,I_EVENT_CODE ,I_PICK_CONTAINER_QTY ,I_PICK_FROM_CONTAINER_ID01 ,I_PICK_TO_CONTAINER_ID01 ,I_PICK_TYPE ,I_WAVE_NBR ,I_ZONE ,I_USER ,P_PM_ALLOW_INTRLVG, l_out_message1, l_out_message2  );
--      		IF P_DISPLAY.HANDLE_MESS THEN     			
--      			 null/*� Log_Error_Roll(SQLCODE, l_out_message1, l_out_message2) �*/      			
--      			RAISE FORM_TRIGGER_FAILURE; 
--      	  END IF;
      END IF;
--	  
	  IF (l_out_message2 IS NOT NULL ) THEN
	     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, l_out_message1, 'User_Done_Proc',l_out_message2);
	     RETURN;
	  END IF;
  ELSIF I_PICK_TYPE = 'BR' THEN
       /*� PROCESS_BULK_REPLEN_PICK �*/
    PROCESS_BULK_REPLEN_PICK(
      V_RETURN                   => V_RETURN, 
      I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
      I_FROM_LOCATION_ID         => I_FROM_LOCATION_ID ,
      I_GENERIC_TO_CONTAINER     => I_GENERIC_TO_CONTAINER ,
      I_ITEM_ID                  => I_ITEM_ID ,
      I_PICK_FROM_CONTAINER_ID   => I_PICK_FROM_CONTAINER_ID ,
      I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID ,
      I_FACILITY_ID              => I_FACILITY_ID ,
      I_CASEPACK                 => I_CASEPACK ,
      I_DEST_ID                  => I_DEST_ID ,
      I_DISTRO_NBR               => I_DISTRO_NBR ,
      I_LABELED_RESERVE          => I_LABELED_RESERVE ,
      I_PICK_CONTAINER_QTY       => I_PICK_CONTAINER_QTY ,
      I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 ,
      I_UNITS_PICKED             => I_UNITS_PICKED ,
      I_WAVE_NBR                 => I_WAVE_NBR );
    
  END IF;
  --
  I_OPERATIONS_PERFORMED :=
     NVL(I_OPERATIONS_PERFORMED, 0) + 1;
  I_CONTAINERS_PROCESSED :=
     NVL(I_CONTAINERS_PROCESSED, 0) +
     I_CONTAINER_QTY;
  I_UNITS_PROCESSED :=
     NVL(I_UNITS_PROCESSED, 0) +
     (I_CONTAINER_QTY * I_CASEPACK);
  --------------------------------------------
  COMMIT;
  --------------------------------------------
EXCEPTION
WHEN oper_failure THEN
  /*� Cancel_Work �*/
  ROLLBACK;
  
   /*� Call_Msg_Window_P('OPER_FAILURE') �*/
   I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'OPER_FAILURE');
   PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
 --  RAISE FRM_TRIGGER_FAILURE;
WHEN OTHERS THEN
    /*� Log_Error_Roll(SQLCODE, 'User_Done_Proc ', SQLERRM) �*/
   PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'User_Done_Proc',SQLERRM);
   PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
 --  RAISE FRM_TRIGGER_FAILURE;

 
END User_Done_Proc;
--
--
PROCEDURE CHECK_CATCH_WEIGHT(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,   
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    P_VERSION                IN NUMBER,    
    I_BULK_GROUP             IN VARCHAR2,
    I_ORIGINAL_PICK_QTY      IN NUMBER,
    I_CONTAINER_QTY          IN NUMBER,
    I_FROM_LOCATION_ID       IN VARCHAR2,
    I_CONTAINERS_PROCESSED   IN OUT NUMBER,
    I_OPERATIONS_PERFORMED   IN OUT NUMBER,
    I_UNITS_PROCESSED        IN OUT NUMBER,
    I_CASEPACK               IN OUT NUMBER,
    I_DISTRO_NBR             IN VARCHAR2,
    I_DISTRO_TS              IN DATE,
    I_FIRST_SCAN_FLAG        IN VARCHAR2,
    I_OLD_TO_CID             IN OUT VARCHAR2,
    I_PICK_CONTAINER_QTY     IN NUMBER,
    I_ZONE                   IN VARCHAR2, 
    I_PM_ALLOW_INTRLVG       IN VARCHAR2,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2 ,
    I_SUGGESTED_LOC          IN OUT VARCHAR2 ,
    I_TO_LOCATION_ID         IN VARCHAR2 ,
    I_TOTAL_WEIGHT           IN NUMBER ,
    I_CALLING_BLOCK          IN OUT VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_DEST_ID                IN NUMBER ,
    I_EVENT_CODE             IN NUMBER ,
    I_FIRST_WEIGHT_FLAG      IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_UNITS_PICKED           IN OUT NUMBER ,
    I_WAVE_NBR               IN NUMBER ,    
    I_GOTO_FIELD             IN OUT VARCHAR2,
    calling_block_in         IN VARCHAR2)
IS

L_catch_weight_ind    ITEM_MASTER.CATCH_WEIGHT%TYPE;

CURSOR check_item_cursor IS
   SELECT catch_weight
     FROM item_master 
    WHERE facility_id = I_FACILITY_ID
      AND item_id     = I_ITEM_ID;

BEGIN
      OPEN check_item_cursor;
      FETCH check_item_cursor INTO L_catch_weight_ind;
      IF L_catch_weight_ind = 'Y' AND I_PICK_TYPE = 'B'
       AND (I_TOTAL_WEIGHT is NULL OR I_TOTAL_WEIGHT = 0) THEN
           /*� GO_ITEM('WEIGHT.TOTAL_WEIGHT') �*/           
          I_GOTO_FIELD    := 'WEIGHT.TOTAL_WEIGHT';
      ELSE
         IF I_CALLING_BLOCK = 'BULK_PICK'  AND I_FIRST_WEIGHT_FLAG = 'Y' THEN 
     
          /*� User_Done_Proc �*/             
         USER_DONE_PROC (
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
              I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
              I_FROM_LOCATION_ID         =>  I_FROM_LOCATION_ID,
              I_GENERIC_TO_CONTAINER     =>  I_GENERIC_TO_CONTAINER,
              I_ITEM_ID                  =>  I_ITEM_ID,
              I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
              I_TO_LOCATION_ID           =>  I_TO_LOCATION_ID,
              I_FACILITY_ID              =>  I_FACILITY_ID,
              I_USER                     =>  I_USER,
              I_BULK_GROUP               =>  I_BULK_GROUP,
              I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
              I_CONTAINERS_PROCESSED     =>  I_CONTAINERS_PROCESSED,
              I_OPERATIONS_PERFORMED     =>  I_OPERATIONS_PERFORMED,
              I_UNITS_PROCESSED          =>  I_UNITS_PROCESSED,
              I_UNITS_PICKED             =>  I_UNITS_PICKED,
              I_CASEPACK                 =>  I_CASEPACK,
              I_DEST_ID                  =>  I_DEST_ID,
              I_DISTRO_NBR               =>  I_DISTRO_NBR,
              I_DISTRO_TS                =>  I_DISTRO_TS,
              I_EVENT_CODE               =>  I_EVENT_CODE,
              I_FIRST_SCAN_FLAG          =>  I_FIRST_SCAN_FLAG,
              I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
              I_LABELED_PICKING          =>  I_LABELED_PICKING,
              I_OLD_TO_CID               =>  I_OLD_TO_CID,
              I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
              I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID01   =>  I_PICK_TO_CONTAINER_ID,
              I_PICK_TYPE                =>  I_PICK_TYPE,
              I_SUGGESTED_LOC            =>  I_SUGGESTED_LOC,
              I_WAVE_NBR                 =>  I_WAVE_NBR,
              I_ZONE                     =>  I_ZONE,
              P_PM_ALLOW_INTRLVG         =>  I_PM_ALLOW_INTRLVG,
              P_VERSION                  =>  P_VERSION);               
            
            I_CALLING_BLOCK := 'TO_LOCATION';
            /*� GO_TO_LOCATION �*/
            Go_To_Location(
              V_RETURN                 => V_RETURN, 
              I_GOTO_FIELD             => I_GOTO_FIELD,
              I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID ,
              I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
              I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
              I_FACILITY_ID            => I_FACILITY_ID ,
              I_DEST_ID                => I_DEST_ID ,
              I_DISTRO_NBR             => I_DISTRO_NBR ,
              I_LABELED_PICKING        => I_LABELED_PICKING ,
              I_OLD_TO_CID             => I_OLD_TO_CID ,
              I_PICK_TYPE              => I_PICK_TYPE ,
              I_SUGGESTED_LOC          => I_SUGGESTED_LOC ,              
              I_WAVE_NBR               => I_WAVE_NBR );
             
         ELSIF calling_block_in = 'TO_LOCATION' THEN
            IF I_PICK_TYPE IN ('B','BR') THEN
            
              PKG_HOTEL_PICKING_ADF.MOVE_CONTAINER(V_RETURN, I_MSG_DISPLAY ,I_GENERIC_TO_CONTAINER ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_PICK_TO_CONTAINER_ID ,I_SUGGESTED_LOCATION ,I_TO_LOCATION_ID ,I_FACILITY_ID ,I_EVENT_CODE ,I_PICK_TYPE ,I_UNITS_PICKED ,I_WAVE_NBR ,I_USER ,I_DEST_ID);
              IF( V_RETURN IS NOT NULL) THEN
			    RETURN;
			  END IF;
              --ns IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
--           		EXCEPTION
--           	  	WHEN OTHERS THEN 
	           	  	 /*� Log_Error_Roll(SQLCODE, 'Move_Container', SQLERRM) �*/
                  
  	     --    	END;

            END IF;
            
               IF I_LABELED_PICKING = 'Y' THEN 
			       I_GOTO_FIELD := 'EXIT';
                   /*� Tr_Exit �*/
               ELSE
                   /*� GO_ITEM('Bulk_Pick.Generic_To_Container') �*/ 
                   I_GOTO_FIELD := 'BULK_PICK.GENERIC_TO_CONTAINER';
                   /*� CLEAR_BLOCK �*/
                   /*� Get_Generic_Pick_Directive �*/
               END IF;
    
         END IF;
      END IF;
      CLOSE check_item_cursor;
  
EXCEPTION
--   WHEN FRM_TRIGGER_FAILURE THEN
--      RAISE;      	
   WHEN OTHERS THEN
       PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Check_Catch_Weight',SQLERRM);
       PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
       /*� Log_Error_Roll(SQLCODE, 'Check_Catch_Weight ', SQLERRM) �*/
--      RAISE FRM_TRIGGER_FAILURE;
END;
--
  
--
PROCEDURE Validate_Qty(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER,
    I_UNITS_PROCESSED          IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER,
    I_OLD_TO_CID               IN OUT VARCHAR2,
    I_ITEM_ID                  IN OUT VARCHAR2,
    I_TO_LOCATION_ID           IN OUT VARCHAR2,
    I_TOTAL_WEIGHT             IN OUT NUMBER,
    I_UNITS_PICKED             IN OUT NUMBER,
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2,
    I_SUGGESTED_LOC            IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_CONTAINER_QTY01          IN OUT NUMBER ,
    I_MASTER_TO_CONTAINER      IN OUT VARCHAR2 ,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,
    I_GOTO_FIELD               IN OUT VARCHAR2,
    I_FIRST_WEIGHT_FLAG        IN OUT VARCHAR2 ,
    I_EVENT_CODE               IN OUT NUMBER ,
    P_VERSION                  IN NUMBER,
    I_BULK_GROUP               IN VARCHAR2,
    I_DISTRO_NBR               IN VARCHAR2,
    I_DISTRO_TS                IN DATE,
    I_ZONE                     IN VARCHAR2,
    I_PM_ALLOW_INTRLVG         IN VARCHAR2,
    I_USER                     IN VARCHAR2,
    I_DEST_ID                  IN NUMBER,
    I_WAVE_NBR                 IN NUMBER,
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY2           IN NUMBER ,
    I_EMPTY_PRESSED            IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG          IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_TYPE                IN VARCHAR2 ,    
    SYSTEM_CURSOR_BLOCK        IN VARCHAR2 )
IS
/*****************************************************************************************
* Author     	 Date         Defect#         Change Description
*--------------------------------------------------------------------------
*  L. Coronel  07/06/2007   CR35260         WMS Splitting: Accenture Change
*                                           R3.1 - Process full pallet CPs as BP. 
*  L. Coronel  08/29/2007   CR35261         WMS Splitting: Accenture
*                                           Change R3.2 Process Full pallet Cs as
*                                           BPs
*                                           1 Includes C picks along with
*                                           other picks.
*                                           2 When the associate received the pick 
*                                           quantity differs message, the associate 
*                                           will not be allowed to indicate N for C picks.
*****************************************************************************************/

------------------------------------------------------------------
dummy               VARCHAR2(1);
dummy2              VARCHAR2(1);
value_found         BOOLEAN;
t_child             NUMBER;
t_old_calling_block VARCHAR2(50);
------------------------------------------------------------------
CURSOR Check_For_Children_Cursor IS
   SELECT 'X'
     FROM Sys.Dual 
    WHERE EXISTS (SELECT Container_ID
                    FROM Container
                   WHERE Facility_Id         = I_FACILITY_ID
                     AND Master_Container_Id =
                         I_PICK_TO_CONTAINER_ID);
------------------------------------------------------------------
CURSOR count_sys_child IS
   SELECT COUNT(container_id)
     FROM Container
    WHERE facility_id         = I_FACILITY_ID
      AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
      AND container_status    = 'P';
------------------------------------------------------------------
BEGIN
   /*� ENTER �*/ 
	IF I_CONTAINER_QTY <= 0 THEN
   I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_QTY'); 
	     /*� CALL_MSG_WINDOW_P('INV_QTY') �*/
      RAISE FRM_TRIGGER_FAILURE;  
	ELSIF I_CONTAINER_QTY IS NULL THEN
     I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'PARTIAL_ENTRY'); 
       /*� Call_Msg_Window_P('PARTIAL_ENTRY') �*/
     RAISE FRM_TRIGGER_FAILURE;   
	END IF;	  
  -- Start CR35261 - Mshah
  -- Modified to only look for this condition when pick type is not C 
  IF (( NVL(I_CONTAINER_QTY, 0) = 0 )  OR ( I_CONTAINER_QTY > I_PICK_CONTAINER_QTY )  OR ( I_CONTAINER_QTY > I_ORIGINAL_PICK_QTY )) 
     AND I_PICK_TYPE <> 'C'   OR 
     (( NVL(I_CONTAINER_QTY, 0) = 0 OR NVL(I_CONTAINER_QTY2, 0) = 0 ) AND I_PICK_TYPE = 'C')
  THEN
     I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_QTY');
     /*� CALL_MSG_WINDOW_P('INV_QTY') �*/
     RAISE FRM_TRIGGER_FAILURE;
  END IF;
  -- End CR35261 - Mshah
  
  -- Start CR35261 - LCC
  /*IF ( I_CONTAINER_QTY < I_PICK_CONTAINER_QTY ) THEN
    IF CALL_MSG_WINDOW('DIFF_PICK') = 'N' THEN
    	RAISE FORM_TRIGGER_FAILURE;
    END IF;
  END IF;*/
  
  IF I_PICK_TYPE = 'BP' THEN 
  		I_EVENT_CODE := 26;
  ELSIF I_PICK_TYPE = 'CP' THEN
  		I_EVENT_CODE := 51;
  ELSIF I_PICK_TYPE = 'C' THEN
  		I_EVENT_CODE := 52;
  END IF;  		 
  IF ( I_CONTAINER_QTY < NVL(I_PICK_CONTAINER_QTY,0) ) AND I_PICK_TYPE <> 'C' THEN 
      I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'DIFF_PICK');
     IF  I_MSG_DISPLAY /*� CALL_MSG_WINDOW('DIFF_PICK') �*/ = 'N' THEN
        RAISE FRM_TRIGGER_FAILURE;
     END IF;
     IF I_PICK_TYPE = 'CP' THEN
     		I_EVENT_CODE := 24;
     END IF;  
     
  ELSIF (I_CONTAINER_QTY <> I_PICK_CONTAINER_QTY) AND I_PICK_TYPE = 'C' AND SYSTEM_CURSOR_BLOCK = 'LPN' THEN     
     I_EVENT_CODE := 27;
  ELSIF (I_CONTAINER_QTY <> I_PICK_CONTAINER_QTY) AND I_PICK_TYPE = 'C' AND SYSTEM_CURSOR_BLOCK <> 'LPN' THEN
     I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'DIFF_PICK'); 
      /*� CALL_MSG_WINDOW_P('DIFF_PICK') �*/
     I_EVENT_CODE := 27;
  END IF;
  -- End CR35261  
  -- Start CR35261 - LCC
    IF (I_CONTAINER_QTY < I_PICK_CONTAINER_QTY AND I_PICK_TYPE <> 'C')
    OR (I_CONTAINER_QTY <> I_PICK_CONTAINER_QTY AND I_PICK_TYPE = 'C') THEN
   -- End CR35261
     IF I_LABELED_RESERVE = 'Y' THEN
        IF I_EMPTY_PRESSED = 'Y' THEN
          Check_Empty(
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_FACILITY_ID              => I_FACILITY_ID ,
              From_Location_Id_in        => I_FROM_LOCATION_ID,
              From_Container_Id_in       => I_PICK_FROM_CONTAINER_ID,    
              I_BULK_GROUP               => I_BULK_GROUP ,
              I_DEST_ID                  => I_DEST_ID ,
              I_DISTRO_NBR               => I_DISTRO_NBR ,
              I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 ,
              I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID ,
              I_PICK_TYPE                => I_PICK_TYPE ,
              I_WAVE_NBR                 => I_WAVE_NBR ,
              Empty_Flag_in              => I_EMPTY_PRESSED );
    
     
        END IF;	
        --there are labeled children records for the master
        ----------------------------------------------------------------------
        -- For labeled_reserve = Y and short pick.. scan in children picked --
        ----------------------------------------------------------------------
         /*� Scan_All_Containers �
         -- and show  the child containers thought to be associated with the pick_from_container_id
         -- basically query the VO with */
      I_CALLING_BLOCK := 'CONTAINER'; 
      
     /*ELSIF I_LABELED_RESERVE = 'N' 
     /*AND I_PICK_TYPE IN ('BP', 'B', 'CP') THEN*/
     -- Start CR35261 - LCC
     ELSIF I_LABELED_RESERVE = 'N' 
     AND I_PICK_TYPE IN ('BP', 'B', 'CP', 'C') THEN
     -- End CR35261

        IF I_LABELED_PICKING = 'Y' THEN
        
           OPEN Check_For_Children_Cursor;
           FETCH Check_For_Children_Cursor INTO dummy2;
           CLOSE Check_For_Children_Cursor;
           
           IF dummy2 is NOT NULL THEN
              t_old_calling_block := I_CALLING_BLOCK;
              I_FIRST_WEIGHT_FLAG := 'Y';
              I_CALLING_BLOCK := 'BULK_PICK';
              --
              IF I_FIRST_SCAN_FLAG = 'Y' THEN
                 I_MASTER_TO_CONTAINER := I_PICK_TO_CONTAINER_ID;
                 I_CONTAINER_QTY01 := I_CONTAINER_QTY;
              ELSE
                 OPEN count_sys_child;
                 FETCH count_sys_child INTO t_child;
                 CLOSE count_sys_child;
                 I_CONTAINER_QTY01 := t_child;
              END IF;
              -- There are labeled children pick_to_cid records --
              -- Prompt user to scan in picked children         --
              I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD'); 
               /*� Call_Msg_Window_P ('SCAN_CHILD') �*/
              --
                I_GOTO_FIELD := 'LABEL_CHILD.PICK_TO_CHILD'; 
               /*� SET_ITEM_PROPERTY('Label_Child.Generic_to_Child', DISPLAYED, PROPERTY_FALSE) �*/
               /*� Go_ITEM('LABEL_CHILD.PICK_TO_CHILD') �*/
           ELSE
              IF I_EMPTY_PRESSED = 'Y' THEN
                  Check_Empty(
                    V_RETURN                   => V_RETURN,
                    I_MSG_DISPLAY              => I_MSG_DISPLAY,
                    I_FACILITY_ID              => I_FACILITY_ID ,
                    From_Location_Id_in        => I_FROM_LOCATION_ID,
                    From_Container_Id_in       => I_PICK_FROM_CONTAINER_ID,    
                    I_BULK_GROUP               => I_BULK_GROUP ,
                    I_DEST_ID                  => I_DEST_ID ,
                    I_DISTRO_NBR               => I_DISTRO_NBR ,
                    I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 ,
                    I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID ,
                    I_PICK_TYPE                => I_PICK_TYPE ,
                    I_WAVE_NBR                 => I_WAVE_NBR ,
                    Empty_Flag_in              => I_EMPTY_PRESSED );
              END IF;	
               
               /*� User_Done_Proc �*/
               USER_DONE_PROC (
                V_RETURN                   => V_RETURN,
                I_MSG_DISPLAY              => I_MSG_DISPLAY,
                I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
                I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
                I_FROM_LOCATION_ID         =>  I_FROM_LOCATION_ID,
                I_GENERIC_TO_CONTAINER     =>  I_GENERIC_TO_CONTAINER,
                I_ITEM_ID                  =>  I_ITEM_ID,
                I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
                I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
                I_TO_LOCATION_ID           =>  I_TO_LOCATION_ID,
                I_FACILITY_ID              =>  I_FACILITY_ID,
                I_USER                     =>  I_USER,
                I_BULK_GROUP              =>  I_BULK_GROUP,
                I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
                I_CONTAINERS_PROCESSED     =>  I_CONTAINERS_PROCESSED,
                I_OPERATIONS_PERFORMED     =>  I_OPERATIONS_PERFORMED,
                I_UNITS_PROCESSED          =>  I_UNITS_PROCESSED,
                I_UNITS_PICKED            =>   I_UNITS_PICKED,
                I_CASEPACK                 =>  I_CASEPACK,
                I_DEST_ID                  =>  I_DEST_ID,
                I_DISTRO_NBR               =>  I_DISTRO_NBR,
                I_DISTRO_TS                =>  I_DISTRO_TS,
                I_EVENT_CODE               =>  I_EVENT_CODE,
                I_FIRST_SCAN_FLAG          =>  I_FIRST_SCAN_FLAG,
                I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
                I_LABELED_PICKING          =>  I_LABELED_PICKING,
                I_OLD_TO_CID               =>  I_OLD_TO_CID,
                I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
                I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID,
                I_PICK_TO_CONTAINER_ID01   =>  I_PICK_TO_CONTAINER_ID,
                I_PICK_TYPE                =>  I_PICK_TYPE,
                I_SUGGESTED_LOC            =>  I_SUGGESTED_LOC,
                I_WAVE_NBR                 =>  I_WAVE_NBR,
                I_ZONE                     =>  I_ZONE,
                P_PM_ALLOW_INTRLVG         =>  I_PM_ALLOW_INTRLVG,
                P_VERSION                  =>  P_VERSION);  
              I_CALLING_BLOCK := 'TO_LOCATION';
               /*� GO_TO_LOCATION �*/
               Go_To_Location(
                V_RETURN                 => V_RETURN, 
                I_GOTO_FIELD             => I_GOTO_FIELD,
                I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID ,
                I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
                I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
                I_FACILITY_ID            => I_FACILITY_ID ,
                I_DEST_ID                => I_DEST_ID ,
                I_DISTRO_NBR             => I_DISTRO_NBR ,
                I_LABELED_PICKING        => I_LABELED_PICKING ,
                I_OLD_TO_CID             => I_OLD_TO_CID ,
                I_PICK_TYPE              => I_PICK_TYPE ,
                I_SUGGESTED_LOC          => I_SUGGESTED_LOC ,              
                I_WAVE_NBR               => I_WAVE_NBR );
           END IF;
        ELSE  -- labeled_picking = N --
         
         -- CR35260 - Also check for CP pick
           /*IF I_PICK_TYPE IN ('BP', 'CP') THEN*/
           -- Start CR35261 - LCC
           IF I_PICK_TYPE IN ('BP', 'CP', 'C') THEN 
           	-- End CR35261
           --
               /*� User_Done_Proc �*/
             USER_DONE_PROC (
                V_RETURN                   => V_RETURN,
               I_MSG_DISPLAY              => I_MSG_DISPLAY,
                I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
                I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
                I_FROM_LOCATION_ID         =>  I_FROM_LOCATION_ID,
                I_GENERIC_TO_CONTAINER     =>  I_GENERIC_TO_CONTAINER,
                I_ITEM_ID                  =>  I_ITEM_ID,
                I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
                I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
                I_TO_LOCATION_ID           =>  I_TO_LOCATION_ID,
                I_FACILITY_ID              =>  I_FACILITY_ID,
                I_USER                     =>  I_USER,
                I_BULK_GROUP              =>  I_BULK_GROUP,
                I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
                I_CONTAINERS_PROCESSED     =>  I_CONTAINERS_PROCESSED,
                I_OPERATIONS_PERFORMED     =>  I_OPERATIONS_PERFORMED,
                I_UNITS_PROCESSED          =>  I_UNITS_PROCESSED,
                I_UNITS_PICKED            =>   I_UNITS_PICKED,
                I_CASEPACK                 =>  I_CASEPACK,
                I_DEST_ID                  =>  I_DEST_ID,
                I_DISTRO_NBR               =>  I_DISTRO_NBR,
                I_DISTRO_TS                =>  I_DISTRO_TS,
                I_EVENT_CODE               =>  I_EVENT_CODE,
                I_FIRST_SCAN_FLAG          =>  I_FIRST_SCAN_FLAG,
                I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
                I_LABELED_PICKING          =>  I_LABELED_PICKING,
                I_OLD_TO_CID               =>  I_OLD_TO_CID,
                I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
                I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID,
                I_PICK_TO_CONTAINER_ID01   =>  I_PICK_TO_CONTAINER_ID,
                I_PICK_TYPE                =>  I_PICK_TYPE,
                I_SUGGESTED_LOC            =>  I_SUGGESTED_LOC,
                I_WAVE_NBR                 =>  I_WAVE_NBR,
                I_ZONE                     =>  I_ZONE,
                P_PM_ALLOW_INTRLVG         =>  I_PM_ALLOW_INTRLVG,
                P_VERSION                  =>  P_VERSION); 
              I_CALLING_BLOCK := 'TO_LOCATION';
               /*� GO_TO_LOCATION �*/
               Go_To_Location(
                  V_RETURN                 => V_RETURN, 
                  I_GOTO_FIELD             => I_GOTO_FIELD,
                  I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID ,
                  I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
                  I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
                  I_FACILITY_ID            => I_FACILITY_ID ,
                  I_DEST_ID                => I_DEST_ID ,
                  I_DISTRO_NBR             => I_DISTRO_NBR ,
                  I_LABELED_PICKING        => I_LABELED_PICKING ,
                  I_OLD_TO_CID             => I_OLD_TO_CID ,
                  I_PICK_TYPE              => I_PICK_TYPE ,
                  I_SUGGESTED_LOC          => I_SUGGESTED_LOC ,              
                  I_WAVE_NBR               => I_WAVE_NBR );
                   
           ELSIF I_PICK_TYPE = 'B' THEN
              IF I_EMPTY_PRESSED = 'Y' THEN
                   Check_Empty(
                      V_RETURN                   => V_RETURN,
                      I_MSG_DISPLAY              => I_MSG_DISPLAY,
                      I_FACILITY_ID              => I_FACILITY_ID ,
                      From_Location_Id_in        => I_FROM_LOCATION_ID,
                      From_Container_Id_in       => I_PICK_FROM_CONTAINER_ID,    
                      I_BULK_GROUP               => I_BULK_GROUP ,
                      I_DEST_ID                  => I_DEST_ID ,
                      I_DISTRO_NBR               => I_DISTRO_NBR ,
                      I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 ,
                      I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID ,
                      I_PICK_TYPE                => I_PICK_TYPE ,
                      I_WAVE_NBR                 => I_WAVE_NBR ,
                      Empty_Flag_in              => I_EMPTY_PRESSED );
              END IF;	
              t_old_calling_block := I_CALLING_BLOCK;
              I_FIRST_WEIGHT_FLAG := 'Y';
              I_CALLING_BLOCK := 'BULK_PICK';

              OPEN Check_For_Children_Cursor;
              FETCH Check_For_Children_Cursor INTO dummy;
              CLOSE Check_For_Children_Cursor;
              
              IF dummy is NOT NULL THEN
                 IF I_FIRST_SCAN_FLAG = 'Y' THEN
                    I_MASTER_TO_CONTAINER := I_GENERIC_TO_CONTAINER;
                    I_CONTAINER_QTY01 := I_CONTAINER_QTY;
                    ---------------------------------------------------------------------
                    -- new screen to scan in generic labels for children to containers --
                    -- if labeled_reserve = N and ticketing is required                --
                    ---------------------------------------------------------------------
                    
                    I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD'); 
                     /*� Call_Msg_Window_P ('SCAN_CHILD') �*/
                    
                    I_GOTO_FIELD := 'LABEL_CHILD.GENERIC_TO_CHILD'; 
                     /*� SET_ITEM_PROPERTY('Label_Child.Pick_to_Child', DISPLAYED, PROPERTY_FALSE) �*/
                     /*� GO_ITEM('LABEL_CHILD.GENERIC_TO_CHILD') �*/
                 ELSE -- not first scan --
                    OPEN count_sys_child;
                    FETCH count_sys_child INTO t_child;
                    CLOSE count_sys_child;
                    I_CONTAINER_QTY01 := t_child;
                    -- 
                    IF t_child > 0 THEN
                       ---------------------------------------------------------------------
                       -- new screen to scan in generic labels for children to containers --
                       -- if labeled_reserve = N and ticketing is required                --
                       ---------------------------------------------------------------------
                       I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD'); 
                        /*� Call_Msg_Window_P ('SCAN_CHILD') �*/
                        
                       I_GOTO_FIELD := 'LABEL_CHILD.GENERIC_TO_CHILD'; 
                        /*� SET_ITEM_PROPERTY('Label_Child.Pick_to_Child', DISPLAYED, PROPERTY_FALSE) �*/
                        /*� GO_ITEM('LABEL_CHILD.GENERIC_TO_CHILD') �*/
                    ELSE -- no children in to_cid --
                          CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
                        /*� Check_Catch_Weight (t_old_calling_block) �*/
                  
                    END IF;
                 END IF;
              ELSE -- no child found --
                    CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
                  /*� Check_Catch_Weight (t_old_calling_block) �*/
 
              END IF;
           END IF; -- end check pick_type of 'B' and 'BP'
        END IF; -- end check labeled_picking --
     ELSE  -- labeled_reserve = N with pick_type = 'BR'
      
         /*� User_Done_Proc �*/
        USER_DONE_PROC (
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
              I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
              I_FROM_LOCATION_ID         =>  I_FROM_LOCATION_ID,
              I_GENERIC_TO_CONTAINER     =>  I_GENERIC_TO_CONTAINER,
              I_ITEM_ID                  =>  I_ITEM_ID,
              I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
              I_TO_LOCATION_ID           =>  I_TO_LOCATION_ID,
              I_FACILITY_ID              =>  I_FACILITY_ID,
              I_USER                     =>  I_USER,
              I_BULK_GROUP               =>  I_BULK_GROUP,
              I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
              I_CONTAINERS_PROCESSED     =>  I_CONTAINERS_PROCESSED,
              I_OPERATIONS_PERFORMED     =>  I_OPERATIONS_PERFORMED,
              I_UNITS_PROCESSED          =>  I_UNITS_PROCESSED,
              I_UNITS_PICKED             =>  I_UNITS_PICKED,
              I_CASEPACK                 =>  I_CASEPACK,
              I_DEST_ID                  =>  I_DEST_ID,
              I_DISTRO_NBR               =>  I_DISTRO_NBR,
              I_DISTRO_TS                =>  I_DISTRO_TS,
              I_EVENT_CODE               =>  I_EVENT_CODE,
              I_FIRST_SCAN_FLAG          =>  I_FIRST_SCAN_FLAG,
              I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
              I_LABELED_PICKING          =>  I_LABELED_PICKING,
              I_OLD_TO_CID               =>  I_OLD_TO_CID,
              I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
              I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID01   =>  I_PICK_TO_CONTAINER_ID,
              I_PICK_TYPE                =>  I_PICK_TYPE,
              I_SUGGESTED_LOC            =>  I_SUGGESTED_LOC,
              I_WAVE_NBR                 =>  I_WAVE_NBR,
              I_ZONE                     =>  I_ZONE,
              P_PM_ALLOW_INTRLVG         =>  I_PM_ALLOW_INTRLVG,
              P_VERSION                  =>  P_VERSION); 
              
        I_CALLING_BLOCK := 'TO_LOCATION';
         /*� GO_TO_LOCATION �*/
         Go_To_Location(
              V_RETURN                 => V_RETURN, 
              I_GOTO_FIELD             => I_GOTO_FIELD,
              I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID ,
              I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
              I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
              I_FACILITY_ID            => I_FACILITY_ID ,
              I_DEST_ID                => I_DEST_ID ,
              I_DISTRO_NBR             => I_DISTRO_NBR ,
              I_LABELED_PICKING        => I_LABELED_PICKING ,
              I_OLD_TO_CID             => I_OLD_TO_CID ,
              I_PICK_TYPE              => I_PICK_TYPE ,
              I_SUGGESTED_LOC          => I_SUGGESTED_LOC ,              
              I_WAVE_NBR               => I_WAVE_NBR );
               
     END IF;
  ELSE -- the input qty = the system qty  ---- 
        Check_Empty(
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_FACILITY_ID              => I_FACILITY_ID ,
              From_Location_Id_in        => I_FROM_LOCATION_ID,
              From_Container_Id_in       => I_PICK_FROM_CONTAINER_ID,    
              I_BULK_GROUP               => I_BULK_GROUP ,
              I_DEST_ID                  => I_DEST_ID ,
              I_DISTRO_NBR               => I_DISTRO_NBR ,
              I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 ,
              I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID ,
              I_PICK_TYPE                => I_PICK_TYPE ,
              I_WAVE_NBR                 => I_WAVE_NBR ,
              Empty_Flag_in              => I_EMPTY_PRESSED );
  
    -- New Catch-weight functionality to check if item requires weight entered --
    t_old_calling_block := I_CALLING_BLOCK;
    I_FIRST_WEIGHT_FLAG := 'Y';
    I_CALLING_BLOCK := 'BULK_PICK';
    
    IF I_LABELED_RESERVE = 'N' AND I_LABELED_PICKING = 'N' THEN
    
       OPEN Check_For_Children_Cursor;
       FETCH Check_For_Children_Cursor INTO dummy;
       CLOSE Check_For_Children_Cursor;
       I_MSG_DISPLAY := I_MSG_DISPLAY||' 4 : '||dummy;
       IF dummy is NOT NULL THEN
       
          IF I_FIRST_SCAN_FLAG = 'Y' THEN
             I_MASTER_TO_CONTAINER := I_GENERIC_TO_CONTAINER;
             I_CONTAINER_QTY01 := I_CONTAINER_QTY;
             ---------------------------------------------------------------------
             -- new screen to scan in generic labels for children to containers --
             -- if labeled_reserve = N and ticketing is required                --
             ---------------------------------------------------------------------
             I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD'); 
              /*� Call_Msg_Window_P ('SCAN_CHILD') �*/ 
             I_GOTO_FIELD := 'LABEL_CHILD.GENERIC_TO_CHILD'; 
              /*� SET_ITEM_PROPERTY('Label_Child.Pick_to_Child', DISPLAYED, PROPERTY_FALSE) �*/
              /*� GO_ITEM('LABEL_CHILD.GENERIC_TO_CHILD') �*/
          ELSE -- not first scn --
          
             OPEN count_sys_child;
             FETCH count_sys_child INTO t_child;
             CLOSE count_sys_child;
             I_MSG_DISPLAY := I_MSG_DISPLAY||' 4-b ';
             I_CONTAINER_QTY01 := t_child;
             -- 
             IF t_child > 0 THEN
                ---------------------------------------------------------------------
                -- new screen to scan in generic labels for children to containers --
                -- if labeled_reserve = N and ticketing is required                --
                ---------------------------------------------------------------------
                I_MSG_DISPLAY :=   PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD'); 
                 /*� Call_Msg_Window_P ('SCAN_CHILD') �*/
                 
                I_GOTO_FIELD := 'LABEL_CHILD.GENERIC_TO_CHILD'; 
                 /*� SET_ITEM_PROPERTY('Label_Child.Pick_to_Child', DISPLAYED, PROPERTY_FALSE) �*/
                 /*� GO_ITEM('LABEL_CHILD.GENERIC_TO_CHILD') �*/
             ELSE
                 /*� Check_Catch_Weight (t_old_calling_block) �*/
                  CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
             END IF;
          END IF;
       ELSE
           /*� Check_Catch_Weight (t_old_calling_block) �*/
            CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
       END IF;
    ELSE 
        /*� Check_Catch_Weight (t_old_calling_block) �*/
        CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
    END IF;
    --
   END IF;
EXCEPTION
   WHEN FRM_TRIGGER_FAILURE THEN 
        PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
        
   WHEN OTHERS THEN
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Validate_Qty',SQLERRM);
     PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
       /*� Log_Error_Roll(SQLCODE, 'Validate_Qty', SQLERRM) �*/


END;
--
--

--
PROCEDURE Process_Remaining_C(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CONTAINER_QTY          IN OUT NUMBER ,
    I_CONTAINER_QTY01        IN OUT NUMBER ,
    I_MASTER_TO_CONTAINER    IN OUT VARCHAR2 ,
    I_CALLING_BLOCK          IN OUT VARCHAR2 ,    
    I_GOTO_FIELD             IN OUT VARCHAR2,
    I_FIRST_WEIGHT_FLAG      IN OUT VARCHAR2 ,
    I_NBR_CHILD              IN OUT NUMBER ,
    I_CONTAINERS_PROCESSED   IN OUT NUMBER,
    I_OPERATIONS_PERFORMED   IN OUT NUMBER,
    I_UNITS_PROCESSED        IN OUT NUMBER,
    I_CASEPACK               IN OUT NUMBER,
    I_OLD_TO_CID             IN OUT VARCHAR2,  
    I_TO_LOCATION_ID         IN OUT VARCHAR2,
    I_TOTAL_WEIGHT           IN OUT NUMBER,
    I_PICK_CONTAINER_QTY     IN OUT NUMBER ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2,
    I_SUGGESTED_LOC          IN OUT VARCHAR2,
    I_EVENT_CODE             IN OUT NUMBER ,
    I_UNITS_PICKED           IN OUT NUMBER,
    I_PICK_FROM_CONTAINER_ID IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID       IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 , 
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG        IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_BULK_GROUP             IN VARCHAR2, 
    I_DISTRO_TS              IN DATE,
    I_ZONE                   IN VARCHAR2,
    I_ORIGINAL_PICK_QTY      IN NUMBER,
    I_PM_ALLOW_INTRLVG       IN VARCHAR2, 
    I_DEST_ID                IN NUMBER,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,   
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    P_VERSION                IN NUMBER  )
IS 
 
/********************************************************************************************
*  Change History:                                                                
*    
*   Author           Date         Defect/CR#    Change Description      
*   ----------------------------------------------------------------------------------------
*   L.Coronel       09/27/07      CR35261       Change R3.2 Process Full pallet Cs
*                                               as BPs
*                                               1. New program unit that will handle processing
*                                               of shorted LPNs. This will create pick_directive
*                                               records for these shorted LPNs so that MAS and 
*                                               RMS will be notified during wave_close process.
*********************************************************************************************/
   rec_count             NUMBER(8);
   i                     NUMBER(8);
   l_cid                 Container.Container_ID%TYPE;
   l_unit_qty            Container_item.unit_qty%TYPE;
   l_item_id             Container_item.item_id%TYPE;
   dummy                 VARCHAR2(1);
   t_child               NUMBER;
   t_old_calling_block   VARCHAR2(50);
   l_count               number(8); 
   t_count               NUMBER(8) := 0;


   CURSOR Check_For_Children_Cursor IS
   SELECT 'X'
   FROM Sys.Dual
   WHERE EXISTS (SELECT Container_ID
                FROM Container
                WHERE Facility_Id         = I_FACILITY_ID
                  AND Master_Container_Id = I_PICK_TO_CONTAINER_ID);

   CURSOR count_sys_child IS
      SELECT COUNT(container_id)
      FROM Container
      WHERE facility_id         = I_FACILITY_ID
        AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
        AND container_status    = 'P';                                    

   CURSOR get_pd_record1(item_id_in IN pick_directive.item_id%TYPE, unit_qty_in IN pick_directive.case_pack_size%TYPE) IS
      SELECT *
        FROM pick_directive
       WHERE facility_id = I_FACILITY_ID
         AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
         AND item_id = item_id_in
         AND case_pack_size = unit_qty_in
         AND pick_type = 'C'
         AND rownum < 2
         FOR UPDATE OF pick_container_qty, unit_qty NOWAIT;
         
   CURSOR get_pd_record2(item_id_in IN pick_directive.item_id%TYPE, 
                        unit_qty_in IN pick_directive.case_pack_size%TYPE) IS
      SELECT *
        FROM pick_directive
       WHERE facility_id = I_FACILITY_ID
         AND wave_nbr = I_WAVE_NBR
         AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
         AND item_id = item_id_in
         AND case_pack_size = unit_qty_in
         AND pick_type = 'C'
         AND rownum < 2
       FOR UPDATE OF pick_container_qty, unit_qty NOWAIT;   

      
    
   get_pd_rec            get_pd_record1%ROWTYPE;
   
   CURSOR get_item_id IS
      SELECT item_id, unit_qty
        FROM container_item
       WHERE facility_id = I_FACILITY_ID
         AND container_id = l_cid;        

BEGIN

-- Start CR35261 - LCC: issue# 76 Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
  DELETE from ROSS_EVENTCODE_USERID_TMP; 
  COMMIT;

   --Inserting data into ROSS_EVENTCODE_USERID_TMP   
  IF I_PICK_TYPE = 'C' THEN
     INSERT INTO ROSS_EVENTCODE_USERID_TMP
     (
      EVENT_CD,
      USER_ID,
      LOCATION_ID
     )
     VALUES (27, I_USER, I_FROM_LOCATION_ID);
  END IF;	
-- End CR35261 - LCC: issue# 76
  
   --Find out if there are any containers left on the master that have not been scanned
--ns TODO: ADD Cursor to get the containers 
   rec_count :=  null/*� Get_Group_Row_Count('CONTAINER_DATA') �*/;

   IF rec_count > 0 THEN
      FOR i IN 1..rec_count LOOP
         l_cid :=  null/*� Get_Group_Char_Cell('CONTAINER_DATA.Container_ID',i) �*/;
         
         OPEN get_item_id;
         FETCH get_item_id INTO l_item_id, l_unit_qty;
         CLOSE get_item_id;
         
         IF P_VERSION = 1 THEN 
         		OPEN get_pd_record1(l_item_id, l_unit_qty);
         		FETCH get_pd_record1 INTO get_pd_rec;    
         ELSE
         		OPEN get_pd_record2(l_item_id, l_unit_qty);
         		FETCH get_pd_record2 INTO get_pd_rec;    
         END IF;	
                  
         INSERT INTO pick_directive VALUES (get_pd_rec.facility_id,
                                            get_pd_rec.wave_nbr,
                                            get_pd_rec.distro_nbr,
                                            l_cid,
                                            get_pd_rec.dest_id,
                                            get_pd_rec.pick_to_container_id,
                                            l_item_id,
                                            get_pd_rec.lot_nbr,
                                            get_pd_rec.case_pack_size,
                                            get_pd_rec.pick_type,
                                            get_pd_rec.zone,
                                            1,
                                            l_unit_qty,
                                            get_pd_rec.cube,
                                            get_pd_rec.labels_printed_flag,
                                            get_pd_rec.pick_order,
                                            get_pd_rec.break_by_distro,
                                            get_pd_rec.distro_ts,
                                            get_pd_rec.wip_code,
                                            get_pd_rec.user_id,
                                            'Y',
                                            get_pd_rec.process_date);

        
           IF get_pd_rec.pick_container_qty > 1 THEN
           	  
           	  IF P_VERSION = 1 THEN 	
           				UPDATE pick_directive
           		  		 SET pick_container_qty = pick_container_qty - 1,
           		      	 	unit_qty = GREATEST(unit_qty - l_unit_qty, 0)
           		 		WHERE CURRENT OF get_pd_record1;
           		ELSE 		
           		    UPDATE pick_directive
           		  		 SET pick_container_qty = pick_container_qty - 1,
           		      	 	unit_qty = GREATEST(unit_qty - l_unit_qty, 0)
           		 		WHERE CURRENT OF get_pd_record2;
           		END IF; 
           ELSE
           		
           		 IF P_VERSION = 1 THEN 	
           		 		DELETE pick_directive
           		 		WHERE CURRENT OF get_pd_record1;
           		 ELSE		
           		 		DELETE pick_directive
           		 		WHERE CURRENT OF get_pd_record2;
           		 END IF;
           END IF;
                      		 		
		      -- update the master container id of the LPN to NONE and the location to location of the MLP.
		      UPDATE Container
		         SET Location_ID = I_FROM_LOCATION_ID,
		             Master_Container_Id = 'NONE'
		       WHERE Facility_id = I_FACILITY_ID
		         AND Container_id = l_cid;
		         
		      UPDATE container c
		         SET pick_from_container_id = l_cid
		       WHERE facility_id = I_FACILITY_ID
		         AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
		         AND dest_id = get_pd_rec.dest_id
		         AND container_status = 'P'
		         AND ROWNUM < 2
		         AND EXISTS (SELECT 'X'
		                       FROM container_item
		                      WHERE facility_Id = I_FACILITY_ID
		                        AND container_id = c.container_id
		                        AND item_id = l_item_id
		                        AND unit_qty = l_unit_qty);   
      COMMIT;
      IF P_VERSION = 1 THEN 	
      		CLOSE get_pd_record1; 
      ELSE
      		CLOSE get_pd_record2; 
      END IF;	
           		
	    END LOOP;   
   END IF;
   --Update the inputted quantity with the number of containers scanned in
   I_CONTAINER_QTY := I_NBR_CHILD;
   I_NBR_CHILD := 0;

   t_old_calling_block := I_CALLING_BLOCK;
   I_FIRST_WEIGHT_FLAG := 'Y';
   I_CALLING_BLOCK := 'BULK_PICK';

   IF I_LABELED_RESERVE = 'N' AND I_LABELED_PICKING = 'N' THEN

      OPEN Check_For_Children_Cursor;
      FETCH Check_For_Children_Cursor INTO dummy;
      CLOSE Check_For_Children_Cursor;

      IF dummy is NOT NULL THEN
         IF I_FIRST_SCAN_FLAG = 'Y' THEN
            I_MASTER_TO_CONTAINER := I_GENERIC_TO_CONTAINER;
            I_CONTAINER_QTY01 := I_CONTAINER_QTY;
         ELSE
            OPEN count_sys_child;
            FETCH count_sys_child INTO t_child;
            CLOSE count_sys_child;
            I_CONTAINER_QTY01 := t_child;
         END IF;

          /*� GO_ITEM('LABEL_CHILD.GENERIC_TO_CHILD') �*/
          I_GOTO_FIELD := 'LABEL_CHILD.GENERIC_TO_CHILD';
          I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD');
          /*� Call_Msg_Window_P ('SCAN_CHILD') �*/
      ELSE
          /*� Check_Catch_Weight (t_old_calling_block) �*/
          CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              calling_block_in         =>  t_old_calling_block);
      END IF;

   ELSE

      /*� Check_Catch_Weight (t_old_calling_block) �*/
      CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
   END IF;

EXCEPTION
--   WHEN FRM_TRIGGER_FAILURE THEN
--      RAISE;
      
   WHEN OTHERS THEN
     PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_Remaining_C',SQLERRM  );
     PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
     
       /*� Log_Error(SQLCODE, 'Process_Remaining_C', SQLERRM) �*/
--       RAISE FRM_TRIGGER_FAILURE;

END Process_Remaining_C;
--
--
PROCEDURE Process_Remaining_CIDS(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CONTAINER_QTY          IN OUT NUMBER ,
    I_CONTAINER_QTY01        IN OUT NUMBER ,
    I_MASTER_TO_CONTAINER    IN OUT VARCHAR2 ,
    I_CALLING_BLOCK          IN OUT VARCHAR2 ,    
    I_GOTO_FIELD             IN OUT VARCHAR2,
    I_FIRST_WEIGHT_FLAG      IN OUT VARCHAR2 ,
    I_NBR_CHILD              IN OUT NUMBER ,
    I_CONTAINERS_PROCESSED   IN OUT NUMBER,
    I_OPERATIONS_PERFORMED   IN OUT NUMBER,
    I_UNITS_PROCESSED        IN OUT NUMBER,
    I_CASEPACK               IN OUT NUMBER,
    I_OLD_TO_CID             IN OUT VARCHAR2,  
    I_TO_LOCATION_ID         IN OUT VARCHAR2,
    I_TOTAL_WEIGHT           IN OUT NUMBER,
    I_PICK_CONTAINER_QTY     IN OUT NUMBER ,
    I_SUGGESTED_LOCATION     IN OUT VARCHAR2,
    I_SUGGESTED_LOC          IN OUT VARCHAR2,
    I_EVENT_CODE             IN OUT NUMBER ,
    I_UNITS_PICKED           IN OUT NUMBER,
    I_PICK_FROM_CONTAINER_ID IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_FROM_LOCATION_ID       IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_GENERIC_TO_CHILD       IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG        IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_DISTRO_NBR             IN VARCHAR2 ,
    I_BULK_GROUP             IN VARCHAR2, 
    I_DISTRO_TS              IN DATE,
    I_ZONE                   IN VARCHAR2,
    I_ORIGINAL_PICK_QTY      IN NUMBER,
    I_PM_ALLOW_INTRLVG       IN VARCHAR2, 
    I_DEST_ID                IN NUMBER,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER ,   
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    P_VERSION                IN NUMBER  )
IS
/*****************************************************************************************
* Author         Date         Defect#         Change Description
*--------------------------------------------------------------------------
*  L. Coronel  07/06/2007   CR35260         WMS Splitting: Accenture Change
*                                           R3.1 - Process full pallet CPs as BP.
*  M. Jose     02/06/2009   CO53221         WMS MCC: Accenture Change
*                                           The unscanned containers will be moved to 
*                                           WH specific UNLOCATEDLOC
*                                           Log the unscanned containers in ROSS_AUDIT_TRAIL
*****************************************************************************************/

   --This procedure checks to see what containers are left on the master.  Any containers are
   --removed from the master and moved to an unlocated location.

   rec_count             NUMBER(8);
   l_unloc               Location.Location_ID%TYPE;
   i                     NUMBER(8);
   l_cid                 Container.Container_ID%TYPE;
   l_status              VARCHAR2(200);
   l_unit_qty            NUMBER(8);
   l_item_id             item_master.item_id%TYPE;
   l_case_pck_qty        container_item.unit_qty%TYPE;
   dummy                 VARCHAR2(1);
   dummy2                VARCHAR2(1);
   t_child               NUMBER;
   t_old_calling_block   VARCHAR2(50);
   t_gen_child           Container.Container_ID%TYPE;
   l_row_id              VARCHAR2(50);
   l_pick_qty            pick_directive.pick_container_qty%TYPE;
   /*START MJose <CO53221> - 1*/
   L_wh_id               wh.wh_id%TYPE;
   L_create_divert_flag  wh.create_divert_flag%TYPE;
   L_found               VARCHAR2(1);
   L_Code                ERROR_LOG.Error_Code%TYPE;
   L_Message             ERROR_LOG.Error_Msg%TYPE;
   /*END MJose <CO53221> - 1*/

   CURSOR Check_For_Children_Cursor IS
      SELECT 'X'
      FROM Sys.Dual
      WHERE EXISTS (SELECT Container_ID
                    FROM Container
                    WHERE Facility_Id         = I_FACILITY_ID
                      AND Master_Container_Id = I_PICK_TO_CONTAINER_ID);

   CURSOR count_sys_child IS
      SELECT COUNT(container_id)
      FROM Container
      WHERE facility_id         = I_FACILITY_ID
        AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
        AND container_status    = 'P';

   CURSOR shorted_unit_qty_cursor IS
      SELECT unit_qty
      FROM Container_Item
      WHERE Facility_Id  = I_FACILITY_ID
        AND Container_Id = l_cid
        AND item_id      = I_ITEM_ID;

   CURSOR get_last_child_cursor IS
      SELECT container_id
        FROM Container
       WHERE facility_id         = I_FACILITY_ID
         AND Master_Container_id = I_PICK_TO_CONTAINER_ID
         AND container_status    = 'P'
         AND sort_order          = GREATEST(sort_order);

   -- CR35260 - New cursor to fetch unit qty of the shorted LPN
   CURSOR cont_item_cursor IS
      SELECT item_id, unit_qty
        FROM container_item
       WHERE facility_id = I_FACILITY_ID
         AND container_id = l_cid;
   --

   -- CR35260 - New cursor to fetch pd record for the pick being shorted.
   CURSOR get_pd_cursor1 (item_id_in IN pick_directive.item_id%TYPE, unit_qty_in IN pick_directive.case_pack_size%TYPE) IS
      SELECT rowid, pick_container_qty
        FROM pick_directive
       WHERE facility_id = I_FACILITY_ID
         AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
         AND item_id = item_id_in
         AND case_pack_size = unit_qty_in
         AND pick_type <> 'U';
         
   -- CR35260 - New cursor to fetch rowid pick container qty of the pick being shorted.
   CURSOR get_pd_cursor2 (item_id_in IN pick_directive.item_id%TYPE, unit_qty_in IN pick_directive.case_pack_size%TYPE) IS
      SELECT rowid, pick_container_qty
        FROM pick_directive
       WHERE facility_id = I_FACILITY_ID
         AND wave_nbr = I_WAVE_NBR
         AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
         AND item_id = item_id_in
         AND case_pack_size = unit_qty_in
         AND pick_type <> 'U';
    --
      
   --

BEGIN

   --Find out if there are any containers left on the master that have not been scanned

   rec_count :=  null/*� Get_Group_Row_Count('CONTAINER_DATA') �*/;

   IF rec_count > 0 THEN

      --Get the unlocated location
      /*START MJose <CO53221> */
      --l_unloc := g_scp(I_FACILITY_ID, 'unlocated_location');
      Get_Wh_Id (I_FACILITY_ID,
                 I_FROM_LOCATION_ID,
                 L_Wh_Id,
                 L_create_divert_flag,
                 L_found);
      IF L_found = 'N' THEN
         l_message := 'Location does not have a warehouse id.'||
                      ' Location :'||I_FROM_LOCATION_ID||
                      ' Container :'||I_PICK_FROM_CONTAINER_ID;
         l_code    := SQLCODE;
         INSERT INTO Error_Log (facility_id,
                                error_code,
                                user_id,
                                error_ts,
                                error_source,
                                error_loc,
                                error_msg)
                        VALUES (I_FACILITY_ID,
                                l_code,
                                I_USER,
                                SYSDATE,
                               'HH_BULK_PK_ACROSS_WV_S',
                               'Process_Remaining_Cids',
                                l_message);
      END IF;

      l_unloc := g_scp_wh(I_FACILITY_ID,
                          L_wh_id,
                         'unlocated_loc');
      IF l_unloc = 'ERROR' THEN
         l_unloc := g_scp(I_FACILITY_ID, 'unlocated_location');
      END IF;
      /*END MJose <CO53221> */

      FOR i IN 1..rec_count LOOP

         l_cid :=  null/*� Get_Group_Char_Cell('CONTAINER_DATA.Container_ID',i) �*/;

         -- CR35260 - Subtracting the unit quantities of the pick for accurate history logging
         IF I_PICK_TYPE = 'CP' THEN
                 OPEN cont_item_cursor;
                 FETCH cont_item_cursor INTO l_item_id, l_case_pck_qty;
                 CLOSE cont_item_cursor;
                 
                 IF P_VERSION = 1 THEN 
                 		OPEN get_pd_cursor1(l_item_id, l_case_pck_qty);
                 		FETCH get_pd_cursor1 INTO l_row_id, l_pick_qty;
                 		CLOSE get_pd_cursor1;
                 ELSE
                 		OPEN get_pd_cursor2(l_item_id, l_case_pck_qty);
                 		FETCH get_pd_cursor2 INTO l_row_id, l_pick_qty;
                 		CLOSE get_pd_cursor2;
                 END IF;	

                 IF l_pick_qty > 1 THEN
                         UPDATE pick_directive
                            SET pick_container_qty = pick_container_qty - 1,
                                unit_qty = GREATEST((unit_qty - l_case_pck_qty), 0)
                          WHERE rowid = l_row_id;
                 ELSE
                       DELETE pick_directive
                        WHERE rowid = l_row_id;
                 END IF;

                 -- Deleting system generated container for the shorted LPN
                 DELETE Container c
                  WHERE facility_id = I_FACILITY_ID
                    AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
                    AND master_container_id = I_PICK_TO_CONTAINER_ID
                    AND ROWNUM < 2
                    AND EXISTS (SELECT 'X'
                                  FROM container_item
                                 WHERE facility_id = I_FACILITY_ID
                                   AND container_id = c.container_id
                                   AND item_id = l_item_id
                                   AND unit_qty = l_case_pck_qty);
         END IF;
         --

         UPDATE Container
         SET Location_ID = l_unloc,
             Master_Container_Id = 'NONE'
         WHERE Facility_ID = I_FACILITY_ID
           AND Container_ID = l_cid;

         /*START MJose <CO53221> */
         OPEN shorted_unit_qty_cursor;
         FETCH shorted_unit_qty_cursor INTO l_unit_qty;
         CLOSE shorted_unit_qty_cursor;
         log_audit_trail (I_FACILITY_ID,
                          'HH_BULK_PK_ACROSS_WV_S',
                          I_USER,
                           l_unloc,
                           l_cid,--LPN moved to wh specific unlocatedloc
                          'UNLC',
                          'PICK',
                          'MASTER_CONTAINER_ID',
                           'NONE', 
                          'TOTAL_UNITS',
                          l_unit_qty);
         /*END MJose <CO53221> */

         IF I_PICK_TYPE = 'B' THEN
            OPEN shorted_unit_qty_cursor;
            FETCH shorted_unit_qty_cursor INTO l_unit_qty;
            CLOSE shorted_unit_qty_cursor;
            --Adjustments will be made to the stock_allocation records
            PKG_HOTEL_PICKING_ADF.ADJUST_DISTRIBUTION(V_RETURN, I_MSG_DISPLAY, I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_FACILITY_ID ,I_PICK_TYPE ,I_WAVE_NBR  ,l_unit_qty);
			IF (V_RETURN IS NOT NULL) THEN
			   RETURN;
			END IF;
            -- IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
         END IF;

         --- delete unpicked system generated pick to containers
        IF I_PICK_TYPE in ('B','BP') THEN
            IF I_LABELED_PICKING = 'Y' THEN
               OPEN Check_For_Children_Cursor;
               FETCH Check_For_Children_Cursor INTO dummy2;
               CLOSE Check_For_Children_Cursor;
               -- check for system generated children --
               IF dummy2 is NOT NULL THEN
                  -- since child exist, find the 'last' system generated child label --
                  OPEN get_last_child_cursor;
                  FETCH get_last_child_cursor INTO t_gen_child;
                  CLOSE get_last_child_cursor;

                  -- Find the 'last' system generated child label to delete --
                  IF t_gen_child is NOT NULL THEN
                    DELETE Container
                     WHERE facility_id = I_FACILITY_ID
                       AND container_id = t_gen_child;
                  END IF;
               END IF;
            END IF;
         END IF;

      END LOOP;

   END IF;

   --Update the inputted quantity with the number of containers scanned in
   I_CONTAINER_QTY := I_NBR_CHILD;
   I_NBR_CHILD := 0;

   COMMIT;

   t_old_calling_block := I_CALLING_BLOCK;
   I_FIRST_WEIGHT_FLAG := 'Y';
   I_CALLING_BLOCK := 'BULK_PICK';

   IF I_LABELED_RESERVE = 'N' AND I_LABELED_PICKING = 'N' THEN

      OPEN Check_For_Children_Cursor;
      FETCH Check_For_Children_Cursor INTO dummy;
      CLOSE Check_For_Children_Cursor;

      IF dummy is NOT NULL THEN
         IF I_FIRST_SCAN_FLAG = 'Y' THEN
            I_MASTER_TO_CONTAINER := I_GENERIC_TO_CONTAINER;
            I_CONTAINER_QTY01 := I_CONTAINER_QTY;
         ELSE
            OPEN count_sys_child;
            FETCH count_sys_child INTO t_child;
            CLOSE count_sys_child;
            I_CONTAINER_QTY01 := t_child;
         END IF;
         
          /*� GO_ITEM('LABEL_CHILD.GENERIC_TO_CHILD') �*/
          I_GOTO_FIELD := 'LABEL_CHILD.GENERIC_TO_CHILD';
           
          /*� Call_Msg_Window_P ('SCAN_CHILD') �*/
          I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SCAN_CHILD');
      ELSE
          /*� Check_Catch_Weight (t_old_calling_block) �*/
          CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
      END IF;

   ELSE
        /*� Check_Catch_Weight (t_old_calling_block) �*/ 
          CHECK_CATCH_WEIGHT(
                              V_RETURN                 =>  V_RETURN,
                              I_MSG_DISPLAY            => I_MSG_DISPLAY,
                              I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                              P_VERSION                => P_VERSION,
                              I_CONTAINER_QTY          => I_CONTAINER_QTY,
                              I_BULK_GROUP             => I_BULK_GROUP ,
                              I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                              I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                              I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                              I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                              I_CASEPACK               => I_CASEPACK,
                              I_DISTRO_NBR             => I_DISTRO_NBR,
                              I_DISTRO_TS              => I_DISTRO_TS,
                              I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                              I_OLD_TO_CID             => I_OLD_TO_CID,
                              I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                              I_ZONE                   => I_ZONE, 
                              I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                              I_GENERIC_TO_CONTAINER   =>  I_GENERIC_TO_CONTAINER ,
                              I_ITEM_ID                =>  I_ITEM_ID,
                              I_PICK_FROM_CONTAINER_ID =>  I_PICK_FROM_CONTAINER_ID,
                              I_PICK_TO_CONTAINER_ID   =>  I_PICK_TO_CONTAINER_ID,
                              I_SUGGESTED_LOCATION     =>  I_SUGGESTED_LOCATION,
                              I_SUGGESTED_LOC          =>  I_SUGGESTED_LOC ,
                              I_TO_LOCATION_ID         =>  I_TO_LOCATION_ID,
                              I_TOTAL_WEIGHT           =>  I_TOTAL_WEIGHT,
                              I_CALLING_BLOCK          =>  I_CALLING_BLOCK,
                              I_FACILITY_ID            =>  I_FACILITY_ID,
                              I_USER                   =>  I_USER,
                              I_DEST_ID                =>  I_DEST_ID,
                              I_EVENT_CODE             =>  I_EVENT_CODE,
                              I_FIRST_WEIGHT_FLAG      =>  I_FIRST_WEIGHT_FLAG,
                              I_LABELED_RESERVE        =>  I_LABELED_RESERVE,
                              I_LABELED_PICKING        =>  I_LABELED_PICKING,
                              I_PICK_TYPE              =>  I_PICK_TYPE,
                              I_UNITS_PICKED           =>  I_UNITS_PICKED,
                              I_WAVE_NBR               =>  I_WAVE_NBR,
                              I_GOTO_FIELD             => I_GOTO_FIELD,
                              calling_block_in         =>  t_old_calling_block);
   END IF;

EXCEPTION
--   WHEN FRM_TRIGGER_FAILURE THEN
--      RAISE;
   WHEN OTHERS THEN
       /*� Log_Error(SQLCODE, 'Process_Remaining_CIDS', SQLERRM) �*/
      PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_Remaining_CIDS',SQLERRM);
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);   
--      RAISE FRM_TRIGGER_FAILURE;

END Process_Remaining_CIDS;
--
--
PROCEDURE bypass_pick(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2, 
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_CODE                     IN OUT VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_AUTH_USER_ID             IN VARCHAR2 ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER ,   
    I_GOTO_FIELD               IN OUT VARCHAR2,
    P_VERSION                  IN NUMBER )
IS
/**********************************************************************************
*  Change History:
*
* Author        Date          Defect/CR#   Change Description
*---------------------------------------------------------------------------------
* R.Macero   04/16/2008        CO43433    Initial Version.
***********************************************************************************/

   CURSOR get_pd_recs1 IS
   SELECT *
     FROM pick_directive
    WHERE facility_id = I_FACILITY_ID 
      AND pick_from_container_id =
          I_PICK_FROM_CONTAINER_ID
      AND pick_type = I_PICK_TYPE;
      
   
   CURSOR get_pd_recs2 IS
   SELECT *
     FROM pick_directive
    WHERE facility_id = I_FACILITY_ID 
      AND pick_from_container_id =
          I_PICK_FROM_CONTAINER_ID
      AND wave_nbr = I_WAVE_NBR
      AND pick_type = I_PICK_TYPE;
   
   
   L_unit_qty   PICK_DIRECTIVE.UNIT_QTY%TYPE;
   L_dest_id    PICK_DIRECTIVE.DEST_ID%TYPE;
   L_zone       PICK_DIRECTIVE.ZONE%TYPE;
   l_form_name  varchar2(50);
BEGIN
   IF (P_VERSION = 1) THEN
    l_form_name  := 'HH_BULK_PK_ACROSS_WV_S';
   ELSE
    l_form_name  := 'HH_BULK_PICK_S';
   END IF;

   -- Write message to error_log if needed for WH.
   IF loc_scp_by_wh (I_FACILITY_ID,
                     I_FROM_LOCATION_ID,
                     'write_byp_in_err_log') = 'Y' THEN
           PKG_HOTEL_PICKING_ADF.P_WRITE_MESSAGE1(
              V_RETURN                 => V_RETURN,
              I_MSG_DISPLAY            => I_MSG_DISPLAY,
              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID ,
              I_ITEM_ID                => I_ITEM_ID ,
              I_FACILITY_ID            => I_FACILITY_ID ,
              I_AUTH_USER_ID           => I_AUTH_USER_ID ,
              I_DISTRO_NBR             => I_DISTRO_NBR ,
              I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID01 ,
              I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
              I_PICK_TYPE              => I_PICK_TYPE ,
              I_WAVE_NBR               => I_WAVE_NBR ,
              I_USER                   => I_USER ,
              i_msg_type               => 'B',
	            I_FORM_NAME  	            => l_form_name);
        IF (V_RETURN IS NOT NULL) THEN
		  RETURN;
		END IF;
--      IF P_DISPLAY.HANDLE_MESS THEN 
--      	   null/*� Log_Error_Roll  (SQLCODE, 'P_write_message - Others', SQLERRM) �*/;
--       		RAISE FORM_TRIGGER_FAILURE; 
--      END IF;
   END IF; 

   -- Mark location for cycle count
   IF Mark_Location(I_FROM_LOCATION_ID,  I_FACILITY_ID) = 'SUCCESS' THEN
      Commit;
   END IF;

   -- Log into Audit Trail.
   IF P_VERSION = 1 THEN 
   		FOR c_get_rec IN get_pd_recs1 LOOP
      		Log_Audit_Trail(I_FACILITY_ID,
          		            'HH_BULK_PK_ACROSS_WV_S',
                      		I_USER,
                      		I_FROM_LOCATION_ID,
                      		I_PICK_FROM_CONTAINER_ID,
                      		'BYPX',
                      		I_CODE,
                      		'DISTRO_NBR',
                      		c_get_rec.DISTRO_NBR,
                      		'ITEM_ID',
                      		c_get_rec.ITEM_ID,
                      		'DEST_ID',
                      		c_get_rec.dest_id,
                      		'PICK_TYPE',
                      		I_PICK_TYPE,
                      		'UNIT_QTY',
                      		c_get_rec.unit_qty,
                      		'PICK_TO_CONTAINER_ID',
                      		c_get_rec.PICK_TO_CONTAINER_ID,
                      		'ZONE',
                      		c_get_rec.zone,
                      		'CONTAINER_QTY',
                      		c_get_rec.PICK_CONTAINER_QTY,
                      		'WAVE_NBR',
                      		I_WAVE_NBR);
   		END LOOP;
   ELSE
   		FOR c_get_rec IN get_pd_recs2 LOOP
      		Log_Audit_Trail(I_FACILITY_ID,
          		            'HH_BULK_PICK_S',
                      		I_USER,
                      		I_FROM_LOCATION_ID,
                      		I_PICK_FROM_CONTAINER_ID,
                      		'BYPX',
                      		I_CODE,
                      		'DISTRO_NBR',
                      		c_get_rec.DISTRO_NBR,
                      		'ITEM_ID',
                      		c_get_rec.ITEM_ID,
                      		'DEST_ID',
                      		c_get_rec.dest_id,
                      		'PICK_TYPE',
                      		I_PICK_TYPE,
                      		'UNIT_QTY',
                      		c_get_rec.unit_qty,
                      		'PICK_TO_CONTAINER_ID',
                      		c_get_rec.PICK_TO_CONTAINER_ID,
                      		'ZONE',
                      		c_get_rec.zone,
                      		'CONTAINER_QTY',
                      		c_get_rec.PICK_CONTAINER_QTY,
                      		'WAVE_NBR',
                      		I_WAVE_NBR);
   		END LOOP;   	
   END IF;		
   --CLEAR_BLOCK(NO_VALIDATE);
   I_CODE := NULL;

   IF I_LABELED_PICKING = 'Y' THEN
     I_GOTO_FIELD := 'CLEAR_BLOCK';
     /*� CLEAR_BLOCK(NO_VALIDATE) �*/
   ELSE
       /*� GO_ITEM('Bulk_Pick.Generic_To_Container') �*/
       I_GOTO_FIELD := 'BULK_PICK.GENERIC_TO_CONTAINER';
      --CLEAR_BLOCK;
       /*� Get_Generic_Pick_Directive �*/
   END IF;

END bypass_pick;
--
--
PROCEDURE BYPASS_KEY_PRESS(
    V_RETURN                   IN OUT VARCHAR2,
    V_CLEAR_BLOCK              IN OUT VARCHAR2 ,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_LOG_BYPASS               IN OUT VARCHAR2,
    I_REASON_CODE              IN OUT VARCHAR2,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,
    I_GOTO_FIELD               IN OUT VARCHAR2, 
    I_FACILITY_ID              IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_AUTH_USER_ID             IN VARCHAR2 ,
    I_DISTRO_NBR               IN VARCHAR2,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER,
    I_USER                     IN VARCHAR2,
    P_VERSION                  IN NUMBER)
IS
L_display             VARCHAR2(1);
L_code_value          CODE_DETAIL.CODE%TYPE;
l_form_name  varchar2(50);
BEGIN
  IF I_LOG_BYPASS = 'N' THEN
    IF Mark_Location(I_FROM_LOCATION_ID, I_FACILITY_ID) = 'SUCCESS' THEN
      COMMIT;
    END IF;
    
    IF I_LABELED_PICKING = 'Y' THEN
      V_CLEAR_BLOCK := 'T';
        /*� CLEAR_BLOCK(NO_VALIDATE);�*/
    ELSE
     I_GOTO_FIELD  := 'BULK_PICK.GENERIC_TO_CONTAINER';
     /*� GO_ITEM('Bulk_Pick.Generic_To_Container');
       CLEAR_BLOCK;
      Get_Generic_Pick_Directive;�*/
    END IF;
  ELSE
    IF g_wave_type(I_FACILITY_ID, I_WAVE_NBR) = 'PO' THEN -- Flow
	   IF (P_VERSION = 1) THEN
		l_form_name  := 'HH_BULK_PK_ACROSS_WV_S';
	   ELSE
		l_form_name  := 'HH_BULK_PICK_S';
	   END IF;
      -- Write message to error_log
      PKG_HOTEL_PICKING_ADF.P_WRITE_MESSAGE1(
              V_RETURN                 => V_RETURN,
              I_MSG_DISPLAY            => I_MSG_DISPLAY,
              I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID ,
              I_ITEM_ID                => I_ITEM_ID ,
              I_FACILITY_ID            => I_FACILITY_ID ,
              I_AUTH_USER_ID           => I_AUTH_USER_ID ,
              I_DISTRO_NBR             => I_DISTRO_NBR ,
              I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID ,
              I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
              I_PICK_TYPE              => I_PICK_TYPE ,
              I_WAVE_NBR               => I_WAVE_NBR ,
              I_USER                   => I_USER ,
              i_msg_type               => 'B',
	            I_FORM_NAME  	            => l_form_name);
      -- mark location for cycle count
      IF Mark_Location(I_FROM_LOCATION_ID, I_FACILITY_ID) = 'SUCCESS' THEN
        COMMIT;
      END IF;
      IF I_LABELED_PICKING = 'Y' THEN
        V_CLEAR_BLOCK := 'T';
        /*� CLEAR_BLOCK(NO_VALIDATE); �*/
      ELSE
        I_GOTO_FIELD  := 'BULK_PICK.GENERIC_TO_CONTAINER'; 
        /*� GO_ITEM('Bulk_Pick.Generic_To_Container'); �*/
        --CLEAR_BLOCK;
        /*� Get_Generic_Pick_Directive; �*/
      END IF;
    ELSE -- Hotel
      /* Start CO43433 LCoronel - 5 */
      -- Check if there is a need to enter a reason code.
      CK_RSN_CD_REQ(I_FACILITY_ID,'BYPX',L_display,L_code_value);
      IF L_display = 'Y' THEN
        /*� :work_local.previous_item := :system.cursor_item; 
        GO_BLOCK('REASON'); -- Display screen to enter reason code.�*/
        I_GOTO_FIELD := 'REASON';
      ELSE
        IF l_code_value = 'NONE' THEN
          /*� Call_Msg_Window_P('NO_CODE_FOUND', 'E');�*/
          I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_CODE_FOUND','E');
        END IF;
        I_REASON_CODE := L_code_value;
        BYPASS_PICK(
            V_RETURN                   => V_RETURN,
            I_MSG_DISPLAY              => I_MSG_DISPLAY, 
            I_FROM_LOCATION_ID         => I_FROM_LOCATION_ID,
            I_GENERIC_TO_CONTAINER     => I_GENERIC_TO_CONTAINER,
            I_ITEM_ID                  => I_ITEM_ID,
            I_PICK_FROM_CONTAINER_ID   => I_PICK_FROM_CONTAINER_ID,
            I_CODE                     => I_REASON_CODE,
            I_FACILITY_ID              => I_FACILITY_ID,
            I_USER                     => I_USER,
            I_AUTH_USER_ID             => I_AUTH_USER_ID,
            I_DISTRO_NBR               => I_DISTRO_NBR,
            I_LABELED_PICKING          => I_LABELED_PICKING,
            I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01,
            I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID,
            I_PICK_TYPE                => I_PICK_TYPE,
            I_WAVE_NBR                 => I_WAVE_NBR,   
            I_GOTO_FIELD               => I_GOTO_FIELD,
            P_VERSION                  => P_VERSION);
      END IF;
      /* End CO43433 LCoronel - 5 */
    END IF;
    /* End CO43433 LCoronel - 4 */
    I_LOG_BYPASS := 'Y';
  END IF;
END BYPASS_KEY_PRESS;
--
--


PROCEDURE PROCESS_WEIGHT(
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2 ,
    I_TOTAL_WEIGHT             IN OUT NUMBER ,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,
    I_GOTO_FIELD               IN OUT VARCHAR2,
    I_UNITS_PICKED             IN OUT NUMBER,
    I_UNITS_PROCESSED          IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER,
    I_OLD_TO_CID               IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 , 
    I_MASTER_TO_CONTAINER      IN OUT VARCHAR2 ,
    I_FIRST_WEIGHT_FLAG        IN OUT VARCHAR2 ,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER,
    I_TO_LOCATION_ID           IN VARCHAR2 ,
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_DEST_ID                  IN NUMBER ,
    I_EVENT_CODE               IN NUMBER ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER,
    P_VERSION                  IN NUMBER,
    I_BULK_GROUP               IN VARCHAR2,
    I_DISTRO_NBR               IN VARCHAR2,
    I_DISTRO_TS                IN DATE,
    I_ZONE                     IN VARCHAR2,
    I_PM_ALLOW_INTRLVG         IN VARCHAR2,
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY2           IN NUMBER ,
    I_EMPTY_PRESSED            IN VARCHAR2 ,
    I_FIRST_SCAN_FLAG          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER ,
    I_PICK_CONTAINER_QTY       IN NUMBER)
IS

   L_child_weight   container.container_weight%TYPE;
   L_child_nbr      NUMBER(10);
   req_weight       EXCEPTION;

   ----------------------------------------------------------------
   -- Cursor to Check if Pick To Master CID has labeled children --
   ----------------------------------------------------------------
   CURSOR check_children_cursor IS
      SELECT count(container_id)
        FROM container
       WHERE facility_id = I_FACILITY_ID
         AND master_container_id = I_PICK_TO_CONTAINER_ID;

BEGIN

   IF I_TOTAL_WEIGHT > 0 
   AND I_CALLING_BLOCK = 'TO_LOCATION' THEN  -- weight has been entered --
      ------------------------------------------------------
      -- Check if Pick To Master CID has labeled children --
      ------------------------------------------------------
         OPEN check_children_cursor;
         FETCH check_children_cursor INTO L_child_nbr;
         CLOSE check_children_cursor;
      --
      IF L_child_nbr > 0 THEN  -- labeled children exists --
         L_child_weight := I_TOTAL_WEIGHT/L_child_nbr;
            UPDATE container
               SET container_weight    = L_child_weight
             WHERE facility_id         = I_FACILITY_ID
               AND master_container_id = I_PICK_TO_CONTAINER_ID;
      ELSE -- master with unlabeled children --
            UPDATE container
               SET container_weight    = I_TOTAL_WEIGHT
             WHERE facility_id         = I_FACILITY_ID
               AND container_id        = I_PICK_TO_CONTAINER_ID;
      END IF; 
      --
      I_TOTAL_WEIGHT := '';
   ELSIF I_CALLING_BLOCK = 'TO_LOCATION'
    AND (I_TOTAL_WEIGHT = 0 or I_TOTAL_WEIGHT is NULL) THEN
      raise req_weight;
   END IF;
   --
   IF I_CALLING_BLOCK = 'BULK_PICK' THEN
       /*� User_Done_Proc �*/
        USER_DONE_PROC (
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
              I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
              I_FROM_LOCATION_ID         =>  I_FROM_LOCATION_ID,
              I_GENERIC_TO_CONTAINER     =>  I_GENERIC_TO_CONTAINER,
              I_ITEM_ID                  =>  I_ITEM_ID,
              I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
              I_TO_LOCATION_ID           =>  I_TO_LOCATION_ID,
              I_FACILITY_ID              =>  I_FACILITY_ID,
              I_USER                     =>  I_USER,
              I_BULK_GROUP               =>  I_BULK_GROUP,
              I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
              I_CONTAINERS_PROCESSED     =>  I_CONTAINERS_PROCESSED,
              I_OPERATIONS_PERFORMED     =>  I_OPERATIONS_PERFORMED,
              I_UNITS_PROCESSED          =>  I_UNITS_PROCESSED,
              I_UNITS_PICKED             =>  I_UNITS_PICKED,
              I_CASEPACK                 =>  I_CASEPACK,
              I_DEST_ID                  =>  I_DEST_ID,
              I_DISTRO_NBR               =>  I_DISTRO_NBR,
              I_DISTRO_TS                =>  I_DISTRO_TS,
              I_EVENT_CODE               =>  I_EVENT_CODE,
              I_FIRST_SCAN_FLAG          =>  I_FIRST_SCAN_FLAG,
              I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
              I_LABELED_PICKING          =>  I_LABELED_PICKING,
              I_OLD_TO_CID               =>  I_OLD_TO_CID,
              I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
              I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID01   =>  I_PICK_TO_CONTAINER_ID,
              I_PICK_TYPE                =>  I_PICK_TYPE,
              I_SUGGESTED_LOC            =>  I_SUGGESTED_LOCATION,
              I_WAVE_NBR                 =>  I_WAVE_NBR,
              I_ZONE                     =>  I_ZONE,
              P_PM_ALLOW_INTRLVG         =>  I_PM_ALLOW_INTRLVG,
              P_VERSION                  =>  P_VERSION); 
              
        /*� Go_To_Location �*/
        Go_To_Location(
              V_RETURN                 => V_RETURN, 
              I_GOTO_FIELD             => I_GOTO_FIELD,
              I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID ,
              I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID ,
              I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
              I_FACILITY_ID            => I_FACILITY_ID ,
              I_DEST_ID                => I_DEST_ID ,
              I_DISTRO_NBR             => I_DISTRO_NBR ,
              I_LABELED_PICKING        => I_LABELED_PICKING ,
              I_OLD_TO_CID             => I_OLD_TO_CID ,
              I_PICK_TYPE              => I_PICK_TYPE ,
              I_SUGGESTED_LOC          => I_SUGGESTED_LOCATION ,              
              I_WAVE_NBR               => I_WAVE_NBR );
              
   ELSIF I_CALLING_BLOCK = 'TO_LOCATION' THEN
      IF I_PICK_TYPE IN ('B','BR') THEN
       
         		PKG_HOTEL_PICKING_ADF.MOVE_CONTAINER(V_RETURN ,I_MSG_DISPLAY,I_GENERIC_TO_CONTAINER ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_PICK_TO_CONTAINER_ID ,I_SUGGESTED_LOCATION ,I_TO_LOCATION_ID ,I_FACILITY_ID ,I_EVENT_CODE ,I_PICK_TYPE ,I_UNITS_PICKED ,I_WAVE_NBR ,I_USER ,I_DEST_ID);
				IF (V_RETURN IS NOT NULL) THEN
				 RETURN;
				END IF;
				
--         		IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
--         EXCEPTION
--           	  WHEN OTHERS THEN 
--           	  	 /*� Log_Error_Roll(SQLCODE, 'Move_Container', SQLERRM) �*/ 
     
      END IF; 
	 IF I_LABELED_PICKING = 'Y' THEN 
		 I_GOTO_FIELD := 'EXIT';
		 /*� Tr_Exit �*/
	 ELSE
		 /*� GO_ITEM('Bulk_Pick.Generic_To_Container') �*/
		 I_GOTO_FIELD := 'BULK_PICK.GENERIC_TO_CONTAINER';
		 /*� CLEAR_BLOCK �*/
		 /*� Get_Generic_Pick_Directive �*/
		 
	 END IF; 
   END IF;
EXCEPTION
WHEN req_weight THEN
    /*� Call_Msg_Window_P('ENTER_WEIGHT') �*/
    I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'ENTER_WEIGHT');
    PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
--    RAISE FRM_TRIGGER_FAILURE;
WHEN OTHERS THEN
    /*� Log_Error_Roll(SQLCODE, 'Process Weight ', SQLERRM) �*/
   PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process',SQLERRM);
   PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);    
--   RAISE FRM_TRIGGER_FAILURE;
END PROCESS_WEIGHT;
--
--
PROCEDURE Process_to_loc_C(
    V_RETURN                   IN OUT VARCHAR2,
    I_FROM_LOCATION_ID       IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_TO_LOCATION_ID         IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 )
IS

/********************************************************************************************
*  Change History:                                                                
*    
*   Author           Date         Defect/CR#    Change Description      
*   --------------------------------------------------------------------------
*   Manan Shah       07/06/2007   CR35261       WMS Splitting: Accenture Change
*																							  R3.2 Process Full pallet Cs as BPs
*                                               Initial Version
*   Michae Chiw      11/26/2008   CR49347       Modified to call DETERMIN_CONTAINER_FINAL_LOC
*********************************************************************************************/

	l_cont_id            Container.Container_ID%TYPE;
	l_service_code       Container.Service_Code%TYPE;
	l_route              Container.Route%TYPE;
	l_final_loc_out      Location.Location_ID%TYPE;
	l_final_loc_in       Location.Location_ID%TYPE;
	l_to_loc_id_out      Location.Location_ID%TYPE;
	l_return_code_out    VARCHAR2(200);
	l_program_name_out   VARCHAR2(200);	
	l_return_status      VARCHAR2(40);
--start CO49347		
  l_dest_id            Ship_Dest.Dest_ID%TYPE;
  l_carrier_out        Carrier_Service_Route.Carrier_Code%TYPE;
  l_final_cont_id      Container.Container_ID%TYPE;
  l_expedite_flag      Stock_Allocation.Expedite_Flag%TYPE;
  l_def_carrier_code        Carrier_Service_Route.Carrier_Code%TYPE;
  l_def_service_code        Carrier_Service_Route.Service_Code%TYPE;
  l_def_route              	Carrier_Service_Route.Route%TYPE;
  l_wh_id									  wh.wh_id%TYPE;
  l_divert_flag							wh.create_divert_flag%TYPE;
  l_wh_found_flag           VARCHAR2(1);
  l_message									error_log.error_msg%TYPE;
  l_code 									  error_log.error_code%TYPE;           
--end CO49347
	
	
  CURSOR get_pick_to_cont	IS
	SELECT container_id,
--start CO49347
         service_code, route,
         location_id,
         dest_id
--end CO49347
    FROM Container 
   WHERE facility_id = I_FACILITY_ID    
     AND master_container_id = NVL(I_PICK_TO_CONTAINER_ID,
                                   I_PICK_FROM_CONTAINER_ID);

CURSOR ship_dest_cursor (dest_id_in IN ship_dest.dest_id%TYPE) IS
SELECT Default_Carrier_Code, Default_Service_Code, Default_Route
  FROM Ship_Dest
 WHERE Facility_ID = I_FACILITY_ID
   AND Dest_ID     = dest_id_in;

--start CO49347
/*	CURSOR get_door_loc_id IS 
	SELECT d.location_id   
	  FROM Service_Rt_Door s, 
	       Door d  
	 WHERE s.facility_id = I_FACILITY_ID
	   AND s.service_code = l_service_code    
	   AND s.route = l_route    
	   AND s.wh_id = l_wh_id    
	   AND d.facility_id = s.facility_id    
	   AND d.door_id = s.door_id;    

	CURSOR get_logical_dest_cursor IS 
	SELECT NVL(Location.logical_dest_id, '-1')   
	  FROM Location  
	 WHERE facility_id = I_FACILITY_ID
	   AND location_id = l_to_loc_id_out; 
	   
	CURSOR timestamp_duplicate_cursor IS
	SELECT transaction_ts  
	  FROM Sorter_Instructions 
	 WHERE facility_id    = I_FACILITY_ID  
	   AND container_id   = l_cont_id   
	   AND transaction_ts IS NULL;*/
--end CO49347
BEGIN
	
     FOR get_pick_to_cont_rec IN get_pick_to_cont LOOP
        -- Assign values to variables.                      
        l_cont_id          := get_pick_to_cont_rec.container_id;
        l_service_code     := get_pick_to_cont_rec.service_code;
        l_route            := get_pick_to_cont_rec.route;
        -- l_container_status := get_pick_to_cont_rec.container_status;
 
        -- Get the final location id.
        -- Call get_wh_id to get the WH_ID of the dropoff location
        get_wh_id (I_FACILITY_ID,
                   I_TO_LOCATION_ID,
                   l_wh_id,
                   l_divert_flag,
                   l_wh_found_flag);

        IF l_wh_found_flag = 'N' THEN
           l_message := 'Location does not have a warehouse id.'||
                        ' Location :'||I_FROM_LOCATION_ID||
                        ' Container :'||l_cont_id;
           l_code    := SQLCODE;
           INSERT INTO Error_Log (facility_id,
                                  error_code,
                                  user_id,
                                  error_ts,
                                  error_source,
                                  error_loc,
                                  error_msg)
                          VALUES (I_FACILITY_ID,
                                  l_code,
                                  I_USER,
                                  SYSDATE,
                                  'HH_BULK_PK_ACROSS_WV_S',
                                  'Process_To_Location',
                                  l_message);
        END IF;
        
        l_dest_id      := get_pick_to_cont_rec.dest_id;

			  OPEN ship_dest_cursor (l_dest_id);
			  FETCH ship_dest_cursor INTO l_def_carrier_code, l_def_service_code, l_def_route;
			  CLOSE ship_dest_cursor;
			  
        --call this procedure to determine the container final location.
        get_cont_final_loc_by_sorter ( I_FACILITY_ID,
                                        l_wh_id,
                                        NVL(l_def_service_code, l_service_code),
                                        NVL(l_def_route, l_route),
                                        l_final_loc_out,
                                        l_return_status,
                                        l_program_name_out,
                                        l_return_code_out
                                     );

        IF l_return_status <> 'SUCCESS' THEN
          /*� log_error(I_FACILITY_ID,l_program_name_out,l_return_code_out) �*/
            PKG_COMMON_ADF.LOG_ERROR(V_RETURN,I_FACILITY_ID,l_program_name_out,l_return_code_out);
        END IF;
        --end CO49347     
        l_final_loc_in := l_final_loc_out;

        l_return_status := determine_container_to_loc (I_FACILITY_ID,
                                                       l_cont_id,
                                                       l_final_loc_in,
                                                       NULL,
                                                       l_to_loc_id_out,
                                                       l_return_code_out,
                                                       l_program_name_out);
        
        IF l_return_status <> 'SUCCESS' THEN
            /*� log_error(NULL, l_program_name_out, l_return_code_out) �*/
            PKG_COMMON_ADF.LOG_ERROR(V_RETURN,null,l_program_name_out,l_return_code_out);
        END IF;                                                    
				
        UPDATE Container
           SET to_location_id    = l_to_loc_id_out,
               final_location_id = l_final_loc_out,
               service_code      = NVL(l_def_service_code, l_service_code),
               route             = NVL(l_def_route, l_route)
         WHERE facility_id  = I_FACILITY_ID
           AND container_id = l_cont_id;

					 --start CO49347			
					 /*        
           IF l_container_status <> 'P' AND l_divert_flag = 'Y' THEN

           OPEN get_logical_dest_cursor;
           FETCH get_logical_dest_cursor INTO l_logical_dest_id;
           CLOSE get_logical_dest_cursor;
           
      	   OPEN timestamp_duplicate_cursor;
           FETCH timestamp_duplicate_cursor INTO l_transaction_ts;
           value_found := timestamp_duplicate_cursor%FOUND;
           CLOSE timestamp_duplicate_cursor;      
           

           IF value_found THEN        	
    		      UPDATE Sorter_Instructions
                 SET logical_dest_id = l_logical_dest_id
               WHERE facility_id     = I_FACILITY_ID
                 AND container_id    = l_cont_id
                 AND transaction_ts IS NULL; 
           ELSE            	
	            INSERT INTO Sorter_Instructions (facility_id,
	                                           	 container_id,
	                                           	 logical_dest_id)
                                       VALUES (I_FACILITY_ID,
                                               l_cont_id,
                                               l_logical_dest_id);                                             
	         END IF; -- IF value_found
	      END IF; -- IF l_container_status <> 'P' AND divert_flag = 'Y'*/
        --end CO49347
        COMMIT;
  
  END LOOP;
EXCEPTION
WHEN OTHERS THEN
    /*� Log_Error_Roll(SQLCODE, 'Process_to_loc_C ', SQLERRM) �*/
   PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Process_to_loc_C',SQLERRM);
   PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);      
--   RAISE FRM_TRIGGER_FAILURE;           
END Process_to_loc_C;
--
--
PROCEDURE CONF_CONTAINER_KEY_NEXT (
    V_RETURN                 IN OUT VARCHAR2, 
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CASEPACK               IN OUT NUMBER ,
    I_CONF_CONTAINER_ID      IN VARCHAR2 ,
    I_CONTAINER_QTY          IN NUMBER ,
    I_ITEM_ID                IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_CONTAINER_QTY2         IN NUMBER ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 ,
    I_WAVE_NBR               IN NUMBER )
IS
 

t_unit_qty     container_item.unit_qty%TYPE;
dummy          VARCHAR2(1);
t_status       VARCHAR2(1);

CURSOR Child_Exist_Cursor IS
SELECT 'X' 
  FROM Container
 WHERE Facility_id         = I_FACILITY_ID
   AND Master_Container_id = I_PICK_TO_CONTAINER_ID;
------------------------------------------------------------------
CURSOR Get_Qty_Cursor IS
SELECT Unit_Qty
  FROM Container_Item
 WHERE Facility_Id            = I_FACILITY_ID
   AND Container_Id           = I_PICK_TO_CONTAINER_ID;
------------------------------------------------------------------
CURSOR Get_Qty_LR_Cursor IS
SELECT SUM(CI.Unit_Qty)
  FROM Container_Item CI, Container C
 WHERE C.Facility_Id                   = I_FACILITY_ID
   AND CI.Facility_Id                  = I_FACILITY_ID
   AND C.Master_Container_Id           = I_PICK_TO_CONTAINER_ID
   AND C.Container_Id                  = CI.Container_Id;
-------------------------------------------------------------------
CURSOR get_qtys_cursor IS
SELECT SUM(NVL(CI.unit_qty,0))
  FROM Container C, Container_Item CI
 WHERE C.facility_id         = I_FACILITY_ID
   AND C.master_container_id = I_PICK_TO_CONTAINER_ID
   AND CI.facility_id        = C.facility_id
   AND CI.container_id       = C.container_id
   AND CI.item_id            = I_ITEM_ID;
-------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
-- New cursor to sum up the unit_qty of the containers, this will be used if the pick task is CP and it has
-- multiple sku�s. We can not use the existing cursor because I_ITEM_ID will hold �MULTI � SKU� for
-- CP picks with multiple skus.
------------------------------------------------------------------------------------------------------------

CURSOR get_qtys_CP_cursor IS
SELECT SUM(NVL(CI.unit_qty,0))
  FROM Container C, Container_Item CI
 WHERE C.facility_id         = I_FACILITY_ID
   AND C.master_container_id = I_PICK_TO_CONTAINER_ID
   AND CI.facility_id        = C.facility_id
   AND CI.container_id       = C.container_id
   AND CI.item_id           IN (SELECT Item_id
                                  FROM pick_directive
                                 WHERE facility_id = I_FACILITY_ID
                                   AND pick_from_container_id = I_PICK_FROM_CONTAINER_ID
                                   -- Start CR35261 - LCC
                                   /*AND pick_type IN (I_PICK_TYPE, 'CP'));*/
                                   AND pick_type IN (I_PICK_TYPE, 'CP', 'C'));
                                   -- End CR35261


BEGIN
	
	
	 t_status := PKG_HOTEL_PICKING_ADF.OPEN_WAVE(V_RETURN ,I_MSG_DISPLAY, I_FACILITY_ID ,I_WAVE_NBR  );
	 IF (V_RETURN IS NOT NULL) THEN
	   RETURN;
	 END IF;
	-- IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;

-- NOTE: This validation is moved to client side.
--   IF NVL(I_CONF_CONTAINER_ID,' ') <> I_PICK_FROM_CONTAINER_ID THEN
--       /*� Call_Msg_Window_P('INV_CONTAINER', 'W') �*/
--      I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER');
--      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN); 
--   END IF;
   --
   IF I_PICK_TYPE = 'BP' THEN
      IF I_LABELED_RESERVE = 'N' THEN
      
         OPEN Child_Exist_Cursor;
         FETCH Child_Exist_Cursor INTO dummy;
         CLOSE Child_Exist_Cursor;
         
         IF dummy is NULL THEN
            OPEN Get_Qty_Cursor;
            FETCH Get_Qty_Cursor INTO t_unit_qty;
            CLOSE Get_Qty_Cursor;
         ELSE
            OPEN get_qtys_cursor;
            FETCH get_qtys_cursor INTO t_unit_qty;
            CLOSE get_qtys_cursor;
         END IF;
      ELSE
         OPEN Get_Qty_LR_Cursor;
         FETCH Get_Qty_LR_Cursor INTO t_unit_qty;
         CLOSE Get_Qty_LR_Cursor;
      END IF;
      --
      I_CASEPACK :=
         ROUND( NVL(t_unit_qty,0)/NVL(I_CONTAINER_QTY,1) );
      --
   -- Start CR35261 - LCC
   /*ELSIF I_PICK_TYPE = 'CP' THEN*/
   ELSIF I_PICK_TYPE IN ('CP', 'C') THEN
   -- End CR35261
      IF I_LABELED_RESERVE = 'N' THEN
         OPEN Child_Exist_Cursor;
         FETCH Child_Exist_Cursor INTO dummy;
         CLOSE Child_Exist_Cursor;
         IF dummy is NULL THEN
            OPEN Get_Qty_Cursor;
            FETCH Get_Qty_Cursor INTO t_unit_qty;
            CLOSE Get_Qty_Cursor;
         ELSE
            OPEN get_qtys_CP_cursor;
            FETCH get_qtys_CP_cursor INTO t_unit_qty;
            CLOSE get_qtys_CP_cursor;
         END IF;
      ELSE
         OPEN Get_Qty_LR_Cursor;
         FETCH Get_Qty_LR_Cursor INTO t_unit_qty;
         CLOSE Get_Qty_LR_Cursor;
      END IF;
      --
     --CR35261: LCC � change #1
     /*I_CASEPACK :=*/
     /*ROUND( NVL(t_unit_qty,0)/NVL(I_CONTAINER_QTY,1) );*/
      
      IF I_PICK_TYPE <> 'C' THEN
        I_CASEPACK := ROUND( NVL(t_unit_qty,0)/NVL(I_CONTAINER_QTY,1) );
      ELSE
        I_CASEPACK := ROUND( NVL(t_unit_qty,0)/NVL(I_CONTAINER_QTY2,1) );
      END IF;
   END IF;
   -- End CR35261
   --
    /*� NEXT_FIELD �*/
    
END CONF_CONTAINER_KEY_NEXT;
 
--
/*
 Wrapper around PKG_HOTEL_PICKING_ADF.V_REASON_CODE to return INT instead of boolean
*/
  FUNCTION V_REASON_CODE(
      V_RETURN         IN OUT VARCHAR2,
      I_MSG_DISPLAY    IN OUT VARCHAR2,
      I_CODE           IN VARCHAR2 )
    RETURN INT
  IS
   
  BEGIN
 
    RETURN PKG_COMMON_ADF.Boolean2Int(PKG_HOTEL_PICKING_ADF.V_REASON_CODE(V_RETURN, I_MSG_DISPLAY , I_CODE ));
  
  END V_REASON_CODE;
--
PROCEDURE ahl_loc_override(
    I_SUGGESTED_LOCATION IN VARCHAR2 ,
    I_TO_LOCATION_ID     IN VARCHAR2 ,
    I_FACILITY_ID        IN VARCHAR2 ,
    SYSTEM_CURRENT_FORM  IN VARCHAR2 ,
    PK1_VALUE            IN ACTIVITY_LOG.CONTROL_DATA1%TYPE)
IS
curform varchar2(40);
n_transaction_id         activity_log.transaction_id%TYPE;
s_transaction_type       activity_log.transaction_type%TYPE;
b_first_insert           BOOLEAN;
BEGIN



  IF I_TO_LOCATION_ID <> I_SUGGESTED_LOCATION THEN
 	   	select transaction_id.nextval into n_transaction_id from dual;
  	   	curform := SYSTEM_CURRENT_FORM;
  	   	b_first_insert := TRUE;
  	   	s_transaction_type := 'O';
  	   	write_activity_log_data(
  	   		                  b_first_insert,
  	   	                    I_FACILITY_ID,
  	   	                    n_transaction_id,
  	   	                    s_transaction_type,
  	   	                    curform,
  	   	                    'CONTAINER',
  	   	                    'LOCATION_ID',
  	   	                    I_SUGGESTED_LOCATION,
  	   	                    I_TO_LOCATION_ID,
  	   	                    'MASTER_CONTAINER_ID',PK1_VALUE,null,null,null,null,null,null,null,null,
  	   											null,null,null,null,null,null,null,null,null,null);
   End If;
END;
--
-- procedure is called on F4 Done key on TO_LOCATION page2 of bulk picking form
--
 PROCEDURE TO_LOCATION_DONE_KEY (
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID   IN OUT VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT VARCHAR2 ,
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2 ,
    I_SUGGESTED_LOC            IN OUT VARCHAR2,
    I_TOTAL_WEIGHT             IN OUT NUMBER ,
    I_CALLING_BLOCK            IN OUT VARCHAR2 ,
    I_GOTO_FIELD               IN OUT VARCHAR2,
    I_UNITS_PICKED             IN OUT NUMBER,
    I_UNITS_PROCESSED          IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER,
    I_OLD_TO_CID               IN OUT VARCHAR2,
    I_PICK_FROM_CONTAINER_ID01 IN OUT VARCHAR2 , 
    I_MASTER_TO_CONTAINER      IN OUT VARCHAR2 ,
    I_FIRST_WEIGHT_FLAG        IN OUT VARCHAR2 ,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER,
    P_PM_NO_PICK_FLAG          IN OUT VARCHAR2 ,
    P_PM_NO_TASK_FLAG          IN OUT VARCHAR2 , 
    I_FIRST_SCAN_FLAG          IN OUT VARCHAR2 ,
    P_Global_Intrlvg           IN OUT VARCHAR2,
    I_TO_LOCATION_ID           IN VARCHAR2 ,
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2 ,
    I_DEST_ID                  IN NUMBER ,
    I_EVENT_CODE               IN NUMBER ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER,
    P_VERSION                  IN NUMBER,
    I_BULK_GROUP               IN VARCHAR2,
    I_DISTRO_NBR               IN VARCHAR2,
    I_DISTRO_TS                IN DATE,
    I_ZONE                     IN VARCHAR2,
    I_PM_ALLOW_INTRLVG         IN VARCHAR2,  
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY2           IN NUMBER ,
    I_EMPTY_PRESSED            IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,  
    SYSTEM_CURRENT_FORM        IN VARCHAR2) is
 ahl_container_id     Container.Container_id%TYPE;
 BEGIN
 
   DELETE
  FROM ROSS_EVENTCODE_USERID_TMP;
  COMMIT;
  IF I_PICK_TYPE IN ('CP','C','BP') THEN
    INSERT
    INTO ROSS_EVENTCODE_USERID_TMP
      (
        EVENT_CD,
        USER_ID
        
      )
      VALUES
      (
        I_EVENT_CODE,
        I_USER
      );
      END IF;
         PKG_HOTEL_PICKING_ADF.Validate_To_Location3(
                V_RETURN         => V_RETURN,
                I_MSG_DISPLAY    => I_MSG_DISPLAY,
                I_FACILITY_ID    => I_FACILITY_ID ,
                suggested_loc_in => I_SUGGESTED_LOCATION,
                location_id_in   => I_TO_LOCATION_ID );
				/*� IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF; �*/
		IF (V_RETURN IS NOT NULL)  THEN
		  RETURN;
		END IF;
         I_FIRST_SCAN_FLAG := 'Y';
         
         -- Check for catch weight --
         IF (I_TOTAL_WEIGHT   is NULL or I_TOTAL_WEIGHT = 0) AND I_PICK_TYPE = 'B' THEN
           I_FIRST_WEIGHT_FLAG   := 'N';
           I_CALLING_BLOCK       := 'TO_LOCATION';
          /*� Check_Catch_Weight (:work.calling_block);�*/
          CHECK_CATCH_WEIGHT(
                    V_RETURN                 =>  V_RETURN,
                    I_MSG_DISPLAY            => I_MSG_DISPLAY,
                    I_CONF_LOCATION_ID       => I_CONF_LOCATION_ID,
                    P_VERSION                => P_VERSION,
                    I_CONTAINER_QTY          => I_CONTAINER_QTY,
                    I_BULK_GROUP             => I_BULK_GROUP ,
                    I_ORIGINAL_PICK_QTY      => I_ORIGINAL_PICK_QTY,
                    I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                    I_CONTAINERS_PROCESSED   => I_CONTAINERS_PROCESSED,
                    I_OPERATIONS_PERFORMED   => I_OPERATIONS_PERFORMED,
                    I_UNITS_PROCESSED        => I_UNITS_PROCESSED,
                    I_CASEPACK               => I_CASEPACK,
                    I_DISTRO_NBR             => I_DISTRO_NBR,
                    I_DISTRO_TS              => I_DISTRO_TS,
                    I_FIRST_SCAN_FLAG        => I_FIRST_SCAN_FLAG,
                    I_OLD_TO_CID             => I_OLD_TO_CID,
                    I_PICK_CONTAINER_QTY     => I_PICK_CONTAINER_QTY,
                    I_ZONE                   => I_ZONE, 
                    I_PM_ALLOW_INTRLVG       => I_PM_ALLOW_INTRLVG,
                    I_GENERIC_TO_CONTAINER   => I_GENERIC_TO_CONTAINER ,
                    I_ITEM_ID                => I_ITEM_ID,
                    I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID,
                    I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID,
                    I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
                    I_SUGGESTED_LOC          => I_SUGGESTED_LOC ,
                    I_TO_LOCATION_ID         => I_TO_LOCATION_ID,
                    I_TOTAL_WEIGHT           => I_TOTAL_WEIGHT,
                    I_CALLING_BLOCK          => I_CALLING_BLOCK,
                    I_FACILITY_ID            => I_FACILITY_ID,
                    I_USER                   => I_USER,
                    I_DEST_ID                => I_DEST_ID,
                    I_EVENT_CODE             => I_EVENT_CODE,
                    I_FIRST_WEIGHT_FLAG      => I_FIRST_WEIGHT_FLAG,
                    I_LABELED_RESERVE        => I_LABELED_RESERVE,
                    I_LABELED_PICKING        => I_LABELED_PICKING,
                    I_PICK_TYPE              => I_PICK_TYPE,
                    I_UNITS_PICKED           => I_UNITS_PICKED,
                    I_WAVE_NBR               => I_WAVE_NBR,
                    I_GOTO_FIELD             => I_GOTO_FIELD,
                    calling_block_in         => I_CALLING_BLOCK);
         ELSIF I_TOTAL_WEIGHT > 0 THEN
           I_CALLING_BLOCK := 'TO_LOCATION';
           /*� Process_weight;�*/
           PROCESS_WEIGHT(
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_PICK_TO_CONTAINER_ID     => I_PICK_TO_CONTAINER_ID,
              I_SUGGESTED_LOCATION       => I_SUGGESTED_LOCATION,
              I_TOTAL_WEIGHT             => I_TOTAL_WEIGHT,
              I_CALLING_BLOCK            => I_CALLING_BLOCK,
              I_GOTO_FIELD               => I_GOTO_FIELD,
              I_UNITS_PICKED             => I_UNITS_PICKED,
              I_UNITS_PROCESSED          => I_UNITS_PROCESSED,
              I_CASEPACK                 => I_CASEPACK,
              I_OLD_TO_CID               => I_OLD_TO_CID,
              I_PICK_FROM_CONTAINER_ID01 => I_PICK_FROM_CONTAINER_ID01 , 
              I_MASTER_TO_CONTAINER      => I_MASTER_TO_CONTAINER,
              I_FIRST_WEIGHT_FLAG        => I_FIRST_WEIGHT_FLAG,
              I_CONTAINERS_PROCESSED     => I_CONTAINERS_PROCESSED,
              I_OPERATIONS_PERFORMED     => I_OPERATIONS_PERFORMED,
              I_TO_LOCATION_ID           => I_TO_LOCATION_ID,
              I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
              I_GENERIC_TO_CONTAINER     => I_GENERIC_TO_CONTAINER,
              I_ITEM_ID                  => I_ITEM_ID,
              I_PICK_FROM_CONTAINER_ID   => I_PICK_FROM_CONTAINER_ID,
              I_FACILITY_ID              => I_FACILITY_ID,
              I_USER                     => I_USER,
              I_DEST_ID                  => I_DEST_ID,
              I_EVENT_CODE               => I_EVENT_CODE,
              I_LABELED_PICKING          => I_LABELED_PICKING,
              I_PICK_TYPE                => I_PICK_TYPE,
              I_WAVE_NBR                 => I_WAVE_NBR,
              P_VERSION                  => P_VERSION,
              I_BULK_GROUP               => I_BULK_GROUP,
              I_DISTRO_NBR               => I_DISTRO_NBR,
              I_DISTRO_TS                => I_DISTRO_TS,
              I_ZONE                     => I_ZONE,
              I_PM_ALLOW_INTRLVG         => I_PM_ALLOW_INTRLVG,
              I_CONTAINER_QTY            => I_CONTAINER_QTY,
              I_FROM_LOCATION_ID         => I_FROM_LOCATION_ID,
              I_CONTAINER_QTY2           => I_CONTAINER_QTY2,
              I_EMPTY_PRESSED            => I_EMPTY_PRESSED,
              I_FIRST_SCAN_FLAG          => I_FIRST_SCAN_FLAG,
              I_LABELED_RESERVE          => I_LABELED_RESERVE,
              I_ORIGINAL_PICK_QTY        => I_ORIGINAL_PICK_QTY,
              I_PICK_CONTAINER_QTY       => I_PICK_CONTAINER_QTY);
         ELSE -- no catch_weight --
            -- Start - CR35261 - MShah
           IF I_PICK_TYPE = 'C' THEN
             /*� Process_to_loc_C;�*/
              Process_to_loc_C(
                  V_RETURN                 => V_RETURN,
                  I_FROM_LOCATION_ID       => I_FROM_LOCATION_ID,
                  I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID,
                  I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID,
                  I_TO_LOCATION_ID         => I_TO_LOCATION_ID,
                  I_FACILITY_ID            => I_FACILITY_ID,
                  I_USER                   => I_USER);
           END IF;
           -- End - CR35261 - MSha
          
              PKG_HOTEL_PICKING_ADF.MOVE_CONTAINER(V_RETURN ,I_MSG_DISPLAY,I_GENERIC_TO_CONTAINER ,I_ITEM_ID ,I_PICK_FROM_CONTAINER_ID ,I_PICK_TO_CONTAINER_ID ,I_SUGGESTED_LOCATION ,I_TO_LOCATION_ID ,I_FACILITY_ID ,I_EVENT_CODE ,I_PICK_TYPE ,I_UNITS_PICKED ,I_WAVE_NBR ,I_USER ,I_DEST_ID);
				IF (V_RETURN IS NOT NULL) THEN
				  RETURN;
				END IF;
                                
				--        IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
--           EXCEPTION
--           	  WHEN OTHERS THEN 
--           	  	Log_Error_Roll(SQLCODE, 'Move_Container', SQLERRM);
--           END;
--           IF FORM_SUCCESS THEN
             IF I_Labeled_Picking = 'Y' THEN 
			    I_GOTO_FIELD :=  'EXIT';
				RETURN; 
             ELSE
             /* Start CO43433 LCoronel - 1 */
                IF I_PM_ALLOW_INTRLVG = 'Y' THEN
                   P_PM_NO_TASK_FLAG := 'N';
                   P_PM_NO_PICK_FLAG := 'N';
                   PKG_HOTEL_PICKING_ADF.PREPARE_EXIT_PICKING_INTRLVG(I_FACILITY_ID ,I_USER  );
                   P_Global_Intrlvg := 'INTRLVG';
                   /*� Set_Global_Intrlvg('INTRLVG') 
                   NEW_FORM( lower('hh_putaway_inventory_s')); �*/
                ELSE
                   I_GOTO_FIELD := 'BULK_PICK.GENERIC_TO_CONTAINER';
                   /*�CLEAR_BLOCK;
                   GO_ITEM('Bulk_Pick.Generic_To_Container');
                   CLEAR_BLOCK;
                   Get_Generic_Pick_Directive; �*/
                END IF;
             /* End CO43433 LCoronel - 1 */
             END IF; 
         END IF;
         -- AHL override change
         IF I_SUGGESTED_LOCATION <> I_TO_LOCATION_ID THEN
         	  IF I_Labeled_Picking = 'Y' THEN 
         	  	 ahl_container_id := I_PICK_TO_CONTAINER_ID;
         	  ELSE 
         	  	 ahl_container_id := I_GENERIC_TO_CONTAINER;
         	  END IF;	  
            --
           ahl_loc_override(
            I_SUGGESTED_LOCATION => I_SUGGESTED_LOCATION,
            I_TO_LOCATION_ID     => I_TO_LOCATION_ID,
            I_FACILITY_ID        => I_FACILITY_ID,
            SYSTEM_CURRENT_FORM  => SYSTEM_CURRENT_FORM,
            PK1_VALUE            => ahl_container_id);
            --
         END IF; -- End AHL change.
 END TO_LOCATION_DONE_KEY;
 --
PROCEDURE TR_EXIT_BP (    
    V_RETURN                   IN OUT VARCHAR2,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2,      
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_OPERATIONS_PERFORMED     IN NUMBER,
    I_Units_Processed          IN NUMBER,
    I_Containers_Processed     IN NUMBER,
    I_REFERENCE_CODE           IN VARCHAR2 ,
    I_Start_Time               IN DATE 
    ) IS
BEGIN
    /* Begin Change Infosys technologies R3 'BP pick completed' -  Container History     09/22/2006*/   		
    /* Code Added */
    /* Deleting Global Temporary Table before Form close*/
    	 --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
      	 DELETE from ROSS_EVENTCODE_USERID_TMP; 
      	 COMMIT;
    /* End Change Infosys technologies R3 'BP pick completed' -  Container History     09/22/2006*/   		

   /*� Save_Labor_Prod �*/
    Save_Labor_Prod( p_facility_id => i_facility_id, p_labeled_picking => i_labeled_picking, p_user => I_user, p_Operations_Performed => I_operations_performed, 
            p_Start_Time => I_start_time, p_Units_Processed => I_units_processed, p_Containers_Processed => I_containers_processed, p_Reference_Code => I_reference_code, V_RETURN => v_return);
   
   -- BEGIN CR560
   -- IF :BULK_PICK.PICK_FROM_CONTAINER_ID IS NOT NULL THEN
   -- END CR560

      -- CR35260 - Also unlock locked CP picks
      /* UPDATE PICK_DIRECTIVE 
         SET PICK_IN_PROGRESS = 'N',
             USER_ID          = NULL
       WHERE FACILITY_ID      = :WORK.FACILITY_ID
         -- BEGIN CR560 
   	     -- Include the current user and pick_in_progress flag in where condition
   	     AND USER_ID          = :WORK.USER    -- current user
   	     AND PICK_IN_PROGRESS = 'Y'  -- pick_in_progress flag   	     
         -- AND PICK_FROM_CONTAINER_ID = :BULK_PICK.PICK_FROM_CONTAINER_ID
         -- END CR560
         AND PICK_TYPE IN ('B', 'BP');  */

   -- BEGIN CR560        
   -- END IF;
   -- END CR560
   
      UPDATE PICK_DIRECTIVE 
         SET PICK_IN_PROGRESS = 'N',
             USER_ID          = NULL
       WHERE FACILITY_ID      = I_FACILITY_ID
   	     AND USER_ID          = I_USER   
   	     AND PICK_IN_PROGRESS = 'Y'  
         -- Start CR35261
         AND PICK_TYPE IN ('B', 'BP', 'CP', 'C');
         -- End CR35261
         
      COMMIT; 
   
      /*� EXIT_FORM (NO_VALIDATE);�*/
END TR_EXIT_BP;
/*
 Block: Container 
 Field: CONTAINER_ID
 Trigger: Key_Next_item
*/
PROCEDURE CONT_CONTAINER_ID_KEY_NEXT (
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_CONTAINER_ID           IN OUT VARCHAR2 ,    
    I_NBR_CHILD              IN OUT NUMBER ,
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_EVENT_CODE             IN NUMBER ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_PICK_TYPE              IN VARCHAR2 )
IS
 
--
Begin
--
-- KEY-NEXT-ITEM (CONTAINER.CONTAINER_ID
--This procedure processes the container id scanned in by the cast member as a result of 
--inputting a quantity less than the reqested quantity.  Find out which containers are 
--missing
--
DECLARE
  rec_count  NUMBER(8);
  i          NUMBER(8);
  l_found    BOOLEAN;
  l_child   container.container_id%TYPE;
  
CURSOR container_data
IS
  SELECT Container_ID
  FROM Container
  WHERE Facility_ID       = I_FACILITY_ID
  AND Container_Status    <> 'D'
  AND Container_ID        = I_CONTAINER_ID
  AND Master_Container_ID = I_PICK_FROM_CONTAINER_ID;
--
  CURSOR find_sys_gen_child IS
     SELECT container_id
       FROM Container
      WHERE facility_id         = I_FACILITY_ID
        /*AND Master_Container_Id = I_PICK_TO_CONTAINER_ID*/
        AND Container_Status    = 'P'
        AND ((I_PICK_TYPE <> 'CP'
              AND Master_Container_Id = I_PICK_TO_CONTAINER_ID)
              OR 
             (I_PICK_TYPE = 'CP'
              AND pick_from_container_id = I_PICK_TO_CONTAINER_ID
              AND master_container_id = I_PICK_TO_CONTAINER_ID));

BEGIN
  -- Start CR35261 - LCC: issue# 88 Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
   --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
     DELETE from ROSS_EVENTCODE_USERID_TMP; 
     COMMIT;

  IF I_PICK_TYPE IN ('CP','C','BP') THEN
     INSERT INTO ROSS_EVENTCODE_USERID_TMP
     (
      EVENT_CD,
      USER_ID,
      LOCATION_ID
     )
     VALUES (I_EVENT_CODE, I_USER, I_CONF_LOCATION_ID);
  END IF;
  -- End CR35261 - LCC: issue# 88
  
  --
  --Find out how many children the system thinks exist
  --
  rec_count :=  null/*� Get_Group_Row_Count('CONTAINER_DATA') �*/;
  
  --
  --Loop through each child and try to find the one scanned in
  --

  l_found := FALSE;
  OPEN container_data;
  FETCH container_data into l_child;
  l_found  := container_data%FOUND;
  CLOSE container_data; 

   IF l_found = TRUE THEN
     I_NBR_CHILD := I_NBR_CHILD + 1;
     -- update system generated to container as each picked from container is picked --
     -- unpicked to_containers will be deleted from the system.                      --
     IF I_LABELED_PICKING = 'Y' THEN
         OPEN find_sys_gen_child;
         FETCH find_sys_gen_child INTO l_child;
         CLOSE find_sys_gen_child;
         --
         IF l_child is NOT NULL THEN
            UPDATE container
               SET container_status = 'D',
                   user_id = I_USER
             WHERE facility_id = I_FACILITY_ID
               AND (container_id = I_PICK_TO_CONTAINER_ID OR
                    container_id = l_child);
         END IF;
     ELSIF I_LABELED_PICKING = 'N' THEN
        UPDATE container
           SET container_status = 'D',
               user_id = I_USER
         WHERE facility_id = I_FACILITY_ID
           AND container_id = I_CONTAINER_ID;
     END IF;
     commit;
   END IF;
   I_CONTAINER_ID := NULL;

   --
   --Check to see if the container was found
   --
   IF l_found = FALSE THEN
     --
     --Tell the cast member that the container is invalid - it should be removed
     --
      /*� Call_Msg_Window_P('INV_CONTAINER') �*/
      I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER', 'W');
   END IF;
END;
End;
--
/*
 Block: Label Child 
 Field: Generic_TO_CHILD
 Trigger: Key_Next_item
*/
PROCEDURE LC_GENERIC_TO_CHILD_KEY_NEXT(
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_CONTAINER_QTY        IN OUT NUMBER ,
    I_GENERIC_TO_CHILD     IN OUT VARCHAR2 ,    
    I_FIRST_SCAN_FLAG      IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID     IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID IN VARCHAR2 ,
    I_FACILITY_ID          IN VARCHAR2 ,
    I_USER                 IN VARCHAR2 ,
    I_EVENT_CODE           IN NUMBER ,
    I_PICK_TYPE            IN VARCHAR2 )
IS
Begin
DECLARE
t_dummy   VARCHAR2(1);
t_child   container.container_id%TYPE;

CURSOR check_child_container IS
   SELECT 'X'
     FROM Container
    WHERE facility_id  = I_FACILITY_ID
      AND Container_Id = I_GENERIC_TO_CHILD;

CURSOR find_sys_child IS
   SELECT container_id
     FROM Container
    WHERE facility_id         = I_FACILITY_ID
      AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
      AND Container_Status    = 'P';

BEGIN
   
   -- Start CR35261 - LCC: issue# 88 Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
   --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
     DELETE from ROSS_EVENTCODE_USERID_TMP; 
     COMMIT;

  IF I_PICK_TYPE IN ('CP','C','BP') THEN
     INSERT INTO ROSS_EVENTCODE_USERID_TMP
     (
      EVENT_CD,
      USER_ID,
      LOCATION_ID
     )
     VALUES (I_EVENT_CODE, I_USER, I_CONF_LOCATION_ID);
  END IF;
-- End CR35261 - LCC: issue# 88
  
   -- check that generic child label does not already exist in the system --
   IF I_GENERIC_TO_CHILD IS NOT NULL THEN
      OPEN check_child_container;
      FETCH check_child_container INTO t_dummy;
      IF t_dummy is not NULL THEN
          /*� Call_Msg_Window_P('INV_CONTAINER') �*/
        I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER', 'W');
        -- RAISE FORM_TRIGGER_FAILURE;
      END IF;
      CLOSE check_child_container;
      --
      OPEN find_sys_child;
      FETCH find_sys_child INTO t_child;
      CLOSE find_sys_child;
      --
      IF t_child is NOT NULL THEN
         IF I_CONTAINER_QTY > 0 THEN
            INSERT INTO Container (Facility_Id,
                                   Container_Id,
                                   Master_Container_Id,
                                   Container_Status)
                           Values (I_FACILITY_ID,
                                   I_GENERIC_TO_CHILD,
                                   I_GENERIC_TO_CONTAINER,
                                   'D');

            UPDATE Container
               SET Container_status = 'D'
             WHERE Facility_Id = I_FACILITY_ID
               AND Container_ID = t_child;

            UPDATE Container_item
               SET Container_id        = I_GENERIC_TO_CHILD
             WHERE Facility_Id         = I_FACILITY_ID
               AND container_id        = t_child;
            --
            COMMIT;
            --
            I_GENERIC_TO_CHILD := '';
            I_CONTAINER_QTY    := I_CONTAINER_QTY - 1;
            I_FIRST_SCAN_FLAG := 'N';
         ELSIF I_CONTAINER_QTY = 0 THEN
             /*� Call_Msg_Window_P('NO_PICKS') �*/
             I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_PICKS', 'W');
           -- RAISE FORM_TRIGGER_FAILURE;
         END IF;
      END IF;
   END IF;
EXCEPTION
--WHEN FORM_TRIGGER_FAILURE THEN
 --RAISE FORM_TRIGGER_FAILURE;
WHEN OTHERS THEN
  /*� Log_Error_Roll(SQLCODE, 'Label_Child.Generic_To_Child ', SQLERRM) �*/  
  PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Label_Child.Generic_To_Child',SQLERRM);
 --RAISE FORM_TRIGGER_FAILURE;
END;
End;

/*
 Block: Label Child 
 Field: PICK_TO_CHILD
 Trigger: Key_Next_item
*/
PROCEDURE LC_PICK_TO_CHILD_KEY_NEXT(
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_CONTAINER_QTY        IN OUT NUMBER ,
    I_PICK_TO_CHILD        IN OUT VARCHAR2 ,
    I_FIRST_SCAN_FLAG      IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID     IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID IN VARCHAR2 ,
    I_FACILITY_ID          IN VARCHAR2 ,
    I_USER                 IN VARCHAR2 ,
    I_EVENT_CODE           IN NUMBER ,
    I_PICK_TYPE            IN VARCHAR2 )
IS
Begin
DECLARE
t_dummy         VARCHAR2(1);
t_child         container.container_id%TYPE;
inv_container   EXCEPTION;
inv_pick       	EXCEPTION;

CURSOR check_child_container IS
   SELECT 'X'
     FROM Container
    WHERE facility_id         = I_FACILITY_ID
      AND Master_Container_Id = I_PICK_TO_CONTAINER_ID
      AND Container_Id        = I_PICK_TO_CHILD
      AND Container_status    = 'P';

BEGIN

   -- Start CR35261 - LCC: issue# 88 Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
   --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
     DELETE from ROSS_EVENTCODE_USERID_TMP; 
     COMMIT;

  IF I_PICK_TYPE IN ('CP','C','BP') THEN
     INSERT INTO ROSS_EVENTCODE_USERID_TMP
     (
      EVENT_CD,
      USER_ID,
      LOCATION_ID
     )
     VALUES (I_EVENT_CODE, I_USER, I_CONF_LOCATION_ID);
  END IF;
  -- End CR35261 - LCC: issue# 88
  
   -- check that labeled child container exists in the system --
   IF I_PICK_TO_CHILD IS NOT NULL THEN
      OPEN check_child_container;
      FETCH check_child_container INTO t_dummy;
      IF t_dummy is NULL THEN
         CLOSE check_child_container;
      	 RAISE inv_container;
      END IF;
      CLOSE check_child_container;
      --
      IF I_CONTAINER_QTY > 0 THEN

         UPDATE Container
            SET Container_status = 'D'
          WHERE Facility_Id = I_FACILITY_ID
            AND Container_ID = I_PICK_TO_CHILD;
         --
         COMMIT;
         --
         I_PICK_TO_CHILD := '';
         I_CONTAINER_QTY    := I_CONTAINER_QTY - 1;
         I_FIRST_SCAN_FLAG := 'N';
      ELSIF I_CONTAINER_QTY = 0 THEN
         RAISE inv_pick;
      END IF;
   END IF;
EXCEPTION
WHEN inv_container THEN
    /*� Call_Msg_Window_P('INV_CONTAINER') �*/
    I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER', 'W');
   --RAISE FORM_TRIGGER_FAILURE;
WHEN inv_pick THEN
    /*� Call_Msg_Window_P('NO_PICKS') �*/
    I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_PICKS', 'W');
  -- RAISE FORM_TRIGGER_FAILURE;
WHEN OTHERS THEN
  /*� Log_Error_Roll(SQLCODE, 'Label_Child.Pick_To_Child ', SQLERRM) �*/
  PKG_COMMON_ADF.LOG_ERROR(V_RETURN, SQLCODE, 'Label_Child.Pick_To_Child',SQLERRM);
 --RAISE FORM_TRIGGER_FAILURE;
END;
End;
--
PROCEDURE LC_F4_KEY(
    V_RETURN                 IN OUT VARCHAR2,
    I_MSG_DISPLAY            IN OUT VARCHAR2,
    I_GENERIC_TO_CHILD       IN OUT VARCHAR2 ,
    I_PICK_TO_CHILD          IN OUT VARCHAR2 ,
    I_OLD_TO_CID             IN VARCHAR2 ,
    I_PICK_CONTAINER_QTY     IN NUMBER ,
    I_LABELED_RESERVE        IN VARCHAR2 ,
    I_LABELED_PICKING        IN VARCHAR2 ,
    I_DISTRO_NBR             IN VARCHAR2,
    I_WAVE_NBR               IN NUMBER ,
    I_DEST_ID                IN NUMBER ,
    I_GENERIC_TO_CONTAINER   IN VARCHAR2 ,
    I_BP_CONTAINER_QTY       IN NUMBER ,
    I_LC_CONTAINER_QTY       IN NUMBER ,
    I_FIRST_SCAN_FLAG        IN OUT VARCHAR2 ,
    I_CONF_LOCATION_ID       IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID   IN VARCHAR2 ,
    I_FACILITY_ID            IN VARCHAR2 ,
    I_USER                   IN VARCHAR2 ,
    I_EVENT_CODE             IN NUMBER ,
    I_PICK_TYPE              IN VARCHAR2 ) IS

CURSOR check_pick_to_exists IS
 SELECT 'X'
   FROM Sys.Dual
  WHERE EXISTS (SELECT facility_id
                  FROM pick_directive
                 WHERE facility_id = I_FACILITY_ID
                   AND (pick_to_container_id = I_PICK_TO_CONTAINER_ID
                        OR pick_to_container_id = NVL(I_OLD_TO_CID,'X')));
t_dummy								VARCHAR2(1);     
t_pick_to_not_exists 	BOOLEAN;                   
BEGIN

  
		   -- CR35260 - Check if pick to child exits
		   OPEN check_pick_to_exists;
		   FETCH check_pick_to_exists INTO t_dummy;
		   t_pick_to_not_exists := check_pick_to_exists%NOTFOUND;
		   CLOSE check_pick_to_exists;
		   --       

       IF I_BP_CONTAINER_QTY <> I_PICK_CONTAINER_QTY THEN
          
          /*� Call_Msg_Window_P('DIFF_PICK') �*/
           I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'DIFF_PICK', 'W');
       END IF;        	
       IF I_LC_CONTAINER_QTY > 0 THEN 
          /*� Call_Msg_Window_P('PICK_REQ') �*/
           I_MSG_DISPLAY :=  PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'PICK_REQ', 'W');
           RAISE FRM_TRIGGER_FAILURE;
       ELSE

          -- CR35260 - Also check for CP picks
          /*IF :work_local.labeled_reserve = 'N' AND 
          	:work_local.pick_type in ('B', 'BP') THEN*/   
          -- Start CR35261       
          IF I_LABELED_RESERVE = 'N' AND I_PICK_TYPE in ('B', 'BP', 'CP', 'C') THEN
          -- End CR35261
             IF I_LABELED_PICKING = 'Y' THEN
                I_PICK_TO_CHILD := '';
                
                -- CR35260 - If pick type is 'CP' then pick to
                --           should not exists in pick directive

                /*DELETE container
                 WHERE facility_id         = :work.facility_id
                   AND master_container_id = :bulk_pick.pick_to_container_id
                   AND container_status    = 'P'; */
                -- Start 35261 - LCC
                /*IF :work_local.pick_type <> 'CP' OR (:work_local.pick_type = 'CP' AND t_pick_to_not_exists) THEN*/
                  IF I_PICK_TYPE NOT IN ('CP', 'C') OR (I_PICK_TYPE IN ('CP', 'C') AND t_pick_to_not_exists) THEN
		                DELETE container
		                 WHERE facility_id         = I_FACILITY_ID
		                   AND master_container_id = I_PICK_TO_CONTAINER_ID
		                   AND container_status    = 'P';
                END IF;
                -- End CR35261
             ELSE
                I_GENERIC_TO_CHILD := '';
                
                BEGIN
                 PKG_HOTEL_PICKING_ADF.TRANSFER_GENERIC_CHILD(
                    V_RETURN                 => V_RETURN,
                    I_MSG_DISPLAY            => I_MSG_DISPLAY,
                    I_GENERIC_TO_CONTAINER   => I_GENERIC_TO_CONTAINER,
                    I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID,
                    I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID,
                    I_FACILITY_ID            => I_FACILITY_ID,
                    I_DEST_ID                => I_DEST_ID,
                    I_DISTRO_NBR             => I_DISTRO_NBR,
                    I_OLD_TO_CID             => I_OLD_TO_CID,
                    I_PICK_TYPE              => I_PICK_TYPE,
                    I_WAVE_NBR               => I_WAVE_NBR) ;
                 
							END;
                -- CR35260 - If pick type is 'CP' then pick to 
                --           should not exists in pick directive

                /*DELETE container
                 WHERE facility_id         = :work.facility_id
                   AND master_container_id = :bulk_pick.generic_to_container
                   AND container_status    = 'P';*/

                -- Start 35261 - LCC
                IF I_PICK_TYPE NOT IN ('CP', 'C') OR (I_PICK_TYPE IN ('CP', 'C') AND t_pick_to_not_exists) THEN
		                DELETE container
		                 WHERE facility_id         = I_FACILITY_ID
		                   AND master_container_id = I_GENERIC_TO_CONTAINER
		                   AND container_status    = 'P';
                END IF;       
                -- End CR35261
             END IF;

             /*� Check_catch_weight (:work.calling_block); �*/
          ELSE
             I_GENERIC_TO_CHILD := '';
             BEGIN
             		PKG_HOTEL_PICKING_ADF.TRANSFER_GENERIC_CHILD(
                    V_RETURN                 => V_RETURN,
                    I_MSG_DISPLAY            => I_MSG_DISPLAY,
                    I_GENERIC_TO_CONTAINER   => I_GENERIC_TO_CONTAINER,
                    I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID,
                    I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID,
                    I_FACILITY_ID            => I_FACILITY_ID,
                    I_DEST_ID                => I_DEST_ID,
                    I_DISTRO_NBR             => I_DISTRO_NBR,
                    I_OLD_TO_CID             => I_OLD_TO_CID,
                    I_PICK_TYPE              => I_PICK_TYPE,
                    I_WAVE_NBR               => I_WAVE_NBR) ;
						 END;	
             /*� Check_catch_weight (:work.calling_block); �*/
          END IF;
        END IF;
END; 
--
PROCEDURE Validate_Qty2( 
    V_RETURN                   IN OUT VARCHAR2,
    I_MSG_DISPLAY              IN OUT VARCHAR2, 
    I_GOTO_FIELD               IN OUT VARCHAR2 ,  
    I_SUGGESTED_LOCATION       IN OUT VARCHAR2 ,   
    I_CONF_LOCATION_ID         IN VARCHAR2 ,
    I_CONTAINER_QTY            IN NUMBER ,
    I_FROM_LOCATION_ID         IN VARCHAR2 ,
    I_GENERIC_TO_CONTAINER     IN VARCHAR2 ,
    I_ITEM_ID                  IN VARCHAR2 ,
    I_PICK_FROM_CONTAINER_ID   IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID     IN OUT  VARCHAR2 ,
    I_TO_LOCATION_ID           IN VARCHAR2 ,
    I_FACILITY_ID              IN VARCHAR2 ,
    I_USER                     IN VARCHAR2,
    I_BULK_GROUP               IN VARCHAR2 ,
    I_ORIGINAL_PICK_QTY        IN NUMBER,
    I_CONTAINERS_PROCESSED     IN OUT NUMBER ,
    I_OPERATIONS_PERFORMED     IN OUT NUMBER ,
    I_UNITS_PROCESSED          IN OUT NUMBER ,    
    I_UNITS_PICKED             IN OUT NUMBER,
    I_CASEPACK                 IN OUT NUMBER ,
    I_DEST_ID                  IN NUMBER ,
    I_DISTRO_NBR               IN VARCHAR2 ,
    I_DISTRO_TS                IN DATE ,
    I_EVENT_CODE               IN NUMBER ,
    I_FIRST_SCAN_FLAG          IN VARCHAR2 ,
    I_LABELED_RESERVE          IN VARCHAR2 ,
    I_LABELED_PICKING          IN VARCHAR2 ,
    I_OLD_TO_CID               IN OUT VARCHAR2 ,
    I_PICK_CONTAINER_QTY       IN NUMBER ,
    I_PICK_FROM_CONTAINER_ID01 IN VARCHAR2 ,
    I_PICK_TO_CONTAINER_ID01   IN VARCHAR2 ,
    I_PICK_TYPE                IN VARCHAR2 ,
    I_SUGGESTED_LOC            IN OUT VARCHAR2 ,
    I_WAVE_NBR                 IN NUMBER ,
    I_ZONE                     IN VARCHAR2 ,
    P_PM_ALLOW_INTRLVG         IN VARCHAR2,
    P_VERSION                  IN NUMBER) IS 
BEGIN

  USER_DONE_PROC (
              V_RETURN                   => V_RETURN,
              I_MSG_DISPLAY              => I_MSG_DISPLAY,
              I_CONF_LOCATION_ID         => I_CONF_LOCATION_ID,
              I_CONTAINER_QTY            =>  I_CONTAINER_QTY,
              I_FROM_LOCATION_ID         =>  I_FROM_LOCATION_ID,
              I_GENERIC_TO_CONTAINER     =>  I_GENERIC_TO_CONTAINER,
              I_ITEM_ID                  =>  I_ITEM_ID,
              I_PICK_FROM_CONTAINER_ID   =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID     =>  I_PICK_TO_CONTAINER_ID,
              I_TO_LOCATION_ID           =>  I_TO_LOCATION_ID,
              I_FACILITY_ID              =>  I_FACILITY_ID,
              I_USER                     =>  I_USER,
              I_BULK_GROUP               =>  I_BULK_GROUP,
              I_ORIGINAL_PICK_QTY        =>  I_ORIGINAL_PICK_QTY,
              I_CONTAINERS_PROCESSED     =>  I_CONTAINERS_PROCESSED,
              I_OPERATIONS_PERFORMED     =>  I_OPERATIONS_PERFORMED,
              I_UNITS_PROCESSED          =>  I_UNITS_PROCESSED,
              I_UNITS_PICKED             =>  I_UNITS_PICKED,
              I_CASEPACK                 =>  I_CASEPACK,
              I_DEST_ID                  =>  I_DEST_ID,
              I_DISTRO_NBR               =>  I_DISTRO_NBR,
              I_DISTRO_TS                =>  I_DISTRO_TS,
              I_EVENT_CODE               =>  I_EVENT_CODE,
              I_FIRST_SCAN_FLAG          =>  I_FIRST_SCAN_FLAG,
              I_LABELED_RESERVE          =>  I_LABELED_RESERVE,
              I_LABELED_PICKING          =>  I_LABELED_PICKING,
              I_OLD_TO_CID               =>  I_OLD_TO_CID,
              I_PICK_CONTAINER_QTY       =>  I_PICK_CONTAINER_QTY,
              I_PICK_FROM_CONTAINER_ID01 =>  I_PICK_FROM_CONTAINER_ID,
              I_PICK_TO_CONTAINER_ID01   =>  I_PICK_TO_CONTAINER_ID,
              I_PICK_TYPE                =>  I_PICK_TYPE,
              I_SUGGESTED_LOC            =>  I_SUGGESTED_LOCATION,
              I_WAVE_NBR                 =>  I_WAVE_NBR,
              I_ZONE                     =>  I_ZONE,
              P_PM_ALLOW_INTRLVG         =>  P_PM_ALLOW_INTRLVG,
              P_VERSION                  =>  P_VERSION); 
               
 Go_To_Location(
    V_RETURN                 => V_RETURN, 
    I_GOTO_FIELD             => I_GOTO_FIELD,
    I_PICK_FROM_CONTAINER_ID => I_PICK_FROM_CONTAINER_ID,
    I_PICK_TO_CONTAINER_ID   => I_PICK_TO_CONTAINER_ID,
    I_SUGGESTED_LOCATION     => I_SUGGESTED_LOCATION,
    I_FACILITY_ID            => I_FACILITY_ID,
    I_DEST_ID                => I_DEST_ID,
    I_DISTRO_NBR             => I_DISTRO_NBR,
    I_LABELED_PICKING        => I_LABELED_PICKING,
    I_OLD_TO_CID             => I_OLD_TO_CID,
    I_PICK_TYPE              => I_PICK_TYPE,
    I_SUGGESTED_LOC          => I_SUGGESTED_LOC,
    I_WAVE_NBR               => I_WAVE_NBR);
END Validate_Qty2;
--
END PKG_PICKING;
/
