CREATE OR REPLACE PACKAGE PKG_INDEPENDENT_ADF
AS
  f_container_status CONTAINER.Container_Status%TYPE;
  f_dest_id CONTAINER.Dest_ID%TYPE;
  f_expedite_flag CONTAINER.Expedite_Flag%TYPE;
  f_carrier_code CONTAINER.Carrier_Code%TYPE;
  f_service_code CONTAINER.Service_Code%TYPE;
  f_route CONTAINER.Route%TYPE;
  f_bol_nbr CONTAINER.BOL_Nbr%TYPE;
  f_master_container_id CONTAINER.Master_Container_ID%TYPE;
  f_location_id CONTAINER.Location_ID%TYPE;
  f_putaway_date CONTAINER.Putaway_Date%TYPE;
  l_new_container VARCHAR2(1) := 'N';
  l_dummy         VARCHAR2(1);
  l_container_id CONTAINER.Container_ID%TYPE;
  f_container_qty CONTAINER_ITEM.Container_Qty%TYPE;
  t_container_qty CONTAINER_ITEM.Container_Qty%TYPE;
  t_expedite_flag CONTAINER.Expedite_Flag%TYPE;
  t_carrier_code CONTAINER.Carrier_Code%TYPE;
  t_service_code CONTAINER.Service_Code%TYPE;
  t_route CONTAINER.Route%TYPE;
  t_bol_nbr CONTAINER.BOL_Nbr%TYPE;
  l_value         VARCHAR2(1);
  l_mixed_dest_id NUMBER;
  l_check CONTAINER.Location_ID%TYPE;
  l_location_id CONTAINER.Location_ID%TYPE;
  f_po_nbr CONTAINER.po_nbr%TYPE;
  c_next_dest_id CONTAINER_ROUTE.Next_dest_id%TYPE               := NULL;
  c_final_dest_id CONTAINER_ROUTE.Final_dest_id%TYPE             := NULL;
  c_current_dest_id CONTAINER_ROUTE.Current_dest_id%TYPE         := NULL;
  g_scp_mst_bld_cont CONTAINER_ROUTE.Master_build_method%TYPE    := NULL;
  t_master_build_method CONTAINER_ROUTE.Master_build_method%TYPE := NULL;
  c_current_route CONTAINER_ROUTE.Current_route%TYPE             := NULL;
  m_next_dest_id CONTAINER_ROUTE.Next_dest_id%TYPE               :=NULL;
  m_final_dest_id CONTAINER_ROUTE.Final_dest_id%TYPE             := NULL;
  m_current_dest_id CONTAINER_ROUTE.Current_dest_id%TYPE         := NULL;
  m_current_route CONTAINER_ROUTE.Current_route%TYPE             := NULL;
  set_to_internal BOOLEAN;
  wip_code_error  EXCEPTION;
  bad_date        EXCEPTION;
  l_pick_to_container_id PICK_DIRECTIVE.pick_to_container_id%TYPE;
  l_continue VARCHAR(1) := 'Y';
  l_mld_bld_meth_overide scp.scp_current_val%TYPE;
  l_count     NUMBER;
  lv_putaway  VARCHAR2(1) := 'N';
  value_found BOOLEAN;
  t_mark_status CONTAINER_ATTRIBUTE.Mark_Status%TYPE;
  t_ticket_request_status CONTAINER_ATTRIBUTE.Ticket_Request_Status%TYPE;
  --
  t_item_id container_item.item_id%TYPE;
  l_from_strg_location_type Loc_Type.Storage_Location_Flag%TYPE;
  l_to_strg_location_type Loc_Type.Storage_Location_Flag%TYPE;
  l_from_stgg_location_type Loc_Type.Staging_Location_Flag%TYPE;
  l_to_stgg_location_type Loc_Type.Staging_Location_Flag%TYPE;
  get_location_type_dtl_found BOOLEAN;
  l_mlp_wave_nbr Container.wave_nbr%TYPE := NULL;
  l_lpn_wave_nbr Container.wave_nbr%TYPE := NULL;
  l_check_wave_numbers     NUMBER;
  check_wave_numbers_found BOOLEAN;
  l_unit_qty               NUMBER;
  l_wave_nbr CONTAINER.WAVE_NBR%TYPE;
  l_ups UNIT_PICK_ZONE.UNIT_PICK_SYSTEM_CODE%TYPE;
  l_item_id CONTAINER_ITEM.ITEM_ID%TYPE;
  l_compliance_type     VARCHAR2(1);
  l_supplement          VARCHAR2(1);
  l_item1_is_hazmat     BOOLEAN := FALSE;
  l_item2_is_hazmat     BOOLEAN := FALSE;
  l_hazmat              VARCHAR2(10);
  l_item1_is_food       BOOLEAN := FALSE;
  l_item2_is_food       BOOLEAN := FALSE;
  e_cant_mix_item       EXCEPTION;
  l_item1_is_supplement BOOLEAN := FALSE;
  l_item2_is_supplement BOOLEAN := FALSE;
  l_item1_can_be_mixed  BOOLEAN := TRUE;
  l_item2_can_be_mixed  BOOLEAN := TRUE;
  l_item2_storage_cat PUTAWAY_REQ.spec_storage_name%TYPE;
  l_item1_storage_cat PUTAWAY_REQ.spec_storage_name%TYPE;
  l_mixed_pallet_flag PUTAWAY_REQ.mixed_pallet_flag%TYPE;
  l_container CONTAINER_ITEM.container_id%TYPE;
  l_min_pallet_flag PUTAWAY_REQ.MIXED_PALLET_FLAG%TYPE;
  l_comp_found            BOOLEAN;
  l_supp_found            BOOLEAN;
  e_break_mlp             EXCEPTION;
  e_cant_mix_item_cust     EXCEPTION;
  l_get_task_queue_info   VARCHAR2(1);
  l_task_queue_found      BOOLEAN;
  task_queue_exist        EXCEPTION;
  FRM_TRIGGER_FAILURE     EXCEPTION;
  CHECK_POPUP             VARCHAR2(50);
  PREV_SINGLE_SKU_ENTERED BOOLEAN:=FALSE;
  PROCEDURE add_labeled_container1(
      V_RETURN               IN OUT VARCHAR2,
      I_MSG_DISPLAY          IN OUT VARCHAR2,
      I_FACILITY_ID          IN VARCHAR2,
      I_CONTAINER_ID         IN OUT VARCHAR2,
      I_GENERIC_CONTAINER_ID IN VARCHAR2,
      I_SINGLE_SKU16_FLAG    IN VARCHAR2,
      I_ITEM_ID              IN VARCHAR2,
      I_O_POPUP              IN OUT VARCHAR2);
  PROCEDURE add_labeled_container2(
      USER_CHOICE            IN VARCHAR2,
      V_RETURN               IN OUT VARCHAR2,
      I_MSG_DISPLAY          IN OUT VARCHAR2,
      I_FACILITY_ID          IN VARCHAR2,
      I_CONTAINER_ID         IN OUT VARCHAR2,
      I_GENERIC_CONTAINER_ID IN VARCHAR2,
      I_SINGLE_SKU16_FLAG    IN OUT VARCHAR2,
      I_ITEM_ID              IN VARCHAR2,
      I_SINGLE_SKU12_FLAG    IN OUT VARCHAR2,
      I_STORAGE_CAT          IN VARCHAR,
      I_O_POPUP              IN OUT VARCHAR2);
  PROCEDURE add_labeled_container3(
      USER_CHOICE            IN VARCHAR2,
      V_RETURN               IN OUT VARCHAR2,
      I_MSG_DISPLAY          IN OUT VARCHAR2,
      I_FACILITY_ID          IN VARCHAR2,
      I_CONTAINER_ID         IN OUT VARCHAR2,
      I_GENERIC_CONTAINER_ID IN VARCHAR2,
      I_SINGLE_SKU16_FLAG    IN OUT VARCHAR2,
      I_ITEM_ID              IN VARCHAR2,
      I_SINGLE_SKU12_FLAG    IN OUT VARCHAR2,
      I_STORAGE_CAT          IN VARCHAR,
      I_O_POPUP              IN OUT VARCHAR2);
  PROCEDURE add_labeled_container4(
      USER_CHOICE            IN VARCHAR2,
      V_RETURN               IN OUT VARCHAR2,
      I_MSG_DISPLAY          IN OUT VARCHAR2,
      I_FACILITY_ID          IN VARCHAR2,
      I_CONTAINER_ID         IN OUT VARCHAR2,
      I_GENERIC_CONTAINER_ID IN VARCHAR2,
      I_CONT_DEST_ID         IN OUT NUMBER,
      I_CONT_STATUS          IN OUT VARCHAR2,
      G_MLD_ENABLED          IN VARCHAR2,
      I_BUILD_METHOD         IN VARCHAR2,
      I_LOCATION_ID          IN VARCHAR2,
      I_NEW_CONTAINER_FLAG   IN VARCHAR2,
      I_CONSOLIDATE_PEND_WIP IN VARCHAR2,
      P_GO_ITEM              IN OUT VARCHAR2,
      I_O_POPUP              IN OUT VARCHAR2);
  PROCEDURE add_labeled_container5(
      V_RETURN       IN OUT VARCHAR2,
      I_CONTAINER_ID IN VARCHAR2,
      I_FACILITY_ID  IN VARCHAR2,
      USER_CHOICE    IN VARCHAR2,
      I_CONT_DEST_ID IN OUT NUMBER,
      I_CONT_STATUS  IN OUT VARCHAR2 );
  PROCEDURE add_labeled_container6(
      V_RETURN               IN OUT VARCHAR2,
      I_MSG_DISPLAY          IN OUT VARCHAR2,
      I_FACILITY_ID          IN VARCHAR2,
      G_FACILITY_TYPE        IN VARCHAR2,
      I_CONTAINER_ID         IN OUT VARCHAR2,
      I_GENERIC_CONTAINER_ID IN VARCHAR2,
      I_CONT_DEST_ID         IN OUT NUMBER,
      I_NEW_CONTAINER_FLAG   IN OUT VARCHAR2,
      G_MLD_ENABLED          IN VARCHAR2,
      I_BUILD_METHOD         IN VARCHAR2,
      I_USER                 IN VARCHAR2,
      I_LOCATION_ID          IN VARCHAR2,
      I_O_POPUP              IN OUT VARCHAR2) ;
  PROCEDURE add_labeled_container7(
      USER_CHOICE            IN VARCHAR2,
      V_RETURN               IN OUT VARCHAR2,
      I_MSG_DISPLAY          IN OUT VARCHAR2,
      I_FACILITY_ID          IN VARCHAR2,
      I_CONTAINER_ID         IN OUT VARCHAR2,
      I_GENERIC_CONTAINER_ID IN VARCHAR2,
      I_ITEM_ID              IN OUT VARCHAR2,
      I_STORAGE_CAT          IN OUT VARCHAR,
      I_CONT_DEST_ID         IN NUMBER,
      I_CONT_STATUS          IN VARCHAR2,
      G_MLD_ENABLED          IN VARCHAR2,
      I_USER                 IN VARCHAR2,
      I_UPS_CODE             IN OUT VARCHAR2,
      I_LPN_COUNT            IN OUT NUMBER );
  PROCEDURE RESET_GLOBAL_PARAMS;
  FUNCTION get_equipment_type_desc_wp(
      L_OUT_EXISTS         IN OUT NUMBER ,
      L_OUT_ERROR_MESSAGE  IN OUT VARCHAR2 ,
      L_OUT_EQUIPMENT_DESC IN OUT VARCHAR2 ,
      ti_equipment_type    IN OUT NUMBER ,
      facilityId           IN VARCHAR2)
    RETURN INTEGER;
  PROCEDURE p_get_order_by(
      p_facility_id IN VARCHAR2 );
  PROCEDURE p_error_log(
      I_FACILITY_ID IN VARCHAR2,
      I_USER        IN VARCHAR2,
      L_LOCATION    IN VARCHAR2 );
  FUNCTION assign_task_to_user(
      L_OUT_ERROR_MESSAGE IN OUT VARCHAR2 ,
      facilityId          IN VARCHAR2 ,
      taskId              IN NUMBER ,
      userId              IN VARCHAR2)
    RETURN INTEGER;
END PKG_INDEPENDENT_ADF;
/


CREATE OR REPLACE PACKAGE BODY PKG_INDEPENDENT_ADF
AS
PROCEDURE add_labeled_container1(
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_FACILITY_ID          IN VARCHAR2,
    I_CONTAINER_ID         IN OUT VARCHAR2,
    I_GENERIC_CONTAINER_ID IN VARCHAR2,
    I_SINGLE_SKU16_FLAG    IN VARCHAR2,
    I_ITEM_ID              IN VARCHAR2,
    I_O_POPUP              IN OUT VARCHAR2)
IS
  CURSOR c_see_if_cont_qty_gr_1
  IS
    SELECT Container_Qty
    FROM Container_Item
    WHERE Facility_ID = I_FACILITY_ID
    AND Container_ID  = l_container_id;
  CURSOR add_cont_cursor
  IS
    SELECT t1.container_status,
      t1.dest_id,
      t1.expedite_flag,
      t1.carrier_code,
      t1.service_code,
      t1.route,
      t1.bol_nbr,
      t1.master_container_id,
      t1.location_id,
      t1.putaway_date,
      t2.mark_status,
      t2.ticket_request_status,
      t1.po_nbr
    FROM container_attribute t2,
      container t1
    WHERE t1.facility_ID = I_FACILITY_ID
    AND t1.container_ID  = I_CONTAINER_ID
    AND t1.facility_id   = t2.facility_id(+)
    AND t1.container_id  = t2.container_id(+);
  CURSOR c_get_task_queue_info
  IS
    SELECT 'x'
    FROM task_queue
    WHERE facility_id= I_FACILITY_ID
    AND container_id = f_master_container_id
    AND rownum       < 2;
  CURSOR get_cont_item (container_id_in container.container_id%TYPE)
  IS
    SELECT item_id
    FROM container_item
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = container_id_in;
BEGIN
  l_mixed_dest_id        := TO_NUMBER(g_scp(I_FACILITY_ID,'mixed_dest_id'));
  l_mld_bld_meth_overide := g_scp(I_FACILITY_ID, 'mld_bld_meth_overide');
  IF I_CONTAINER_ID       = I_GENERIC_CONTAINER_ID THEN
    I_MSG_DISPLAY        := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  IF has_children(I_FACILITY_ID, I_CONTAINER_ID) = 'Y' THEN
    I_MSG_DISPLAY                               := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MASTER_CONT','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  l_container_id := I_CONTAINER_ID;
  OPEN c_see_if_cont_qty_gr_1;
  FETCH c_see_if_cont_qty_gr_1 INTO f_container_qty;
  CLOSE c_see_if_cont_qty_gr_1;
  IF (f_container_qty > 1) THEN
    I_MSG_DISPLAY    := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MASTER_CONT','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  OPEN add_cont_cursor;
  FETCH add_cont_cursor
  INTO f_container_status,
    f_dest_id,
    f_expedite_flag,
    f_carrier_code,
    f_service_code,
    f_route,
    f_bol_nbr,
    f_master_container_id,
    f_location_id,
    f_putaway_date,
    t_mark_status,
    t_ticket_request_status,
    f_po_nbr;
  value_found := add_cont_cursor%NOTFOUND;
  CLOSE add_cont_cursor;
  IF value_found THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  IF f_container_status IN ('T', 'R', 'P', 'M', 'S') THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONT_STAT','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  BEGIN
    IF PKG_HOTEL_INV_ADF.F_GET_MARK_STATUS(V_RETURN ,I_MSG_DISPLAY, I_FACILITY_ID ,I_CONTAINER_ID) = 'Y' THEN
      I_CONTAINER_ID                                                                              := NULL;
      I_MSG_DISPLAY                                                                               := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MARK_MISMATCH', 'E');
      RAISE FRM_TRIGGER_FAILURE;
      RETURN;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    IF V_RETURN IS NOT NULL THEN
      RAISE FRM_TRIGGER_FAILURE;
      RETURN;
    END IF;
  END;
  OPEN c_get_task_queue_info;
  FETCH c_get_task_queue_info INTO l_get_task_queue_info;
  l_task_queue_found := c_get_task_queue_info%FOUND;
  CLOSE c_get_task_queue_info;
  IF l_task_queue_found THEN
    RAISE task_queue_exist;
  END IF;
  IF I_SINGLE_SKU16_FLAG     = 'Y' THEN
    PREV_SINGLE_SKU_ENTERED := TRUE;
    IF I_ITEM_ID            IS NOT NULL THEN
      OPEN get_cont_item(I_container_id);
      FETCH get_cont_item INTO t_item_id;
      CLOSE get_cont_item;
      IF I_ITEM_ID               <> t_item_id THEN
        IF SUBSTR(I_ITEM_ID,1,12) = SUBSTR(t_item_id,1,12) THEN
          I_MSG_DISPLAY          := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CONV_SING_SKU12', 'C');
          I_O_POPUP              := 'CONV_SING_SKU12_POPUP1';
        ELSE
          I_MSG_DISPLAY  := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MLP_LPN_ITEM', 'E');
          I_CONTAINER_ID :=NULL;
          RAISE FRM_TRIGGER_FAILURE;
        END IF;
      END IF;
    END IF;
  END IF;
EXCEPTION
WHEN TASK_QUEUE_EXIST THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'TQ_EXIST','E');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN FRM_TRIGGER_FAILURE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container1;
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
PROCEDURE add_labeled_container2(
    USER_CHOICE            IN VARCHAR2,
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_FACILITY_ID          IN VARCHAR2,
    I_CONTAINER_ID         IN OUT VARCHAR2,
    I_GENERIC_CONTAINER_ID IN VARCHAR2,
    I_SINGLE_SKU16_FLAG    IN OUT VARCHAR2,
    I_ITEM_ID              IN VARCHAR2,
    I_SINGLE_SKU12_FLAG    IN OUT VARCHAR2,
    I_STORAGE_CAT          IN VARCHAR,
    I_O_POPUP              IN OUT VARCHAR2)
IS
  CURSOR get_cont_item (container_id_in container.container_id%TYPE)
  IS
    SELECT item_id
    FROM container_item
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = container_id_in;
  CURSOR c_get_child
  IS
    SELECT container_id
    FROM container
    WHERE facility_id       = I_FACILITY_ID
    AND master_container_id = I_GENERIC_CONTAINER_ID;
  CURSOR c_get_compliance_type(container_id_in container.container_id%TYPE)
  IS
    SELECT 'x'
    FROM class cl,
      item_master im,
      container c,
      container_item ci
    WHERE c.facility_id    = I_FACILITY_ID
    AND c.container_id     = container_id_in
    AND c.facility_id      = ci.facility_id
    AND c.container_id     = ci.container_id
    AND ci.facility_id     = im.facility_id
    AND ci.item_id         = im.item_id
    AND im.facility_id     = c.facility_id
    AND im.class           = cl.class
    AND im.department      = cl.department
    AND cl.compliance_type = 'FOOD';
  CURSOR c_get_supplements (container_id_in container.container_id%TYPE)
  IS
    SELECT 'x'
    FROM class cl,
      item_master im,
      container c,
      container_item ci,
      cms_uda_item_ff cf
    WHERE c.facility_id              = I_FACILITY_ID
    AND c.container_id               = container_id_in
    AND c.facility_id                = ci.facility_id
    AND c.container_id               = ci.container_id
    AND ci.facility_id               = im.facility_id
    AND ci.item_id                   = im.item_id
    AND im.facility_id               = cl.facility_id
    AND im.class                     = cl.class
    AND im.department                = cl.department
    AND NVL(cl.compliance_type,'N') <> 'FOOD'
    AND SUBSTR(im.item_id,1,12)      = SUBSTR(cf.item_id,1,12)
    AND cf.uda_id                    = 2550
    AND cf.uda_text                 IS NOT NULL;
BEGIN
  IF USER_CHOICE         = 'Y' THEN
    I_SINGLE_SKU12_FLAG := 'X';
    I_SINGLE_SKU16_FLAG := 'N';
  ELSIF USER_CHOICE      = 'N' THEN
    I_SINGLE_SKU16_FLAG := 'Y';
    I_SINGLE_SKU12_FLAG := 'N';
    I_CONTAINER_ID      := NULL;
    I_MSG_DISPLAY       := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MLP_LPN_ITEM', 'E');
    RAISE FRM_TRIGGER_FAILURE;
  END IF;
  IF(PREV_SINGLE_SKU_ENTERED   = FALSE) THEN
    IF I_SINGLE_SKU12_FLAG     = 'Y' THEN
      PREV_SINGLE_SKU_ENTERED := TRUE;
      IF I_ITEM_ID            IS NOT NULL THEN
        OPEN c_get_child;
        FETCH c_get_child INTO l_container;
        CLOSE c_get_child;
        OPEN c_get_compliance_type(l_container);
        FETCH c_get_compliance_type INTO l_compliance_type;
        l_comp_found := c_get_compliance_type%FOUND;
        CLOSE c_get_compliance_type;
        IF l_comp_found THEN
          l_item1_is_food := TRUE;
        END IF;
        OPEN get_cont_item(l_container);
        FETCH get_cont_item INTO t_item_id;
        CLOSE get_cont_item;
        l_hazmat            := fn_chk_item_is_hazmat (I_FACILITY_ID, t_item_id, NULL,NULL);
        IF l_hazmat         <> 'NON_HAZMAT' THEN
          l_item1_is_hazmat := TRUE;
        END IF;
        l_mixed_pallet_flag    := get_putaway_req.fn_get_mixed_pallet_flag (I_FACILITY_ID, t_item_id);
        IF l_mixed_pallet_flag  = 'N' THEN
          l_item1_can_be_mixed := FALSE;
        END IF;
        OPEN c_get_supplements(l_container);
        FETCH c_get_supplements INTO l_supplement;
        l_supp_found := c_get_supplements%FOUND;
        CLOSE c_get_supplements;
        IF l_supp_found THEN
          l_item1_is_supplement := TRUE;
        END IF;
        l_item1_storage_cat := I_STORAGE_CAT;
        OPEN c_get_compliance_type(I_CONTAINER_ID);
        FETCH c_get_compliance_type INTO l_compliance_type;
        l_comp_found := c_get_compliance_type%FOUND;
        CLOSE c_get_compliance_type;
        IF l_comp_found THEN
          l_item2_is_food := TRUE;
        END IF;
        OPEN c_get_supplements(I_CONTAINER_ID);
        FETCH c_get_supplements INTO l_supplement;
        l_supp_found := c_get_supplements%FOUND;
        CLOSE c_get_supplements;
        IF l_supp_found THEN
          l_item2_is_supplement := TRUE;
        END IF;
        OPEN get_cont_item(I_CONTAINER_ID);
        FETCH get_cont_item INTO t_item_id;
        CLOSE get_cont_item;
        l_hazmat            := fn_chk_item_is_hazmat (I_FACILITY_ID, t_item_id, NULL,NULL);
        IF l_hazmat         <> 'NON_HAZMAT' THEN
          l_item2_is_hazmat := TRUE;
        END IF;
        l_mixed_pallet_flag    := get_putaway_req.fn_get_mixed_pallet_flag (I_FACILITY_ID, t_item_id);
        l_item2_storage_cat    := get_putaway_req.fn_get_item_spec_storage_name(I_FACILITY_ID, t_item_id);
        IF l_mixed_pallet_flag  = 'N' THEN
          l_item2_can_be_mixed := FALSE;
        ELSE
          l_item2_can_be_mixed := TRUE;
        END IF;
        IF SUBSTR(I_ITEM_ID,1,12) <> SUBSTR(t_item_id,1,12) THEN
          IF (l_item1_is_hazmat    = TRUE AND l_item2_is_food = TRUE) OR (l_item2_is_hazmat = TRUE AND l_item1_is_food = TRUE) THEN
            raise e_cant_mix_item;
          END IF;
          IF (l_item1_is_supplement = TRUE AND l_item1_is_hazmat = FALSE AND l_item2_is_hazmat = TRUE) OR (l_item2_is_supplement = TRUE AND l_item2_is_hazmat = FALSE AND l_item1_is_hazmat = TRUE)THEN
            raise e_cant_mix_item;
          END IF;
          IF (l_item1_can_be_mixed = FALSE OR l_item2_can_be_mixed = FALSE) AND (NVL(l_item1_storage_cat,'~') <> NVL(l_item2_storage_cat,'~')) THEN
            raise e_cant_mix_item;
          END IF;
          I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CONV_MULT_SKU12', 'C');
          I_O_POPUP     :='CONV_SING_SKU12_POPUP2';
        END IF;
      END IF;
    END IF;
  END IF;
EXCEPTION
WHEN e_cant_mix_item THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MLP_LPN_ITEM','E');
  RESET_GLOBAL_PARAMS;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container2;
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
PROCEDURE add_labeled_container3(
    USER_CHOICE            IN VARCHAR2,
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_FACILITY_ID          IN VARCHAR2,
    I_CONTAINER_ID         IN OUT VARCHAR2,
    I_GENERIC_CONTAINER_ID IN VARCHAR2,
    I_SINGLE_SKU16_FLAG    IN OUT VARCHAR2,
    I_ITEM_ID              IN VARCHAR2,
    I_SINGLE_SKU12_FLAG    IN OUT VARCHAR2,
    I_STORAGE_CAT          IN VARCHAR,
    I_O_POPUP              IN OUT VARCHAR2)
IS
  CURSOR get_cont_item (container_id_in container.container_id%TYPE)
  IS
    SELECT item_id
    FROM container_item
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = container_id_in;
  CURSOR c_check_mlp_has_hazmat
  IS
    SELECT NVL(MIN(get_putaway_req.fn_get_mixed_pallet_flag (I_FACILITY_ID, ci.item_id)), 'X') mixed_pallet_flag
    FROM container_item ci,
      container c
    WHERE ci.facility_id      = I_FACILITY_ID
    AND ci.facility_id        = c.facility_id
    AND c.master_container_id = I_GENERIC_CONTAINER_ID
    AND c.container_id        = ci.container_id;
  CURSOR c_get_compliance_type(container_id_in container.container_id%TYPE)
  IS
    SELECT 'x'
    FROM class cl,
      item_master im,
      container c,
      container_item ci
    WHERE c.facility_id    = I_FACILITY_ID
    AND c.container_id     = container_id_in
    AND c.facility_id      = ci.facility_id
    AND c.container_id     = ci.container_id
    AND ci.facility_id     = im.facility_id
    AND ci.item_id         = im.item_id
    AND im.facility_id     = c.facility_id
    AND im.class           = cl.class
    AND im.department      = cl.department
    AND cl.compliance_type = 'FOOD';
  CURSOR c_get_child
  IS
    SELECT container_id
    FROM container
    WHERE facility_id       = I_FACILITY_ID
    AND master_container_id = I_GENERIC_CONTAINER_ID;
  CURSOR c_get_supplements (container_id_in container.container_id%TYPE)
  IS
    SELECT 'x'
    FROM class cl,
      item_master im,
      container c,
      container_item ci,
      cms_uda_item_ff cf
    WHERE c.facility_id              = I_FACILITY_ID
    AND c.container_id               = container_id_in
    AND c.facility_id                = ci.facility_id
    AND c.container_id               = ci.container_id
    AND ci.facility_id               = im.facility_id
    AND ci.item_id                   = im.item_id
    AND im.facility_id               = cl.facility_id
    AND im.class                     = cl.class
    AND im.department                = cl.department
    AND NVL(cl.compliance_type,'N') <> 'FOOD'
    AND SUBSTR(im.item_id,1,12)      = SUBSTR(cf.item_id,1,12)
    AND cf.uda_id                    = 2550
    AND cf.uda_text                 IS NOT NULL;
BEGIN
  IF USER_CHOICE         = 'Y' THEN
    I_SINGLE_SKU12_FLAG := 'N';
    I_SINGLE_SKU16_FLAG := 'N';
  ELSIF USER_CHOICE      = 'N' THEN
    I_SINGLE_SKU16_FLAG := 'N';
    I_SINGLE_SKU12_FLAG := 'Y';
    I_CONTAINER_ID      := NULL;
    I_MSG_DISPLAY       := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MLP_LPN_ITEM', 'E');
    RAISE FRM_TRIGGER_FAILURE;
  END IF;
  IF(PREV_SINGLE_SKU_ENTERED   = FALSE) THEN
    IF I_SINGLE_SKU12_FLAG     = 'X' THEN
      PREV_SINGLE_SKU_ENTERED := TRUE;
      IF I_ITEM_ID            IS NOT NULL THEN
        OPEN get_cont_item(I_CONTAINER_ID);
        FETCH get_cont_item INTO t_item_id;
        CLOSE get_cont_item;
        IF SUBSTR(I_ITEM_ID,1,12) <> SUBSTR(t_item_id,1,12) THEN
          I_SINGLE_SKU16_FLAG     := 'N';
          I_SINGLE_SKU12_FLAG     := 'X';
          I_CONTAINER_ID          := NULL;
          I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MLP_LPN_ITEM', 'E');
          RAISE FRM_TRIGGER_FAILURE;
        END IF;
      END IF;
    ELSE
      IF(PREV_SINGLE_SKU_ENTERED = FALSE) THEN
        PREV_SINGLE_SKU_ENTERED := TRUE;
        IF I_ITEM_ID            IS NOT NULL THEN
          OPEN c_check_mlp_has_hazmat;
          FETCH c_check_mlp_has_hazmat INTO l_min_pallet_flag;
          CLOSE c_check_mlp_has_hazmat;
          IF l_min_pallet_flag = 'N' AND I_STORAGE_CAT = g_scp(I_FACILITY_ID, 'mixed_SKU') THEN
            raise e_break_mlp;
          END IF;
          FOR i IN c_get_child
          LOOP
            OPEN c_get_compliance_type(i.container_id);
            FETCH c_get_compliance_type INTO l_compliance_type;
            l_comp_found := c_get_compliance_type%FOUND;
            CLOSE c_get_compliance_type;
            IF l_comp_found THEN
              l_item1_is_food := TRUE;
              EXIT;
            END IF;
          END LOOP;
          FOR i IN c_get_child
          LOOP
            OPEN get_cont_item(i.container_id);
            FETCH get_cont_item INTO t_item_id;
            CLOSE get_cont_item;
            l_hazmat            := fn_chk_item_is_hazmat (I_FACILITY_ID, t_item_id, NULL,NULL);
            IF l_hazmat         <> 'NON_HAZMAT' THEN
              l_item1_is_hazmat := TRUE;
              EXIT;
            END IF;
          END LOOP;
          FOR i IN c_get_child
          LOOP
            OPEN get_cont_item(i.container_id);
            FETCH get_cont_item INTO t_item_id;
            CLOSE get_cont_item;
            l_mixed_pallet_flag    := get_putaway_req.fn_get_mixed_pallet_flag (I_FACILITY_ID, t_item_id);
            IF l_mixed_pallet_flag  = 'N' THEN
              l_item1_can_be_mixed := FALSE;
              EXIT;
            END IF;
          END LOOP;
          FOR i IN c_get_child
          LOOP
            OPEN c_get_supplements(i.container_id);
            FETCH c_get_supplements INTO l_supplement;
            l_supp_found := c_get_supplements%FOUND;
            CLOSE c_get_supplements;
            IF l_supp_found THEN
              l_item1_is_supplement := TRUE;
              EXIT;
            END IF;
          END LOOP;
          l_item1_storage_cat := I_STORAGE_CAT;
        END IF;
      END IF;
      OPEN c_get_compliance_type(I_CONTAINER_ID);
      FETCH c_get_compliance_type INTO l_compliance_type;
      l_comp_found := c_get_compliance_type%FOUND;
      CLOSE c_get_compliance_type;
      IF l_comp_found THEN
        l_item2_is_food := TRUE;
      END IF;
      OPEN c_get_supplements(I_CONTAINER_ID);
      FETCH c_get_supplements INTO l_supplement;
      l_supp_found := c_get_supplements%FOUND;
      CLOSE c_get_supplements;
      IF l_supp_found THEN
        l_item2_is_supplement := TRUE;
      END IF;
      OPEN get_cont_item(I_CONTAINER_ID);
      FETCH get_cont_item INTO t_item_id;
      CLOSE get_cont_item;
      l_hazmat            := fn_chk_item_is_hazmat (I_FACILITY_ID, t_item_id, NULL,NULL);
      IF l_hazmat         <> 'NON_HAZMAT' THEN
        l_item2_is_hazmat := TRUE;
      END IF;
      l_mixed_pallet_flag    := get_putaway_req.fn_get_mixed_pallet_flag (I_FACILITY_ID, t_item_id);
      l_item2_storage_cat    := get_putaway_req.fn_get_item_spec_storage_name(I_FACILITY_ID, t_item_id);
      IF l_mixed_pallet_flag  = 'N' THEN
        l_item2_can_be_mixed := FALSE;
      ELSE
        l_item2_can_be_mixed := TRUE;
      END IF;
      IF (l_item1_is_hazmat = TRUE AND l_item2_is_food = TRUE) OR (l_item2_is_hazmat = TRUE AND l_item1_is_food = TRUE) THEN
        raise e_cant_mix_item;
      END IF;
      IF (l_item1_is_supplement = TRUE AND l_item1_is_hazmat = FALSE AND l_item2_is_hazmat = TRUE) OR (l_item2_is_supplement = TRUE AND l_item2_is_hazmat = FALSE AND l_item1_is_hazmat = TRUE)THEN
        raise e_cant_mix_item;
      END IF;
      IF (l_item1_can_be_mixed = FALSE OR l_item2_can_be_mixed = FALSE) AND NVL(l_item1_storage_cat,'~') <> NVL(l_item2_storage_cat,'~') THEN
        raise e_cant_mix_item;
      END IF;
    END IF;
  END IF;
  IF f_dest_id    IS NULL THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'ASSIGN_DC_DEST', 'C');
    I_O_POPUP     :='ASSIGN_DC_DEST_POPUP';
  END IF;
EXCEPTION
WHEN e_break_mlp THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SPLIT_MLP');
  RESET_GLOBAL_PARAMS;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RETURN;
WHEN e_cant_mix_item THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MLP_LPN_ITEM','E');
  RESET_GLOBAL_PARAMS;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RETURN;
WHEN FRM_TRIGGER_FAILURE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  RESET_GLOBAL_PARAMS;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container3;
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
PROCEDURE add_labeled_container4(
    USER_CHOICE            IN VARCHAR2,
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_FACILITY_ID          IN VARCHAR2,
    I_CONTAINER_ID         IN OUT VARCHAR2,
    I_GENERIC_CONTAINER_ID IN VARCHAR2,
    I_CONT_DEST_ID         IN OUT NUMBER,
    I_CONT_STATUS          IN OUT VARCHAR2,
    G_MLD_ENABLED          IN VARCHAR2,
    I_BUILD_METHOD         IN VARCHAR2,
    I_LOCATION_ID          IN VARCHAR2,
    I_NEW_CONTAINER_FLAG   IN VARCHAR2,
    I_CONSOLIDATE_PEND_WIP IN VARCHAR2,
    P_GO_ITEM              IN OUT VARCHAR2,
    I_O_POPUP              IN OUT VARCHAR2)
IS
  CURSOR master_location_cursor
  IS
    SELECT CM.location_id
    FROM Container CM,
      Container CC
    WHERE CC.Facility_ID       = I_FACILITY_ID
    AND CC.Container_ID        = I_CONTAINER_ID
    AND CC.Facility_ID         = CM.Facility_ID
    AND CC.Master_Container_ID = CM.Container_ID;
  CURSOR storage_container_cursor(location_id_in CONTAINER.Location_ID%TYPE)
  IS
    SELECT 'Y'
    FROM Loc_Type LT,
      Location L
    WHERE L.Facility_ID          = I_FACILITY_ID
    AND L.Location_ID            = location_id_in
    AND L.Facility_ID            = LT.Facility_ID
    AND L.Location_Type          = LT.Location_Type
    AND LT.Storage_Location_Flag = 'Y'
    AND rownum                   < 2;
  CURSOR c_chk_if_distrib
  IS
    SELECT pick_to_container_id
    FROM Pick_Directive
    WHERE Facility_ID           = I_FACILITY_ID
    AND (Pick_From_Container_ID = I_CONTAINER_ID
    OR Pick_From_Container_ID   = I_GENERIC_CONTAINER_ID
    OR Pick_From_Container_ID   = f_master_container_id);
  CURSOR get_location_type_dtl (location_id_in CONTAINER.Location_ID%TYPE)
  IS
    SELECT LT.storage_location_flag,
      LT.staging_location_flag
    FROM Loc_Type LT,
      Location L
    WHERE L.Facility_ID = I_FACILITY_ID
    AND L.Location_ID   = location_id_in
    AND L.Facility_ID   = LT.Facility_ID
    AND L.Location_Type = LT.Location_Type;
  CURSOR c_see_if_cont_qty_gr_1
  IS
    SELECT Container_Qty
    FROM Container_Item
    WHERE Facility_ID = I_FACILITY_ID
    AND Container_ID  = l_container_id;
  CURSOR c_get_csr_bol
  IS
    SELECT expedite_flag,
      carrier_code,
      service_code,
      route,
      bol_nbr
    FROM Container
    WHERE Facility_ID = I_FACILITY_ID
    AND Container_ID  = I_GENERIC_CONTAINER_ID;
BEGIN
  IF f_dest_id    IS NULL THEN
    IF USER_CHOICE = 'Y' THEN
      f_dest_id   := TO_NUMBER(g_scp(I_FACILITY_ID, 'DC_dest_ID'));
      UPDATE Container
      SET Dest_ID       = f_dest_id
      WHERE Facility_ID = I_FACILITY_ID
      AND Container_ID  = I_CONTAINER_ID;
    ELSIF USER_CHOICE   = 'N' THEN
      I_MSG_DISPLAY    := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_DEST_ID', 'W');
      RAISE FRM_TRIGGER_FAILURE;
      RETURN;
    END IF;
  END IF;
  IF f_master_container_id <> 'NONE' THEN
    OPEN master_location_cursor;
    FETCH master_location_cursor INTO f_location_id;
    CLOSE master_location_cursor;
  END IF;
  OPEN storage_container_cursor(f_location_id);
  FETCH storage_container_cursor INTO l_dummy;
  IF storage_container_cursor%FOUND THEN
    CLOSE storage_container_cursor;
    OPEN c_chk_if_distrib;
    FETCH c_chk_if_distrib INTO l_pick_to_container_id;
    IF c_chk_if_distrib%FOUND THEN
      CLOSE c_chk_if_distrib;
      I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_FROM_LOC','E');
      RAISE FRM_TRIGGER_FAILURE;
      RETURN;
    ELSE
      CLOSE c_chk_if_distrib;
    END IF;
  ELSE
    CLOSE storage_container_cursor;
  END IF;
  OPEN get_location_type_dtl (f_location_id);
  FETCH get_location_type_dtl
  INTO l_from_strg_location_type,
    l_from_stgg_location_type;
  get_location_type_dtl_found := get_location_type_dtl%FOUND;
  CLOSE get_location_type_dtl;
  IF f_location_id <> I_LOCATION_ID THEN
    OPEN get_location_type_dtl (I_LOCATION_ID);
    FETCH get_location_type_dtl
    INTO l_to_strg_location_type,
      l_to_stgg_location_type;
    get_location_type_dtl_found := get_location_type_dtl%FOUND;
    CLOSE get_location_type_dtl;
    IF get_location_type_dtl_found THEN
      IF (l_from_strg_location_type = 'N' AND l_from_stgg_location_type = 'N') THEN
        I_MSG_DISPLAY              := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_FROM_LOC','E');
        RAISE FRM_TRIGGER_FAILURE;
        RETURN;
      ELSIF l_from_strg_location_type <> l_to_strg_location_type THEN
        I_MSG_DISPLAY                 := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_ROS_LOCTYPE','E');
        RAISE FRM_TRIGGER_FAILURE;
        RETURN;
      END IF;
    END IF;
  END IF;
  IF (PKG_HOTEL_INV_ADF.V_LOCATION(V_RETURN, I_MSG_DISPLAY, I_FACILITY_ID ,f_location_id) = 'N') THEN
    I_CONTAINER_ID                                                                       := NULL;
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  IF I_NEW_CONTAINER_FLAG = 'Y' THEN
    l_new_container      := 'Y';
  ELSE
    l_new_container := 'N';
  END IF;
  IF l_new_container         = 'N' THEN
    IF f_master_container_id = I_GENERIC_CONTAINER_ID THEN
      I_MSG_DISPLAY         := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CHILD_ASSIGNED','E');
      RAISE FRM_TRIGGER_FAILURE;
      RETURN;
    END IF;
    l_container_id := I_GENERIC_CONTAINER_ID;
    OPEN c_see_if_cont_qty_gr_1;
    FETCH c_see_if_cont_qty_gr_1 INTO t_container_qty;
    IF c_see_if_cont_qty_gr_1%FOUND THEN
      CLOSE c_see_if_cont_qty_gr_1;
      P_GO_ITEM     := 'Y';
      I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_BUILD_CONT','E');
      RETURN;
    END IF;
    CLOSE c_see_if_cont_qty_gr_1;
  END IF;
  IF I_CONSOLIDATE_PEND_WIP                                                                       = 'N' THEN
    IF PKG_HOTEL_INV_ADF.CHECK_WIP_CODES2(I_GENERIC_CONTAINER_ID ,I_FACILITY_ID, I_CONTAINER_ID ) = 'N' THEN
      RAISE wip_code_error;
    END IF;
  END IF;
  IF PKG_HOTEL_INV_ADF.CHECK_BEST_BEFORE_DATE2(I_GENERIC_CONTAINER_ID ,I_FACILITY_ID, I_CONTAINER_ID ) = 'N' THEN
    RAISE bad_date;
  END IF;
  IF (l_new_container = 'Y') THEN
    I_CONT_DEST_ID   := f_dest_id;
    I_CONT_STATUS    := f_container_status;
  END IF;
  IF f_container_status = 'A' THEN
    I_MSG_DISPLAY      := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_STYLE_DONE','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  ELSIF (f_container_status = 'D' AND I_CONT_STATUS <> 'D') OR (f_container_status <> 'D' AND I_CONT_STATUS = 'D') THEN
    I_MSG_DISPLAY          := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONT_STAT','E');
    RAISE FRM_TRIGGER_FAILURE;
    RETURN;
  END IF;
  IF l_new_container      = 'N' THEN
    IF f_container_status = 'D' AND I_CONT_STATUS = 'D' THEN
      IF (I_CONT_DEST_ID <> f_dest_id) AND G_MLD_ENABLED = 'N' THEN
        I_MSG_DISPLAY    := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'DESTS_NOT_EQUAL', 'W');
        RAISE FRM_TRIGGER_FAILURE;
        RETURN;
      END IF;
      IF I_BUILD_METHOD = 'INTERNAL' OR I_BUILD_METHOD IS NULL THEN
        OPEN c_get_csr_bol;
        FETCH c_get_csr_bol
        INTO t_expedite_flag,
          t_carrier_code,
          t_service_code,
          t_route,
          t_bol_nbr;
        CLOSE c_get_csr_bol;
        IF (f_expedite_flag <> t_expedite_flag OR f_carrier_code <> t_carrier_code OR f_service_code <> t_service_code OR f_route <> t_route) THEN
          I_MSG_DISPLAY     := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CSR_NOT_EQUAL','W');
          RAISE FRM_TRIGGER_FAILURE;
          RETURN;
        END IF;
      END IF;
      IF (f_bol_nbr     IS NULL AND t_bol_nbr IS NOT NULL) OR (f_bol_nbr IS NOT NULL AND t_bol_nbr IS NULL) THEN
        IF f_bol_nbr    IS NULL THEN
          I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_FROM_BOL', 'C');
          I_O_POPUP     :='NO_FROM_BOL_POPUP';
          CHECK_POPUP   :='NO_FROM_BOL_POPUP';
        ELSIF t_bol_nbr IS NULL THEN
          I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_TO_BOL', 'C');
          I_O_POPUP     :='NO_TO_BOL_POPUP';
          CHECK_POPUP   :='NO_TO_BOL_POPUP';
        END IF;
      END IF;
    ELSE
      IF (I_CONT_DEST_ID <> f_dest_id) AND (I_CONT_DEST_ID <> l_mixed_dest_id) THEN
        I_MSG_DISPLAY    := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MULTI_DEST_ADD', 'C');
        I_O_POPUP        :='MULTI_DEST_ADD_POPUP';
      ELSE
        I_CONT_STATUS := f_container_status;
      END IF;
    END IF;
  END IF;
EXCEPTION
WHEN FRM_TRIGGER_FAILURE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN WIP_CODE_ERROR THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_MIX_WIP', 'E');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN BAD_DATE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  I_MSG_DISPLAY           := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'MIXED_BBD', 'E');
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container4;
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
PROCEDURE add_labeled_container5(
    V_RETURN       IN OUT VARCHAR2,
    I_CONTAINER_ID IN VARCHAR2,
    I_FACILITY_ID  IN VARCHAR2,
    USER_CHOICE    IN VARCHAR2,
    I_CONT_DEST_ID IN OUT NUMBER,
    I_CONT_STATUS  IN OUT VARCHAR2 )
IS
BEGIN
  IF(CHECK_POPUP IN('NO_FROM_BOL_POPUP', 'NO_TO_BOL_POPUP') ) THEN
    l_value   := USER_CHOICE;
    IF l_value = 'Y' THEN
      UPDATE Container
      SET bol_nbr       = f_bol_nbr
      WHERE Facility_ID = I_FACILITY_ID
      AND Container_ID  = I_CONTAINER_ID;
    END IF;
    IF l_value = 'N' THEN
      PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
      RETURN;
    END IF;
  ELSE
    IF(CHECK_POPUP      = 'MULTI_DEST_ADD_POPUP') THEN
      IF USER_CHOICE    = 'Y' THEN
        I_CONT_DEST_ID := l_mixed_dest_id;
        I_CONT_STATUS  := f_container_status;
      ELSE
        RAISE FRM_TRIGGER_FAILURE;
        RETURN;
      END IF;
    END IF;
  END IF;
EXCEPTION
WHEN FRM_TRIGGER_FAILURE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container5;
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
PROCEDURE add_labeled_container6(
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_FACILITY_ID          IN VARCHAR2,
    G_FACILITY_TYPE        IN VARCHAR2,
    I_CONTAINER_ID         IN OUT VARCHAR2,
    I_GENERIC_CONTAINER_ID IN VARCHAR2,
    I_CONT_DEST_ID         IN OUT NUMBER,
    I_NEW_CONTAINER_FLAG   IN OUT VARCHAR2,
    G_MLD_ENABLED          IN VARCHAR2,
    I_BUILD_METHOD         IN VARCHAR2,
    I_USER                 IN VARCHAR2,
    I_LOCATION_ID          IN VARCHAR2,
    I_O_POPUP              IN OUT VARCHAR2)
IS
  CURSOR get_child_dest_id
  IS
    SELECT next_dest_id,
      final_dest_id,
      current_dest_id,
      current_route
    FROM container_route
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = I_CONTAINER_ID;
  CURSOR get_master_dest_id
  IS
    SELECT next_dest_id,
      final_dest_id,
      current_dest_id,
      current_route,
      master_build_method
    FROM container_route
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = I_GENERIC_CONTAINER_ID;
  CURSOR storage_container_cursor(location_id_in CONTAINER.Location_ID%TYPE)
  IS
    SELECT 'Y'
    FROM Loc_Type LT,
      Location L
    WHERE L.Facility_ID          = I_FACILITY_ID
    AND L.Location_ID            = location_id_in
    AND L.Facility_ID            = LT.Facility_ID
    AND L.Location_Type          = LT.Location_Type
    AND LT.Storage_Location_Flag = 'Y'
    AND rownum                   < 2;
BEGIN
  IF G_MLD_ENABLED = 'Y' THEN
    OPEN get_child_dest_id;
    FETCH get_child_dest_id
    INTO c_next_dest_id,
      c_final_dest_id,
      c_current_dest_id,
      c_current_route;
    CLOSE get_child_dest_id;
  END IF;
  IF l_new_container = 'Y' THEN
    DELETE FROM ROSS_EVENTCODE_USERID_TMP;
    COMMIT;
    INSERT
    INTO ROSS_EVENTCODE_USERID_TMP
      (
        EVENT_CD,
        USER_ID,
        LOCATION_ID
      )
      VALUES
      (
        45,
        I_USER,
        I_LOCATION_ID
      );
    INSERT
    INTO Container
      (
        Facility_ID,
        Container_ID,
        Location_ID,
        Master_Container_ID,
        po_nbr,
        User_ID,
        Receipt_Date,
        Container_Status,
        Dest_ID,
        Expedite_Flag,
        Carrier_Code,
        Service_Code,
        Route,
        BOL_Nbr
      )
      VALUES
      (
        I_FACILITY_ID,
        I_GENERIC_CONTAINER_ID,
        I_LOCATION_ID,
        'NONE',
        f_po_nbr,
        I_USER,
        SYSDATE,
        'I',
        f_dest_id,
        f_expedite_flag,
        f_carrier_code,
        f_service_code,
        f_route,
        f_bol_nbr
      );
    INSERT
    INTO Container_Attribute
      (
        FACILITY_ID,
        CONTAINER_ID,
        TICKET_REQUEST_STATUS,
        UPLOADED_FLAG,
        UNIT_ID_COMPLETE,
        MLP_MOVE_COMPLETE,
        ASSIGN_PV
      )
      VALUES
      (
        I_FACILITY_ID,
        I_GENERIC_CONTAINER_ID,
        NULL,
        'C',
        'Y',
        'Y',
        NULL
      );
    I_NEW_CONTAINER_FLAG          := 'N';
    IF G_MLD_ENABLED               = 'Y' THEN
      IF NVL(c_next_dest_id, -99) <> NVL(c_current_dest_id, -99) THEN
        g_scp_mst_bld_cont        := rtrim(g_scp(I_FACILITY_ID, 'mld_bld_container'));
        IF l_mld_bld_meth_overide  = 'Y' OR g_scp_mst_bld_cont = 'PERPALLET' THEN
          t_master_build_method   := I_BUILD_METHOD;
        ELSE
          t_master_build_method := g_scp_mst_bld_cont;
        END IF;
        UPDATE container_route
        SET ORIGINAL_DEST_ID         = c_current_dest_id,
          Next_dest_id               = c_next_dest_id,
          final_dest_id              = c_final_dest_id,
          current_route              = c_current_route,
          prev_route                 = NULL,
          master_build_method        = t_master_build_method
        WHERE facility_id            = I_FACILITY_ID
        AND container_id             = I_GENERIC_CONTAINER_ID;
      ElSIF NVL(c_next_dest_id, -99) = NVL(c_current_dest_id, -99) THEN
        UPDATE container_route
        SET Next_dest_id      = c_current_dest_id,
          final_dest_id       = c_current_dest_id,
          current_route       = NULL,
          master_build_method = 'INTERNAL'
        WHERE facility_id     = I_FACILITY_ID
        AND container_id      = I_GENERIC_CONTAINER_ID;
        set_to_internal      := TRUE;
      END IF;
    END IF;
  ELSIF l_new_container = 'N' THEN
    IF G_MLD_ENABLED    = 'Y' THEN
      OPEN get_master_dest_id;
      FETCH get_master_dest_id
      INTO m_next_dest_id,
        m_final_dest_id,
        m_current_dest_id,
        m_current_route,
        t_master_build_method;
      CLOSE get_master_dest_id;
      IF l_mld_bld_meth_overide = 'Y' THEN
        t_master_build_method  := I_BUILD_METHOD;
      END IF;
      IF t_master_build_method       = 'INTERNAL' THEN
        IF NVL(m_next_dest_id, -99) <> NVL(c_next_dest_id, -99) THEN
          UPDATE container_route
          SET next_dest_id      = l_mixed_dest_id,
            final_dest_id       = l_mixed_dest_id,
            master_build_method = 'INTERNAL'
          WHERE facility_id     = I_FACILITY_ID
          AND container_id      = I_GENERIC_CONTAINER_ID;
          set_to_internal      := TRUE;
        END IF;
      ELSIF t_master_build_method                                                                                   = 'FINAL_DEST' THEN
        IF is_master_build_method_ok(I_FACILITY_ID, I_GENERIC_CONTAINER_ID, t_master_build_method, I_CONTAINER_ID) <> 'Y' THEN
          IF l_mld_bld_meth_overide                                                                                 = 'N' THEN
            I_MSG_DISPLAY                                                                                          := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'BLD_MTH_FIN_E','E');
            RAISE FRM_TRIGGER_FAILURE;
            RETURN;
          ELSE
            I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'BLD_MTH_FIN_C', 'C');
            I_O_POPUP     :='BLD_MTH_FIN_POPUP';
          END IF;
        ELSIF t_master_build_method    = 'ROUTE' THEN
          IF NVL(m_final_dest_id, -99) = NVL(c_final_dest_id, -99) AND NVL(m_current_route, '-99') = NVL(c_current_route, '-99') THEN
            NULL;
          ELSIF NVL(m_current_route, '-99') <> NVL(c_current_route, '-99') THEN
            IF l_mld_bld_meth_overide        = 'N' THEN
              I_MSG_DISPLAY                 := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'BLD_MTH_RTE_E','E');
              RAISE FRM_TRIGGER_FAILURE;
              RETURN;
            ELSE
              I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'BLD_MTH_RTE_C', 'C');
              I_O_POPUP     :='BLD_MTH_RTE_POPUP';
            END IF;
          ELSE
            SELECT dest_id
            INTO I_CONT_DEST_ID
            FROM distribution_route
            WHERE facility_type = G_FACILITY_TYPE
            AND route           = c_current_route
            AND sequence_nbr    =
              (SELECT MAX(sequence_nbr)
              FROM distribution_route
              WHERE facility_type = G_FACILITY_TYPE
              AND route           = c_current_route
              );
            UPDATE CONTAINER_ROUTE
            SET final_dest_id = I_CONT_DEST_ID
            WHERE facility_id = I_FACILITY_ID
            AND container_id  = I_GENERIC_CONTAINER_ID;
          END IF;
        ELSIF t_master_build_method                                                                                   = 'NEXT_DEST' THEN
          IF is_master_build_method_ok(I_FACILITY_ID, I_GENERIC_CONTAINER_ID, t_master_build_method, I_CONTAINER_ID) <> 'Y' THEN
            IF l_mld_bld_meth_overide                                                                                 = 'N' THEN
              I_MSG_DISPLAY                                                                                          := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'BLD_MTH_NXT_E','E');
              RAISE FRM_TRIGGER_FAILURE;
              RETURN;
            ELSE
              I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'BLD_MTH_NXT_C', 'C');
              I_O_POPUP     :='BLD_MTH_NXT_POPUP';
            END IF;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;
  l_check := container_mp_check(I_FACILITY_ID, I_GENERIC_CONTAINER_ID);
  OPEN storage_container_cursor(I_LOCATION_ID);
  FETCH storage_container_cursor INTO lv_putaway;
  CLOSE storage_container_cursor;
  IF lv_putaway = 'Y' THEN
    UPDATE Container
    SET putaway_date  = NVL(f_putaway_date, SYSDATE),
      Po_nbr          = f_po_nbr
    WHERE Facility_ID = I_FACILITY_ID
    AND Container_ID IN (I_CONTAINER_ID, I_GENERIC_CONTAINER_ID);
  ELSE
    UPDATE Container
    SET putaway_date  = NULL,
      Po_nbr          = f_po_nbr
    WHERE Facility_ID = I_FACILITY_ID
    AND Container_ID IN (I_CONTAINER_ID, I_GENERIC_CONTAINER_ID);
  END IF;
EXCEPTION
WHEN FRM_TRIGGER_FAILURE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container6;
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
PROCEDURE add_labeled_container7(
    USER_CHOICE            IN VARCHAR2,
    V_RETURN               IN OUT VARCHAR2,
    I_MSG_DISPLAY          IN OUT VARCHAR2,
    I_FACILITY_ID          IN VARCHAR2,
    I_CONTAINER_ID         IN OUT VARCHAR2,
    I_GENERIC_CONTAINER_ID IN VARCHAR2,
    I_ITEM_ID              IN OUT VARCHAR2,
    I_STORAGE_CAT          IN OUT VARCHAR,
    I_CONT_DEST_ID         IN NUMBER,
    I_CONT_STATUS          IN VARCHAR2,
    G_MLD_ENABLED          IN VARCHAR2,
    I_USER                 IN VARCHAR2,
    I_UPS_CODE             IN OUT VARCHAR2,
    I_LPN_COUNT            IN OUT NUMBER )
IS
  CURSOR is_mixed_dest
  IS
    SELECT COUNT(DISTINCT dest_id)
    FROM container
    WHERE facility_id       = I_FACILITY_ID
    AND master_container_id = I_GENERIC_CONTAINER_ID;
  CURSOR c_chk_if_master_empty
  IS
    SELECT 'x'
    FROM Container
    WHERE Facility_ID       = I_FACILITY_ID
    AND Master_Container_ID = f_master_container_id
    AND rownum              < 2;
  CURSOR get_wave_nbr (container_id_in Container.container_id%TYPE)
  IS
    SELECT wave_nbr
    FROM container
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = container_id_in;
  CURSOR check_wave_numbers (container_id_in Container.container_id%TYPE)
  IS
    SELECT COUNT(DISTINCT(wave_nbr))
    FROM CONTAINER
    WHERE facility_id       = I_FACILITY_ID
    AND master_container_id = container_id_in;
  CURSOR c_get_container_details
  IS
    SELECT c.Wave_Nbr,
      ci.unit_qty,
      ci.item_id
    FROM Container c,
      Container_Item ci
    WHERE ci.Facility_ID = c.Facility_ID
    AND c.Facility_ID    = I_FACILITY_ID
    AND ci.Container_Id  = c.Container_ID
    AND c.Container_Id   = I_CONTAINER_ID;
  CURSOR c_get_ups
  IS
    SELECT upz.unit_pick_system_code
    FROM container c,
      unit_pick_zone upz
    WHERE c.facility_id  = I_FACILITY_ID
    AND c.facility_id    = upz.facility_id
    AND c.to_location_id = upz.drop_off_loc
    AND c.container_id   = I_CONTAINER_ID;
BEGIN
  IF(USER_CHOICE = 'Y') THEN
    UPDATE CONTAINER_ROUTE
    SET next_dest_id      = l_mixed_dest_id,
      final_dest_id       = l_mixed_dest_id,
      master_build_method = 'INTERNAL'
    WHERE facility_id     = I_FACILITY_ID
    AND container_id      = I_GENERIC_CONTAINER_ID;
    set_to_internal      := TRUE;
  ELSIF(USER_CHOICE       = 'N') THEN
    l_continue           := 'N';
  END IF;
  IF l_continue <> 'N' THEN
    get_container_location (I_FACILITY_ID, I_GENERIC_CONTAINER_ID, l_location_id);
    DELETE FROM ROSS_EVENTCODE_USERID_TMP;
    COMMIT;
    INSERT
    INTO ROSS_EVENTCODE_USERID_TMP
      (
        EVENT_CD,
        USER_ID,
        LOCATION_ID
      )
      VALUES
      (
        45,
        I_USER,
        l_location_id
      );
    UPDATE Container
    SET location_id       = NULL,
      master_container_id = I_GENERIC_CONTAINER_ID,
      user_id             = I_USER
    WHERE Facility_ID     = I_FACILITY_ID
    AND Container_ID      = I_CONTAINER_ID;
    PKG_HOTEL_INV_ADF.UPDATE_UPS_CODE(I_CONTAINER_ID ,I_GENERIC_CONTAINER_ID ,I_ITEM_ID ,I_UPS_CODE ,I_FACILITY_ID );
    IF NVL(f_master_container_id, 'NONE') <> 'NONE' THEN
      OPEN c_chk_if_master_empty;
      FETCH c_chk_if_master_empty INTO l_dummy;
      IF c_chk_if_master_empty%NOTFOUND THEN
        get_container_location (I_FACILITY_ID, I_GENERIC_CONTAINER_ID, l_location_id);
        DELETE FROM ROSS_EVENTCODE_USERID_TMP;
        COMMIT;
        INSERT
        INTO ROSS_EVENTCODE_USERID_TMP
          (
            EVENT_CD,
            USER_ID,
            LOCATION_ID
          )
          VALUES
          (
            42,
            I_USER,
            l_location_id
          );
        DELETE Container
        WHERE Facility_ID = I_FACILITY_ID
        AND Container_ID  = f_master_container_id;
      END IF;
      CLOSE c_chk_if_master_empty;
    END IF;
    OPEN is_mixed_dest;
    FETCH is_mixed_dest INTO l_count;
    CLOSE is_mixed_dest;
    get_container_location (I_FACILITY_ID, I_GENERIC_CONTAINER_ID, l_location_id);
    DELETE FROM ROSS_EVENTCODE_USERID_TMP;
    COMMIT;
    INSERT
    INTO ROSS_EVENTCODE_USERID_TMP
      (
        EVENT_CD,
        USER_ID,
        LOCATION_ID
      )
      VALUES
      (
        45,
        I_USER,
        l_location_id
      );
    UPDATE Container
    SET Dest_ID        = DECODE(l_count, 1, I_CONT_DEST_ID, l_mixed_dest_id),
      Container_Status = I_CONT_STATUS
    WHERE Facility_ID  = I_FACILITY_ID
    AND Container_ID   = I_GENERIC_CONTAINER_ID;
    IF G_MLD_ENABLED   = 'Y' AND set_to_internal <> TRUE THEN
      UPDATE Container_Route
      SET master_build_method = t_master_build_method
      WHERE Facility_ID       = I_FACILITY_ID
      AND Container_ID        = I_GENERIC_CONTAINER_ID;
    END IF;
    COMMIT;
    PKG_HOTEL_INV_ADF.UPDATE_LPN_COUNT(I_GENERIC_CONTAINER_ID ,I_FACILITY_ID ,I_LPN_COUNT);
    PKG_HOTEL_INV_ADF.UPDATE_STORAGE_CATEGORY(V_RETURN, I_GENERIC_CONTAINER_ID ,I_FACILITY_ID, I_STORAGE_CAT );
    IF V_RETURN IS NOT NULL THEN
      RAISE FRM_TRIGGER_FAILURE;
    END IF;
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'SUCCESS_OPER','M');
  END IF;
  OPEN get_wave_nbr (I_CONTAINER_ID);
  FETCH get_wave_nbr INTO l_lpn_wave_nbr;
  CLOSE get_wave_nbr;
  OPEN check_wave_numbers (I_GENERIC_CONTAINER_ID);
  FETCH check_wave_numbers INTO l_check_wave_numbers;
  check_wave_numbers_found := check_wave_numbers%FOUND;
  CLOSE check_wave_numbers;
  IF l_check_wave_numbers = 1 THEN
    l_mlp_wave_nbr       := l_lpn_wave_nbr;
  ELSE
    l_mlp_wave_nbr := NULL;
  END IF;
  UPDATE Container
  SET wave_nbr      = l_mlp_wave_nbr
  WHERE facility_id =I_FACILITY_ID
  AND container_id  = I_GENERIC_CONTAINER_ID;
  COMMIT;
  IF smart_move.send_inbound_carton(I_FACILITY_ID, I_CONTAINER_ID) = 'Y' THEN
    OPEN c_get_container_details;
    FETCH c_get_container_details INTO l_wave_nbr, l_unit_qty, l_item_id;
    CLOSE c_get_container_details;
    OPEN c_get_ups;
    FETCH c_get_ups INTO l_ups;
    CLOSE c_get_ups ;
    INSERT
    INTO Inbound_Carton
      (
        Facility_ID,
        Transaction_TS,
        Unit_Pick_System_Code,
        Wave_Nbr,
        Container_ID,
        Item_ID,
        Requested_Unit_Qty
      )
      VALUES
      (
        I_FACILITY_ID,
        NULL,
        l_ups,
        l_wave_nbr,
        I_CONTAINER_ID,
        l_item_id,
        l_unit_qty
      );
  END IF;
  L_CONTINUE     := NULL;
  I_CONTAINER_ID := NULL;
  DELETE FROM ROSS_EVENTCODE_USERID_TMP;
  COMMIT;
  RESET_GLOBAL_PARAMS;
EXCEPTION
WHEN FRM_TRIGGER_FAILURE THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN);
  RESET_GLOBAL_PARAMS;
  RETURN;
WHEN OTHERS THEN
  PREV_SINGLE_SKU_ENTERED := FALSE;
  PKG_COMMON_ADF.RAISE_FAILURE(V_RETURN );
  RESET_GLOBAL_PARAMS;
  RETURN;
END add_labeled_container7;
PROCEDURE RESET_GLOBAL_PARAMS
IS
BEGIN
  f_container_status          :=NULL;
  f_dest_id                   :=NULL;
  f_expedite_flag             :=NULL;
  f_carrier_code              :=NULL;
  f_service_code              :=NULL;
  f_route                     :=NULL;
  f_bol_nbr                   :=NULL;
  f_master_container_id       :=NULL;
  f_location_id               :=NULL;
  f_putaway_date              :=NULL;
  l_new_container             := 'N';
  l_dummy                     :=NULL;
  l_container_id              :=NULL;
  f_container_qty             :=NULL;
  t_container_qty             :=NULL;
  t_expedite_flag             :=NULL;
  t_carrier_code              :=NULL;
  t_service_code              :=NULL;
  t_route                     :=NULL;
  t_bol_nbr                   :=NULL;
  l_value                     :=NULL;
  l_mixed_dest_id             :=NULL;
  l_check                     :=NULL;
  l_location_id               :=NULL;
  f_po_nbr                    :=NULL;
  c_next_dest_id              := NULL;
  c_final_dest_id             := NULL;
  c_current_dest_id           := NULL;
  g_scp_mst_bld_cont          := NULL;
  t_master_build_method       := NULL;
  c_current_route             := NULL;
  m_next_dest_id              :=NULL;
  m_final_dest_id             := NULL;
  m_current_dest_id           := NULL;
  m_current_route             := NULL;
  set_to_internal             :=NULL;
  l_pick_to_container_id      :=NULL;
  l_continue                  := 'Y';
  l_mld_bld_meth_overide      :=NULL;
  l_count                     :=NULL;
  lv_putaway                  := 'N';
  value_found                 :=NULL;
  t_mark_status               :=NULL;
  t_ticket_request_status     :=NULL;
  t_item_id                   :=NULL;
  l_from_strg_location_type   :=NULL;
  l_to_strg_location_type     :=NULL;
  l_from_stgg_location_type   :=NULL;
  l_to_stgg_location_type     :=NULL;
  get_location_type_dtl_found :=NULL;
  l_mlp_wave_nbr              := NULL;
  l_lpn_wave_nbr              := NULL;
  l_check_wave_numbers        :=NULL;
  check_wave_numbers_found    :=NULL;
  l_unit_qty                  :=NULL;
  l_wave_nbr                  :=NULL;
  l_ups                       :=NULL;
  l_item_id                   :=NULL;
  l_compliance_type           :=NULL;
  l_supplement                :=NULL;
  l_item1_is_hazmat           := FALSE;
  l_item2_is_hazmat           := FALSE;
  l_hazmat                    :=NULL;
  l_item1_is_food             := FALSE;
  l_item2_is_food             := FALSE;
  l_item1_is_supplement       := FALSE;
  l_item2_is_supplement       := FALSE;
  l_item1_can_be_mixed        := TRUE;
  l_item2_can_be_mixed        := TRUE;
  l_item2_storage_cat         :=NULL;
  l_item1_storage_cat         :=NULL;
  l_mixed_pallet_flag         :=NULL;
  l_container                 :=NULL;
  l_min_pallet_flag           :=NULL;
  l_comp_found                :=NULL;
  l_supp_found                :=NULL;
  l_get_task_queue_info       :=NULL;
  l_task_queue_found          :=NULL;
  CHECK_POPUP                 :=NULL;
  PREV_SINGLE_SKU_ENTERED     :=FALSE;
END RESET_GLOBAL_PARAMS;
FUNCTION get_equipment_type_desc_wp(
    L_OUT_EXISTS         IN OUT NUMBER ,
    L_OUT_ERROR_MESSAGE  IN OUT VARCHAR2 ,
    L_OUT_EQUIPMENT_DESC IN OUT VARCHAR2 ,
    ti_equipment_type    IN OUT NUMBER ,
    facilityId           IN VARCHAR2)
  RETURN INTEGER
IS
  V_BOOLEAN     BOOLEAN;
  V_BOOLEAN_INT INTEGER;
  T_BOOLEAN     BOOLEAN;
BEGIN
  -- V_CLEAR_BLOCK_BOL := PKG_COMMON_ADF.Int2Boolean(V_CLEAR_BLOCK);
  V_BOOLEAN     := TASK_SQL.GET_EQUIPMENT_TYPE_DESC(L_OUT_ERROR_MESSAGE, T_BOOLEAN, L_OUT_EQUIPMENT_DESC, facilityId, ti_equipment_type);
  L_OUT_EXISTS  := PKG_COMMON_ADF.Boolean2Int(T_BOOLEAN);
  V_BOOLEAN_INT := PKG_COMMON_ADF.Boolean2Int(V_BOOLEAN);
  RETURN V_BOOLEAN_INT;
END get_equipment_type_desc_wp;
PROCEDURE p_get_order_by(
    p_facility_id IN VARCHAR2)
IS
BEGIN
  DELETE TASK_QUEUE
  WHERE facility_id   = p_facility_id
  AND activity_code  IN ('TRMOVI','SHMOVI')
  AND trailer_id NOT IN
    (SELECT trailer_id FROM trailer WHERE facility_id = p_facility_id
    );
  COMMIT;
END p_get_order_by;
PROCEDURE p_error_log(
    I_FACILITY_ID IN VARCHAR2,
    I_USER        IN VARCHAR2,
    L_LOCATION    IN VARCHAR2 )
IS
BEGIN
  INSERT
  INTO error_log
    (
      Facility_id,
      Error_code,
      User_id,
      Error_ts,
      Error_source,
      Error_loc,
      Error_msg
    )
    VALUES
    (
      I_FACILITY_ID,
      455,
      I_USER,
      SYSDATE,
      'HH_TASK_QUEUE_S',
      'Populate_Task_Queue',
      'The location '
      ||L_LOCATION
      ||' does not have a WH ID.'
    );
  COMMIT;
END p_error_log;
FUNCTION assign_task_to_user
  (
    L_OUT_ERROR_MESSAGE IN OUT VARCHAR2 ,
    facilityId          IN VARCHAR2 ,
    taskId              IN NUMBER ,
    userId              IN VARCHAR2
  )
  RETURN INTEGER
IS
  V_BOOLEAN     BOOLEAN;
  V_BOOLEAN_INT INTEGER;
BEGIN
  -- V_CLEAR_BLOCK_BOL := PKG_COMMON_ADF.Int2Boolean(V_CLEAR_BLOCK);
  V_BOOLEAN := TASK_SQL.ASSIGN_TASK_TO_USER(L_OUT_ERROR_MESSAGE, facilityId, taskId, userId);
  IF L_OUT_ERROR_MESSAGE is NULL THEN 
    COMMIT;
    RETURN 1;
  ELSE 
    RETURN 0;
  END IF;
END assign_task_to_user;
END PKG_INDEPENDENT_ADF;
/
