CREATE OR REPLACE PACKAGE PKG_HOTEL_PICKING_SMART_MOVE
IS
  mldenabled VARCHAR2(1);
  mlddest    VARCHAR2(4);
  mldfactype VARCHAR2(4);
  l_dc_dest_id SCP.SCP_Current_Val%TYPE;
  l_fpl_replen_dest_id SCP.SCP_Current_Val%TYPE;
  l_in_transit_loc Location.Location_ID%TYPE;
  l_allow_trouble_put SCP.SCP_Current_Val%TYPE;
  l_labeled_reserve SCP.SCP_Current_Val%TYPE;
  l_dest_id Container.Dest_ID%TYPE;
  l_pick_type Container.Pick_Type%TYPE;
  l_master_container_id Container.Master_Container_ID%TYPE;
  l_container_status Container.Container_Status%TYPE;
  l_to_location_id Container.To_Location_ID%TYPE;
  l_location_id Container.Location_ID%TYPE;
  l_putaway_date Container.Putaway_Date%TYPE;
  L_receipt_date            VARCHAR2(10);
  L_receipt_date_new        VARCHAR2(10);
  L_pick_not_after_date     VARCHAR2(10);
  L_pick_not_after_date_new VARCHAR2(10);
  L_perishable ITEM_MASTER.PERISHABLE_IND%TYPE;
  L_perishable_new ITEM_MASTER.PERISHABLE_IND%TYPE;
  l_lot_nbr_new Container.Lot_nbr%TYPE;
  l_lot_nbr Container.Lot_nbr%TYPE;
  l_case_pack_size_new pick_directive.case_pack_size%TYPE;
  l_case_pack_size pick_directive.case_pack_size%TYPE;
  l_dummy       VARCHAR2(1);
  l_count       NUMBER(6);
  l_count_items NUMBER(6);
  l_ret_code    VARCHAR2(1);
  l_qty pick_directive.unit_qty%TYPE;
  l_desc Item_Master.description%TYPE;
  l_total_container_qty NUMBER := 0;
  l_total_unit_qty      NUMBER := 0;
  l_cid Container.Container_id%TYPE;
  --Begin CR524 - Ross Display of UPS Codes
  L_ups_count NUMBER;
  --End CR524 - Ross Display of UPS Codes
  value_found                BOOLEAN;
  labl_resrv_no_mult_sku_put BOOLEAN := TRUE;
  inv_container              EXCEPTION;
  inv_status                 EXCEPTION;
  inv_dest                   EXCEPTION;
  inv_mixed_casepack         EXCEPTION;
  already_putaway            EXCEPTION;
  cont_on_master             EXCEPTION;
  no_items                   EXCEPTION;
  lab_container              EXCEPTION;
  inv_lot_case               EXCEPTION;
  /* Start - CR43433 - Long - 3 */
  Error_msg EXCEPTION;
  /* End - CR43433 - Long - 3 */
  l_hazmat             VARCHAR2(10);
  l_item_is_hazmat     BOOLEAN;
  l_food               VARCHAR2(1);
  l_item_is_food       BOOLEAN;
  l_supplement         VARCHAR2(1);
  l_item_is_supplement BOOLEAN;
  l_dedicated          NUMBER;
  l_mlp_has_mult_flag  BOOLEAN;
  l_item_count         NUMBER;
  l_from_wh WH.wh_id%TYPE;
  l_divert_flag WH.create_divert_flag%TYPE;
  l_wh_found VARCHAR2(1);
  l_item_storage PUTAWAY_REQ.spec_storage_name%TYPE;
  l_dedicated_zone PUTAWAY_REQ.dedicated_zone_flag%TYPE;
  l_zone            VARCHAR2(1);
  l_pref_zone_found BOOLEAN;
  l_pref_zone PUTAWAY_REQ_WH.preferred_zone%TYPE;
  l_seq_id PUTAWAY_REQ.seq_id%TYPE;
  l_scanned_zone LOCATION.zone%TYPE;
  l_storage_cat PUTAWAY_REQ.spec_storage_name%TYPE;
  l_dedicated_flag PUTAWAY_REQ.dedicated_zone_flag%TYPE;
  l_seq PUTAWAY_REQ.seq_id%TYPE;
  l_preferred_zone PUTAWAY_REQ_WH.preferred_zone%TYPE;

  ---------------------------------------------------------------------------------------------
  PROCEDURE DISPLAY_MOVE_BLOCK(
      V_RETURN                IN OUT VARCHAR2,
      I_MSG_DISPLAY           IN OUT VARCHAR2,
      I_CONTAINER_ID          IN VARCHAR2 ,
      I_CONTAINER_QTY         IN OUT NUMBER ,
      I_DESC                  IN OUT VARCHAR2 ,
      I_DEST_ID               IN OUT NUMBER ,
      I_ITEM_ID               IN OUT VARCHAR2 ,
      I_LOCATION_ID           IN OUT VARCHAR2 ,
      I_OLD_LOCATION          IN OUT VARCHAR2 ,
      I_TO_LOCATION           IN OUT VARCHAR2 ,
      I_UPS_CODE              IN OUT VARCHAR2 ,
      I_WCS_TO_LOCATION       IN OUT VARCHAR2 ,
      I_FACILITY_ID           IN VARCHAR2 ,
      I_CID_REF_NUM           IN OUT NUMBER ,
      I_DIVERT_OUT            IN OUT VARCHAR2 ,
      I_DROPOFF_LOC_WCS       IN OUT VARCHAR2 ,
      I_FROM_LOC_STORAGE_FLAG IN OUT VARCHAR2 ,
      I_WH_ID                 IN OUT VARCHAR2 ,
      I_USER                  IN VARCHAR2,
      V_CLEAR_BLOCK           OUT INT,
      I_STORAGE_CATEGORY      IN OUT VARCHAR2,
      I_SEQ_ID                IN OUT NUMBER,
      I_DEDICATED_ZONE_FLAG   IN OUT VARCHAR2,
      I_ITEM_ID_WL            IN OUT VARCHAR2,
      I_HAS_FOOD              IN OUT VARCHAR2,
      I_HAS_SUPPLEMENT        IN OUT VARCHAR2) ;  
---------------------------------------------------------------------------------------------

END PKG_HOTEL_PICKING_SMART_MOVE;
/


CREATE OR REPLACE PACKAGE Body PKG_HOTEL_PICKING_SMART_MOVE
IS
  FRM_TRIGGER_FAILURE EXCEPTION;
  PRAGMA EXCEPTION_INIT(FRM_TRIGGER_FAILURE, -20777);  
---------------------------------------------------------------------------------------------
  
PROCEDURE DISPLAY_MOVE_BLOCK(
    V_RETURN                IN OUT VARCHAR2 ,
    I_MSG_DISPLAY           IN OUT VARCHAR2,
    I_CONTAINER_ID          IN VARCHAR2 ,
    I_CONTAINER_QTY         IN OUT NUMBER ,
    I_DESC                  IN OUT VARCHAR2 ,
    I_DEST_ID               IN OUT NUMBER ,
    I_ITEM_ID               IN OUT VARCHAR2 ,
    I_LOCATION_ID           IN OUT VARCHAR2 ,
    I_OLD_LOCATION          IN OUT VARCHAR2 ,
    I_TO_LOCATION           IN OUT VARCHAR2 ,
    I_UPS_CODE              IN OUT VARCHAR2 ,
    I_WCS_TO_LOCATION       IN OUT VARCHAR2 ,
    I_FACILITY_ID           IN VARCHAR2 ,
    I_CID_REF_NUM           IN OUT NUMBER ,
    I_DIVERT_OUT            IN OUT VARCHAR2 ,
    I_DROPOFF_LOC_WCS       IN OUT VARCHAR2 ,
    I_FROM_LOC_STORAGE_FLAG IN OUT VARCHAR2 ,
    I_WH_ID                 IN OUT VARCHAR2 ,
    I_USER                  IN VARCHAR2,
    V_CLEAR_BLOCK           OUT INT,
    I_STORAGE_CATEGORY      IN OUT VARCHAR2 ,
    I_SEQ_ID                IN OUT NUMBER ,
    I_DEDICATED_ZONE_FLAG   IN OUT VARCHAR2 ,
    I_ITEM_ID_WL            IN OUT VARCHAR2 ,
    I_HAS_FOOD              IN OUT VARCHAR2 ,
    I_HAS_SUPPLEMENT        IN OUT VARCHAR2)
IS
  t_transit_loc Container.location_id%TYPE;
  t_container_id Container.container_id%TYPE;
  t_master_container_loc_id Container.container_id%TYPE;
  t_container_status Container.Container_Status%TYPE;
  t_dest_id Container.Dest_ID%TYPE;
  value_not_found BOOLEAN;
  inv_from_loc    EXCEPTION;
  inv_cont_stat   EXCEPTION;
  inv_cont        EXCEPTION;
  no_trans_loc    EXCEPTION;
  t_location_status Location.Location_Status%TYPE;
  t_cycle_count_status Location.Cycle_Count_Status%TYPE;
  t_unit_pick_flag Loc_Type.Unit_Pic_Loc_Flag%TYPE;
  scp_labeled_reserve SCP.SCP_CURRENT_VAL%TYPE;
  scp_mixed_wip_stage_loc SCP.SCP_CURRENT_VAL%TYPE;
  temp_to_location_id CONTAINER.TO_LOCATION_ID%TYPE;
  -- BEGIN CR555
  CONT_WAVED EXCEPTION;
  LPN_QTY    NUMBER(6);
  t_dist_qty_out PICK_DIRECTIVE.UNIT_QTY%TYPE;
  t_program_name_out VARCHAR2(20);
  t_ret_status_out   VARCHAR2(8);
  t_ret_code_out     VARCHAR2(10);
  -- END CR555
  -- BEGIN DEFECT394614
  t_cont_id Container.container_id%TYPE;
  t_master_cont_id Container.container_id%TYPE;
  -- END DEFECT394614
  -- BEGIN DEFECT394673
  t_child_cont_id Container.container_id%TYPE;
  t_item_id Container_item.item_id%TYPE;
  t_unit_qty Container_item.unit_qty%TYPE;
  -- END DEFECT394673
  -- BEGIN DEFECT395358
  t_dummy       VARCHAR2(1);
  t_master_cont BOOLEAN;
  t_cont_picked BOOLEAN;
  -- END DEFECT395358
  t_dc_dest_id NUMBER;
  l_directed_dropoff_loc SCP_WH.SCP_CURRENT_VAL%TYPE;
  /*Start - JESD - CO60405*/
  l_wrt_loc container.location_id%TYPE;
  /*End - JESD - CO60405*/
  /*Start CO98623 C. Pineda*/
  l_divert      VARCHAR2(7);
  l_toloc_found BOOLEAN;
  /*End CO98623 C. Pineda*/
  -----------------------------------------------------------------------
  /*
  CURSOR location_id_cursor IS
  SELECT location_id, master_container_id, to_location_id,
  dest_id
  FROM container
  WHERE facility_id  = I_FACILITY_ID
  AND container_id = t_container_id;
  */
  -----------------------------------------------------------------------
  l_get_item putaway_req.spec_storage_name%TYPE;
  l_storage_cat putaway_req.spec_storage_name%TYPE;
  l_pref_zone putaway_req_wh.preferred_zone%TYPE;
  l_from_wh wh.wh_id%TYPE;
  l_seq_id putaway_req.seq_id%TYPE;
  l_seq putaway_req.seq_id%TYPE;
  l_divert_flag wh.create_divert_flag%TYPE;
  l_wh_found           VARCHAR2(1);
  l_hazmat             VARCHAR2(10);
  l_item_is_hazmat     BOOLEAN;
  l_food               VARCHAR2(1);
  l_item_is_food       BOOLEAN;
  l_supplement         VARCHAR2(1);
  l_item_is_supplement BOOLEAN;
  l_dedicated          NUMBER;
  l_mlp_has_mult_flag  BOOLEAN;
  l_dedicated_flag putaway_req.dedicated_zone_flag%TYPE;
  l_item_count          NUMBER;
  l_loc_type            VARCHAR2(1);
  l_packaway_loc        VARCHAR2(1);
  l_packaway_loc_found  BOOLEAN;
  l_hotel_stg_loc_found BOOLEAN;
  l_display_storage     BOOLEAN;
  CURSOR labelN_location_id_cursor
  IS
    SELECT to_location_id,
      dest_id
    FROM container
    WHERE facility_id      = I_FACILITY_ID
    AND (container_id      = t_container_id
    OR master_container_id = t_container_id )
    AND to_location_id    IS NOT NULL ;
  CURSOR labelY_location_id_cursor
  IS
    SELECT Master_Container_Id,
      location_id,
      to_location_id,
      dest_id
    FROM container
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = t_container_id;
  CURSOR Master_location_id_cursor
  IS
    SELECT to_location_id,
      dest_id
    FROM container
    WHERE facility_id       = I_FACILITY_ID
    AND master_container_id = t_container_id;
  CURSOR loc_id_cursor
  IS
    SELECT Master_container_Id,
      location_id
    FROM container
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = t_container_id;
  -----------------------------------------------------------------------
  CURSOR location_cursor
  IS
    SELECT location_status,
      cycle_count_status,
      storage_location_flag,
      unit_pic_loc_flag
    FROM location l,
      loc_type lt
    WHERE l.facility_id  = I_FACILITY_ID
    AND l.location_id    = I_LOCATION_ID
    AND lt.facility_id   = l.facility_id
    AND lt.location_type = l.location_type;
  ---------------------------------------------------------------------
  CURSOR item_id_cursor
  IS
    SELECT item_id
    FROM container_item
    WHERE facility_id = I_FACILITY_ID
    AND container_id  = I_CONTAINER_ID
    ORDER BY unit_qty DESC;
  ---------------------------------------------------------------------
  --Begin CR514 - Ross Display of UPS Codes
  L_ups_code Item_Master.Unit_Pick_System_Code%TYPE;
  L_temp Container.Container_id%TYPE;
  L_ups_count NUMBER := 0;
  L_child_id Container.Container_id%TYPE;
  ---------------------------------------------------------------------
  CURSOR get_ups_count
  IS
    SELECT COUNT(DISTINCT I.unit_pick_system_code)
    FROM container_item CI,
      item_master I
    WHERE CI.facility_id = I_FACILITY_ID
    AND CI.facility_id   = I.facility_id
    AND CI.item_id       = I.item_id
    AND container_id     = I_CONTAINER_ID;
  -----------------------------------------------------------------------
  CURSOR get_ups_code
  IS
    SELECT unit_pick_system_code
    FROM item_master
    WHERE facility_id = I_FACILITY_ID
    AND item_id       = I_ITEM_ID;
  CURSOR get_child_ids
  IS
    SELECT container_id
    FROM container C
    WHERE C.master_container_id = I_CONTAINER_ID
    AND C.facility_id           = I_FACILITY_ID;
  CURSOR get_child_ups_code
  IS
    SELECT DISTINCT I.unit_pick_system_code
    FROM container_item CI,
      Item_Master I
    WHERE CI.facility_id = I_FACILITY_ID
    AND CI.facility_id   = I.facility_id
    AND CI.item_id       = I.item_id
    AND CI.container_id  = L_child_id;
  --End CR514 - Ross Display of UPS Codes
  ---------------------------------------------------------------------
  CURSOR desc_cursor
  IS
    SELECT description
    FROM item_master
    WHERE facility_id = I_FACILITY_ID
    AND item_id       = I_ITEM_ID;
  ----------------------------------------------------------------------
  -- BEGIN CR555
  CURSOR get_LPN_qty
  IS
    SELECT COUNT(container_id)
    FROM container C
    WHERE C.master_container_id = I_CONTAINER_ID
    AND C.facility_id           = I_FACILITY_ID;
  ----------------------------------------------------------------------
  CURSOR C_cont_is_waved
  IS
    SELECT 'x'
    FROM pick_directive pd
    WHERE pd.facility_id           = I_FACILITY_ID
    AND pd.pick_type              <> 'U'
    AND (pd.pick_from_container_id = I_CONTAINER_ID
    OR pd.pick_from_container_id   =
      (SELECT c.master_container_id
      FROM container c
      WHERE c.facility_id = pd.facility_id
      AND c.container_id  = I_CONTAINER_ID
      ));
    ----------------------------------------------------------------------
    --END CR555
    -- BEGIN DEFECT394614
    -- Get the master and child container in Container table
    CURSOR C_get_cont_info
    IS
      SELECT c.container_id,
        c.master_container_id
      FROM container c,
        container_item ci
      WHERE ci.facility_id     = c.facility_id
      AND ci.container_id      = c.container_id
      AND c.facility_id        = I_FACILITY_ID
      AND (c.container_id      = I_CONTAINER_ID
      OR c.master_container_id = I_CONTAINER_ID);
    -- END DEFECT394614
    ----------------------------------------------------------------------
    -- BEGIN DEFECT394673
    CURSOR C_get_item_cp
    IS
      SELECT item_id,
        unit_qty
      FROM container_item
      WHERE facility_id = I_FACILITY_ID
      AND container_id  = t_child_cont_id;
    -- END DEFECT394673
    ----------------------------------------------------------------------
    -- BEGIN DEFECT395358
    -- Check if container is a master container.
    CURSOR C_check_master_cont
    IS
      SELECT 'X'
      FROM container
      WHERE facility_id       = I_FACILITY_ID
      AND master_container_id = I_CONTAINER_ID
      AND rownum              < 2;
    -- Check if the master container and its LPNs to be moved have been picked.
    -- Changed because LPNs could be in Trouble and outbound and screen would not let movement
    -- Now check if D or T and not the DC dest ID
    CURSOR C_master_cont_is_picked
    IS
      SELECT 'X'
      FROM dual
      WHERE EXISTS
        (SELECT 'X'
        FROM container
        WHERE facility_id       = I_FACILITY_ID
        AND master_container_id = I_CONTAINER_ID
        AND container_status   IN ('T','D')
        AND dest_id            <> t_dc_dest_id
        );
    -- Check if the container to be moved has been picked.
    -- Changed because LPNs could be in Trouble and outbound and screen would not let movement
    -- Now check if D or T and not the DC dest ID
    CURSOR C_cont_is_picked
    IS
      SELECT 'X'
      FROM container
      WHERE facility_id     = I_FACILITY_ID
      AND container_id      = I_CONTAINER_ID
      AND container_status IN ('T','D')
      AND dest_id          <> t_dc_dest_id;
    -- END DEFECT395358
    ----------------------------------------------------------------------
    /*Start CO98623 K. Manangan*/
    CURSOR C_get_mlp_toloc
    IS
      SELECT container_id,
        to_location_id
      FROM container
      WHERE facility_id       = I_FACILITY_ID
      AND master_container_id = I_CONTAINER_ID;
    CURSOR C_get_lpn_toloc
    IS
      SELECT to_location_id
      FROM container
      WHERE facility_id = I_FACILITY_ID
      AND container_id  = I_CONTAINER_ID;
    L_move_cont_id Container.Container_id%TYPE;
    L_wcs_to_loc Container.To_Location_id%TYPE;
    L_master_cont     BOOLEAN;
    L_mstr_cont_fetch VARCHAR2(1);
    L_wh_id WH.wh_id%TYPE;
    /*End CO98623 K. Manangan*/
    CURSOR get_item_id (c_temp container.container_id%TYPE)
    IS
      SELECT CI.item_id
      FROM container_item CI
      WHERE CI.facility_id = I_FACILITY_ID
      AND container_id     = c_temp;
    CURSOR c_check_mlp_has_food
    IS
      SELECT 'x'
      FROM container c,
        container_item ci,
        item_master im,
        class cl
      WHERE c.facility_id        = I_FACILITY_ID
      AND (c.master_container_id = I_CONTAINER_ID
      OR c.container_id          = I_CONTAINER_ID)
      AND c.facility_id          = ci.facility_id
      AND c.container_id         = ci.container_id
      AND ci.facility_id         = im.facility_id
      AND ci.item_id             = im.item_id
      AND im.facility_id         = cl.facility_id
      AND im.department          = cl.department
      AND im.class               = cl.class
      AND cl.compliance_type     = 'FOOD'
      AND ROWNUM                 < 2;
    CURSOR check_mlp_has_supplement
    IS
      SELECT 'x'
      FROM container c,
        container_item ci,
        cms_uda_item_ff cf,
        class cl,
        item_master im
      WHERE c.facility_id                                               = I_FACILITY_ID
      AND (c.master_container_id                                        = I_CONTAINER_ID
      OR c.container_id                                                 = I_CONTAINER_ID)
      AND c.facility_id                                                 = ci.facility_id
      AND c.container_id                                                = ci.container_id
      AND ci.facility_id                                                = cf.facility_id
      AND SUBSTR(ci.item_id,1,12)                                       = SUBSTR(cf.item_id,1,12)
      AND cf.uda_id                                                     = 2550
      AND ci.facility_id                                                = im.facility_id
      AND ci.item_id                                                    = im.item_id
      AND im.facility_id                                                = cl.facility_id
      AND im.department                                                 = cl.department
      AND im.class                                                      = cl.class
      AND NVL(cl.compliance_type,'~')                                  <> 'FOOD'
      AND fn_chk_item_is_hazmat (I_FACILITY_ID, im.item_id, NULL, NULL) = 'NON_HAZMAT'
      AND ROWNUM                                                        < 2;
    CURSOR c_check_mlp_has_hazmat
    IS
      SELECT 'x'
      FROM container_item ci,
        container c
      WHERE ci.facility_id                                                = I_FACILITY_ID
      AND ci.facility_id                                                  = c.facility_id
      AND (c.master_container_id                                          = I_CONTAINER_ID
      OR c.container_id                                                   = I_CONTAINER_ID)
      AND c.container_id                                                  = ci.container_id
      AND fn_chk_item_is_hazmat (ci.facility_id, ci.item_id, NULL, NULL) <> 'NON_HAZMAT'
      AND ROWNUM                                                          < 2;
    CURSOR c_check_mlp_has_mult_flag
    IS
      SELECT COUNT (DISTINCT get_putaway_req.fn_get_item_spec_storage_name(ci.facility_id, ci.item_id)) storage_name
      FROM container_item ci,
        container c
      WHERE ci.facility_id                                                                              = I_FACILITY_ID
      AND ci.facility_id                                                                                = c.facility_id
      AND (c.master_container_id                                                                        = I_CONTAINER_ID
      OR c.container_id                                                                                 = I_CONTAINER_ID)
      AND c.container_id                                                                                = ci.container_id
      AND get_putaway_req.fn_get_mixed_pallet_flag(ci.facility_id, ci.item_id)                          = 'N'
      HAVING COUNT (DISTINCT get_putaway_req.fn_get_item_spec_storage_name(ci.facility_id, ci.item_id)) > 1;
    CURSOR c_get_item_count
    IS
      SELECT COUNT(DISTINCT SUBSTR(item_id,1,12))
      FROM container_item ci,
        container c
      WHERE ci.facility_id       = I_FACILITY_ID
      AND ci.facility_id         = c.facility_id
      AND (c.master_container_id = I_CONTAINER_ID
      OR c.container_id          = I_CONTAINER_ID)
      AND c.container_id         = ci.container_id;
    CURSOR c_get_min_seq_id
    IS
      SELECT MIN(seq_id)
      FROM putaway_req
      WHERE facility_id     = I_FACILITY_ID
      AND spec_storage_name = l_storage_cat;
    CURSOR get_to_loc_type
    IS
      SELECT 'x'
      FROM location l,
        loc_type lt,
        container c
      WHERE c.facility_id        = I_FACILITY_ID
      AND (c.master_container_id = I_CONTAINER_ID
      OR c.container_id          = I_CONTAINER_ID)
      AND c.facility_id          = l.facility_id
      AND c.to_location_id       = l.location_id
      AND l.facility_id          = lt.facility_id
      AND l.location_type        = lt.location_type
      AND lt.location_type       = G_SCP(I_FACILITY_ID,'packaway_loc_type')
      AND rownum                 < 2;
    CURSOR get_packaway_to_loc
    IS
      SELECT 'x'
      FROM container c
      WHERE c.facility_id        = I_FACILITY_ID
      AND (c.master_container_id = I_CONTAINER_ID
      OR c.container_id          = I_CONTAINER_ID)
      AND c.to_location_id       = G_SCP(I_FACILITY_ID,'putaway_stage_loc')
      AND rownum                 < 2;
  BEGIN
    V_CLEAR_BLOCK       := 0;
    scp_labeled_reserve := G_SCP(I_FACILITY_ID,'labeled_reserve') ;
    t_dc_dest_id        := to_number(g_scp(I_FACILITY_ID, 'DC_dest_ID'));
    /*Start CO98623 C. Pineda*/
    I_DROPOFF_LOC_WCS := 'N';
    /*End CO98623 C. Pineda*/
    /*Start - JESD - CO60405*/
    IF I_TO_LOCATION IS NOT NULL THEN
      l_wrt_loc      := I_TO_LOCATION;
      /*Start CO98623 C. Pineda*/
    ELSE
      l_directed_dropoff_loc                                            := GET_DROP_OFF_LOC (I_FACILITY_ID, I_CONTAINER_ID);
      IF l_directed_dropoff_loc                                         IS NULL THEN
        IF smart_move_eligible(I_FACILITY_ID, I_CONTAINER_ID, I_WH_ID)='Y' THEN
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move_eligible = Y -> WH: '||I_WH_ID);
          SELECT move_seq_nbr.nextval INTO I_CID_REF_NUM FROM dual;
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move.fn_send_message -> FACILITY: '||I_FACILITY_ID);
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move.fn_send_message -> WH: '||I_WH_ID);
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move.fn_send_message -> MOVE_SEQ_NBR: '||I_CID_REF_NUM);
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move.fn_send_message -> CONTAINER: '||I_CONTAINER_ID);
          l_divert   := smart_move.fn_send_message(I_FACILITY_ID, I_WH_ID, I_CID_REF_NUM, I_CONTAINER_ID, NULL,NULL, 'R', I_WCS_TO_LOCATION);
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move.fn_send_message -> RESULT: '||l_divert);
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move.fn_send_message -> RETURNED LOCATION: '||I_WCS_TO_LOCATION);
          IF l_divert = 'FAILURE' OR l_divert = '0' THEN
            OPEN C_check_master_cont;
            FETCH C_check_master_cont INTO L_mstr_cont_fetch;
            L_master_cont := C_check_master_cont%FOUND;
            CLOSE C_check_master_cont;
            --Check if MLP. If MLP, loop through lpns and get to_location of Smart Move eligible LPN
            IF L_master_cont THEN
              FOR lpn_index IN C_get_mlp_toloc
              LOOP
                IF (smart_move_eligible(I_FACILITY_ID, lpn_index.container_id, L_wh_id) = 'Y') THEN
                  I_WCS_TO_LOCATION                                                    := lpn_index.to_location_id;
                  EXIT;
                END IF;
              END LOOP;
            ELSE
              OPEN C_get_lpn_toloc;
              FETCH C_get_lpn_toloc INTO L_wcs_to_loc;
              L_toloc_found := C_get_lpn_toloc%FOUND;
              CLOSE C_get_lpn_toloc;
              IF L_toloc_found THEN
                I_WCS_TO_LOCATION := L_wcs_to_loc;
              END IF;
            END IF;
            I_DROPOFF_LOC_WCS := 'Y';
            I_DIVERT_OUT      := '0';
          ELSE
            I_DROPOFF_LOC_WCS := 'Y';
            I_DIVERT_OUT      := l_divert;
          END IF;
        ELSE
          I_DROPOFF_LOC_WCS := 'N';
		  LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'smart_move_eligible = N -> WH: '||I_WH_ID);
        END IF;
        /*End CO98623 C. Pineda*/
      END IF; -- Check for directed drop-off location
    END IF;
    /*End - JESD - CO60405*/
    IF (v_container_id(I_CONTAINER_ID, I_FACILITY_ID)                               = 'Y') THEN
      IF (PKG_HOTEL_PICKING_ADF.CK_CONTAINER_STATUS (I_CONTAINER_ID, I_FACILITY_ID) = 'Y') THEN
        -- BEGIN DEFECT394614
        -- Changed the Validation of Distribution quantity of the container
        OPEN C_get_cont_info;
        FETCH C_get_cont_info INTO t_cont_id, t_master_cont_id;
        CLOSE C_get_cont_info;
        IF t_master_cont_id = 'NONE' THEN
          -- Call distributed_qty to see if the container has a distributed quantity
          Distributed_Qty (I_FACILITY_ID, t_cont_id, NULL, NULL, NULL, NULL, t_dist_qty_out, t_program_name_out, t_ret_status_out, t_ret_code_out);
        ELSE
          t_child_cont_id := t_cont_id; -- DEFECT394673
          t_cont_id       := t_master_cont_id;
          -- Call distributed_qty to see if the container has a distributed quantity
          -- BEGIN DEFECT394673
          -- Get the item_id and unit_qty of the container from the container_item table
          -- and pass the values to distributed_qty.
          OPEN C_get_item_cp;
          FETCH C_get_item_cp INTO t_item_id, t_unit_qty;
          CLOSE C_get_item_cp;
          Distributed_Qty (I_FACILITY_ID, t_cont_id, t_item_id, --NULL,
          t_unit_qty,                                           --NULL,
          NULL, NULL, t_dist_qty_out, t_program_name_out, t_ret_status_out, t_ret_code_out);
          -- END DEFECT394673
        END IF;
        -- BEGIN DEFECT395358
        -- Check if container is a master container
        OPEN C_check_master_cont;
        FETCH C_check_master_cont INTO t_dummy;
        t_master_cont := C_check_master_cont%FOUND;
        CLOSE C_check_master_cont;
        IF t_master_cont = TRUE THEN
          -- Check if master container to be moved has been picked.
          OPEN C_master_cont_is_picked;
          FETCH C_master_cont_is_picked INTO t_dummy;
          t_cont_picked := C_master_cont_is_picked%FOUND;
          CLOSE C_master_cont_is_picked;
        ELSE
          -- Check if container to be moved has been picked.
          OPEN C_cont_is_picked;
          FETCH C_cont_is_picked INTO t_dummy;
          t_cont_picked := C_cont_is_picked%found;
          CLOSE C_cont_is_picked;
        END IF;
        -- END DEFECT395358
        -- Check to see if the container has a distributed qty
        -- If so display error message and container quantity.
        -- BEGIN DEFECT395358
        -- If the container has not been picked then display the
        -- error message and container qty. If it has been picked,
        -- continue with move process.
        -- END DEFECT395358
        IF t_dist_qty_out > 0 AND t_cont_picked = FALSE THEN
          OPEN get_LPN_qty;
          FETCH get_LPN_qty INTO LPN_QTY;
          CLOSE get_LPN_qty;
          IF LPN_QTY         = 0 THEN
            I_CONTAINER_QTY := 1;
          ELSE
            I_CONTAINER_QTY := LPN_QTY;
          END IF;
          Raise cont_waved;
        END IF;
        -- END DEFECT394614
        t_container_id := I_CONTAINER_ID;
       
        /* Code added by AB 02/11/00  */
        IF scp_labeled_reserve='N' THEN
          OPEN labelN_location_id_cursor;
          FETCH labelN_location_id_cursor INTO I_TO_LOCATION, I_DEST_ID;
          CLOSE labelN_location_id_cursor;
          --Get location Id
          OPEN loc_id_cursor;
          FETCH loc_id_cursor INTO t_master_container_loc_id,I_LOCATION_ID;
          CLOSE loc_id_cursor;
          -- Save the the orginal location
          I_OLD_LOCATION := I_LOCATION_ID;
          -- Check if null location (i.e., child container)
          IF I_LOCATION_ID IS NULL THEN
            -- We have a child container, get location from its master
            t_container_id := t_master_container_loc_id;
            OPEN loc_id_cursor;
            FETCH loc_id_cursor INTO t_master_container_loc_id,I_LOCATION_ID;
            CLOSE loc_id_cursor;
            -- This is a child container set the old_location to NULL
            I_OLD_LOCATION := NULL;
          END IF;
        ELSIF scp_labeled_reserve = 'Y' THEN
          OPEN labelY_location_id_cursor;
          FETCH labelY_location_id_cursor
          INTO t_master_container_loc_id,
            I_LOCATION_ID,
            I_TO_LOCATION,
            I_DEST_ID;
          CLOSE labelY_location_id_cursor;
          IF I_LOCATION_ID IS NOT NULL THEN -- Start Scanning Children
            -- Save the the orginal location
            I_OLD_LOCATION := I_LOCATION_ID;
            OPEN Master_location_id_cursor;
            LOOP
              FETCH Master_location_id_cursor INTO I_TO_LOCATION, I_DEST_ID;
              EXIT
            WHEN Master_location_id_cursor%NOTFOUND;
              IF Master_location_id_cursor%ROWCOUNT > 1 THEN
                IF temp_to_location_id             !=I_TO_LOCATION THEN
                  scp_mixed_wip_stage_loc          := G_SCP(I_FACILITY_ID,'mixed_wip_stage_loc');
                  I_TO_LOCATION                    := scp_mixed_wip_stage_loc;
                  EXIT;
                END IF;
              END IF;
              temp_to_location_id := I_TO_LOCATION;
            END LOOP;
            CLOSE Master_location_id_cursor ;
          ELSE --Move Child OFF that's done in Process_move
            -- We have a child container, get location from its master
            t_container_id := t_master_container_loc_id;
            OPEN loc_id_cursor ;
            FETCH loc_id_cursor INTO t_master_container_loc_id,I_LOCATION_ID;
            CLOSE loc_id_cursor;
            -- This is a child container set the old_location to NULL
            I_OLD_LOCATION := NULL;
          END IF;
        END IF;
        -- Determine characteristics of the current location of the container
        -- If the location is OUT OF SERVICE, is manually marked for cycle
        -- count, or is a unit picking location, then the move will be
        -- disallowed.
        /*Start CO98623 K. Manangan*/
        --If the container is smart move eligible, assigned the location sent by the WCS
        --or if fn_send_message returns a failure, assign the drop off location of PTV
        IF l_directed_dropoff_loc IS NOT NULL THEN
          I_TO_LOCATION           := l_directed_dropoff_loc;
        END IF;
        IF(I_DROPOFF_LOC_WCS ='Y') THEN
          I_TO_LOCATION     := I_WCS_TO_LOCATION;
          LOG_ERROR_GUI(I_FACILITY_ID, -9999, I_USER, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', 'UPDATED LOCATION: '||I_WCS_TO_LOCATION);
        END IF;
        /*End CO98623 K. Manangan*/
        OPEN get_to_loc_type;
        FETCH get_to_loc_type INTO l_loc_type;
        l_hotel_stg_loc_found := get_to_loc_type%FOUND;
        CLOSE get_to_loc_type;
        OPEN get_packaway_to_loc;
        FETCH get_packaway_to_loc INTO l_packaway_loc;
        l_packaway_loc_found := get_packaway_to_loc%FOUND;
        CLOSE get_packaway_to_loc;
        IF l_hotel_stg_loc_found OR l_packaway_loc_found THEN
          l_display_storage := TRUE;
        ELSE
          l_display_storage := FALSE;
        END IF;
        OPEN location_cursor;
        FETCH location_cursor
        INTO t_location_status,
          t_cycle_count_status,
          I_FROM_LOC_STORAGE_FLAG,
          t_unit_pick_flag;
        value_not_found := location_cursor%NOTFOUND;
        CLOSE location_cursor;
        IF value_not_found = TRUE OR (t_location_status <> 'OUT-SERVICE' AND t_cycle_count_status <> 'MM' AND t_unit_pick_flag = 'N') THEN
          OPEN item_id_cursor;
          FETCH item_id_cursor INTO I_ITEM_ID;
          CLOSE item_id_cursor;
          OPEN desc_cursor;
          FETCH desc_cursor INTO I_DESC;
          CLOSE desc_cursor;
          --Begin CR514 - Ross Display of UPS Codes
          OPEN get_child_ids;
          FETCH get_child_ids INTO L_temp;
          IF get_child_ids%FOUND THEN --checks to see if the container has children
            CLOSE get_child_ids;
            OPEN get_child_ids;
            OPEN c_check_mlp_has_hazmat;
            FETCH c_check_mlp_has_hazmat INTO l_hazmat;
            l_item_is_hazmat := c_check_mlp_has_hazmat%FOUND;
            CLOSE c_check_mlp_has_hazmat;
            OPEN c_check_mlp_has_mult_flag;
            FETCH c_check_mlp_has_mult_flag INTO l_dedicated;
            l_mlp_has_mult_flag := c_check_mlp_has_mult_flag%FOUND;
            CLOSE c_check_mlp_has_mult_flag;
            OPEN c_check_mlp_has_food;
            FETCH c_check_mlp_has_food INTO l_food;
            l_item_is_food := c_check_mlp_has_food%FOUND;
            CLOSE c_check_mlp_has_food;
            OPEN check_mlp_has_supplement;
            FETCH check_mlp_has_supplement INTO l_supplement;
            l_item_is_supplement := check_mlp_has_supplement%FOUND;
            CLOSE check_mlp_has_supplement;
            OPEN c_get_item_count;
            FETCH c_get_item_count INTO l_item_count;
            CLOSE c_get_item_count;
            IF (l_item_count      > 1 AND l_item_is_hazmat AND (l_item_is_food OR l_item_is_supplement)) OR (l_mlp_has_mult_flag) THEN
              I_STORAGE_CATEGORY := g_scp(I_FACILITY_ID, 'mixed_SKU');
            END IF;
            PAR3214.get_wh_id (I_FACILITY_ID, I_LOCATION_ID, l_from_wh, l_divert_flag, l_wh_found);
            LOOP
              FETCH get_child_ids INTO L_child_id; -- finds the child ids
              EXIT
            WHEN get_child_ids %NOTFOUND;
              OPEN get_child_ups_code;
              FETCH get_child_ups_code INTO L_ups_code;
              CLOSE get_child_ups_code;
              IF I_UPS_CODE    IS NULL THEN
                I_UPS_CODE     := L_ups_code;
              ELSIF I_UPS_CODE <> L_ups_code THEN
                I_UPS_CODE     := 'MULT';
              END IF;
              OPEN get_item_id (L_child_id);
              FETCH get_item_id INTO l_get_item;
              CLOSE get_item_id;
              l_storage_cat    := get_putaway_req.fn_get_item_spec_storage_name(I_FACILITY_ID, l_get_item);
              l_pref_zone      := get_putaway_req.fn_get_putaway_pref_zone(I_FACILITY_ID, l_get_item, l_from_wh, NULL, l_seq_id);
              l_dedicated_flag := get_putaway_req.fn_get_dedicated_zone_flag(I_FACILITY_ID, l_get_item);
              IF l_storage_cat IS NOT NULL AND l_seq_id IS NULL THEN
                OPEN c_get_min_seq_id;
                FETCH c_get_min_seq_id INTO l_seq;
                CLOSE c_get_min_seq_id;
              ELSE
                l_seq := l_seq_id;
              END IF;
              IF NVL(I_STORAGE_CATEGORY,'~') <> g_scp(I_FACILITY_ID, 'mixed_SKU') THEN
                IF I_STORAGE_CATEGORY        IS NULL THEN
                  I_STORAGE_CATEGORY         := l_storage_cat;
                  I_SEQ_ID                   := l_seq;
                  I_DEDICATED_ZONE_FLAG      := l_dedicated_flag;
                  I_ITEM_ID_WL               := l_get_item;
                ELSIF l_storage_cat          IS NOT NULL AND I_STORAGE_CATEGORY <> l_storage_cat THEN
                  IF I_SEQ_ID                IS NULL THEN
                    I_STORAGE_CATEGORY       := l_storage_cat;
                    I_SEQ_ID                 := l_seq;
                    I_DEDICATED_ZONE_FLAG    := l_dedicated_flag;
                    I_ITEM_ID_WL             := l_get_item;
                  ELSE
                    IF I_DEDICATED_ZONE_FLAG = 'Y' AND l_dedicated_flag = 'Y' AND l_seq_id < I_SEQ_ID THEN
                      I_STORAGE_CATEGORY    := l_storage_cat;
                      I_SEQ_ID              := l_seq;
                      I_DEDICATED_ZONE_FLAG := l_dedicated_flag;
                      I_ITEM_ID_WL          := l_get_item;
                    ELSIF l_dedicated_flag   = 'Y' THEN
                      I_STORAGE_CATEGORY    := l_storage_cat;
                      I_SEQ_ID              := l_seq;
                      I_DEDICATED_ZONE_FLAG := l_dedicated_flag;
                      I_ITEM_ID_WL          := l_get_item;
                    END IF;
                  END IF;
                END IF;
              END IF;
            END LOOP;
            IF l_display_storage     = FALSE THEN
              I_STORAGE_CATEGORY    := NULL;
              I_SEQ_ID              := NULL;
              I_DEDICATED_ZONE_FLAG := NULL;
              I_ITEM_ID_WL          := NULL;
            END IF;
            CLOSE get_child_ids;
          ELSE
            OPEN get_ups_count;
            FETCH get_ups_count INTO l_ups_count;
            CLOSE get_ups_count;
            IF l_ups_count > 1 THEN
              I_UPS_CODE  := 'MULT';
            ELSE
              OPEN get_ups_code;
              FETCH get_ups_code INTO I_UPS_CODE;
              CLOSE get_ups_code;
            END IF;
            OPEN c_check_mlp_has_hazmat;
            FETCH c_check_mlp_has_hazmat INTO l_hazmat;
            l_item_is_hazmat := c_check_mlp_has_hazmat%FOUND;
            CLOSE c_check_mlp_has_hazmat;
            OPEN c_check_mlp_has_mult_flag;
            FETCH c_check_mlp_has_mult_flag INTO l_dedicated;
            l_mlp_has_mult_flag := c_check_mlp_has_mult_flag%FOUND;
            CLOSE c_check_mlp_has_mult_flag;
            OPEN c_check_mlp_has_food;
            FETCH c_check_mlp_has_food INTO l_food;
            l_item_is_food := c_check_mlp_has_food%FOUND;
            CLOSE c_check_mlp_has_food;
            OPEN check_mlp_has_supplement;
            FETCH check_mlp_has_supplement INTO l_supplement;
            l_item_is_supplement := check_mlp_has_supplement%FOUND;
            CLOSE check_mlp_has_supplement;
            OPEN c_get_item_count;
            FETCH c_get_item_count INTO l_item_count;
            CLOSE c_get_item_count;
            IF (l_item_count      > 1 AND l_item_is_hazmat AND (l_item_is_food OR l_item_is_supplement)) OR (l_mlp_has_mult_flag) THEN
              I_STORAGE_CATEGORY := g_scp(I_FACILITY_ID, 'mixed_SKU');
            END IF;
            PAR3214.get_wh_id (I_FACILITY_ID, I_LOCATION_ID, l_from_wh, l_divert_flag, l_wh_found);
            FOR rec IN get_item_id (I_CONTAINER_ID)
            LOOP
              l_storage_cat    := get_putaway_req.fn_get_item_spec_storage_name(I_FACILITY_ID, rec.item_id);
              l_pref_zone      := get_putaway_req.fn_get_putaway_pref_zone(I_FACILITY_ID, rec.item_id, l_from_wh, NULL, l_seq_id);
              l_dedicated_flag := get_putaway_req.fn_get_dedicated_zone_flag(I_FACILITY_ID, rec.item_id);
              IF l_storage_cat IS NOT NULL AND l_seq_id IS NULL THEN
                OPEN c_get_min_seq_id;
                FETCH c_get_min_seq_id INTO l_seq;
                CLOSE c_get_min_seq_id;
              ELSE
                l_seq := l_seq_id;
              END IF;
              IF NVL(I_STORAGE_CATEGORY,'~') <> g_scp(I_FACILITY_ID, 'mixed_SKU') THEN
                IF I_STORAGE_CATEGORY        IS NULL THEN
                  I_STORAGE_CATEGORY         := l_storage_cat;
                  I_SEQ_ID                   := l_seq;
                  I_DEDICATED_ZONE_FLAG      := l_dedicated_flag;
                  I_ITEM_ID_WL               := rec.item_id;
                ELSIF l_storage_cat          IS NOT NULL AND I_STORAGE_CATEGORY <> l_storage_cat THEN
                  IF I_SEQ_ID                IS NULL THEN
                    I_STORAGE_CATEGORY       := l_storage_cat;
                    I_SEQ_ID                 := l_seq;
                    I_DEDICATED_ZONE_FLAG    := l_dedicated_flag;
                    I_ITEM_ID_WL             := rec.item_id;
                  ELSE
                    IF I_DEDICATED_ZONE_FLAG = 'Y' AND l_dedicated_flag = 'Y' AND l_seq_id < I_SEQ_ID THEN
                      I_STORAGE_CATEGORY    := l_storage_cat;
                      I_SEQ_ID              := l_seq;
                      I_DEDICATED_ZONE_FLAG := l_dedicated_flag;
                      I_ITEM_ID_WL          := rec.item_id;
                    ELSIF l_dedicated_flag   = 'Y' THEN
                      I_STORAGE_CATEGORY    := l_storage_cat;
                      I_SEQ_ID              := l_seq;
                      I_DEDICATED_ZONE_FLAG := l_dedicated_flag;
                      I_ITEM_ID_WL          := rec.item_id;
                    END IF;
                  END IF;
                END IF;
              END IF;
            END LOOP;
            IF l_display_storage     = FALSE THEN
              I_STORAGE_CATEGORY    := NULL;
              I_SEQ_ID              := NULL;
              I_DEDICATED_ZONE_FLAG := NULL;
              I_ITEM_ID_WL          := NULL;
            END IF;
          END IF;
          IF l_item_is_food THEN
            I_HAS_FOOD := 'Y';
          ELSE
            I_HAS_FOOD := 'N';
          END IF;
          IF l_item_is_supplement THEN
            I_HAS_SUPPLEMENT := 'Y';
          ELSE
            I_HAS_SUPPLEMENT := 'N';
          END IF;
          --End CR514 - Ross Display of UPS Codes
          --  update location table with
          --  in-transit setting for entered container location id.
          t_transit_loc := g_scp(I_FACILITY_ID, 'in_transit_loc');
          /* Begin Change R3 LPN Core Move   -  Container History   28-Aug-2006*/
          /* Code Added */
          --Deleting the previous entries of ROSS_EVENTCODE_USERID_TMP
          DELETE
          FROM ROSS_EVENTCODE_USERID_TMP;
          COMMIT;
          --Inserting data into ROSS_EVENTCODE_USERID_TMP
          INSERT
          INTO ROSS_EVENTCODE_USERID_TMP
            (
              EVENT_CD,
              USER_ID
            )
            VALUES
            (
              18,
              I_USER
            );
          COMMIT;
          /* Code Added */
          /* End Change R3 LPN Core Move   -  Container History   28-Aug-2006*/
          IF v_location_id(t_transit_loc, I_FACILITY_ID) = 'N' THEN
            RAISE no_trans_loc;
          END IF;
          UPDATE container
          SET location_id   = t_transit_loc
          WHERE facility_id = I_FACILITY_ID
          AND container_id  = I_CONTAINER_ID;
          COMMIT;
          -- BEGIN CR555
          OPEN get_LPN_qty;
          FETCH get_LPN_qty INTO LPN_QTY;
          CLOSE get_LPN_qty;
          IF LPN_QTY         = 0 THEN
            I_CONTAINER_QTY := 1;
          ELSE
            I_CONTAINER_QTY := LPN_QTY;
          END IF;
          -- END CR555
        ELSE
          RAISE inv_from_loc;
        END IF;
      ELSE
        RAISE inv_cont_stat;
      END IF;
    ELSE
      RAISE inv_cont;
    END IF;
    /*Start - JESD - CO60405*/
    IF l_wrt_loc    IS NOT NULL THEN
      I_TO_LOCATION := l_wrt_loc;
    END IF;
    /*End - JESD - CO60405*/
  EXCEPTION
  WHEN no_trans_loc THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'NO_INTRANS_LOC','E');
    V_CLEAR_BLOCK := 1;
  WHEN inv_from_loc THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_FROM_LOC','E');
    V_CLEAR_BLOCK := 1;
  WHEN inv_cont_stat THEN
    I_MSG_DISPLAY :=PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONT_STATUS','W');
  WHEN inv_cont THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'INV_CONTAINER','E');
    -- BEGIN CR555
  WHEN cont_waved THEN
    I_MSG_DISPLAY := PKG_COMMON_ADF.CALL_MSG_WINDOW(V_RETURN, 'CONT_WAVED');
    -- END CR555
  WHEN OTHERS THEN
    PKG_COMMON_ADF.CALL_LOG_ERROR_GUI(V_RETURN, I_MSG_DISPLAY, I_FACILITY_ID, I_USER, SQLCODE, 'PKG_HOTEL_PICKING_SMART_MOVE', 'DISPLAY_MOVE_BLOCK ', SQLERRM);
  END DISPLAY_MOVE_BLOCK;

END PKG_HOTEL_PICKING_SMART_MOVE;
/
