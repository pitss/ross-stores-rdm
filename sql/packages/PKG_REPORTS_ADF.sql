CREATE OR REPLACE PACKAGE PKG_REPORTS_ADF AS 

  procedure initialize(I_FACILITY_ID    IN VARCHAR2,
	                     I_LANGUAGE       IN VARCHAR2,
	                     I_REPORT_NAME    IN VARCHAR2,
                       I_CLIENT_TYPE    IN VARCHAR2,                   
                       I_HOST_NAME      OUT VARCHAR2,
                       I_PORT           OUT VARCHAR2, 
                       I_TIMEOUT        OUT NUMBER,
                       I_REPORT_SERVER  OUT VARCHAR2,
                       I_KEY_NAME       OUT VARCHAR2,
                       I_DESTYPE        OUT VARCHAR2,
                       I_DESNAME        OUT VARCHAR2,
                       I_DESFORMAT      OUT VARCHAR2,
                       I_ORIENTATION    OUT VARCHAR2,
                       I_PAGESIZE       OUT VARCHAR2,
                       I_REPORT_FILE    OUT VARCHAR2,
                       I_REPORT_TYPE    OUT VARCHAR2
                       ); 

	c_landscape_mode CONSTANT VARCHAR2(30) := 'landscape';
	c_portrait_mode CONSTANT VARCHAR2(30) := 'portrait';

END PKG_REPORTS_ADF;
/


CREATE OR REPLACE PACKAGE BODY PKG_REPORTS_ADF AS

/* Manually extracted from report_setup.pll by PITSS */

	function parameter_value(p_facility_id VARCHAR2,
	                         p_report_name VARCHAR2,
	                         p_parameter_name VARCHAR2)
	                         return VARCHAR2 is
		retval reports_parameters.parameter_value%TYPE := NULL;
    
	 CURSOR param_value_cursor(p_facility_id facility.facility_id%TYPE,
	                          p_report_name reports_parameters.report_name%TYPE,
	                          p_parameter_name reports_parameters.parameter_name%TYPE) IS
		SELECT lower(parameter_value)
		  FROM reports_parameters
		 WHERE LOWER(report_name) = LOWER(p_report_name)
		   AND LOWER(parameter_name) = LOWER(p_parameter_name)
		   AND facility_id = p_facility_id;    
    
	begin
		OPEN param_value_cursor(p_facility_id, p_report_name, p_parameter_name);
			FETCH param_value_cursor INTO retval;
			if param_value_cursor%NOTFOUND then
				retval := NULL;
			end if;
		CLOSE param_value_cursor;

		return retval;
	end;


  procedure initialize(I_FACILITY_ID    IN VARCHAR2,
	                     I_LANGUAGE       IN VARCHAR2,
	                     I_REPORT_NAME    IN VARCHAR2,
                       I_CLIENT_TYPE    IN VARCHAR2,                     
                       I_HOST_NAME      OUT VARCHAR2,
                       I_PORT           OUT VARCHAR2, 
                       I_TIMEOUT        OUT NUMBER,
                       I_REPORT_SERVER  OUT VARCHAR2,
                       I_KEY_NAME       OUT VARCHAR2,                       
                       I_DESTYPE        OUT VARCHAR2,
                       I_DESNAME        OUT VARCHAR2,
                       I_DESFORMAT      OUT VARCHAR2,
                       I_ORIENTATION    OUT VARCHAR2,
                       I_PAGESIZE       OUT VARCHAR2,                       
                       I_REPORT_FILE    OUT VARCHAR2,
                       I_REPORT_TYPE    OUT VARCHAR2
                       ) AS
                       
     v_oracle_sid varchar2(30);  
     
    CURSOR report_info_cursor(p_report_name report_name.report_name%TYPE) IS
      SELECT UPPER(report_type) report_type, LOWER(rdf_file) rdf_file
        FROM report_name
       WHERE report_name = I_report_name
         AND facility_id = I_facility_id; 
         
         
	  CURSOR report_dest_cursor IS
      SELECT DECODE(LOWER(I_orientation), c_landscape_mode, LANDSCAPE_FILENAME, PORTRAIT_FILENAME) filename,
             DECODE(LOWER(I_orientation), c_landscape_mode, LANDSCAPE_PAGESIZE, PORTRAIT_PAGESIZE) pagesize
        FROM report_dest
       WHERE facility_id = I_facility_id
         AND queue_name =  I_desname;         

      v_filename varchar2(50);
      v_pagesize varchar2(10);
     
  BEGIN
    
    -- get the report parameters from db tables and write to OUT params
    begin
    select 
      host_name,
      port_number,
      oracle_sid,
      report_server_id
    into
      i_host_name,
      i_port,
      v_oracle_sid,
      i_report_server
    from 
      repadmin.ross_report_Servers
    where rownum = 1;
    exception
      when no_data_found then
        raise_application_error(-20999, 'No entry in table repadmin.ross_report_servers.');
    end;



    I_HOST_NAME      := g_scp(I_FACILITY_ID, 'RptSvr_host');
    if g_scp(I_FACILITY_ID, 'RptSvr_domain') not in ('NULL', 'ERROR') then
      I_HOST_NAME := I_HOST_NAME || g_scp(I_FACILITY_ID, 'RptSvr_domain');
    end if;
    
    I_PORT           := g_scp(I_FACILITY_ID, 'RptSvr_port');
    
    if g_scp(I_FACILITY_ID, 'RptSvr_timeout') <> 'ERROR' then 
       I_TIMEOUT        := g_scp(I_FACILITY_ID, 'RptSvr_timeout');
    else 
       I_TIMEOUT        := 10000;
    end if;
    
    I_REPORT_SERVER  := 'RptSvr_'||g_scp(I_FACILITY_ID, 'RptSvr_suffix');
    if g_scp(I_FACILITY_ID, 'RptSvr_suffix2') not in ('NULL', 'ERROR') then
      I_REPORT_SERVER := I_REPORT_SERVER || g_scp(I_FACILITY_ID, 'RptSvr_suffix2');
    end if;
    
    
    I_KEY_NAME       := g_scp(I_FACILITY_ID, 'RptSvr_keyname');    
    
    
    if I_client_type = 'WEB' then
      I_DESTYPE := /*'FILE'; should be cache for web reports*/  'CACHE';
    else  -- handheld etc
      I_DESTYPE := parameter_value(I_facility_id, I_report_name, 'destype');
    end if;

   I_DESNAME     := parameter_value(I_facility_id, I_report_name, 'desname'); 
   I_DESFORMAT   := parameter_value(I_facility_id, I_report_name, 'desformat');
   I_ORIENTATION := parameter_value(I_facility_id, I_report_name, 'orientation');
   I_PAGESIZE    := parameter_value(I_facility_id, I_report_name, 'pagesize');


   /*if I_QUEUE_DEF is not null then
     I_DESNAME := g_scp(I_facility_id, I_QUEUE_DEF);
     if I_DESNAME = 'ERROR' then
       raise_application_error(-20999, 'No valid queue.');
      end if; 
   end if;*/

		OPEN report_info_cursor(I_report_name);
			FETCH report_info_cursor INTO I_REPORT_TYPE, I_REPORT_FILE;
			if report_info_cursor%NOTFOUND then
				I_REPORT_FILE := NULL;
				I_REPORT_TYPE := NULL;
			end if;
		CLOSE report_info_cursor;   
    
    if I_REPORT_FILE is NULL then
       I_REPORT_FILE := I_REPORT_NAME;
    end if;

		/*OPEN report_dest_cursor ;
			FETCH report_dest_cursor INTO v_filename, v_pagesize;
			if report_dest_cursor%FOUND then
         this applies only for char mode, we don't have that anymore, so this may be obsolete?
      end if;
		CLOSE report_dest_cursor;*/

    
  END initialize;

END PKG_REPORTS_ADF;
/
