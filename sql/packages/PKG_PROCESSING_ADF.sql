CREATE OR REPLACE PACKAGE PKG_PROCESSING_ADF 
IS
    /*
    *   WIP_BLOCK.CONTAINER_ID.KEY-NEXT-ITEM trigger contains several calls to DB functions, which have
    *   a BOOLEAN OUT parameter. As it is not possible to pass boolean parameters via JDBC interface,
    *   this wrapper functions "hides" the BOOLEANs behind a JDBC-compatible interface and as a side effect
    *   also reduces the number of DB-round-trips needed to validate the container ID.
    *
    *   RETURNS: NULL if container_id_in passes all checks, error message ID otherwise 
    */
    FUNCTION CHECK_CONTAINER(facility_id_in IN VARCHAR2, container_id_in IN VARCHAR2) RETURN VARCHAR2;
    
    /*
    *   Wrapper for the stored function WORK_PROCESSING_PKG.VALIDATE_MLP (uses BOOLEAN as return value)
    */
    FUNCTION VALIDATE_MLP (i_facility_id           IN     facility.facility_id%TYPE,
                            io_mlp                  IN OUT container.container_id%TYPE,
                            o_item_id               OUT    item_master.item_id%TYPE,
                            o_mlp_total_units       OUT    work_activity_audit.units_worked%TYPE,
                            o_mlp_total_cont        OUT    work_activity_audit.container_worked%TYPE,
                            o_worked_units          OUT    work_activity_audit.units_worked%TYPE,
                            o_worked_cont           OUT    work_activity_audit.container_worked%TYPE,
                            o_remaining_units       OUT    work_activity_audit.units_remaining%TYPE,
                            o_remaining_cont        OUT    work_activity_audit.container_remaining%TYPE,
                            o_prev_work_status      OUT    work_activity_audit.status%TYPE,
                            o_ppk_qty               OUT    work_activity_audit.prepack_qty%TYPE,
                            o_nested_qty            OUT    work_activity_audit.nested_qty%TYPE,
                            o_zone                  OUT    location.zone%TYPE,
                            o_wh_id                 OUT    wh.wh_id%TYPE,
                            o_dc_id                 OUT    dc.dc_id%TYPE,
                            o_location_id           OUT    location.location_id%TYPE,
                            i_activity_cd           IN     work_activity_audit.activity_code%TYPE,
                            i_calling_form          IN     VARCHAR2,
                            i_user                  IN     dms_user.user_id%TYPE,
                            o_stand_alone_cnt       OUT    VARCHAR2,
                            o_non_unq_cnt_qty       OUT    VARCHAR2,
                            o_prev_operator_id      OUT    work_activity_audit.operator%TYPE,
                            o_supervisor_id         OUT    work_activity_audit.supervisor%TYPE,
                            o_unq_qty               OUT    VARCHAR2,
                            o_error_msg             OUT    VARCHAR2,
                            o_error_msg_typ         OUT    VARCHAR2) return INT; 
         
    /*
    *   Wrapper for the stored function WORK_PROCESSING_PKG.START_WORK (uses BOOLEAN as return value)
    */                            
    FUNCTION START_WORK (i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT;
      
    /*
    *   Wrapper for the stored function WORK_PROCESSING_PKG.RESUME_WORK (uses BOOLEAN as return value)
    */                                                        
    FUNCTION RESUME_WORK (i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT;
           
    /*
    *   Wrapper for the stored function WORK_PROCESSING_PKG.PAUSE_WORK (uses BOOLEAN as return value)
    */                                                                                    
    FUNCTION PAUSE_WORK(i_facility_id         IN facility.facility_id%TYPE,
                        i_container_id        IN container.container_id%TYPE,
                        i_item_id             IN item_master.item_id%TYPE,
                        i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                        i_prev_status         IN work_activity_audit.status%TYPE,
                        i_next_status         IN work_activity_audit.status%TYPE,
                        i_dc_id               IN work_activity_audit.dc_id%TYPE,
                        i_wh_id               IN work_activity_audit.wh_id%TYPE,
                        i_location_id         IN work_activity_audit.location_id%TYPE,
                        i_zone                IN location.zone%TYPE,
                        i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                        i_operator_id         IN work_activity_audit.operator%TYPE,
                        i_units_worked        IN work_activity_audit.units_worked%TYPE,
                        i_container_worked    IN work_activity_audit.container_worked%TYPE,
                        i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                        i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                        i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                        i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                        i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                        i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                        o_error_msg           OUT user_message.message_code%TYPE,
                        o_error_msg_typ       OUT user_message.message_type%TYPE) return INT;
               
    /*
    *   Wrapper for the stored function WORK_PROCESSING_PKG.COMPLETE_WORK (uses BOOLEAN as return value)
    */                                                                                                            
    FUNCTION COMPLETE_WORK(i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT;
               
    /*
    *   Wrapper for the stored function WORK_PROCESSING_PKG.PROCESS_PV_DONE (uses BOOLEAN as return value)
    */                                                                                                                                        
    FUNCTION PROCESS_PV_DONE (i_facility_id    IN  facility.facility_id%TYPE,
                                i_container_id   IN  container.container_id%TYPE,
                                i_item_id             IN item_master.item_id%TYPE,
                                i_pv_codes       IN  container_pay_variable.pv_code%TYPE,
                                i_zone           IN  location.zone%TYPE,
                                i_activity_cd    IN  work_activity_audit.activity_code%TYPE,
                                i_operator_id    IN  work_activity_audit.operator%TYPE,
                                i_wh_id          IN  wh.wh_id%TYPE,
                                o_error_msg      OUT user_message.message_code%TYPE,
                                o_error_msg_typ  OUT user_message.message_type%TYPE,
								i_commit		 IN VARCHAR2) return INT;                                                                                                            
                                                       
	PROCEDURE ROLLBACK_TO_WORKPV_SAVEPOINT;
	
	PROCEDURE UPDATE_WORK_ACTIVITY_AUDIT(i_facility_id    IN  work_activity_audit.facility_id%TYPE,
									 i_container_id   IN  work_activity_audit.container_id%TYPE,
                            		 i_item_id        IN  work_activity_audit.item_id%TYPE,
                            		 i_supervisor_id  IN  work_activity_audit.supervisor%TYPE,
                            		 i_activity_cd    IN  work_activity_audit.activity_code%TYPE,
                            		 i_operator_id    IN  work_activity_audit.operator%TYPE);
									 
	FUNCTION GET_LPN (I_FACILITY_ID VARCHAR2, I_MLP VARCHAR2) RETURN VARCHAR2;
END PKG_PROCESSING_ADF;
/


CREATE OR REPLACE PACKAGE BODY PKG_PROCESSING_ADF 
IS

    FUNCTION CHECK_CONTAINER(facility_id_in IN VARCHAR2, container_id_in IN VARCHAR2) RETURN VARCHAR2
    IS
        l_result BOOLEAN;
    BEGIN
        IF v_container_ID(container_id_in, facility_id_in) != 'Y' THEN
            RETURN 'INV_CONTAINER';
        END IF;
        
        --Call the function v_container_dummy_sku to see if the dummy sku trouble code is associated.
        IF v_container_dummy_sku(facility_id_in, container_id_in, l_result) = 'SUCCESS' THEN        
            IF l_result THEN
                RETURN 'APP_DUMMY_SKU';
            END IF;
        ELSE
            -- An exception occured
            RETURN 'INV_CONTAINER';            
        END IF;
        
        --Call the function v_container_inbound_trouble to see if an inbound trouble code is associated.
        IF v_container_inbound_trouble(facility_id_in, container_id_in, l_result) = 'SUCCESS' THEN        
            IF l_result THEN
                RETURN 'INB_TRBL_EXISTS';
            END IF;
        ELSE
            -- An exception occured
            RETURN 'INV_CONTAINER';
        END IF;   
        
        RETURN NULL;
         
    END CHECK_CONTAINER;
    
    FUNCTION VALIDATE_MLP (i_facility_id           IN     facility.facility_id%TYPE,
                                io_mlp                  IN OUT container.container_id%TYPE,
                                o_item_id               OUT    item_master.item_id%TYPE,
                                o_mlp_total_units       OUT    work_activity_audit.units_worked%TYPE,
                                o_mlp_total_cont        OUT    work_activity_audit.container_worked%TYPE,
                                o_worked_units          OUT    work_activity_audit.units_worked%TYPE,
                                o_worked_cont           OUT    work_activity_audit.container_worked%TYPE,
                                o_remaining_units       OUT    work_activity_audit.units_remaining%TYPE,
                                o_remaining_cont        OUT    work_activity_audit.container_remaining%TYPE,
                                o_prev_work_status      OUT    work_activity_audit.status%TYPE,
                                o_ppk_qty               OUT    work_activity_audit.prepack_qty%TYPE,
                                o_nested_qty            OUT    work_activity_audit.nested_qty%TYPE,
                                o_zone                  OUT    location.zone%TYPE,
                                o_wh_id                 OUT    wh.wh_id%TYPE,
                                o_dc_id                 OUT    dc.dc_id%TYPE,
                                o_location_id           OUT    location.location_id%TYPE,
                                i_activity_cd           IN     work_activity_audit.activity_code%TYPE,
                                i_calling_form          IN     VARCHAR2,
                                i_user                  IN     dms_user.user_id%TYPE,
                                o_stand_alone_cnt       OUT    VARCHAR2,
                                o_non_unq_cnt_qty       OUT    VARCHAR2,
                                o_prev_operator_id      OUT    work_activity_audit.operator%TYPE,
                                o_supervisor_id         OUT    work_activity_audit.supervisor%TYPE,
                                o_unq_qty               OUT    VARCHAR2,
                                o_error_msg             OUT    VARCHAR2,
                                o_error_msg_typ         OUT    VARCHAR2) return INT
IS
    l_result BOOLEAN;
BEGIN
    l_result := WORK_PROCESSING_PKG.VALIDATE_MLP(i_facility_id, io_mlp, o_item_id, o_mlp_total_units,
                o_mlp_total_cont, o_worked_units, o_worked_cont, o_remaining_units, o_remaining_cont,
                o_prev_work_status, o_ppk_qty, o_nested_qty, o_zone, o_wh_id, o_dc_id,
                o_location_id, i_activity_cd, i_calling_form, i_user,
                o_stand_alone_cnt, o_non_unq_cnt_qty, o_prev_operator_id, o_supervisor_id, o_unq_qty,
                o_error_msg, o_error_msg_typ);
    IF l_result = TRUE THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF; 
END VALIDATE_MLP; 

  FUNCTION START_WORK (i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT
IS
    l_result BOOLEAN;
BEGIN
    l_result := WORK_PROCESSING_PKG.START_WORK(i_facility_id, i_container_id, i_item_id, i_activity_cd,
                i_prev_status, i_next_status, i_dc_id, i_wh_id, i_location_id, i_zone,
                i_supervisor_id, i_operator_id, i_units_worked, i_container_worked,
                i_total_unit_qty, i_num_lpn, i_units_remaining, i_container_remaining,
                i_prepack_qty, i_nested_qty, o_error_msg, o_error_msg_typ);
    IF l_result = TRUE THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;     
END START_WORK;

  FUNCTION RESUME_WORK (i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT
IS
    l_result BOOLEAN;
BEGIN
    l_result := WORK_PROCESSING_PKG.RESUME_WORK(i_facility_id, i_container_id, i_item_id, i_activity_cd,
                i_prev_status, i_next_status, i_dc_id, i_wh_id, i_location_id, i_zone,
                i_supervisor_id, i_operator_id, i_units_worked, i_container_worked,
                i_total_unit_qty, i_num_lpn, i_units_remaining, i_container_remaining,
                i_prepack_qty, i_nested_qty, o_error_msg, o_error_msg_typ);
    IF l_result = TRUE THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;     
END RESUME_WORK;                

  FUNCTION PAUSE_WORK (i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT
IS
    l_result BOOLEAN;
BEGIN
    l_result := WORK_PROCESSING_PKG.PAUSE_WORK(i_facility_id, i_container_id, i_item_id, i_activity_cd,
                i_prev_status, i_next_status, i_dc_id, i_wh_id, i_location_id, i_zone,
                i_supervisor_id, i_operator_id, i_units_worked, i_container_worked,
                i_total_unit_qty, i_num_lpn, i_units_remaining, i_container_remaining,
                i_prepack_qty, i_nested_qty, o_error_msg, o_error_msg_typ);
    IF l_result = TRUE THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;     
END PAUSE_WORK;                

  FUNCTION COMPLETE_WORK (i_facility_id         IN facility.facility_id%TYPE,
                            i_container_id        IN container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_activity_cd         IN work_activity_audit.activity_code%TYPE,
                            i_prev_status         IN work_activity_audit.status%TYPE,
                            i_next_status         IN work_activity_audit.status%TYPE,
                            i_dc_id               IN work_activity_audit.dc_id%TYPE,
                            i_wh_id               IN work_activity_audit.wh_id%TYPE,
                            i_location_id         IN work_activity_audit.location_id%TYPE,
                            i_zone                IN location.zone%TYPE,
                            i_supervisor_id       IN work_activity_audit.supervisor%TYPE,
                            i_operator_id         IN work_activity_audit.operator%TYPE,
                            i_units_worked        IN work_activity_audit.units_worked%TYPE,
                            i_container_worked    IN work_activity_audit.container_worked%TYPE,
                            i_total_unit_qty      IN work_activity_audit.total_unit_qty%TYPE,
                            i_num_lpn             IN work_activity_audit.num_lpn%TYPE,
                            i_units_remaining     IN work_activity_audit.units_remaining%TYPE,
                            i_container_remaining IN work_activity_audit.container_remaining%TYPE,
                            i_prepack_qty         IN work_activity_audit.prepack_qty%TYPE,
                            i_nested_qty          IN work_activity_audit.nested_qty%TYPE,
                            o_error_msg           OUT user_message.message_code%TYPE,
                            o_error_msg_typ       OUT user_message.message_type%TYPE) return INT
IS
    l_result BOOLEAN;
BEGIN
    l_result := WORK_PROCESSING_PKG.COMPLETE_WORK(i_facility_id, i_container_id, i_item_id, i_activity_cd,
                i_prev_status, i_next_status, i_dc_id, i_wh_id, i_location_id, i_zone,
                i_supervisor_id, i_operator_id, i_units_worked, i_container_worked,
                i_total_unit_qty, i_num_lpn, i_units_remaining, i_container_remaining,
                i_prepack_qty, i_nested_qty, o_error_msg, o_error_msg_typ);
    IF l_result = TRUE THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;     
END COMPLETE_WORK;

FUNCTION PROCESS_PV_DONE (i_facility_id    IN  facility.facility_id%TYPE,
                            i_container_id   IN  container.container_id%TYPE,
                            i_item_id             IN item_master.item_id%TYPE,
                            i_pv_codes       IN  container_pay_variable.pv_code%TYPE,
                            i_zone           IN  location.zone%TYPE,
                            i_activity_cd    IN  work_activity_audit.activity_code%TYPE,
                            i_operator_id    IN  work_activity_audit.operator%TYPE,
                            i_wh_id          IN  wh.wh_id%TYPE,
                            o_error_msg      OUT user_message.message_code%TYPE,
                            o_error_msg_typ  OUT user_message.message_type%TYPE,
							i_commit		 IN VARCHAR2) return INT
IS
      t_add_pv_type_cd         work_processing_pkg.pv_type_cd_tab;
      t_add_pv_code            work_processing_pkg.pv_code_tab;
      t_add_group_desc         work_processing_pkg.group_desc_tab;
      t_add_maximum_val        work_processing_pkg.maximum_vaL_tab;
      t_assigned_pv_type_cd    work_processing_pkg.pv_type_cd_tab;
      t_assigned_pv_code       work_processing_pkg.pv_code_tab;
      t_assigned_group_desc    work_processing_pkg.group_desc_tab;

      L_dummy                  VARCHAR2(1);
      L_position               NUMBER;
      L_codes_pv_string        VARCHAR2(200) := Null;
      L_pv_string              VARCHAR2(200) := Null;
      L_current_ascii          NUMBER;
      L_alpha_numeric_ctr      NUMBER := 0;
      L_del_count              NUMBER := 0;
      L_code                   VARCHAR2(90);
      L_pv_type_cd             PAY_VARIABLE_CODE.PV_TYPE_CD%TYPE;
      L_group_desc             PAY_VARIABLE_CODE.GROUP_DESC%TYPE;
      L_maximum_val            PAY_VARIABLE_TYPE.MAXIMUM_VAL%TYPE;
      L_units_per_inner_ctn    CONTAINER_PAY_VARIABLE_HISTORY.UNITS_PER_INNER_CTN%TYPE;
      L_appt_nbr               CONTAINER.APPT_NBR%TYPE;
      L_po_nbr                 CONTAINER.PO_NBR%TYPE;
      L_container_status       CONTAINER.CONTAINER_STATUS%TYPE;
      L_action_type            CONTAINER_PAY_VARIABLE_HISTORY.ACTION_TYPE%TYPE;

      add_index                BINARY_INTEGER;
      assigned_index           BINARY_INTEGER;
      codes_missing            BOOLEAN;

      code_count               NUMBER;
      L_receipt_date           CONTAINER.RECEIPT_DATE%TYPE;
      L_action_ts              CONTAINER.RECEIPT_DATE%TYPE;

      cursor get_receipt_date is
      select receipt_date
        from container
       where facility_id = i_facility_id
         and container_id = i_container_id;

      cursor get_cnt_rcp_date is
      select a.receipt_date, b.act
        from
            (select container_id,receipt_date,max(ACTION_TS)act
               from container_history
              where facility_id = i_facility_id
                and container_id = i_container_id
                and table_name = 'C'
                and RECEIPT_DATE is not null
               group by container_id,receipt_date) a ,
            (select container_id,max(action_ts) act
               from container_history
              where facility_id = i_facility_id
                and container_id = i_container_id
                and table_name = 'C'
                and Action_type = 'A'
               group by container_id ) b
      where a.container_id(+) = b.container_id
        and a.act(+) >= b.act;

      cursor get_container_data is
      select appt_nbr,po_nbr,container_status
        from container
       where facility_id = i_facility_id
        and container_id = i_container_id;

      cursor check_cph_data is
      select 'X'
        from container_pay_variable_history
       where facility_id = i_facility_id
         and activity_cd = i_activity_cd
         and container_id = i_container_id
         and item_id = i_item_id
         and NVL(appt_nbr,'0') = NVL(L_appt_nbr,'0')
         and NVL(po_nbr,'~') = NVL(L_po_nbr,'~');

      cursor get_code_details (pv_code_in IN VARCHAR2) is
      select pvc.pv_type_cd, pvc.group_desc, pvt.maximum_val
        from pay_variable_code pvc, pay_variable_type pvt
       where pvt.facility_id = i_facility_id
         and pvc.facility_id = pvt.facility_id
         and pvt.wh_id = i_wh_id
         and pvc.wh_id = pvt.wh_id
         and pvt.activity_cd = i_activity_cd
         and pvc.activity_cd = pvt.activity_cd
         and pvc.pv_type_cd = pvt.pv_type_cd
         and pvc.pv_code = pv_code_in
         and pvc.active_flag = 'Y'
         and (EXISTS (select 'x'
                        from pay_variable_zone
                       where facility_id = i_facility_id
                         and activity_cd = pvc.activity_cd
                         and pv_code = pv_code_in
                         and zone = i_zone) or
               NOT EXISTS (select 'x'
                             from pay_variable_zone pvz,zone z
                            where pvz.facility_id = i_facility_id
                              and z.facility_id = pvz.facility_id
                              and pvz.activity_cd = pvc.activity_cd
                              and pvz.pv_code = pv_code_in
                              and pvz.zone = z.zone
                              and z.wh_id = i_wh_id));

      cursor check_diff_activity (pv_code_in IN VARCHAR2) is
      select 'x'
        from pay_variable_code pvc
       where pvc.facility_id = i_facility_id
         and pvc.wh_id = i_wh_id
         and pvc.activity_cd <> i_activity_cd
         and pvc.pv_code = pv_code_in
         and pvc.active_flag = 'Y'
         and (EXISTS (select 'x'
                        from pay_variable_zone
                       where facility_id = i_facility_id
                         and activity_cd = pvc.activity_cd
                         and pv_code = pv_code_in
                         and zone = i_zone) or
               NOT EXISTS (select 'x'
                             from pay_variable_zone pvz,zone z
                            where pvz.facility_id = i_facility_id
                              and z.facility_id = pvz.facility_id
                              and pvz.activity_cd = pvc.activity_cd
                              and pvz.pv_code = pv_code_in
                              and pvz.zone = z.zone
                              and z.wh_id = i_wh_id))
         and ROWNUM < 2;

   BEGIN
	  SAVEPOINT WORKPV;
	
      o_error_msg := NULL;
      o_error_msg_typ := NULL;
      --- Check if any invalid characters exist.
      L_codes_pv_string :=  replace((replace(i_pv_codes,'"','')),chr(10),' ');
      L_position := 1;

      WHILE LENGTH (L_codes_pv_string) >= L_position LOOP
         L_current_ascii := ASCII (SUBSTR (L_codes_pv_string,L_position,1));

          if (L_current_ascii NOT BETWEEN 48 and 57) and
             (L_current_ascii NOT BETWEEN 65 and 90) and
             (L_current_ascii NOT BETWEEN 97 and 122) then -- Non alpha-numeric.

             if L_current_ascii <> 32 and L_current_ascii <> 44 then -- Not a space or comma.
                o_error_msg := 'INV_CODE_FORMAT';
                o_error_msg_typ := 'E';
                return 0;
             end if;

          else -- Alpha-numeric
             L_alpha_numeric_ctr := L_alpha_numeric_ctr + 1;
          end if;
          L_position := L_position + 1;
      END LOOP;

      if L_alpha_numeric_ctr < 1 then -- spaces only.
         o_error_msg := 'PARTIAL_ENTRY';
         o_error_msg_typ := 'E';
         return 0;
      end if;

      --- Extract the codes from the string.
      L_codes_pv_string := 'START ' || RTRIM(LTRIM(L_codes_pv_string,' '),' ') || ' END';

      add_index := 1;
      WHILE INSTR(RTRIM(LTRIM(L_codes_pv_string,' '),' '),' ') > 0 LOOP

         L_code := SUBSTR(RTRIM(LTRIM(L_codes_pv_string,' '),' '),1,INSTR(RTRIM(LTRIM(L_codes_pv_string,' '),' '),' ')-1);
         L_codes_pv_string := SUBSTR(RTRIM(LTRIM(L_codes_pv_string,' '),' '), INSTR(RTRIM(LTRIM(L_codes_pv_string,' '),' '),' ')+1);

         if L_code <> 'START' then

            open get_code_details(L_code);
            fetch get_code_details INTO L_pv_type_cd, L_group_desc, L_maximum_val;
            if get_code_details%FOUND then

               t_add_pv_type_cd(add_index) := L_pv_type_cd;
               t_add_pv_code(add_index) := L_code;
               t_add_group_desc(add_index) := L_group_desc;
               t_add_maximum_val(add_index) := L_maximum_val;
               add_index := add_index + 1;

            else

               close get_code_details;

               open check_diff_activity(L_code);
               fetch check_diff_activity INTO L_dummy;
               if check_diff_activity%FOUND then

                  close check_diff_activity;
                  o_error_msg := 'DIFF_ACTV_CD';
                  o_error_msg_typ := 'E';
                  return 0;

               else

                  close check_diff_activity;
                  o_error_msg := 'INV_CODE_INPUT';
                  o_error_msg_typ := 'E';
                  return 0;

               end if;
               close check_diff_activity;

            end if;
            close get_code_details;

         end if;

      END LOOP;
      --- Check enterd codes.
      For j IN t_add_pv_code.FIRST .. t_add_pv_code.LAST LOOP

         code_count := 1.0; -- Start of type code count.

         For k IN t_add_pv_code.FIRST .. t_add_pv_code.LAST LOOP

            if j <> k then

               if t_add_pv_code(j) = t_add_pv_code(k) then -- duplicate code
                  o_error_msg := 'DUP_CODE_INPUT';
                  o_error_msg_typ := 'E';
                  return 0;
               end if;

               if t_add_group_desc(j) = t_add_group_desc(k) then -- Same group.
                  o_error_msg := 'SAME_GROUP';
                  o_error_msg_typ := 'E';
                  return 0;
               end if;

               if t_add_pv_type_cd(j) = t_add_pv_type_cd(k) then -- Same type.

                  code_count := code_count + 1.0;

                  if code_count > t_add_maximum_val(j) then -- Maximum allowable number of codes exceeded.
                     o_error_msg := 'MAX_CODES';
                     o_error_msg_typ := 'E';
                     return 0;
                  end if;

               end if;

            end if;

         END LOOP;

      END LOOP;
      --- Insert into container_pay_variable table.
      open get_receipt_date;
      fetch get_receipt_date INTO L_receipt_date;
      if get_receipt_date%NOTFOUND then
         L_receipt_date := TO_DATE ('01/01/2001','MM/DD/YYYY');
      else
         if L_receipt_date is NUll then
	    open get_cnt_rcp_date;
            fetch get_cnt_rcp_date INTO L_receipt_date, L_action_ts;
	    close get_cnt_rcp_date;
	    if L_receipt_date is NULL then
	       L_receipt_date := L_action_ts;
	    end if;
         end if;
      end if;
      close get_receipt_date;

      For n IN t_add_pv_code.FIRST .. t_add_pv_code.LAST LOOP

         if L_del_Count = 0 and i_commit = 'Y' then
           Delete from container_pay_variable
            where container_id = i_container_id
              and facility_id = i_facility_id
              and receipt_date = L_receipt_date
              and activity_cd = i_activity_cd;
         end if;
		 
		 IF i_commit = 'Y' then
			 INSERT INTO container_pay_variable (facility_id,
														 container_id,
														 receipt_date,
														 activity_cd,
														 pv_code,
														 action_ts,
														 units_per_inner_ctn,
														 user_id
														)
												 VALUES (i_facility_id,
														 i_container_id,
														 L_receipt_date,
														 i_activity_cd,
														 t_add_pv_code(n),
														 sysdate,
														 NULL,
														 i_operator_id);
		 END IF;

         L_del_Count := 1;
      END LOOP;

      if L_del_Count = 1 then
          L_pv_string := SUBSTR(work_processing_pkg.get_pay_var_string(i_facility_id,i_container_id,i_activity_cd),1,200);

          codes_missing := work_processing_pkg.check_pv_codes_required (i_facility_id,i_container_id,i_activity_cd,i_zone,i_wh_id);
	           ---
		IF codes_missing THEN
             o_error_msg := 'CODES_REQUIRED';
             o_error_msg_typ := 'E';
             rollback;
             return 0;
          END IF;

          open  get_container_data;
          fetch get_container_data INTO L_appt_nbr,L_po_nbr,L_container_status;
          close get_container_data;

          open check_cph_data;
          fetch check_cph_data INTO L_dummy;
          if check_cph_data%FOUND then
            l_action_type := 'M';
          else
            l_action_type := 'A';
          end if;
          close check_cph_data;
               ---
		  IF i_commit = 'Y' then
               INSERT INTO  container_pay_variable_history
                           (facility_id,
                            container_id,
                            action_ts,
                            appt_nbr,
                            po_nbr,
                            item_id,
                            activity_cd,
                            screen_name,
                            mark_status,
                            container_status,
                            pay_variable_codes,
                            units_per_inner_ctn,
                            modified_by_user_id,
                            authorized_by_user_id,
                            action_type
                           )
                    VALUES (i_facility_id,
                            i_container_id,
                            sysdate,
                            L_appt_nbr,
                            L_po_nbr,
                            i_item_id,
                            i_activity_cd,
                            'HH_WORK_PROCESSING_S',
                            NUll,
                            L_container_status,
                            L_pv_string,
                            Null,
                            i_operator_id,
                            Null,
                            l_action_type
                     );

                update work_activity_audit
                   set pay_variable_codes = L_pv_string
                 where container_id = i_container_id
                   and facility_id = i_facility_id
                   and activity_code = i_activity_cd
                   and operator = i_operator_id
                   and item_id = i_item_id;
		  END IF;
      end if;
      L_codes_pv_string := Null;
      L_pv_string := NULL;
	  
	  if i_commit = 'Y' then
		commit;
	  end if;

   return 1;
   EXCEPTION
      when OTHERS then
         rollback;
         log_oracle_error (SQLCODE, 'WORK_PROCESSING_PKG.PROCESS_PV_DONE', SQLERRM);
         return 0;
END PROCESS_PV_DONE;                                                                                                                                            

PROCEDURE ROLLBACK_TO_WORKPV_SAVEPOINT IS
BEGIN
 ROLLBACK TO WORKPV;
EXCEPTION
  WHEN OTHERS THEN
    null;
END ROLLBACK_TO_WORKPV_SAVEPOINT;

PROCEDURE UPDATE_WORK_ACTIVITY_AUDIT(i_facility_id    IN  work_activity_audit.facility_id%TYPE,
									 i_container_id   IN  work_activity_audit.container_id%TYPE,
                            		 i_item_id        IN  work_activity_audit.item_id%TYPE,
                            		 i_supervisor_id  IN  work_activity_audit.supervisor%TYPE,
                            		 i_activity_cd    IN  work_activity_audit.activity_code%TYPE,
                            		 i_operator_id    IN  work_activity_audit.operator%TYPE) IS
BEGIN
	update work_activity_audit
    set supervisor = i_supervisor_id
    where container_id = i_container_id
    and facility_id = i_facility_id
    and activity_code = i_activity_cd
    and operator = i_operator_id
    and item_id = i_item_id;
	
	COMMIT;
END UPDATE_WORK_ACTIVITY_AUDIT;

FUNCTION GET_LPN (I_FACILITY_ID VARCHAR2, I_MLP VARCHAR2) RETURN VARCHAR2 IS
CURSOR get_lpn_of_mlp IS
SELECT cw.container_id
FROM container c, container_wip cw  
WHERE c.facility_id = I_FACILITY_ID  
      AND cw.facility_id = c.facility_id  
      AND c.master_container_id = I_MLP  
      AND c.container_id = cw.container_id  
      AND cw.end_ts IS NULL
ORDER BY cw.wip_seq_nbr;

v_lpn					container_wip.container_id%TYPE;
value_found				BOOLEAN;
BEGIN
	OPEN get_lpn_of_mlp;
	FETCH get_lpn_of_mlp INTO v_lpn;
	value_found := get_lpn_of_mlp%FOUND;
	CLOSE get_lpn_of_mlp;
	
	if not value_found then
		v_lpn := null;
	end if;
	return v_lpn;
END;
END PKG_PROCESSING_ADF;
/
