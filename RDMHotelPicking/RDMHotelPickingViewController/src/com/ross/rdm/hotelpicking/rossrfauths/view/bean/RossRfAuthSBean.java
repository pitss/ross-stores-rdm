package com.ross.rdm.hotelpicking.rossrfauths.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class RossRfAuthSBean {

    private static final String USER_ID = "USER_ID";
    private static final String PASSWORD = "PASSWORD";
    private String focus;

    public void initTaskFlow() {
        this.initGlobalVariablesRossRfAuth();
        this.initHhRossRfAuthWorkVariables();
        this.setFocus(USER_ID);
    }

    private void initGlobalVariablesRossRfAuth() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesRossRfAuth");
        oper.execute();
    }

    private void initHhRossRfAuthWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setRossRfAuthWorkVariables");
        oper.execute();
    }

    public Boolean getDisabledUserId() {
        return !USER_ID.equals(focus);
    }

    public Boolean getDisabledPassword() {
        return !PASSWORD.equals(focus);
    }

    public void focusUserId() {
        setFocus(USER_ID);
    }

    public void focusPassword() {
        setFocus(PASSWORD);
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getFocus() {
        return focus;
    }
}
