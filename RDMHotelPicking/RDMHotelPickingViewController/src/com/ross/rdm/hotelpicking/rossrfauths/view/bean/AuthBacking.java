package com.ross.rdm.hotelpicking.rossrfauths.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Auth.jspx
// ---
// ---------------------------------------------------------------------
public class AuthBacking extends RDMHotelPickingBackingBean {
    private RichInputText userIdAuth;
    private RichInputText userPasswordAuth;
    private RichPanelFormLayout rossRfAuthPanel;
    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichOutputText errorMessage;
    private RichLink validateLogonHiddenLink;
    private RichPopup confirmGoToPopup;
    private RichOutputText confirmGoToOutputtext;

    public AuthBacking() {

    }

    public void onChangedUserIdAuth(ValueChangeEvent vce) {
        this.updateModel(vce);
        String userIdAuth = (String) vce.getNewValue();
        ADFUtils.setBoundAttributeValue("UserIdAuth", userIdAuth);
        getRossRfPFBean().focusPassword();
        refreshContentOfUIComponent(getRossRfAuthPanel());
        setFocusOnUIComponent(getUserPasswordAuth());
    }

    public void onChangedUserPasswordAuth(ValueChangeEvent vce) {
        this.updateModel(vce);
        String userPasswordAuth = (String) vce.getNewValue();
        ADFUtils.setBoundAttributeValue("UserPasswordAuth", userPasswordAuth);
        if (StringUtils.isNotEmpty(userPasswordAuth)) {
            new ActionEvent(getValidateLogonHiddenLink()).queue();
        } else {
            getRossRfPFBean().focusUserId();
            refreshContentOfUIComponent(getRossRfAuthPanel());
            setFocusOnUIComponent(getUserIdAuth());
        }
    }

    private RossRfAuthSBean getRossRfPFBean() {
        return (RossRfAuthSBean) getPageFlowBean("RossRfAuthSBean");
    }


    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (getRossRfPFBean() != null) {
            if (!getRossRfPFBean().getDisabledUserId()) {
                setFocusOnUIComponent(getUserIdAuth());
            } else if (!getRossRfPFBean().getDisabledPassword()) {
                setFocusOnUIComponent(getUserPasswordAuth());
            }
        }
    }


    public String okGoLinkAction() {
        return "backGlobalHome";
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String currentFieldId = (clientEvent.getComponent()).getId();
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (valueNotChanged(submittedValue, field.getValue())) {
                    if ("it1".equals(currentFieldId) && StringUtils.isNotEmpty(submittedValue))
                        this.onChangedUserIdAuth(new ValueChangeEvent(this.getUserIdAuth(), null, submittedValue));
                    else if ("it2".equals(currentFieldId) && StringUtils.isNotEmpty(submittedValue))
                        this.onChangedUserIdAuth(new ValueChangeEvent(this.getUserPasswordAuth(), null,
                                                                      submittedValue));
                }
            }
        }
    }

    private boolean valueNotChanged(String submittedVal, Object fieldValue) {
        if (fieldValue instanceof String) {
            return (StringUtils.isEmpty((String) fieldValue) && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase((String) fieldValue);
        } else {
            return (fieldValue == null && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase(fieldValue == null ? "" : fieldValue.toString());
        }
    }

    public String validateLoggonHiddenAction() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateLogon");
        Map map = oper.getParamsMap();
        map.put("authUserId", getUserIdAuth().getValue());
        map.put("authUserPassword", getUserPasswordAuth().getValue());
        oper.execute();
        String navigation = "backGlobalHome";
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            ADFContext.getCurrent().getPageFlowScope().put("globalAuthOk",
                                                           ADFUtils.getBoundAttributeValue("GlobalAuthOk"));
            ADFContext.getCurrent().getPageFlowScope().put("globalUserPrivLevel",
                                                           ADFUtils.getBoundAttributeValue("GlobalUserPrivLevel"));
            ADFContext.getCurrent().getPageFlowScope().put("globalAuthUserId",
                                                           ADFUtils.getBoundAttributeValue("GlobalAuthUserId"));
            /* This messages are being handled in PTS_QTY screen, in PTS Picking Form
            String returnMsg = (String) oper.getResult();
            if (StringUtils.isNotEmpty(returnMsg) && !"ON".equalsIgnoreCase(returnMsg)) {
                navigation = null;
                this.getUserPasswordAuth().setDisabled(true);
                this.refreshContentOfUIComponent(this.getUserPasswordAuth());
                this.getConfirmGoToOutputtext().setValue(this.getMessage(returnMsg, null, null, null));
                this.getConfirmGoToPopup().show(new RichPopup.PopupHints());
            } */
        }
        return navigation;
    }

    public void setConfirmGoToPopup(RichPopup confirmGoToPopup) {
        this.confirmGoToPopup = confirmGoToPopup;
    }

    public RichPopup getConfirmGoToPopup() {
        return confirmGoToPopup;
    }

    public void setConfirmGoToOutputtext(RichOutputText confirmGoToOutputtext) {
        this.confirmGoToOutputtext = confirmGoToOutputtext;
    }

    public RichOutputText getConfirmGoToOutputtext() {
        return confirmGoToOutputtext;
    }

    public void setValidateLogonHiddenLink(RichLink validateLogonHiddenLink) {
        this.validateLogonHiddenLink = validateLogonHiddenLink;
    }

    public RichLink getValidateLogonHiddenLink() {
        return validateLogonHiddenLink;
    }


    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setUserIdAuth(RichInputText userIdAuth) {
        this.userIdAuth = userIdAuth;
    }

    public RichInputText getUserIdAuth() {
        return userIdAuth;
    }

    public void setUserPasswordAuth(RichInputText userPasswordAuth) {
        this.userPasswordAuth = userPasswordAuth;
    }

    public RichInputText getUserPasswordAuth() {
        return userPasswordAuth;
    }

    public void setRossRfAuthPanel(RichPanelFormLayout rossRfAuthPanel) {
        this.rossRfAuthPanel = rossRfAuthPanel;
    }

    public RichPanelFormLayout getRossRfAuthPanel() {
        return rossRfAuthPanel;
    }

}
