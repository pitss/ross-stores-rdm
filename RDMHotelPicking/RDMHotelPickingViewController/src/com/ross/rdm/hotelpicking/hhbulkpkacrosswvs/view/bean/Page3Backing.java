package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page3.jspx
// ---    
// ---------------------------------------------------------------------
public class Page3Backing extends BulkPickBaseBean {

    private RichInputText containerId;
    private RichLink f6Lpn;
    private RichLink f4Done;
    private RichLink f3Exit;
    private RichOutputText errorWarnInfoMessage;
    private RichIcon errorWarnInfoIcon;
    private RichPanelGroupLayout allMessagesPanel;
  
    private RichPopup gotoPopup;
    private RichOutputText popupMessage;
    private RichPopup markLocationPopup;
    private RichOutputText mlPopupMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class); 

    public Page3Backing() {

  }

  public void onChangedContainerIdContainer(ValueChangeEvent vce) {
      _logger.info("onChangedContainerIdContainer Start");
      hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel());
      OperationBinding opr = (OperationBinding) ADFUtils.findOperation(OPR_CONTAINER_KEY_NEXT);
      if (opr != null) { 
          opr.getParamsMap().put("containerId", vce.getNewValue());
          Map returnValues = (Map) opr.execute();
          List<String> errors = (List<String>) returnValues.get("ErrorMessage");
          ADFUtils.setBoundAttributeValue(ATTR_CONTAINER_ID, null);
          getContainerId().setValue(null);
          
          if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
              ADFContext.getCurrent().getRequestScope().put("ValidationError", "Y");
              return;
          } 
      }
      _logger.info("onChangedContainerIdContainer End");
       
  }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
       setFocusOnUIComponent(getContainerId()); 
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void performKey(ClientEvent clientEvent) {
            _logger.info("performKey Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEY_PRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEY_PRESSED);
            
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            }
            else if (F4_KEY_CODE.equals(keyPressed)) {
                   ActionEvent actionEvent = new ActionEvent(this.getF4Done());
                   actionEvent.queue();
                } 
            else if (F6_KEY_CODE.equals(keyPressed)) {
                   ActionEvent   actionEvent = new ActionEvent(this.getF6Lpn());
                   actionEvent.queue();
                }                 
            }
            _logger.info("performKey End");
        }

    public void f3ExitListener(ActionEvent actionEvent) {
        _logger.info("f3ExitListener Start");
        if (PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE))) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR,CNNT_CNCL_START );
            return;
        }
        
        OperationBinding oper =  (OperationBinding)ADFUtils.findOperation(OPR_CONT_CHILD_EXIST);
        oper.getParamsMap().put("type","S"); //check for scanned child
        boolean valid = (Boolean)oper.execute();
        if ( valid){
            getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR,CNNT_CNCL_START );            
        }
        else{
            ADFUtils.setBoundAttributeValue("NbrChild", new BigDecimal(0));
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONTAINER_QTY);
            ADFUtils.invokeAction("return");
        }
        _logger.info("f3ExitListener End");
    }

    public void f4DoneListener(ActionEvent actionEvent) {
        _logger.info("f4DoneListener Start");
        if ( YES.equals(ADFContext.getCurrent().getRequestScope().get("ValidationError"))) {
        _logger.warning("F4KeyListener Validation Errors exist, returning no-op");
            return;
        }
        hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel());
        OperationBinding oper =  (OperationBinding)ADFUtils.findOperation(OPR_CONT_CHILD_EXIST);
        oper.getParamsMap().put("type","S"); //check for scanned child
        boolean scanChildExists = (Boolean)oper.execute();
        boolean isPickTypeC = PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE));
        if (   ! scanChildExists && isPickTypeC){
            Object msg = 
            getMessage(NO_CONT_BYPASS, "C", getBoundAttribute(ATTR_FACILITY_ID), getBoundAttribute(ATTR_LANGUAGE_CODE)); 
            getPopupMessage().setValue(msg);
            this.getGotoPopup().show(new RichPopup.PopupHints()); 
        } 
        else if (  ! scanChildExists && ! isPickTypeC ){
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONTAINER_QTY);
            getBulkPkAcrossWvPageFlowBean().setErrorMessage(getMessage(INV_CONTAINER, WARN, getBoundAttribute(ATTR_FACILITY_ID), getBoundAttribute(ATTR_LANGUAGE_CODE)));
            ADFUtils.invokeAction("return");
        }
        else {
            oper =  (OperationBinding)ADFUtils.findOperation(OPR_CONT_CHILD_EXIST);
            oper.getParamsMap().put("type","U"); //check for scanned child
            scanChildExists = (Boolean)oper.execute();
            if ( ! scanChildExists &&  isPickTypeC ){ 
                Object msg = 
                getMessage(CONFIRM_NOT_EMP, WARN, getBoundAttribute(ATTR_FACILITY_ID), getBoundAttribute(ATTR_LANGUAGE_CODE)); 
                getMlPopupMessage().setValue(msg); 
                this.getMarkLocationPopup().show(new RichPopup.PopupHints()); 
            }
            
            processRemainingCorCIDS(isPickTypeC);
            
            
        }
        _logger.info("f4DoneListener End");
    }
    
    private void processRemainingCorCIDS( ){       
        boolean isPickTypeC = PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE));
        processRemainingCorCIDS(isPickTypeC);
    }
    private void processRemainingCorCIDS(boolean isPickTypeC){
        _logger.info("processRemainingCorCIDS Start");
        if (! isPickTypeC ){
            //Process_Remaining_CIDS;
            _logger.info("processing CIDS");
            OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_PROCESS_REMAIN_CIDS);
            opr.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion() );
            opr.getParamsMap().put("pmAllowIntrlvg",getBulkPkAcrossWvPageFlowBean().getPmAllowIntrlvg());
            Map returnValues = (Map)opr.execute(); 
            List<String> errors = (List<String>) returnValues.get("Errors");
            if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
                return;
            }
            Object gotoField = returnValues.get("GoToField");
            Object callingBlock = returnValues.get("CallingBlock");
           // Object clearBlock = returnValues.get("ClearBlock");
            
            performNavigation(callingBlock,gotoField);
        }
        else {
            //Process_Remaining_C;
            _logger.info("processing C");
            OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_PROCESS_REMAIN_C);
            opr.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion() );
            opr.getParamsMap().put("pmAllowIntrlvg",getBulkPkAcrossWvPageFlowBean().getPmAllowIntrlvg());
            Map returnValues = (Map)opr.execute(); 
            List<String> errors = (List<String>) returnValues.get("Errors");
            if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
                return;
            }
            Object gotoField = returnValues.get("GoToField");
            Object callingBlock = returnValues.get("CallingBlock"); 
            
            performNavigation(callingBlock,gotoField);
        }

        _logger.info("processRemainingCorCIDS End");
    }
    
    private void performNavigation(Object callingBlock, Object gotoField){

        _logger.info("performNavigation Start");
        if (gotoField != null ) {
            String goToFieldToString = gotoField.toString();
            String goToFieldCastString = (String)gotoField;
            if (goToFieldCastString.contains("."))
              getBulkPkAcrossWvPageFlowBean().setIsFocusOn(goToFieldCastString);
            
            if (goToFieldToString.startsWith("WEIGHT"))
               ADFUtils.invokeAction("weight");
            else if (goToFieldToString.startsWith("LABEL_CHILD"))
               ADFUtils.invokeAction("labelChild");
            else if (goToFieldToString.startsWith("BULK_PICK"))
               ADFUtils.invokeAction("bulkPick"); 
            else if (goToFieldToString.startsWith("TO_LOCATION"))
               ADFUtils.invokeAction("location");
            else if (goToFieldToString.startsWith("EXIT"))
               ADFUtils.invokeAction("backGlobalHome");
        }
        else if (callingBlock != null && "BULK_PICK".equals(callingBlock)){
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONTAINER_QTY);
            ADFUtils.invokeAction("bulkPick");            
        }
        else if (callingBlock != null && "TO_LOCATION".equals(callingBlock))
           ADFUtils.invokeAction("location");

        _logger.info("performNavigation End");
        //Else stay on bulk pick screen, there must be some exception.
    }
    
    public void mlOkDialogListener(ActionEvent actionEvent) {
        _logger.info("mlOkDialogListener Start");
        //CONFIRM_NOT_EMP popup
        getMarkLocationPopup().hide();
        OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_MARK_LOCATION);
        opr.execute();
        processRemainingCorCIDS();
        _logger.info("mlOkDialogListener End");
    }
    
    public String yesDialogAction() {  
        // NO_CONT_BYPASS popup
        getGotoPopup().hide();
        getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONFIRM_LOC);
        return this.bypassAction();
    } 
    public void noDialogListener(ActionEvent actionEvent) {  
        _logger.info("okDialogListener Start");
        // NO_CONT_BYPASS popup
        getGotoPopup().hide(); 
        _logger.info("okDialogListener End");
    }    
    public void F6KeyListener(ActionEvent actionEvent) {
        _logger.info("displayLPNs Start");
        OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_QUERY_LPN);
        if (opr != null)
          opr.execute();
        
        DCIteratorBinding iter = ADFUtils.findIterator(ITER_LPN);
      //  if (iter != null) iter.executeQuery();
        if (iter == null || iter.getEstimatedRowCount() <= 0){
            getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, NO_DATA ); 
            return;
        }
        else  {   //navigate only if LPNs found
           getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONT_CONTID);
           ADFUtils.invokeAction("F6LPN");
        }        
        _logger.info("displayLPNs End");
    }

    public void setF6Lpn(RichLink f6Lpn) {
        this.f6Lpn = f6Lpn;
    }

    public RichLink getF6Lpn() {
        return f6Lpn;
    }

    public void setF4Done(RichLink f4Done) {
        this.f4Done = f4Done;
    }

    public RichLink getF4Done() {
        return f4Done;
    }

    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

 
    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

 

    public void setGotoPopup(RichPopup gotoPopup) {
        this.gotoPopup = gotoPopup;
    }

    public RichPopup getGotoPopup() {
        return gotoPopup;
    }

    public void setPopupMessage(RichOutputText popupMessage) {
        this.popupMessage = popupMessage;
    }

    public RichOutputText getPopupMessage() {
        return popupMessage;
    }

    public void setMarkLocationPopup(RichPopup markLocationPopup) {
        this.markLocationPopup = markLocationPopup;
    }

    public RichPopup getMarkLocationPopup() {
        return markLocationPopup;
    }

    public void setMlPopupMessage(RichOutputText mlPopupMessage) {
        this.mlPopupMessage = mlPopupMessage;
    }

    public RichOutputText getMlPopupMessage() {
        return mlPopupMessage;
    }

  public String bypassAction() {
    String action = null;
    OperationBinding opr = (OperationBinding) ADFUtils.findOperation("bypassKeyPress");
    if (opr != null) {
        opr.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion());
        Map returnValues = (Map) opr.execute();
        Object gotoField = returnValues.get("GoToField");
        Object callingBlock = returnValues.get("CallingBlock");
        Object clearBlock = returnValues.get("ClearBlock");
        if ("BULK_PICK.GENERIC_TO_CONTAINER".equals(callingBlock) ||
            "BULK_PICK.GENERIC_TO_CONTAINER".equals(gotoField)) {
            if (clearBlock != null && "T".equals(clearBlock)) {
              action = "clearBlock";
            } else {
              setIsFocusOn("BULK_PICK.GENERIC_TO_CONTAINER");
              action = "genericPick";
            }
        } 
        else if ("REASON".equals(gotoField)) {
          action = "F7BYPASS";
        }
    }
    return action;
  }
}

