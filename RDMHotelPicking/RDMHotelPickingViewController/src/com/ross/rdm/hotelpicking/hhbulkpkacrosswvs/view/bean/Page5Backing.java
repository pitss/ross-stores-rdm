package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page5.jspx
// ---    
// ---------------------------------------------------------------------
public class Page5Backing extends BulkPickBaseBean {

    private RichInputText pickToChild;
    private RichInputText genericToChild;
    private RichInputText containerQty;
    private RichLink f3Exit;
    private RichLink f4Done;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page5Backing.class); 

    public Page5Backing() {

    }
   
  public void onChangedGenericToChildLabelChild(ValueChangeEvent vce) {
      _logger.info("onChangedGenericToChildLabelChild Start");
      OperationBinding opr = (OperationBinding) ADFUtils.findOperation(OPR_GENERICCHILD_KEY_NEXT);
      if (opr != null) { 
          Map returnValues = (Map) opr.execute();
          List<String> errors = (List<String>) returnValues.get("Errors");
          if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
              return;
          } 
      }
      _logger.info("onChangedGenericToChildLabelChild End");
  }

  public void onChangedPickToChildLabelChild(ValueChangeEvent vce) {
      _logger.info("onChangedPickToChildLabelChild Start");
      OperationBinding opr = (OperationBinding) ADFUtils.findOperation(OPR_PICKCHILD_KEY_NEXT);
      if (opr != null) { 
          Map returnValues = (Map) opr.execute();
          List<String> errors = (List<String>) returnValues.get("Errors");
          if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
              return;
          } 
      }
      _logger.info("onChangedPickToChildLabelChild End"); 
  }

    public void onRegionLoad(ComponentSystemEvent cse) {
        _logger.info("onRegionLoad Start");
        Object gotoFld = getBulkPkAcrossWvPageFlowBean().getIsFocusOn();
        if (PICK_TO_CHILD.equals(gotoFld)){
            setFocusOnUIComponent(getPickToChild());
            ADFUtils.setBoundAttributeValue(ATTR_SHOW_GENERIC_OR_PICK, "P");
        }
        else if (GENERIC_TO_CHILD.equals(gotoFld)){ 
            ADFUtils.setBoundAttributeValue(ATTR_SHOW_GENERIC_OR_PICK, "G");
            setFocusOnUIComponent(getGenericToChild());
        }
        _logger.info("onRegionLoad End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEY_PRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEY_PRESSED);
            
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            }
            else if (F4_KEY_CODE.equals(keyPressed)) {
                     ActionEvent actionEvent = new ActionEvent(this.getF4Done());
                    actionEvent.queue(); 
            }
       }
        _logger.info("performKey End");
    }
        
    public void f4DoneListener(ActionEvent actionEvent) {
        _logger.info("f4DoneListener Start");
        
        _logger.info("f4DoneListener End");
    }

    public void f3ExitListener(ActionEvent actionEvent) { 
      _logger.info("f3ExitListener Start");
        getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONTAINER_QTY);
        ADFUtils.invokeAction("return");
      _logger.info("f3ExitListener End");
    }
 

    public void setPickToChild(RichInputText pickToChild) {
        this.pickToChild = pickToChild;
    }

    public RichInputText getPickToChild() {
        return pickToChild;
    }

    public void setGenericToChild(RichInputText genericToChild) {
        this.genericToChild = genericToChild;
    }

    public RichInputText getGenericToChild() {
        return genericToChild;
    }

    public void setContainerQty(RichInputText containerQty) {
        this.containerQty = containerQty;
    }

    public RichInputText getContainerQty() {
        return containerQty;
    } 
    
    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

    public void setF4Done(RichLink f4Done) {
        this.f4Done = f4Done;
    }

    public RichLink getF4Done() {
        return f4Done;
    }


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }
}
