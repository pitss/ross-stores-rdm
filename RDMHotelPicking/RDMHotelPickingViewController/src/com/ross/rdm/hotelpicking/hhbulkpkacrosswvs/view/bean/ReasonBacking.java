package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLocalViewRowImpl;

import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Reason.jspx
// ---    
// ---------------------------------------------------------------------
public class ReasonBacking extends BulkPickBaseBean {
    
    private RichSelectOneChoice reasonCode;
    private RichLink f3Exit;
    private RichLink f4Done;
    private RichLink f6List;
    private static final String OPR_V_REASON_CODE = "vReasonCode";
    private static final String OPR_BYPASS_PICK   = "bypassPick";
    private static final String IS_PAGE_LOAD   = "isPageLoad"; 
    private static final String ERR = "E";
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(ReasonBacking.class); 

    public ReasonBacking() {

   }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        Object pageLoad = ADFContext.getCurrent().getViewScope().get(IS_PAGE_LOAD);
        if (pageLoad == null){//First time execution on page load
            ADFContext.getCurrent().getViewScope().put(IS_PAGE_LOAD, NO);
            getReasonCode().setValue(0);
        }       
             
        String clientId = getReasonCode().getClientId(FacesContext.getCurrentInstance());
        String script = "expandSelectOneChoice('" + clientId + "');";
        this.writeJavaScriptToClient(script.toString());
        refreshContentOfUIComponent(getReasonCode());  
        _logger.info("onRegionLoad End");
    }

    public void setReasonCode(RichSelectOneChoice reasonCode) {
        this.reasonCode = reasonCode;
    }

    public RichSelectOneChoice getReasonCode() {
        return reasonCode;
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Done());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_bulk_pk_across_wv_s_REASON_F6,
                                                        isBulkPickNotMain() ?
                                                        RoleBasedAccessConstants.FORM_NAME_hh_bulk_pick_s :
                                                        RoleBasedAccessConstants.FORM_NAME_hh_bulk_pk_across_wv_s)) {
                    ActionEvent actionEvent = new ActionEvent(this.getF6List());
                    actionEvent.queue();
                } else {
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            ERROR, "NOT_ALLOWED");
                }
            }

        }
        _logger.info("performKey End");
    }
    
    public void f3ExitListener(ActionEvent actionEvent) { 
        _logger.info("f3ExitListener Start");
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
        String prevItem = wLocal.getPreviousItem(); 
        if (prevItem == null)
           getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONTAINER_QTY);         
        else
           getBulkPkAcrossWvPageFlowBean().setIsFocusOn( prevItem);         
        
        getBulkPkAcrossWvPageFlowBean().setReturnFromOtherPage(true);
        if (prevItem == null || prevItem.startsWith("BULK_PICK"))
          ADFUtils.invokeAction("return");
        else
          ADFUtils.invokeAction("returnPage3");
        _logger.info("f3ExitListener End");
    }
    
    public void f4DoneListener(ActionEvent actionEvent) {
        _logger.info("f4DoneListener Start");
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_bulk_pk_across_wv_s_REASON_F4,
                                                isBulkPickNotMain() ? RoleBasedAccessConstants.FORM_NAME_hh_bulk_pick_s :
                                                RoleBasedAccessConstants.FORM_NAME_hh_bulk_pk_across_wv_s)) {
            Object reason = JSFUtils.resolveExpression("#{bindings.Code.attributeValue}");

            if (reason == null) {
                showMessagesPanel("E",
                                  getMessage("INV_REASON_CODE", "W", getBoundAttribute("FacilityId"),
                                             getBoundAttribute("LanguageCode")));
                return;
            }
            if (vReasonCode(reason)) {
                bypassPick();
            } else {
                showMessagesPanel("E",
                                  getMessage("INV_REASON_CODE", "W", getBoundAttribute("FacilityId"),
                                             getBoundAttribute("LanguageCode")));
            }

        } else {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), ERROR,
                                    "NOT_ALLOWED");
        }
        _logger.info("f4DoneListener End");
    }
    
    private boolean vReasonCode(Object reason) {
        _logger.info("vReasonCode Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(OPR_V_REASON_CODE);
        op.getParamsMap().put("code",  reason);
        op.execute();
        String l_return = null;
        if (op.getErrors().isEmpty()) {
            if (op.getResult() != null) {
              Map codeList = (Map) op.getResult();
                if (ERR.equals(codeList.get("MessageType"))) {
                    this.hideMessagesPanel();
                    this.showMessagesPanel(ERR, (String)codeList.get("Message"));
                    return false;
                }
                l_return = (String)codeList.get("ValidCode");
            }
        }
        this.hideMessagesPanel();

        _logger.info("vReasonCode End");
        if ("1".equals(l_return)) 
            return true;  
        else
            return false;    
    }
    
    private void bypassPick() {
        _logger.info("bypassPick Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(OPR_BYPASS_PICK);
        op.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion() ); 
        
        Map returnValues = (Map)op.execute();
        List<String> errors = ( List<String>) returnValues.get("Errors");
        if (processMessages(errors)){ //return if errors                    
            return;
        }
        Object gotoField    = returnValues.get("GoToField"); 
        
        if (gotoField != null){
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String)gotoField);  
            if ("BULK_PICK.GENERIC_TO_CONTAINER".equals(gotoField))
                ADFUtils.invokeAction("genericPick"); 
        }
        _logger.info("bypassPick End");
    }
    
    private boolean processMessages(List<String> errors){
        _logger.info("processMessages Start");
          boolean hasErrors = false;
            if(errors != null && !errors.isEmpty()){
                this.showMessagesPanel(errors.get(0), errors.get(1));
                hasErrors = true;
            }
            else{ 
                this.hideMessagesPanel();
            }
        _logger.info("processMessages End");
        return hasErrors;
    }
    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);

        if ("W".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("warning");
        } else if ("I".equals(messageType) || "M".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("info");
        } else {
            this.getErrorWarnInfoIcon().setName("error");
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel End");
    }
    
    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("hideMessagesPanel End");
    }

    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

    public void setF4Done(RichLink f4Done) {
        this.f4Done = f4Done;
    }

    public RichLink getF4Done() {
        return f4Done;
    }

    public void setF6List(RichLink f6List) {
        this.f6List = f6List;
    }

    public RichLink getF6List() {
        return f6List;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }
}
