package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.math.BigDecimal;

import java.util.List;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class BulkPickBaseBean extends RDMHotelPickingBackingBean {
    public BulkPickBaseBean() {
        super();
    }
    
     static final String HH_BULK_PK_ACROSS_WV_PAGE_FLOW_BEAN = "HhBulkPkAcrossWvSBean";
     static final String ERROR = "E";
     static final String WARN = "W";
     static final String INFO = "I";
     static final String CONF = "C";
     static final String SUCCESS = "S";
     static final String MSGTYPE_M = "M";
     static final String REQ_ICON = "required";
     static final String ERR_ICON = "error";
     static final String WRN_ICON = "warning";
     static final String INF_ICON = "info";
     static final String SCC_ICON = "logo";
     static final String INTRLVG  = "INTRLVG";
     
     static final BigDecimal ONE  = new BigDecimal(1);
     static final BigDecimal ZERO = new BigDecimal(0);
     static final String KEY_PRESSED = "keyPressed";
    
    // Block names
     static final String BULK_PICK      = "BULK_PICK";
     static final String CONTAINER      = "CONTAINER";
     static final String CONT_CONTID    = "CONTAINER.CONTAINER_ID";
     static final String TO_LOCATION    = "TO_LOCATION";
     static final String WEIGHT         = "WEIGHT";
     static final String LABEL_CHILD    = "LABEL_CHILD";
     static final String CONFIRM_LOC    = "BULK_PICK.CONF_LOCATION_ID";
     static final String CONFIRM_CONT   = "BULK_PICK.CONF_CONTAINER_ID";
     static final String PICK_CONTAINER = "BULK_PICK.PICK_TO_CONTAINER_ID";
     static final String GEN_CONTAINER  = "BULK_PICK.GENERIC_TO_CONTAINER";
     static final String CONTAINER_QTY  = "BULK_PICK.CONTAINER_QTY";
     static final String TO_LOCATION_ID = "TO_LOCATION.TO_LOCATION_ID";
     static final String PICK_TO_CHILD  = "LABEL_CHILD.PICK_TO_CHILD";
     static final String TOTAL_WEIGHT   = "WEIGHT.TOTAL_WEIGHT";
     static final String GENERIC_TO_CHILD  = "LABEL_CHILD.GENERIC_TO_CHILD";
    
     static final String NO   = "N";
     static final String YES  = "Y";
     static final String NONE = "NONE";
     static final String STR_0 = "0";
     static final String STR_1 = "1";
     static final String STR_2 = "2";
     static final String STR_3 = "3";
     static final String STR_4 = "4";
     //Error Codes
     static final String MUST_BE_NUMERIC = "MUST_BE_NUMERIC";
     static final String INV_CONTAINER = "INV_CONTAINER";
     static final String INV_LOCATION  = "INV_LOCATION";
     static final String PARTIAL_ENTRY = "PARTIAL_ENTRY";
     static final String INV_QTY       = "INV_QTY";
     static final String DIFF_PICK     = "DIFF_PICK";
     static final String NO_DATA       = "NO_DATA";
     static final String CNNT_CNCL_START = "CNNT_CNCL_START";
     static final String CAN_NOT_CANCEL  = "CAN_NOT_CANCEL";
     static final String NO_CONT_BYPASS  = "NO_CONT_BYPASS";
     static final String CONFIRM_EMPTY   = "CONFIRM_EMPTY";
     static final String CONFIRM_NOT_EMP = "CONFIRM_NOT_EMP";
     static final String FIRST_RECORD    = "FIRST RECORD";
     static final String LAST_RECORD     = "LAST RECORD";
     //Pick Types
     static final String PICK_B  = "B";
     static final String PICK_BP = "BP";
     static final String PICK_BR = "BR";
     static final String PICK_C  = "C";
     static final String PICK_CP = "CP";
     
     //Attributes
     static final String ATTR_PICK_TO_CONTAINER_ID = "PickToContainerId";
     static final String ATTR_CONF_CONTAINER_ID    = "ConfContainerId";
     static final String ATTR_CONTAINER_ID         = "ContainerId";
     static final String ATTR_CONF_LOCATION_ID     = "ConfLocationId";
     static final String ATTR_CONTAINER_QTY        = "ContainerQty";
     static final String ATTR_GENERIC_TO_CONTAINER = "GenericToContainer";
     static final String ATTR_FROM_LOCATION_ID     = "FromLocationId";
     static final String ATTR_FACILITY_ID          = "FacilityId";
     static final String ATTR_LANGUAGE_CODE        = "LanguageCode";
     static final String ATTR_PICK_FROM_CONT_ID    = "PickFromContainerId";
     static final String ATTR_PICK_TYPE            = "PickType";
     static final String ATTR_WL_PICK_FROM_CONTAINER  = "WorkLocalPickFromContainerId";
     static final String ATTR_SHOW_GENERIC_OR_PICK = "ShowGenericOrPick";
     static final String ATTR_CALLING_BLOCK        = "CallingBlock";
     static final String ATTR_TO_LOCATION_ID       = "ToLocationId"; 
     //Operations
     static final String OPR_GET_PICK_DIRECTIVE  = "getPickDirective"; 
     static final String OPR_BYPASS_KEY_PRESS    = "bypassKeyPress";
     static final String OPR_V_FROM_CONTAINER    = "vFromContainer";
     static final String OPR_CONF_CONTAINER_NEXT = "confContainerKeyNext";
     static final String OPR_QUERY_LPN           = "QueryLPN";
     static final String OPR_VALIDATE_QTY        = "validateQty";
     static final String OPR_CONT_CHILD_EXIST    = "containerChildExist";
     static final String OPR_PROCESS_REMAIN_CIDS = "processRemainingCIDS";
     static final String OPR_PROCESS_REMAIN_C    = "processRemainingC";
     static final String OPR_MARK_LOCATION       = "markLocation";
     static final String OPR_PROCESS_WEIGHT      = "processWeight";
     static final String OPR_TO_LOC_DONE_KEY     = "toLocationDoneKey";
     static final String OPR_TR_EXIT_BP          = "trExitBP";
     static final String OPR_CONTAINER_KEY_NEXT  = "ContContainerIdKeyNext";
     static final String OPR_PICKCHILD_KEY_NEXT   = "LabelChildPickToChildKeyNext";
     static final String OPR_GENERICCHILD_KEY_NEXT = "ContContainerIdKeyNext";
     static final String OPR_RUN_PURGE_DIRECTIVE   = "runPurgeDirective";
     static final String OPR_CHECK_FOR_CHILDREN    = "CheckForChildren";
     static final String OPR_COUNT_SYS_CHILD       = "CountSysChild";
     static final String OPR_USER_DONE_TO_LOCATION = "UserDoneToLocation";
     static final String OPR_CHECK_CATCH_WEIGHT    = "checkCatchWeight";
     //Iterators
     static final String ITER_LPN   =  "HhBulkPkAcrossWvSLpnViewIterator";
          
    static HhBulkPkAcrossWvSBean getBulkPkAcrossWvPageFlowBean() {
        return  (HhBulkPkAcrossWvSBean) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(HH_BULK_PK_ACROSS_WV_PAGE_FLOW_BEAN); 
    }
    
     void getAndShowMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon, RichPanelGroupLayout allMessagesPanel,
                                  String messageType, String msgCode ) {
         String msg =   getMessage(msgCode, messageType);
         showMessagesPanel(errorWarnInfoMessage ,errorWarnInfoIcon ,allMessagesPanel, messageType, msg);
     }
     String getMessage(String msgCode, String messageType){
         return   getMessage(msgCode, messageType, getBoundAttribute(ATTR_FACILITY_ID), getBoundAttribute(ATTR_LANGUAGE_CODE));          
     }
     void showMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon, RichPanelGroupLayout allMessagesPanel, String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        errorWarnInfoMessage.setValue(null);
        errorWarnInfoIcon.setName(null);
        allMessagesPanel.setVisible(false);

        if (WARN.equals(messageType)) {
            errorWarnInfoIcon.setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            errorWarnInfoIcon.setName(INF_ICON);
        } else {
            if (ERROR.equals(messageType)) {
                errorWarnInfoIcon.setName(ERR_ICON);
            } else {
                if (SUCCESS.equals(messageType)) {
                    errorWarnInfoIcon.setName(SCC_ICON);
                }
            }
        } 
        errorWarnInfoMessage.setValue(msg);
        allMessagesPanel.setVisible(true);
        refreshContentOfUIComponent(allMessagesPanel);
    }

     void hideMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon, RichPanelGroupLayout allMessagesPanel) {
        errorWarnInfoMessage.setValue(null);
        errorWarnInfoIcon.setName(null);
        refreshContentOfUIComponent(allMessagesPanel);
    }
     
     String getBoundAttribute(String attrName){
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String)val;
        return null;
    }
     
     
    void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set){
        if (set){
            addErrorStyleToComponent(uiComponent);
//            selectTextOnInputElement(uiComponent);
            icon.setName(ERR_ICON);  
            
        }
        else {
            icon.setName(REQ_ICON); 
            removeErrorStyleToComponent(uiComponent);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(icon);
        AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }
    
 /*   void selectTextOnInputElement(RichInputText uiComponent){
        String clientId = uiComponent.getClientId(FacesContext.getCurrentInstance());
        String script = "selectInputText('" + clientId + "');";
        this.writeJavaScriptToClient(script.toString());
        refreshContentOfUIComponent(uiComponent);
    } */
    
    boolean processMessages(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon, RichPanelGroupLayout allMessagesPanel, List<String> errors){
       boolean hasErrors = false;
         if(errors != null && !errors.isEmpty()){
             this.showMessagesPanel(errorWarnInfoMessage,errorWarnInfoIcon,allMessagesPanel,errors.get(0), errors.get(1));
             hasErrors = true;
         }
         else{ 
             this.hideMessagesPanel(errorWarnInfoMessage,errorWarnInfoIcon,allMessagesPanel);
         }
     return hasErrors;
 }
 
    void setIsFocusOn (String comp){
        getBulkPkAcrossWvPageFlowBean().setIsFocusOn(comp);
    }
    
    String getIsFocuOn() {
        return getBulkPkAcrossWvPageFlowBean().getIsFocusOn();
    }
    
    public boolean isBulkPickMain() {
        String header = (String) ADFUtils.getBoundAttributeValue("Header");
        if(null != header){
            if(header.equals("BULK PICK")){
                getBulkPkAcrossWvPageFlowBean().setFormName("Bulk_Pick");
            }
            else{
                getBulkPkAcrossWvPageFlowBean().setFormName("Bulk_Pick_Across_Waves");
            }
        }
        else{
            getBulkPkAcrossWvPageFlowBean().setFormName("Bulk_Pick_Across_Waves");
        }
           
        return getBulkPkAcrossWvPageFlowBean().getFormName().equals("Bulk_Pick");
    }
    
    public boolean isBulkPickNotMain() {
        if(null != getBulkPkAcrossWvPageFlowBean().getFormName()){
            return getBulkPkAcrossWvPageFlowBean().getFormName().equals("Bulk_Pick");
        }
        return false;
    }
}
