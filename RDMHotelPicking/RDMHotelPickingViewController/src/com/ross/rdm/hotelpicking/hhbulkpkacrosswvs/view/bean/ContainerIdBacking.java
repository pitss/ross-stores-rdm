package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for ContainerId.jspx
// ---    
// ---------------------------------------------------------------------
public class ContainerIdBacking extends BulkPickBaseBean {

    private RichTable lpnTable;
    private RichOutputText lpnCid; 
    private RichLink f3Exit;
    private static ADFLogger _logger = ADFLogger.createADFLogger(ContainerIdBacking.class);
    private RichOutputText errorWarnInfoMessage;
    private RichIcon errorWarnInfoIcon;
    private RichPanelGroupLayout messagePanel;

    public ContainerIdBacking() {

  }

    public void setLpnTable(RichTable lpnTable) {
        this.lpnTable = lpnTable;
    }

    public RichTable getLpnTable() {
        return lpnTable;
    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        Object initFlag = ADFContext.getCurrent().getViewScope().get("BPInitLPN");
        if (initFlag == null) { //Set focus only once
            setLocalFocusOnUIComponent(getLpnTable());
            ADFContext.getCurrent().getViewScope().put("BPInitLPN",YES);
        }   
        DCIteratorBinding ib = ADFUtils.findIterator(ITER_LPN);
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == 0) {
            getErrorWarnInfoIcon().setName(INF_ICON);
            getErrorWarnInfoMessage().setValue(FIRST_RECORD);
        } else if (crow == trow - 1) {
            getErrorWarnInfoIcon().setName(INF_ICON);
            getErrorWarnInfoMessage().setValue(LAST_RECORD);
        } else {
            getErrorWarnInfoMessage().setValue(null);
            getErrorWarnInfoIcon().setName(null);
        }        
    }
   
    public void setLpnCid(RichOutputText lpnCid) {
        this.lpnCid = lpnCid;
    }

    public RichOutputText getLpnCid() {
        return lpnCid;
    }
    
    public void setLocalFocusOnUIComponent(UIComponent component) {
        _logger.info("setLocalFocusOnUIComponent start");
        String clientId = component.getClientId(FacesContext.getCurrentInstance()); 
       // StringBuilder script = new StringBuilder("var elem = document.getElementById('" + clientId + ":0:cid::content');");
        StringBuilder script = new StringBuilder("var elem = document.querySelector('[id=\"" + clientId + "\"]');");
        script.append("SetFocusOnUIcomp(elem);");
        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
        _logger.info("setLocalFocusOnUIComponent end");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEY_PRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEY_PRESSED);
            
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            }                         
        }
        _logger.info("performKey end");
    }

    public void F3ExistListener(ActionEvent actionEvent) {
        if (CONT_CONTID.equals(getBulkPkAcrossWvPageFlowBean().getIsFocusOn()))
            ADFUtils.invokeAction("page3"); 
        else
            ADFUtils.invokeAction("return");
    }

    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setMessagePanel(RichPanelGroupLayout messagePanel) {
        this.messagePanel = messagePanel;
    }

    public RichPanelGroupLayout getMessagePanel() {
        return messagePanel;
    }


}
