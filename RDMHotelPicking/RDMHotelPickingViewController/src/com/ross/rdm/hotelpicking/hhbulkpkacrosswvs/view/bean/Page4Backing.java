package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page4.jspx
// ---    
// ---------------------------------------------------------------------
public class Page4Backing extends BulkPickBaseBean {

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichLink f3Exit;
    private RichLink f4Done;
    private RichInputText totalWeight;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page4Backing.class); 

    public Page4Backing() {

  }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEY_PRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEY_PRESSED);

            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Done());
                actionEvent.queue();
            }
        }
        _logger.info("performKey End");
    }
    public void f4DoneListener(ActionEvent actionEvent) {
        _logger.info("f4DoneListener Start");
        try{
       OperationBinding opr  = (OperationBinding) ADFUtils.findOperation(OPR_PROCESS_WEIGHT);
       opr.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion() );
       opr.getParamsMap().put("pmAllowIntrlvg", getBulkPkAcrossWvPageFlowBean().getPmAllowIntrlvg());
       Map returnValues = (Map)opr.execute();
        List<String> errors = (List<String>) returnValues.get("Errors");
        if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
            return;
        }
        Object gotoField = returnValues.get("GoToField");
        Object callingBlock = returnValues.get("CallingBlock"); 
         performNavigation(callingBlock,gotoField);
        }
        catch (Exception e){
            _logger.severe("Error invoking operation "+ OPR_PROCESS_WEIGHT + " " + e.getMessage());
        }
        _logger.info("f4DoneListener End");
    }

    private void performNavigation(Object callingBlock, Object gotoField){
        _logger.info("performNavigation Start");
        if (gotoField != null){
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String)gotoField);
        }

        if (callingBlock != null && CONTAINER.equals(callingBlock))
           ADFUtils.invokeAction("container");
        else if (callingBlock != null && TO_LOCATION.equals(callingBlock))
           ADFUtils.invokeAction("toLocation"); 
        else if (gotoField != null && gotoField.toString().startsWith(BULK_PICK))
           ADFUtils.invokeAction("clearBlock"); 
        
        _logger.info("performNavigation End");
    }
    public void f3ExitListener(ActionEvent actionEvent) {
        _logger.info("f3ExitListener Start");
        if (BULK_PICK.equals(getBoundAttribute(ATTR_CALLING_BLOCK))){
            setIsFocusOn(CONTAINER_QTY);
            ADFUtils.invokeAction("return");
        }
        else {
            setIsFocusOn(TO_LOCATION_ID);
            ADFUtils.invokeAction("toLocation");
        }
        _logger.info("f3ExitListener End");
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        setFocusOnUIComponent(getTotalWeight());
    }
    
    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

    public void setF4Done(RichLink f4Done) {
        this.f4Done = f4Done;
    }

    public RichLink getF4Done() {
        return f4Done;
    }

    public void setTotalWeight(RichInputText totalWeight) {
        this.totalWeight = totalWeight;
    }

    public RichInputText getTotalWeight() {
        return totalWeight;
    }
}
