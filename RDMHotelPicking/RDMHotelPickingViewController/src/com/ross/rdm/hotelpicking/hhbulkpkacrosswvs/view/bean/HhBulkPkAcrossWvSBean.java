package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSLabelChildViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLaborProdViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLocalViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkViewRowImpl;

import java.math.BigDecimal;

import java.sql.Date;

import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Row;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhBulkPkAcrossWvSBean extends RDMHotelPickingPageFlowBean {
    private String pmTaskQueueInd; // Item/Parameter hh_bulk_pk_across_wv_s.PM_TASK_QUEUE_IND
    private String pmAllowIntrlvg; // Item/Parameter hh_bulk_pk_across_wv_s.PM_ALLOW_INTRLVG
    private String pmNoTaskFlag; // Item/Parameter hh_bulk_pk_across_wv_s.PM_NO_TASK_FLAG
    private String pmNoPickFlag; // Item/Parameter hh_bulk_pk_across_wv_s.PM_NO_PICK_FLAG
    private String pmRowid; // Item/Parameter hh_bulk_pk_across_wv_s.PM_ROWID
    private BigDecimal pmWaveNbr; // Item/Parameter hh_bulk_pk_across_wv_s.PM_WAVE_NBR
    private String pmPickType; // Item/Parameter hh_bulk_pk_across_wv_s.PM_PICK_TYPE
    private String pmPickTypeC; // Item/Parameter hh_bulk_pk_across_wv_s.PM_PICK_TYPE_C
    private String pmPickTypeCp; // Item/Parameter hh_bulk_pk_across_wv_s.PM_PICK_TYPE_CP
    private String pmStartLocation; // Item/Parameter hh_bulk_pk_across_wv_s.PM_START_LOCATION
    private String pmCallingForm; // Item/Parameter hh_bulk_pk_across_wv_s.PM_CALLING_FORM
    private String pmUpsCode; // Item/Parameter hh_bulk_pk_across_wv_s.PM_UPS_CODE
    private Date pmWaveDate; // Item/Parameter hh_bulk_pk_across_wv_s.PM_WAVE_DATE
    private String pmUpsGroupInd; // Item/Parameter hh_bulk_pk_across_wv_s.PM_UPS_GROUP_IND
    private BigDecimal pVersion;
    private String errorMessage;
    private String bypassPick;
    private String returnErrorMessage;
    private String lGlobalIntrlvg;
    private String formName;

    public void setLGlobalIntrlvg(String lGlobalIntrlvg) {
        this.lGlobalIntrlvg = lGlobalIntrlvg;
    }

    public String getLGlobalIntrlvg() {
        return lGlobalIntrlvg;
    }


    private static final String YES = "Y";
    private static final String NO = "N";
    private static final String PICK_CONTAINER = "PC";
    private static final String GENERIC_CONTAINER = "GC";
    private static final BigDecimal ONE = new BigDecimal(1);
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhBulkPkAcrossWvSBean.class);

    private String isFocusOn;
    private boolean returnFromOtherPage;

    public void setReturnErrorMessage(String returnErrorMessage) {
        this.returnErrorMessage = returnErrorMessage;
    }

    public String getReturnErrorMessage() {
        return returnErrorMessage;
    }

    public void setReturnFromOtherPage(boolean returnFromOtherPage) {
        this.returnFromOtherPage = returnFromOtherPage;
    }

    public boolean isReturnFromOtherPage() {
        return returnFromOtherPage;
    }

    public void initTaskFlow() {
        _logger.info("initTaskFlow Start");
        this.setReturnErrorMessage(null); //reset before start.
        this.performOperation("CreateInsert", false); //Create Row in Main
        this.performOperation("setGlobalVariablesBulkPick", true);
        this.initParams();

        this.performOperation("setWorkLaborProdVariables", false);

        Object message = this.setWorkLocalVariables();
        if (message != null && !PICK_CONTAINER.equals(message) && !GENERIC_CONTAINER.equals(message)) {
            // Error returned, do not continue return back to calling form
            //   ADFContext.getCurrent().getPageFlowScope().put("ExitForm", "Y");
            setReturnErrorMessage((String) message);
            //JSFUtils.addFacesErrorMessage((String)message);
            return;
        }

        this.performOperation("setWorkVariables", true);

        GlobalVariablesViewRowImpl glRow = getGlobalVariablesViewCurrentRow();
        HhBulkPkAcrossWvSWorkViewRowImpl work = getWorkViewCurrentRow();
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = getWorkLocalViewCurrentRow();

        //OTM-1610: Must reset flag else will create problem in interleaving putaway
        //Note flag is set on page1backing okDialogListener, if Interleaving true.
        glRow.setBP2PutawayNoPickFlag(null);

        if (NO.equals(wLocal.getLabeledPicking())) {
            //Ver V2 only in task queue ind

            if (ONE.equals(getPVersion())) {
                setPmStartLocation(glRow.getGlobalStartPickLoca());
                setPmPickType(glRow.getGlobalNoPickPackPickType()); //:PARAMETER.PM_Pick_Type := :GLOBAL.No_Pick_Pack_Pick_Type;
                setPmUpsCode(glRow.getGlobalUnitPickSystemCode()); // :PARAMETER.PM_UPS_CODE := :GLOBAL.unit_pick_system_code;
                setPmWaveDate(glRow.getGlobalWaveDate()); //:PARAMETER.PM_wave_date := to_date(:GLOBAL.wave_date, 'MM/DD/RRRR');
                setPmPickTypeC(glRow.getGlobalNoPickPackPickTypeC()); // :PARAMETER.PM_Pick_Type_C := :GLOBAL.No_Pick_Pack_Pick_Type_C;
                setPmPickTypeCp(glRow.getGlobalNoPickPackPickTypeCP()); // :PARAMETER.PM_Pick_Type_CP := :GLOBAL.No_Pick_Pack_Pick_Type_CP;

            } else {
                if (getPmTaskQueueInd() == null || !YES.equals(getPmTaskQueueInd())) {
                    if (glRow.getGlobalWaveNbr() != null)
                        setPmWaveNbr(new BigDecimal(glRow.getGlobalWaveNbr()));

                    setPmStartLocation(glRow.getGlobalStartPickLoca());
                    setPmPickType(glRow.getGlobalNoPickPackPickType());
                    setPmCallingForm(glRow.getGlobalCallingForm());
                    setPmPickTypeC(glRow.getGlobalNoPickPackPickTypeC()); // :PARAMETER.PM_Pick_Type_C := :GLOBAL.No_Pick_Pack_Pick_Type_C;
                    setPmPickTypeCp(glRow.getGlobalNoPickPackPickTypeCP()); // :PARAMETER.PM_Pick_Type_CP := :GLOBAL.No_Pick_Pack_Pick_Type_CP;
                }
            }

            if (YES.equals(allowProcessByAisle(work.getFacilityId(), glRow.getGlobalStartPickLoca()))) {
                wLocal.setProcessByAisle(locScpByWarehouse(work.getFacilityId(), glRow.getGlobalStartPickLoca()));
            } else
                wLocal.setProcessByAisle("N");

            //Since we are picking without a pick packet, lets display the pallet id to pick
            getGenericPickDirective();

            Object gotoFld = work.getGotoField();
            if ("BULK_PICK.CONF_LOCATION_ID".equals(gotoFld)) {
                work.setGotoField("GEN_CONT");
                setIsFocusOn((String) gotoFld);
            } else if ("BULK_PICK.PICK_TO_CONTAINER".equals(gotoFld)) {
                work.setGotoField("PIC_CONT");
                setIsFocusOn((String) gotoFld);
            } else {
                //set Focus Generic_To_Container
                work.setGotoField("GEN_CONT");
                setIsFocusOn("BULK_PICK.GENERIC_TO_CONTAINER");
            }
        } else {
            //set Focus Pick_To_Container_ID
            work.setGotoField("PIC_CONT");
            setIsFocusOn("BULK_PICK.PICK_TO_CONTAINER");
        }

        this.callSetAhlInfo();
        createOtherViewRowsIfNeeded();

        _logger.info("initTaskFlow End");
    }

    public void returnToGenericPickDirective() {
        getGenericPickDirective();

        HhBulkPkAcrossWvSWorkViewRowImpl work = getWorkViewCurrentRow();
        Object gotoFld = work.getGotoField();
        if ("BULK_PICK.CONF_LOCATION_ID".equals(gotoFld)) {
            work.setGotoField("GEN_CONT");
            setIsFocusOn((String) gotoFld);
        } else if ("BULK_PICK.PICK_TO_CONTAINER".equals(gotoFld)) {
            work.setGotoField("PIC_CONT");
            setIsFocusOn((String) gotoFld);
        } else {
            //set Focus Generic_To_Container
            work.setGotoField("GEN_CONT");
            setIsFocusOn("BULK_PICK.GENERIC_TO_CONTAINER");
        }
    }

    private void performOperation(String operName, boolean addVersion) {
        _logger.info("performOperation Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(operName);
        if (addVersion) {
            oper.getParamsMap().put("pVersion", getPVersion());
        }
        oper.execute();
        _logger.info("performOperation End");
    }

    private Object setWorkLocalVariables() {
        _logger.info("setWorkLocalVariables Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setWorkLocalVariables");
        oper.getParamsMap().put("pmTaskQueueInd", this.getPmTaskQueueInd());
        oper.getParamsMap().put("pVersion", getPVersion());

        _logger.info("setWorkLocalVariables End");
        return oper.execute();

    }

    public void callSetAhlInfo() {
        _logger.info("callSetAhlInfo End");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callSetAhlInfo");
        oper.getParamsMap().put("facilityId", getWorkViewCurrentRow().getFacilityId());
        if (getPVersion() != ONE)
            oper.getParamsMap().put("formName", "HH_BULK_PICK_S");
        else
            oper.getParamsMap().put("formName", "HH_BULK_PK_ACROSS_WV_S");
        oper.execute();

        _logger.info("callSetAhlInfo End");
    }

    public void getGenericPickDirective() {
        _logger.info("getGenericPickDirective Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("getGenericPickDirective");
        oper.getParamsMap().put("pVersion", getPVersion());
        oper.getParamsMap().put("pmWaveNbr", getPmWaveNbr());
        oper.getParamsMap().put("pmWaveDate", getPmWaveDate());
        oper.getParamsMap().put("pmPickTypeC", getPmPickTypeC());
        oper.getParamsMap().put("pmPickTypeCP", getPmPickTypeCp());
        oper.getParamsMap().put("pmPickType", getPmPickType());
        oper.getParamsMap().put("pmStartLocation", getPmStartLocation());
        oper.getParamsMap().put("pmUpsCode", getPmUpsCode());
        oper.getParamsMap().put("pmUpsGroupInd", getPmUpsGroupInd());
        oper.getParamsMap().put("pmTaskQueueInd",   getPmTaskQueueInd());
        oper.getParamsMap().put("pmRowid", getPmRowid());
        oper.getParamsMap().put("pmNoPickFlag", getPmNoPickFlag());
        oper.getParamsMap().put("pmNoTaskFlag", getPmNoTaskFlag());
        oper.getParamsMap().put("pmAllowIntrlvg", getPmAllowIntrlvg());
        Map returnValues = (Map) oper.execute();
        List<String> errors = null;
        if (returnValues != null) {
            Object errMsg = returnValues.get("ErrorMessage");
            if (errMsg != null) {
                errors = (List<String>) errMsg;
                setErrorMessage(errors.get(1));
            }
            Object noPickFlag = returnValues.get("NoPickFlag");
            if (noPickFlag != null)
                setPmNoPickFlag((String) noPickFlag);

            Object noTaskFlag = returnValues.get("NoTaskFlag");
            if (noTaskFlag != null)
                setPmNoTaskFlag((String) noTaskFlag);

            Object globalIntrlvg = returnValues.get("GlobalIntrlvg");

            if (globalIntrlvg != null) {
                setGlobalIntrlvg(globalIntrlvg);
                setReturnErrorMessage(getErrorMessage());
                if (null != errMsg && !errMsg.toString().isEmpty())
                    ADFContext.getCurrent().getPageFlowScope().put("pNoRemainingPick", "Y");
                // JSFUtils.addFacesErrorMessage(getErrorMessage());
                /*        if ("EXIT_INTRLVG".equals(globalIntrlvg)) {
                    ADFContext.getCurrent().getPageFlowScope().put("ExitForm", "Y");
                } else if ("INTRLVG".equals(globalIntrlvg))
                    ADFContext.getCurrent().getPageFlowScope().put("ExitForm",
                                                                   "PA"); */ //NEW_FORM( lower('hh_putaway_inventory_s'))
            }
        }
        _logger.info("getGenericPickDirective End");
    }

    protected void setGlobalIntrlvg(Object intrlvg) {
        _logger.info("setGlobalIntrlvg Start");
        setLGlobalIntrlvg((String) intrlvg);
        GlobalVariablesViewRowImpl glRow = getGlobalVariablesViewCurrentRow();
        if (glRow == null)
            return;
        if ("EXIT_INTRLVG".equals(intrlvg)) {
            glRow.setGlobalNoTaskFlag(NO);
            glRow.setGlobalAllowIntrlvg(NO);
            glRow.setGlobalNoPickFlag(NO);
            glRow.setGlobalCallingForm(null);
            glRow.setGlobalNoPutFlag(NO);
            glRow.setGlobalProcessByAisle(NO);
        } else {
            glRow.setGlobalNoTaskFlag(getPmNoTaskFlag());
            glRow.setGlobalAllowIntrlvg(getPmAllowIntrlvg());
            glRow.setGlobalNoPickFlag(getPmNoPickFlag());
            if (getPVersion() == ONE)
                glRow.setGlobalCallingForm("hh_bulk_pk_across_wv_s");
            else
                glRow.setGlobalCallingForm("hh_bulk_pick_s");
            glRow.setGlobalProcessByAisle(getWorkLocalViewCurrentRow().getProcessByAisle());
        }
        _logger.info("setGlobalIntrlvg End");
    }

    public static HhBulkPkAcrossWvSWorkViewRowImpl getWorkViewCurrentRow() {
        return (HhBulkPkAcrossWvSWorkViewRowImpl) (ADFUtils.findIterator("HhBulkPkAcrossWvSWorkViewIterator")).getCurrentRow();
    }

    public static HhBulkPkAcrossWvSWorkLocalViewRowImpl getWorkLocalViewCurrentRow() {
        return (HhBulkPkAcrossWvSWorkLocalViewRowImpl) ADFUtils.findIterator("HhBulkPkAcrossWvSWorkLocalViewIterator").getCurrentRow();
    }

    public static HhBulkPkAcrossWvSWorkLaborProdViewRowImpl getWorkLaborProdViewCurrentRow() {
        return (HhBulkPkAcrossWvSWorkLaborProdViewRowImpl) ADFUtils.findIterator("HhBulkPkAcrossWvSWorkLaborProdViewIterator").getCurrentRow();
    }

    public static HhBulkPkAcrossWvSLabelChildViewRowImpl getLabelChildViewCurrentRow() {
        return (HhBulkPkAcrossWvSLabelChildViewRowImpl) ADFUtils.findIterator("HhBulkPkAcrossWvSLabelChildViewIterator").getCurrentRow();
    }

    private void createOtherViewRowsIfNeeded() {
        _logger.info("createOtherViewRowsIfNeeded Start");
        DCIteratorBinding iter = ADFUtils.findIterator("HhBulkPkAcrossWvSContainerViewIterator");
        Object rr = iter.getCurrentRow();
        if (rr == null) {
            Row row = iter.getViewObject().createRow();
            iter.getViewObject().insertRow(row);
        }
        _logger.info("createOtherViewRowsIfNeeded End");
    }

    private void initParams() {
        _logger.info("initParams Start");
        setIsFocusOn("BULK_PICK.GENERIC_TO_CONTAINER");

        GlobalVariablesViewRowImpl row = getGlobalVariablesViewCurrentRow();
        this.setPmAllowIntrlvg(row.getGlobalAllowIntrlvg());


        Object noTaskFlag = row.getGlobalNoTaskFlag();
        Object noPickFlag = row.getGlobalNoPickFlag();

        if (getPVersion() == ONE) {
            this.setPmNoTaskFlag(noTaskFlag == null ? "N" : (String) noTaskFlag);
            this.setPmNoPickFlag(noPickFlag == null ? "N" : (String) noPickFlag);
        } else {
            this.setPmNoTaskFlag((String) noTaskFlag);
            this.setPmNoPickFlag((String) noPickFlag);
        }
        _logger.info("initParams End");
    }

    public static GlobalVariablesViewRowImpl getGlobalVariablesViewCurrentRow() {
        return (GlobalVariablesViewRowImpl) (ADFUtils.findIterator("GlobalVariablesViewIterator")).getCurrentRow();

    }

    private String allowProcessByAisle(String facilityId, String startPickLoc) {
        _logger.info("allowProcessByAisle Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("allowProcessByAisle");
        if (op != null) {
            op.getParamsMap().put("facilityId", facilityId);
            op.getParamsMap().put("startPickLoc", startPickLoc);
            _logger.info("allowProcessByAisle End");
            return (String) op.execute();
        }
        _logger.info("allowProcessByAisle End");
        return "N";
    }

    private String locScpByWarehouse(String facilityId, String startPickLoc) {
        _logger.info("locScpByWarehouse Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("locScpByWarehouse");
        if (op != null) {
            op.getParamsMap().put("facilityId", facilityId);
            op.getParamsMap().put("startPickLoc", startPickLoc);
            op.getParamsMap().put("scpName", "process_by_aisle");
            _logger.info("locScpByWarehouse End");
            return (String) op.execute();
        }
        _logger.info("locScpByWarehouse End");
        return "N";
    }

    /**
     * sends a partial page submit for the given component
     *
     * @param  component on page
     */
    public void refreshContentOfUIComponent(UIComponent component) {
        _logger.info("refreshContentOfUIComponent Start");
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(component);
        _logger.info("refreshContentOfUIComponent End");
    }

    public void setPmTaskQueueInd(String pmTaskQueueInd) {
        this.pmTaskQueueInd = pmTaskQueueInd;
    }

    public String getPmTaskQueueInd() {
        return pmTaskQueueInd;
    }

    public void setPmAllowIntrlvg(String pmAllowIntrlvg) {
        this.pmAllowIntrlvg = pmAllowIntrlvg;
    }

    public String getPmAllowIntrlvg() {
        return pmAllowIntrlvg;
    }

    public void setPmNoTaskFlag(String pmNoTaskFlag) {
        this.pmNoTaskFlag = pmNoTaskFlag;
    }

    public String getPmNoTaskFlag() {
        return pmNoTaskFlag;
    }

    public void setPmNoPickFlag(String pmNoPickFlag) {
        this.pmNoPickFlag = pmNoPickFlag;
    }

    public String getPmNoPickFlag() {
        return pmNoPickFlag;
    }

    public void setPmRowid(String pmRowid) {
        this.pmRowid = pmRowid;
    }

    public String getPmRowid() {
        return pmRowid;
    }

    public void setPmWaveNbr(BigDecimal pmWaveNbr) {
        this.pmWaveNbr = pmWaveNbr;
    }

    public BigDecimal getPmWaveNbr() {
        return pmWaveNbr;
    }

    public void setPmPickType(String pmPickType) {
        this.pmPickType = pmPickType;
    }

    public String getPmPickType() {
        return pmPickType;
    }

    public void setPmPickTypeC(String pmPickTypeC) {
        this.pmPickTypeC = pmPickTypeC;
    }

    public String getPmPickTypeC() {
        return pmPickTypeC;
    }

    public void setPmPickTypeCp(String pmPickTypeCp) {
        this.pmPickTypeCp = pmPickTypeCp;
    }

    public String getPmPickTypeCp() {
        return pmPickTypeCp;
    }

    public void setPmStartLocation(String pmStartLocation) {
        this.pmStartLocation = pmStartLocation;
    }

    public String getPmStartLocation() {
        return pmStartLocation;
    }

    public void setPmCallingForm(String pmCallingForm) {
        this.pmCallingForm = pmCallingForm;
    }

    public String getPmCallingForm() {
        return pmCallingForm;
    }

    public void setPmUpsCode(String pmUpsCode) {
        this.pmUpsCode = pmUpsCode;
    }

    public String getPmUpsCode() {
        return pmUpsCode;
    }

    public void setPmWaveDate(Date pmWaveDate) {
        this.pmWaveDate = pmWaveDate;
    }

    public Date getPmWaveDate() {
        return pmWaveDate;
    }

    public void setPmUpsGroupInd(String pmUpsGroupInd) {
        this.pmUpsGroupInd = pmUpsGroupInd;
    }

    public String getPmUpsGroupInd() {
        return pmUpsGroupInd;
    }

    public void setPVersion(BigDecimal pVersion) {
        this.pVersion = pVersion;
    }

    public BigDecimal getPVersion() {
        return pVersion;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setBypassPick(String bypassPick) {
        this.bypassPick = bypassPick;
    }

    public String getBypassPick() {
        return bypassPick;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return formName;
    }


}
