package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLocalViewRowImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends BulkPickBaseBean {

    private RichOutputText errorWarnInfoMessage;
    private RichIcon errorWarnInfoIcon;
    private RichPanelGroupLayout allMessagesPanel;
    private RichInputText toLocationId;
    private RichLink f4Done;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichIcon toLocationIdIcon;

    public Page2Backing() {
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        this.setFocusOnUIComponent(this.getToLocationId());
        _logger.info("onRegionLoad End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEY_PRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEY_PRESSED);

        if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Done());
                actionEvent.queue();
            }
        }
        _logger.info("performKey End");
    }
    public void F3KeyListener(ActionEvent actionEvent) {
        _logger.info("F3KeyListener Start");
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
        wLocal.setFirstScanFlag(YES);
        Object ptype = wLocal.getPickType();
        if (PICK_BP.equals(ptype) || PICK_BR.equals(ptype) || PICK_B.equals(ptype) || PICK_CP.equals(ptype) || PICK_C.equals(ptype)) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, CAN_NOT_CANCEL );
        } else if (NO.equals(wLocal.getLabeledPicking())) {
            ADFUtils.invokeAction("clearBlock");
        } else {
            ADFUtils.invokeAction("backGlobalHome");
        }
        _logger.info("F3KeyListener End");
    }

    public void f4DoneListener(ActionEvent actionEvent) {
        _logger.info("f4DoneListener Start");
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_bulk_pk_across_wv_s_TO_LOCATION_F4,
                                                isBulkPickNotMain() ? RoleBasedAccessConstants.FORM_NAME_hh_bulk_pick_s :
                                                RoleBasedAccessConstants.FORM_NAME_hh_bulk_pk_across_wv_s)) {
            setErrorStyleOnComponent(getToLocationId(), getToLocationIdIcon(), false);
            hideMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel());
            String toLocation = getBoundAttribute(ATTR_TO_LOCATION_ID);
            if (toLocation == null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), ERROR,
                                        PARTIAL_ENTRY);
                setErrorStyleOnComponent(getToLocationId(), getToLocationIdIcon(), true);
            } else {
                toLocDoneKey();
            }

        } else {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), ERROR,
                                    "NOT_ALLOWED");
        }
        _logger.info("f4DoneListener End");
    }
    
    private void toLocDoneKey(){
        _logger.info("toLocDoneKey Start");
           OperationBinding oper = (OperationBinding) ADFUtils.findOperation(OPR_TO_LOC_DONE_KEY);
            if (oper != null){
                oper.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion());
                oper.getParamsMap().put("pmAllowIntrlvg", getBulkPkAcrossWvPageFlowBean().getPmAllowIntrlvg());
                oper.getParamsMap().put("pmNoPickFlag", getBulkPkAcrossWvPageFlowBean().getPmNoPickFlag());
                oper.getParamsMap().put("pmNoTaskFlag", getBulkPkAcrossWvPageFlowBean().getPmNoTaskFlag());
                
                Map returnValues = (Map)oper.execute();
                List<String> errors = new ArrayList<String>(); 
                if (returnValues != null)
                         errors = ( List<String>) returnValues.get("ErrorMessage");
                else return;
                if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)){ //return if errors 
                    setErrorStyleOnComponent(getToLocationId(),getToLocationIdIcon(),true );
                    return;
                }
                Object gotoField    = returnValues.get("GoToField");
                Object callingBlock = returnValues.get("CallingBlock");
                Object pmNoPickFlag = returnValues.get("pmNoPickFlag");
                Object pmNoTaskFlag = returnValues.get("pmNoTaskFlag");
                Object globalIntrlvg = returnValues.get("GlobalIntrlvg");
                
                if (pmNoPickFlag != null)
                    getBulkPkAcrossWvPageFlowBean().setPmNoPickFlag((String)pmNoPickFlag);
                if (pmNoTaskFlag != null)
                    getBulkPkAcrossWvPageFlowBean().setPmNoTaskFlag((String)pmNoTaskFlag);
                
                if (gotoField != null){
                    getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String)gotoField);                    
                }
                if (INTRLVG.equals(globalIntrlvg)){
                    getBulkPkAcrossWvPageFlowBean().setGlobalIntrlvg(INTRLVG);
                    ADFUtils.invokeAction(INTRLVG);
                }
                   
                else
                    performNavigation (callingBlock,gotoField);  
            } 
    
       // performNavigation(null,TOTAL_WEIGHT);
       _logger.info("toLocDoneKey End");
    }
    
    private void performNavigation(Object callingBlock, Object gotoField){
        _logger.info("performNavigation Start");
        if (gotoField != null){
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String)gotoField);
        }

        if (callingBlock != null && CONTAINER.equals(callingBlock))
           ADFUtils.invokeAction("container"); 
        else if (gotoField != null && gotoField.toString().startsWith(WEIGHT))
           ADFUtils.invokeAction("weight");
        else if (gotoField != null && gotoField.toString().startsWith(LABEL_CHILD))
           ADFUtils.invokeAction("labelChild");
        else if (gotoField != null && gotoField.toString().startsWith(BULK_PICK))
           ADFUtils.invokeAction("clearBlock");
        else if (gotoField != null && gotoField.toString().startsWith("EXIT"))
           ADFUtils.invokeAction("backGlobalHome");         
        
        _logger.info("performNavigation End");
    }
    
    public void setToLocationId(RichInputText toLocationId) {
        this.toLocationId = toLocationId;
    }

    public RichInputText getToLocationId() {
        return toLocationId;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }



    public void setF4Done(RichLink f4Done) {
        this.f4Done = f4Done;
    }

    public RichLink getF4Done() {
        return f4Done;
    }


    public void setToLocationIdIcon(RichIcon toLocationIdIcon) {
        this.toLocationIdIcon = toLocationIdIcon;
    }

    public RichIcon getToLocationIdIcon() {
        return toLocationIdIcon;
    }
}
