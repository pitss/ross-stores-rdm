package com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSLabelChildViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLocalViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkViewRowImpl;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page1.jspx
// ---    
// ---------------------------------------------------------------------
public class Page1Backing extends BulkPickBaseBean  {


    private RichInputText genericToContainer;
    private RichInputText pickToContainer;
    private RichInputText confLocationId;
    private RichIcon containerIdIcon;
    private RichPanelFormLayout myviewPFL;
    private boolean confContainerDisabled = true; 
    
    private RichPanelFormLayout mainPanelForm;
    private RichIcon containerQtyIcon;
    private RichIcon confContainerIdIcon;
    private RichIcon confLocationIdIcon;
    private RichLink f3Exit;
    private RichLink f4Done;
    private RichLink f6Lpn;
    private RichLink f7Bypass;
    private RichInputText confContainerId;
    private RichInputText containerQty;
    private RichOutputText popupMessage;
    private RichPopup gotoPopup;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichDialog dgConfDiffPick;
    private RichOutputText confDiffPickText;
    private RichPopup ppConfDiffPick;
    private RichPopup confEmptyPopup;
    private RichOutputText confEmptyMessage;

    public Page1Backing() {

  }
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelGroupLayout allMessagesPanel; 
    
    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey(KEY_PRESSED)) {
            Double keyPressed = (Double) clientEvent.getParameters().get(KEY_PRESSED);
            
            if (F3_KEY_CODE.equals(keyPressed)) {
                ADFContext.getCurrent().getRequestScope().put("F3Opr", YES);
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            }
            else if (F7_KEY_CODE.equals(keyPressed)) {
                 ActionEvent  actionEvent = new ActionEvent(this.getF7Bypass());  
                 actionEvent.queue();
                }
            else if (! isValidationError()) {//no validation errors present, continue navigation
                ActionEvent actionEvent = null;
                 if (F4_KEY_CODE.equals(keyPressed)) {
                     actionEvent = new ActionEvent(this.getF4Done());
                } else if (F6_KEY_CODE.equals(keyPressed)) {
                     actionEvent = new ActionEvent(this.getF6Lpn());
                } 
                if (actionEvent != null ) 
                    actionEvent.queue();
            }
        }
        _logger.info("performKey End");
    }
    public void onLoadListener(ActionEvent actionEvent) { 
        try {
             _logger.info("OnLoadListener Start"); 
         Object gotoFld = getBulkPkAcrossWvPageFlowBean().getIsFocusOn();//  work.getGotoField();
            
         if (CONFIRM_LOC.equals(gotoFld)){
             if ( getBulkPkAcrossWvPageFlowBean().isReturnFromOtherPage()){
                 getBulkPkAcrossWvPageFlowBean().setReturnFromOtherPage(false);
                 validateConfLocation( getBoundAttribute(ATTR_CONF_LOCATION_ID) );
             }
             setFocusOnUIComponent(getConfLocationId());
         }
         else if (PICK_CONTAINER.equals(gotoFld)){       
             setFocusOnUIComponent(getPickToContainer());
         }
         else if (CONFIRM_CONT.equals(gotoFld)){
             if ( getBulkPkAcrossWvPageFlowBean().isReturnFromOtherPage()){
                 getBulkPkAcrossWvPageFlowBean().setReturnFromOtherPage(false);
                 validateConfContainer( getBoundAttribute(ATTR_CONF_CONTAINER_ID) );
             }
             setFocusOnUIComponent(getConfContainerId());
         }
         else if (CONTAINER_QTY.equals(gotoFld)){
             setFocusOnUIComponent(getContainerQty());
         }
        else {
             setFocusOnUIComponent(getGenericToContainer());
         }

         getContainerIdIcon().setVisible(false);

         if (getBulkPkAcrossWvPageFlowBean().getErrorMessage() != null){
             showMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(), ERROR, getBulkPkAcrossWvPageFlowBean().getErrorMessage());
         } 
             _logger.info("OnLoadListener End");
         }
         catch (Exception e){
             _logger.severe("OnLoadListener: Unexpected exception: ", e.getMessage());
         }
        finally { 
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMainPanelForm());
        }
    } 
  
    private boolean validateFromLocation(){
        _logger.info("validateFromLocation Start");
        if (getBoundAttribute(ATTR_FROM_LOCATION_ID) != null){
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation(OPR_V_FROM_CONTAINER);
             
            opr.getParamsMap().put("bpFromContainerId", getBoundAttribute(ATTR_PICK_FROM_CONT_ID));
            opr.getParamsMap().put("facilityId",        getBoundAttribute(ATTR_FACILITY_ID));
            opr.getParamsMap().put("wlFromContainerId", getBoundAttribute(ATTR_WL_PICK_FROM_CONTAINER));
            opr.getParamsMap().put("locationId",        getBoundAttribute(ATTR_FROM_LOCATION_ID));
            List<String> errors = (List<String>)opr.execute();
            
            if (errors != null && errors.size()> 0){
                showMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, errors.get(0));
                return false;
            }
           getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONFIRM_LOC); 
            _logger.info("validateFromLocation End");
           return true;
        }
        _logger.info("validateFromLocation End");
        return false;
    }
     
 
  public void onChangedGenericToContainerBulkPick(ValueChangeEvent vce) {
      _logger.info("onChangedGenericToContainerBulkPick Start");
      //no need to validate if F3-Exit key was clicked
      if (YES.equals(ADFContext.getCurrent().getRequestScope().get("F3Opr"))) return;
     if (vce.getNewValue() != null && ! NONE.equals(vce.getNewValue())){
        hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel());
        ADFUtils.setBoundAttributeValue(ATTR_GENERIC_TO_CONTAINER, ((String) vce.getNewValue()).toUpperCase());   
        if ( validateFromLocation()){ 
         //   setFocusOnUIComponent(getConfLocationId());
        }         
    }
    else {
        getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, INV_CONTAINER); 
    }
      _logger.info("onChangedGenericToContainerBulkPick End");
  }

  public void onChangedPickToContainerIdBulkPick(ValueChangeEvent vce) {
      _logger.info("onChangedPickToContainerIdBulkPick Start");
      //no need to validate if F3-Exit key was clicked
      if (YES.equals(ADFContext.getCurrent().getRequestScope().get("F3Opr"))) return;

      getPickDirective();
       
      if (vce.getNewValue() != null){
          hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel());
          ADFUtils.setBoundAttributeValue(ATTR_PICK_TO_CONTAINER_ID, ((String) vce.getNewValue()).toUpperCase());   
          if ( !validateFromLocation()){
              return;
              } 
      }
      getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONFIRM_LOC);
  //    setFocusOnUIComponent(getConfLocationId());
    _logger.info("onChangedPickToContainerIdBulkPick End");
  }

    private void getPickDirective() {
        _logger.info("getPickDirective Start");
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation(OPR_GET_PICK_DIRECTIVE);
        if (opr != null) {
            opr.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion());
            Map returnValues = (Map) opr.execute();
            List<String> errors = (List<String>) returnValues.get("Errors");
            if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)) { //return if errors
                return;
            }
            Object gotoField = returnValues.get("GoToField");
            Object callingBlock = returnValues.get("CallingBlock"); 
            performNavigation (callingBlock,gotoField); 
        }
        _logger.info("getPickDirective End");
    }
 
    public void validateConfLocation(String confLocId){
        _logger.info("validateConfLocation Start"); 
        if (confLocId == null) return;
        hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel());
        setValidationError(false); 
        String fromLocId = getBoundAttribute(ATTR_FROM_LOCATION_ID);
        if ( ( fromLocId != null &&  !fromLocId.equals(confLocId.toUpperCase()) )  ){
            getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, INV_LOCATION );
            setErrorStyleOnComponent(getConfLocationId(),getConfLocationIdIcon(),true );
            //stay on the field
            setValidationError(true);
            _logger.warning("validateConfLocation: Invalid Location");
            return;
        }
 //       ADFUtils.setBoundAttributeValue(ATTR_PICK_TO_CONTAINER_ID, (confLocId).toUpperCase());  
        setErrorStyleOnComponent(getConfLocationId(),getConfLocationIdIcon(),false );
        getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONFIRM_CONT);
        _logger.info("validateConfLocation End");
    }
    
  public void onChangedConfLocationIdBulkPick(ValueChangeEvent vce) {
      _logger.info("onChangedConfLocationIdBulkPick Start");
      //no need to validate if F3-Exit key was clicked
      if (YES.equals(ADFContext.getCurrent().getRequestScope().get("F3Opr"))) return;

      validateConfLocation((String)vce.getNewValue());  
    _logger.info("onChangedConfLocationIdBulkPick End");
  }
  
  private void validateConfContainer(String confContId) {
      if (confContId == null) return;
      _logger.info("validateConfContainer Start");
      setValidationError(false);
      hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel()); 
      String fromContId = getBoundAttribute(ATTR_PICK_FROM_CONT_ID);
      if ( ( fromContId != null &&  !fromContId.equals(confContId.toUpperCase()) )  ){
          getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, INV_CONTAINER );
          setErrorStyleOnComponent(getConfContainerId(),getConfContainerIdIcon(),true );
          //stay on the field
          setValidationError(true);         
          
          _logger.info("validateConfContainer validation failure: Invalid Container");
          return;
      }     
      setErrorStyleOnComponent(getConfContainerId(),getConfContainerIdIcon(),false );
      OperationBinding oper = (OperationBinding) ADFUtils.findOperation(OPR_CONF_CONTAINER_NEXT);
      Object mesg = oper.execute();
      if (mesg != null){
          showMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, (String)mesg);
          setValidationError(true);
          _logger.info("validateConfContainer Error encountered: " + mesg);
          return;
      }
      getBulkPkAcrossWvPageFlowBean().setIsFocusOn(CONTAINER_QTY); 
      //   setFocusOnUIComponent(getContainerQty());
      _logger.info("validateConfContainer End");
  }
  public void onChangedConfContainerIdBulkPick(ValueChangeEvent vce) {
      _logger.info("onChangedConfContainerIdBulkPick Start");
      //no need to validate if F3-Exit key was clicked
      if (YES.equals(ADFContext.getCurrent().getRequestScope().get("F3Opr"))) return;

      validateConfContainer((String)vce.getNewValue()); 
    _logger.info("onChangedConfContainerIdBulkPick End");
  }

  public void onChangedContainerQtyBulkPick(ValueChangeEvent vce) {
      //no need to validate if F3-Exit key was clicked
      if (YES.equals(ADFContext.getCurrent().getRequestScope().get("F3Opr"))) return;

      Object val = vce.getNewValue();
      if (null != val && Pattern.matches("[0-9]+", (String)val) == true) { //valid data 
             setValidationError(false); 
             hideMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel());
             setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),false );
          }
      else { //non-numeric data
              setValidationError(true); 
              getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, MUST_BE_NUMERIC );
              setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true ); 
          }
      
  }

  public void onChangedContainerQty2WorkLocal(ValueChangeEvent vce) {
    
  }
 

    public void F3KeyListener(ActionEvent actionEvent) {
        _logger.info("F3KeyListener Start");
            getBulkPkAcrossWvPageFlowBean().setGlobalIntrlvg("EXIT_INTRLVG");
            OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_TR_EXIT_BP);
            opr.execute();
            int depth = getPageFlowStackDepth();
            
            if ("TQ".equals(getBulkPkAcrossWvPageFlowBean().getPmCallingForm())){
                popTaskFlow(3);
                return;
            }
        
            if (depth >= 4)
              popTaskFlow(depth-2);
            else
              popTaskFlow(2);
              //ADFUtils.invokeAction("backGlobalHome");
            
        _logger.info("F3KeyListener End");
    }
    
    private boolean validateEntry(){
        boolean isValid = true;
        Object containerQty = ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY); 
        Object conLocId= getBoundAttribute(ATTR_CONF_LOCATION_ID);
        Object conConId= getBoundAttribute(ATTR_CONF_CONTAINER_ID);
        if ( conConId == null || conLocId == null || containerQty == null) {
            isValid = false;
                getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, PARTIAL_ENTRY);
              
             if (conLocId == null)
                setErrorStyleOnComponent(getConfLocationId(),getConfLocationIdIcon(),true );
             else if (conConId == null)
                setErrorStyleOnComponent(getConfContainerId(),getConfContainerIdIcon(),true );
            else if (containerQty == null)
                setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
        }
        return isValid;
    }
    
    public void F4KeyListener(ActionEvent actionEvent) {
        _logger.info("F4KeyListener Start");
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_bulk_pk_across_wv_s_BULK_PICK_F4,
                                                isBulkPickMain() ? RoleBasedAccessConstants.FORM_NAME_hh_bulk_pick_s :
                                                RoleBasedAccessConstants.FORM_NAME_hh_bulk_pk_across_wv_s)) {
            if (isValidationError()) {
                _logger.warning("F4KeyListener Validation Errors exist, returning no-op");
                return;
            } //do not perform action if validation errors are present
            hideMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel());

            if (!validateEntry()) {
                return;
            } else {
                if (PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE)) ||
                    (!PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE)) &&
                     CONTAINER_QTY.equals(getBulkPkAcrossWvPageFlowBean().getIsFocusOn()))) { // OR current field is BP container qty
                    validateQty();
                }
            }
        } else {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), ERROR,
                                    "NOT_ALLOWED");
        }
        _logger.info("F4KeyListener End");
    }
    
    public void F6KeyListener(ActionEvent actionEvent) {
        _logger.info("F6KeyListener Start");
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_bulk_pk_across_wv_s_BULK_PICK_F6,
                                                isBulkPickMain() ? RoleBasedAccessConstants.FORM_NAME_hh_bulk_pick_s :
                                                RoleBasedAccessConstants.FORM_NAME_hh_bulk_pk_across_wv_s)) {
            if (isValidationError()) { //do not perform action if validation errors are present
                _logger.warning("F6KeyListener Validation Errors exist, returning no-op");
                return;
            }
            ;

            if (!validateEntry()) {
                return;
            } else {
                Object pickContQty = ADFUtils.getBoundAttributeValue("PickContainerQty");
                int pickContQtyInt = 0;
                int contQtyInt = 0;
                if (pickContQty != null) {
                    pickContQtyInt = ((BigDecimal) pickContQty).intValue();
                }
                Object containerQty = ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY);
                if (containerQty != null) {
                    contQtyInt = (new BigDecimal((String) containerQty)).intValue();
                }

                if (contQtyInt <= 0 ||
                    (!PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE)) && contQtyInt > pickContQtyInt)) {
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            ERROR, INV_QTY);
                    setErrorStyleOnComponent(getContainerQty(), getContainerQtyIcon(), true);
                    return;
                } else {
                    //                RichLink rcb = (RichLink)actionEvent.getSource();
                    //                String focusOn = (String)rcb.getAttributes().get("focusField");
                    String focusOn = getBulkPkAcrossWvPageFlowBean().getIsFocusOn();
                    if (CONTAINER_QTY.equals(focusOn)) {
                        if (PICK_C.equals(getBoundAttribute(ATTR_PICK_TYPE)) && contQtyInt != pickContQtyInt) {
                            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(),
                                                    getAllMessagesPanel(), WARN, DIFF_PICK);
                            setErrorStyleOnComponent(getContainerQty(), getContainerQtyIcon(), true);
                            return;
                        }
                        displayLPNs();
                    }
                }
            }
            displayLPNs();

        } else {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), ERROR,
                                    "NOT_ALLOWED");
        }
        _logger.info("F6KeyListener End");
    }
    
    public void F7BypassListener(ActionEvent actionEvent) {
        _logger.info("F7BypassListener Start");
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_bulk_pk_across_wv_s_BULK_PICK_F7,
                                                isBulkPickMain() ? RoleBasedAccessConstants.FORM_NAME_hh_bulk_pick_s :
                                                RoleBasedAccessConstants.FORM_NAME_hh_bulk_pk_across_wv_s)) {
            if (ADFUtils.getBoundAttributeValue(ATTR_PICK_TO_CONTAINER_ID) == null) {
                return;
            }

            OperationBinding opr = (OperationBinding) ADFUtils.findOperation(OPR_BYPASS_KEY_PRESS);
            if (opr != null) {
                opr.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion());
                Map returnValues = (Map) opr.execute();
                List<String> errors = (List<String>) returnValues.get("Errors");
                if (processMessages(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), errors)) { //return if errors
                    return;
                }
                Object gotoField = returnValues.get("GoToField");
                Object callingBlock = returnValues.get("CallingBlock");
                Object clearBlock = returnValues.get("ClearBlock");
                if (GEN_CONTAINER.equals(callingBlock) || GEN_CONTAINER.equals(gotoField)) {
                    if (clearBlock != null && "T".equals(clearBlock)) {
                        ADFUtils.invokeAction("clearBlock");
                        return;
                    } else {
                        getBulkPkAcrossWvPageFlowBean().setIsFocusOn(GEN_CONTAINER);
                        ADFUtils.invokeAction("genericPick");
                        return;
                    }

                } else if ("REASON".equals(gotoField)) {
                    ADFUtils.invokeAction("F7BYPASS");
                } else if (clearBlock != null && "T".equals(clearBlock)) {
                    clearBlock();
                }
                if (gotoField != null) {
                    //Set previous item before setting the next item to navigate.
                    String prevItem = getBulkPkAcrossWvPageFlowBean().getIsFocusOn();
                    HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
                    wLocal.setPreviousItem(prevItem);

                    getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String) gotoField);
                }
            }
        } else {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), ERROR,
                                    "NOT_ALLOWED");
        }
        _logger.info("F7BypassListener End");
    }

    private void clearBlock(){
        _logger.info("clearBlock Start");
        ADFUtils.setBoundAttributeValue(ATTR_CONF_LOCATION_ID, null);
        ADFUtils.setBoundAttributeValue(ATTR_CONF_CONTAINER_ID, null);
        ADFUtils.setBoundAttributeValue(ATTR_CONTAINER_QTY, null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMainPanelForm());
        _logger.info("clearBlock End");
    }
    
    public void displayLPNs(){
        _logger.info("displayLPNs Start");
        OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_QUERY_LPN);
        if (opr != null)
          opr.execute();
        
        DCIteratorBinding iter = ADFUtils.findIterator(ITER_LPN);
        if (iter == null || iter.getEstimatedRowCount() <= 0){
            getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, NO_DATA ); 
            return;
        }
        else     //navigate only if LPNs found
           ADFUtils.invokeAction("F6LPN");
        
        _logger.info("displayLPNs End");
    }
    private BigDecimal getNonNullNumber(Object val){
        if (val == null)
           return ZERO;
        return (BigDecimal)val;
    }
    private void validateQty(){  
        _logger.info("validateQty Start");
           Object cntQty = ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY);
           
           if (cntQty == null    ){
               getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, PARTIAL_ENTRY); 
               setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
               return; 
           }
           else if  (new BigDecimal((String)cntQty).intValue() <= 0 ){
               getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, INV_QTY); 
               setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
               return; 
           }
           else {
               HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
               BigDecimal qty = new BigDecimal( (String)cntQty);
               BigDecimal pickCntQty = getNonNullNumber(wLocal.getPickContainerQty());
               BigDecimal origPckQty = getNonNullNumber(wLocal.getOriginalPickQty());
               BigDecimal cntQty2    = getNonNullNumber(wLocal.getContainerQty2());
          
               String wlPickType = wLocal.getPickType();
               if (((( qty.intValue() > pickCntQty.intValue() ) || ( qty.intValue()  > origPckQty.intValue() )) &&  ! PICK_C.equals(wlPickType)  )
                 ||(( qty.intValue() == 0 ||  cntQty2.intValue() == 0 ) && PICK_C.equals(wlPickType))){
                       getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, INV_QTY); 
                       setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
                       return;        
                   }
           }
          HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
          String wlPickType = wLocal.getPickType();
          BigDecimal pickCntQty = getNonNullNumber(wLocal.getPickContainerQty());
          BigDecimal qty = new BigDecimal((String)cntQty);
          int eventCode = -1;
           if (PICK_BP.equals(wlPickType))
               eventCode = 26;
           else if(PICK_CP.equals(wlPickType))
               eventCode = 51;
           else if(PICK_C.equals(wlPickType))
               eventCode = 52;   
           wLocal.setEventCode(new BigDecimal(eventCode));
        
           if (( qty.intValue() < pickCntQty.intValue() ) && !PICK_C.equals(wlPickType)) {
//               getConfDiffPickText().setValue( getMessage(DIFF_PICK, CONF ) ); 
//               this.getPpConfDiffPick().show(new RichPopup.PopupHints());                
               getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, DIFF_PICK); 
               setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
               return;                
           }
           else if (( qty.intValue() != pickCntQty.intValue() ) &&  PICK_C.equals(wlPickType)){
               eventCode = 27;
               wLocal.setEventCode(new BigDecimal(eventCode));
               String diffPick = getMessage(DIFF_PICK,ERROR);
               ADFContext.getCurrent().getViewScope().put("CBFrom",STR_0);
               getConfDiffPickText().setValue(diffPick);
               getPpConfDiffPick().show(new RichPopup.PopupHints());
//               getAndShowMessagesPanel(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),ERROR, DIFF_PICK); 
//               setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
               return;
           } 
           validateQty2(); 
       _logger.info("validateQty End");
    }

    private void validateQty2(){
        String t_old_calling_block = null;
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
        Object cntQty = ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY);
        String wlPickType = wLocal.getPickType();
        BigDecimal pickCntQty = getNonNullNumber(wLocal.getPickContainerQty());
        BigDecimal qty = new BigDecimal( (String)cntQty);
        if ( ( qty.intValue() < pickCntQty.intValue()  && !PICK_C.equals(wlPickType) )||
             (qty.intValue() != pickCntQty.intValue() && PICK_C.equals(wlPickType))){
            String lblRsrv = wLocal.getLabeledReserve();

            HhBulkPkAcrossWvSWorkViewRowImpl work = HhBulkPkAcrossWvSBean.getWorkViewCurrentRow();
            if (YES.equals(lblRsrv)){
                if (YES.equals(wLocal.getEmptyPressed())){
                    ADFContext.getCurrent().getViewScope().put("CBFrom",STR_1);
                    checkEmpty( getBoundAttribute(ATTR_FROM_LOCATION_ID),  getBoundAttribute(ATTR_PICK_FROM_CONT_ID),wLocal.getEmptyPressed());
                }
                //scan_all_containers
                ADFUtils.invokeAction("container");
            }
            else if(NO.equals(lblRsrv) && (PICK_BP.equals(wlPickType) ||PICK_B.equals(wlPickType) ||PICK_CP.equals(wlPickType) ||PICK_C.equals(wlPickType) )){
                if (YES.equals(wLocal.getLabeledPicking())){
                    OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CheckForChildren");                    
                     oper.getParamsMap().put("pFacilityId", getBoundAttribute(ATTR_FACILITY_ID));
                     oper.getParamsMap().put("pPickToContainerId", getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID));
                        
                         Object dummy2 =  oper.execute();
                         if (dummy2 != null){
                            // t_old_calling_block = work.getCallingBlock();
                             wLocal.setFirstWeightFlag(YES);
                             work.setCallingBlock(BULK_PICK);
                             
                             HhBulkPkAcrossWvSLabelChildViewRowImpl labelChild = HhBulkPkAcrossWvSBean.getLabelChildViewCurrentRow();
                             if (YES.equals(wLocal.getFirstScanFlag())){
                                 labelChild.setMasterToContainer(getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID));
                                 labelChild.setContainerQty((BigDecimal)ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY));
                             }
                             else {
                                  OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("CountSysChild");                                  
                                  oper2.getParamsMap().put("pFacilityId", getBoundAttribute(ATTR_FACILITY_ID));
                                  oper2.getParamsMap().put("pPickToContainerId", getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID));
                                  Object t_child =  oper2.execute();
                                 labelChild.setContainerQty((BigDecimal)t_child);
                             }
                             ADFUtils.invokeAction("labelChild");
                         }
                         else { //dumm2 null
                             if (YES.equals(wLocal.getEmptyPressed())){
                                 ADFContext.getCurrent().getViewScope().put("CBFrom",STR_2);
                                 checkEmpty( getBoundAttribute(ATTR_FROM_LOCATION_ID),  getBoundAttribute(ATTR_PICK_FROM_CONT_ID),wLocal.getEmptyPressed());
                             }
                         }
                }
                else {// labeled_picking = N --
                    if( (PICK_BP.equals(wlPickType)   ||PICK_CP.equals(wlPickType) ||PICK_C.equals(wlPickType) )){
                        userDoneProcToLocation();
                        return;
                    }
                    else if (PICK_B.equals(wlPickType)){
                        if (YES.equals(wLocal.getEmptyPressed())){
                            ADFContext.getCurrent().getViewScope().put("CBFrom",STR_3);
                            checkEmpty( getBoundAttribute(ATTR_FROM_LOCATION_ID),  getBoundAttribute(ATTR_PICK_FROM_CONT_ID),wLocal.getEmptyPressed());
                        } 
                    }
                }
            }
            else  
                userDoneProcToLocation();
           
        }
        else {
            ADFContext.getCurrent().getViewScope().put("CBFrom",STR_4);
            String cont = checkEmpty( getBoundAttribute(ATTR_FROM_LOCATION_ID),  getBoundAttribute(ATTR_PICK_FROM_CONT_ID),wLocal.getEmptyPressed());
            if (YES.equals(cont))
                validateQty5();
        }
            
    }
    
    private void validateQty5() {
        String t_old_calling_block = null;
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
        HhBulkPkAcrossWvSWorkViewRowImpl work = HhBulkPkAcrossWvSBean.getWorkViewCurrentRow();
        HhBulkPkAcrossWvSLabelChildViewRowImpl labelChild = HhBulkPkAcrossWvSBean.getLabelChildViewCurrentRow();
        t_old_calling_block  = work.getCallingBlock();
        wLocal.setFirstWeightFlag(YES);
        work.setCallingBlock(BULK_PICK);
        
        if (NO.equals(wLocal.getLabeledReserve()) && NO.equals(wLocal.getLabeledPicking())){
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CheckForChildren");                    
             oper.getParamsMap().put("pFacilityId", getBoundAttribute(ATTR_FACILITY_ID));
             oper.getParamsMap().put("pPickToContainerId", getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID)); 
             
            Object dummy =  oper.execute();
            if (dummy != null){
                if (YES.equals(wLocal.getFirstScanFlag())){
                    labelChild.setMasterToContainer(getBoundAttribute(ATTR_GENERIC_TO_CONTAINER));
                    labelChild.setContainerQty((BigDecimal)ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY));
                    getBulkPkAcrossWvPageFlowBean().setIsFocusOn("LABEL_CHILD.GENERIC_TO_CHILD");
                    ADFUtils.invokeAction("labelChild");
                    return;
                }
                else {
                     OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("CountSysChild");                                  
                     oper2.getParamsMap().put("pFacilityId", getBoundAttribute(ATTR_FACILITY_ID));
                     oper2.getParamsMap().put("pPickToContainerId", getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID));
                     BigDecimal t_child = (BigDecimal) oper2.execute();
                    labelChild.setContainerQty(t_child);
                    if (t_child.intValue() > 0 ){
                        getBulkPkAcrossWvPageFlowBean().setIsFocusOn("LABEL_CHILD.GENERIC_TO_CHILD");
                        ADFUtils.invokeAction("labelChild");
                        return;
                    }
                    else  
                      checkCatchWeight(t_old_calling_block); 
                }
            }
            else  
            checkCatchWeight(t_old_calling_block); 
        }
        else  
        checkCatchWeight(t_old_calling_block); 
    }
    private void validateQty4() {
        String t_old_calling_block = null;
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
        HhBulkPkAcrossWvSWorkViewRowImpl work = HhBulkPkAcrossWvSBean.getWorkViewCurrentRow();
        HhBulkPkAcrossWvSLabelChildViewRowImpl labelChild = HhBulkPkAcrossWvSBean.getLabelChildViewCurrentRow();
        t_old_calling_block  = work.getCallingBlock();
        wLocal.setFirstWeightFlag(YES); 
        work.setCallingBlock(BULK_PICK);
        
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(OPR_CHECK_FOR_CHILDREN);                    
         oper.getParamsMap().put("pFacilityId", getBoundAttribute(ATTR_FACILITY_ID));
         oper.getParamsMap().put("pPickToContainerId", getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID));
            
             Object dummy =  oper.execute();
             if (dummy != null){
                 if (YES.equals(wLocal.getFirstScanFlag())){
                     labelChild.setMasterToContainer(getBoundAttribute(ATTR_GENERIC_TO_CONTAINER));
                     labelChild.setContainerQty((BigDecimal)ADFUtils.getBoundAttributeValue(ATTR_CONTAINER_QTY));
                     getBulkPkAcrossWvPageFlowBean().setIsFocusOn("LABEL_CHILD.GENERIC_TO_CHILD");
                     ADFUtils.invokeAction("labelChild");
                     return;
                 }
                 else {
                      OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation(OPR_COUNT_SYS_CHILD);                                  
                      oper2.getParamsMap().put("pFacilityId", getBoundAttribute(ATTR_FACILITY_ID));
                      oper2.getParamsMap().put("pPickToContainerId", getBoundAttribute(ATTR_PICK_TO_CONTAINER_ID));
                      BigDecimal t_child = (BigDecimal) oper2.execute();
                     labelChild.setContainerQty(t_child);
                     
                     if (t_child.intValue() > 0 ){
                         getBulkPkAcrossWvPageFlowBean().setIsFocusOn("LABEL_CHILD.GENERIC_TO_CHILD");
                         ADFUtils.invokeAction("labelChild");
                         return;
                     }
                     else  
                           checkCatchWeight(t_old_calling_block); 
                 }
             }
             else ;
              checkCatchWeight(t_old_calling_block); 
    }
    
    private void checkCatchWeight(String olCallingBlock){ 
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(OPR_CHECK_CATCH_WEIGHT);                                  
        oper.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion());
        oper.getParamsMap().put("pmAllowIntrlvg", getBulkPkAcrossWvPageFlowBean().getPmAllowIntrlvg());
        oper.getParamsMap().put("callingBlockIn", olCallingBlock);
         Map returnValues = (Map)oper.execute();
      
        Object gotoField    = returnValues.get("GoToField");
        Object callingBlock = returnValues.get("CallingBlock"); 
        performNavigation (callingBlock,gotoField);  
    }   
 
    private void userDoneProcToLocation(){
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(OPR_USER_DONE_TO_LOCATION);
         if (oper != null){
             oper.getParamsMap().put("pVersion", getBulkPkAcrossWvPageFlowBean().getPVersion());
             oper.getParamsMap().put("pmAllowIntrlvg", getBulkPkAcrossWvPageFlowBean().getPmAllowIntrlvg());

             Map returnValues = (Map)oper.execute();
             List<String> errors = ( List<String>) returnValues.get("Errors");
             if (processMessages(getErrorWarnInfoMessage(),getErrorWarnInfoIcon(),getAllMessagesPanel(),errors)){ //return if errors   
                 setErrorStyleOnComponent(getContainerQty(),getContainerQtyIcon(),true );
                 return;
             }
             Object gotoField    = returnValues.get("GoToField"); 
             
             if (gotoField != null){
                 getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String)gotoField);                    
             }
             ADFUtils.invokeAction("location"); 
         } 
    }
    private String checkEmpty(String fromLocation, String fromContainer, String emptyFlag) {

        _logger.info("checkEmpty Start");
        long t_container_qty = 0;
        String facilityId = getBoundAttribute(ATTR_FACILITY_ID);
        OperationBinding op = (OperationBinding)ADFUtils.findOperation("getContainerQty");
        if (op != null){
            op.getParamsMap().put("facilityId", facilityId);
            op.getParamsMap().put("locationId", fromLocation);
            op.getParamsMap().put("containerId", fromContainer);
            _logger.info("locScpByWarehouse End");
             t_container_qty = (Long)op.execute();
        }
        
        if ( "M".equals(emptyFlag)){
            OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_MARK_LOCATION);
            opr.execute(); 
            
            OperationBinding opr2 = (OperationBinding)ADFUtils.findOperation(OPR_RUN_PURGE_DIRECTIVE);
            opr2.execute(); 
        }
        else if ( YES.equals(emptyFlag) && t_container_qty > 0  ){
            //if confirm_empty
            Object msg = getMessage(CONFIRM_EMPTY, CONF); 
            getContainerQty().setDisabled(true);
            refreshContentOfUIComponent(getContainerQty());
            getConfEmptyMessage().setValue(msg); 
            this.getConfEmptyPopup().show(new RichPopup.PopupHints());
            return NO;
        }
        else if ( NO.equals(emptyFlag) && t_container_qty == 0 ) {
            //confirm not empty
            Object msg = getMessage(CONFIRM_NOT_EMP, CONF); 
            getContainerQty().setDisabled(true);
            refreshContentOfUIComponent(getContainerQty());
            getConfEmptyMessage().setValue(msg); 
            this.getConfEmptyPopup().show(new RichPopup.PopupHints()); 
            return NO;
        }
       
        _logger.info("checkEmpty End");
        return YES;
    }
    
    private void continueCheckEmpty(){
        
        Object cbFrom = ADFContext.getCurrent().getViewScope().get("CBFrom");
        if  (STR_1.equals(cbFrom)){
         //   Scan_All_Containers;
            ADFUtils.invokeAction("container");
        }
        else if (STR_2.equals(cbFrom)){
            userDoneProcToLocation();
            return;
        }
        else if (STR_3.equals(cbFrom)){
            validateQty4();
        }
        else if (STR_4.equals(cbFrom)){
            
            validateQty5();
        }
    }
    public void yesConfEmptyActionListener(ActionEvent actionEvent) { //same for CONFIRM_EMPTY and CONFIRM_NOT_EMP
        getConfEmptyPopup().hide();
        OperationBinding opr = (OperationBinding)ADFUtils.findOperation(OPR_MARK_LOCATION);
        opr.execute();       
        
        continueCheckEmpty();
    }
    public void noConfEmptyActionListener(ActionEvent actionEvent) { //same for CONFIRM_EMPTY and CONFIRM_NOT_EMP
        getConfEmptyPopup().hide();
        continueCheckEmpty();
    }    
    
    
    private void performNavigation(Object callingBlock, Object gotoField){
        _logger.info("performNavigation Start");
        if (gotoField != null){
            getBulkPkAcrossWvPageFlowBean().setIsFocusOn((String)gotoField);
        }
        ADFUtils.setBoundAttributeValue(ATTR_CALLING_BLOCK, BULK_PICK); //Set calling block prior to navigation.
        
        if (callingBlock != null && CONTAINER.equals(callingBlock))
           ADFUtils.invokeAction("container");
        else if (callingBlock != null && TO_LOCATION.equals(callingBlock))
           ADFUtils.invokeAction("location");
        else if (gotoField != null && gotoField.toString().startsWith(WEIGHT))
           ADFUtils.invokeAction("weight");
        else if (gotoField != null && gotoField.toString().startsWith(LABEL_CHILD))
           ADFUtils.invokeAction("labelChild");
        //Else stay on bulk pick screen, there must be some exception.
        
        _logger.info("performNavigation End");
    }
    

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {  
        _logger.info("onRegionLoad Start");
        Object bypass = getBulkPkAcrossWvPageFlowBean().getBypassPick();
        if (YES.equals(bypass)) {
            getBulkPkAcrossWvPageFlowBean().setBypassPick(NO);
            F7BypassListener(null);
            return;
        }
        Object err = getBulkPkAcrossWvPageFlowBean().getReturnErrorMessage();
        if (err != null) {
            getPopupMessage().setValue(err); 
            this.getGotoPopup().show(new RichPopup.PopupHints()); 
         
            return;
        }
 
        onLoadListener(null);
        _logger.info("onRegionLoad End");
    }
    
    public void okDialogListener(ActionEvent actionEvent) {  
        _logger.info("okDialogListener Start"); 
        getGotoPopup().hide(); 
        getBulkPkAcrossWvPageFlowBean().setReturnErrorMessage(null);
        String intrlvg = getBulkPkAcrossWvPageFlowBean().getLGlobalIntrlvg();
        if ("INTRLVG".equals(intrlvg)){
            ADFContext.getCurrent().getPageFlowScope().put("ExitForm", "PA");
            //OTM-1610, set calling form to NOT-null value, so putaway does not come back to BP
            //Note: this flag is reset on InitTaskflow method to avoid cashing the flag value.
            GlobalVariablesViewRowImpl grow = HhBulkPkAcrossWvSBean.getGlobalVariablesViewCurrentRow();
            grow.setBP2PutawayNoPickFlag("Y");
            
            ADFUtils.invokeAction("onLoadNavigation");
        }           
        else {
            int depth = getPageFlowStackDepth();
            if (depth >= 4)
              popTaskFlow(depth-2);
            else
            ADFUtils.invokeAction("backGlobalHome"); 
        }
           
        _logger.info("okDialogListener End");
    } 
    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setGenericToContainer(RichInputText genericToContainer) {
        this.genericToContainer = genericToContainer;
    }

    public RichInputText getGenericToContainer() {
        return genericToContainer;
    }

    public void setPickToContainer(RichInputText pickToContainer) {
        this.pickToContainer = pickToContainer;
    }

    public RichInputText getPickToContainer() {
        return pickToContainer;
    }

    public void setConfLocationId(RichInputText confLocationId) {
        this.confLocationId = confLocationId;
    }

    public RichInputText getConfLocationId() {
        return confLocationId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setMyviewPFL(RichPanelFormLayout myviewPFL) {
        this.myviewPFL = myviewPFL;
    }

    public RichPanelFormLayout getMyviewPFL() {
        return myviewPFL;
    }

    public void setMainPanelForm(RichPanelFormLayout mainPanelForm) {
        this.mainPanelForm = mainPanelForm;
    }

    public RichPanelFormLayout getMainPanelForm() {
        return mainPanelForm;
    }


    public void setContainerQtyIcon(RichIcon containerQtyIcon) {
        this.containerQtyIcon = containerQtyIcon;
    }

    public RichIcon getContainerQtyIcon() {
        return containerQtyIcon;
    }

    public void setConfContainerIdIcon(RichIcon confContainerIdIcon) {
        this.confContainerIdIcon = confContainerIdIcon;
    }

    public RichIcon getConfContainerIdIcon() {
        return confContainerIdIcon;
    }

    public void setConfLocationIdIcon(RichIcon confLocationIdIcon) {
        this.confLocationIdIcon = confLocationIdIcon;
    }

    public RichIcon getConfLocationIdIcon() {
        return confLocationIdIcon;
    }


    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

    public void setF4Done(RichLink f4Done) {
        this.f4Done = f4Done;
    }

    public RichLink getF4Done() {
        return f4Done;
    }

    public void setF6Lpn(RichLink f6Lpn) {
        this.f6Lpn = f6Lpn;
    }

    public RichLink getF6Lpn() {
        return f6Lpn;
    }

    public void setF7Bypass(RichLink f7Bypass) {
        this.f7Bypass = f7Bypass;
    }

    public RichLink getF7Bypass() {
        return f7Bypass;
    }
    public void setValidationError(boolean validationError) {
        //this.validationError = validationError;
     ADFContext.getCurrent().getViewScope().put("ValidationError", validationError);

    }

    public boolean isValidationError() {
        Object valErr = ADFContext.getCurrent().getViewScope().get("ValidationError");
        if (valErr == null) return false;
        
        return (Boolean)valErr;
    }

    public void setConfContainerId(RichInputText confContainerId) {
        this.confContainerId = confContainerId;
    }

    public RichInputText getConfContainerId() {
        return confContainerId;
    }

    public void setContainerQty(RichInputText containerQty) {
        this.containerQty = containerQty;
    }

    public RichInputText getContainerQty() {
        return containerQty;
    }
    public void setConfContainerDisabled(boolean confContainerDisabled) {
        this.confContainerDisabled = confContainerDisabled;
    }

    public boolean isConfContainerDisabled() {
        return confContainerDisabled;
    } 

    public void setPopupMessage(RichOutputText popupMessage) {
        this.popupMessage = popupMessage;
    }

    public RichOutputText getPopupMessage() {
        return popupMessage;
    }

    public void setGotoPopup(RichPopup gotoPopup) {
        this.gotoPopup = gotoPopup;
    }

    public RichPopup getGotoPopup() {
        return gotoPopup;
    }

    public void setDgConfDiffPick(RichDialog dgConfDiffPick) {
        this.dgConfDiffPick = dgConfDiffPick;
    }

    public RichDialog getDgConfDiffPick() {
        return dgConfDiffPick;
    }

 

    public void setConfDiffPickText(RichOutputText confDiffPickText) {
        this.confDiffPickText = confDiffPickText;
    }

    public RichOutputText getConfDiffPickText() {
        return confDiffPickText;
    }

    public void yesConfDiffPickActionListener(ActionEvent actionEvent) {
        getPpConfDiffPick().hide();
        Object cbFrom = ADFContext.getCurrent().getViewScope().get("CBFrom");
        if  (STR_0.equals(cbFrom)){
            validateQty2();
            return;
        }
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocal = HhBulkPkAcrossWvSBean.getWorkLocalViewCurrentRow();
        if (PICK_CP.equals(wLocal.getPickType()))
           wLocal.setEventCode(new BigDecimal(24));
    }

    public void performConfDiffPickKey(ClientEvent clientEvent) {
        // Add event code here...
    }

    public void noConfDiffPickListener(ActionEvent actionEvent) {
         getPpConfDiffPick().hide();
    }

    public void setPpConfDiffPick(RichPopup ppConfDiffPick) {
        this.ppConfDiffPick = ppConfDiffPick;
    }

    public RichPopup getPpConfDiffPick() {
        return ppConfDiffPick;
    }

    public void setConfEmptyPopup(RichPopup confEmptyPopup) {
        this.confEmptyPopup = confEmptyPopup;
    }

    public RichPopup getConfEmptyPopup() {
        return confEmptyPopup;
    }

    public void setConfEmptyMessage(RichOutputText confEmptyMessage) {
        this.confEmptyMessage = confEmptyMessage;
    }

    public RichOutputText getConfEmptyMessage() {
        return confEmptyMessage;
    }

    public void performConfEmptyKey(ClientEvent clientEvent) {
        // Add event code here...
    }
}
