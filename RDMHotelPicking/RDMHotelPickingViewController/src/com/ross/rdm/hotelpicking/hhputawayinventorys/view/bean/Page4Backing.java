package com.ross.rdm.hotelpicking.hhputawayinventorys.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainIntrlvgViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLocalViewRowImpl;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page4.jspx
// ---
// ---------------------------------------------------------------------
public class Page4Backing extends RDMHotelPickingBackingBean {
    private final static String CALL_PREPARE_EXIT_PUTAWAY_OPER = "callPrepareExitPutaway";
    private final static String CALL_POPULATE_PUTAWAY_OPER = "callPopulatePutaway";
    private final static String CALL_TR_KEY_IN_CURSORS_OPER = "callTrKeyInCursors";
    private final static String CALL_V_SUGGESTED_MPL_OPER = "callVSuggestedMpl";
    private final static String GLOBAL_VARIABLES_VO_ITERATOR = "GlobalVariablesViewIterator";
    private final static String MAININTRLVG_VO_ITERATOR = "HhPutawayInventorySMainIntrlvgViewIterator";
    private final static String MAIN_VO_ITERATOR = "HhPutawayInventorySMainViewIterator";

    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String HH_PUTAWAY_PAGE_FLOW_BEAN = "HhPutawayInventorySBean";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelGroupLayout allMessagesPanel;
    private RichInputText confirmContainerId;
    private RichIcon confirmCidIcone;
    private RichPopup confirmGoToPopup;
    private RichOutputText confirmGoToOutputtext;
    private RichDialog confirmGoToDialog;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page4Backing.class);
    private RichInputText storage;

    public Page4Backing() {

    }

    public String trKeyIn(String key) {
        _logger.info("trKeyuIn() Start");
        _logger.fine("key value: " + key);
        String goBlock = "";
        HhPutawayInventorySMainIntrlvgViewRowImpl hhPutawayInventorySMainIntrlvgViewRow =
            this.getHhPutawayInventorySMainIntrlvgCurrentRow();
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        if ("F3".equalsIgnoreCase(key)) {
            this.callPrepareExitPutaway();
            this.setGlobalVariables("EXIT_INTRLVG");
            //ADFUtils.invokeAction("backGlobalHome");
            _logger.info("trKeyuIn() End");
            return goBlock = "bulkPickReturn";
        } else {
            if ("F4".equals(key)) {
                boolean continu = true;
                if (null == hhPutawayInventorySMainIntrlvgViewRow.getConfrmContainerId()) {
                    this.hideMessagesPanel();
                    setErrorStyleClass(this.getConfirmContainerId(), true);
                    this.setFocusOnUIComponent(this.getConfirmContainerId());
                    this.showMessagesPanel(ERROR, this.getMessage(PARTIAL_ENTRY, ERROR, facilityId, langCode));
                    //this.refreshContentOfUIComponent(this.getConfirmContainerId());
                    _logger.info("trKeyuIn() End");
                    return null;
                } else {
                    if (!hhPutawayInventorySMainIntrlvgViewRow.getConfrmContainerId().equalsIgnoreCase(hhPutawayInventorySMainIntrlvgViewRow.getContainerId())) {
                        String contValid = this.callvSuggestedMlp();
                        if (YES.equalsIgnoreCase(contValid)) {
                            hhPutawayInventorySMainIntrlvgViewRow.setContainerId(hhPutawayInventorySMainIntrlvgViewRow.getConfrmContainerId());
                            hhPutawayInventorySMainIntrlvgViewRow.setLocationId(this.callTrKeyInCursors(facilityId,
                                                                                                        this.getHhPutawayInventorySMainIntrlvgCurrentRow().getConfrmContainerId()));
                            goBlock = "Main";
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.CONTAINER_ID");

                        } else {
                            setErrorStyleClass(this.getConfirmContainerId(), true);
                            this.clearField(this.getConfirmContainerId());
                            this.setFocusOnUIComponent(this.getConfirmContainerId());
                            continu = false;
                            if ("I".equalsIgnoreCase(contValid)) {
                                this.hideMessagesPanel();
                                this.showMessagesPanel(ERROR,
                                                       this.getMessage("INV_CONTAINER", ERROR, facilityId, langCode));
                            } else if ("S".equalsIgnoreCase(contValid)) {
                                this.hideMessagesPanel();
                                this.showMessagesPanel(WARN,
                                                       this.getMessage("INV_CONT_STATUS", WARN, facilityId, langCode));
                            } else if ("D".equalsIgnoreCase(contValid)) {
                                this.hideMessagesPanel();
                                this.showMessagesPanel(WARN, this.getMessage("PUT_INV_DEST", WARN, facilityId, langCode));
                            } else if ("M".equalsIgnoreCase(contValid)) {
                                this.hideMessagesPanel();

                                this.showMessagesPanel(ERROR,
                                                       this.getMessage("CONT_ON_MASTER", ERROR, facilityId, langCode));
                            } else if ("X".equalsIgnoreCase(contValid)) {
                                hhPutawayInventorySMainIntrlvgViewRow.setConfrmContainerId(null);
                                this.showMessagesPanel(ERROR, this.getMessage("SPLIT_MLP", WARN, facilityId, langCode));

                            } else if ("Z".equalsIgnoreCase(contValid)) {
                                hhPutawayInventorySMainIntrlvgViewRow.setConfrmContainerId(null);
                                this.showMessagesPanel(ERROR, this.getMessage("INV_STOR_CAT", WARN, facilityId, langCode));
                            }


                        }
                    }
                }
                if (continu) {
                    HhPutawayInventorySMainViewRowImpl mainCurrentRow = this.getHhPutawayInventorySMainCurrentRow();
                    mainCurrentRow.setContainerId(hhPutawayInventorySMainIntrlvgViewRow.getContainerId());
                    goBlock = "Main";
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.TOLOCATION_ID");

                }
            } else {
                if ("F7".equals(key)) {
                    hhPutawayInventorySMainIntrlvgViewRow.setConfrmContainerId(null);
                    hhPutawayInventorySMainIntrlvgViewRow.setConfrmContainerId(null);
                    this.setFocusOnUIComponent(this.getConfirmContainerId());
                    this.callPopulatePutaway(hhPutawayInventorySMainIntrlvgViewRow.getLocationId(),
                                             hhPutawayInventorySMainIntrlvgViewRow.getContainerId(), "SKIP");
                }
            }
        }
        //        if (!goBlock.isEmpty())
        //            ADFUtils.invokeAction(goBlock);
        _logger.fine("goBlock value: " + goBlock);
        _logger.info("trKeyuIn() End");
        return goBlock;
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);
        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("ccmi")) {
                this.getConfirmCidIcone().setName("error");
                this.refreshContentOfUIComponent(this.getConfirmCidIcone());
            }

        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("ccmi")) {
                this.getConfirmCidIcone().setName("required");
                this.refreshContentOfUIComponent(this.getConfirmCidIcone());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }


    public void f7SkipActionListener(ActionEvent actionEvent) {
        // Add event code here...
      
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MAIN_INTRLVG_F7,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            this.trKeyIn("F7");
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
          
        }
    }


    private void callPopulatePutaway(String locationId, String containerId, String modeIn) {
        _logger.info("callPopulatePutaway() Start");
        _logger.fine("locationId value: " + locationId + "\n" + "containerId value: " + containerId + "\n" +
                     "modeIn value: " + modeIn);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_POPULATE_PUTAWAY_OPER);
        op.getParamsMap().put("locationId", locationId);
        op.getParamsMap().put("containerId", containerId);
        op.getParamsMap().put("facilityId", this.getFacilityIdAttrValue());
        op.getParamsMap().put("pmAllowaIntrlvg", this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg());
        op.getParamsMap().put("pmStartLocation", this.getHhPutawayInventorySBeanPageFlowBean().getPmStartLocation());
        op.getParamsMap().put("pmodein", modeIn);
        List<String> liseCodes = (List<String>) op.execute();
        if (op.getErrors() != null && op.getErrors().isEmpty()) {
            if (liseCodes != null) {
                String msgCode = liseCodes.get(0);
                String msgMode = liseCodes.get(1);
                if ("MSG".equalsIgnoreCase(liseCodes.get(2))) {
                    String error = this.getErrorMessage(msgCode, msgMode);
                    if (error != null && !error.isEmpty()) {
                        this.getConfirmGoToOutputtext().setValue(error);
                        this.getConfirmGoToPopup().show(new RichPopup.PopupHints());
                    }
                }
            }
        }
        _logger.info("callPopulatePutaway() End");
    }

    private String getErrorMessage(String taskIdIn, String modeIn) {
        _logger.info("getErrorMessage() Start");
        _logger.fine("taskIdIn value: " + taskIdIn + "\n" + "modeIn value: " + modeIn);
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        String error = null;
        if (YES.equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getPmNoTaskFlag())) {

            if (YES.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getProcessByAisle())) {
                this.hideMessagesPanel();
                error = this.getMessage("NO_MORE_TASK", WARN, facilityId, langCode);
            } else {
                this.hideMessagesPanel();
                error = this.getMessage("NO_TASK_ZONE", WARN, facilityId, langCode);
            }
            this.callPrepareExitPutaway();
            this.setGlobalVariables("EXIT_INTRLVG");
            this.getConfirmContainerId().setDisabled(true);
            this.refreshContentOfUIComponent(this.getConfirmContainerId());
            // ADFUtils.invokeAction("Home");
            //NS: 3/4/206: Should not process any further, else it will override global variables
            //to INTRLVG before returning. For has EXIT_FORM(NO_VALIDATE);
            return error;
        } else {
            if (NO.equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getPmNoPutFlag()))
                if (!"SKIP".equalsIgnoreCase(modeIn))
                    this.getHhPutawayInventorySBeanPageFlowBean().setPmNoPutFlag(YES);
            if ("LOCATION".equalsIgnoreCase(taskIdIn)) {
                this.hideMessagesPanel();
                error = this.getMessage("NO_EMPTY_LOC", WARN, facilityId, langCode);
            } else {
                this.hideMessagesPanel();
                error = this.getMessage("NO_PUTAWAY_TASK", WARN, facilityId, langCode);
            }
        }
        if ("SKIP".equalsIgnoreCase(modeIn))
            this.getHhPutawayInventorySBeanPageFlowBean().setPmNoTaskFlag(YES);
        this.callPrepareExitPutaway();
        this.setGlobalVariables("INTRLVG");
        _logger.info("getErrorMessage() End");
        return error;
    }

    public String okGoLinkAction() {
        this.getConfirmGoToPopup().hide();
        this.popTaskFlow(this.getPageFlowStackDepth() - 2);
        return null;
    }


    private void setGlobalVariables(String modeIn) {
        _logger.info("setGlobalVariables() Start");
        _logger.fine("modeIn value: " + modeIn);
        GlobalVariablesViewRowImpl globalVariablesRow = this.getGlobalVariablesCurrentRow();
        if ("EXIT_INTRLVG".equalsIgnoreCase(modeIn)) {
            globalVariablesRow.setGlobalAllowIntrlvg(NO);
            globalVariablesRow.setGlobalNoPutFlag(NO);
            globalVariablesRow.setGlobalNoTaskFlag(NO);
            globalVariablesRow.setGlobalNoPickFlag(NO);
            globalVariablesRow.setGlobalCallingForm(null);
            globalVariablesRow.setGlobalProcessByAisle(NO);
        } else {
            globalVariablesRow.setGlobalCallingForm("hh_putaway_inventory_s");
            HhPutawayInventorySBean invBean = this.getHhPutawayInventorySBeanPageFlowBean();
            if (invBean != null) {
                globalVariablesRow.setGlobalNoPutFlag(invBean.getPmNoPutFlag());
                globalVariablesRow.setGlobalAllowIntrlvg(invBean.getPmAllowIntrlvg());
                globalVariablesRow.setGlobalNoTaskFlag(invBean.getPmNoTaskFlag());
            }
        }
        _logger.info("setGlobalVariables() End");
    }

    private String callvSuggestedMlp() {
        _logger.info("callvSuggestedMlp() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_V_SUGGESTED_MPL_OPER);
        op.execute();
        if (op.getErrors().isEmpty()) {
            _logger.info("callvSuggestedMlp() End");
            return ((String) op.getResult());
        }
        _logger.info("callvSuggestedMlp() End");
        return null;
    }

    private String callTrKeyInCursors(String facilityId, String containerId) {
        _logger.info("callTrKeyInCursors() Start");
        _logger.fine("facilityId value: " + facilityId + "\n" + "containerId value: " + containerId);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_TR_KEY_IN_CURSORS_OPER);
        op.getParamsMap().put("facilityId", facilityId);
        op.getParamsMap().put("containerId", containerId);
        op.execute();
        if (op.getErrors().isEmpty()) {
            _logger.info("callTrKeyInCursors() End");
            return ((String) op.getResult());
        }
        _logger.info("callTrKeyInCursors() End");
        return null;
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel() End");
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("hideMessagesPanel() End");
    }

    private HhPutawayInventorySBean getHhPutawayInventorySBeanPageFlowBean() {
        return ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN));

    }

    private GlobalVariablesViewRowImpl getGlobalVariablesCurrentRow() {
        return (GlobalVariablesViewRowImpl) ADFUtils.findIterator(GLOBAL_VARIABLES_VO_ITERATOR).getCurrentRow();
    }

    private HhPutawayInventorySMainViewRowImpl getHhPutawayInventorySMainCurrentRow() {
        return (HhPutawayInventorySMainViewRowImpl) ADFUtils.findIterator(MAIN_VO_ITERATOR).getCurrentRow();

    }

    private HhPutawayInventorySMainIntrlvgViewRowImpl getHhPutawayInventorySMainIntrlvgCurrentRow() {
        return (HhPutawayInventorySMainIntrlvgViewRowImpl) ADFUtils.findIterator(MAININTRLVG_VO_ITERATOR).getCurrentRow();

    }

    private HhPutawayInventorySWorkLocalViewRowImpl getHhPutawayInventorySWorkLocalViewCurrentRow() {
        return (HhPutawayInventorySWorkLocalViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLocalViewIterator").getCurrentRow();

    }

    private void callPrepareExitPutaway() {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_PREPARE_EXIT_PUTAWAY_OPER);
        op.execute();
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);

    }

    private String getContainerIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);

    }

    private String getLangCodeAttrValue() {

        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }


    public void confirmContainerValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        if (valueChangeEvent.getNewValue() != null) {
            this.updateModel(valueChangeEvent);
        }
    }

    public void setConfirmContainerId(RichInputText confirmContainerId) {
        this.confirmContainerId = confirmContainerId;
    }

    public RichInputText getConfirmContainerId() {
        return confirmContainerId;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setConfirmCidIcone(RichIcon confirmCidIcone) {
        this.confirmCidIcone = confirmCidIcone;
    }

    public RichIcon getConfirmCidIcone() {
        return confirmCidIcone;
    }

    public String exitAction() {
        _logger.info("exitAction() Start");
        // Add event code here...
        this.trKeyIn("F3");
        this.popTaskFlow(this.getPageFlowStackDepth() - 2);
        _logger.info("exitAction() End");
        return null;
    }

    public String doneAction() {
        // Add event code here...
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MAIN_INTRLVG_F4,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            return this.trKeyIn("F4");
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
            return "";
        }
       
    }

    public void setStorage(RichInputText storage) {
        this.storage = storage;
    }

    public RichInputText getStorage() {
        return storage;
    }

    public void setConfirmGoToDialog(RichDialog confirmGoToDialog) {
        this.confirmGoToDialog = confirmGoToDialog;
    }

    public RichDialog getConfirmGoToDialog() {
        return confirmGoToDialog;
    }

    public void setConfirmGoToOutputtext(RichOutputText confirmGoToOutputtext) {
        this.confirmGoToOutputtext = confirmGoToOutputtext;
    }

    public RichOutputText getConfirmGoToOutputtext() {
        return confirmGoToOutputtext;
    }

    public void setConfirmGoToPopup(RichPopup confirmGoToPopup) {
        this.confirmGoToPopup = confirmGoToPopup;
    }

    public RichPopup getConfirmGoToPopup() {
        return confirmGoToPopup;
    }
}
