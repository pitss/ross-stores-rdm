package com.ross.rdm.hotelpicking.hhputawayinventorys.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainIntrlvgViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLaborProdNewViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLocalViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkViewRowImpl;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMHotelPickingBackingBean {
    @SuppressWarnings("compatibility:8631505097640221771")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichPopup confirmPopup;
    private RichDialog confirmDialog;
    private RichDialog confirmMarkDialog;

    private RichOutputText dialogOutputText;
    private RichInputText containerId;
    private RichOutputText warningOutputText;
    private RichOutputText errorOutputText;
    private RichOutputText infoOutputText;
    private RichPanelGridLayout page1PanelGridLayout;
    private RichInputText locationId;
    private RichInputText itemId;
    private RichInputText upsCode;
    private RichInputText itemDesc;
    private RichInputText caseQty;
    private RichInputText suggestedLocId;
    private RichInputText toLocationId;
    private RichInputText confirmCaseQty;
    private RichLink f5NextLocLink;
    private RichLink f8MarkLink;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichIcon containerIdIcone;
    private RichIcon toLocationIcone;
    private RichIcon confrimCaseIcone;
    private RichPopup confirmMarkPopup;
    private RichOutputText dialogConfirmMarkOutputtext;
    private RichPopup confirmGoToPopup;
    private RichOutputText confirmGoToOutputtext;
    private RichDialog confirmGoToDialog;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f6ClearLink;
    private RichPanelGroupLayout evenlPanel;
    private final static String WARNING_STATUS = "WARNING";
    private final static String ERROR_STATUS = "ERROR";
    private final static String NO_ERROR_STATUS = "NO_ERROR";
    private final static String INFO_STATUS = "INFO";
    private final static String NEW_FPL_BLOCK = "NEW_FPL";
    private final static String MULTI_SKU_BLOCK = "MULTI_SKU";
    private final static String CONFIRM_P_POPUP_LAUNCHER = "CONFIRM_P";
    private final static String CHECK_TRB_POPUP_LAUNCHER = "CHECK_TRB";
    private final static String USER_CHOICE_DEST_CONFIRM = "USER_CHOICE_DEST";
    private final static String USER_CHOICE_WIP_CONFIRM = "USER_CHOICE_WIP";
    private final static String CALL_GET_CONTAINER_INFO_OPER = "getContaierInfo";
    private final static String CALL_GET_CONTAINER_INFO2_OPER = "getContainerInfo2";
    private final static String CALL_GET_CONTAINER_INFO3_OPER = "getContainerInfo3";
    private final static String CALL_GET_CONTAINER_INFO4_OPER = "getContainerInfo4";
    private final static String CALL_V_CONT_TRANSIT_OPER = "callVContTransit";
    private final static String CALL_V_PUTAWAY_LOCATION_OPER = "callVPutawayLocation";
    private final static String CALL_CHECK_CONTAINER_TROUBLE_OPER = "callCheckContainerTrouble";
    private final static String CALL_PUTAWAY_MERCHANDISE_OPER = "callPutawayMerchandise";
    private final static String CALL_PROCESS_PUTAWAY_EXCEPTION_OPER = "callProcessPutawayExcpetion";
    private final static String CALL_PREPARE_EXIT_PUTAWAY_OPER = "callPrepareExitPutaway";
    private final static String CALL_MARK_LOC_OPER = "callMarkLoc";
    private final static String CALL_FIND_LOCATIONS_OPER = "callFindLocations";
    private final static String CALL_P_UPDATE_PENDING_QTYS_OPER = "callPupdatePendingQtys";
    private final static String CALL_CKRSNCDREQ_OPER = "callCkRsnCdReq";
    private final static String CALL_EXIT_TR_OPER = "callTrExit";
    private final static String CALL_V_PUT_QTY_OPER = "vPutQty";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String HH_PUTAWAY_PAGE_FLOW_BEAN = "HhPutawayInventorySBean";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String PAGE1_PAGEDEF_NAME =
        "com_ross_rdm_hotelpicking_hhputawayinventorys_view_pageDefs_Page1PageDef";
    private RichInputText storage;
    private RichLink goMusltiSku;


    public Page1Backing() {
        super();
    }

    public void onChangedContainerIdMain(ValueChangeEvent vce) {
        _logger.info("onChangedContainerIdMain() Start");
        _logger.fine("vce value: " + vce);
        long lStartTime = new Date().getTime(); // start time
        this.updateModel(vce);
        _logger.info("---------------------------------");
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            this.hideMessagesPanel();
            this.getContainerInfo(((String) vce.getNewValue()).toUpperCase());
        }
        long lEndTime = new Date().getTime(); // end time
        long difference = lEndTime - lStartTime; // check different
        _logger.info("Elapsed time for method call Get Container Details  --ADF +DB milliseconds: " + difference);
        _logger.info("onChangedContainerIdMain() End");
    }

    public void onChangedToLocationIdMain(ValueChangeEvent vce) {
        _logger.info("onChangedToLocationIdMain() Start");
        _logger.fine("vce value: " + vce);
        long lStartTime = new Date().getTime();
        _logger.info("---------------------------------");
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        Object f5key = ADFContext.getCurrent().getRequestScope().get("F5Key");
        Object noSuggLoc = ADFContext.getCurrent().getRequestScope().get("noSuggLoc");
        if (YES.equals(f5key)) {
            if (null != noSuggLoc && !noSuggLoc.toString().isEmpty())
                this.showMessagesPanel(ERROR, noSuggLoc.toString());
            return;
        }
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            this.hideMessagesPanel();
            this.getHhPutawayInventorySMainViewCurrentRow().setToLocationId(((String) vce.getNewValue()).toUpperCase());
            if (!this.isNavigationExecuted())
                if (YES.equalsIgnoreCase(this.callVPutawayLocation())) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("");
                    this.hideMessagesPanel();
                    this.getToLocationId().setDisabled(true);
                    this.setErrorStyleClass(this.getToLocationId(), false);
                    this.refreshContentOfUIComponent(this.getToLocationId());
                } else {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("TO");
                    this.getToLocationId().setDisabled(false);
                    this.refreshContentOfUIComponent(this.getToLocationId());
                    this.setErrorStyleClass(this.getToLocationId(), true);
                    this.selectTextOnUIComponent(this.getToLocationId());
                }
        }
        long lEndTime = new Date().getTime();
        long difference = lEndTime - lStartTime;
        _logger.info("Elapsed time for method call  Suggested location--ADF +DB milliseconds: " + difference);
        _logger.info("onChangedToLocationIdMain() End");
    }

    private void getContainerInfo(String containerId) {
        _logger.info("getContainerInfo() Start");
        _logger.fine("vce containerId: " + containerId);
        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, containerId);
        String facilityId = (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
        if ("NE".equalsIgnoreCase(this.callVContTransit(containerId)))
            if (!this.callCheckContainerTrouble(containerId))
                this.callGetContainerInfo(containerId, facilityId);
        _logger.info("getContainerInfo() End");
    }

    private void setFocusContainerId() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.CONTAINER_ID");
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusToLocationId() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.TOLOCATION_ID");
        this.setFocusOnUIComponent(this.getToLocationId());
    }

    private void setFocusConfirmCaseQty() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.CONFIRM_CASE_QTY");
        this.setFocusOnUIComponent(this.getConfirmCaseQty());
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);
        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("mcid")) {
                this.getContainerIdIcone().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcone());
            } else if (field.getId().equalsIgnoreCase("mlid")) {
                this.getToLocationIcone().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getToLocationIcone());
            } else {
                this.getConfrimCaseIcone().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getConfrimCaseIcone());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("mcid")) {
                this.getContainerIdIcone().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcone());
            } else if (field.getId().equalsIgnoreCase("mlid")) {
                this.getToLocationIcone().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getToLocationIcone());
            } else {
                this.getConfrimCaseIcone().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getConfrimCaseIcone());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    private boolean validateContainerId() {
        _logger.info("validateContainerId() Start");
        if (!this.getContainerId().isDisabled()) {
            if (this.getHhPutawayInventorySBeanPageFlowBean().getIsError() && this.getContainerIdAttrValue() != null) {
                this.setErrorStyleClass(this.getContainerId(), true);
                this.selectTextOnUIComponent(this.getContainerId());
                _logger.info("validateContainerId() End");
                return false;
            }
            if (this.getContainerIdAttrValue() == null) {
                this.setErrorStyleClass(this.getContainerId(), true);
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.setErrorStyleClass(this.getContainerId(), false);
            }
        }
        _logger.info("validateContainerId() End");
        return true;
    }

    private boolean validateToLocation() {
        _logger.info("validateToLocation() Start");
        if (!this.getToLocationId().isDisabled()) {
            if (this.getHhPutawayInventorySBeanPageFlowBean().getIsError() &&
                ADFUtils.getBoundAttributeValue("ToLocationId") != null) {
                this.onChangedToLocationIdMain(new ValueChangeEvent(this.getToLocationId(), null,
                                                                    ADFUtils.getBoundAttributeValue("ToLocationId")));
                _logger.info("validateToLocation() End");
                return false;
            }
            if (ADFUtils.getBoundAttributeValue("ToLocationId") == null) {
                this.setErrorStyleClass(this.getToLocationId(), true);
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                _logger.info("validateToLocation() End");
                return false;
            } else {
                this.setErrorStyleClass(this.getToLocationId(), false);
            }
        }
        _logger.info("validateToLocation() End");
        return true;
    }

    private boolean validateConfirmCaseQty() {
        _logger.info("validateConfirmCaseQty() Start");
        BigDecimal confirmCase = (BigDecimal) ADFUtils.getBoundAttributeValue("ConfirmCaseQty");
        if (!this.getConfirmCaseQty().isDisabled()) {
            if (this.getHhPutawayInventorySBeanPageFlowBean().getIsError() && confirmCase != null) {
                this.setErrorStyleClass(this.getConfirmCaseQty(), true);
                _logger.info("validateConfirmCaseQty() End");
                return false;
            }
            if (confirmCase == null) {
                this.setErrorStyleClass(this.getConfirmCaseQty(), true);
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                _logger.info("validateConfirmCaseQty() End");
                return false;
            } else {
                this.setErrorStyleClass(this.getConfirmCaseQty(), false);
            }
        }
        _logger.info("validateConfirmCaseQty() End");
        return true;
    }

    private void removeErrorStyleClass() {
        this.setErrorStyleClass(this.getToLocationId(), false);
        this.setErrorStyleClass(this.getContainerId(), false);
        this.setErrorStyleClass(this.getConfirmCaseQty(), false);
    }

    public void f5NextLocActionListener(ActionEvent actionEvent) {
       
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MAIN_F5,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            if (this.validateContainerId()) {
                this.trKeyIn("F5");
            }
        }else{
        this.showMessagesPanel("E",this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
        }
    }

    public void f8MarkActionListener(ActionEvent actionEvent) {
        _logger.info("f8MarkActionListener() Start");
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MAIN_F8,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            if (this.validateContainerId() && this.validateToLocation()) {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                this.getConfirmCaseQty().setDisabled(true);
                this.getToLocationId().setDisabled(true);
                this.refreshContentOfUIComponent(this.getConfirmCaseQty());
                this.refreshContentOfUIComponent(this.getLocationId());
                this.getDialogConfirmMarkOutputtext().setValue(this.getMessage("CONFIRM_OPER", CONF,
                                                                               this.getFacilityIdAttrValue(),
                                                                               this.getLangCodeAttrValue()));
                this.getConfirmMarkPopup().show(new RichPopup.PopupHints());
            }
        }else{
        this.showMessagesPanel("E",this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(), this.getLangCodeAttrValue()));
        }
        
        
        
        _logger.info("f8MarkActionListener() End");
    }


    public void f3ExitActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    private boolean isPopupVisible() {
        return this.getHhPutawayInventorySBeanPageFlowBean().getIsPopupShowing();
    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        _logger.info("f4DoneActionListener() Start");

       
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MAIN_F4,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            if (this.isPopupVisible())
                return;
            if (this.validateContainerId() && this.validateToLocation() && this.validateConfirmCaseQty())
                this.trKeyIn("F4");
        }else{
            this.showMessagesPanel("E",
                                    this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()));
        }
        
        
       
        _logger.info("f4DoneActionListener() End");
    }

    private void processInvLocId() {
        this.getToLocationId().setDisabled(false);
        this.getConfirmCaseQty().setDisabled(true);
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.TOLOCATION_ID");
        this.setErrorStyleClass(this.getToLocationId(), true);
        this.setErrorStyleClass(this.getConfirmCaseQty(), false);
        this.refreshContentOfUIComponent(this.getToLocationId());
        this.refreshContentOfUIComponent(this.getConfirmCaseQty());
        this.selectTextOnUIComponent(this.getToLocationId());
    }

    private void processInvQty() {
        this.getToLocationId().setDisabled(true);
        this.getConfirmCaseQty().setDisabled(false);
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.CONFIRM_CASE_QTY");
        this.setErrorStyleClass(this.getToLocationId(), false);
        this.setErrorStyleClass(this.getConfirmCaseQty(), true);
        this.refreshContentOfUIComponent(this.getToLocationId());
        this.refreshContentOfUIComponent(this.getConfirmCaseQty());
        this.selectTextOnUIComponent(this.getConfirmCaseQty());
    }

    private String prcoessF4key(HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow,
                                HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow) {
        String goBlock = "";
        String correctLoc = this.callVPutawayLocation();
        String correctQty = null;
        if (YES.equalsIgnoreCase(correctLoc))
            correctQty = this.callVPutQty(hhPutawayInventorySMainViewRow.getConfirmCaseQty());
        if (YES.equalsIgnoreCase(correctLoc) && YES.equalsIgnoreCase(correctQty)) {
            if (YES.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag())) {
                if (YES.equalsIgnoreCase(this.callVPutQty(hhPutawayInventorySMainViewRow.getConfirmCaseQty()))) {
                    this.processF4Key(hhPutawayInventorySMainViewRow.getToLocationId());
                }
            } else {
                if (YES.equalsIgnoreCase(this.callVPutQty(hhPutawayInventorySMainViewRow.getConfirmCaseQty()))) {
                    if (null != hhPutawayInventorySWorkLocalViewRow.getReasonCode() &&
                        !(nvl(hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                              NO)).equals(hhPutawayInventorySMainViewRow.getLocationId())) {
                        if (hhPutawayInventorySMainViewRow.getSuggestedLocId() != null) {
                            this.callProcessPutawayException(hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                                             hhPutawayInventorySWorkLocalViewRow.getReasonCode());
                        } else {
                            this.callProcessPutawayException(hhPutawayInventorySMainViewRow.getLocationId(),
                                                             hhPutawayInventorySWorkLocalViewRow.getReasonCode());
                        }
                        hhPutawayInventorySWorkLocalViewRow.setReasonCode(null);
                    }
                    this.processF4Key(hhPutawayInventorySMainViewRow.getToLocationId());
                    if (YES.equals(this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg())) {
                        goBlock = this.continueF4key();
                    }
                }
            }
        } else {
            if (NO.equalsIgnoreCase(correctLoc)) {
                this.processInvLocId();
            } else {
                if (NO.equalsIgnoreCase(correctQty)) {
                    this.processInvQty();
                }
            }
        }
        return goBlock;
    }

    private String processF3() {
        String goBlock = "";
        this.callPupdatePendingQtys();
        if (this.getHhPutawayInventorySBeanPageFlowBean() != null &&
            YES.equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg())) {
            this.clearCurrentRow();
            this.getHhPutawayInventorySMainViewCurrentRow().setBlock("MainIntrlvg");
            goBlock = "MainIntrlvg";
        }
        return goBlock;
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        _logger.fine("Key value: " + key);
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            this.getHhPutawayInventorySMainViewCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        String goBlock = "";
        if ("F3".equalsIgnoreCase(key)) {
            goBlock = this.processF3();
            if (!"MainIntrlvg ".equalsIgnoreCase(goBlock)) {
                this.callTrExit();
                
                if ("TQ".equals(getHhPutawayInventorySBeanPageFlowBean().getPmCallingForm())){
                    popTaskFlow(3);
                    return;
                }
                    
                ADFUtils.invokeAction("Home");
            }
        } else {
            if ("F4".equalsIgnoreCase(key)) {
                this.prcoessF4key(hhPutawayInventorySMainViewRow, hhPutawayInventorySWorkLocalViewRow);
            } else {
                if ("F5".equalsIgnoreCase(key)) {
                    goBlock = this.processF5key(hhPutawayInventorySMainViewRow, hhPutawayInventorySWorkLocalViewRow);
                } else {
                    if ("F8".equalsIgnoreCase(key)) {
                        this.processF8Key(hhPutawayInventorySMainViewRow);
                    }
                }
            }
        }
        if (goBlock != null && !goBlock.isEmpty()) {
            ADFUtils.invokeAction(goBlock);
        }
        _logger.info("trKeyIn() End");
    }

    private void processF8Key(HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow) {
        if (hhPutawayInventorySMainViewRow.getToLocationId() != null) {
            List<String> codeList = this.callMarkLocation();
            if (codeList != null) {
                if ("FAILED".equalsIgnoreCase(codeList.get(0))) {

                    this.showMessagesPanel(ERROR,
                                           this.getMessage(codeList.get(2), codeList.get(1),
                                                           this.getFacilityIdAttrValue(), this.getLangCodeAttrValue()));
                } else {

                    this.showMessagesPanel("S",
                                           this.getMessage("SUCCESS_OPER", "", this.getFacilityIdAttrValue(),
                                                           this.getLangCodeAttrValue()));
                    this.removeErrorStyleClass();
                    this.callDoCommit();

                }
            }
        }
    }

    private String processF5key(HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow,
                                HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow) {
        if ("MAIN.CONTAINER_ID".equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getIsFocusOn())) {
            this.setErrorOutputText(this.getErrorOutputText());
            this.showMessagesPanel(ERROR,
                                   (this.getMessage("NO_LOC_YET", ERROR, this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue())));

        } else {
            if (hhPutawayInventorySMainViewRow.getSuggestedLocId() != null) {
                hhPutawayInventorySMainViewRow.setToLocationId(null);
                hhPutawayInventorySMainViewRow.setConfirmCaseQty(null);
                List<String> returnList = this.callCkRsnCdReq();
                if (!returnList.isEmpty()) {
                    if (YES.equalsIgnoreCase(returnList.get(0))) {
                        this.setErrorStyleClass(this.getToLocationId(), false);
                        hhPutawayInventorySWorkLocalViewRow.setCallingItem("F5");
                        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
                        return "Reason";
                    } else {
                        if ("NONE".equalsIgnoreCase(returnList.get(1))) {
                            this.setErrorStyleClass(this.getToLocationId(), true);
                            this.showMessagesPanel(WARN,
                                                   this.getMessage("NO_RSN_CD_FOUND", WARN,
                                                                   this.getFacilityIdAttrValue(),
                                                                   this.getLangCodeAttrValue()));
                        }
                        this.callProcessPutawayException(hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                                         returnList.get(1));

                        if (this.callPupdatePendingQtys().getErrors().isEmpty()) {
                            hhPutawayInventorySMainViewRow.setSuggestedLocId(null);
                            returnList = this.callFindLocation();
                            if (null != returnList)
                                if (NO_ERROR_STATUS.equalsIgnoreCase(returnList.get(0))) {
                                    if (null == hhPutawayInventorySMainViewRow.getSuggestedLocId()) {
                                        this.setErrorStyleClass(this.getToLocationId(), true);
                                        this.showMessagesPanel(INFO,
                                                               this.getMessage("NO_EMPTY_LOC", INFO,
                                                                               this.getFacilityIdAttrValue(),
                                                                               this.getLangCodeAttrValue()));
                                    }
                                } else {
                                    if (ERROR_STATUS.equalsIgnoreCase(returnList.get(0))) {
                                        this.setErrorStyleClass(this.getToLocationId(), true);
                                        this.showMessagesPanel(ERROR, returnList.get(1));
                                    }
                                }
                        }
                    }
                }
            } else {
                this.noLocYet();
            }
        }
        return null;
    }

    private void noLocYet() {
        this.showMessagesPanel(ERROR,
                               this.getMessage("NO_LOC_YET", ERROR, this.getFacilityIdAttrValue(),
                                               this.getLangCodeAttrValue()));
        this.setErrorStyleClass(this.getConfirmCaseQty(), false);
        this.setErrorStyleClass(this.getToLocationId(), true);
        this.getToLocationId().setDisabled(false);
        this.getConfirmCaseQty().setDisabled(true);
        this.refreshContentOfUIComponent(this.getToLocationId());
        this.refreshContentOfUIComponent(this.getConfirmCaseQty());
        this.setFocusToLocationId();
        this.selectTextOnUIComponent(this.getToLocationId());
    }

    private String continueF4key() {
        this.getHhPutawayInventorySBeanPageFlowBean().setPmNoPutFlag(NO);
        this.getHhPutawayInventorySBeanPageFlowBean().setPmNoTaskFlag(NO);
        this.callPrepareExitPutaway();
        this.setGlobalVariables("INTRLVG");
        GlobalVariablesViewRowImpl grow = this.getGlobalVariablesCurrentRow();
        Object flag = grow.getBP2PutawayNoPickFlag();
        if (flag == null)
            return "goBulkPick";
        else
            return "nextIntrlvg";
    }

    public void navigateToBulkPick() {
        this.navigateToAction("goBulkPick");
    }

    public void confirmMarkDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmMarkDialogListener() Start");
        boolean yesClicked = (dialogEvent.getOutcome()).equals(DialogEvent.Outcome.yes);
        if (yesClicked) {
            this.hideMessagesPanel();
            this.trKeyIn("F8");
        }
        HhPutawayInventorySBean piBean = this.getHhPutawayInventorySBeanPageFlowBean();
        String isFocusOn = piBean.getIsFocusOn();
        switch (isFocusOn) {
        case "MAIN.TOLOCATION_ID":
            this.getToLocationId().setDisabled(false);
            this.refreshContentOfUIComponent(this.getToLocationId());
            this.setFocusToLocationId();
            break;
        case "MAIN.TOLOCATION_ID_R":
            this.getToLocationId().setDisabled(false);
            this.refreshContentOfUIComponent(this.getToLocationId());
            this.setFocusToLocationId();
            break;
        case "MAIN.CONFIRM_CASE_QTY_R":
            this.getConfirmCaseQty().setDisabled(false);
            this.refreshContentOfUIComponent(this.getConfirmCaseQty());
            this.setFocusConfirmCaseQty();
            break;
        case "MAIN.CONFIRM_CASE_QTY":
            this.getConfirmCaseQty().setDisabled(false);
            this.refreshContentOfUIComponent(this.getConfirmCaseQty());
            this.setFocusConfirmCaseQty();
            break;
        default:
            break;
        }
        _logger.info("confirmMarkDialogListener() End");
    }

    private List<String> callMarkLocation() {
        _logger.info("callmarkLocation() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_MARK_LOC_OPER);
        op.getParamsMap().put("locationId", this.getHhPutawayInventorySMainViewCurrentRow().getToLocationId());
        List<String> returnList = (List<String>) op.execute();
        if (op.getErrors().isEmpty()) {
            _logger.info("callMarkLocation() End");
            _logger.fine("returnList value: " + returnList);
            return returnList;
        }
        _logger.info("callMarkLocation() End");
        return null;
    }

    private void callDoCommit() {
        ADFUtils.findOperation("callDoCommit").execute();
    }

    private List<String> callFindLocation() {
        _logger.info("callFindLocation() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_FIND_LOCATIONS_OPER);
        op.getParamsMap().put("pmAllowIntrlvg", this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg());
        op.getParamsMap().put("pmStartLoc", this.getHhPutawayInventorySBeanPageFlowBean().getPmStartLocation());
        List<String> returnList = (List<String>) op.execute();
        if (op.getErrors().isEmpty())
            if (!returnList.isEmpty()) {
                _logger.info("callFindLocation() End");
                _logger.fine("returnList value: " + returnList);
                return returnList;
            }
        _logger.info("callFindLocation() End");
        return null;
    }

    private List<String> callCkRsnCdReq() {
        _logger.info("callCkRsnCdReq() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_CKRSNCDREQ_OPER);
        List<String> returnList = (List<String>) op.execute();
        if (op.getErrors().isEmpty())
            if (!returnList.isEmpty()) {
                _logger.info("callCkRsnCdReq() End");
                _logger.fine("returnList value: " + returnList);
                return returnList;
            }
        _logger.info("callCkRsnCdReq() End");
        return null;

    }


    private void callPrepareExitPutaway() {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_PREPARE_EXIT_PUTAWAY_OPER);
        op.execute();
    }

    private void setGlobalVariables(String modeIn) {
        _logger.info("setGlobalVariables() Start");
        _logger.fine("modeIn value: " + modeIn);
        GlobalVariablesViewRowImpl globalVariablesRow = this.getGlobalVariablesCurrentRow();
        if ("EXIT_INTRLVG".equalsIgnoreCase(modeIn)) {
            globalVariablesRow.setGlobalAllowIntrlvg(NO);
            globalVariablesRow.setGlobalNoPutFlag(NO);
            globalVariablesRow.setGlobalNoTaskFlag(NO);
            globalVariablesRow.setGlobalNoPickFlag(NO);
            globalVariablesRow.setGlobalCallingForm(null);
            globalVariablesRow.setGlobalProcessByAisle(NO);
        } else {
            globalVariablesRow.setGlobalCallingForm("hh_putaway_inventory_s");
            globalVariablesRow.setGlobalNoPutFlag(this.getHhPutawayInventorySBeanPageFlowBean().getPmNoPutFlag());
            globalVariablesRow.setGlobalAllowIntrlvg(this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg());
            globalVariablesRow.setGlobalNoTaskFlag(this.getHhPutawayInventorySBeanPageFlowBean().getPmNoTaskFlag());
        }
        _logger.info("setGlobalVariablesn() End");
    }

    private GlobalVariablesViewRowImpl getGlobalVariablesCurrentRow() {
        return (GlobalVariablesViewRowImpl) ADFUtils.findIterator("GlobalVariablesViewIterator").getCurrentRow();
    }


    public void callProcessPutawayException(String locationId, String code) {
        _logger.info("callProcessPutawayException() Start");
        _logger.fine("Location Id: " + locationId + "\n" + "code value: " + code);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_PROCESS_PUTAWAY_EXCEPTION_OPER);
        op.getParamsMap().put("locationId", locationId);
        op.getParamsMap().put("code", code);
        List<String> codes = (List<String>) op.execute();
        if (codes != null) {
            this.showMessagesPanel(ERROR, codes.get(1));
        } else {
            this.hideMessagesPanel();
        }
        _logger.info("callProcessPutawayException() End");
    }

    public Object nvl(Object arg0, Object arg1) {
        return (arg0 == null) ? arg1 : arg0;
    }

    public void processF4Key(String locationId) {
        _logger.info("processF4Key() Start");
        _logger.fine("Location Id: " + locationId);
        String containerId = null;
        String itemId = null;
        Boolean continuePutaway = Boolean.FALSE;
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            this.getHhPutawayInventorySMainViewCurrentRow();
        HhPutawayInventorySWorkLaborProdNewViewRowImpl hhPutawayInventorySWorkLaborProdViewRow =
            this.getHhPutawayInventorySWorkLaborProdViewCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        containerId = hhPutawayInventorySMainViewRow.getContainerId();
        itemId = hhPutawayInventorySMainViewRow.getItemId();
        OperationBinding op = this.callPupdatePendingQtys();
        if (op.getErrors().isEmpty()) {
            op =
                this.callPutawayMerchandise(locationId, containerId, itemId, hhPutawayInventorySMainViewRow.getBlock());
            if (op.getErrors().isEmpty()) {
                List<Object> result = (List<Object>) op.getResult();
                if (null != result) {
                    if (YES.equalsIgnoreCase((String) result.get(0))) {
                        this.showMessagesPanel("S",
                                               this.getMessage("SUCCESS_OPER", "", this.getFacilityIdAttrValue(),
                                                               this.getLangCodeAttrValue()));
                        this.getHhPutawayInventorySBeanPageFlowBean().setIntrlvgIsLoaded(false);
                        this.callDoCommit();
                        hhPutawayInventorySWorkLaborProdViewRow.setContainersProcessed((hhPutawayInventorySWorkLaborProdViewRow.getContainersProcessed()).add((BigDecimal) result.get(2)));
                        hhPutawayInventorySWorkLaborProdViewRow.setUnitsProcessed(hhPutawayInventorySWorkLaborProdViewRow.getUnitsProcessed().add((BigDecimal) result.get(1)));
                        if ("Main".equalsIgnoreCase(hhPutawayInventorySMainViewRow.getBlock())) {
                            hhPutawayInventorySWorkLaborProdViewRow.setOperationsPerformed(hhPutawayInventorySWorkLaborProdViewRow.getOperationsPerformed().add(new BigDecimal(1)));
                            if (YES.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag()))
                                if (NO.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getReplenEntireContFlag()))
                                    continuePutaway = Boolean.TRUE;
                            this.getContainerId().setDisabled(false);
                            this.getConfirmCaseQty().setDisabled(true);
                            this.refreshContentOfUIComponent(this.getContainerId());
                            this.refreshContentOfUIComponent(this.getConfirmCaseQty());
                            this.setFocusContainerId();
                            if (Boolean.FALSE.equals(continuePutaway)) {
                                if (null == this.getHhPutawayInventorySBeanPageFlowBean().getPutawayContainerId()) {
                                
                                }
                                this.clearFields();
                            } else {
                                containerId = hhPutawayInventorySMainViewRow.getContainerId();
                                this.clearFields();
                                hhPutawayInventorySMainViewRow.setContainerId(containerId);
                                this.showMessagesPanel("S",
                                                       this.getMessage("PUT_CONTINUE", INFO,
                                                                       this.getFacilityIdAttrValue(),
                                                                       this.getLangCodeAttrValue()));
                            }
                        }
                    }
                }
            }
        }
        _logger.info("processF4Key() End");
    }

    private OperationBinding callPutawayMerchandise(String locationId, String containerId, String itemId,
                                                    String blockName) {
        _logger.info("callPutawayMerchandise() Start");
        _logger.fine("Location Id: " + locationId + "\n" + "Container Id: " + containerId + "Item id: " + itemId +
                     "Block Name: " + blockName);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_PUTAWAY_MERCHANDISE_OPER);
        op.getParamsMap().put("locationId", locationId);
        op.getParamsMap().put("containerId", containerId);
        op.getParamsMap().put("itemId", itemId);
        op.getParamsMap().put("blockName", blockName);
        op.execute();
        _logger.info("callPutawayMerchandise() End");
        _logger.fine("op value: " + op);
        return op;
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);

    }

    private String getContainerIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);

    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }


    private String callVContTransit(String containerId) {
        _logger.info("callVContTransit() Start");
        _logger.fine("Container Id: " + containerId);
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_V_CONT_TRANSIT_OPER);
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
        String result = null;
        if (oper.getErrors().isEmpty()) {
            result = (String) oper.getResult();
            if ("EQ".equals(result)) {
                this.showTransitCidPopup(facilityId, langCode);
            }
        }
        _logger.info("callVContTransit() End");
        return result;
    }

    private void showTransitCidPopup(String facilityId, String langCode) {
        this.getContainerId().setDisabled(true);
        this.refreshContentOfUIComponent(this.getContainerId());
        String msg = this.getMessage("CONFIRM_PUTAWAY", WARN, facilityId, langCode);
        this.getDialogOutputText().setValue(msg);
        this.getConfirmPopup().setLauncherVar(CONFIRM_P_POPUP_LAUNCHER);
        this.getConfirmPopup().show(new RichPopup.PopupHints());
        this.getHhPutawayInventorySBeanPageFlowBean().setIsPopupShowing(true);
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmDialogListener() Start");
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        String containerId = this.getContainerIdAttrValue();
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            if (CONFIRM_P_POPUP_LAUNCHER.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                if (!this.callCheckContainerTrouble(containerId))
                    this.callGetContainerInfo(containerId, facilityId);
            } else {
                if (CHECK_TRB_POPUP_LAUNCHER.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                    this.getContainerId().setDisabled(false);
                    this.refreshContentOfUIComponent(this.getContainerId());
                    this.callGetContainerInfo(containerId, facilityId);
                } else {
                    if (USER_CHOICE_DEST_CONFIRM.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                        this.getContainerId().setDisabled(false);
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.callGetContainerInfo2(containerId, facilityId);
                    } else {
                        if (USER_CHOICE_WIP_CONFIRM.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                            this.getContainerId().setDisabled(false);
                            this.refreshContentOfUIComponent(this.getContainerId());
                            this.callGetContainerInfo3(containerId, facilityId, YES);
                        }
                    }
                }
            }
        } else {
            if (CONFIRM_P_POPUP_LAUNCHER.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                this.getContainerId().setDisabled(false);
                this.refreshContentOfUIComponent(this.getContainerId());
                this.hideMessagesPanel();
                this.clearContainerField();
                this.selectTextOnUIComponent(this.getContainerId());
            } else {
                if (CHECK_TRB_POPUP_LAUNCHER.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                    this.f6ClearActionListener(new ActionEvent(this.getF6ClearLink()));
                } else {
                    if (USER_CHOICE_DEST_CONFIRM.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                        this.getContainerId().setDisabled(false);
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.showMessagesPanel(WARN, this.getMessage("PUT_INV_DEST", WARN, containerId, langCode));
                        this.clearContainerField();
                        this.selectTextOnUIComponent(this.getContainerId());
                    } else {
                        if (USER_CHOICE_WIP_CONFIRM.equalsIgnoreCase(this.getConfirmPopup().getLauncherVar())) {
                            this.getContainerId().setDisabled(false);
                            this.refreshContentOfUIComponent(this.getContainerId());
                            this.clearContainerField();
                            this.f6ClearActionListener(null);
                        }
                    }
                }
            }
        }
        _logger.info("confirmDialogListener() End");
    }


    private void clearContainerField() {
        ADFUtils.setBoundAttributeValue("ContainerId", null);
        this.getContainerId().resetValue();
    }


    private boolean callCheckContainerTrouble(String containerId) {
        _logger.info("callCheckContainerTrouble Start");
        _logger.fine("Container Id: " + containerId);
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_CHECK_CONTAINER_TROUBLE_OPER);
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("formName", "HH_PUTAWAY_INVENTORY_S");
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (YES.equalsIgnoreCase((String) oper.getResult())) {
                this.getContainerId().setDisabled(true);
                this.refreshContentOfUIComponent(this.getContainerId());
                this.getHhPutawayInventorySBeanPageFlowBean().setIsPopupShowing(true);
                String msg = this.getMessage("TRB_STATUS", CONF, facilityId, langCode);
                this.getDialogOutputText().setValue(msg);
                this.getConfirmPopup().setLauncherVar(CHECK_TRB_POPUP_LAUNCHER);
                this.getConfirmPopup().show(new RichPopup.PopupHints());
                _logger.info("callCheckContainerTrouble() End");
                return true;
            }

        }
        _logger.info("callCheckContainerTrouble() End");
        return false;
    }

    private HhPutawayInventorySMainViewRowImpl getHhPutawayInventorySMainViewCurrentRow() {
        return (HhPutawayInventorySMainViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMainViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySWorkLocalViewRowImpl getHhPutawayInventorySWorkLocalViewCurrentRow() {
        return (HhPutawayInventorySWorkLocalViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLocalViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySMainIntrlvgViewRowImpl getHhPutawayInventorySMainIntrlvgViewCurrentRow() {
        return (HhPutawayInventorySMainIntrlvgViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMainIntrlvgViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySWorkLaborProdNewViewRowImpl getHhPutawayInventorySWorkLaborProdViewCurrentRow() {
        return (HhPutawayInventorySWorkLaborProdNewViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLaborProdNewViewIterator").getCurrentRow();
    }

    private OperationBinding callPupdatePendingQtys() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_P_UPDATE_PENDING_QTYS_OPER);
        oper.execute();
        return oper;
    }

    private HhPutawayInventorySWorkViewRowImpl getHhPutawayInventorySWorkViewCurrentRow() {
        return (HhPutawayInventorySWorkViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkViewIterator").getCurrentRow();

    }

    private void callTrExit() {
        _logger.info("callTrExit() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_EXIT_TR_OPER);
        oper.getParamsMap().put("containerId", this.getHhPutawayInventorySBeanPageFlowBean().getPutawayContainerId());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                this.clearFields();
                this.showMessagesPanel(ERROR, codes.get(0));
            }
        }
        _logger.info("callTrExit() Start");
    }

    private void callGetContainerInfo(String containerId, String facilityId) {
        _logger.info("callGetContainerInfo() Start");
        _logger.fine("Container Id value: " + containerId + "\n" + "Facility Id value: " + facilityId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_GET_CONTAINER_INFO_OPER);
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                if (ERROR_STATUS.equals(codes.get(2))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                    this.setErrorStyleClass(this.getContainerId(), true);
                    this.showMessagesPanel(ERROR, codes.get(1));
                    this.setFocusContainerId();
                    this.selectTextOnUIComponent(this.getContainerId());

                } else {
                    if (WARNING_STATUS.equals(codes.get(2))) {
                        this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                        this.setErrorStyleClass(this.getContainerId(), true);
                        this.showMessagesPanel(WARN, codes.get(1));
                        this.setFocusContainerId();
                        this.selectTextOnUIComponent(this.getContainerId());
                    } else {
                        if (USER_CHOICE_DEST_CONFIRM.equals(codes.get(2))) {
                            String langCode = (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
                            String msg = this.getMessage(codes.get(2), codes.get(1), facilityId, langCode);
                            this.getContainerId().setDisabled(true);
                            this.refreshContentOfUIComponent(this.getContainerId());
                            this.getDialogOutputText().setValue(msg);
                            this.getConfirmPopup().setLauncherVar(USER_CHOICE_DEST_CONFIRM);
                            this.getConfirmPopup().show(new RichPopup.PopupHints());
                        } else {
                            if (NO_ERROR_STATUS.equals(codes.get(2))) {
                                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                                this.hideMessagesPanel();
                                this.callGetContainerInfo2(containerId, facilityId);
                            }
                        }
                    }
                }
            }
        }
        _logger.info("callGetContainerInfo() End");
    }


    private void callGetContainerInfo2(String containerId, String facilityId) {
        _logger.info("callGetContainerInfo2() Start");
        _logger.fine("Container Id value: " + containerId + "\n" + "Facility Id value: " + facilityId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_GET_CONTAINER_INFO2_OPER);
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                if (ERROR_STATUS.equals(codes.get(2))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                    this.setErrorStyleClass(this.getContainerId(), true);
                    this.showMessagesPanel(ERROR, codes.get(1));
                    this.setFocusContainerId();
                    this.selectTextOnUIComponent(this.getContainerId());
                } else {
                    if (USER_CHOICE_WIP_CONFIRM.equals(codes.get(2))) {
                        this.getContainerId().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.getDialogOutputText().setValue(codes.get(1));
                        this.getConfirmPopup().setLauncherVar(USER_CHOICE_WIP_CONFIRM);
                        this.getConfirmPopup().show(new RichPopup.PopupHints());
                    } else {
                        if (NO_ERROR_STATUS.equals(codes.get(2))) {
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                            this.hideMessagesPanel();
                            this.callGetContainerInfo3(containerId, facilityId, YES);
                        }
                    }
                }
            }
        }
        _logger.info("callGetContainerInfo2() End");
    }

    private HhPutawayInventorySBean getHhPutawayInventorySBeanPageFlowBean() {
        return ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN));

    }

    private void callGetContainerInfo3(String containerId, String facilityId, String wip) {
        _logger.info("callGetContainerInfo3() Start");
        _logger.fine("Container Id value: " + containerId + "\n" + "Facility Id value: " + facilityId + "\n" +
                     "wip value: " + wip);
        String allowIntrlvg =
            ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN)).getPmAllowIntrlvg();
        String startLocation =
            ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN)).getPmStartLocation();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_GET_CONTAINER_INFO3_OPER);
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("pmAllowaIntrlvg", allowIntrlvg);
        oper.getParamsMap().put("pmStartLocation", startLocation);
        oper.getParamsMap().put("wip", wip);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                if (WARNING_STATUS.equals(codes.get(0))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                    this.setErrorStyleClass(this.getContainerId(), true);
                    this.showMessagesPanel(WARN, codes.get(1));
                    this.setFocusContainerId();
                    this.selectTextOnUIComponent(this.getContainerId());
                }
                if (INFO_STATUS.equals(codes.get(0))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.showMessagesPanel(INFO, codes.get(2));

                }
                if ("MAIN_INTRLVG".equals(codes.get(0))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    ADFUtils.invokeAction("MainIntrlvg");
                }
                if (NO_ERROR_STATUS.equals(codes.get(0)) || MULTI_SKU_BLOCK.equals(codes.get(0))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.hideMessagesPanel();
                    this.callGetContainerInfo4(containerId, facilityId, allowIntrlvg, startLocation);
                }
            }
        }
        _logger.info("callGetContainerInfo3() End");

    }

    private void callGetContainerInfo4(String containerId, String facilityId, String allowIntrlvg,
                                       String startLocation) {
        _logger.info("callGetContainerInfo4() Start");
        _logger.fine("Container Id value: " + containerId + "\n" + "Facility Id value: " + facilityId + "\n" +
                     "allowIntrlvg value " + allowIntrlvg + "\n" + "startLocation value: " + startLocation);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_GET_CONTAINER_INFO4_OPER);
        oper.getParamsMap().put("containerId", containerId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("pmAllowaIntrlvg", allowIntrlvg);
        oper.getParamsMap().put("pmStartLocation", startLocation);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                if (INFO_STATUS.equals(codes.get(0))) {
                    this.showMessagesPanel(INFO, codes.get(2));
                }
                if (MULTI_SKU_BLOCK.equals(codes.get(0))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MULTI_SKU.CONFIRM_LOC");
                    this.goMultISku();
                } else {
                    if (NEW_FPL_BLOCK.equals(codes.get(0))) {
                        this.getContainerId().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.getConfirmGoToOutputtext().setValue(codes.get(2));
                        this.getConfirmGoToPopup().show(new RichPopup.PopupHints());
                    } else {
                        if (WARNING_STATUS.equals(codes.get(0))) {
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                            this.setErrorStyleClass(this.getContainerId(), true);
                            this.showMessagesPanel(WARN, codes.get(1));
                            this.selectTextOnUIComponent(this.getContainerId());
                        } else {
                            if (NO_ERROR_STATUS.equals(codes.get(0))) {
                                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                                this.setErrorStyleClass(this.getContainerId(), false);
                                this.hideMessagesPanel();
                                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                                this.getContainerId().setDisabled(true);
                                this.getToLocationId().setDisabled(false);
                                this.refreshContentOfUIComponent(this.getToLocationId());
                                this.getF5NextLocLink().setImmediate(true);
                                this.refreshContentOfUIComponent(this.getF5NextLocLink());
                                this.setFocusToLocationId();
                                this.refreshContentOfUIComponent(this.getF5NextLocLink());
                                this.refreshContentOfUIComponent(this.getF8MarkLink());
                            }
                        }
                    }
                }
            }
        }
        _logger.info("callGetContainerInfo3() End");
    }

    private void goMultISku() {
        (new ActionEvent(this.getGoMusltiSku())).queue();
    }

    public void confirmGoToDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
            ADFUtils.invokeAction("NewFpl");
        }
    }


    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType) || "S".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel() End");
    }

    private void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void clearFields() {
        _logger.info("clearFields() Start");
        this.clearCurrentRow();
        this.getContainerId().resetValue();
        this.getConfirmCaseQty().resetValue();
        this.getItemDesc().resetValue();
        this.getCaseQty().resetValue();
        this.getItemId().resetValue();
        this.getLocationId().resetValue();
        this.getSuggestedLocId().resetValue();
        this.getUpsCode().resetValue();
        this.getToLocationId().resetValue();
        this.getStorage().resetValue();
        this.refreshContentOfUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(this.getConfirmCaseQty());
        this.refreshContentOfUIComponent(this.getItemDesc());
        this.refreshContentOfUIComponent(this.getCaseQty());
        this.refreshContentOfUIComponent(this.getItemId());
        this.refreshContentOfUIComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getToLocationId());
        this.refreshContentOfUIComponent(this.getSuggestedLocId());
        this.refreshContentOfUIComponent(this.getUpsCode());
        this.refreshContentOfUIComponent(this.getStorage());
        _logger.info("clearFields() End");
    }


    private String callVPutawayLocation() {
        _logger.info("callVPutawayLocation() Start");
        HhPutawayInventorySWorkLocalViewRowImpl workLocalRow = this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        String locationId = this.getHhPutawayInventorySMainViewCurrentRow().getToLocationId();
        String pickType = workLocalRow.getPickType();
        String hotReplenFlag = workLocalRow.getHotReplenFlag();
        Object suggestedLocId = ADFUtils.getBoundAttributeValue("SuggestedLocId");
        if ("BR".equals(pickType) || "BD".equals(pickType) || YES.equals(hotReplenFlag)) {
            if (locationId == null || !locationId.equals(suggestedLocId)) {
                this.showMessagesPanel(WARN,
                                       this.getMessage("LOC_MISMATCH", WARN, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                _logger.info("callVPutawayLocation() End");
                this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("TO");
                return NO;
            }
        }
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_V_PUTAWAY_LOCATION_OPER);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                if (codes != null) {
                    if (ERROR.equalsIgnoreCase(codes.get(0))) {
                        this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                        this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("TO");
                        this.showMessagesPanel(ERROR, codes.get(1));
                        _logger.info("callVPutawayLocation() End");
                        return NO;
                    } else {
                        if (WARN.equalsIgnoreCase(codes.get(0))) {
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("TO");
                            this.showMessagesPanel(WARN, codes.get(1));
                            _logger.info("callVPutawayLocation() End");
                            return NO;
                        } else {
                            this.hideMessagesPanel();
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("");
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                            _logger.info("callVPutawayLocation() End");
                            return YES;
                        }
                    }
                }
            } else {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("");
                return YES;
            }
        }
        _logger.info("callVPutawayLocation() End");
        return NO;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null && !currentPageDef.startsWith(PAGE1_PAGEDEF_NAME)) {
            navigation = true;
        }
        return navigation;
    }

    public void onChangedConfirmCaseQtyMain(ValueChangeEvent vce) {
        _logger.info("onChangedConfirmCaseQtyMain() Start");
        _logger.fine("vce value: " + vce);
        long lStartTime = new Date().getTime();
        _logger.info("---------------------------------");
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        String newValue = (String) vce.getNewValue();
        this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
        this.hideMessagesPanel();
        if (newValue != null) {
            if (!newValue.matches("[0-9]+")) {
                ADFUtils.setBoundAttributeValue("ConfirmCaseQty", new BigDecimal(-1));
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                this.showMessagesPanel(WARN,
                                       this.getMessage("QTY_MISMATCH", WARN, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                this.setErrorStyleClass(this.getConfirmCaseQty(), true);
                this.selectTextOnUIComponent(this.getConfirmCaseQty());
                _logger.info("onChangedConfirmCaseQtyMain() End");
                return;
            }
            BigDecimal newValueDec = new BigDecimal(newValue);
            ADFUtils.setBoundAttributeValue("ConfirmCaseQty", newValueDec);
            this.callVPutQty(newValueDec);
        }

        long lEndTime = new Date().getTime();
        long difference = lEndTime - lStartTime;
        _logger.info("Elapsed time for method call  enter qty--ADF +DB milliseconds: " + difference);
        _logger.info("onChangedConfirmCaseQtyMain() End");
    }

    private String callVPutQty(BigDecimal qty) {
        _logger.info("callVPutQty() Start");
        if (null == qty) {
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLangCodeAttrValue()));
            return NO;
        } else if (!qty.equals(ADFUtils.getBoundAttributeValue("CaseQty"))) {
            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
            this.showMessagesPanel(WARN,
                                   this.getMessage("QTY_MISMATCH", WARN, this.getFacilityIdAttrValue(),
                                                   this.getLangCodeAttrValue()));
            this.setErrorStyleClass(this.getConfirmCaseQty(), true);
            this.selectTextOnUIComponent(this.getConfirmCaseQty());
            _logger.info("callVPutQty() End");
            return NO;
        } else {
            ADFUtils.setBoundAttributeValue("ConfirmCaseQty", qty);
            this.setErrorStyleClass(this.getConfirmCaseQty(), false);
            if (!this.getHhPutawayInventorySBeanPageFlowBean().getIsError())
                this.hideMessagesPanel();
            _logger.info("callVPutQty() End");
            return YES;
        }
    }


    public void f6ClearActionListener(ActionEvent actionEvent) {
        _logger.info("f6ClearActionListener() Start");
        this.removeErrorStyleClass();
        this.clearFields();
        this.hideMessagesPanel();
        this.getContainerId().setDisabled(false);
        this.getToLocationId().setDisabled(true);
        this.getConfirmCaseQty().setDisabled(true);
        this.refreshContentOfUIComponent(this.getF4DoneLink());
        this.refreshContentOfUIComponent(this.getF5NextLocLink());
        this.refreshContentOfUIComponent(this.getF8MarkLink());
        this.refreshContentOfUIComponent(this.getF6ClearLink());
        this.setFocusContainerId();
        _logger.info("f6ClearActionListener() End");
    }


    private void clearCurrentRow() {
        _logger.info("clearCurrentRow() Start");
        HhPutawayInventorySMainViewRowImpl hhRowImpl = this.getHhPutawayInventorySMainViewCurrentRow();
        hhRowImpl.setCaseQty(null);
        hhRowImpl.setConfirmCaseQty(null);
        hhRowImpl.setContainerId(null);
        hhRowImpl.setItemDesc(null);
        hhRowImpl.setItemId(null);
        hhRowImpl.setSuggestedLocId(null);
        hhRowImpl.setLocationId(null);
        hhRowImpl.setToLocationId(null);
        hhRowImpl.setUpsCode(null);
        hhRowImpl.setStorageCategory(null);
        _logger.info("f6ClearCurrentRow() End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ADFContext.getCurrent().getRequestScope().put("F5Key", YES);
                Object suggLoc = ADFUtils.getBoundAttributeValue("SuggestedLocId");
                if (null == suggLoc || (suggLoc.toString()).isEmpty())
                    ADFContext.getCurrent().getRequestScope().put("noSuggLoc",
                                                                  this.getMessage("NO_LOC_YET", ERROR,
                                                                                  this.getFacilityIdAttrValue(),
                                                                                  this.getLangCodeAttrValue()));
                ActionEvent actionEvent = new ActionEvent(this.getF5NextLocLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                this.f6ClearActionListener(new ActionEvent(clientEvent.getComponent()));
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8MarkLink());
                actionEvent.queue();
            } else if (F1_KEY_CODE.equals(keyPressed)) {
                this.getConfirmMarkPopup().hide();
                this.confirmMarkDialogListener(new DialogEvent(this.getConfirmMarkDialog(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.getConfirmMarkPopup().hide();
                this.confirmMarkDialogListener(new DialogEvent(this.getConfirmMarkDialog(), DialogEvent.Outcome.no));
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("mcid".equalsIgnoreCase(item) && submittedValue != null && !submittedValue.isEmpty()) {
                    this.setErrorStyleClass(this.getContainerId(), false);
                    this.onChangedContainerIdMain(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                } else if ("mlid".equalsIgnoreCase(item) && submittedValue != null && !submittedValue.isEmpty()) {
                    this.setErrorStyleClass(this.getLocationId(), false);
                    this.onChangedToLocationIdMain(new ValueChangeEvent(this.getLocationId(), null, submittedValue));
                    if (!"TO".equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getIsErrorOn()))
                        this.performQtyFocus();
                } else if ("ccqt".equalsIgnoreCase(item) && submittedValue != null && !submittedValue.isEmpty()) {
                    this.setErrorStyleClass(this.getConfirmCaseQty(), false);
                    this.onChangedConfirmCaseQtyMain(new ValueChangeEvent(this.getConfirmCaseQty(), null,
                                                                          submittedValue));
                }

            }
        }
        _logger.info("performKey() End");
    }

    public void performQtyFocus() {
        this.getConfirmCaseQty().setDisabled(false);
        this.setFocusConfirmCaseQty();
        this.refreshContentOfUIComponent(this.getPage1PanelGridLayout());
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        _logger.info("peformKeyPopup() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.getConfirmPopup().hide();
                this.getContainerId().setDisabled(false);
                this.refreshContentOfUIComponent(this.getContainerId());
                this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.getConfirmPopup().hide();
                this.getContainerId().setDisabled(false);
                this.refreshContentOfUIComponent(this.getContainerId());
                this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.no));
            }
        }
        _logger.info("peformKeyPopup() End");
    }


    public void yesBtnPopupMarkActionListener(ActionEvent actionEvent) {
        this.getConfirmMarkPopup().hide();
        this.confirmMarkDialogListener(new DialogEvent(this.getConfirmMarkDialog(), DialogEvent.Outcome.yes));
    }

    public void noBtnPopupMarkActionListener(ActionEvent actionEvent) {
        this.getConfirmMarkPopup().hide();
        this.confirmMarkDialogListener(new DialogEvent(this.getConfirmMarkDialog(), DialogEvent.Outcome.no));
    }


    public void okLinkGotoPopupActionListener(ActionEvent actionEvent) {
        _logger.info("okLinkGotoPopupActionListener() Start");
        this.getConfirmGoToPopup().hide();
        if (!"callFromForm".equalsIgnoreCase(this.getConfirmGoToPopup().getLauncherVar())) {
            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
            ADFUtils.invokeAction("NewFpl");
        } else {
            ADFUtils.invokeAction("backHome");
        }
        _logger.info("okLinkGotoPopupActionListener() End");
    }

    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsPopupShowing(false);
        this.getConfirmPopup().hide();
        this.getContainerId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getContainerId());
        this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsPopupShowing(false);
        this.getConfirmPopup().hide();
        this.getContainerId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getContainerId());
        this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.no));
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Putaway Start");
        String callingForm = this.getHhPutawayInventorySBeanPageFlowBean().getPmCallingForm();
        String errorMsg = this.getHhPutawayInventorySBeanPageFlowBean().getFormCallErrorMsg();
        String modeIn = this.getHhPutawayInventorySBeanPageFlowBean().getModeIn();
        HhPutawayInventorySMainViewRowImpl currentMainRow = this.getHhPutawayInventorySMainViewCurrentRow();
        if (null != errorMsg && !"MainIntrlvg".equalsIgnoreCase(errorMsg)) {
            this.getConfirmGoToPopup().setLauncherVar("callFromForm");
            this.getErrorWarnInfoIcon().setName("warning");
            this.getConfirmGoToOutputtext().setValue(this.getErrorMessage(errorMsg, modeIn));
            this.getConfirmGoToPopup().show(new RichPopup.PopupHints());
            _logger.info("onRegionLoad Putaway End");
            return;
        } else if ("MainIntrlvg".equalsIgnoreCase(errorMsg)) {
            if (currentMainRow != null) {
                HhPutawayInventorySMainIntrlvgViewRowImpl currentRow =
                    this.getHhPutawayInventorySMainIntrlvgViewCurrentRow();
                String cid = currentRow == null ? null : currentRow.getContainerId();
                if (null != cid) {
                    this.callGetContainerInfo(cid, this.getFacilityIdAttrValue());
                    this.getEvenlPanel().setVisible(false);
                    this.getEvenlPanel().setRendered(false);
                    this.refreshContentOfUIComponent(this.getEvenlPanel());
                }
            }
        }
        else if (callingForm != null && callingForm.equals("TQ")  /*errorMsg == null 
                                                                    Fix 3971: this block only if we come from Task Queue. */){
           HhPutawayInventorySMainViewRowImpl mainrow = this.getHhPutawayInventorySMainViewCurrentRow();
            if (mainrow != null){
                String cid = mainrow.getContainerId();
                if (null != cid){
                    this.callGetContainerInfo(cid, this.getFacilityIdAttrValue());
                }                
            }
        }
        /*Fix 3971: Deactivate this panel to ensure onRegionLoad is not executed again*/
        this.getEvenlPanel().setVisible(false);
        this.getEvenlPanel().setRendered(false);
        this.refreshContentOfUIComponent(this.getEvenlPanel());        
        _logger.info("onRegionLoad Putaway End");
    }

    public List<String> callPopulatePutaway() {
        _logger.info("callPopulatePutaway() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPopulatePutaway");
        oper.getParamsMap().put("locationId", null);
        oper.getParamsMap().put("containerId", null);
        oper.getParamsMap().put("facilityId", this.getHhPutawayInventorySWorkViewCurrentRow().getFacilityId());
        oper.getParamsMap().put("pmAllowaIntrlvg", this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg());
        oper.getParamsMap().put("pmStartLocation", this.getHhPutawayInventorySBeanPageFlowBean().getPmStartLocation());
        oper.getParamsMap().put("pmodein", NO);
        _logger.info("callPopulatePutaway() End");
        return (List<String>) oper.execute();
    }

    private String getErrorMessage(String taskIdIn, String modeIn) {
        _logger.info("getErrorMessage() Start");
        _logger.fine("taskIdIn value: " + taskIdIn + "\n" + "modeIn value: " + modeIn);
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        String error = null;
        String noTaskFlag = this.getHhPutawayInventorySBeanPageFlowBean().getPmNoTaskFlag();
        String noPutFlag = this.getHhPutawayInventorySBeanPageFlowBean().getPmNoPutFlag();
        if (YES.equalsIgnoreCase(noTaskFlag)) {
            if (YES.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getProcessByAisle())) {
                error = this.getMessage("NO_MORE_TASK", WARN, facilityId, langCode);
            } else {
                error = this.getMessage("NO_TASK_ZONE", WARN, facilityId, langCode);
            }
            this.callPrepareExitPutaway();
            this.setGlobalVariables("EXIT_INTRLVG");
            return error;
        } else {
            if (NO.equalsIgnoreCase(noPutFlag))
                if (!"SKIP".equalsIgnoreCase(modeIn))
                    this.getHhPutawayInventorySBeanPageFlowBean().setPmNoPutFlag(YES);
            if ("LOCATION".equalsIgnoreCase(taskIdIn)) {
                error = this.getMessage("NO_EMPTY_LOC", WARN, facilityId, langCode);
            } else {
                error = this.getMessage("NO_PUTAWAY_TASK", WARN, facilityId, langCode);
                if (!YES.equalsIgnoreCase((String) (ADFContext.getCurrent().getPageFlowScope().get("pNoRemainingPick"))))
                    ADFContext.getCurrent().getViewScope().put("E2ENav", "BP");
            }
        }
        if ("SKIP".equalsIgnoreCase(modeIn))
            this.getHhPutawayInventorySBeanPageFlowBean().setPmNoTaskFlag(YES);
        this.callPrepareExitPutaway();
        this.setGlobalVariables("INTRLVG");
        _logger.info("getErrorMessage() End");
        return error;
    }

    public String okGoLinkAction() {
        this.getConfirmGoToPopup().hide();
        if (!"callFromForm".equalsIgnoreCase(this.getConfirmGoToPopup().getLauncherVar())) {
            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
            return "NewFpl";
        } else {
            Object nav = ADFContext.getCurrent().getViewScope().get("E2ENav");
            if ("BP".equals(nav)) {
                ADFContext.getCurrent().getViewScope().put("E2ENav", null);
                ADFUtils.invokeAction("goBulkPick");
            } else
                this.popTaskFlow(getPageFlowStackDepth() - 2);
        }
        return null;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmDialog(RichDialog confirmDialog) {
        this.confirmDialog = confirmDialog;
    }

    public RichDialog getConfirmDialog() {
        return confirmDialog;
    }


    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setWarningOutputText(RichOutputText warningOutputText) {
        this.warningOutputText = warningOutputText;
    }

    public RichOutputText getWarningOutputText() {
        return warningOutputText;
    }

    public void setErrorOutputText(RichOutputText errorOutputText) {
        this.errorOutputText = errorOutputText;
    }

    public RichOutputText getErrorOutputText() {
        return errorOutputText;
    }

    public void setInfoOutputText(RichOutputText infoOutputText) {
        this.infoOutputText = infoOutputText;
    }

    public RichOutputText getInfoOutputText() {
        return infoOutputText;
    }

    public void setPage1PanelGridLayout(RichPanelGridLayout page1PanelGridLayout) {
        this.page1PanelGridLayout = page1PanelGridLayout;
    }

    public RichPanelGridLayout getPage1PanelGridLayout() {
        return page1PanelGridLayout;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setItemId(RichInputText itemId) {
        this.itemId = itemId;
    }

    public RichInputText getItemId() {
        return itemId;
    }

    public void setUpsCode(RichInputText upsCode) {
        this.upsCode = upsCode;
    }

    public RichInputText getUpsCode() {
        return upsCode;
    }

    public void setItemDesc(RichInputText itemDesc) {
        this.itemDesc = itemDesc;
    }

    public RichInputText getItemDesc() {
        return itemDesc;
    }

    public void setCaseQty(RichInputText caseQty) {
        this.caseQty = caseQty;
    }

    public RichInputText getCaseQty() {
        return caseQty;
    }

    public void setSuggestedLocId(RichInputText suggestedLocId) {
        this.suggestedLocId = suggestedLocId;
    }

    public RichInputText getSuggestedLocId() {
        return suggestedLocId;
    }

    public void setToLocationId(RichInputText toLocationId) {
        this.toLocationId = toLocationId;
    }

    public RichInputText getToLocationId() {
        return toLocationId;
    }

    public void setConfirmCaseQty(RichInputText confirmCaseQty) {
        this.confirmCaseQty = confirmCaseQty;
    }

    public RichInputText getConfirmCaseQty() {
        return confirmCaseQty;
    }


    public void setF5NextLocLink(RichLink f5NextLocLink) {
        this.f5NextLocLink = f5NextLocLink;
    }

    public RichLink getF5NextLocLink() {
        return f5NextLocLink;
    }

    public void setF8MarkLink(RichLink f8MarkLink) {
        this.f8MarkLink = f8MarkLink;
    }

    public RichLink getF8MarkLink() {
        return f8MarkLink;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setContainerIdIcone(RichIcon containerIdIcone) {
        this.containerIdIcone = containerIdIcone;
    }

    public RichIcon getContainerIdIcone() {
        return containerIdIcone;
    }

    public void setToLocationIcone(RichIcon toLocationIcone) {
        this.toLocationIcone = toLocationIcone;
    }

    public RichIcon getToLocationIcone() {
        return toLocationIcone;
    }

    public void setConfrimCaseIcone(RichIcon confrimCaseIcone) {
        this.confrimCaseIcone = confrimCaseIcone;
    }

    public RichIcon getConfrimCaseIcone() {
        return confrimCaseIcone;
    }

    public void setConfirmMarkPopup(RichPopup confirmMarkPopup) {
        this.confirmMarkPopup = confirmMarkPopup;
    }

    public RichPopup getConfirmMarkPopup() {
        return confirmMarkPopup;
    }

    public void setDialogConfirmMarkOutputtext(RichOutputText dialogConfirmMarkOutputtext) {
        this.dialogConfirmMarkOutputtext = dialogConfirmMarkOutputtext;
    }

    public RichOutputText getDialogConfirmMarkOutputtext() {
        return dialogConfirmMarkOutputtext;
    }

    public void setConfirmGoToPopup(RichPopup confirmGoToPopup) {
        this.confirmGoToPopup = confirmGoToPopup;
    }

    public RichPopup getConfirmGoToPopup() {
        return confirmGoToPopup;
    }


    public void setConfirmGoToOutputtext(RichOutputText confirmGoToOutputtext) {
        this.confirmGoToOutputtext = confirmGoToOutputtext;
    }

    public RichOutputText getConfirmGoToOutputtext() {
        return confirmGoToOutputtext;
    }

    public void setConfirmGoToDialog(RichDialog confirmGoToDialog) {
        this.confirmGoToDialog = confirmGoToDialog;
    }

    public RichDialog getConfirmGoToDialog() {
        return confirmGoToDialog;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF6ClearLink(RichLink f6ClearLink) {
        this.f6ClearLink = f6ClearLink;
    }

    public RichLink getF6ClearLink() {
        return f6ClearLink;
    }

    public void setConfirmMarkDialog(RichDialog confirmMarkDialog) {
        this.confirmMarkDialog = confirmMarkDialog;
    }

    public RichDialog getConfirmMarkDialog() {
        return confirmMarkDialog;
    }

    public void setEvenlPanel(RichPanelGroupLayout evenlPanel) {
        this.evenlPanel = evenlPanel;
    }

    public RichPanelGroupLayout getEvenlPanel() {
        return evenlPanel;
    }


    public void setStorage(RichInputText storage) {
        this.storage = storage;
    }

    public RichInputText getStorage() {
        return storage;
    }


    public void setGoMusltiSku(RichLink goMusltiSku) {
        this.goMusltiSku = goMusltiSku;
    }

    public RichLink getGoMusltiSku() {
        return goMusltiSku;
    }
}
