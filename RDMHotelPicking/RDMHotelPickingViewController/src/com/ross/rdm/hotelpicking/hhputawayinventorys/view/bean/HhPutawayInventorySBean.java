package com.ross.rdm.hotelpicking.hhputawayinventorys.view.bean;


import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLocalViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkViewRowImpl;

import java.math.BigDecimal;

import java.util.List;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhPutawayInventorySBean extends RDMHotelPickingPageFlowBean {
    @SuppressWarnings("compatibility:-967273046172877555")
    private static final long serialVersionUID = -5472152972019458892L;
    private String pmAllowIntrlvg; // Item/Parameter hh_putaway_inventory_s.PM_ALLOW_INTRLVG
    private String pmProcessByAisle; // Item/Parameter hh_putaway_inventory_s.PM_PROCESS_BY_AISLE
    private String pmNoTaskFlag; // Item/Parameter hh_putaway_inventory_s.PM_NO_TASK_FLAG
    private String pmStartLocation; // Item/Parameter hh_putaway_inventory_s.PM_START_LOCATION
    private String pmNoPutFlag; // Item/Parameter hh_putaway_inventory_s.PM_NO_PUT_FLAG
    private String putawayContainerId; // Item/Parameter hh_putaway_inventory_s.PUTAWAY_CONTAINER_ID
    private String maxUsers;
    private String isFocusOn;
    private String isErrorOn;
    private String formCallErrorMsg;
    private String modeIn;
    private String pmCallingForm;

    private boolean intrlvgIsLoaded;
    private boolean isPopupShowing;
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhPutawayInventorySBean.class);

    public void initTaskFlow() {
        _logger.info("initTaskFlow() Start");
        //this.callCheckMaxUsers(); //commenting for testing performance issue
        this.createInsertMainCurrentRow();
        this.initGlobalesVariables();
        this.initHhPutawayInventorySWorkLocalVariables();
        this.initHhPutawayInventoryWorkLaborProdVariables();
        this.initHhPutawayInventoryWorkVariables();
        this.initParams();
        this.callSetAhlInfo();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewCurrentRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        if ("Y".equalsIgnoreCase(this.getPmAllowIntrlvg())) {
            GlobalVariablesViewRowImpl gVViewImplRowImpl = this.getGlobalVariablesViewCurrentRow();
            hhPutawayInventorySWorkLocalViewCurrentRow.setProcessByAisle(this.getPmProcessByAisle());
            hhPutawayInventorySWorkLocalViewCurrentRow.setCallingForm(gVViewImplRowImpl.getGlobalCallingForm());
            hhPutawayInventorySWorkLocalViewCurrentRow.setEventCode(new BigDecimal(54));
            List<String> callResult = callPopulatePutaway();
            this.setFormCallErrorMsg(null);
            if (callResult != null) {
                if ("MSG".equalsIgnoreCase(callResult.get(2))) {
                    this.setFormCallErrorMsg(callResult.get(0));
                    this.setModeIn(callResult.get(1));
                }
            } else {
                this.setFormCallErrorMsg("MainIntrlvg");
                this.setIntrlvgIsLoaded(false);
            }
        } else
            hhPutawayInventorySWorkLocalViewCurrentRow.setEventCode(new BigDecimal(20)); 
        
        if (getPutawayContainerId() != null && ! "".equals(getPutawayContainerId()) ){
            HhPutawayInventorySMainViewRowImpl mainrow = getHhPutawayInventorySMainViewCurrentRow();
            mainrow.setContainerId(getPutawayContainerId());
        }
        _logger.info("initTaskFlow() End");
    }

    public List<String> callPopulatePutaway() {
        _logger.info("callPopulatePutaway() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPopulatePutaway");
        oper.getParamsMap().put("locationId", null);
        oper.getParamsMap().put("containerId", null);
        oper.getParamsMap().put("facilityId", this.getHhPutawayInventorySWorkViewCurrentRow().getFacilityId());
        oper.getParamsMap().put("pmAllowaIntrlvg", this.getPmAllowIntrlvg());
        oper.getParamsMap().put("pmStartLocation", this.getPmStartLocation());
        oper.getParamsMap().put("pmodein", "N");
        _logger.info("callPopulatePutaway() End");
        return (List<String>) oper.execute();
    }


    private void createInsertMainCurrentRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsert");
        oper.execute();

    }

    public void setIsPopupShowing(boolean isPopupShowing) {
        this.isPopupShowing = isPopupShowing;
    }

    public boolean getIsPopupShowing() {
        return isPopupShowing;
    }


    public void setIntrlvgIsLoaded(boolean intrlvgIsLoaded) {
        this.intrlvgIsLoaded = intrlvgIsLoaded;
    }

    public boolean isIntrlvgIsLoaded() {
        return intrlvgIsLoaded;
    }

    public void setMultiSkuCurrentRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setMultiSkuCurrentRow");
        oper.execute();
    }

    public void setNewFplCurrentRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setNewFplCurrentRow");
        oper.execute();
    }

    public void setReasonCurrentRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setReasonCurrentRow");
        oper.execute();
        if (oper.getErrors().isEmpty()) {


        }
    }

    public void setMainIntrlvgCurrentRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setMainIntrlvgCurrentRow");
        oper.execute();
    }

    public void callCheckMaxUsers() {
        _logger.info("callCheckMaxUsers() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckMaxUsers");
        List<String> maxUsersCheck = (List<String>) oper.execute();
        if (oper.getErrors().isEmpty())
            if (maxUsersCheck != null) {
                this.setMaxUsers("N");
            }
        _logger.info("callCheckMaxUsers() End");
    }


    public void initParams() {
        _logger.info("initParams() Start");
        GlobalVariablesViewRowImpl gVViewImplRowImpl = this.getGlobalVariablesViewCurrentRow();
        if (this.getPmAllowIntrlvg() == null)
            this.setPmAllowIntrlvg(gVViewImplRowImpl.getGlobalAllowIntrlvg());
        if (this.getPmNoPutFlag() == null)
            this.setPmNoPutFlag(gVViewImplRowImpl.getGlobalNoPutFlag());
        if (this.getPmProcessByAisle() == null)
            this.setPmProcessByAisle(gVViewImplRowImpl.getGlobalProcessByAisle());
        if (this.getPmStartLocation() == null)
            this.setPmStartLocation(gVViewImplRowImpl.getGlobalStartPickLoca());
        this.setFormCallErrorMsg(null);
        this.setModeIn(null);
        this.setIsFocusOn("MAIN.CONTAINER_ID");
        this.setIsError(false);
        _logger.info("initParams() End");
    }

    private void initGlobalesVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariables");
        oper.execute();
    }

    private void initHhPutawayInventoryWorkLaborProdVariables() {
        OperationBinding oper =
            (OperationBinding) ADFUtils.findOperation("setHhPutawayInventoryWorkLaborProdVariables");
        oper.execute();
    }

    private void initHhPutawayInventorySWorkLocalVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhPutawayInventorySWorkLocalVariables");
        oper.execute();
    }


    private void initHhPutawayInventoryWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhPutawayInventoryWorkVariables");
        oper.execute();
    }

    public void callSetAhlInfo() {
        _logger.info("callSetAhlInfo() Start");
        String facilityId = null;
        if (null != this.getHhPutawayInventorySWorkViewCurrentRow()) {
            facilityId = this.getHhPutawayInventorySWorkViewCurrentRow().getFacilityId();
        } else {
            facilityId = "PR";
        }
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callSetAhlInfo");
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("formName", "HH_PUTAWAY_INVENTORY_S");
        oper.execute();
        _logger.info("callSetAhlInfo() End");
    }

    private HhPutawayInventorySMainViewRowImpl getHhPutawayInventorySMainViewCurrentRow() {
        return (HhPutawayInventorySMainViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMainViewIterator").getCurrentRow();
    }
    
    private HhPutawayInventorySWorkLocalViewRowImpl getHhPutawayInventorySWorkLocalViewCurrentRow() {
        return (HhPutawayInventorySWorkLocalViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLocalViewIterator").getCurrentRow();
    }


    private GlobalVariablesViewRowImpl getGlobalVariablesViewCurrentRow() {
        return (GlobalVariablesViewRowImpl) (ADFUtils.findIterator("GlobalVariablesViewIterator")).getCurrentRow();

    }


    private HhPutawayInventorySWorkViewRowImpl getHhPutawayInventorySWorkViewCurrentRow() {
        return (HhPutawayInventorySWorkViewRowImpl) (ADFUtils.findIterator("HhPutawayInventorySWorkViewIterator")).getCurrentRow();
    }


    public void setPmAllowIntrlvg(String pmAllowIntrlvg) {
        this.pmAllowIntrlvg = pmAllowIntrlvg;
    }

    public String getPmAllowIntrlvg() {
        return pmAllowIntrlvg;
    }

    public void setPmProcessByAisle(String pmProcessByAisle) {
        this.pmProcessByAisle = pmProcessByAisle;
    }

    public String getPmProcessByAisle() {
        return pmProcessByAisle;
    }

    public void setPmNoTaskFlag(String pmNoTaskFlag) {
        this.pmNoTaskFlag = pmNoTaskFlag;
    }

    public String getPmNoTaskFlag() {
        return pmNoTaskFlag;
    }

    public void setPmStartLocation(String pmStartLocation) {
        this.pmStartLocation = pmStartLocation;
    }

    public String getPmStartLocation() {
        return pmStartLocation;
    }

    public void setPmNoPutFlag(String pmNoPutFlag) {
        this.pmNoPutFlag = pmNoPutFlag;
    }

    public String getPmNoPutFlag() {
        return pmNoPutFlag;
    }

    public void setPutawayContainerId(String putawayContainerId) {
        this.putawayContainerId = putawayContainerId;
    }

    public String getPutawayContainerId() {
        return putawayContainerId;
    }

    public void setIsErrorOn(String isErrorOn) {
        this.isErrorOn = isErrorOn;
    }

    public String getIsErrorOn() {
        return isErrorOn;
    }
    private boolean isError;
    private String buttonPresed;

    public void setButtonPresed(String buttonPresed) {
        this.buttonPresed = buttonPresed;
    }

    public String getButtonPresed() {
        return buttonPresed;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }

    public boolean getIsError() {
        return isError;
    }

    public void setMaxUsers(String maxUsers) {
        this.maxUsers = maxUsers;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public String getMaxUsers() {
        return maxUsers;
    }

    public void setModeIn(String modeIn) {
        this.modeIn = modeIn;
    }

    public String getModeIn() {
        return modeIn;
    }

    public void setFormCallErrorMsg(String formCallErrorMsg) {
        this.formCallErrorMsg = formCallErrorMsg;
    }

    public String getFormCallErrorMsg() {
        return formCallErrorMsg;
    }


    public void setPmCallingForm(String pmCallingForm) {
        this.pmCallingForm = pmCallingForm;
    }

    public String getPmCallingForm() {
        return pmCallingForm;
    }
}
