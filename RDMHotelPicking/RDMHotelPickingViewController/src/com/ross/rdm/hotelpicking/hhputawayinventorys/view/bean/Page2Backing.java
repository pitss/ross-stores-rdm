package com.ross.rdm.hotelpicking.hhputawayinventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMultiSkuViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLaborProdNewViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLocalViewRowImpl;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMHotelPickingBackingBean {
    private RichInputText confirmLoction;
    private RichInputText confirmQty;
    private RichInputText containerId;
    private RichInputText locationId;
    private RichInputText containerQty;
    private RichInputText unitQty;
    private RichInputText itemId;
    private RichInputText description;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichLink f3ExitLink;
    private RichLink f5NextLocLink;
    private RichLink f8MarkLink;
    private RichLink f4DoneLink;
    private RichLink f6ClearLink;
    private RichPanelGroupLayout allMessagesPanel;
    private final static String INFO_STATUS = "INFO";
    private final static String CALL_V_PUTAWAY_LOCATION_OPER = "callVPutawayLocation";
    private final static String CALL_V_CONTAINER_ID_OPER = "callvContainerId";
    private final static String CALL_MARK_LOC_OPER = "callMarkLoc";
    private final static String CALL_V_PUT_QTY_OPER = "vPutQty";
    private final static String CALL_P_UPDATE_PENDING_QTYS_OPER = "callPupdatePendingQtys";
    private final static String CALL_EXIT_TR = "callTrExit";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String HH_PUTAWAY_PAGE_FLOW_BEAN = "HhPutawayInventorySBean";
    private final static String CONTAINER_ID_ATTR = "ContainerId1";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private RichIcon confirmLocIcone;
    private RichIcon confirmQtyIcone;
    private RichIcon containerIdIcone;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);


    public Page2Backing() {

    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        if (set) {
            String fieldId = field.getId();
            this.addErrorStyleToComponent(field);
            if (fieldId.equalsIgnoreCase("clms")) {
                this.getConfirmLocIcone().setName("error");
                this.refreshContentOfUIComponent(this.getConfirmLocIcone());
            } else if (fieldId.equalsIgnoreCase("cqms")) {
                this.getConfirmQtyIcone().setName("error");
                this.refreshContentOfUIComponent(this.getConfirmQtyIcone());
            } else {
                this.getContainerIdIcone().setName("error");
                this.refreshContentOfUIComponent(this.getContainerIdIcone());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("clms")) {
                this.getConfirmLocIcone().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getConfirmLocIcone());
            } else if (field.getId().equalsIgnoreCase("cqms")) {
                this.getConfirmQtyIcone().setName("");
                this.refreshContentOfUIComponent(this.getConfirmQtyIcone());
            } else {
                this.getContainerIdIcone().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcone());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        if ("F3".equalsIgnoreCase(key)) {
            this.callTrExit();
        } else {
            if ("F4".equalsIgnoreCase(key)) {
                if (YES.equalsIgnoreCase(this.callVPutawayLocation())) {
                    this.hideMessagesPanel();
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.setErrorStyleClass(this.getConfirmLoction(), false);
                    this.getConfirmQty().setDisabled(false);
                    this.refreshContentOfUIComponent(this.getConfirmQty());
                    this.setFocusConfirmQty();
                    if ("MULTI_SKU.CONFIRM_QTY".equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getIsFocusOn())) {
                        if (YES.equalsIgnoreCase(this.callVPutQty())) {
                            this.processF4Key(this.getHhPutawayInventorySMultiSkuViewCurrentRow().getConfirmLoc());
                        }
                    } else if (this.callVContainerId()) {
                        this.processF4Key(this.getHhPutawayInventorySMultiSkuViewCurrentRow().getConfirmLoc());
                    }
                } else {
                    this.setErrorStyleClass(this.getConfirmLoction(), true);
                }

            } else {
                if ("F8".equalsIgnoreCase(key)) {
                    if (this.getHhPutawayInventorySMultiSkuViewCurrentRow().getLocationId() != null) {
                        List<String> codeList = this.callMarkLocation();
                        if (codeList != null) {
                            if ("FAILED".equalsIgnoreCase(codeList.get(0))) {

                                this.showMessagesPanel(ERROR,
                                                       this.getMessage(codeList.get(2), codeList.get(1),
                                                                       this.getFacilityIdAttrValue(),
                                                                       this.getLangCodeAttrValue()));
                            } else {
                                this.showMessagesPanel(SUCCESS,
                                                       this.getMessage("SUCCESS_OPER", SUCCESS,
                                                                       this.getFacilityIdAttrValue(),
                                                                       this.getLangCodeAttrValue()));
                                this.removeErrorStyleClass();
                                this.selectTextOnUIComponent(this.getContainerId());

                            }
                        }
                    }
                }
            }
        }
        _logger.info("trKeyIn() End");
    }

    public void f3ExitActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
       
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MULTI_SKU_F4,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            if (this.validateContainerId() && this.validateConfrimLoc()) {
                this.trKeyIn("F4");
            }
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
        }
    }


    public void f8MarkActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_MULTI_SKU_F8,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            if (this.validateConfrimLoc())
                this.trKeyIn("F8");
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
        }
        
       
    }


    public void onChangedConfirmLocMultiSku(ValueChangeEvent vce) {
        _logger.info("onChangedConfirmLocMultiSku() Start");
        _logger.fine("vce value: " + vce);
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        this.updateModel(vce);
        if (vce.getNewValue() != null) {
            this.getHhPutawayInventorySMultiSkuViewCurrentRow().setConfirmLoc(((String) vce.getNewValue()).toUpperCase());
            if (YES.equalsIgnoreCase((this.callVPutawayLocation()))) {
                this.goNextNavigableField();
            } else {
                this.setErrorStyleClass(this.getConfirmLoction(), true);
            }
        }
        _logger.info("onChangedConfirmLocMultiSku() End");
    }

    private void goNextNavigableField() {
        this.hideMessagesPanel();
        this.setErrorStyleClass(this.getConfirmLoction(), false);
        this.getConfirmLocIcone().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getConfirmLocIcone());
        this.getConfirmLoction().setDisabled(true);
        this.refreshContentOfUIComponent(this.getConfirmLoction());
        if (null == ADFUtils.getBoundAttributeValue("LastConfirmQty")) {
            this.getContainerId().setDisabled(false);
            this.refreshContentOfUIComponent(this.getContainerId());
            this.setFocusContainerId();
        } else {
            this.getConfirmQty().setDisabled(false);
            this.refreshContentOfUIComponent(this.getConfirmQty());
            this.setFocusConfirmQty();
        }
    }

    private String processF4Key(String locationId) {
        _logger.info("processF4Key() Start");
        _logger.fine("locationId value: " + locationId);
        String containerId = null;
        String itemId = null;
        String goBlock = null;
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            this.getHhPutawayInventorySMainViewCurrentRow();
        HhPutawayInventorySWorkLaborProdNewViewRowImpl hhPutawayInventorySWorkLaborProdViewRow =
            this.getHhPutawayInventorySWorkLaborProdViewCurrentRow();
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            this.getHhPutawayInventorySMultiSkuViewCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();

        containerId = hhPutawayInventorySMultiSkuViewRow.getContainerId();
        itemId = hhPutawayInventorySMultiSkuViewRow.getItemId();
        OperationBinding op = this.callPupdatePendingQtys();
        op.execute();
        if (op.getErrors().isEmpty()) {
            op =
                this.callPutawayMerchandise(locationId, containerId, itemId, hhPutawayInventorySMainViewRow.getBlock());
            List<Object> result = (List<Object>) op.execute();
            if (op.getErrors().isEmpty()) {
                if (YES.equalsIgnoreCase((String) result.get(0))) {
                    this.callDoCommit();
                    this.hideMessagesPanel();
                    this.showMessagesPanel(SUCCESS,
                                           this.getMessage("SUCCESS_OPER", SUCCESS, this.getFacilityIdAttrValue(),
                                                           this.getFacilityIdAttrValue()));
                    hhPutawayInventorySWorkLaborProdViewRow.setContainersProcessed((hhPutawayInventorySWorkLaborProdViewRow.getContainersProcessed()).add((BigDecimal) result.get(2)));
                    hhPutawayInventorySWorkLaborProdViewRow.setUnitsProcessed(hhPutawayInventorySWorkLaborProdViewRow.getUnitsProcessed().add((BigDecimal) result.get(1)));
                    if ("MULTI_SKU.CONTAINER_ID".equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getIsFocusOn())) {
                        BigDecimal unitQty = hhPutawayInventorySMultiSkuViewRow.getUnitQty();
                        BigDecimal containerQty = hhPutawayInventorySMultiSkuViewRow.getContainerQty();
                        if (containerQty != null && containerQty.compareTo(new BigDecimal(0)) == 1)
                            hhPutawayInventorySMultiSkuViewRow.setContainerQty(hhPutawayInventorySMultiSkuViewRow.getContainerQty().subtract(new BigDecimal(1)));
                        if (unitQty != null && unitQty.compareTo(new BigDecimal(0)) == 1)
                            hhPutawayInventorySMultiSkuViewRow.setUnitQty(hhPutawayInventorySMultiSkuViewRow.getUnitQty().subtract((BigDecimal) result.get(1)));
                    }

                    if ("MULTI_SKU.CONTAINER_ID".equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getIsFocusOn()) ||
                        "MULTI_SKU.CONFIRM_QTY".equalsIgnoreCase(this.getHhPutawayInventorySBeanPageFlowBean().getIsFocusOn()) &&
                        hhPutawayInventorySMultiSkuViewRow.getContainerQty().equals(BigDecimal.ZERO)) {
                        hhPutawayInventorySWorkLaborProdViewRow.setOperationsPerformed(hhPutawayInventorySWorkLaborProdViewRow.getOperationsPerformed().add(new BigDecimal(1)));
                        hhPutawayInventorySWorkLocalViewRow.setPrevKittingLocId(hhPutawayInventorySMultiSkuViewRow.getLocationId());
                        if (YES.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag())) {
                            this.callAdjustPflValues(locationId, itemId);
                           // containerId = hhPutawayInventorySMainViewRow.getContainerId();
                            goBlock = "Main";
                            this.clearFields();
                            this.hideMessagesPanel();
                            this.showMessagesPanel(INF_ICON,
                                                   this.getMessage("PUT_CONTINUE", INF_ICON, this.getFacilityIdAttrValue(),
                                                                   this.getLangCodeAttrValue()));
                        } else {
                            op = this.callDisplayDirective();
                            if (op.getErrors().isEmpty()) {
                                List<String> codes = (List<String>) op.getResult();
                                if (codes != null) {
                                    if (codes.size() == 1 && NO.equalsIgnoreCase(codes.get(0))) {
                                        if (NO.equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getSingleSkuFlag()) &&
                                            (!"CR".equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getPickType()) &&
                                             !"CD".equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getPickType()))) {
                                            this.getConfirmQty().setDisabled(true);
                                            this.refreshContentOfUIComponent(this.getConfirmQty());
                                            this.getContainerId().setDisabled(false);
                                            this.refreshContentOfUIComponent(this.getContainerId());
                                            this.setFocusContainerId();
                                        }
                                        goBlock = "Main";
                                    } else if (codes.size() > 3 && INFO_STATUS.equalsIgnoreCase(codes.get(1))) {
                                        this.hideMessagesPanel();
                                        this.showMessagesPanel(INF_ICON, codes.get(2));
                                        goBlock = "NewFpl";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        _logger.fine("goBlock value: " + goBlock);
        _logger.info("processF4Key() End");
        return goBlock;
    }

    private void callDoCommit() {
        ADFUtils.findOperation("callDoCommit").execute();
    }

    private String callVPutawayLocation() {
        _logger.info("callVPutawayLocation() Start");
        String locationId = getHhPutawayInventorySMultiSkuViewCurrentRow().getConfirmLoc();
        String multiSkuLocationId = getHhPutawayInventorySMultiSkuViewCurrentRow().getLocationId();
        if (locationId == null || !locationId.equals(multiSkuLocationId)  ) {
            this.showMessagesPanel(WARN,
                                   this.getMessage("LOC_MISMATCH", WARN, this.getFacilityIdAttrValue(),
                                                   this.getLangCodeAttrValue()));
            _logger.info("callVPutawayLocation() End");
            return NO;
        }

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_V_PUTAWAY_LOCATION_OPER);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                if (codes != null) {
                    if (ERROR.equalsIgnoreCase(codes.get(0))) {
                        this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                        this.showMessagesPanel(ERROR, codes.get(1));
                        _logger.info("callVPutawayLocation() End");
                        return NO;
                    } else {
                        if (WARN.equalsIgnoreCase(codes.get(0))) {
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                            this.showMessagesPanel(WARN, codes.get(1));
                            _logger.info("callVPutawayLocation() End");
                            return NO;
                        } else {
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                            _logger.info("callVPutawayLocation() End");
                            return YES;
                        }
                    }
                }
            }
        }
        _logger.info("callVPutawayLocation() End");
        return NO;
    }


    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        _logger.fine("clientEvent value: " + clientEvent);
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                this.f6ClearActionListener(new ActionEvent(clientEvent.getComponent()));
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8MarkLink());
                actionEvent.queue();
            }
        }
        _logger.info("performKey() End");
    }

    public void onChangedConfirmQtyMultiSku(ValueChangeEvent vce) {
        _logger.info("onChangedConfirmQtyMultiSku() Start");
        _logger.fine("vce value: " + vce);
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        String newValue = (String) vce.getNewValue();
        if (newValue != null) {
            if (!newValue.matches("[0-9]+")) {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                this.showMessagesPanel(WARN,
                                       this.getMessage("QTY_MISMATCH", WARN, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                this.setErrorStyleClass(this.getConfirmQty(), true);
                this.selectTextOnUIComponent(this.getConfirmQty());
            } else {
                this.setErrorStyleClass(this.getConfirmQty(), false);
                BigDecimal newValueDec = new BigDecimal(newValue);
                ADFUtils.setBoundAttributeValue("ConfirmQty1", newValueDec);
                this.updateModel(vce);
                if (YES.equalsIgnoreCase(this.callVPutQty())) {
                    this.setErrorStyleClass(this.getConfirmQty(), false);

                } else {
                    ADFUtils.setBoundAttributeValue("confrimQtyString", null);
                    this.getConfirmQty().resetValue();
                    this.setErrorStyleClass(this.getConfirmQty(), true);
                    this.selectTextOnUIComponent(this.getConfirmQty());
                }
            }
        }
        _logger.info("onChangedConfirmQtyMultiSku() End");
    }


    private String callVPutQty() {
        _logger.info("callVPutQty() Start");
        String facilityId = this.getFacilityIdAttrValue();
        String langCode = this.getLangCodeAttrValue();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_V_PUT_QTY_OPER);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if (NO.equalsIgnoreCase(codeList.get(0))) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                    if (WARN.equalsIgnoreCase(codeList.get(1))) {
                        this.hideMessagesPanel();
                        this.showMessagesPanel(WARN,
                                               this.getMessage(codeList.get(2), codeList.get(1), facilityId, langCode));
                    }
                    _logger.info("callVPutQty() End");
                    return NO;
                } else {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.hideMessagesPanel();
                    _logger.info("callVPutQty() End");
                    return YES;
                }
            }
        }
        _logger.info("callVPutQty() End");
        return NO;
    }


    public void onChangedContainerIdMultiSku(ValueChangeEvent vce) {
        _logger.info("onChangedContainerIdMultiSku() Start");
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        this.updateModel(vce);
        if (vce.getNewValue() != null) {
            this.callVContainerId();
        }
        _logger.info("onChangedContainerIdMultiSku() End");
    }

    private List<String> callMarkLocation() {
        _logger.info("callMarkLocation() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_MARK_LOC_OPER);
        op.getParamsMap().put("locationId", this.getHhPutawayInventorySMultiSkuViewCurrentRow().getConfirmLoc());
        List<String> returnList = (List<String>) op.execute();
        if (op.getErrors().isEmpty()) {
            _logger.info("callMarkLocation() End");
            return returnList;
        }
        _logger.info("callMarkLocation() End");
        return null;

    }

    private OperationBinding callDisplayDirective() {
        _logger.info("callDisplayDirective() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("callDisplayPutDirective");
        op.execute();
        _logger.info("callDisplayDirective() End");
        return op;
    }

    private void callAdjustPflValues(String locationId, String itemId) {
        _logger.info("callAdjustPflValues() Start");
        _logger.fine("locationId value: " + locationId + "\n" + "itemId value: " + itemId);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("callAdjustPflValues");
        op.getParamsMap().put("locationId", locationId);
        op.getParamsMap().put("itemId", itemId);
        String err = (String) op.execute();
        if (err != null) {
            this.hideMessagesPanel();
            this.showMessagesPanel(ERROR, err);
        } else {
            this.hideMessagesPanel();
        }
        _logger.info("callAdjustPflValues() End");
    }

    private boolean callVContainerId() {
        _logger.info("callVContainerId() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_V_CONTAINER_ID_OPER);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(codeList.get(0))) {
                    _logger.info("Unable to execute 'CALL_V_CONTAINER_ID_OPER' operation!");
                    this.hideMessagesPanel();
                    this.setErrorStyleClass(this.getContainerId(), true);
                    this.showMessagesPanel(ERROR, codeList.get(1));
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                    return false;
                } else {
                    this.setErrorStyleClass(this.getContainerId(), false);
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.hideMessagesPanel();
                    return true;
                }
            }
        }

        _logger.info("callVContainerId() End");
        return true;
    }


    private void callTrExit() {
        _logger.info("callTrExit() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_EXIT_TR);
        oper.getParamsMap().put("containerId", this.getHhPutawayInventorySBeanPageFlowBean().getPutawayContainerId());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                this.hideMessagesPanel();
                this.clearFields();
                this.showMessagesPanel(ERROR, codes.get(0));
            }
            ADFUtils.invokeAction("Home");
        }
        _logger.info("callTrExit() End");
    }

    private OperationBinding callPupdatePendingQtys() {
        _logger.info("callPupdatePendingQtys() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_P_UPDATE_PENDING_QTYS_OPER);
        oper.execute();
        _logger.info("callPupdatePendingQtys() End");
        return oper;
    }

    private OperationBinding callPutawayMerchandise(String locationId, String containerId, String itemId,
                                                    String blockName) {
        _logger.info("callPutawayMerchandise() Start");
        _logger.info("locationId value: " + locationId + "\n" + "containerId value: " + containerId + "\n" +
                     "itemId value: " + itemId + "\n" + "blockName value: " + blockName);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("callPutawayMerchandise");
        op.getParamsMap().put("locationId", locationId);
        op.getParamsMap().put("containerId", containerId);
        op.getParamsMap().put("itemId", itemId);
        op.getParamsMap().put("blockName", blockName);
        op.execute();
        _logger.info("callPutawayMerchandise() End");
        return op;
    }

    private void clearFields() {
        _logger.info("clearFields() Start");
        this.hideMessagesPanel();
        removeErrorStyleClass();
        this.clearCurrentRow();
        this.getConfirmQty().resetValue();
        this.getConfirmLoction().resetValue();
        this.getContainerId().resetValue();
        this.getConfirmLoction().setDisabled(false);
        this.getContainerId().setDisabled(true);
        this.refreshContentOfUIComponent(this.getConfirmLoction());
        this.refreshContentOfUIComponent(this.getContainerId());
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow() {
        _logger.info("clearCurrentRow() Start");
        HhPutawayInventorySMultiSkuViewRowImpl currRow = this.getHhPutawayInventorySMultiSkuViewCurrentRow();
        currRow.setconfrimQtyString(null);
        currRow.setConfirmLoc(null);
        currRow.setContainerId(null);
        _logger.info("clearCurrentRow() End");
    }

    private HhPutawayInventorySBean getHhPutawayInventorySBeanPageFlowBean() {
        return ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN));

    }

    public void f6ClearActionListener(ActionEvent actionEvent) {
        this.clearFields();
        this.setFocusConfirmLocation();

    }

    private void removeErrorStyleClass() {
        _logger.info("removeErrorStyleClass() Start");
        this.removeErrorStyleToComponent(this.getConfirmQty());
        this.removeErrorStyleToComponent(this.getContainerId());
        this.removeErrorStyleToComponent(this.getConfirmLoction());
        this.refreshContentOfUIComponent(this.getConfirmLoction());
        this.refreshContentOfUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(this.getConfirmQty());
        this.getConfirmLocIcone().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getConfirmLocIcone());
        this.getContainerIdIcone().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getContainerIdIcone());
        _logger.info("removeErrorStyleClass() End");
    }

    private boolean validateConfrimLoc() {
        _logger.info("validateConfrimLoc() Start");
        if (!this.getConfirmLoction().isDisabled()) {
            if (this.getHhPutawayInventorySBeanPageFlowBean().getIsError() &&
                ADFUtils.getBoundAttributeValue("ConfirmLoc") != null) {
                this.getConfirmLocIcone().setName("error");
                this.refreshContentOfUIComponent(this.getConfirmLocIcone());
                this.selectTextOnUIComponent(this.getConfirmLoction());
                _logger.info("validateConfrimLoc() End");
                return false;
            }
            if (ADFUtils.getBoundAttributeValue("ConfirmLoc") == null) {
                this.setErrorStyleClass(this.getConfirmLoction(), true);
                this.showMessagesPanel(ERROR, this.getMessage("PARTIAL_ENTRY", null, null, null));
                _logger.info("validateConfrimLoc() End");
                return false;
            } else {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getConfirmLoction(), false);
            }
        }
        _logger.info("validateConfrimLoc() End");
        return true;

    }

    private boolean validateContainerId() {
        _logger.info("validateContainerId() Start");
        if (!this.getContainerId().isDisabled()) {
            if (this.getContainerIdAttrValue() != null) {
                this.onChangedContainerIdMultiSku(new ValueChangeEvent(this.getContainerId(), null,
                                                                       this.getContainerIdAttrValue()));
                return !this.getHhPutawayInventorySBeanPageFlowBean().getIsError();
            }
            if (this.getContainerIdAttrValue() == null) {
                _logger.info("Exception: containerIdAttrValue null! : REQUIRED_FIELD_ERROR ");
                this.setErrorStyleClass(this.getContainerId(), true);
                this.showMessagesPanel(ERROR, this.getMessage("PARTIAL_ENTRY", null, null, null));
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getContainerId(), false);
            }
        }
        _logger.info("validateContainerId() End");
        return true;
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);

        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("warning");
        } else if (INF_ICON.equals(messageType) || "M".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("info");
        } else if (SUCCESS.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("logo");

        } else {
            this.getErrorWarnInfoIcon().setName("error");
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel() End");
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);

        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("hideMessagesPanel() End");
    }


    private void setFocusContainerId() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MULTI_SKU.CONTAINER_ID");
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusConfirmLocation() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MULTI_SKU.CONFIRM_LOC");
        this.setFocusOnUIComponent(this.getConfirmLoction());
    }

    private void setFocusConfirmQty() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MULTI_SKU.CONFIRM_QTY");
        this.setFocusOnUIComponent(this.getConfirmQty());
    }

    private HhPutawayInventorySMainViewRowImpl getHhPutawayInventorySMainViewCurrentRow() {
        return (HhPutawayInventorySMainViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMainViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySWorkLocalViewRowImpl getHhPutawayInventorySWorkLocalViewCurrentRow() {
        return (HhPutawayInventorySWorkLocalViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLocalViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySMultiSkuViewRowImpl getHhPutawayInventorySMultiSkuViewCurrentRow() {
        return (HhPutawayInventorySMultiSkuViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMultiSkuViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySWorkLaborProdNewViewRowImpl getHhPutawayInventorySWorkLaborProdViewCurrentRow() {
        return (HhPutawayInventorySWorkLaborProdNewViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLaborProdNewViewIterator").getCurrentRow();
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);

    }

    private String getContainerIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);

    }

    private String getLangCodeAttrValue() {

        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setConfirmLoction(RichInputText confirmLoction) {
        this.confirmLoction = confirmLoction;
    }

    public RichInputText getConfirmLoction() {
        return confirmLoction;
    }

    public void setConfirmQty(RichInputText confirmQty) {
        this.confirmQty = confirmQty;
    }

    public RichInputText getConfirmQty() {
        return confirmQty;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setContainerQty(RichInputText containerQty) {
        this.containerQty = containerQty;
    }

    public RichInputText getContainerQty() {
        return containerQty;
    }

    public void setUnitQty(RichInputText unitQty) {
        this.unitQty = unitQty;
    }

    public RichInputText getUnitQty() {
        return unitQty;
    }

    public void setItemId(RichInputText itemId) {
        this.itemId = itemId;
    }

    public RichInputText getItemId() {
        return itemId;
    }

    public void setDescription(RichInputText description) {
        this.description = description;
    }

    public RichInputText getDescription() {
        return description;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }


    public void setConfirmLocIcone(RichIcon confirmLocIcone) {
        this.confirmLocIcone = confirmLocIcone;
    }

    public RichIcon getConfirmLocIcone() {
        return confirmLocIcone;
    }

    public void setConfirmQtyIcone(RichIcon confirmQtyIcone) {
        this.confirmQtyIcone = confirmQtyIcone;
    }

    public RichIcon getConfirmQtyIcone() {
        return confirmQtyIcone;
    }

    public void setContainerIdIcone(RichIcon containerIdIcone) {
        this.containerIdIcone = containerIdIcone;
    }

    public RichIcon getContainerIdIcone() {
        return containerIdIcone;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF5NextLocLink(RichLink f5NextLocLink) {
        this.f5NextLocLink = f5NextLocLink;
    }

    public RichLink getF5NextLocLink() {
        return f5NextLocLink;
    }

    public void setF8MarkLink(RichLink f8MarkLink) {
        this.f8MarkLink = f8MarkLink;
    }

    public RichLink getF8MarkLink() {
        return f8MarkLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF6ClearLink(RichLink f6ClearLink) {
        this.f6ClearLink = f6ClearLink;
    }

    public RichLink getF6ClearLink() {
        return f6ClearLink;
    }
}
