package com.ross.rdm.hotelpicking.hhputawayinventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMultiSkuViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySNewFplViewRowImpl;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;
// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page3.jspx
// ---
// ---------------------------------------------------------------------
public class Page3Backing extends RDMHotelPickingBackingBean {

    private RichPopup confirmationPopup;
    private RichOutputText dialogOutputText;
    private final static String CALL_ASSIGN_NEW_FPL_OPER = "callAssignNewFpl";
    private final static String CALL_EXIT_TR = "callTrExit";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String HH_PUTAWAY_PAGE_FLOW_BEAN = "HhPutawayInventorySBean";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private RichInputText itemId;
    private RichInputText itemDesc;
    private RichInputText locationId;
    private RichInputText capacity;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon capacityIcon;
    private RichIcon locationIcon;
    private RichLink f3ExitLink;
    private RichLink f6ClearLink;
    private RichLink f4DoneLink;
    private RichDialog exitDialog;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class);


    public Page3Backing() {

    }


    private boolean checkCapicityFieldRequired() {
        _logger.info("checkCapacityFieldRequired() Start");

        if (!this.getCapacity().isDisabled()) {
            if (ADFUtils.getBoundAttributeValue("Capacity") != null &&
                "NEW_FPL.CAPACITY".equals(this.getHhPutawayInventorySBeanPageFlowBean().getIsErrorOn())) {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.CAPACITY");
                this.refreshContentOfUIComponent(this.getCapacity());
                this.getCapacityIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getCapacityIcon());
                this.addErrorStyleToComponent(this.getCapacity());
                this.selectTextOnUIComponent(this.getCapacity());
                _logger.info("checkCapacityFieldRequired() End");
                return false;
            }
        }
        _logger.info("checkCapacityFieldRequired() End");
        return true;
    }

    private boolean checkLocationFieldRequired() {
        _logger.info("checkLocationFieldRequired() Start");
        if (!this.getLocationId().isDisabled()) {
            if (ADFUtils.getBoundAttributeValue("LocationId1") != null &&
                this.getHhPutawayInventorySBeanPageFlowBean().getIsError()) {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
                this.refreshContentOfUIComponent(this.getLocationId());
                this.getLocationIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getLocationIcon());
                this.addErrorStyleToComponent(this.getLocationId());
                this.selectTextOnUIComponent(this.getLocationId());
                _logger.info("checkLocationFieldRequired() End");
                return false;
            } else {

                if (ADFUtils.getBoundAttributeValue("LocationId1") == null) {
                    this.addErrorStyleToComponent(this.getLocationId());
                    this.getLocationIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getLocationIcon());
                    this.showMessagesPanel(ERROR, this.getMessage("PARTIAL_ENTRY", null, null, null));
                    _logger.info("checkLocationFieldRequired() End");
                    return false;
                } else {
                    this.getLocationIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getLocationIcon());
                    this.removeErrorStyleToComponent(this.getLocationId());
                }
            }

        }
        _logger.info("checkLocationFieldRequired() End");
        return true;
    }

    private void setFocusLocationId() {
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
        this.setFocusOnUIComponent(this.getLocationId());
    }

    private void setFocusCapacity() {
        this.getCapacity().setDisabled(false);
        this.refreshContentOfUIComponent(this.getCapacity());
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.CAPACITY");
        this.setFocusOnUIComponent(this.getCapacity());

    }


    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        _logger.fine("Key value: " + key);
        if ("F3".equalsIgnoreCase(key)) {
            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("EXIT_POPUP");
            this.getDialogOutputText().setValue(this.getMessage("CONFIRM_EXIT", "C", this.getFacilityIdAttrValue(),
                                                                this.getLangCodeAttrValue()));
            this.getConfirmationPopup().show(new RichPopup.PopupHints());

        } else {
            if ("F4".equalsIgnoreCase(key)) {
                if (this.callAssignNewFpl()) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.getLocationId().setStyleClass("");
                    this.getCapacity().setStyleClass("");
                    this.getCapacityIcon().setName("");
                    this.getLocationIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getCapacityIcon());
                    this.refreshContentOfUIComponent(this.getLocationIcon());
                    this.removeErrorStyleToComponent(this.getLocationId());
                    this.removeErrorStyleToComponent(this.getCapacity());
                    this.getHhPutawayInventorySMultiSkuViewCurrentRow().setLocationId(this.getHhPutawayInventorySNewFplViewCurrentRow().getLocationId());
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MULTI_SKU.CONFIRM_LOC");
                    ADFUtils.invokeAction("MultiSku");

                } else {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                    this.setLocationError();

                }

            }

        }
        _logger.info("trKeyIn() End");
    }


    public void confirmDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmDialogListener() Start");
        // Add event code here...
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            this.callTrExit();
        } else {
            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
            this.selectTextOnUIComponent(this.getLocationId());

        }
        _logger.info("confirmDialogListener() End");
    }

    private void callTrExit() {
        _logger.info("callTrExit() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_EXIT_TR);
        oper.getParamsMap().put("containerId", this.getHhPutawayInventorySBeanPageFlowBean().getPutawayContainerId());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                this.hideMessagesPanel();
                this.clearFields();
                this.showMessagesPanel(ERROR, codes.get(0));
            }
            ADFUtils.invokeAction("Home");
        }
        _logger.info("callTrExit() End");
    }

    private boolean callAssignNewFpl() {
        _logger.info("callAssignNewFpl() Start");

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_ASSIGN_NEW_FPL_OPER);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codes = (List<String>) oper.getResult();
                this.hideMessagesPanel();
                if (WARN.equalsIgnoreCase(codes.get(0))) {
                    this.showMessagesPanel(WARN, codes.get(1));
                    _logger.info("callAssignNewFpl() End");
                    return false;
                } else {
                    _logger.info("callAssignNewFpl() End");
                    return true;
                }
            }
        }
        _logger.info("callAssignNewFpl() End");
        return true;
    }

    public void locationIdValueChangeListener(ValueChangeEvent vce) {
        _logger.info("locationIdValueChangeListener() Start");
        _logger.fine("vce value: " + vce);
        this.updateModel(vce);
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        this.setErrorStyleClass(this.getLocationId(), false);
        this.hideMessagesPanel();
        this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
        if (vce.getNewValue() != null && !((String) vce.getNewValue()).isEmpty()) {
            this.getHhPutawayInventorySNewFplViewCurrentRow().setLocationId(((String) vce.getNewValue()).toUpperCase());

        }
        _logger.info("locationIdValueChangeListener() End");
    }

    public void onChangedCapacityNewFpl(ValueChangeEvent vce) {
        _logger.info("onChangedCapacityNewFpl() Start");
        _logger.fine("vce value: " + vce);
        if (this.getHhPutawayInventorySBeanPageFlowBean() == null)
            return;
        this.setErrorStyleClass(this.getCapacity(), false);
        String newValue = (String) vce.getNewValue();
        if (newValue != null) {
            if (!newValue.matches("[0-9]+")) {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(true);
                this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("NEW_FPL.CAPACITY");
                this.showMessagesPanel(ERROR,
                                       this.getMessage("MUST_BE_NUMERIC", ERROR, this.getFacilityIdAttrValue(),
                                                       this.getLangCodeAttrValue()));
                this.setErrorStyleClass(this.getCapacity(), true);
                this.selectTextOnUIComponent(this.getCapacity());
            } else {
                this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                BigDecimal newValueDec = new BigDecimal(newValue);
                ADFUtils.setBoundAttributeValue("Capacity", newValueDec);
                this.updateModel(vce);
                this.setErrorStyleClass(this.getCapacity(), false);
                this.hideMessagesPanel();
                this.selectTextOnUIComponent(this.getCapacity());
                this.getHhPutawayInventorySBeanPageFlowBean().setIsErrorOn("");
            }
        }
        _logger.info("onChangedCapacityNewFpl() End");

    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "/n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("lnf")) {
                this.getLocationIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getLocationIcon());
            } else if (field.getId().equalsIgnoreCase("cnf")) {
                this.getCapacityIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getCapacityIcon());
            }

        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("lnf")) {
                this.getLocationIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getLocationIcon());
            } else if (field.getId().equalsIgnoreCase("cnf")) {
                this.getCapacityIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getCapacityIcon());
            }
        }
        _logger.info("onChangedCapacityNewFpl() End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                this.f6ClearActionListener(new ActionEvent(clientEvent.getComponent()));
            } else if (ENTER_KEY_CODE.equals(keyPressed)) {
                String loc = (String) this.getLocationId().getValue();
                if ("lnf".equalsIgnoreCase(clientEvent.getComponent().getId()) && loc != null &&
                    StringUtils.isNotEmpty(loc)) {
                    this.getHhPutawayInventorySBeanPageFlowBean().setIsError(false);
                    this.setErrorStyleClass(this.getLocationId(), false);
                    this.hideMessagesPanel();
                    this.setFocusCapacity();
                }
            }
        }
        _logger.info("performKey() End");
    }

    private void setLocationError() {
        _logger.info("setLocationError() Start");
        this.getLocationIcon().setName(ERR_ICON);
        this.getCapacity().setDisabled(true);
        this.refreshContentOfUIComponent(this.getCapacity());
        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("NEW_FPL.LOCATION_ID");
        this.addErrorStyleToComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getLocationIcon());
        this.selectTextOnUIComponent(this.getLocationId());
        _logger.info("setLocationError() End");
    }

    private HhPutawayInventorySBean getHhPutawayInventorySBeanPageFlowBean() {
        return ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN));
    }


    private void clearFields() {
        _logger.info("clearFields() Start");
        this.hideMessagesPanel();
        this.getHhPutawayInventorySNewFplViewCurrentRow().setCapacity(null);
        this.getHhPutawayInventorySNewFplViewCurrentRow().setLocationId(null);
        this.getLocationId().resetValue();
        this.getCapacity().resetValue();
        this.getLocationIcon().setName(REQ_ICON);
        this.getCapacityIcon().setName("");
        this.setErrorStyleClass(this.getLocationId(), false);
        this.setErrorStyleClass(this.getCapacity(), false);
        this.getCapacity().setDisabled(true);
        this.refreshContentOfUIComponent(this.getCapacity());
        this.refreshContentOfUIComponent(this.getF4DoneLink());
        this.refreshContentOfUIComponent(this.getF6ClearLink());
        this.refreshContentOfUIComponent(this.getLocationId());
        this.setFocusLocationId();
        _logger.info("setLocationError() End");
    }


    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel() End");
    }

    public void yesConfrimExitLinkActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.getConfirmationPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }

    public void noConfrimExitLinkActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.getConfirmationPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
    }

    public void performFkeyExitPopup(ClientEvent clientEvent) {
        _logger.info("performKkeyExitPopup() Start");
        // Add event code here...
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
            }
        }
        _logger.info("performKkeyExitPopup() End");
    }

    private void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private HhPutawayInventorySMultiSkuViewRowImpl getHhPutawayInventorySMultiSkuViewCurrentRow() {
        return (HhPutawayInventorySMultiSkuViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMultiSkuViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySNewFplViewRowImpl getHhPutawayInventorySNewFplViewCurrentRow() {
        return (HhPutawayInventorySNewFplViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySNewFplViewIterator").getCurrentRow();
    }

    public void f3ExitActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.trKeyIn("F3");
    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        // Add event code here...
    
        
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_NEW_FPL_F4,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
            if (!this.getHhPutawayInventorySBeanPageFlowBean().getIsError() && this.checkCapicityFieldRequired() &&
                this.checkLocationFieldRequired())
                this.trKeyIn("F4");
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
        }
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);

    }

    private String getLangCodeAttrValue() {

        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setItemId(RichInputText itemId) {
        this.itemId = itemId;
    }

    public RichInputText getItemId() {
        return itemId;
    }

    public void setItemDesc(RichInputText itemDesc) {
        this.itemDesc = itemDesc;
    }

    public RichInputText getItemDesc() {
        return itemDesc;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setCapacity(RichInputText capacity) {
        this.capacity = capacity;
    }

    public RichInputText getCapacity() {
        return capacity;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }


    public void setCapacityIcon(RichIcon capacityIcon) {
        this.capacityIcon = capacityIcon;
    }

    public RichIcon getCapacityIcon() {
        return capacityIcon;
    }

    public void setLocationIcon(RichIcon locationIcon) {
        this.locationIcon = locationIcon;
    }

    public RichIcon getLocationIcon() {
        return locationIcon;
    }

    public void f6ClearActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.clearFields();
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF6ClearLink(RichLink f6ClearLink) {
        this.f6ClearLink = f6ClearLink;
    }

    public RichLink getF6ClearLink() {
        return f6ClearLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setExitDialog(RichDialog exitDialog) {
        this.exitDialog = exitDialog;
    }

    public RichDialog getExitDialog() {
        return exitDialog;
    }
}
