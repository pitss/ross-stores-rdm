package com.ross.rdm.hotelpicking.hhputawayinventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySReasonViewRowImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLocalViewRowImpl;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Reason.jspx
// ---
// ---------------------------------------------------------------------
public class ReasonBacking extends RDMHotelPickingBackingBean {
    private RichOutputText warningOutputText;
    private RichOutputText errorOutputText;
    private RichOutputText infoOutputText;
    private RichInputText code;
    private RichPanelGroupLayout allMessagesPanel;
    private RichSelectOneChoice codeSelectOneChoice;
    private RichLink f4DoneLink;
    private RichLink f3ExitLink;
    private RichIcon reasonIcon;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(ReasonBacking.class);
    private final static String ERROR_STATUS = "ERROR";
    private final static String NO_ERROR_STATUS = "NO_ERROR";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String CALL_V_REASON_CODE_OPER = "callVReasonCode2";
    private final static String CALL_PROCESS_PUTAWAY_EXCEPTION_OPER = "callProcessPutawayExcpetion";
    private final static String CALL_P_UPDATE_PENDING_QTYS_OPER = "callPupdatePendingQtys";
    private final static String CALL_FIND_LOCATIONS_OPER = "callFindLocations";
    private final static String HH_PUTAWAY_PAGE_FLOW_BEAN = "HhPutawayInventorySBean";
    private RichLink f6ListLink;

    public ReasonBacking() {

    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        _logger.fine("Key value: " + key);
        String goBlock = null;
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            this.getHhPutawayInventorySWorkLocalViewCurrentRow();
        if ("F3".equalsIgnoreCase(key)) {
            this.clearFields();
            hhPutawayInventorySWorkLocalViewRow.setCallingItem(null);
            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.TOLOCATION_ID_R");
            ADFUtils.invokeAction("Main");
        } else {
            if ("F4".equalsIgnoreCase(key)) {
                if (Boolean.TRUE.equals(this.callVReasonCode2())) {
                    HhPutawayInventorySReasonViewRowImpl hhPutawayInventorySReasonViewRow =
                        this.gethhPutawayInventorySReasonViewCurrentRow();
                    if ("F5".equalsIgnoreCase(hhPutawayInventorySWorkLocalViewRow.getCallingItem())) {
                        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
                            this.getHhPutawayInventorySMainViewCurrentRow();

                        if (null != hhPutawayInventorySMainViewRow.getSuggestedLocId()) {
                            this.callProcessPutawayException(hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                                             hhPutawayInventorySReasonViewRow.getCode());
                            this.callPupdatePendingQtys();

                        } else {
                            this.callProcessPutawayException(hhPutawayInventorySMainViewRow.getToLocationId(),
                                                             hhPutawayInventorySReasonViewRow.getCode());

                        }
                        hhPutawayInventorySMainViewRow.setSuggestedLocId(null);
                        List<String> returnList = this.callFindLocation();
                        if (NO_ERROR_STATUS.equalsIgnoreCase(returnList.get(0))) {
                            if (null == hhPutawayInventorySMainViewRow.getSuggestedLocId()) {
                                this.hideMessagesPanel();
                                this.showMessagesPanel(INFO,
                                                       this.getMessage("NO_EMPTY_LOC", INFO,
                                                                       this.getFacilityIdAttrValue(),
                                                                       this.getLangCodeAttrValue()));
                            }
                            this.clearFields();
                            goBlock = "Main";
                            this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.TOLOCATION_ID_R");
                        } else {
                            if (ERROR_STATUS.equalsIgnoreCase(returnList.get(0))) {
                                this.hideMessagesPanel();
                                this.showMessagesPanel(ERROR, returnList.get(1));
                            }
                        }
                    } else {
                        hhPutawayInventorySWorkLocalViewRow.setReasonCode(hhPutawayInventorySReasonViewRow.getCode());
                        this.clearFields();
                        goBlock = "Main";
                        this.getHhPutawayInventorySBeanPageFlowBean().setIsFocusOn("MAIN.CONFIRM_CASE_QTY_R");
                    }
                    hhPutawayInventorySWorkLocalViewRow.setCallingItem(null);
                }
            } else {
                if ("F6".equalsIgnoreCase(key)) {
                    this.getReasonIcon().setName(REQ_ICON);
                    this.hideMessagesPanel();
                    this.removeErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
                    this.refreshContentOfUIComponent(this.getReasonIcon());
                    String clientId = this.getCodeSelectOneChoice().getId();
                    String script = "openListOfValues(custFind('" + clientId + "'));";
                    this.writeJavaScriptToClient(script.toString());
                    refreshContentOfUIComponent(this.getCodeSelectOneChoice());
                }
            }
        }
        if (null != goBlock && !goBlock.isEmpty())
            ADFUtils.invokeAction(goBlock);
        _logger.info("trKeyIn() End");

    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_REASON_F4,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
                if (this.validateReason()) {
                    this.trKeyIn("F4");
                }
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
        }
        
        
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        String clientId = this.getCodeSelectOneChoice().getClientId(FacesContext.getCurrentInstance());
        String script = "setReasonInitialFocus('" + clientId + "');";
        this.writeJavaScriptToClient(script.toString());
        refreshContentOfUIComponent(this.getCodeSelectOneChoice());
        _logger.info("onRegionLoad End");
    }

    private boolean validateReason() {
        _logger.info("validateReason() Start");
        if (((String) ADFUtils.getBoundAttributeValue("CodeLOV") == null)) {
            this.addErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
            this.getReasonIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getReasonIcon());
            this.showMessagesPanel(ERROR, "Invalid reason code.");
            _logger.info("validateReason() End");
            return false;
        }
        this.getReasonIcon().setName(REQ_ICON);
        this.removeErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
        this.refreshContentOfUIComponent(this.getReasonIcon());
        _logger.info("validateReason() End");
        return true;
    }

    public void f3ExitActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f6ListActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_putaway_inventory_s_REASON_F6,RoleBasedAccessConstants.FORM_NAME_hh_putaway_inventory_s)){
                    this.trKeyIn("F6");
        }else{
        this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", "", this.getFacilityIdAttrValue(),this.getLangCodeAttrValue()));
        }
       
    }

    private void clearFields() {
        this.getReasonIcon().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getReasonIcon());
        this.clearCurrentRow();
    }

    private void clearCurrentRow() {
        _logger.info("clearCurrentRow() Start");
        HhPutawayInventorySReasonViewRowImpl rowIpl = this.gethhPutawayInventorySReasonViewCurrentRow();
        rowIpl.setCode(null);
        rowIpl.setCodeDesc(null);
        rowIpl.setCodeLOV(null);
        rowIpl.setCodeCount(null);
        _logger.info("clearCurrentRow() End");
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        _logger.info("showMessagesPanel() End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6ListLink());
                actionEvent.queue();
            }
        }
        _logger.info("performKey() End");
    }

    private void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }


    public void codeListDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            this.gethhPutawayInventorySReasonViewCurrentRow().setCode(this.gethhPutawayInventorySReasonViewCurrentRow().getCodeLOV());
            this.refreshContentOfUIComponent(this.getCode());
        }
    }

    private OperationBinding callPupdatePendingQtys() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_P_UPDATE_PENDING_QTYS_OPER);
        oper.execute();
        return oper;
    }

    public void callProcessPutawayException(String locationId, String code) {
        _logger.info("callProcessPutawayException() Start");
        _logger.fine("Location ID value: " + locationId + "\n" + "code value: " + code);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_PROCESS_PUTAWAY_EXCEPTION_OPER);
        op.getParamsMap().put("locationId", locationId);
        op.getParamsMap().put("code", code);
        List<String> codes = (List<String>) op.execute();
        if (codes != null) {
            this.showMessagesPanel(ERROR, (codes.get(1)));
        } else {
            this.hideMessagesPanel();
        }
        _logger.info("callProcessPutawayException() End");
    }

    private List<String> callFindLocation() {
        _logger.info("callFindLocation() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_FIND_LOCATIONS_OPER);
        op.getParamsMap().put("pmAllowIntrlvg", this.getHhPutawayInventorySBeanPageFlowBean().getPmAllowIntrlvg());
        op.getParamsMap().put("pmStartLoc", this.getHhPutawayInventorySBeanPageFlowBean().getPmStartLocation());
        List<String> returnList = (List<String>) op.execute();
        if (op.getErrors().isEmpty()) {
            if (!returnList.isEmpty())
                _logger.info("callFindLocation() End");
            return returnList;
        }
        _logger.info("callFindLocation() End");
        return null;
    }

    private Boolean callVReasonCode2() {
        _logger.info("callVReasonCode2() Start");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation(CALL_V_REASON_CODE_OPER);
        op.getParamsMap().put("code", this.gethhPutawayInventorySReasonViewCurrentRow().getCode());
        op.execute();
        if (op.getErrors().isEmpty()) {
            if (null != op.getResult()) {
                List<String> codeList = (List<String>) op.getResult();
                if (ERROR.equalsIgnoreCase(codeList.get(0))) {
                    this.hideMessagesPanel();
                    this.showMessagesPanel(ERROR, codeList.get(1));
                    _logger.info("callVReasonCode2() End");
                    return Boolean.FALSE;
                }
            }
        }
        this.hideMessagesPanel();
        _logger.info("callVReasonCode2() End");
        return Boolean.TRUE;
    }

    public void onBLurServerListener(ClientEvent clientEvent) {
        if (null != clientEvent) {
            this.setFocusOnUIComponent((RichInputText) clientEvent.getComponent());
        }
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);

    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void reasonCodeSocValueChangeListener(ValueChangeEvent valueChangeEvent) {
        this.updateModel(valueChangeEvent);
        if (valueChangeEvent.getNewValue() != null) {
            ADFUtils.setBoundAttributeValue("Code1", valueChangeEvent.getNewValue());
        }
    }

    private HhPutawayInventorySBean getHhPutawayInventorySBeanPageFlowBean() {
        return ((HhPutawayInventorySBean) this.getPageFlowBean(HH_PUTAWAY_PAGE_FLOW_BEAN));

    }

    private HhPutawayInventorySReasonViewRowImpl gethhPutawayInventorySReasonViewCurrentRow() {
        return (HhPutawayInventorySReasonViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySReasonViewIterator").getCurrentRow();
    }

    private HhPutawayInventorySWorkLocalViewRowImpl getHhPutawayInventorySWorkLocalViewCurrentRow() {
        return (HhPutawayInventorySWorkLocalViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySWorkLocalViewIterator").getCurrentRow();

    }

    private HhPutawayInventorySMainViewRowImpl getHhPutawayInventorySMainViewCurrentRow() {
        return (HhPutawayInventorySMainViewRowImpl) ADFUtils.findIterator("HhPutawayInventorySMainViewIterator").getCurrentRow();

    }

    public void setWarningOutputText(RichOutputText warningOutputText) {
        this.warningOutputText = warningOutputText;
    }

    public RichOutputText getWarningOutputText() {
        return warningOutputText;
    }

    public void setErrorOutputText(RichOutputText errorOutputText) {
        this.errorOutputText = errorOutputText;
    }

    public RichOutputText getErrorOutputText() {
        return errorOutputText;
    }

    public void setInfoOutputText(RichOutputText infoOutputText) {
        this.infoOutputText = infoOutputText;
    }

    public RichOutputText getInfoOutputText() {
        return infoOutputText;
    }

    public void setCode(RichInputText code) {
        this.code = code;
    }

    public RichInputText getCode() {
        return code;
    }

    public void setCodeSelectOneChoice(RichSelectOneChoice codeSelectOneChoice) {
        this.codeSelectOneChoice = codeSelectOneChoice;
    }

    public RichSelectOneChoice getCodeSelectOneChoice() {
        return codeSelectOneChoice;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setCurrentRowWithKeyVal(ClientEvent clientEvent) {
        if (clientEvent != null) {
            String subValue = (String) clientEvent.getParameters().get("submittedValue");
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("setCurrentRowWithKeyValue");
            op.getParamsMap().put("rowKey", subValue);
            op.execute();
            if (op.getErrors().isEmpty()) {
                ADFUtils.setBoundAttributeValue("Code1", subValue);
            }

        }
    }


    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }


    public void setReasonIcon(RichIcon reasonIcon) {
        this.reasonIcon = reasonIcon;
    }

    public RichIcon getReasonIcon() {
        return reasonIcon;
    }

    public void setF6ListLink(RichLink f6ListLink) {
        this.f6ListLink = f6ListLink;
    }

    public RichLink getF6ListLink() {
        return f6ListLink;
    }
}
