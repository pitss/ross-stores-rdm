package com.ross.rdm.hotelpicking.hhnopickpacks.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSNoPickPackViewRowImpl;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSWorkViewRowImpl;

import java.io.IOException;
import java.io.Serializable;

import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhNoPickPackSBean implements Serializable {
    @SuppressWarnings("compatibility:-2725563129662306675")
    private static final long serialVersionUID = -7552458233691005683L;
    private String pmAllowIntrlvg; // Item/Parameter hh_no_pick_pack_s.PM_ALLOW_INTRLVG
    private String cPickType; // Item/Parameter hh_no_pick_pack_s.C_PICK_TYPE
    private String cpPickType; // Item/Parameter hh_no_pick_pack_s.CP_PICK_TYPE
    private String bpPickType; // Item/Parameter hh_no_pick_pack_s.BP_PICK_TYPE
    private String maxUsers;
    private String isFocusOn;
    private boolean isError;
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhNoPickPackSBean.class);


    public void setIsError(boolean isError) {
        this.isError = isError;
    }

    public boolean isIsError() {
        return isError;
    }


    public void initTaskFlow() {
        this.createInsert(); 
        this.initGlobalesVariables();
        this.initHhNoPickPackWorkLaborProdVariables();
        this.initHhNoPickPackWorkVariables();
        this.initParams();
        this.callSetAhlInfo();
        this.setIsError(false);
        this.setIsFocusOn("waveNbrNoPickPack");

    }
    
    private HhNoPickPackSNoPickPackViewRowImpl getHhNoPickPackSNoPickPackViewRow() {
        return (HhNoPickPackSNoPickPackViewRowImpl) ADFUtils.findIterator("HhNoPickPackSNoPickPackViewIterator").getCurrentRow();

    }
    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setMaxUsers(String maxUsers) {
        this.maxUsers = maxUsers;
    }

    public String getMaxUsers() {
        return maxUsers;
    }

    public void callCheckMaxUsers() {
        _logger.info("callCheckMaxUsers() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckMaxUsers");
        List<String> maxUsersCheck = (List<String>) oper.execute();
        if (oper.getErrors().isEmpty())
            if (maxUsersCheck != null) {
                this.setMaxUsers("N");
            }
        _logger.info("callCheckMaxUsers() End");

    }

    public void setPmAllowIntrlvg(String pmAllowIntrlvg) {
        this.pmAllowIntrlvg = pmAllowIntrlvg;
    }

    public String getPmAllowIntrlvg() {
        return pmAllowIntrlvg;
    }

    public void setCPickType(String cPickType) {
        this.cPickType = cPickType;
    }

    public String getCPickType() {
        return cPickType;
    }

    public void setCpPickType(String cpPickType) {
        this.cpPickType = cpPickType;
    }

    public String getCpPickType() {
        return cpPickType;
    }

    public void setBpPickType(String bpPickType) {
        this.bpPickType = bpPickType;
    }

    public String getBpPickType() {
        return bpPickType;
    }

    private void createInsert() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsert");
        oper.execute();
    }

    private void initGlobalesVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesforNoPickPack");
        oper.execute();
    }

    private void initHhNoPickPackWorkLaborProdVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhNoPickPackWorkLaborProdVariables");
        oper.execute();
    }

    private void initHhNoPickPackWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhNoPickPackWorkVariables");
        oper.execute();
    }

    private void initParams() {
        _logger.info("initParams() Start");
        GlobalVariablesViewRowImpl gVViewImplRowImpl = this.getGlobalVariablesViewCurrentRow();
        this.setPmAllowIntrlvg(gVViewImplRowImpl.getGlobalAllowIntrlvg());
        this.setBpPickType(gVViewImplRowImpl.getGlobalBPPickType());
        this.setCpPickType(gVViewImplRowImpl.getGlobalCPPickType());
        this.setCPickType(gVViewImplRowImpl.getGlobalCPickType());
        _logger.info("initParams() End");
    }

    public GlobalVariablesViewRowImpl getGlobalVariablesViewCurrentRow() {
        return (GlobalVariablesViewRowImpl) (ADFUtils.findIterator("GlobalVariablesViewIterator")).getCurrentRow();

    }
    private void callSetAhlInfo() {
        _logger.info("callSetAhlInfo() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callSetAhlInfo");
        oper.getParamsMap().put("facilityId",this.getHhNoPickPackSNoPickPackWorkViewRow().getFacilityId());
        oper.getParamsMap().put("formName", "HH_NO_PICK_PACK_S");
        oper.execute();
        _logger.info("callSetAhlInfo() End");
    }

    private HhNoPickPackSWorkViewRowImpl getHhNoPickPackSNoPickPackWorkViewRow() {
        return (HhNoPickPackSWorkViewRowImpl) (ADFUtils.findIterator("HhNoPickPackSWorkViewIterator")).getCurrentRow();
    }

    public String onLogout() {
        _logger.info("onLogout() Start");
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        String url = ectx.getRequestContextPath() +
                 "/adfAuthentication?logout=true&end_url=/faces/login.jsf";    
        try {
            ectx.redirect(url);
        } catch (IOException e) {
            _logger.severe("Failure to logout. IOException: " + e.getMessage()); 
        }
        fctx.responseComplete();
        _logger.info("onLogout() End");
        return null;
    }
}
