package com.ross.rdm.hotelpicking.hhnopickpacks.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSNoPickPackViewRowImpl;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSWorkViewRowImpl;

import java.util.List;
import java.util.regex.Pattern;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichMessage;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.Row;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------

public class Page1Backing extends RDMHotelPickingBackingBean {

    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String WAVE_NUMBER_ATTR = "WaveNbr1";
    private final static String PICK_TYPE_ATTR = "PickType1";
    private final static String PICK_ATTR = "Pick";
    private final static String CALL_WAVE_NUMBER = "getWaveNumber";
    private final static String CALL_START_PICK_LOC = "getStartPicLoc";
    private final static String GET_WAVE_STATUS = "getWaveStatus";
    private final static String GET_WAVE_TYPE = "gWaveType";
    private final static String GET_USER_PRIVILIGE = "checkUserPrivilige";
    private final static String CALL_LOCSCP_BYWH = "getloCScpByWh";
    private final static String CALL_PICK_TYPE = "getCallPickType";
    private final static String GET_WH_ID = "getWareHouseId";
    private final static String G_SCP_WH = "getGscpWh";
    private final static String START_PIC_LOC = "StartPickLoc";
    private final static String NO_PICK_PACK_ITERATOR = "HhNoPickPackSNoPickPackViewIterator";
    private final static String ERROR_STYLE_CLASS = "p_AFError";
    private RichMessage infoMessage;
    private RichMessage warningMessage;
    private RichDialog confirmDialog;
    private RichDialog bulkPickScreenDialog;
    private RichMessage errorAfMessage;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichOutputText bulkPickScreenDialogOutputText;
    private RichMessage confirmationMessage;
    private RichPanelGroupLayout errorPanel;
    private RichPanelFormLayout noPickPackFormLayout;
    private RichInputText waveNumber;
    private RichInputText pick;
    private RichOutputText warningOutputText;
    private RichOutputText errorOutputText;
    private RichOutputText infoOutputText;
    private RichIcon iconErrorMessage;
    private RichInputText startPicLocation;
    private String pic;
    private RichInputText pickType;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichIcon waveNumberIcon;
    private RichIcon startPicLocIcon;
    private RichIcon picTypeIcon;
    private RichIcon pickIcon;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    public Page1Backing() {

    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorOutputText().setValue(null);
        this.getIconErrorMessage().setName(null);
        //this.getErrorPanel().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
        this.refreshContentOfUIComponent(this.getErrorOutputText());
        this.refreshContentOfUIComponent(this.getIconErrorMessage());
        _logger.info("hideMessagesPanel() End");

    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);
        // CLEAR PREVIOUS MESSAGES
        this.getErrorOutputText().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getErrorPanel().setVisible(false);
        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorOutputText().setValue(msg);
        this.getErrorPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
        _logger.info("showMessagesPanel() End");
    }

    public void onChangedWaveNbrNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedWaveNbrNoPickPack() Start");
        _logger.fine("vce value: " + vce);
        if (!this.isCurrentTaskFlowActive())
            return;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String waveNumber = (String) vce.getNewValue();
            this.validateWaveNumber(waveNumber);
        }
        _logger.info("onChangedWaveNbrNoPickPack() End");
    }

    public void changedStartPickLocNoPickPack(ValueChangeEvent vce) {
        _logger.info("changedStartPickLocNoPickPack() Start");
        _logger.fine("vce value: " + vce);
        if (!this.isCurrentTaskFlowActive())
            return;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String startPicLoc = ((String) vce.getNewValue()).toUpperCase();
            ADFUtils.setBoundAttributeValue("StartPickLoc", startPicLoc);
            this.validateStartLocation(startPicLoc);
        }
        _logger.info("changedStartPickLocNoPickPack() End");
    }

    public void onChangedPickTypeNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedPickTypeNoPickPack() Start");
        _logger.fine("vce value: " + vce);
        if (!this.isCurrentTaskFlowActive())
            return;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String picType = ((String) vce.getNewValue()).toUpperCase();
            ADFUtils.setBoundAttributeValue("PickType1", picType);
            this.validatePickType(picType);
        }
        _logger.info("onChangedPickTypeNoPickPack() End");
    }

    public void onChangedPickNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedPickNoPickPack() Start");
        _logger.fine("vce value: " + vce);
        if (!this.isCurrentTaskFlowActive())
            return;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String pick = ((String) vce.getNewValue()).toUpperCase();
            this.setPic(pick);
            ADFUtils.setBoundAttributeValue("Pick", pick);
            this.validatePick(pick);
        }
        _logger.info("onChangedPickNoPickPack() End");
    }

    private List<String> callWaveNumber(String waveNumber, String facilityId) {
        _logger.info("callWaveNumber() Start");
        _logger.fine("waveNumber value: " + waveNumber + "\n" + "facilityId value: " + facilityId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_WAVE_NUMBER);
        oper.getParamsMap().put("waveNumber", waveNumber);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
//        if (oper.getErrors().isEmpty()) {
//        }
        List<String> result = (List<String>) oper.getResult();
        _logger.fine("result value: " + result);
        _logger.info("callWaveNumber() End");
        return result;
    }

    private List<String> callPicLoc(String startPicLoc, String facilityId) {
        _logger.info("callPicLoc() Start");
        _logger.fine("startPicLoc value: " + startPicLoc + "\n" + "facilityId value: " + facilityId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_START_PICK_LOC);
        oper.getParamsMap().put("startPicLoc", startPicLoc);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
        List<String> result = null;
        if (oper.getErrors().isEmpty()) {
            result = (List<String>) oper.getResult();
        }
        _logger.fine("result value: " + result);
        _logger.info("callPicLoc() End");
        return result;
    }

    private List<String> callPicType(String picType, String facilityId, String waveNumber) {
        _logger.info("callPicType() Start");
        _logger.fine("picType value: " + picType + "\n" + "facilityId value: " + facilityId + "\n" +
                     "waveNumber value: " + waveNumber);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_PICK_TYPE);
        oper.getParamsMap().put("pickType", picType);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("waveNumber", waveNumber);
        oper.execute();
        List<String> result = null;
        if (oper.getErrors().isEmpty()) {
            result = (List<String>) oper.getResult();
        }
        _logger.info("result value: " + result);
        _logger.info("callPicType() End");
        return result;
    }

    private String getWareHouseId(String facilityId, String picLocationID) {
        _logger.info("getWareHouseId() Start");
        _logger.fine("facilityId value: " + facilityId + "\n" + "picLocationID value: " + picLocationID);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GET_WH_ID);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("picLocationID", picLocationID);
        _logger.info("getWareHouseId() End");
        return (String) oper.execute();
    }

    private String geScpwh(String facilityId, String whId) {
        _logger.info("geScpwh() Start");
        _logger.fine("facilityId value: " + facilityId + "\n" + "whId value: " + whId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(G_SCP_WH);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("whId", whId);
        _logger.info("geScpwh() End");
        return (String) oper.execute();
    }

    public String exitNoPickPack() {
        return this.trKeyIn("F3");
    }

    public String openNoPickPack(ActionEvent actionEvent) {
        _logger.info("openNoPickPack() Start");
        //        String waveNumber = (String) ADFUtils.getBoundAttributeValue(WAVE_NUMBER_ATTR);
//        String startPicLoc = "";
//        if (ADFUtils.getBoundAttributeValue(START_PIC_LOC) != null)
//            startPicLoc = ((String) ADFUtils.getBoundAttributeValue(START_PIC_LOC)).toUpperCase();
        
       
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_no_pick_pack_s_NO_PICK_PACK_F4,RoleBasedAccessConstants.FORM_NAME_hh_no_pick_pack_s)){
                  if (validateValidFields() && validateRequiredFields())
                      this.trKeyIn("F4");
                  }else{
                  showMessagesPanel(ERROR,this.getMessage( "NOT_ALLOWED", ERROR, (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR),(String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
              }
        _logger.info("openNoPickPack() End");
        return null; 
    }

    private String trKeyIn(String keyName) {
        _logger.info("trKeyIn() Start");
        _logger.fine("keyName value: " + keyName);
        HhNoPickPackSNoPickPackViewRowImpl hhPickPackNoPickPackViewRow = this.getHhNoPickPackSNoPickPackViewRow();
        if ("F3".equalsIgnoreCase(keyName)) {
            String action = null;
            action = this.logoutExitBTF();
            this.hideMessagesPanel();
            _logger.info("trKeyIn() End");
            return action;
        } else if ("F4".equalsIgnoreCase(keyName)) {
            HhNoPickPackSBean hhNoPickPackSBean = (HhNoPickPackSBean) this.getPageFlowBean("HhNoPickPackSBean");
            String pick = hhPickPackNoPickPackViewRow.getPick();
            String waveNumber = hhPickPackNoPickPackViewRow.getWaveNbr();
            String startPicLoc = hhPickPackNoPickPackViewRow.getStartPickLoc();
            String picType = hhPickPackNoPickPackViewRow.getPickType();
            String facilityId = (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
            String user = this.getHhNoPickPackSNoPickPackWorkViewRow().getUser();
            List<String> waveStatus = checkWaveStatus(waveNumber, facilityId);
            if (waveStatus == null) {
                if (pick == null) {
                    _logger.info("Exception: pick is null! : PARTIAL_ENTRY");
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                    String msg =
                        this.getMessage(PARTIAL_ENTRY, ERROR, facilityId,
                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                    this.showMessagesPanel(ERROR, msg);
                } else {
                    if ("C".equalsIgnoreCase(pick)) {
                        hhNoPickPackSBean.setCPickType("C");
                        hhNoPickPackSBean.setCpPickType("");
                        hhNoPickPackSBean.setBpPickType("");
                    } else {
                        if ("U".equalsIgnoreCase(pick)) {
                            hhNoPickPackSBean.setCPickType("");
                            hhNoPickPackSBean.setCpPickType("CP");
                            hhNoPickPackSBean.setBpPickType("BP");
                        } else {
                            hhNoPickPackSBean.setCPickType("C");
                            hhNoPickPackSBean.setCpPickType("CP");
                            hhNoPickPackSBean.setBpPickType("BP");
                        }
                    }
                    String wHAllowIntrlv = loCScpByWh(facilityId, startPicLoc, "interleaving_enabled");
                    String gWaveType = gWaveType(facilityId, waveNumber);
                    if (("BP").equalsIgnoreCase(picType) && (YES).equalsIgnoreCase(wHAllowIntrlv) &&
                        !"PO".equalsIgnoreCase(gWaveType)) {
                        boolean putawayUserPrivilege = checkUserPrivilige(user, facilityId, "hh_putaway_inventory_s");
                        boolean pickingUserPrvilige = checkUserPrivilige(user, facilityId, "hh_bulk_pick_s");
                        if (putawayUserPrivilege && pickingUserPrvilige) {
                            String message =
                                this.getMessage("CONFIRM_INTRLVG", "C", facilityId,
                                                (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                            this.getDialogOutputText().setValue(message);
                            this.disableFields();
                            this.getConfirmPopup().show(new RichPopup.PopupHints());
                            _logger.info("trKeyIn() End");
                            return null;
                        } else {
                            if (!pickingUserPrvilige) {
                                hhNoPickPackSBean.setPmAllowIntrlvg(NO);
                            } else {
                                _logger.info("Exception: Insufficient privelidges!");
                                String msg =
                                    this.getMessage("INSUFF_PRIV", WARN, facilityId,
                                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                                this.showMessagesPanel(WARN, msg);
                                hhNoPickPackSBean.setPmAllowIntrlvg(NO);
                                this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                            }
                        }
                    } else {
                        hhNoPickPackSBean.setPmAllowIntrlvg(NO);
                    }
                    this.callPickForm();
                }
            } else {
                _logger.info("Exception: waveStatus is non-null value.");
                String error = waveStatus.get(1);
                String erroMsg = error.substring(0, error.indexOf("RAISE"));
                this.showMessagesPanel(waveStatus.get(0), erroMsg);
                this.addErrorStyleToComponent(this.getPickType());
                this.getPicTypeIcon().setName(ERR_ICON);
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                this.getWaveNumber().setDisabled(true);
                this.getPickType().setDisabled(false);
                this.getPick().setDisabled(true);
                this.refreshContentOfUIComponent(this.getWaveNumber());
                this.refreshContentOfUIComponent(this.getPickType());
                this.refreshContentOfUIComponent(this.getPick());
                this.refreshContentOfUIComponent(this.getPicTypeIcon());
                this.refreshContentOfUIComponent(this.getNoPickPackFormLayout());
                this.setFocusPickType();
                this.selectTextOnUIComponent(this.getPickType());
            }
        }
        _logger.info("trKeyIn() End");
        return null;
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmDialogListener() Start");
        _logger.fine("dialogEvent value: " + dialogEvent);
        HhNoPickPackSBean hhNoPickPackSBean = (HhNoPickPackSBean) this.getPageFlowBean("HhNoPickPackSBean");
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            hhNoPickPackSBean.setPmAllowIntrlvg(YES);
        } else {
            hhNoPickPackSBean.setPmAllowIntrlvg(NO);
        }
        this.callPickForm();
        _logger.info("confirmDialogListener() End");
    }

    private String loCScpByWh(String facilityId, String startPicLoc, String string) {
        _logger.info("loCScpByWh() Start");
        _logger.fine("facilityId value: " + facilityId + "\n" + "startPicLoc value: " + startPicLoc);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CALL_LOCSCP_BYWH);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("startPicLoc", startPicLoc);
        _logger.info("loCScpByWh() End");
        return (String) oper.execute();
    }

    private String gWaveType(String facilityId, String waveNumber) {
        _logger.info("gWaveType() Start");
        _logger.fine("facilityId value: " + facilityId + "\n" + "waveNumber value: " + waveNumber);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GET_WAVE_TYPE);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("waveNumber", waveNumber);
        _logger.info("gWaveType() End");
        return (String) oper.execute();
    }

    private boolean checkUserPrivilige(String userId, String facilityId, String screenName) {
        _logger.info("checkUserPrivilige() Start");
        _logger.fine("userId value: " + userId + "\n" + "facilityId value: " + facilityId + "\n" +
                     "screenName value: " + screenName);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GET_USER_PRIVILIGE);
        oper.getParamsMap().put("user", userId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("screenName", screenName);
        _logger.info("checkUserPrivilige() End");
        return (Boolean) oper.execute();
    }

    private boolean validateRequiredFields() {
        _logger.info("validateRequiredFields() Start");
        Row currentRow = this.getCurrentNoPickPackRow();
        boolean validateOK = this.validateRequiredField(currentRow.getAttribute("WaveNbr"), this.getWaveNumber());
        if (validateOK) {
            validateOK =
                this.validateRequiredField(currentRow.getAttribute("StartPickLoc"), this.getStartPicLocation());
            if (!validateOK) {
                _logger.info("Exception: Failed to validate required field.");
            
                this.showMessagesPanel(ERROR,
                                       this.getMessage("INV_LOCATION", ERROR,
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                this.setFocusStartPickLocation();
                this.addErrorStyleToComponent(this.getStartPicLocation());
                this.getStartPicLocIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getStartPicLocIcon());
            } else {
                validateOK = this.validateRequiredField(currentRow.getAttribute("PickType"), this.getPickType());
                if (!validateOK) {
                    _logger.info("Exception: Failed to validate required field.");
                   
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_PICK_TYPE", ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                    this.setFocusPickType();
                    this.addErrorStyleToComponent(this.getPickType());
                    this.getPicTypeIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getPicTypeIcon());
                } else {
                    if ("BP".equalsIgnoreCase((String) currentRow.getAttribute("PickType"))) {
                        validateOK = this.validateRequiredField(currentRow.getAttribute("Pick"), this.getPick());
                        if (!validateOK) {
                            _logger.info("Exception: Failed to validate required field.");
                          
                            this.showMessagesPanel(ERROR,
                                                   this.getMessage(PARTIAL_ENTRY, ERROR,
                                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                            this.setFocusPick();
                            this.addErrorStyleToComponent(this.getPick());
                            this.getPickIcon().setName(ERR_ICON);
                            this.refreshContentOfUIComponent(this.getPickIcon());
                        }
                    }
                }
            }
        } else {
            _logger.info("Exception: Failed to validate required field.");
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.setFocusWaveNumber();
            this.addErrorStyleToComponent(this.getWaveNumber());
            this.getWaveNumberIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getWaveNumberIcon());
        }
        this.getHhNoPickPackSBeanPageFlowBean().setIsError(validateOK);
        _logger.fine("validateOK value: " + validateOK);
        _logger.info("validateRequiredFields() End");
        return validateOK;
    }

    private boolean validateValidFields() {
        _logger.info("validateValidFields() Start");
        String waveNumberStyle = this.getWaveNumber().getStyleClass();
        String startPickLocationStyle = this.getStartPicLocation().getStyleClass();
        String pickTypeStyle = this.getPickType().getStyleClass();
        String pickStyle = this.getPick().getStyleClass();
        _logger.info("validateValidFields() End");
        return (waveNumberStyle == null || !waveNumberStyle.contains(ERROR_STYLE_CLASS)) &&
               (pickTypeStyle == null || !pickTypeStyle.contains(ERROR_STYLE_CLASS)) &&
               (pickStyle == null || !pickStyle.contains(ERROR_STYLE_CLASS)) &&
               (startPickLocationStyle == null || !startPickLocationStyle.contains(ERROR_STYLE_CLASS));
    }

    private Row getCurrentNoPickPackRow() {
        return ADFUtils.findIterator(NO_PICK_PACK_ITERATOR).getCurrentRow();
    }

    private List<String> checkWaveStatus(String waveNumber, String facilityId) {
        _logger.info("checkWaveStatus() Start");
        _logger.fine("waveNumber value: " + waveNumber + "\n" + "facilityId value: " + facilityId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(GET_WAVE_STATUS);
        oper.getParamsMap().put("waveNumber", waveNumber);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
        List<String> result = null;
        if (oper.getErrors().isEmpty()) {
            result = (List<String>) oper.getResult();
        }
        _logger.fine("result value: " + result);
        _logger.fine("checkWaveStatus() End");
        return result;
    }

    private void callPickForm() {
        _logger.info("callPickForm() Start");
        HhNoPickPackSBean hhNoPickPackSBean = this.getHhNoPickPackSBeanPageFlowBean();

        HhNoPickPackSNoPickPackViewRowImpl hhPickPackNoPickPackViewRow = this.getHhNoPickPackSNoPickPackViewRow();
        GlobalVariablesViewRowImpl grow = hhNoPickPackSBean.getGlobalVariablesViewCurrentRow();

        String allowInterlvg = hhNoPickPackSBean.getPmAllowIntrlvg();
        grow.setGlobalAllowIntrlvg(allowInterlvg);

        String pick = hhPickPackNoPickPackViewRow.getPick();
        String waveNumber = hhPickPackNoPickPackViewRow.getWaveNbr();
        if (waveNumber != null)
         grow.setGlobalWaveNbr(waveNumber.toString());

        String startPicLoc = hhPickPackNoPickPackViewRow.getStartPickLoc();
        grow.setGlobalStartPickLoca(startPicLoc);

        String picType = hhPickPackNoPickPackViewRow.getPickType();
        if (pick != null) {
        grow.setGlobalNoPickPackPickType(picType.toUpperCase());


        String l_choose_pick = getLChoosePickValue();
        if ("BP".equals(picType) && YES.equals(l_choose_pick)) {
            if ("C".equals(pick)) {
                grow.setGlobalNoPickPackPickTypeC("C");
                grow.setGlobalNoPickPackPickTypeCP("~");
                grow.setGlobalNoPickPackPickType("~");
            } else if ("U".equals(pick)) {
                grow.setGlobalNoPickPackPickTypeC("~");
                grow.setGlobalNoPickPackPickTypeCP("CP");
                grow.setGlobalNoPickPackPickType("BP");
            } else {
                grow.setGlobalNoPickPackPickTypeC("C");
                grow.setGlobalNoPickPackPickTypeCP("CP");
                grow.setGlobalNoPickPackPickType("BP");
            }
        } else if ("BP".equals(picType)) {
            grow.setGlobalNoPickPackPickTypeC("C");
            grow.setGlobalNoPickPackPickTypeCP("CP");
            grow.setGlobalNoPickPackPickType("BP");
        } else {
            grow.setGlobalNoPickPackPickType(picType);
        }}

        if (pick != null && waveNumber != null && startPicLoc != null && picType != null) {
            if (("BP").equalsIgnoreCase(picType) || ("B").equalsIgnoreCase(picType) ||
                ("PR").equalsIgnoreCase(picType)) {
                //call BP form
                String callingFrom = "hh_no_pick_pack_s";
                grow.setGlobalCallingForm(callingFrom);
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(false);
                ADFUtils.invokeAction("bulkPick");
            } else if (("CP").equalsIgnoreCase(picType) || ("CR").equalsIgnoreCase(picType) ||
                       ("C").equalsIgnoreCase(picType)) {
                // call CP form
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(false);
                grow.setGlobalCallingForm("HH_CONTAINER_PICK_S");
                ADFUtils.invokeAction("containerPick");
            } else if (("UM").equalsIgnoreCase(picType) || ("UR").equalsIgnoreCase(picType)) {
                //call prime unit pick form
            } else {
                //call begin unit pick form
            }
        }
        _logger.info("callPickForm() End");
    }

    private String getLChoosePickValue() {
        _logger.info("getLChoosePickValue() Start");
        String facilityId = (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
        String picLocationID = ((String) ADFUtils.getBoundAttributeValue(START_PIC_LOC)).toUpperCase();
        String whId = getWareHouseId(facilityId, picLocationID);
        String choosePick = geScpwh(facilityId, whId);
        _logger.fine("choosePick value: " + choosePick);
        _logger.info("getLChoosePickValue() End");
        return choosePick;
    }

    private void disableFields() {
        _logger.info("disableFields() Start");
        this.getWaveNumber().setDisabled(true);
        this.getStartPicLocation().setDisabled(true);
        this.getPickType().setDisabled(true);
        this.getPick().setDisabled(true);
        this.refreshContentOfUIComponent(this.getWaveNumber());
        this.refreshContentOfUIComponent(this.getStartPicLocation());
        this.refreshContentOfUIComponent(this.getPickType());
        this.refreshContentOfUIComponent(this.getPick());
        _logger.info("disableFields() End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            boolean isError = this.getHhNoPickPackSBeanPageFlowBean().isIsError();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) && !isError &&
                       (null != submittedVal && !submittedVal.isEmpty())) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
        _logger.info("performKey() End");
    }

    private void queueValueChangeEvent(RichInputText textFields, Object newValue) {
        ValueChangeEvent vce = new ValueChangeEvent(textFields, null, newValue);
        String id = textFields.getId();
        switch (id) {
        case "wnpp":
            this.onChangedWaveNbrNoPickPack(vce);
            break;
        case "splp":
            this.changedStartPickLocNoPickPack(vce);
            break;
        case "ptnn":
            this.onChangedPickTypeNoPickPack(vce);
            break;
        case "pnpp":
            this.onChangedPickNoPickPack(vce);
            //            if (!this.getHhNoPickPackSBeanPageFlowBean().isIsError())
            //                this.setFocusWaveNumber();
            break;
        default:
            break;
        }
    }

    private boolean isCurrentTaskFlowActive() {
        return this.getHhNoPickPackSBeanPageFlowBean() != null;

    }

    public void performKeyPopup(ClientEvent clientEvent) {
        _logger.info("performKeyPopup() Start");
        _logger.fine("clientEvent value: " + clientEvent);
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.getConfirmPopup().hide();
                this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.no));
                this.getConfirmPopup().hide();
            }
        }
        _logger.info("performKeyPopup() End");
    }

    private void validateWaveNumber(String waveNumber) {
        _logger.info("validateWaveNumber() Start");
        _logger.fine("waveNumber value: " + waveNumber);
        if (!this.getWaveNumber().isDisabled())
            if (null != waveNumber && Pattern.matches("[0-9]+", waveNumber) == true) {
                String facilityId = (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
                List<String> result = this.callWaveNumber(waveNumber, facilityId);
                if (result != null && !result.isEmpty()) {
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                    this.showMessagesPanel(result.get(0), result.get(1));
                    this.addErrorStyleToComponent(this.getWaveNumber());
                    this.getWaveNumberIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getWaveNumberIcon());
                    this.setFocusWaveNumber();
                    this.selectTextOnUIComponent(this.getWaveNumber());
                } else {
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(false);
                    this.hideMessagesPanel();
                    this.getWaveNumber().setDisabled(true);
                    this.removeErrorStyleToComponent(this.getWaveNumber());
                    this.getWaveNumberIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getWaveNumberIcon());
                    this.refreshContentOfUIComponent(this.getWaveNumber());
                    this.setFocusStartPickLocation();

                }
                this.refreshContentOfUIComponent(this.getNoPickPackFormLayout());
            } else {
                if (null == waveNumber)
                    this.showMessagesPanel(ERROR,
                                           this.getMessage(PARTIAL_ENTRY, ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                else
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("MUST_BE_NUMERIC", ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                this.addErrorStyleToComponent(this.getWaveNumber());
                this.getWaveNumberIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getWaveNumberIcon());
                this.setFocusWaveNumber();
                this.selectTextOnUIComponent(this.getWaveNumber());
                this.refreshContentOfUIComponent(this.getNoPickPackFormLayout());
            }
        _logger.info("validateWaveNumber() End");
    }

    private void validateStartLocation(String startPicLoc) {
        _logger.info("validateStartLocation() Start");
        if (!this.getStartPicLocation().isDisabled()) {
            if (startPicLoc == null) {
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR,
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            } else {
                String facilityId = (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
                List<String> result = this.callPicLoc(startPicLoc.toUpperCase(), facilityId);
                if (result != null && !result.isEmpty()) {
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                    this.showMessagesPanel(result.get(0), result.get(1));
                    this.addErrorStyleToComponent(this.getStartPicLocation());
                    this.getStartPicLocIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getStartPicLocIcon());
                    this.selectTextOnUIComponent(this.getStartPicLocation());
                } else {
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(false);
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getStartPicLocation());
                    this.getStartPicLocIcon().setName(REQ_ICON);
                    this.getPickType().setDisabled(false);
                    this.refreshContentOfUIComponent(this.getStartPicLocIcon());
                    this.refreshContentOfUIComponent(this.getStartPicLocation());
                    this.refreshContentOfUIComponent(this.getPickType());
                    this.setFocusPickType();
                }
            }
            this.refreshContentOfUIComponent(this.getNoPickPackFormLayout());

        }
        _logger.info("validateStartLocation() End");
    }

    private void validatePickType(String picType) {
        _logger.info("validatePickType() Start");
        _logger.fine("picType value: " + picType);
        if (!this.getPickType().isDisabled()) {
            if (picType == null) {
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR,
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            } else {
                String facilityId = (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
                String waveNumber = (String) ADFUtils.getBoundAttributeValue(WAVE_NUMBER_ATTR);
                String picLocationID = ((String) ADFUtils.getBoundAttributeValue(START_PIC_LOC)).toUpperCase();
                HhNoPickPackSNoPickPackViewRowImpl currRow = this.getHhNoPickPackSNoPickPackViewRow();
                if (("C").equalsIgnoreCase(picType) || ("CP").equalsIgnoreCase(picType) ||
                    ("BP").equalsIgnoreCase(picType)) {
                    String whId = getWareHouseId(facilityId, picLocationID);
                    String choosePick = geScpwh(facilityId, whId);
                    if ((!("BP").equalsIgnoreCase(picType)) ||
                        (("BP").equalsIgnoreCase(picType) && NO.equalsIgnoreCase(choosePick))) {
                        currRow.setPick("ALL");
                    }
                    List<String> result = this.callPicType(picType.toUpperCase(), facilityId, waveNumber);
                    if (result != null && !result.isEmpty()) {
                        this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                        this.showMessagesPanel(result.get(0), result.get(1));
                        this.addErrorStyleToComponent(this.getPickType());
                        this.getPicTypeIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getPicTypeIcon());
                        this.setFocusPickType();
                        this.selectTextOnUIComponent(this.getPickType());
                    } else {
                        this.getHhNoPickPackSBeanPageFlowBean().setIsError(false);
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getPickType());
                        this.getPicTypeIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getPicTypeIcon());
                        this.refreshContentOfUIComponent(this.getPickType());
                        HhNoPickPackSBean hhNoPickPackSBean =
                            (HhNoPickPackSBean) this.getPageFlowBean("HhNoPickPackSBean");
                        GlobalVariablesViewRowImpl grow = hhNoPickPackSBean.getGlobalVariablesViewCurrentRow();
                        if (("BP").equalsIgnoreCase(picType) && (YES).equalsIgnoreCase(choosePick)) {
                            //currRow.setPick(null);
                            this.getPick().setDisabled(false);
                            this.getPickType().setDisabled(true);
                            this.refreshContentOfUIComponent(this.getPick());
                            this.refreshContentOfUIComponent(this.getPickType());
                            this.setFocusPick();
                            this.selectTextOnUIComponent(this.getPick());
                            grow.setGlobalCPPickType(hhNoPickPackSBean.getCpPickType());
                            grow.setGlobalCPickType(hhNoPickPackSBean.getCPickType());
                            grow.setGlobalBPPickType(hhNoPickPackSBean.getBpPickType());
                        } else {
                            if ((!("BP").equalsIgnoreCase(picType)) ||
                                (("BP").equalsIgnoreCase(picType) && NO.equalsIgnoreCase(choosePick))) {
                                currRow.setPick("ALL");
                                this.getPick().setDisabled(true);
                                this.getPickType().setDisabled(true);
                                this.refreshContentOfUIComponent(this.getPickType());
                                this.refreshContentOfUIComponent(this.getPick());
                                this.getWaveNumber().setDisabled(false);
                                this.refreshContentOfUIComponent(this.getWaveNumber());
                                this.setFocusWaveNumber();
                                this.selectTextOnUIComponent(this.getWaveNumber());

                                if (!YES.equalsIgnoreCase(choosePick)) {
                                    grow.setGlobalCPPickType("CP");
                                    grow.setGlobalCPickType("C");
                                    grow.setGlobalBPPickType("BP");
                                } else
                                    grow.setGlobalBPPickType(currRow.getPickType());
                            }
                        }
                    }
                    this.refreshContentOfUIComponent(this.getNoPickPackFormLayout());
                } else {
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_PICK_TYPE", ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                    this.addErrorStyleToComponent(this.getPickType());
                    this.getPicTypeIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getPicTypeIcon());
                    this.setFocusPickType();
                    this.selectTextOnUIComponent(this.getPickType());
                    this.refreshContentOfUIComponent(this.getNoPickPackFormLayout());
                }
            }
        }
        _logger.info("validatePickType() End");
    }

    private void validatePick(String pick) {
        _logger.info("validatePick() Start");
        _logger.fine("pick value: " + pick);
        if (!this.getPick().isDisabled()) {
            if (("C").equalsIgnoreCase(pick) || ("U").equalsIgnoreCase(pick) || ("ALL").equalsIgnoreCase(pick)) {
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(false);
                this.hideMessagesPanel();
                this.removeErrorStyleToComponent(this.getPick());
                this.getWaveNumber().setDisabled(false);
                this.getPick().setDisabled(true);
                this.getPickIcon().setName("");
                this.refreshContentOfUIComponent(this.getWaveNumber());
                this.refreshContentOfUIComponent(this.getPickIcon());
                this.refreshContentOfUIComponent(this.getPick());
                this.setFocusWaveNumber();
            } else {
                if (pick == null) {
                    this.showMessagesPanel(ERROR,
                                           this.getMessage(PARTIAL_ENTRY, ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                } else {
                    this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_PICK", ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));

                }
                this.getHhNoPickPackSBeanPageFlowBean().setIsError(true);
                this.getPickIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getPickIcon());
                this.addErrorStyleToComponent(this.getPick());
                this.setFocusPick();
                this.selectTextOnUIComponent(this.getPick());

            }
        }
        _logger.info("validatePick() End");
    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        _logger.info("onRegionLoad() Start");
        _logger.fine("cse value: " + cse);
        if (getHhNoPickPackSBeanPageFlowBean() == null) {
            _logger.info("onRegionLoad() End");
            return;
        }
        Object gotoFld = getHhNoPickPackSBeanPageFlowBean().getIsFocusOn();
        if ("waveNbrNoPickPack".equals(gotoFld))
            this.setFocusOnUIComponent(this.getWaveNumber());
        else if ("pickTypeNoPickPack".equals(gotoFld))
            this.setFocusOnUIComponent(this.getPickType());
        else if ("startPickLocNoPickPack".equals(gotoFld))
            this.setFocusOnUIComponent(this.getStartPicLocation());
        else if ("pickNoPickPack".equals(gotoFld))
            this.setFocusOnUIComponent(this.getPick());
        _logger.info("onRegionLoad() End");
    }

    private HhNoPickPackSBean getHhNoPickPackSBeanPageFlowBean() {
        return ((HhNoPickPackSBean) this.getPageFlowBean("HhNoPickPackSBean"));
    }

    public void okLinkGotoPopupActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.invokeAction("bulkPick");
    }

    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.getConfirmPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.getConfirmPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.no));
    }

    private HhNoPickPackSWorkViewRowImpl getHhNoPickPackSNoPickPackWorkViewRow() {
        return (HhNoPickPackSWorkViewRowImpl) (ADFUtils.findIterator("HhNoPickPackSWorkViewIterator")).getCurrentRow();
    }

    private HhNoPickPackSNoPickPackViewRowImpl getHhNoPickPackSNoPickPackViewRow() {
        return (HhNoPickPackSNoPickPackViewRowImpl) ADFUtils.findIterator("HhNoPickPackSNoPickPackViewIterator").getCurrentRow();
    }

    public void setWaveNumber(RichInputText waveNumber) {
        this.waveNumber = waveNumber;
    }

    public RichInputText getWaveNumber() {
        return waveNumber;
    }

    public void setPick(RichInputText pick) {
        this.pick = pick;
    }

    public RichInputText getPick() {
        return pick;
    }

    public void setWaveNumberIcon(RichIcon waveNumberIcon) {
        this.waveNumberIcon = waveNumberIcon;
    }

    public RichIcon getWaveNumberIcon() {
        return waveNumberIcon;
    }

    public void setPickIcon(RichIcon pickIcon) {
        this.pickIcon = pickIcon;
    }

    public RichIcon getPickIcon() {
        return pickIcon;
    }

    public void setPicTypeIcon(RichIcon picTypeIcon) {
        this.picTypeIcon = picTypeIcon;
    }

    public RichIcon getPicTypeIcon() {
        return picTypeIcon;
    }

    public void setStartPicLocIcon(RichIcon startPicLocIcon) {
        this.startPicLocIcon = startPicLocIcon;
    }

    public RichIcon getStartPicLocIcon() {
        return startPicLocIcon;
    }

    public void setBulkPickScreenDialog(RichDialog bulkPickScreenDialog) {
        this.bulkPickScreenDialog = bulkPickScreenDialog;
    }

    public RichDialog getBulkPickScreenDialog() {
        return bulkPickScreenDialog;
    }

    public void setConfirmDialog(RichDialog confirmDialog) {
        this.confirmDialog = confirmDialog;
    }

    public RichDialog getConfirmDialog() {
        return confirmDialog;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setBulkPickScreenDialogOutputText(RichOutputText bulkPickScreenDialogOutputText) {
        this.bulkPickScreenDialogOutputText = bulkPickScreenDialogOutputText;
    }

    public RichOutputText getBulkPickScreenDialogOutputText() {
        return bulkPickScreenDialogOutputText;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmationMessage(RichMessage confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    public RichMessage getConfirmationMessage() {
        return confirmationMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorAfMessage(RichMessage errorAfMessage) {
        this.errorAfMessage = errorAfMessage;
    }

    public RichMessage getErrorAfMessage() {
        return errorAfMessage;
    }

    public void setStartPicLocation(RichInputText startPicLocation) {
        this.startPicLocation = startPicLocation;
    }

    public RichInputText getStartPicLocation() {
        return startPicLocation;
    }

    public void setPickType(RichInputText pickType) {
        this.pickType = pickType;
    }

    public RichInputText getPickType() {
        return pickType;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic() {
        return pic;
    }

    public void setWarningOutputText(RichOutputText warningOutputText) {
        this.warningOutputText = warningOutputText;
    }

    public RichOutputText getWarningOutputText() {
        return warningOutputText;
    }

    public void setErrorOutputText(RichOutputText errorOutputText) {
        this.errorOutputText = errorOutputText;
    }

    public RichOutputText getErrorOutputText() {
        return errorOutputText;
    }

    public void setInfoOutputText(RichOutputText infoOutputText) {
        this.infoOutputText = infoOutputText;
    }

    public RichOutputText getInfoOutputText() {
        return infoOutputText;
    }


    public void setInfoMessage(RichMessage infoMessage) {
        this.infoMessage = infoMessage;
    }

    public RichMessage getInfoMessage() {
        return infoMessage;
    }

    public void setWarningMessage(RichMessage warningMessage) {
        this.warningMessage = warningMessage;
    }

    public RichMessage getWarningMessage() {
        return warningMessage;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setNoPickPackFormLayout(RichPanelFormLayout noPickPackFormLayout) {
        this.noPickPackFormLayout = noPickPackFormLayout;
    }

    public RichPanelFormLayout getNoPickPackFormLayout() {
        return noPickPackFormLayout;
    }

    private void setFocusWaveNumber() {
        this.getHhNoPickPackSBeanPageFlowBean().setIsFocusOn("waveNbrNoPickPack");
        this.setFocusOnUIComponent(this.getWaveNumber());
    }

    private void setFocusStartPickLocation() {
        this.getHhNoPickPackSBeanPageFlowBean().setIsFocusOn("startPickLocNoPickPack");
        this.setFocusOnUIComponent(this.getStartPicLocation());
    }

    private void setFocusPickType() {
        this.getHhNoPickPackSBeanPageFlowBean().setIsFocusOn("pickTypeNoPickPack");
        this.setFocusOnUIComponent(this.getPickType());
    }

    private void setFocusPick() {
        this.getHhNoPickPackSBeanPageFlowBean().setIsFocusOn("pickNoPickPack");
        this.setFocusOnUIComponent(this.getPick());
    }

}

