package com.ross.rdm.hotelpicking.hhmoveinventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichMessage;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMHotelPickingBackingBean {

    private final static String MOVE_INVENTORY_ITERATOR = "HhMoveInventorySMoveBlockViewIterator";
    private final static String FORM_NAME = "HH_MOVE_INVENTORY_S";
    private final static String F2_ITEM_KEY = "MOVE_BLOCK.F2";
    private final static String F3_ITEM_KEY = "MOVE_BLOCK.F3";
    private final static String F4_ITEM_KEY = "MOVE_BLOCK.F4";
    private final static String F5_ITEM_KEY = "MOVE_BLOCK.F5";
    private final static String F6_ITEM_KEY = "MOVE_BLOCK.F6";
    private final static String F7_ITEM_KEY = "MOVE_BLOCK.F7";
    private final static String F8_ITEM_KEY = "MOVE_BLOCK.F8";

    private final static String CONTAINER_ITEM = "containerId";

    private RichPopup confirmPopup;
    private RichPopup pendingTaskPopup;
    private RichMessage confirmationMessage;
    private RichMessage pendingTaskMessage;
    private RichPanelGroupLayout errorPanel;
    private RichPanelFormLayout moveInventoryPanel;
    private RichOutputText errorMessage;
    private RichInputText inputTextContainerId;
    private RichPopup markForCycleCountPopup;
    private RichInputText inputTextToLocation;
    private RichIcon iconErrorMessage;
    private RichIcon iconContainerId;
    private RichIcon iconToLocation;
    private RichLink exitLink;
    private RichLink detailLink;
    private RichLink waveLink;
    private RichDialog confirmDialog;
    private RichDialog confirmDialog2;
    private RichLink shutlLink;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    public Page1Backing() {

    }

    private Row getCurrentMoveInventoryRow() {
        return ADFUtils.findIterator(MOVE_INVENTORY_ITERATOR).getCurrentRow();
    }

    public void valueChangeContainerId(ValueChangeEvent valueChangeEvent) {
        _logger.info("valueChangeContainerId() Start");
        if(!this.isNavigationExecuted()){
            if (this.getHhMovePageFlowBean() != null) {
                this.getHhMovePageFlowBean().setIsError(false);

                String newValue = (String) valueChangeEvent.getNewValue();
                if (newValue != null && !newValue.equalsIgnoreCase((String) valueChangeEvent.getOldValue()) &&
                    !this.getInputTextContainerId().isDisabled()) {
                    this.validatorContainerId(newValue, false);
                }
            }
        }
        _logger.info("valueChangeContainerId() End");
    }

    public void validatorContainerId(String containerId, boolean executeDone) {
        _logger.info("validatorContainerId() Start");
        this.getHhMovePageFlowBean().setIsWavedContainer(false);
        if (containerId != null && !"".equals(containerId.trim())) {
            this.getHhMovePageFlowBean().setIsError(false);
            if (executeDone) {
                this.executeDisplayMoveBlockOperation(containerId.toUpperCase(), true);
            } else {
                this.callCheckContainerTrouble(containerId);
            }
        } else {
            _logger.info("containerId is null!");
            this.getHhMovePageFlowBean().setIsError(true);
            Row currentRow = this.getCurrentMoveInventoryRow();
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR, (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                   (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(this.getInputTextContainerId());
            this.getIconContainerId().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconContainerId());
            this.setFocusOnUIComponent(this.getInputTextContainerId());
        }
        _logger.info("validatorContainerId() End");

    }

    private void callCheckContainerTrouble(String containerId) {
        _logger.info("callCheckContainerTrouble() Start");
        Row currentRow = this.getCurrentMoveInventoryRow();
        String facilityId = (String) currentRow.getAttribute(ATTR_FACILITY_ID);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckContainerTrouble");
        oper.getParamsMap().put("containerId", containerId.toUpperCase());
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("formName", FORM_NAME);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (YES.equalsIgnoreCase((String) oper.getResult())) {
                String langCode = (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE);
                this.getConfirmationMessage().setMessage(this.getMessage("TRB_STATUS", CONF, facilityId, langCode));
                this.disableAllFields();
                this.getConfirmPopup().show(new RichPopup.PopupHints());
            } else {
                _logger.info("Could not execute 'callCheckContainerTrouble'!");
                this.executeDisplayMoveBlockOperation(containerId.toUpperCase(), false);
            }
        }
        _logger.info("callCheckContainerTrouble() End");
    }

    private void clearFormFields() {
        _logger.info("clearFormFields() Start");
        this.getHhMovePageFlowBean().setIsError(false);
        this.getHhMovePageFlowBean().setIsWavedContainer(false);
        this.hideMessagesPanel();
        Row currentRow = this.getCurrentMoveInventoryRow();
        currentRow.setAttribute("WcsToLocation", null);
        currentRow.setAttribute("DestId", null);
        currentRow.setAttribute("ContainerId", null);
        currentRow.setAttribute("LocationId", null);
        currentRow.setAttribute("ItemId", null);
        currentRow.setAttribute("Desc", null);
        currentRow.setAttribute("UpsCode", null);
        currentRow.setAttribute("ToLocation", null);
        currentRow.setAttribute("OldLocation", null);
        currentRow.setAttribute("PutawayDate", null);
        currentRow.setAttribute("ContainerStatus", null);
        currentRow.setAttribute("ContainerQty", null);
        currentRow.setAttribute("StorageCategory", null);
        this.refreshContentOfUIComponent(this.getMoveInventoryPanel());
        _logger.info("clearFormFields() End");
    }

    private void disableAllFields() {
        _logger.info("disableAllFields Start");
        this.getInputTextContainerId().setDisabled(true);
        this.getInputTextToLocation().setDisabled(true);
        this.refreshContentOfUIComponent(this.getInputTextContainerId());
        this.refreshContentOfUIComponent(this.getInputTextToLocation());
        _logger.info("disableAllFields End");
    }

    public void confirmationDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmationDialogListener Start");
        _logger.fine("dialogEvent value: " + dialogEvent);
        Row currentRow = this.getCurrentMoveInventoryRow();
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            String containerId = (String) currentRow.getAttribute("ContainerId");
            this.executeDisplayMoveBlockOperation(containerId != null ? containerId.toUpperCase() : null, false);
        } else if (DialogEvent.Outcome.no.equals(dialogEvent.getOutcome())) {
            this.clearFormFields();
            this.getInputTextContainerId().setDisabled(false);
            this.refreshContentOfUIComponent(this.getInputTextContainerId());
            currentRow.setAttribute("ContainerId", null);
            this.getInputTextContainerId().resetValue();
            this.setFocusContainerId();
        }
        _logger.info("confirmationDialogListener End");

    }

    public void cancelConfirmationPopupListener(PopupCanceledEvent popupCanceledEvent) {
        _logger.info("cancelConfirmationPopupListener Start");
        Row currentRow = this.getCurrentMoveInventoryRow();
        currentRow.setAttribute("ContainerId", null);
        this.getInputTextContainerId().resetValue();
        this.setFocusContainerId();
        _logger.info("cancelConfirmationPopupListener End");

    }

    public void executeDisplayMoveBlockOperation(String containerId, boolean executeDone) {
        _logger.info("executeDisplayMoveBlockOperation Start");
        _logger.fine("containerId value: " + containerId);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callDisplayMoveBlock");
        oper.getParamsMap().put("containerId", containerId);
        List<String> errorsMessages = (List<String>) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (errorsMessages != null && !errorsMessages.isEmpty()) {
                _logger.info("Unable to execute 'callDisplayMoveBlock'!");
                this.getHhMovePageFlowBean().setIsError(true);
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                this.addErrorStyleToComponent(this.getInputTextContainerId());
                this.getIconContainerId().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconContainerId());
                this.selectTextOnUIComponent(this.getInputTextContainerId());
                Row currentRow = this.getCurrentMoveInventoryRow();
                String wavedMsg =
                    this.getMessage("CONT_WAVED", WARN, (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                    (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE));
                if (wavedMsg != null && wavedMsg.equalsIgnoreCase(errorsMessages.get(1))) {
                    this.getHhMovePageFlowBean().setIsWavedContainer(true);
                }

            } else {
                this.getHhMovePageFlowBean().setIsWavedContainer(false);
                this.getHhMovePageFlowBean().setIsError(false);
                this.hideMessagesPanel();

                this.getInputTextContainerId().setDisabled(true);
                this.getIconContainerId().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconContainerId());

                if (executeDone) {
                    // this.doneMoveInventory();
                    // ISSUE 1923
                    this.validatorToLocation((String) ADFUtils.getBoundAttributeValue("ToLocation"), true);
                } else {
                    this.removeErrorStyleToComponent(this.getInputTextContainerId());
                    this.refreshContentOfUIComponent(this.getInputTextContainerId());
                    this.getInputTextToLocation().setDisabled(false);
                    this.setFocusToLocation();
                }
            }
            _logger.info("executeDisplayMoveBlockOperation End");
        }
    }

    public void valueChangeToLocation(ValueChangeEvent valueChangeEvent) {
        _logger.info("valueChangeToLocation Start");
        if(!this.isNavigationExecuted()){
            if (null != this.getHhMovePageFlowBean()) {
                this.getHhMovePageFlowBean().setIsError(false);
                String newValue = (String) valueChangeEvent.getNewValue();
                if (newValue != null && !newValue.equalsIgnoreCase((String) valueChangeEvent.getOldValue()) &&
                    !this.getInputTextToLocation().isDisabled()) {
                    this.validatorToLocation(newValue, false);
                }
            }
        }
        _logger.info("valueChangeToLocation End");
    }

    public void validatorToLocation(String toLocation, boolean executeDone) {
        _logger.info("validatorToLocation Start");
        _logger.fine("toLocation value: " + toLocation + "\n" + "executeDone value: " + executeDone);
        if ((ADFUtils.getBoundAttributeValue("ContainerId") != null &&
             !"".equals(ADFUtils.getBoundAttributeValue("ContainerId"))) && toLocation != null &&
            !"".equals(toLocation.trim())) {
             OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateToLocation1");
            oper.getParamsMap().put("toLocation", toLocation.toUpperCase());
            List<String> errorsMessages = (List<String>) oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                if (errorsMessages != null && !errorsMessages.isEmpty()) {
                    this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                    if (ERROR.equals(errorsMessages.get(0))) {
                        _logger.info("Unable to execute 'callValidateToLocation1'!");
                        this.getHhMovePageFlowBean().setIsError(true);
                        this.getIconToLocation().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getIconToLocation());
                        this.addErrorStyleToComponent(this.getInputTextToLocation());
                        this.selectTextOnUIComponent(this.getInputTextToLocation());
                    } else {
                        // bug 2113 we need to handle also W message type
                        if (WARN.equals(errorsMessages.get(0))) {
                            _logger.info("Unable to execute 'callValidateToLocation1'!");
                            this.getHhMovePageFlowBean().setIsError(true);
                            this.getIconToLocation().setName(ERR_ICON);
                            this.refreshContentOfUIComponent(this.getIconToLocation());
                            this.addErrorStyleToComponent(this.getInputTextToLocation());
                            this.selectTextOnUIComponent(this.getInputTextToLocation());
                        } else {
                            this.getHhMovePageFlowBean().setIsError(false);
                            this.removeErrorStyleToComponent(this.getInputTextToLocation());
                            this.refreshContentOfUIComponent(this.getInputTextToLocation());
                            this.getIconToLocation().setName(REQ_ICON);
                            this.refreshContentOfUIComponent(this.getIconToLocation());
                            this.setFocusToLocation();
                        }
                    }
                } else {
                    if (executeDone) {
                        this.doneMoveInventory();
                    } else {
                        this.getHhMovePageFlowBean().setIsError(false);
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getInputTextToLocation());
                        this.refreshContentOfUIComponent(this.getInputTextToLocation());
                        this.getIconToLocation().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getIconToLocation());
                        this.setFocusToLocation();
                    }
                }
            }
        }
        _logger.info("validatorToLocation End");
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel Start");
        _logger.fine("messageType value: " + messageType + "\n" + "msg value: " + msg);
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
        _logger.info("showMessagesPanel End");
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel Start");
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
        _logger.info("hideMessagesPanel End");
    }

    private String callCheckScreenOptionPriv(String optionName, String formName) {
        _logger.info("callCheckScreenOptionPriv() Start");
        _logger.fine("optionName value: " + optionName + "\n" + "formName value: " + formName);
        String result = "OFF";
        Row currentRow = this.getCurrentMoveInventoryRow();
        if (currentRow != null) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckScreenOptionPriv");
            oper.getParamsMap().put("facilityId", currentRow.getAttribute(ATTR_FACILITY_ID));
            oper.getParamsMap().put("userId", currentRow.getAttribute("UserId"));
            oper.getParamsMap().put("formName", (formName == null) ? FORM_NAME : formName);
            oper.getParamsMap().put("optionName", optionName);
            result = (String) oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {

                if ("OFF".equals(result)) {
                    this.getHhMovePageFlowBean().setIsError(true);
                    this.showMessagesPanel(WARN,
                                           this.getMessage("NOT_ALLOWED", WARN,
                                                           (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                           (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
                }
            }
        }
        _logger.fine("result value: " + result);
        _logger.info("callCheckScreenOptionPriv() End");
        return result;
    }

    private boolean checkFields(boolean executeDone) {
        _logger.info("checkFields() Start");
        Row currentRow = this.getCurrentMoveInventoryRow();
        boolean validateOK =
            this.validateRequiredField(currentRow.getAttribute("ContainerId"), this.getInputTextContainerId());
        if (validateOK) {
            if (this.getInputTextToLocation().isDisabled()) {
                this.validatorContainerId((String) currentRow.getAttribute("ContainerId"), executeDone);
            } else {
                validateOK =
                    this.validateRequiredField(currentRow.getAttribute("ToLocation"), this.getInputTextToLocation());
                if (validateOK) {
                    this.validatorToLocation((String) currentRow.getAttribute("ToLocation"), executeDone);
                } else {
                    _logger.info("ValidateOK is false");
                    this.getIconToLocation().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getIconToLocation());
                    this.showMessagesPanel(ERROR,
                                           this.getMessage(PARTIAL_ENTRY, ERROR,
                                                           (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                           (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
                    this.setFocusToLocation();
                }
            }
        } else {
            this.getIconContainerId().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconContainerId());
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR, (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                   (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
            this.setFocusContainerId();
        }
        _logger.info("checkFields() End");
        return validateOK;
    }

    public String detailAction() {
        _logger.info("detailAction() Start");
        // F2
        String action = null;
        if (!this.getHhMovePageFlowBean().isIsError()) {
            Row currentRow = this.getCurrentMoveInventoryRow();
            String result =
                this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F2,
                                               RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
            if (!"OFF".equals(result)) {
                if (currentRow.getAttribute("ContainerId") != null) {
                    OperationBinding oper = (OperationBinding) ADFUtils.findOperation("findContainerItemsByParams1");
                    oper.getParamsMap().put("facilityId", currentRow.getAttribute(ATTR_FACILITY_ID));
                    oper.getParamsMap().put("containerId", currentRow.getAttribute("ContainerId"));
                    Long contHasItems = (Long) oper.execute();
                    if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                        if (contHasItems != null && contHasItems.longValue() > 0) {
                            action = "Page2";
                            this.removeErrorStyleToComponent(this.getInputTextToLocation());
                            this.getIconToLocation().setName(REQ_ICON);
                            this.refreshContentOfUIComponent(this.getIconToLocation());
                            this.getHhMovePageFlowBean().setIsError(false);
                        } else {
                            this.getHhMovePageFlowBean().setIsError(true);
                            _logger.info("Could not execute 'findContainerItemsByParams'!");
                            this.showMessagesPanel(WARN,
                                                   this.getMessage("NO_DATA", WARN,
                                                                   (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                                   (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
                        }
                    }
                } else {
                    _logger.info("Could not execute 'findContainerItemsByParams'!");
                    if (currentRow.getAttribute("ToLocation") == null) {
                        // CLEAR THE FORMS FIELDS...
                        this.clearFormFields();
                    }
                    this.showMessagesPanel(ERROR,
                                           this.getMessage(PARTIAL_ENTRY, ERROR,
                                                           (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                           (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
                    this.addErrorStyleToComponent(this.getInputTextContainerId());
                    this.getIconContainerId().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getIconContainerId());
                }
            }

            _logger.fine("action value: " + action);
            _logger.info("detailAction() End");
        }
        return action;
    }

    public String exitAction() {
        _logger.info("exitAction() Start");
        // F3
        String action = null;
        String result =
            this.callCheckScreenOptionPriv(F3_ITEM_KEY, RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
        if (!"OFF".equals(result)) {
            HhMoveInventorySBean bean = this.getHhMovePageFlowBean();
            if (bean != null) {
                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("exitMoveInventory");
                oper.getParamsMap().put("moveContainerId", bean.getMoveContainerId());
                List<String> errorsMessages = (List<String>) oper.execute();
                if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                    if (errorsMessages != null && !errorsMessages.isEmpty()) {
                        this.getHhMovePageFlowBean().setIsError(true);
                        this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                    } else
                        _logger.info("Unable to execute 'exitMoveInventory'!");
                    action = this.logoutExitBTF();
                    this.hideMessagesPanel();
                    
                    if ("TQ".equals(getHhMovePageFlowBean().getPmCallingForm())) { //return to Task Queue page 1
                       popTaskFlow(3);                          
                       return null;
                    }
                }
            }
        }
        _logger.fine("action value: " + action);
        _logger.info("exitAction() End");
        return action;
    }

    public void doneActionListener(ActionEvent actionEvent) {
        _logger.info("doneActionListener() Start");
        // F4
        if (!this.getHhMovePageFlowBean().isIsError()) {
            String result =
                this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F4,
                                               RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
            if (!"OFF".equals(result)) {
                /* if (this.checkFields()) {
                    this.doneMoveInventory();
                } */
                this.checkFields(true);
            }
        }
        _logger.info("doneActionListener() End");
    }

    private void doneMoveInventory() {
        _logger.info("doneMoveInventory() Start");
        this.removeErrorStyleToComponent(this.getInputTextContainerId());
        this.removeErrorStyleToComponent(this.getInputTextToLocation());
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callDoneMoveInventory");
        List<String> errorsMessages = (List<String>) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (errorsMessages != null && !errorsMessages.isEmpty()) {
                if (errorsMessages.contains(this.logoutExitBTF())) {
                    ADFUtils.invokeAction(this.logoutExitBTF());
                } else if (errorsMessages.contains("PENDING_TASK")) {
                    _logger.info("Unable to call 'callDoneMoveInventory'! : PENDING_TASK");
                    this.disableAllFields();
                    this.getPendingTaskMessage().setMessage(errorsMessages.get(1));
                    this.getPendingTaskPopup().show(new RichPopup.PopupHints());
                } else if (errorsMessages.contains("EMPTY_LOCATION")) {
                    _logger.info("Unable to call 'callDoneMoveInventory'! : EMPTY_LOCATION");
                    this.getHhMovePageFlowBean().setIsError(true);
                    this.getInputTextToLocation().setDisabled(false);
                    this.addErrorStyleToComponent(this.getInputTextToLocation());
                    this.getIconToLocation().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getIconToLocation());
                    this.selectTextOnUIComponent(this.getInputTextToLocation());
                } else {
                    this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));

                    if (MSGTYPE_M.equals(errorsMessages.get(0))) {
                        // MOVE SUCCESS
                        this.getHhMovePageFlowBean().setIsError(false);
                        this.getInputTextToLocation().resetValue();
                        this.getInputTextToLocation().setDisabled(true);
                        this.getInputTextContainerId().resetValue();
                        this.getInputTextContainerId().setDisabled(false);
                        this.setFocusContainerId();
                    } else {
                        _logger.info("Unable to call 'callDoneMoveInventory'! : PENDING_TASK");
                        this.getHhMovePageFlowBean().setIsError(true);
                        this.getInputTextToLocation().setDisabled(false);
                        this.addErrorStyleToComponent(this.getInputTextToLocation());
                        this.getIconToLocation().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getIconToLocation());
                        this.selectTextOnUIComponent(this.getInputTextToLocation());
                    }
                    this.refreshContentOfUIComponent(this.getMoveInventoryPanel());
                }
            } else {
                this.hideMessagesPanel();
            }
        }
        _logger.info("doneMoveInventory() End");
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        _logger.info("performKeyPopup() Start");
        _logger.fine("clientEvent value: " + clientEvent);

        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.getConfirmPopup().hide();
                this.confirmationDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.confirmationDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.no));
                this.getConfirmPopup().hide();
            }
        }
        _logger.info("performKeyPopup() End");
    }

    public void performKeyPopup2(ClientEvent clientEvent) {
        _logger.info("performKeyPopup2() Start");
        _logger.fine("clientEvent value: " + clientEvent);
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.getPendingTaskPopup().hide();
                this.pendingTaskDialogListener(new DialogEvent(this.getConfirmDialog2(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.getPendingTaskPopup().hide();
                this.pendingTaskDialogListener(new DialogEvent(this.getConfirmDialog2(), DialogEvent.Outcome.no));
            }
        }
        _logger.info("performKeyPopup2() End");
    }

    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmPopup().hide();
        this.confirmationDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmPopup().hide();
        this.confirmationDialogListener(new DialogEvent(this.getConfirmDialog(), DialogEvent.Outcome.no));
    }


    public void yesLinkPopup2ActionListener(ActionEvent actionEvent) {
        this.getPendingTaskPopup().hide();
        this.pendingTaskDialogListener(new DialogEvent(this.getConfirmDialog2(), DialogEvent.Outcome.yes));
    }

    public void noLinkPopup2ActionListener(ActionEvent actionEvent) {
        this.getPendingTaskPopup().hide();
        this.pendingTaskDialogListener(new DialogEvent(this.getConfirmDialog2(), DialogEvent.Outcome.no));
    }

    public void pendingTaskDialogListener(DialogEvent dialogEvent) {
        _logger.info("pendingTaskDialogListener() Start");
        _logger.fine("dialogEvent value: " + dialogEvent);
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("processMove");
            List<String> errorsMessages = (List<String>) oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                if (errorsMessages != null && !errorsMessages.isEmpty()) {
                    if (errorsMessages.contains(this.logoutExitBTF())) {
                        ADFUtils.invokeAction(this.logoutExitBTF());
                    } else if (errorsMessages.contains("EMPTY_LOCATION")) {
                        _logger.info("Unable to execute 'processMove'! : EMPTY LOCATION");
                        this.getHhMovePageFlowBean().setIsError(true);
                        this.addErrorStyleToComponent(this.getInputTextToLocation());
                        this.getIconToLocation().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getIconToLocation());
                        this.getInputTextToLocation().setDisabled(false);
                        this.refreshContentOfUIComponent(this.getInputTextToLocation());
                        this.setFocusToLocation();
                    } else {
                        this.getHhMovePageFlowBean().setIsError(false);
                        this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                        this.getInputTextToLocation().setDisabled(true);
                        this.getInputTextContainerId().setDisabled(false);
                        this.refreshContentOfUIComponent(this.getInputTextContainerId());
                        this.refreshContentOfUIComponent(this.getInputTextToLocation());
                        this.setFocusContainerId();
                    }
                } else {
                    this.hideMessagesPanel();
                }
                // this.refreshContentOfUIComponent(this.getMoveInventoryPanel());
            }
        } else {
            this.getInputTextToLocation().setDisabled(false);
            this.getInputTextContainerId().setDisabled(true);
            this.refreshContentOfUIComponent(this.getInputTextContainerId());
            this.refreshContentOfUIComponent(this.getInputTextToLocation());
            this.selectTextOnUIComponent(this.getInputTextToLocation());
        }
        _logger.info("pendingTaskDialogListener() End");

    }

    private HhMoveInventorySBean getHhMovePageFlowBean() {
        return (HhMoveInventorySBean) this.getPageFlowBean("HhMoveInventorySBean");
    }

    public void clearActionListener(ActionEvent actionEvent) {
        _logger.info("clearActionListener() Start");
        // F5
        String result =
            this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F5,
                                           RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
        if (!"OFF".equals(result)) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("clearMoveInventory");
            oper.getParamsMap().put("moveContainerId", this.getHhMovePageFlowBean().getMoveContainerId());
            List<String> errorsMessages = (List<String>) oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                if (errorsMessages != null && !errorsMessages.isEmpty()) {
                    _logger.info("Unable to execute 'clearMoveInventory'!");
                    this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                } else {
                    // CLEAR THE FORMS FIELDS...
                    this.getInputTextToLocation().setDisabled(true);
                    this.getInputTextContainerId().setDisabled(false);
                    this.getInputTextContainerId().resetValue();
                    this.getInputTextToLocation().resetValue();
                    this.removeErrorStyleToComponent(this.getInputTextContainerId());
                    this.getIconContainerId().setName(REQ_ICON);
                    this.removeErrorStyleToComponent(this.getInputTextToLocation());
                    this.getIconToLocation().setName(REQ_ICON);
                    this.clearFormFields();

                    this.setFocusContainerId();
                }
            }
        }
        _logger.info("clearActionListener() End");
    }

    public String waveAction() {
        _logger.info("waveAction() Start");
        // F6
        String action = null;
        if (!this.getHhMovePageFlowBean().isIsError() || this.getHhMovePageFlowBean().getIsWavedContainer()) {
            Row currentRow = this.getCurrentMoveInventoryRow();
          if (currentRow != null) {
            boolean validateOK =
                this.validateRequiredField(currentRow.getAttribute("ContainerId"), this.getInputTextContainerId());
            if (validateOK) {

                    String result =
                        this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F6,
                                                       RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
                    if (!"OFF".equals(result)) {
                        OperationBinding oper =
                            (OperationBinding) ADFUtils.findOperation("findWaveInformationByParams");
                        oper.getParamsMap().put("facilityId", currentRow.getAttribute(ATTR_FACILITY_ID));
                        oper.getParamsMap().put("containerId", currentRow.getAttribute("ContainerId"));
                        Long contIsWaved = (Long) oper.execute();
                        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                            if (contIsWaved != null && contIsWaved.longValue() > 0) {
                                action = "getWaveLabel";
                                this.hideMessagesPanel();
                            } else {
                                _logger.info("Unable to execute 'findWaveInformationByParams'! : NO_DATA");
                                this.showMessagesPanel(WARN,
                                                       this.getMessage("NO_DATA", WARN,
                                                                       (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                                       (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
                                if (this.getInputTextContainerId().isDisabled()) {
                                    this.setFocusToLocation();
                                } else {
                                    this.setFocusContainerId();
                                }
                            }
                        }
                    }
                }
             else {
                _logger.info("ContainerId validation failed! : PARTIAL");
                this.getIconContainerId().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconContainerId());
                this.showMessagesPanel(ERROR,
                                       this.getMessage(PARTIAL_ENTRY, ERROR,
                                                       (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                       (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
                this.setFocusContainerId();
            }
            _logger.fine("action value: " + action);
            _logger.info("waveAction() End");
        }
        }
        return action;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Move Start");
        if (!this.getHhMovePageFlowBean().isIsPage1Load()) {
            this.callStartUpLocalSecurity();
            this.getHhMovePageFlowBean().setIsPage1Load(true);
        }
        
        if(StringUtils.isNotEmpty(this.getHhMovePageFlowBean().getInitErrorMsg())){
            String errorType = this.getHhMovePageFlowBean().getInitErrorType();
            String errorMsg = this.getHhMovePageFlowBean().getInitErrorMsg();
            this.getHhMovePageFlowBean().setInitErrorMsg(null);
            this.getHhMovePageFlowBean().setInitErrorType(null);
            this.showMessagesPanel(errorType, errorMsg);
            
            this.addErrorStyleToComponent(this.getInputTextContainerId());
            this.getIconContainerId().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconContainerId());
            this.setFocusOnUIComponent(this.getInputTextContainerId());
        }
        else{
            if (this.getHhMovePageFlowBean().isCheckWavedContainer() &&
                this.getHhMovePageFlowBean().getIsWavedContainer()) {
                this.getHhMovePageFlowBean().setCheckWavedContainer(false);
                Row currentRow = this.getCurrentMoveInventoryRow();
                String wavedMsg =
                    this.getMessage("CONT_WAVED", WARN, (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                    (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE));
                if (wavedMsg != null && !wavedMsg.isEmpty()) {
                    this.showMessagesPanel(ERROR, wavedMsg);
                }
            }
        }
        _logger.info("onRegionLoad Move End");
    }

    public String shutlAction() {
        String action = null;
        String result =
            this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F7,
                                           RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
        if (!"OFF".equals(result)) {
            action = "goShuttleTrailer";
        }
        return action;
    }

    public String markAction() {
        _logger.info("markAction() Start");
        // TODO -> F8
        String result =
            this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F8,
                                           RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
        if (!"OFF".equals(result)) {
            /*
            Call_Nautilus_Form ('hh_mark_for_cycle_count_s');
            AHL.SET_AHL_INFO(:WORK.FACILITY_ID, :SYSTEM.CURRENT_FORM);
            */
            this.getMarkForCycleCountPopup().show(new RichPopup.PopupHints());
        }
        _logger.info("markAction() End");
        return null;
    }


    private void callStartUpLocalSecurity() {
        _logger.info("callStartUpLocalSecurity() Start");
        Row currentRow = this.getCurrentMoveInventoryRow();
        if (currentRow != null) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callStartUpLocalSecurity");
            oper.getParamsMap().put("facilityId", currentRow.getAttribute(ATTR_FACILITY_ID));
            oper.getParamsMap().put("formName", FORM_NAME);
            oper.getParamsMap().put("userId", currentRow.getAttribute("UserId"));
            List<String> disabledItems = (List<String>) oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                if (disabledItems != null) {
                    for (String item : disabledItems) {
                        if ("MOVE_BLOCK.CONTAINER_ID".equals(item)) {
                            this.getInputTextContainerId().setDisabled(true);
                        } else if ("MOVE_BLOCK.TO_LOCATION".equals(item)) {
                            this.getInputTextToLocation().setDisabled(true);
                        }
                    }
                }
            }
        }
        _logger.info("callStartUpLocalSecurity() End");
    }

    private void setFocusContainerId() {
        this.getHhMovePageFlowBean().setDefaultFocusId("containerId");
        this.setFocusOnUIComponent(this.getInputTextContainerId());
    }

    private void setFocusToLocation() {
        this.getHhMovePageFlowBean().setDefaultFocusId("toLocation");
        this.setFocusOnUIComponent(this.getInputTextToLocation());
    }

    public void executeFunctionKey(ClientEvent clientEvent) {
        _logger.info("executeFunctionKey() Start");
        _logger.fine("clientEvent value: " + clientEvent);
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            boolean executeEvent = true;
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            // WE HAVE TO UPDATE THE MODEL...
            if (CONTAINER_ITEM.equals(item)) {
                ADFUtils.setBoundAttributeValue("ContainerId", submittedValue);
            } else {
                if (ADFUtils.getBoundAttributeValue("ContainerId") != null &&
                    !"".equals(ADFUtils.getBoundAttributeValue("ContainerId"))) {
                    ADFUtils.setBoundAttributeValue("ToLocation", submittedValue);
                } else {
                    executeEvent = false;
                }
            }

            if (executeEvent) {
                if (ENTER_KEY_CODE.equals(keyPressed)) {
                    if (CONTAINER_ITEM.equals(item)) {
                        this.validatorContainerId(submittedValue, false);
                    } else {
                        this.validatorToLocation(submittedValue, false);
                    }
                } else if (F2_KEY_CODE.equals(keyPressed)) {
                    ActionEvent actionEvent = new ActionEvent(this.getDetailLink());
                    actionEvent.queue();
                } else if (F3_KEY_CODE.equals(keyPressed)) {
                    ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                    actionEvent.queue();
                } else if (F4_KEY_CODE.equals(keyPressed)) {
                    String result =
                        this.callCheckScreenOptionPriv(RoleBasedAccessConstants.OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F4,
                                                       RoleBasedAccessConstants.FORM_NAME_hh_move_inventory_s);
                    if (!"OFF".equals(result)) {
                    if (CONTAINER_ITEM.equals(item)) {
                        this.validatorContainerId(submittedValue, true);
                    } else {
                        this.validatorToLocation(submittedValue, true);
                    }
                    }
                } else if (F5_KEY_CODE.equals(keyPressed)) {
                    this.clearActionListener(new ActionEvent(clientEvent.getComponent()));
                } else if (F6_KEY_CODE.equals(keyPressed)) {
                    ActionEvent actionEvent = new ActionEvent(this.getWaveLink());
                    actionEvent.queue();
                } else if (F7_KEY_CODE.equals(keyPressed)) {
                    ActionEvent actionEvent = new ActionEvent(this.getShutlLink());
                    actionEvent.queue();
                }
            }
        }
        _logger.info("executeFunctionKey() End");
    }

    public void setConfirmDialog(RichDialog confirmDialog) {
        this.confirmDialog = confirmDialog;
    }

    public RichDialog getConfirmDialog() {
        return confirmDialog;
    }

    public void setConfirmDialog2(RichDialog confirmDialog2) {
        this.confirmDialog2 = confirmDialog2;
    }

    public RichDialog getConfirmDialog2() {
        return confirmDialog2;
    }

    public void setShutlLink(RichLink shutlLink) {
        this.shutlLink = shutlLink;
    }

    public RichLink getShutlLink() {
        return shutlLink;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setIconContainerId(RichIcon iconContainerId) {
        this.iconContainerId = iconContainerId;
    }

    public RichIcon getIconContainerId() {
        return iconContainerId;
    }

    public void setIconToLocation(RichIcon iconToLocation) {
        this.iconToLocation = iconToLocation;
    }

    public RichIcon getIconToLocation() {
        return iconToLocation;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDetailLink(RichLink detailLink) {
        this.detailLink = detailLink;
    }

    public RichLink getDetailLink() {
        return detailLink;
    }

    public void setWaveLink(RichLink waveLink) {
        this.waveLink = waveLink;
    }

    public RichLink getWaveLink() {
        return waveLink;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmationMessage(RichMessage confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    public RichMessage getConfirmationMessage() {
        return confirmationMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setMoveInventoryPanel(RichPanelFormLayout moveInventoryPanel) {
        this.moveInventoryPanel = moveInventoryPanel;
    }

    public RichPanelFormLayout getMoveInventoryPanel() {
        return moveInventoryPanel;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setInputTextContainerId(RichInputText inputTextContainerId) {
        this.inputTextContainerId = inputTextContainerId;
    }

    public RichInputText getInputTextContainerId() {
        return inputTextContainerId;
    }

    public void setMarkForCycleCountPopup(RichPopup markForCycleCountPopup) {
        this.markForCycleCountPopup = markForCycleCountPopup;
    }

    public RichPopup getMarkForCycleCountPopup() {
        return markForCycleCountPopup;
    }

    public void setPendingTaskPopup(RichPopup pendingTaskPopup) {
        this.pendingTaskPopup = pendingTaskPopup;
    }

    public RichPopup getPendingTaskPopup() {
        return pendingTaskPopup;
    }

    public void setPendingTaskMessage(RichMessage pendingTaskMessage) {
        this.pendingTaskMessage = pendingTaskMessage;
    }

    public RichMessage getPendingTaskMessage() {
        return pendingTaskMessage;
    }

    public void setInputTextToLocation(RichInputText inputTextToLocation) {
        this.inputTextToLocation = inputTextToLocation;
    }

    public RichInputText getInputTextToLocation() {
        return inputTextToLocation;
    }
    
    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_hotelpicking_hhmoveinventorys_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
