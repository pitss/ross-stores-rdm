package com.ross.rdm.hotelpicking.hhmoveinventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.SelectionEvent;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMHotelPickingBackingBean {

    private final static String MOVE_INVENTORY_ITERATOR = "HhMoveInventorySMoveBlockViewIterator";
    private final static String FORM_NAME = "HH_MOVE_INVENTORY_S";
    private final static String F3_ITEM_KEY = "CONTAINER_ITEM.F3";
    static final String FIRST_RECORD = "FIRST RECORD";
    static final String LAST_RECORD = "LAST RECORD";
    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichOutputText errorMessage;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichTable itemTable;


    public Page2Backing() {

    }

    public String exitAction() {
        _logger.info("exitAction() Start");
        String action = null;
        Row currentRow = ADFUtils.findIterator(MOVE_INVENTORY_ITERATOR).getCurrentRow();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckScreenOptionPriv");
        oper.getParamsMap().put("facilityId", currentRow.getAttribute(ATTR_FACILITY_ID));
        oper.getParamsMap().put("userId", currentRow.getAttribute("UserId"));
        oper.getParamsMap().put("formName", FORM_NAME);
        oper.getParamsMap().put("optionName", F3_ITEM_KEY);
        String result = (String) oper.execute();
        if (oper.getErrors().isEmpty()) {
            if ("OFF".equals(result)) {
                this.showMessagesPanel(WARN,
                                       this.getMessage("NOT_ALLOWED", WARN,
                                                       (String) currentRow.getAttribute(ATTR_FACILITY_ID),
                                                       (String) currentRow.getAttribute(ATTR_LANGUAGE_CODE)));
            } else {
                HhMoveInventorySBean bean = (HhMoveInventorySBean) this.getPageFlowBean("HhMoveInventorySBean");
                if (currentRow.getAttribute("LocationId") == null) {
                    bean.setDefaultFocusId("containerId");
                } else {
                    bean.setDefaultFocusId("toLocation");
                }

                action = "backPage1";

                this.hideMessagesPanel();
            }
        }
        _logger.fine("action value: " + action);
        _logger.info("exitAction() End");
        return action;
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("showMessagesPanel() End");
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("hideMessagesPanel() End");
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        this.showFirstLastRecordInfo();
        _logger.info("onRegionLoad End");
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib = ADFUtils.findIterator("HhMoveInventorySContainerItemSQLViewIterator");
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == trow - 1) {
            this.showMessagesPanel(INFO, LAST_RECORD);
        } else if (crow == 0) {
            this.showMessagesPanel(INFO, FIRST_RECORD);
        } else {
            this.hideMessagesPanel();
        }
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        // Add event code here...
        JSFUtils.resolveMethodExpression("#{bindings.HhMoveInventorySContainerItemSQLView.collectionModel.makeCurrent}",
                                         SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                         selectionEvent });
        this.showFirstLastRecordInfo();
    }

    public void setItemTable(RichTable itemTable) {
        this.itemTable = itemTable;
    }

    public RichTable getItemTable() {
        return itemTable;
    }
}
