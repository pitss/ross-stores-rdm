package com.ross.rdm.hotelpicking.hhmoveinventorys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.common.NameMapping;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import java.util.List;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhMoveInventorySBean extends RDMHotelPickingPageFlowBean {

    @SuppressWarnings("compatibility:-1825281800000977375")
    private static final long serialVersionUID = 1L;
    private String moveContainerId; // Item/Parameter hh_move_inventory_s.MOVE_CONTAINER_ID
    private String defaultFocusId;
    private String isFocusOn;
    private boolean isWavedContainer;
    private boolean isError;
    private boolean isPage1Load;
    private boolean checkWavedContainer;
    private String  pmCallingForm;


    private String initErrorMsg;
    private String initErrorType;

    private static ADFLogger _logger = ADFLogger.createADFLogger(HhMoveInventorySBean.class);

    public void initTaskFlow() {
        _logger.info("initTaskFlow() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("initMoveInventory");
        oper.getParamsMap().put("moveContainerId", moveContainerId);
        List<String> errorsMessages = (List<String>) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            _logger.info("Could not execute 'initMoveInventory'!");
            if (errorsMessages != null && !errorsMessages.isEmpty()) {
                this.setInitErrorType(errorsMessages.get(0));
                this.setInitErrorMsg(errorsMessages.get(1));
            }
            if ("TQ".equals(getPmCallingForm()) && getMoveContainerId() != null) 
                this.setDefaultFocusId("toLocation");
            else
                this.setDefaultFocusId("inittask");
        }
        _logger.info("initTaskFlow() End");
    }

    public void setMoveContainerId(String moveContainerId) {
        this.moveContainerId = moveContainerId;
    }

    public String getMoveContainerId() {
        return moveContainerId;
    }

    public void setDefaultFocusId(String defaultFocusId) {
        this.defaultFocusId = defaultFocusId;
    }

    public String getDefaultFocusId() {
        return defaultFocusId;
    }

    public void setIsWavedContainer(boolean isWavedContainer) {
        this.isWavedContainer = isWavedContainer;
    }

    public boolean getIsWavedContainer() {
        return isWavedContainer;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }

    public boolean isIsError() {
        return isError;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsPage1Load(boolean isPage1Load) {
        this.isPage1Load = isPage1Load;
    }

    public boolean isIsPage1Load() {
        return isPage1Load;
    }
    
    public void setCheckWavedContainer(boolean checkWavedContainer) {
        this.checkWavedContainer = checkWavedContainer;
    }

    public boolean isCheckWavedContainer() {
        return checkWavedContainer;
    }
    
    public TaskFlowId getShuttleTrailerSTaskFlowId(){
        // return "/WEB-INF/com/ross/rdm/trailermgmt/hhshuttletrailers/view/taskflow/HhShuttleTrailerSTaskFlow.xml";
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_shuttle_trailer_s", "trailermgmt/"));
    }
    
    public void setInitErrorMsg(String initErrorMsg) {
        this.initErrorMsg = initErrorMsg;
    }

    public String getInitErrorMsg() {
        return initErrorMsg;
    }

    public void setInitErrorType(String initErrorType) {
        this.initErrorType = initErrorType;
    }

    public String getInitErrorType() {
        return initErrorType;
    }
    
    public void setPmCallingForm(String pmCallingForm) {
        this.pmCallingForm = pmCallingForm;
    }

    public String getPmCallingForm() {
        return pmCallingForm;
    }    
}
