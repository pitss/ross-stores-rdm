package com.ross.rdm.hotelpicking.hhpickacrosswaves.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import java.util.Date;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhPickAcrossWaveSBean extends RDMHotelPickingPageFlowBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhPickAcrossWaveSBean.class);
    private String pmAllowIntrlvg; // Item/Parameter hh_pick_across_wave_s.PM_ALLOW_INTRLVG
    private String cPickType; // Item/Parameter hh_pick_across_wave_s.C_PICK_TYPE
    private String cpPickType; // Item/Parameter hh_pick_across_wave_s.CP_PICK_TYPE
    private String bpPickType; // Item/Parameter hh_pick_across_wave_s.BP_PICK_TYPE
    private Date waveDate;
    private Boolean isStartPickLocValidated;
    private Boolean isPickTypeValidated;
    private Boolean isPickValidated;
    private Boolean isUpsValidated;
    private String isFocusOn;
    private Boolean dateValidated;
    private Boolean pickEnabled;

    public void initTaskFlowPickAcrossWave() {
        _logger.info("Pick Across Wave page initialization begun");
        this.initGlobalVariablesPickAcrossWave();
        this.initHhPickAcrossWaveWorkVariables();
        this.initHhPickAcrossWaveNoPickPackRow();
        this.setCPickType(null);
        this.setCpPickType(null);
        this.setBpPickType(null);
        this.setIsStartPickLocValidated(false);
        this.setIsPickTypeValidated(false);
        this.setIsUpsValidated(false);
        this.setIsPickValidated(true);
        this.setIsFocusOn("StartPickLocField");
        _logger.info("Pick Across Wave page initialization completed");
    }

    private void initGlobalVariablesPickAcrossWave() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesPickAcrossWave");
        oper.execute();
    }

    private void initHhPickAcrossWaveNoPickPackRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("CreateInsertNoPickPack");
        oper.execute();
    }

    private void initHhPickAcrossWaveWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhPickAcrossWaveWorkVariables");
        oper.execute();
    }

    public void setPickAcrossWaveNoPickPackCurrentRow() {
        ADFUtils.findOperation("setPickAcrossWaveNoPickPackCurrentRow").execute();
    }

    public void setPmAllowIntrlvg(String pmAllowIntrlvg) {
        this.pmAllowIntrlvg = pmAllowIntrlvg;
    }

    public String getPmAllowIntrlvg() {
        return pmAllowIntrlvg;
    }

    public void setCPickType(String cPickType) {
        this.cPickType = cPickType;
    }

    public String getcPickType() {
        return cPickType;
    }

    public void setCpPickType(String cpPickType) {
        this.cpPickType = cpPickType;
    }

    public String getCpPickType() {
        return cpPickType;
    }

    public void setBpPickType(String bpPickType) {
        this.bpPickType = bpPickType;
    }

    public String getBpPickType() {
        return bpPickType;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setWaveDate(Date waveDate) {
        this.waveDate = waveDate;
    }

    public Date getWaveDate() {
        return waveDate;
    }

    public void setDateValidated(Boolean dateValidated) {
        this.dateValidated = dateValidated;
    }

    public Boolean getDateValidated() {
        return dateValidated;
    }

    public void setPickEnabled(Boolean pickEnabled) {
        this.pickEnabled = pickEnabled;
    }

    public Boolean getPickEnabled() {
        return pickEnabled;
    }

    public void setIsStartPickLocValidated(Boolean isStartPickLocValidated) {
        this.isStartPickLocValidated = isStartPickLocValidated;
    }

    public Boolean getIsStartPickLocValidated() {
        return isStartPickLocValidated;
    }

    public void setIsPickTypeValidated(Boolean isPickTypeValidated) {
        this.isPickTypeValidated = isPickTypeValidated;
    }

    public Boolean getIsPickTypeValidated() {
        return isPickTypeValidated;
    }

    public void setIsPickValidated(Boolean isPickValidated) {
        this.isPickValidated = isPickValidated;
    }

    public Boolean getIsPickValidated() {
        return isPickValidated;
    }

    public void setIsUpsValidated(Boolean isUpsValidated) {
        this.isUpsValidated = isUpsValidated;
    }

    public Boolean getIsUpsValidated() {
        return isUpsValidated;
    }
}
