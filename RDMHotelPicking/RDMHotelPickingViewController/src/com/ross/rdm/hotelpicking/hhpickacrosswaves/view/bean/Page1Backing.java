package com.ross.rdm.hotelpicking.hhpickacrosswaves.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMHotelPickingBackingBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(RDMHotelPickingBackingBean.class);
    private final static String START_PICK_LOC_ATTR = "StartPickLoc";
    private final static String PICK_TYPE_ATTR = "PickType";
    private final static String PICK_ATTR = "Pick";
    private final static String UNIT_PICK_SYSTEM_CODE_ATTR = "UnitPickSystemCode";
    private final static String WAVE_DATE_ATTR = "WaveDate";
    private final static String HH_PICK_ACROSS_WAVE_FLOW_BEAN = "HhPickAcrossWaveSBean";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String parameterCPickType = "C";
    private final static String parameterCPPickType = "CP";
    private final static String parameterBPPickType = "BP";
    private String parameterPmAllowIntrlvg = "";

    private RichInputText waveDateNoPickPack;
    private RichPanelFormLayout pickAcrossWavePanel;
    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichOutputText errorMessage;
    private RichInputText startPickLocNoPickPack;
    private RichInputText pickTypeNoPickPack;
    private RichInputText unitPickSystemCodeNoPickPack;
    private RichInputText pickNoPickPack;
    private RichPopup confirmIntrlvgPopup;
    private RichOutputText dialogConfirmIntrlvgOutputtext;
    private RichDialog confirmIntrlvgDialog;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichIcon startPickLocIcon;
    private RichIcon pickTypeIcon;
    private RichIcon upsIcon;
    private RichIcon waveDateIcon;
    private RichIcon pickIcon;
    private RichInputText waveDateString;
    private String f4SubmittedValue;

    public Page1Backing() {

    }

    public boolean onChangedStartPickLocNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedStartPickLocNoPickPack() Start");
        if (!this.isCurrentTaskFlowActive())
            return false;
        if (null != this.getHhPickAcrossWaveSBeanPageFlowBean()) {
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsStartPickLocValidated(false);
        }
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String startPickLoc = ((String) vce.getNewValue());
            return this.callVStartPickLoc2(startPickLoc);
        }
        _logger.info("onChangedStartPickLocNoPickPack() End");
        return true;
    }

    private boolean callVStartPickLoc2(String startPickLoc) {
        _logger.info("callVStartPickLoc2() Start");
        if (startPickLoc != null)
            startPickLoc = startPickLoc.toUpperCase();
        String facilityId = getFacilityIdAttrValue();

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVStartPickLoc2");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("startPickLoc", startPickLoc);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(codeList.get(0))) {
                    String msg = codeList.get(1);
                    this.showErrorPanel(msg);
                } else {
                    this.showErrorPanel(codeList.get(0));
                }
                this.getStartPickLocIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getStartPickLocIcon());
                this.refreshContentOfUIComponent(this.getStartPickLocNoPickPack());
                this.addErrorStyleToComponent(this.getStartPickLocNoPickPack());
                this.getHhPickAcrossWaveSBeanPageFlowBean().setIsStartPickLocValidated(false);
                this.setFocusStartPickLoc();
                this.selectTextOnUIComponent(this.getStartPickLocNoPickPack());
                _logger.info("callVStartPickLoc2() End");
                return false;
            } else {
                this.getHhPickAcrossWaveSBeanPageFlowBean().setIsStartPickLocValidated(true);
                ADFUtils.setBoundAttributeValue(START_PICK_LOC_ATTR, startPickLoc);
                this.getStartPickLocIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getStartPickLocIcon());
                this.hideMessagesPanel();
                this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("PickTypeField");
                this.removeErrorStyleToComponent(this.getStartPickLocNoPickPack());
                this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
            }
        }
        _logger.info("callVStartPickLoc2() End");
        return true;
    }

    public boolean onChangedPickTypeNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedPickTypeNoPickPack() Start");
        if (!this.isCurrentTaskFlowActive())
            return false;
        if (null != this.getHhPickAcrossWaveSBeanPageFlowBean()) {
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsPickTypeValidated(false);
        }
        if (null != vce.getNewValue()) {
            String enteredPickTypeValue = ((String) vce.getNewValue());
            return this.validatePickType(enteredPickTypeValue);
        }
        _logger.info("onChangedPickTypeNoPickPack() End");
        return true;
    }

    private boolean validatePickType(String pickType) {
        _logger.info("validatePickType() Start");
        if (pickType != null)
            pickType = pickType.toUpperCase();
        List<String> pickTypes = new ArrayList<String>(Arrays.asList("B", "BP", "BR", "C", "CR", "CP"));
        if (!pickTypes.contains(pickType)) {
            this.getPickTypeIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getPickTypeIcon());
            this.showErrorPanel(this.getMessage("INV_PICK_TYPE", ERROR, this.getFacilityIdAttrValue(),
                                                this.getLangCodeAttrValue()));
            this.addErrorStyleToComponent(this.getPickTypeNoPickPack());
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsPickTypeValidated(false);
            this.setFocusPickType();
            this.selectTextOnUIComponent(this.getPickTypeNoPickPack());
            _logger.info("validatePickType() End");
            return false;
        } else {
            String l_choose_pick = getLChoosePickValue();

            if (pickType.toString().equals("BP") && l_choose_pick.toString().equals(YES)) {
                this.getHhPickAcrossWaveSBeanPageFlowBean().setPickEnabled(true);
                this.getPickIcon().setName(REQ_ICON);
            } else {
                this.getHhPickAcrossWaveSBeanPageFlowBean().setPickEnabled(false);
                this.getPickIcon().setName("");
                ADFUtils.setBoundAttributeValue(PICK_ATTR, "ALL");
            }
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsPickTypeValidated(true);
            ADFUtils.setBoundAttributeValue(PICK_TYPE_ATTR, pickType);
            this.getPickTypeIcon().setName(REQ_ICON);
            this.refreshContentOfUIComponent(this.getPickTypeIcon());
            this.hideMessagesPanel();
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("UpsField");
            this.removeErrorStyleToComponent(this.getPickTypeNoPickPack());
            this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
        }
        _logger.info("validatePickType() End");
        return true;
    }

    private String getLChoosePickValue() {
        _logger.info("getLChoosePickValue() Start");
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String startPickLoc = (String) ADFUtils.getBoundAttributeValue("StartPickLoc");

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callGetWhId");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("startPickLoc", startPickLoc);
        oper.execute();
        String wh_id = (String) oper.getResult();

        OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("getGscpWh");
        Map map2 = oper2.getParamsMap();
        map2.put("facilityId", facilityId);
        map2.put("whId", wh_id);
        String l_choose_pick = (String) oper2.execute();
        _logger.info("getLChoosePickValue() End");
        return l_choose_pick;
    }

    public boolean onChangedUnitPickSystemCodeNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedUnitPickSystemCodeNoPickPack() Start");
        if (!this.isCurrentTaskFlowActive())
            return false;
        String pickType = (String) ADFUtils.getBoundAttributeValue(PICK_TYPE_ATTR);
        if (!pickType.equalsIgnoreCase("C")) {
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsUpsValidated(false);
            if (null != vce.getNewValue()) {
                String enteredUnitPickSystemCode = ((String) vce.getNewValue());
                return this.callVUpsCode(enteredUnitPickSystemCode);
            }
        } else {
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsUpsValidated(true);
        }        
        _logger.info("onChangedUnitPickSystemCodeNoPickPack() End");
        return true;
    }

    private boolean callVUpsCode(String unitPickSystemCode) {
        _logger.info("callVUpsCode() Start");
        if (unitPickSystemCode != null)
            unitPickSystemCode = unitPickSystemCode.toUpperCase();
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String i_pick_type = (String) ADFUtils.getBoundAttributeValue("PickType");

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVUpsCode");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("i_pick_type", i_pick_type);
        map.put("i_unit_pick_system_code", unitPickSystemCode);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (oper.getResult() != null) {
                List<String> codeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(codeList.get(0))) {
                    this.showErrorPanel(codeList.get(1));
                } else {
                    this.showErrorPanel(codeList.get(0));
                }
                this.getUpsIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getUpsIcon());
                this.selectTextOnUIComponent(this.getUnitPickSystemCodeNoPickPack());
                this.addErrorStyleToComponent(this.getUnitPickSystemCodeNoPickPack());
                this.setFocusUnitPickSystem();
                this.getHhPickAcrossWaveSBeanPageFlowBean().setIsUpsValidated(false);
                _logger.info("callVUpsCode() End");
                return false;
            } else {
                this.getHhPickAcrossWaveSBeanPageFlowBean().setIsUpsValidated(true);
                ADFUtils.setBoundAttributeValue(UNIT_PICK_SYSTEM_CODE_ATTR, unitPickSystemCode);
                this.hideMessagesPanel();
                this.getUpsIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getUpsIcon());
                this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("WaveDateField");
                this.removeErrorStyleToComponent(this.getUnitPickSystemCodeNoPickPack());
                this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
            }
        }
        _logger.info("callVUpsCode() End");
        return true;
    }

    public void onChangedWaveDateString(ValueChangeEvent vce) {
        if (!this.isCurrentTaskFlowActive())
            return;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredDateValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateWaveDate(enteredDateValue);
        } else {
            this.hideMessagesPanel();
            this.getWaveDateIcon().setName("");
            this.refreshContentOfUIComponent(this.getWaveDateIcon());
            this.removeErrorStyleToComponent(this.getWaveDateString());
            this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
            this.getHhPickAcrossWaveSBeanPageFlowBean().setDateValidated(true);
        }
    }

    public void onChangedWaveDateNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedWaveDateNoPickPack() Start");
        if (!this.isCurrentTaskFlowActive())
            return;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredDateValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateWaveDate(enteredDateValue);
        } else {
            this.hideMessagesPanel();
            this.getWaveDateIcon().setName("");
            this.refreshContentOfUIComponent(this.getWaveDateIcon());
            this.removeErrorStyleToComponent(this.getWaveDateString());
            this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
            this.getHhPickAcrossWaveSBeanPageFlowBean().setDateValidated(true);
        }
        _logger.info("onChangedWaveDateNoPickPack() End");
    }

    private boolean validateWaveDate(String enteredDateValue) {
        _logger.info("validateWaveDate() Start");
        if (enteredDateValue == null || enteredDateValue.equals("")) {
            this.getHhPickAcrossWaveSBeanPageFlowBean().setDateValidated(null);
            _logger.info("validateWaveDate() End");
            return false;
        }
        Date waveDate = validateWaveDateType(enteredDateValue);
        if (waveDate != null) {
            java.sql.Date sqlStartDate = new java.sql.Date(waveDate.getTime());
            ADFUtils.setBoundAttributeValue(WAVE_DATE_ATTR, sqlStartDate);
            String pickType = (String) ADFUtils.getBoundAttributeValue("PickType");
            String l_choose_pick = getLChoosePickValue();

            if (pickType.toString().equalsIgnoreCase("BP") && l_choose_pick.toString().equalsIgnoreCase(YES)) {
                this.getHhPickAcrossWaveSBeanPageFlowBean().setPickEnabled(true);
                this.getPickIcon().setName(REQ_ICON);
            } else {
                this.getHhPickAcrossWaveSBeanPageFlowBean().setPickEnabled(false);
                this.getPickIcon().setName("");
            }

            this.hideMessagesPanel();
            this.getWaveDateIcon().setName("");
            this.refreshContentOfUIComponent(this.getWaveDateIcon());
            this.removeErrorStyleToComponent(this.getWaveDateString());
            this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
            this.getHhPickAcrossWaveSBeanPageFlowBean().setDateValidated(true);
            _logger.info("validateWaveDate() End");
            return true;
        }
        this.getHhPickAcrossWaveSBeanPageFlowBean().setDateValidated(false);
        _logger.info("validateWaveDate() End");
        return false;
    }

    private Date validateWaveDateType(String inputWaveDate) {
        _logger.info("validateWaveDateType() Start");
        Date waveDate = null;
        if (inputWaveDate.length() == 10 && this.checkNumeric(inputWaveDate)) { 
            String[] knownPatterns = { "MM/dd/yyyy", "MM-dd-yyyy" };
            for (String pattern : knownPatterns) {
                SimpleDateFormat sdf = new SimpleDateFormat(pattern);
                sdf.setLenient(false);
                try {
                    waveDate = sdf.parse(inputWaveDate); 
                    break;
                } catch (Exception e) { 
                    this.showWaveDateError();
                }
            }
        } else
            this.showWaveDateError();
        _logger.info("validateWaveDateType() End");
        return waveDate;
    }

    private boolean checkNumeric(String date) {
        String[] tokens = date.split("-|\\/");
        if (tokens.length == 3) {
            return this.isNumeric(tokens[0]) && this.isNumeric(tokens[1]) && this.isNumeric(tokens[2]);
        } else
            return false;
    }

    public boolean isNumeric(String s) {
        return java.util.regex.Pattern.matches("\\d+", s);
    }

    private void showWaveDateError() {
        String message =
            this.getMessage("INV_WAVE_DATE", ERROR, this.getFacilityIdAttrValue(), this.getLangCodeAttrValue());
        this.showErrorPanel(message);
        this.getWaveDateIcon().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getWaveDateIcon());
        this.selectTextOnUIComponent(this.getWaveDateString());
        this.addErrorStyleToComponent(this.getWaveDateString());
        this.setFocusWaveDate();
    }

    public boolean onChangedPickNoPickPack(ValueChangeEvent vce) {
        _logger.info("onChangedPickNoPickPack() Start");
        if (!this.isCurrentTaskFlowActive())
            return false;
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsPickValidated(false);
        if (null != vce.getNewValue()) {
            String enteredPickValue = ((String) vce.getNewValue());
            return this.validatePick(enteredPickValue);
        }
        _logger.info("onChangedPickNoPickPack() End");
        return true;
    }

    private boolean validatePick(String pick) {
        _logger.info("validatePick() Start");
        if (null != pick)
            pick = pick.toUpperCase();
        List<String> pickValues = new ArrayList<String>(Arrays.asList("C", "U", "ALL"));
        if (null == pick || !pickValues.contains(pick)) {
            this.showErrorPanel(this.getMessage("INV_PICK", ERROR, this.getFacilityIdAttrValue(),
                                                this.getLangCodeAttrValue()));
            this.getPickIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getPickIcon());
            this.selectTextOnUIComponent(this.getPickNoPickPack());
            this.addErrorStyleToComponent(this.getPickNoPickPack());
            this.setFocusPick();
            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsPickValidated(false);
            _logger.info("validatePick() End");
            return false;
        }
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsPickValidated(true);
        ADFUtils.setBoundAttributeValue(PICK_ATTR, pick);
        this.hideMessagesPanel();
        this.getPickIcon().setName(REQ_ICON);
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("StartPickLocField");
        this.refreshContentOfUIComponent(this.getPickIcon());
        this.removeErrorStyleToComponent(this.getPickNoPickPack());
        this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
        _logger.info("validatePick() End");
        return true;
        //todo work with sql exception, see fmb
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");

        if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F4".equalsIgnoreCase(key)) {

            String startPickLoc = (String) ADFUtils.getBoundAttributeValue(START_PICK_LOC_ATTR);
            String pickType = (String) ADFUtils.getBoundAttributeValue(PICK_TYPE_ATTR);
            String unitPickSystemCode = (String) ADFUtils.getBoundAttributeValue(UNIT_PICK_SYSTEM_CODE_ATTR);
            Date waveDate = (Date) ADFUtils.getBoundAttributeValue(WAVE_DATE_ATTR);
            String pickBound = (String) ADFUtils.getBoundAttributeValue(PICK_ATTR);
            boolean passedValidation = false;

            String currentFocus = this.getHhPickAcrossWaveSBeanPageFlowBean().getIsFocusOn();
            String tempFocus = currentFocus;
            if ("StartPickLocField".equals(currentFocus)) {
                if ((this.getF4SubmittedValue() == null || this.getF4SubmittedValue().isEmpty()) &&
                    null == startPickLoc) {
                    this.showErrorPanel(this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                        this.getLangCodeAttrValue()));
                    this.getStartPickLocIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getStartPickLocIcon());
                    this.addErrorStyleToComponent(this.getStartPickLocNoPickPack());
                    this.setFocusStartPickLoc();
                    this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
                    return;
                } else {
                    passedValidation =
                        this.onChangedStartPickLocNoPickPack(new ValueChangeEvent(this.getStartPickLocNoPickPack(),
                                                                                  null,
                                                                                  this.getStartPickLocNoPickPack().getValue()));
                    startPickLoc = (String) ADFUtils.getBoundAttributeValue(START_PICK_LOC_ATTR);
                    currentFocus = this.getHhPickAcrossWaveSBeanPageFlowBean().getIsFocusOn();
                    this.setF4SubmittedValue(null);
                }
            }
            if ("PickTypeField".equals(currentFocus)) {
                if ((this.getF4SubmittedValue() == null || this.getF4SubmittedValue().isEmpty()) && null == pickType) {
                    this.showErrorPanel(this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                        this.getLangCodeAttrValue()));
                    this.getPickTypeIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getPickTypeIcon());
                    this.addErrorStyleToComponent(this.getPickTypeNoPickPack());
                    this.setFocusPickType();
                    this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
                    return;
                } else {
                    passedValidation =
                        this.onChangedPickTypeNoPickPack(new ValueChangeEvent(this.getPickTypeNoPickPack(), null,
                                                                              this.getPickTypeNoPickPack().getValue()));
                    pickType = (String) ADFUtils.getBoundAttributeValue(PICK_TYPE_ATTR);
                    currentFocus = this.getHhPickAcrossWaveSBeanPageFlowBean().getIsFocusOn();
                    this.setF4SubmittedValue(null);
                }
            }
            if ("UpsField".equals(currentFocus)) {
                if (!pickType.equalsIgnoreCase("C")) {
                    if ((this.getF4SubmittedValue() == null || this.getF4SubmittedValue().isEmpty()) &&
                        null == unitPickSystemCode) {
                        this.showErrorPanel(this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                            this.getLangCodeAttrValue()));
                        this.getUpsIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getUpsIcon());
                        this.addErrorStyleToComponent(this.getUnitPickSystemCodeNoPickPack());
                        this.setFocusUnitPickSystem();
                        this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
                        return;
                    } else {
                        passedValidation =
                            this.onChangedUnitPickSystemCodeNoPickPack(new ValueChangeEvent(this.getUnitPickSystemCodeNoPickPack(),
                                                                                            null,
                                                                                            this.getUnitPickSystemCodeNoPickPack().getValue()));
                        unitPickSystemCode = (String) ADFUtils.getBoundAttributeValue(UNIT_PICK_SYSTEM_CODE_ATTR);
                        currentFocus = this.getHhPickAcrossWaveSBeanPageFlowBean().getIsFocusOn();
                        this.setF4SubmittedValue(null);
                    }
                } else {
                    passedValidation = true;
                }

            }
            if ("WaveDateField".equals(currentFocus)) {
                if (waveDate == null) {
                    passedValidation = true;
                    if (null != this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                        if (!this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                            return;
                        }
                    }
                    pickBound = (String) ADFUtils.getBoundAttributeValue(PICK_ATTR);
                    if (this.getHhPickAcrossWaveSBeanPageFlowBean().getPickEnabled() != null &&
                        this.getHhPickAcrossWaveSBeanPageFlowBean().getPickEnabled() &&
                        (pickBound == null || pickBound.isEmpty())) {
                        this.showErrorPanel(this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                            this.getLangCodeAttrValue()));
                        this.getPickIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getPickIcon());
                        this.addErrorStyleToComponent(this.getPickNoPickPack());
                        this.setFocusPick();
                        this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
                        return;
                    } else {
                        this.onChangedWaveDateString(new ValueChangeEvent(this.getWaveDateString(), null,
                                                                          this.getWaveDateString().getValue())); 
                        
                        if (null != this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                            if (!this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                                return;
                            }
                        }
                    }
                } else {
                    if (null != this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                        if (!this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                            return;
                        } else {
                            passedValidation = true;
                        }
                    }
                }
            }
            if ("PickField".equals(currentFocus)) {
                passedValidation =
                    this.onChangedPickNoPickPack(new ValueChangeEvent(this.getPickNoPickPack(), null,
                                                                      this.getPickNoPickPack().getValue()));
                pickBound = (String) ADFUtils.getBoundAttributeValue(PICK_ATTR);
                if (pickBound == null || pickBound.isEmpty()) {
                    this.showErrorPanel(this.getMessage(PARTIAL_ENTRY, ERROR, this.getFacilityIdAttrValue(),
                                                        this.getLangCodeAttrValue()));
                    this.getPickIcon().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getPickIcon());
                    this.addErrorStyleToComponent(this.getPickNoPickPack());
                    this.setFocusPick();
                    this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
                    return;
                }
            }

            if (!passedValidation ||
                (null != this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated() &&
                 this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated() == false)) {
                return;
            }

            this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn(tempFocus);
            if (startPickLoc != null && !startPickLoc.isEmpty() && pickType != null && !pickType.isEmpty() &&
                unitPickSystemCode != null && !unitPickSystemCode.isEmpty()) {
                String l_choose_pick = getLChoosePickValue();
                String pick = getPickValue();
                if (pickType.toString().equals("BP") && l_choose_pick.toString().equals(YES)) {
                    validatePick(pick);
                    this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn(tempFocus);
                    HhPickAcrossWaveSBean pickAcrossWaveSBean =
                        (HhPickAcrossWaveSBean) this.getPageFlowBean("HhPickAcrossWaveSBean");
                    if (null == pick || pick.isEmpty()) {
                        //todo call msg window... see fmb


                    } else if (pick.equalsIgnoreCase("C")) {
                        pickAcrossWaveSBean.setCPickType("C");
                        pickAcrossWaveSBean.setCpPickType("~");
                        pickAcrossWaveSBean.setBpPickType("~");
                    } else if (pick.equalsIgnoreCase("U")) {
                        pickAcrossWaveSBean.setCPickType("~");
                        pickAcrossWaveSBean.setCpPickType("CP");
                        pickAcrossWaveSBean.setBpPickType("BP");
                    } else {
                        pickAcrossWaveSBean.setCPickType("C");
                        pickAcrossWaveSBean.setCpPickType("CP");
                        pickAcrossWaveSBean.setBpPickType("BP");
                    }
                }
                String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callLocScpByWh");
                Map map = oper.getParamsMap();
                map.put("facilityId", facilityId);
                map.put("startPickLoc", startPickLoc);
                oper.execute();
                String whAllowIntrlv = "";
                if (oper.getErrors().isEmpty()) {
                    if (oper.getResult() != null) {
                        whAllowIntrlv = (String) oper.getResult();
                    }
                }
                if (pickType.equalsIgnoreCase("BP") && whAllowIntrlv.equalsIgnoreCase(YES)) {
                    String userId = (String) ADFUtils.getBoundAttributeValue("UserId");
                    boolean usrPrivPutaway = checkUserPrivilige(userId, facilityId, "hh_putaway_inventory_s");
                    boolean usrPrivPicking = checkUserPrivilige(userId, facilityId, "hh_bulk_pk_across_wv_s");
                    if (usrPrivPicking && usrPrivPutaway) {
                        this.getDialogConfirmIntrlvgOutputtext().setValue(this.getMessage("CONFIRM_INTRLVG", CONF,
                                                                                          this.getFacilityIdAttrValue(),
                                                                                          this.getLangCodeAttrValue()));
                        this.disableFields();
                        this.getConfirmIntrlvgPopup().show(new RichPopup.PopupHints());
                        return;
                    } else if (!usrPrivPicking) {
                        ((HhPickAcrossWaveSBean) this.getPageFlowBean("HhPickAcrossWaveSBean")).setPmAllowIntrlvg(NO);
                    } else {
                        String msg = this.getMessage("INSUFF_PRIV", WARN, facilityId, "langCode");
                        this.showErrorPanel(msg);
                        ((HhPickAcrossWaveSBean) this.getPageFlowBean("HhPickAcrossWaveSBean")).setPmAllowIntrlvg(NO);
                    }
                } else {
                    ((HhPickAcrossWaveSBean) this.getPageFlowBean("HhPickAcrossWaveSBean")).setPmAllowIntrlvg(NO);
                }
            }
            callPickForm();
            _logger.info("trKeyIn() End");
        }
    }

    private void callPickForm() {
        _logger.info("callPickForm() Start");
        String startPickLoc = (String) ADFUtils.getBoundAttributeValue(START_PICK_LOC_ATTR);
        String pickType = (String) ADFUtils.getBoundAttributeValue(PICK_TYPE_ATTR);

        String unitPickSystemCode = (String) ADFUtils.getBoundAttributeValue(UNIT_PICK_SYSTEM_CODE_ATTR);
        Date waveDate = (Date) ADFUtils.getBoundAttributeValue(WAVE_DATE_ATTR);

        GlobalVariablesViewRowImpl gVViewImplRowImpl = this.getGlobalVariablesViewCurrentRow();
        gVViewImplRowImpl.setGlobalStartPickLoca(startPickLoc);
        gVViewImplRowImpl.setGlobalUnitPickSystemCode(unitPickSystemCode);
        if (waveDate != null) {
            long time = waveDate.getTime();
            java.sql.Date date = new java.sql.Date(time);
            gVViewImplRowImpl.setGlobalWaveDate(date);
        }
        gVViewImplRowImpl.setGlobalAllowIntrlvg(getHhPickAcrossWaveSBeanPageFlowBean().getPmAllowIntrlvg());
        gVViewImplRowImpl.setGlobalNoPickFlag(NO);
        gVViewImplRowImpl.setGlobalNoTaskFlag(NO);
        String l_choose_pick = getLChoosePickValue();
        gVViewImplRowImpl.setGlobalCallingForm("HH_CONT_PK_ACROSS_WV_S");
        if ("BP".equals(pickType) && YES.equals(l_choose_pick)) {
            String pick = (String) ADFUtils.getBoundAttributeValue(PICK_ATTR);
            if ("C".equals(pick)) {
                gVViewImplRowImpl.setGlobalNoPickPackPickTypeC(parameterCPickType);
                gVViewImplRowImpl.setGlobalNoPickPackPickTypeCP("~");
                gVViewImplRowImpl.setGlobalNoPickPackPickType("~");
            } else if ("U".equals(pick)) {
                gVViewImplRowImpl.setGlobalNoPickPackPickTypeC("~");
                gVViewImplRowImpl.setGlobalNoPickPackPickTypeCP(parameterCPPickType);
                gVViewImplRowImpl.setGlobalNoPickPackPickType(parameterBPPickType);
            } else {
                gVViewImplRowImpl.setGlobalNoPickPackPickTypeC(parameterCPickType);
                gVViewImplRowImpl.setGlobalNoPickPackPickTypeCP(parameterCPPickType);
                gVViewImplRowImpl.setGlobalNoPickPackPickType(parameterBPPickType);
            }

        } else if ("BP".equals(pickType)) {
            gVViewImplRowImpl.setGlobalNoPickPackPickTypeC(parameterCPickType);
            gVViewImplRowImpl.setGlobalNoPickPackPickTypeCP(parameterCPPickType);
            gVViewImplRowImpl.setGlobalNoPickPackPickType(parameterBPPickType);
        } else {
            gVViewImplRowImpl.setGlobalNoPickPackPickType(pickType);
        }
        if (StringUtils.isNotEmpty(startPickLoc) && StringUtils.isNotEmpty(pickType) &&
            ("C".equalsIgnoreCase(pickType) || StringUtils.isNotEmpty(unitPickSystemCode))) {
            List<String> pickTypesBulk = new ArrayList<String>(Arrays.asList("B", "BP", "BR"));
            List<String> pickTypesCont = new ArrayList<String>(Arrays.asList("C", "CR", "CP"));
            if (pickTypesBulk.contains(pickType)) {
                ADFUtils.invokeAction("bulkPick");
            } else if (pickTypesCont.contains(pickType)) {
                gVViewImplRowImpl.setGlobalCallingForm("HH_CONT_PK_ACROSS_WV_S");
                ADFUtils.invokeAction("containerPick");
            }
        }
        _logger.info("callPickForm() End");
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                this.setF4SubmittedValue((String) clientEvent.getParameters().get("submittedValue"));
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) { 
                
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                RichInputText field = (RichInputText) clientEvent.getComponent();
                String currentFieldId = field.getId();
                if ("sLoc".equals(currentFieldId)) {
                    this.onChangedStartPickLocNoPickPack(new ValueChangeEvent(this.getStartPickLocNoPickPack(), null,
                                                                              submittedValue));
                    if (null != ADFUtils.getBoundAttributeValue(START_PICK_LOC_ATTR) &&
                        null != this.getHhPickAcrossWaveSBeanPageFlowBean().getIsStartPickLocValidated() &&
                        this.getHhPickAcrossWaveSBeanPageFlowBean().getIsStartPickLocValidated()) {
                        this.setFocusPickType();
                    }
                } else if ("pT".equals(currentFieldId)) {
                    this.onChangedPickTypeNoPickPack(new ValueChangeEvent(this.getPickTypeNoPickPack(), null,
                                                                          submittedValue));
                    if (null != ADFUtils.getBoundAttributeValue(PICK_TYPE_ATTR) &&
                        null != this.getHhPickAcrossWaveSBeanPageFlowBean().getIsPickTypeValidated() &&
                        this.getHhPickAcrossWaveSBeanPageFlowBean().getIsPickTypeValidated()) {
                        this.setFocusUnitPickSystem();
                    }
                } else if ("ups".equals(currentFieldId)) {
                    this.onChangedUnitPickSystemCodeNoPickPack(new ValueChangeEvent(this.getUnitPickSystemCodeNoPickPack(),
                                                                                    null, submittedValue));
                    if (Boolean.TRUE.equals(this.getHhPickAcrossWaveSBeanPageFlowBean().getIsUpsValidated())) {
                        this.setFocusWaveDate();
                    }
                } else if ("pick".equals(currentFieldId)) {
                    this.onChangedPickNoPickPack(new ValueChangeEvent(this.getPickNoPickPack(), null, submittedValue));
                    if (null != ADFUtils.getBoundAttributeValue(PICK_ATTR) &&
                        null != this.getHhPickAcrossWaveSBeanPageFlowBean().getIsPickValidated() &&
                        this.getHhPickAcrossWaveSBeanPageFlowBean().getIsPickValidated()) {
                        this.setFocusStartPickLoc();
                    }
                }
                this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
            }
        }
    }

    public void performKeyWave(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                Date waveDate = (Date) ADFUtils.getBoundAttributeValue(WAVE_DATE_ATTR);
                if (waveDate == null && (this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated() == null) ||
                    this.getHhPickAcrossWaveSBeanPageFlowBean().getDateValidated()) {
                    if (this.getHhPickAcrossWaveSBeanPageFlowBean().getPickEnabled()) {
                        this.setFocusPick();
                    } else {
                        this.setFocusStartPickLoc();
                    }
                    this.refreshContentOfUIComponent(this.getPickAcrossWavePanel());
                }
            }
        }
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.getConfirmIntrlvgPopup().hide();
                this.confirmDialogListener(new DialogEvent(this.getConfirmIntrlvgDialog(), DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.confirmDialogListener(new DialogEvent(this.getConfirmIntrlvgDialog(), DialogEvent.Outcome.no));
                this.getConfirmIntrlvgPopup().hide();
            }
        }
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            ((HhPickAcrossWaveSBean) this.getPageFlowBean("HhPickAcrossWaveSBean")).setPmAllowIntrlvg(YES);
        } else {
            ((HhPickAcrossWaveSBean) this.getPageFlowBean("HhPickAcrossWaveSBean")).setPmAllowIntrlvg(NO);
        }
        this.callPickForm();
    }

    private boolean checkUserPrivilige(String userId, String facilityId, String screenName) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("checkUserPrivilige");
        oper.getParamsMap().put("user", userId);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("screenName", screenName);

        return (Boolean) oper.execute();
    }

    public void okLinkGotoPopupActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.invokeAction("bulkPick");
    }

    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.getConfirmIntrlvgPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getConfirmIntrlvgDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        // Add event code here...
        this.getConfirmIntrlvgPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getConfirmIntrlvgDialog(), DialogEvent.Outcome.no));
    }

    private HhPickAcrossWaveSBean getHhPickAcrossWaveSBeanPageFlowBean() {
        return ((HhPickAcrossWaveSBean) this.getPageFlowBean(HH_PICK_ACROSS_WAVE_FLOW_BEAN));
    }

    public void confirmIntrlvgDialogListener(DialogEvent dialogEvent) {
        parameterPmAllowIntrlvg = dialogEvent.getOutcome().toString();
    }

    private void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void showErrorPanel(String error) {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getErrorPanel().setVisible(false);

        this.getErrorMessage().setValue(error);
        this.getIconErrorMessage().setName(ERR_ICON);
        this.getErrorPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void disableFields() {
        this.getStartPickLocNoPickPack().setDisabled(true);
        this.getPickTypeNoPickPack().setDisabled(true);
        this.getUnitPickSystemCodeNoPickPack().setDisabled(true);
        this.getWaveDateString().setDisabled(true);
        this.getPickNoPickPack().setDisabled(true);
        this.refreshContentOfUIComponent(this.getStartPickLocNoPickPack());
        this.refreshContentOfUIComponent(this.getPickTypeNoPickPack());
        this.refreshContentOfUIComponent(this.getUnitPickSystemCodeNoPickPack());
        this.refreshContentOfUIComponent(this.getWaveDateString());
        this.refreshContentOfUIComponent(this.getPickNoPickPack());
    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        if (getHhPickAcrossWaveSBeanPageFlowBean() == null)
            return;
        Object gotoFld = getHhPickAcrossWaveSBeanPageFlowBean().getIsFocusOn();
        if ("StartPickLocField".equals(gotoFld))
            setFocusOnUIComponent(getStartPickLocNoPickPack());
        else if ("PickTypeField".equals(gotoFld))
            setFocusOnUIComponent(getPickTypeNoPickPack());
        else if ("UpsField".equals(gotoFld))
            setFocusOnUIComponent(getUnitPickSystemCodeNoPickPack());
        else if ("WaveDateField".equals(gotoFld))
            setFocusOnUIComponent(getWaveDateString());
        else if ("PickField".equals(gotoFld))
            setFocusOnUIComponent(getPickNoPickPack());
    }

    private GlobalVariablesViewRowImpl getGlobalVariablesViewCurrentRow() {
        return (GlobalVariablesViewRowImpl) ADFUtils.findIterator("GlobalVariablesViewIterator").getCurrentRow();
    }

    private String getStartPickLocAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(START_PICK_LOC_ATTR);
    }

    private String getPickTypeValue() {
        return (String) ADFUtils.getBoundAttributeValue(PICK_TYPE_ATTR);
    }

    private String getUnitPickSystemCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(UNIT_PICK_SYSTEM_CODE_ATTR);
    }

    private Date getWaveDateValue() {
        return (Date) ADFUtils.getBoundAttributeValue(WAVE_DATE_ATTR);
    }

    private String getPickValue() {
        return (String) ADFUtils.getBoundAttributeValue(PICK_ATTR);
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_pick_across_wv_s_NO_PICK_PACK_F4,RoleBasedAccessConstants.FORM_NAME_hh_pick_across_wv_s)){
                  this.trKeyIn("F4");   
              }else{
                 // showMessage("E", "NOT_ALLOWED", null);
                 showErrorPanel(this.getMessage("NOT_ALLOWED", ERROR, this.getFacilityIdAttrValue(),
                                                this.getLangCodeAttrValue()));
              }
        
    }

    public void setWaveDateNoPickPack(RichInputText waveDateNoPickPack) {
        this.waveDateNoPickPack = waveDateNoPickPack;
    }

    public RichInputText getWaveDateNoPickPack() {
        return waveDateNoPickPack;
    }

    public void setPickAcrossWavePanel(RichPanelFormLayout pickAcrossWavePanel) {
        this.pickAcrossWavePanel = pickAcrossWavePanel;
    }

    public RichPanelFormLayout getPickAcrossWavePanel() {
        return pickAcrossWavePanel;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setStartPickLocNoPickPack(RichInputText startPickLocNoPickPack) {
        this.startPickLocNoPickPack = startPickLocNoPickPack;
    }

    public RichInputText getStartPickLocNoPickPack() {
        return startPickLocNoPickPack;
    }

    public void setPickTypeNoPickPack(RichInputText pickTypeNoPickPack) {
        this.pickTypeNoPickPack = pickTypeNoPickPack;
    }

    public RichInputText getPickTypeNoPickPack() {
        return pickTypeNoPickPack;
    }

    public void setUnitPickSystemCodeNoPickPack(RichInputText unitPickSystemCodeNoPickPack) {
        this.unitPickSystemCodeNoPickPack = unitPickSystemCodeNoPickPack;
    }

    public RichInputText getUnitPickSystemCodeNoPickPack() {
        return unitPickSystemCodeNoPickPack;
    }

    public void setPickNoPickPack(RichInputText pickNoPickPack) {
        this.pickNoPickPack = pickNoPickPack;
    }

    public RichInputText getPickNoPickPack() {
        return pickNoPickPack;
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setConfirmIntrlvgPopup(RichPopup confirmIntrlvgPopup) {
        this.confirmIntrlvgPopup = confirmIntrlvgPopup;
    }

    public RichPopup getConfirmIntrlvgPopup() {
        return confirmIntrlvgPopup;
    }

    public void setDialogConfirmIntrlvgOutputtext(RichOutputText dialogConfirmIntrlvgOutputtext) {
        this.dialogConfirmIntrlvgOutputtext = dialogConfirmIntrlvgOutputtext;
    }

    public RichOutputText getDialogConfirmIntrlvgOutputtext() {
        return dialogConfirmIntrlvgOutputtext;
    }

    public void setParameterPmAllowIntrlvg(String parameterPmAllowIntrlvg) {
        this.parameterPmAllowIntrlvg = parameterPmAllowIntrlvg;
    }

    public String getparameterPmAllowIntrlvg() {
        return parameterPmAllowIntrlvg;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    private void setFocusStartPickLoc() {
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("StartPickLocField");
        this.setFocusOnUIComponent(this.getStartPickLocNoPickPack());
    }

    private void setFocusPickType() {
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("PickTypeField");
        this.setFocusOnUIComponent(this.getStartPickLocNoPickPack());
    }

    private void setFocusUnitPickSystem() {
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("UpsField");
        this.setFocusOnUIComponent(this.getUnitPickSystemCodeNoPickPack());
    }

    private void setFocusWaveDate() {
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("WaveDateField");
        this.setFocusOnUIComponent(this.getWaveDateString());
    }

    private void setFocusPick() {
        this.getHhPickAcrossWaveSBeanPageFlowBean().setIsFocusOn("PickField");
        this.setFocusOnUIComponent(this.getPickNoPickPack());
    }

    public void setStartPickLocIcon(RichIcon startPickLocIcon) {
        this.startPickLocIcon = startPickLocIcon;
    }

    public RichIcon getStartPickLocIcon() {
        return startPickLocIcon;
    }

    public void setPickTypeIcon(RichIcon pickTypeIcon) {
        this.pickTypeIcon = pickTypeIcon;
    }

    public RichIcon getPickTypeIcon() {
        return pickTypeIcon;
    }

    public void setUpsIcon(RichIcon upsIcon) {
        this.upsIcon = upsIcon;
    }

    public RichIcon getUpsIcon() {
        return upsIcon;
    }

    public void setWaveDateIcon(RichIcon waveDateIcon) {
        this.waveDateIcon = waveDateIcon;
    }

    public RichIcon getWaveDateIcon() {
        return waveDateIcon;
    }

    public void setPickIcon(RichIcon pickIcon) {
        this.pickIcon = pickIcon;
    }

    public RichIcon getPickIcon() {
        return pickIcon;
    }

    public void setConfirmIntrlvgDialog(RichDialog confirmIntrlvgDialog) {
        this.confirmIntrlvgDialog = confirmIntrlvgDialog;
    }

    public RichDialog getConfirmIntrlvgDialog() {
        return confirmIntrlvgDialog;
    }

    public void setWaveDateString(RichInputText waveDateString) {
        this.waveDateString = waveDateString;
    }

    public RichInputText getWaveDateString() {
        return waveDateString;
    }

    public void setF4SubmittedValue(String f4SubmittedValue) {
        this.f4SubmittedValue = f4SubmittedValue;
    }

    public String getF4SubmittedValue() {
        return f4SubmittedValue;
    }

    private boolean isCurrentTaskFlowActive() {
        return this.getHhPickAcrossWaveSBeanPageFlowBean() != null;
    }
}
