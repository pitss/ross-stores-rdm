package com.ross.rdm.hotelpicking.hhcontainerinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------

public class Page1Backing extends RDMHotelPickingBackingBean {

    private RichPanelFormLayout containerInquiryPanel;
    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichOutputText errorMessage;
    private RichInputText containerIdCtrl;

    private RichIcon containerIdIcon;

    private static ADFLogger logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichLink f2Link;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f5Link;
    private RichLink f8Link;
    private RichLink f9Link;
    private RichLink changeContainerHiddenLink;

    public static final String OPR_UPD_ROSS_EVENT_CODE = "updateRossEventcodeUseridTmp";
    public static final String OPR_CLR_ROSS_EVENT_CODE = "clearRossEventcodeUseridTmp";
    public static final String OPR_DIS_HAZMAT_MESG = "displayHazmatMessage";
    public static final String OPR_EXEC_QRY_CHILD_CONT_ITM = "executeQueryChildContainerItem";
    public static final String OPR_EXEC_QRY_CONT_ITM = "executeQueryContainerItem";

    public static final String OPR_CREATE_DUMMY_ROUTE = "CreateDummyRoute";
    public static final String OPR_DEL_DUMMY_ROUTE = "DeleteDummyRoute";
    public static final String OPR_CREATE_WITH_PARAM = "initContainerInquiry";

    public static final String BEAN_CONT_INQUIRY = "HhContainerInquirySBean";


    public Page1Backing() {
    }

    public void onChangedContainerIdControl(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhContainerInquirySBean pfb = getPageFlowBean();
            if (pfb != null) //3839: NPE fix
                pfb.setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeContainerHiddenLink());
            actionEvent.queue();
        }
    }

    /**
     * In ADF, when there is no data for a particular VO, controls for it are not rendered.
     * So in this case, we create a "dummy" row, which will then be removed again when no longer needed.
     */
    private void createDummyObjects() {
        if (null == ADFUtils.getBoundAttributeValue("RouteContainerId")) { // no route data, we need a dummy
            if (!executeOperation(OPR_CREATE_DUMMY_ROUTE)) {
                logger.severe("Dummy ROUTE could not be created!");
            }
        }
    }

    /**
     * Cleanup of temporary dummy rows
     */
    private void cleanupDummyObjects() {
        if (Boolean.TRUE.equals(ADFUtils.getBoundAttributeValue("IsNew"))) { // when new entity -> it must be our dummy
            if (!executeOperation(OPR_DEL_DUMMY_ROUTE)) {
                logger.severe("Could not delete dummy ROUTE!");
            }
        }
    }

    private void showError() {
        showMessage(ERROR, MSG_INV_CONTAINER, containerIdCtrl);
        this.getContainerIdIcon().setName(ERR_ICON);
        this.refreshContentOfUIComponent(this.getContainerIdIcon());
        if (!executeOperation(OPR_CREATE_WITH_PARAM)) {
            logger.severe("Could not create DUMMY_EMPTY_ROW!");
        }
    }

    private void showMessagesPanel(String messageType, String msg) {
        if (WARN.equals(messageType)) {
            getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            getIconErrorMessage().setName(LOGO_ICON);
        } else {
            getIconErrorMessage().setName(ERR_ICON);
        }
        getErrorMessage().setValue(msg);
        getIconErrorMessage().setVisible(true);
        refreshContentOfUIComponent(getErrorPanel());
    }


    private void hideMessages() {
        getErrorMessage().setValue(null);
        getIconErrorMessage().setName(null);
        refreshContentOfUIComponent(getErrorPanel());
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setContainerInquiryPanel(RichPanelFormLayout containerInquiryPanel) {
        this.containerInquiryPanel = containerInquiryPanel;
    }

    public RichPanelFormLayout getContainerInquiryPanel() {
        return containerInquiryPanel;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    protected boolean executeOperation(String operationName) {
        oracle.binding.OperationBinding operation = ADFUtils.findOperation(operationName);
        operation.execute();
        List errors = operation.getErrors();
        if (null != errors && errors.size() > 0) {
            return false;
        }

        return true;
    }

    protected boolean executeOperation(String operationName, RichInputText onBehalfOf, String messageType,
                                       String messageId) {
        oracle.binding.OperationBinding operation = ADFUtils.findOperation(operationName);
        operation.execute();
        List errors = operation.getErrors();
        if (null != errors && errors.size() > 0) {
            showMessage(messageType, messageId, onBehalfOf);
            return false;
        }

        return true;
    }

    protected Object getOperationResult(String operationName) {
        oracle.binding.OperationBinding operation = ADFUtils.findOperation(operationName);
        return operation.getResult();
    }

    protected void showMessage(String type, String id, RichInputText control) {
        showMessagesPanel(type, getMessage(id, type, getFacilityId(), getLangCode()));
        if (null != control) {
            addErrorStyleToComponent(control);
            setFocusOnUIComponent(control);
        }
    }

    protected CurrentUser getCurrentUser() {
        return (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
    }

    protected String getFacilityId() {
        return getCurrentUser().getFacilityId();
    }

    protected String getLangCode() {
        return getCurrentUser().getLanguageCode();
    }

    public void setContainerIdCtrl(RichInputText containerIdCtrl) {
        this.containerIdCtrl = containerIdCtrl;
    }

    public RichInputText getContainerIdCtrl() {
        return containerIdCtrl;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void f5LabelAction(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_inquiry_MAIN_F5,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_inquiry)) {
            if (!hasData())
                return;
            Boolean applyLabel = (Boolean) ADFUtils.getBoundAttributeValue("ApplyLabel");
            if (null != applyLabel && applyLabel) {
                cleanupDummyObjects(); // so that the dummy objects do not get committed to the DB

                OperationBinding ob = ADFUtils.findOperation("applyLabel");
                ob.getParamsMap().put("user", this.getCurrentUser().getUserId());
                ob.execute();

                if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
                    createDummyObjects(); // need them again
                    displayHazmatMessage();
                }
            } else {
                displayHazmatMessage();
            }
        } else {
            showMessage("E", "NOT_ALLOWED", null);
        }
    }

    private void displayHazmatMessage() {
        if (!executeOperation(OPR_DIS_HAZMAT_MESG)) {
            logger.severe("Could not call 'displayHazmatMessage'!");
            return;
        }
        String message = (String) getOperationResult(OPR_DIS_HAZMAT_MESG);
        HhContainerInquirySBean pfb = getPageFlowBean();
        if (null != message) {
            pfb.setHazmatMessage(message);
            showMessage("I", message, null);
        } else {
            pfb.setHazmatMessage(null);
        }

    }


    // before navigating, check whether we have a valid container
    private boolean hasData() {
        String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
        return !containerId.contains("-") /* "DUMMY_EMPTY_ROW".equals(containerId) */;
    }

    public void f2ItemsAction(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_inquiry_MAIN_F2,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_inquiry)) {
            navigateToAction("Page2");
        } else {
            showMessage("E", "NOT_ALLOWED", null);
        }

    }

    public void f3Action(ActionEvent actionEvent) {
        OperationBinding ob = ADFUtils.findOperation("Rollback");
        if (ob != null)
            ob.execute();
        navigateToAction("Back");
    }

    public void f4RouteAction(ActionEvent actionEvent) {

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_inquiry_MAIN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_inquiry)) {
            if (YES.equals(JSFUtils.resolveExpression("#{pageFlowScope.HhContainerInquirySBean.mldEnabled}"))) {
                if (!hasData())
                    return;
                navigateToAction("Page5");
            }
        } else {
            showMessage("E", "NOT_ALLOWED", null);
        }
    }

    public void f8RecvAction(ActionEvent actionEvent) {

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_inquiry_MAIN_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_inquiry)) {
            if (!hasData())
                return;
            navigateToAction("Page3");
        } else {
            showMessage("E", "NOT_ALLOWED", null);
        }
    }

    public void f9ShipAction(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_inquiry_MAIN_F9,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_inquiry)) {
            if (!hasData())
                return;
            navigateToAction("Page4_6");
        } else {
            showMessage("E", "NOT_ALLOWED", null);
        }

    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2Link());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Link());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF9Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                    submittedVal.equals(field.getValue())) {
                    onChangedContainerIdControl(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        HhContainerInquirySBean pfb = getPageFlowBean();
        String hazmatMessage = pfb.getHazmatMessage();
        if (null != hazmatMessage)
            this.showMessage("I", hazmatMessage, null);
        else
            this.hideMessages();
    }

    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    public void setF9Link(RichLink f9Link) {
        this.f9Link = f9Link;
    }

    public RichLink getF9Link() {
        return f9Link;
    }

    public String changeContainerHiddenAction() {
        HhContainerInquirySBean pfb = getPageFlowBean();
        if (!pfb.getExecutedKey()) {
            String containerId = (String) pfb.getContainerId();
            String boundContainerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
            if (boundContainerId != null)
                if (boundContainerId.contains("-")) {
                    logger.finest("Removing DUMMY_EMPTY_ROW");
                    if (!executeOperation("Delete")) { // remove dummy empty row
                        logger.severe("DUMMY_EMPTY_ROW could not be deleted!");
                    }
                } else {
                    cleanupDummyObjects();
                }
            if (null != containerId && containerId.length() > 0) {
                if (!executeOperation("findContainer", containerIdCtrl, ERROR, MSG_INV_CONTAINER)) {
                    logger.severe("Could not execute 'findContainer'!");
                    return null;
                }
                // if container with this ID was found, the binding will be non-empty
                if (null == ADFUtils.getBoundAttributeValue("ContainerId")) {
                    logger.info("Container not found. Container ID: " + containerId);
                    showError();
                    return null;
                }
                createDummyObjects();
                removeErrorStyleToComponent(containerIdCtrl);
                this.getContainerIdIcon().setName("required");
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
                hideMessages();
                displayHazmatMessage();
            } else {
                showError();
            }
            this.refreshContentOfUIComponent(this.getF4Link());
        }
        pfb.setExecutedKey(null);
        return null;
    }

    public void setChangeContainerHiddenLink(RichLink changeContainerHiddenLink) {
        this.changeContainerHiddenLink = changeContainerHiddenLink;
    }

    public RichLink getChangeContainerHiddenLink() {
        return changeContainerHiddenLink;
    }

    public void prepareItemsPage() {
        boolean isMasterFlag = true;
        if (hasData()) {
            isMasterFlag = YES.equals(ADFUtils.getBoundAttributeValue("MasterFlag"));
        }
        getPageFlowBean().setMasterFlag(isMasterFlag ? YES : NO);
        //        OperationBinding op =
        //            ADFUtils.findOperation(isMasterFlag ? OPR_EXEC_QRY_CHILD_CONT_ITM : OPR_EXEC_QRY_CONT_ITM);
        //        op.getParamsMap().put("facility", getFacilityId());
        //        op.getParamsMap().put("container", getPageFlowBean().getContainerId());
        //        op.execute();
    }

    private HhContainerInquirySBean getPageFlowBean() {
        return ((HhContainerInquirySBean) this.getPageFlowBean(BEAN_CONT_INQUIRY));
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_hotelpicking_hhcontainerinquirys_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
