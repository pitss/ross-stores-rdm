package com.ross.rdm.hotelpicking.hhcontainerinquirys.view.bean;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------

public class HhContainerInquirySBean {

    private String maxUsers;
    private Boolean containerIdEnabled;
    private String hazmatMessage = null;
    private Boolean executedKey = Boolean.FALSE;
    private String masterFlag = "Y";

    public void setHazmatMessage(String hazmatMessage) {
        this.hazmatMessage = hazmatMessage;
    }

    public String getHazmatMessage() {
        return hazmatMessage;
    }
    // container ID storage is here to preserve the value across navigation to subpages
    private String containerId;
    private String mldEnabled;


    public void setMaxUsers(String maxUsers) {
        this.maxUsers = maxUsers;
    }

    public String getMaxUsers() {
        return maxUsers;
    }

    public void setContainerIdEnabled(Boolean containerIdEnabled) {
        this.containerIdEnabled = containerIdEnabled;
    }

    public Boolean getContainerIdEnabled() {
        return containerIdEnabled;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setContainerId(String containerId) {
        this.containerId = containerId.toUpperCase();
    }

    public String getContainerId() {
        return containerId;
    }

    public void setMldEnabled(String mldEnabled) {
        this.mldEnabled = mldEnabled;
    }

    public String getMldEnabled() {
        return mldEnabled;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }

    public void setMasterFlag(String masterFlag) {
        this.masterFlag = masterFlag;
    }

    public String getMasterFlag() {
        return masterFlag;
    }
}
