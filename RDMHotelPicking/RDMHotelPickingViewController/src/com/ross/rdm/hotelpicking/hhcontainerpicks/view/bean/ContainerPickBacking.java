package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ContainerPick.jspx
// ---
// ---------------------------------------------------------------------
public class ContainerPickBacking extends ContainerPickBaseBean {

    private RichInputText genericToContainer;
    private RichInputText pickToContainer;
    private RichInputText confFromContainerId;
    private RichIcon iconGenericToContainer;
    private RichIcon iconConfFromContainerId;
    private RichLink f3Exit;
    private RichLink f5Full;
    private RichLink f6Desc;
    private RichLink f7Bypass;
    private RichLink f8Invctl;
    private RichLink validateConfFromContainerIdLink;
    private RichLink validateGenericToContainerLink;

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;

    private final static String GENERIC_TO_CONTAINER_ITEM = "gtocid";
    private final static String PICK_TO_CONTAINER_ITEM = "ptcid";
    private final static String CONF_FROM_CONTAINER_ITEM = "cfcid";
    private final static String CONTAINERPICK_PAGEDEF_NAME =
        "com_ross_rdm_hotelpicking_hhcontainerpicks_view_pageDefs_ContainerPickPageDef";
    private RichPanelGroupLayout mainPanel;

    private RichPopup exitPopup;
    private RichOutputText dialogOutputText;
    private RichPopup successPopup;
    private RichOutputText dialogOutputText2;
    private RichDialog okGoToLocationDialog;
    private RichOutputText okToLocatioInputText;
    private RichPopup okGoToLocationPpoup;
    private RichLink succesPopUpLink;
    private RichLink exitPopUpLink;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;

    public ContainerPickBacking() {

    }

    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.CONTAINER_PICK_BLOCK);

        HhContainerPickSBean pfBean = getContainerPickBean();
        String gotoFld = pfBean.getIsFocusOn();

        if (GEN_CONTAINER.equals(gotoFld) || PICK_CONTAINER.equals(gotoFld)) {
            if ("Y".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("LabeledPicking"))) {
                getGenericToContainer().setDisabled(true);
                refreshContentOfUIComponent(getGenericToContainer());
                getPickToContainer().setDisabled(false);
                setFocusOnUIComponent(getPickToContainer());
            } else {
                getPickToContainer().setDisabled(true);
                refreshContentOfUIComponent(getPickToContainer());
                getGenericToContainer().setDisabled(false);
                setFocusOnUIComponent(getGenericToContainer());
            }
        } else if (CONFIRM_CONT.equals(gotoFld) || FROM_LOC.equals(gotoFld)) {
            getConfFromContainerId().setDisabled(false);
            setFocusOnUIComponent(getConfFromContainerId());
        }
        pfBean.setIsFocusOn(null);
    }

    public void onValueChangedGenericToContainerContainerPick(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            pfBean.setLinkPressedContainerPick(null);

            String newValue = (String) vce.getNewValue();
            if (newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())) {
                this.updateModel(vce);
                ActionEvent actionEvent = new ActionEvent(this.getValidateGenericToContainerLink());
                actionEvent.queue();
            }
        }
    }

    /**
     * Validate Generic to Container
     */
    public String onChangedGenericToContainerContainerPick() {
        String action = null;
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            if (pfBean.getLinkPressedContainerPick() != null) {
                pfBean.setLinkPressedContainerPick(null);
            } else {
                OperationBinding oper = ADFUtils.findOperation("validateGenericToContainerContainerPick");
                List<String> results = (List<String>) oper.execute();
                if (results != null && results.size() == 2) {
                    String returnedError = results.get(1);
                    String message =
                        this.getMessage(NO_PICK_AISLE, WARN, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                    String message2 =
                        this.getMessage(NO_PICKS, WARN, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                    if (message.equals(returnedError) || message2.equals(returnedError)) {
                        // EXIT WITH POP UP
                        this.disabledAllField();

                        this.getDialogOutputText().setValue(returnedError);
                        this.getExitPopup().show(new RichPopup.PopupHints());
                    } else {
                        this.showMessagesPanel(results.get(0), results.get(1));

                        this.addErrorStyleToComponent(this.getGenericToContainer());
                        this.getIconGenericToContainer().setName("error");
                        this.refreshContentOfUIComponent(this.getIconGenericToContainer());
                        this.setFocusOnUIComponent(this.getGenericToContainer());
                    }
                } else if (results != null && results.size() == 1) {
                    if (!HhContainerPickSBean.CONTAINER_PICK_BLOCK.equals(results.get(0))) {
                        action = pfBean.getActionByBlockName(results.get(0));

                        if (this.logoutExitBTF().equals(action)) {
                            action = null;
                            String message =
                                this.getMessage(DMS_ERROR, ERROR,
                                                (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                            this.disabledAllField();

                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        }
                    } else {
                        this.hideMessagesPanel();
                        this.getGenericToContainer().setDisabled(true);
                        this.removeErrorStyleToComponent(this.getGenericToContainer());
                        this.getIconGenericToContainer().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getIconGenericToContainer());

                        pfBean.setIsFocusOn(CONFIRM_CONT);
                        this.setFocusOnUIComponent(this.getConfFromContainerId());
                    }
                } else if (results != null && results.size() == 3) {
                    // EXIT WITH POP UP
                    this.disabledAllField();

                    this.getDialogOutputText().setValue(results.get(1));
                    this.getExitPopup().show(new RichPopup.PopupHints());
                } else {
                    this.hideMessagesPanel();

                    if (ADFUtils.getBoundAttributeValue("FromLocationId") != null) {
                        this.getGenericToContainer().setDisabled(true);
                        this.removeErrorStyleToComponent(this.getGenericToContainer());
                        this.getIconGenericToContainer().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getIconGenericToContainer());

                        pfBean.setIsFocusOn(CONFIRM_CONT);
                        this.setFocusOnUIComponent(this.getConfFromContainerId());
                    }
                }
            }
        }
        return action;
    }

    public void onValueChangedConfFromContainerIdContainerPick(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            pfBean.setLinkPressedContainerPick(null);

            String newValue = (String) vce.getNewValue();
            if (newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())) {
                this.updateModel(vce);
                ActionEvent actionEvent = new ActionEvent(this.getValidateConfFromContainerIdLink());
                actionEvent.queue();
            }
        }
    }

    /**
     * Validate Confirm from Container
     */
    public String onChangedConfFromContainerIdContainerPickAction() {
        String action = null;
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            if (pfBean.getLinkPressedContainerPick() != null) {
                pfBean.setLinkPressedContainerPick(null);
            } else {
                action = validateConfFromContainerId();
            }
        }
        return action;
    }

    public String f3ExitAction() {
        String action = null;
        if ("Y".equals(ADFUtils.getBoundAttributeValue("DropoffRequired"))) {
            this.showMessagesPanel("E",
                                   this.getMessage("CNNT_CNCL_START", "E",
                                                   (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                   (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
        } else {
            // TODO -> :global.error_flag := 'Y';
            ADFUtils.setBoundAttributeValue("NewPickToContainer", this.getGenericToContainer().getValue());
            ADFUtils.findOperation("clearBlock").execute();
            OperationBinding oper = ADFUtils.findOperation("callPkgContainerPickingAdfTrExit");
            List<String> errorsMessages = (List<String>) oper.execute();
            if (errorsMessages != null && errorsMessages.size() > 1) {
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
            } else {
                //Return to Task Queue
                if (YES.equals(getContainerPickBean().getPmTaskQueueInd()))
                    popTaskFlow(3);
                else
                    action = this.logoutExitBTF();
                this.hideMessagesPanel();
            }
        }
        return action;
    }

    public String f5FullAction() {
        String action = null;
        

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F5,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {
            String newValue = (String) getConfFromContainerId().getValue();
            if (!this.whenValidateItemConfFromContainerId(newValue)) {
                OperationBinding oper = ADFUtils.findOperation("callPkgContainerPickingAdfCheckEmptyPickToContainer");
                List<String> results = (List<String>) oper.execute();
                if (results != null) {
                    if (results.size() == 1) {
                        action = results.get(0);
                        if (this.logoutExitBTF().equals(action)) {
                            action = null;
                            String message =
                                this.getMessage("DMS_ERROR", "E",
                                                (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                            this.disabledAllField();

                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        }
                    } else if (results.size() == 2) {
                        this.showMessagesPanel(results.get(0), results.get(1));
                        this.setErrorInField();
                    }
                } else {
                    this.clearErrorInField();
                    ADFUtils.setBoundAttributeValue("CallingBlock", HhContainerPickSBean.CONTAINER_PICK_BLOCK);
                    oper = ADFUtils.findOperation("navigateToLocationInContainerPick");
                    oper.getParamsMap().put("fullDoneFlagIn", "F");
                    results = (List<String>) oper.execute();
                    if (results != null && results.size() > 1) {
                        this.showMessagesPanel(results.get(0), results.get(1));
                        this.setErrorInField();
                    } else if (results != null && results.size() == 1) {
                        this.hideMessagesPanel();
                        this.clearErrorInField();
                        action = getContainerPickBean().getActionByBlockName(results.get(0));
                        if (this.logoutExitBTF().equals(action)) {
                            action = null;
                            String message =
                                this.getMessage("DMS_ERROR", "E",
                                                (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                            this.disabledAllField();

                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        }
                    }
                }
            }

        } else {
            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }
        if (StringUtils.isNotEmpty(action)) {
            
            this.setF5FullPressed(true);
        }

        return action;
    }

    public void setF5FullPressed(boolean f5FullPressed) {
        ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).setF5FullPressed(f5FullPressed);
    }

    public boolean isF5FullPressed() {
        return ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).isF5FullPressed();
    }
    
    private void setErrorInField() {
        if (!this.getConfFromContainerId().isDisabled()) {
            this.addErrorStyleToComponent(this.getConfFromContainerId());
            this.getIconConfFromContainerId().setName("error");
            this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
            this.setFocusOnUIComponent(this.getConfFromContainerId());
        } else {
            this.addErrorStyleToComponent(this.getGenericToContainer());
            this.getIconGenericToContainer().setName("error");
            this.refreshContentOfUIComponent(this.getIconGenericToContainer());
            if ("Y".equals(ADFUtils.getBoundAttributeValue("LabeledPicking"))) {
                this.addErrorStyleToComponent(this.getGenericToContainer());
                this.setFocusOnUIComponent(this.getGenericToContainer());
            } else {
                this.addErrorStyleToComponent(this.getPickToContainer());
                this.setFocusOnUIComponent(this.getPickToContainer());
            }
        }
    }

    private void clearErrorInField() {
        if (!this.getConfFromContainerId().isDisabled()) {
            this.removeErrorStyleToComponent(this.getConfFromContainerId());
            this.getIconConfFromContainerId().setName("required");
            this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
            this.setFocusOnUIComponent(this.getConfFromContainerId());
        } else {
            this.removeErrorStyleToComponent(this.getGenericToContainer());
            this.getIconGenericToContainer().setName("required");
            this.refreshContentOfUIComponent(this.getIconGenericToContainer());
            if ("Y".equals(ADFUtils.getBoundAttributeValue("LabeledPicking"))) {
                this.removeErrorStyleToComponent(this.getGenericToContainer());
                this.setFocusOnUIComponent(this.getGenericToContainer());
            } else {
                this.removeErrorStyleToComponent(this.getConfFromContainerId());
                this.setFocusOnUIComponent(this.getConfFromContainerId());
            }
        }
    }

    public String f6DescAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F6,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {
            String newValue = (String) getConfFromContainerId().getValue();
            if (!this.whenValidateItemConfFromContainerId(newValue)) {
                if (ADFUtils.getBoundAttributeValue("ItemId") != null &&
                    ADFUtils.getBoundAttributeValue("Description") != null) {
                    action = HhContainerPickSBean.ITEM_DESCRIPTION_ACTION;
                }
            }

        } else {


            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }


        return action;
    }

    public String f7BypassAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {

            String newValue = (String) getConfFromContainerId().getValue();
            if (!this.whenValidateItemConfFromContainerId(newValue)) {
                if (ADFUtils.getBoundAttributeValue("PickFromContainerId") != null) {
                    OperationBinding oper = ADFUtils.findOperation("containerPickContainerPickF7");
                    List<String> result = (List<String>) oper.execute();
                    if (result != null && result.size() == 1) {
                        HhContainerPickSBean pfBean = getContainerPickBean();
                        action = pfBean.getActionByBlockName(result.get(0));
                        if (HhContainerPickSBean.CONTAINER_PICK_ACTION.equals(action)) {
                            action = null;
                            if (!this.getConfFromContainerId().isDisabled()) {
                                pfBean.setIsFocusOn(CONFIRM_CONT);
                            } else if ("Y".equals(ADFUtils.getBoundAttributeValue("LabeledPicking"))) {
                                pfBean.setIsFocusOn(PICK_CONTAINER);
                            } else {
                                pfBean.setIsFocusOn(GEN_CONTAINER);
                            }
                            this.refreshContentOfUIComponent(this.getMainPanel());
                        } else if (this.logoutExitBTF().equals(action)) {
                            // EXIT WITH POP UP
                            this.disabledAllField();

                            String message =
                                this.getMessage("NO_PICKS", "W", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                            this.getDialogOutputText().setValue(message);
                            this.getExitPopup().show(new RichPopup.PopupHints());
                        }
                        this.hideMessagesPanel();
                        this.clearErrorInField();
                    } else if (result != null && result.size() > 1) {

                        String message =
                            this.getMessage("NO_PICKS", "W", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                            (String) ADFUtils.getBoundAttributeValue("LanguageCode"));

                        if (message.equals(result.get(1))) {
                            // EXIT WITH POP UP
                            this.disabledAllField();

                            this.getDialogOutputText().setValue(message);
                            this.getExitPopup().show(new RichPopup.PopupHints());
                        } else {
                            this.showMessagesPanel(result.get(0), result.get(1));
                            this.setErrorInField();
                        }
                    }
                }
            }

        } else {


            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }


        return action;
    }

    public String f8InvctlAction() {
        String action = null;
        // ISSUE 1579

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {
            this.whenValidateItemConfFromContainerId((String) this.getConfFromContainerId().getValue());

            if (ADFUtils.getBoundAttributeValue("PickFromContainerId") != null) {
                OperationBinding oper = ADFUtils.findOperation("containerPickContainerPickF8");
                List<String> result = (List<String>) oper.execute();
                if (result != null && result.size() == 1) {
                    action = getContainerPickBean().getActionByBlockName(result.get(0));
                    this.hideMessagesPanel();
                    this.clearErrorInField();
                } else if (result != null && result.size() > 1) {
                    if ("E".equalsIgnoreCase(result.get(0))) {
                        this.setErrorInField();
                    } else if ("M".equalsIgnoreCase(result.get(0))) {
                        this.clearErrorInField();
                    }
                    this.showMessagesPanel(result.get(0), result.get(1));
                }
            }

        } else {


            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }


        return action;
    }

    /**
     * Custom event executed only when function keys are pressed
     * It queues action event on the appropriate link/button
     * @param clientEvent
     */
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            pfBean.setLinkPressedContainerPick(null);

            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String item = (String) clientEvent.getParameters().get("item");
            // WE HAVE TO UPDATE THE MODEL...
            if (GENERIC_TO_CONTAINER_ITEM.equals(item)) {
                ADFUtils.setBoundAttributeValue("GenericToContainer", submittedValue);
            } else if (CONF_FROM_CONTAINER_ITEM.equals(item)) {
                ADFUtils.setBoundAttributeValue("ConfFromContainerId", submittedValue);
            }

            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.checkEmptyItem(component, item);
                } else if (CONF_FROM_CONTAINER_ITEM.equals(item)) {
                    ActionEvent actionEvent = new ActionEvent(this.getValidateConfFromContainerIdLink());
                    actionEvent.queue();
                } else if (GENERIC_TO_CONTAINER_ITEM.equals(item)) {
                    ActionEvent actionEvent = new ActionEvent(this.getValidateGenericToContainerLink());
                    actionEvent.queue();
                }
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Exit());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed) && CONF_FROM_CONTAINER_ITEM.equals(item)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Full());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed) && CONF_FROM_CONTAINER_ITEM.equals(item)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Desc());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed) && CONF_FROM_CONTAINER_ITEM.equals(item)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Bypass());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed) && CONF_FROM_CONTAINER_ITEM.equals(item)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Invctl());
                actionEvent.queue();
            }
        }
    }

    private void checkEmptyItem(RichInputText component, String item) {
        if (GENERIC_TO_CONTAINER_ITEM.equals(item) || PICK_TO_CONTAINER_ITEM.equals(item)) {
            this.showMessagesPanel("E",
                                   this.getMessage("INV_CONTAINER", "E",
                                                   (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                   (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            this.addErrorStyleToComponent(component);
            this.getIconGenericToContainer().setName("error");
            this.refreshContentOfUIComponent(this.getIconGenericToContainer());
        } else if (CONF_FROM_CONTAINER_ITEM.equals(item)) {
            this.showMessagesPanel("W",
                                   this.getMessage("PARTIAL_ENTRY", "W",
                                                   (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                   (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            this.addErrorStyleToComponent(component);
            this.getIconConfFromContainerId().setName("error");
            this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
        }
    }

    public void setGenericToContainer(RichInputText genericToContainer) {
        this.genericToContainer = genericToContainer;
    }

    public RichInputText getGenericToContainer() {
        return genericToContainer;
    }

    public void setPickToContainer(RichInputText pickToContainer) {
        this.pickToContainer = pickToContainer;
    }

    public RichInputText getPickToContainer() {
        return pickToContainer;
    }

    public void setConfFromContainerId(RichInputText confFromContainerId) {
        this.confFromContainerId = confFromContainerId;
    }

    public RichInputText getConfFromContainerId() {
        return confFromContainerId;
    }


    public void setF3Exit(RichLink f3Exit) {
        this.f3Exit = f3Exit;
    }

    public RichLink getF3Exit() {
        return f3Exit;
    }

    public void setF5Full(RichLink f5Full) {
        this.f5Full = f5Full;
    }

    public RichLink getF5Full() {
        return f5Full;
    }

    public void setF6Desc(RichLink f6Desc) {
        this.f6Desc = f6Desc;
    }

    public RichLink getF6Desc() {
        return f6Desc;
    }

    public void setF7Bypass(RichLink f7Bypass) {
        this.f7Bypass = f7Bypass;
    }

    public RichLink getF7Bypass() {
        return f7Bypass;
    }

    public void setF8Invctl(RichLink f8Invctl) {
        this.f8Invctl = f8Invctl;
    }

    public RichLink getF8Invctl() {
        return f8Invctl;
    }

    public void setIconGenericToContainer(RichIcon iconGenericToContainer) {
        this.iconGenericToContainer = iconGenericToContainer;
    }

    public RichIcon getIconGenericToContainer() {
        return iconGenericToContainer;
    }

    public void setIconConfFromContainerId(RichIcon iconConfFromContainerId) {
        this.iconConfFromContainerId = iconConfFromContainerId;
    }

    public RichIcon getIconConfFromContainerId() {
        return iconConfFromContainerId;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if ("W".equals(messageType)) {
            this.getIconErrorMessage().setName("warning");
        } else if ("I".equals(messageType)) {
            this.getIconErrorMessage().setName("info");
        } else if ("M".equals(messageType)) {
            this.getIconErrorMessage().setName("logo");
        } else {
            this.getIconErrorMessage().setName("error");
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public boolean isErrorOnThePage() {
        boolean error = false;
        if (this.getIconErrorMessage() != null) {
            error = this.getIconErrorMessage().isVisible() && "error".equals(this.getIconErrorMessage().getName());
        }
        return error;
    }

    public void setMainPanel(RichPanelGroupLayout mainPanel) {
        this.mainPanel = mainPanel;
    }

    public RichPanelGroupLayout getMainPanel() {
        return mainPanel;
    }

    private boolean whenValidateItemConfFromContainerId(String newValue) {
        boolean error = false;
        if (StringUtils.isNotEmpty(newValue) &&
            !newValue.equals(ADFUtils.getBoundAttributeValue("PickFromContainerId"))) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            if ("1".equals(pfBean.getPVersion())) {
                if ((!"C".equals(ADFUtils.getBoundAttributeValue("PickType")) ||
                     !"Y".equals(ADFUtils.getBoundAttributeValue("LabeledPicking")))) {
                    error = true;
                }
            } else {
                error = true;
            }
        }

        if (error) {
            this.showMessagesPanel("W",
                                   this.getMessage("INV_CONTAINER", "W",
                                                   (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                   (String) ADFUtils.getBoundAttributeValue("LanguageCode")));

            this.addErrorStyleToComponent(this.getConfFromContainerId());
            this.getIconConfFromContainerId().setName("error");
            this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
            this.setFocusOnUIComponent(this.getConfFromContainerId());
        }

        return error;
    }

    private String validateConfFromContainerId() {
        String navigation = null;
        String newValue = (String) getConfFromContainerId().getValue();
        this.hideMessagesPanel();
        if (!this.whenValidateItemConfFromContainerId(newValue)) {
            HhContainerPickSBean pfBean = getContainerPickBean();
            // ADFUtils.setBoundAttributeValue("ConfFromContainerId", newValue); //To update the model
            OperationBinding oper = ADFUtils.findOperation("validateConfFromContainerIdContainerPick");
            List<String> results = (List<String>) oper.execute();
            if (results != null && results.size() == 1) {
                this.hideMessagesPanel();
                this.removeErrorStyleToComponent(this.getConfFromContainerId());
                this.getIconConfFromContainerId().setName("required");
                this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
                navigation = pfBean.getActionByBlockName(results.get(0));

                if (this.logoutExitBTF().equals(navigation)) {
                    navigation = null;
                    String message =
                        this.getMessage("DMS_ERROR", "E", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                    this.disabledAllField();

                    this.getDmsErrorOutputText().setValue(message);
                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                }
            } else if (results != null && results.size() == 2) {
                this.showMessagesPanel(results.get(0), results.get(1));
                if ("E".equals(results.get(0)) || "W".equals(results.get(0))) {
                    this.addErrorStyleToComponent(this.getConfFromContainerId());
                    this.getIconConfFromContainerId().setName("error");
                } else {
                    this.removeErrorStyleToComponent(this.getConfFromContainerId());
                    this.getIconConfFromContainerId().setName("required");
                }
                this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
                this.setFocusOnUIComponent(this.getConfFromContainerId());
            } else if (results != null && results.size() > 2) { // CASE -> goFromMasterChild2
                this.getConfFromContainerId().setDisabled(true);
                this.removeErrorStyleToComponent(this.getConfFromContainerId());
                this.getIconConfFromContainerId().setName("required");
                this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
                if (results.size() == 4) {
                    // EXIT WITH POP UP
                    this.getDialogOutputText2().setValue(results.get(3));
                    this.getSuccessPopup().show(new RichPopup.PopupHints());
                } else {
                    navigation = this.executeLogicBeforeNavigate();
                }
            } else {
                this.hideMessagesPanel();
                this.removeErrorStyleToComponent(this.getConfFromContainerId());
                this.getIconConfFromContainerId().setName("required");
                this.refreshContentOfUIComponent(this.getIconConfFromContainerId());
            }
        }
        return navigation;
    }

    private boolean hasErrorConfFromContainerId() {
        return "error".equals(this.getIconConfFromContainerId().getName());
    }

    public void performKeyExitPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitPopUpLink());
                actionEvent.queue();
            }
        }
    }

    public void yesLinkConfirmExitPopupActionListener(ActionEvent actionEvent) {
        this.getExitPopup().hide();
        String action = null;
        HhContainerPickSBean pfBean = getContainerPickBean();
        if (pfBean.isCalledFromMenu()) {
            action = HhContainerPickSBean.PAGE1_ACTION;
        } else {
            action = this.logoutExitBTF();
        }
        ADFUtils.invokeAction(action);
    }

    public void performKeySuccessPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getSuccesPopUpLink());
                actionEvent.queue();
            }
        }
    }

    public void yesLinkConfirmSuccessPopupActionListener(ActionEvent actionEvent) {
        this.getSuccessPopup().hide();
        ADFUtils.invokeAction(this.executeLogicBeforeNavigate());
    }

    private String executeLogicBeforeNavigate() {
        String navigation = null;
        HhContainerPickSBean pfBean = getContainerPickBean();
        if (!new BigDecimal(0).equals(ADFUtils.getBoundAttributeValue("PickContainerQty"))) {
            pfBean.setExecuteActionFromMasterChild2("DIFF_PICK");
            navigation = HhContainerPickSBean.FROM_MASTER_CHILD2_ACTION;
        } else {
            OperationBinding oper = ADFUtils.findOperation("containerPickFromMasterChild2F4");
            List<String> results = (List<String>) oper.execute();
            if (results != null) {
                if (results.size() == 1) {
                    navigation = results.get(0);
                    if (this.logoutExitBTF().equals(navigation)) {
                        navigation = null;
                        String message =
                            this.getMessage("DMS_ERROR", "E", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                            (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                        this.disabledAllField();

                        this.getDmsErrorOutputText().setValue(message);
                        this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                    }
                } else if (results.size() == 2) {
                    pfBean.setExecuteActionFromMasterChild2(results.get(0) + "||" + results.get(1));
                    navigation = HhContainerPickSBean.FROM_MASTER_CHILD2_ACTION;
                } else if (results.size() == 3 && "CHECK_EMPTY".equals(results.get(0))) {
                    pfBean.setExecuteActionFromMasterChild2("CHECK_EMPTY");
                    pfBean.setMessageFromMasterChild2(results.get(2));
                    navigation = HhContainerPickSBean.FROM_MASTER_CHILD2_ACTION;
                } else {
                    oper = ADFUtils.findOperation("loadTheScrnWrap");
                    ContainerPickResults result = (ContainerPickResults) oper.execute();
                    if (result.isOperationError()) {
                        this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                    } else if (result.getIsTrExit()) {
                        navigation = pfBean.getActionByBlockName(result.getNavigationBlock());
                        if (this.logoutExitBTF().equals(navigation)) {
                            navigation = null;
                            String message =
                                this.getMessage("DMS_ERROR", "E",
                                                (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                            this.disabledAllField();

                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        }
                    } else if (result.getNavigationBlock() != null) {
                        if (result.getMessage() != null && !result.getMessage().isEmpty() &&
                            !"TO_LOCATION".equalsIgnoreCase(result.getNavigationBlock())) {
                            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                        }
                        oper = ADFUtils.findOperation("callDoCommit");
                        oper.execute();
                        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                            if (result.getNavigationItem() != null) {
                                pfBean.setIsFocusOn(result.getNavigationBlock() + "." + result.getNavigationItem());
                            }
                            String msgReturn = result.getMessage() == null ? "" : result.getMessage().get(1);
                            if (msgReturn != null && !msgReturn.isEmpty()) {
                                if ("TO_LOCATION".equalsIgnoreCase(result.getNavigationBlock())) {
                                    // EXIT WITH POP UP
                                    this.disabledAllField();

                                    this.getOkToLocatioInputText().setValue(msgReturn);
                                    this.getOkGoToLocationPpoup().setLauncherVar(pfBean.getActionByBlockName(result.getNavigationBlock()));
                                    this.getOkGoToLocationPpoup().show(new RichPopup.PopupHints());
                                }
                            } else {
                                navigation = pfBean.getActionByBlockName(result.getNavigationBlock());
                            }
                        }
                    } else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                        this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                    }
                }
            }
        }
        return navigation;
    }

    private void disabledAllField() {
        this.getGenericToContainer().setDisabled(true);
        this.getConfFromContainerId().setDisabled(true);
        this.getPickToContainer().setDisabled(true);
        this.refreshContentOfUIComponent(this.getGenericToContainer());
        this.refreshContentOfUIComponent(this.getConfFromContainerId());
        this.refreshContentOfUIComponent(this.getPickToContainer());
    }

    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setValidateConfFromContainerIdLink(RichLink validateConfFromContainerIdLink) {
        this.validateConfFromContainerIdLink = validateConfFromContainerIdLink;
    }

    public RichLink getValidateConfFromContainerIdLink() {
        return validateConfFromContainerIdLink;
    }

    public void setValidateGenericToContainerLink(RichLink validateGenericToContainerLink) {
        this.validateGenericToContainerLink = validateGenericToContainerLink;
    }

    public RichLink getValidateGenericToContainerLink() {
        return validateGenericToContainerLink;
    }

    public void setSuccessPopup(RichPopup successPopup) {
        this.successPopup = successPopup;
    }

    public RichPopup getSuccessPopup() {
        return successPopup;
    }

    public void setDialogOutputText2(RichOutputText dialogOutputText2) {
        this.dialogOutputText2 = dialogOutputText2;
    }

    public RichOutputText getDialogOutputText2() {
        return dialogOutputText2;
    }

    public void setOkGoToLocationDialog(RichDialog okGoToLocationDialog) {
        this.okGoToLocationDialog = okGoToLocationDialog;
    }

    public RichDialog getOkGoToLocationDialog() {
        return okGoToLocationDialog;
    }

    public void setOkToLocatioInputText(RichOutputText okToLocatioInputText) {
        this.okToLocatioInputText = okToLocatioInputText;
    }

    public RichOutputText getOkToLocatioInputText() {
        return okToLocatioInputText;
    }

    public void setOkGoToLocationPpoup(RichPopup okGoToLocationPpoup) {
        this.okGoToLocationPpoup = okGoToLocationPpoup;
    }

    public RichPopup getOkGoToLocationPpoup() {
        return okGoToLocationPpoup;
    }

    public String okToLocationAction() {
        this.getOkGoToLocationPpoup().hide();
        String action = this.getOkGoToLocationPpoup().getLauncherVar();
        if (action == null) {
            this.getConfFromContainerId().setDisabled(false);
            this.refreshContentOfUIComponent(this.getConfFromContainerId());
        }
        return action;
    }

    public String f1YesConfirmLinkAction() {
        this.getExitPopup().hide();
        String action = null;
        HhContainerPickSBean pfBean = getContainerPickBean();
        if (pfBean.isCalledFromMenu()) {
            action = HhContainerPickSBean.PAGE1_ACTION;
        } else {
            action = this.logoutExitBTF();
        }
        return action;
    }

    public void setSuccesPopUpLink(RichLink succesPopUpLink) {
        this.succesPopUpLink = succesPopUpLink;
    }

    public RichLink getSuccesPopUpLink() {
        return succesPopUpLink;
    }

    public void setExitPopUpLink(RichLink exitPopUpLink) {
        this.exitPopUpLink = exitPopUpLink;
    }

    public RichLink getExitPopUpLink() {
        return exitPopUpLink;
    }

    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }

    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }

    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null && !currentPageDef.startsWith(CONTAINERPICK_PAGEDEF_NAME)) {
            navigation = true;
        }
        return navigation;
    }

    private HhContainerPickSBean getContainerPickBean() {
        return (HhContainerPickSBean) this.getPageFlowBean(BEAN_CONT_PICK);
    }
}
