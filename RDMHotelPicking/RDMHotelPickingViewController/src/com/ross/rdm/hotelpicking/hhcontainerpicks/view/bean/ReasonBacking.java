package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Reason.jspx
// ---
// ---------------------------------------------------------------------
public class ReasonBacking extends ContainerPickBaseBean 
{
    @SuppressWarnings("compatibility:-2774698315301855243")
    private static final long serialVersionUID = 1L;

    private RichSelectOneChoice codeSelectOneChoice;
    private RichSelectOneChoice codeMarkSelectOneChoice;
    private RichIcon iconCode;
    private RichIcon iconCodeMark;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f6ListLink;
    
    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;
    
    private RichPopup exitPopup;
    private RichOutputText dialogOutputText;

    public ReasonBacking() {

    }

    public String exitAction() 
    {
        if(YES.equals(ADFUtils.getBoundAttributeValue("Consolidate"))){
            ADFUtils.setBoundAttributeValue("Consolidate", NO);
        }
        ADFUtils.findOperation("clearBlock").execute();
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        String previousItem = (String) ADFUtils.getBoundAttributeValue("PreviousItem");
        if(HhContainerPickSBean.CONTAINER_PICK_BLOCK.equals(previousItem)){
            pfBean.setIsFocusOn(CONFIRM_CONT);
        }
        else if(HhContainerPickSBean.CONTAINER_PICK_TWO_BLOCK.equals(previousItem)){
            // TODO ?
        }
        else if(HhContainerPickSBean.FROM_MASTER_CHILD_BLOCK.equals(previousItem)){
            // TODO ?
        }
        else if(HhContainerPickSBean.TO_MASTER_CHILD_BLOCK.equals(previousItem)){
            // TODO ?  
        }
        return pfBean.getActionByBlockName(previousItem);
    }

    public String doneAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_REASON_F4,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                            
                     boolean vReasonCode = true;
                     if(YES.equals(ADFUtils.getBoundAttributeValue("Consolidate"))){
                         if(ADFUtils.getBoundAttributeValue("CodeMark") == null
                             || "".equals(ADFUtils.getBoundAttributeValue("CodeMark"))){
                             vReasonCode = false;
                             this.showMessagesPanel(ERROR, this.getMessage("INV_REASON_CODE", ERROR, 
                                                                         (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                                         (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                             
                             this.addErrorStyleToChoiceComponent(this.getCodeMarkSelectOneChoice());
                             this.getIconCodeMark().setName(ERR_ICON);
                             this.refreshContentOfUIComponent(this.getIconCodeMark());
                             this.setFocusOnUIComponent(this.getCodeMarkSelectOneChoice());
                         }
                         else{
                             this.hideMessagesPanel();
                             this.removeErrorStyleToChoiceComponent(this.getCodeMarkSelectOneChoice());
                             this.getIconCodeMark().setName(REQ_ICON);
                             this.refreshContentOfUIComponent(this.getIconCodeMark());
                         }
                     }
                     else{
                         if(ADFUtils.getBoundAttributeValue("Code") == null
                             || "".equals(ADFUtils.getBoundAttributeValue("Code"))){
                             vReasonCode = false;
                             this.showMessagesPanel(ERROR, this.getMessage("INV_REASON_CODE", ERROR, 
                                                                         (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                                         (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                             
                             this.addErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
                             this.getIconCode().setName(ERR_ICON);
                             this.refreshContentOfUIComponent(this.getIconCode());
                             this.setFocusOnUIComponent(this.getCodeSelectOneChoice());
                         }
                         else{
                             this.hideMessagesPanel();
                             this.removeErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
                             this.getIconCode().setName(REQ_ICON);
                             this.refreshContentOfUIComponent(this.getIconCode());
                         }
                     }
                     
                     if(vReasonCode){
                         HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                         if(YES.equals(ADFUtils.getBoundAttributeValue("Consolidate"))){
                             OperationBinding op = ADFUtils.findOperation("markConsolidate");
                             op.execute();
                             if(op.getErrors() == null || op.getErrors().isEmpty()){
                                 this.showMessagesPanel(MSGTYPE_M, this.getMessage("SUCCESS_OPER", MSGTYPE_M, 
                                                                             (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                                             (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                                 action = pfBean.getActionByBlockName((String) ADFUtils.getBoundAttributeValue("PreviousItem"));
                             }
                         }
                         else{
                             OperationBinding op = ADFUtils.findOperation("callByPassPick");
                             op.getParamsMap().put("callingBlock", "REASON");
                             List<String> result = (List<String>) op.execute();
                             if(result != null){
                                 if(result.size() == 1){
                                     action = pfBean.getActionByBlockName(result.get(0));
                                 }
                                 else{
                                     this.showMessagesPanel(result.get(0), result.get(1));
                                 }                                        
                             }
                         }
                         
                         if(this.logoutExitBTF().equals(action)){
                             action = null;
                             this.getCodeSelectOneChoice().setDisabled(true);
                             this.getCodeMarkSelectOneChoice().setDisabled(true);
                             this.refreshContentOfUIComponent(this.getCodeSelectOneChoice());
                             this.refreshContentOfUIComponent(this.getCodeMarkSelectOneChoice());
                             
                             String message = this.getMessage("NO_PICKS", WARN,
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                             this.getDialogOutputText().setValue(message);
                             this.getExitPopup().show(new RichPopup.PopupHints());
                         }
                         else if(HhContainerPickSBean.CONTAINER_PICK_ACTION.equals(action)){
                             pfBean.setIsFocusOn(CONFIRM_CONT);
                         }
                         else if(HhContainerPickSBean.CONTAINER_PICK_TWO_ACTION.equals(action)){
                             // TODO
                         }
                         else if(HhContainerPickSBean.FROM_MASTER_CHILD_ACTION.equals(action)){
                             // TODO
                         }
                         else if(HhContainerPickSBean.TO_MASTER_CHILD_ACTION.equals(action)){
                             // TODO    
                         }
                     }
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
       
        return action;
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.checkEmptyItem();
                }
                else{
                    this.hideMessagesPanel();
                    if(YES.equals(ADFUtils.getBoundAttributeValue("Consolidate"))){
                        this.removeErrorStyleToChoiceComponent(this.getCodeMarkSelectOneChoice());
                        this.getIconCodeMark().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getIconCodeMark());
                    }
                    else{
                        this.removeErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
                        this.getIconCode().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getIconCode());
                    }
                }
            }
            else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } 
            else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            }
            else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6ListLink());
                actionEvent.queue();
            }
        }
    }
    
    private void checkEmptyItem() {
        this.showMessagesPanel(ERROR, this.getMessage("INV_REASON_CODE", ERROR, 
                                                    (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        
        if(YES.equals(ADFUtils.getBoundAttributeValue("Consolidate"))){
            this.addErrorStyleToChoiceComponent(this.getCodeMarkSelectOneChoice());
            this.getIconCodeMark().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconCodeMark());
            // this.setFocusOnUIComponent(this.getCodeMarkSelectOneChoice());
        }
        else{
            this.addErrorStyleToChoiceComponent(this.getCodeSelectOneChoice());
            this.getIconCode().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconCode());
            // this.setFocusOnUIComponent(this.getCodeSelectOneChoice());
        }
    }
    
    public void performKeyExitPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.yesLinkConfirmExitPopupActionListener(null);
            }
        }
    }
    
    public void yesLinkConfirmExitPopupActionListener(ActionEvent actionEvent) {
        ADFUtils.invokeAction(this.logoutExitBTF());
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setIconCode(RichIcon iconCode) {
        this.iconCode = iconCode;
    }

    public RichIcon getIconCode() {
        return iconCode;
    }

    public void setIconCodeMark(RichIcon iconCodeMark) {
        this.iconCodeMark = iconCodeMark;
    }

    public RichIcon getIconCodeMark() {
        return iconCodeMark;
    }

    public void setF6ListLink(RichLink f6ListLink) {
        this.f6ListLink = f6ListLink;
    }

    public RichLink getF6ListLink() {
        return f6ListLink;
    }

    public void setCodeSelectOneChoice(RichSelectOneChoice codeSelectOneChoice) {
        this.codeSelectOneChoice = codeSelectOneChoice;
    }

    public RichSelectOneChoice getCodeSelectOneChoice() {
        return codeSelectOneChoice;
    }

    public void setCodeMarkSelectOneChoice(RichSelectOneChoice codeMarkSelectOneChoice) {
        this.codeMarkSelectOneChoice = codeMarkSelectOneChoice;
    }

    public RichSelectOneChoice getCodeMarkSelectOneChoice() {
        return codeMarkSelectOneChoice;
    }
    
    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }
    
    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public void hideMessagesPanel()
    {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public boolean isErrorOnThePage(){
        boolean error = false;
        if(this.getIconErrorMessage() != null){
            error = this.getIconErrorMessage().isVisible() && ERR_ICON.equals(this.getIconErrorMessage().getName());
        }
        return error;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        if(!HhContainerPickSBean.REASON_BLOCK.equals(ADFUtils.getBoundAttributeValue("MainBlock"))){
            ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.REASON_BLOCK);
            RichSelectOneChoice lov = null;
            String clientId = null;
            if(YES.equals(ADFUtils.getBoundAttributeValue("Consolidate"))){
                lov = getCodeMarkSelectOneChoice();
                clientId = lov.getClientId(FacesContext.getCurrentInstance());
            }
            else{
                lov = getCodeSelectOneChoice();
                clientId = lov.getClientId(FacesContext.getCurrentInstance());
            }
            String script = "expandSelectOneChoiceContainerPickReason('" + clientId + "');";
            this.writeJavaScriptToClient(script.toString());
            refreshContentOfUIComponent(lov);
        }
    }
    
    public String f6Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_REASON_F6,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {
            writeJavaScriptToClient("onClickOnCodeLovContainerPickReason(null);");
        } else {
            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }
        return null;
    }
}
