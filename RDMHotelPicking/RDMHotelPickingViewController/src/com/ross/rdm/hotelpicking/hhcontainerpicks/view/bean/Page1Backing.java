package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMHotelPickingBackingBean {
    @SuppressWarnings("compatibility:-1895582425498173493")
    private static final long serialVersionUID = 1L;

    private RichInputText masterContainer;
    private RichInputText firstContainer;
    private RichInputText lastContainer;
    private RichIcon iconMaster;
    private RichIcon iconFirst;
    private RichIcon iconLast;
    private RichLink f1DisplayLink;
    private RichLink f3ExitLink;
    private RichPopup confirmPopup1;
    private RichOutputText dialogOutputText1;
    private RichPopup confirmPopup2;
    private RichOutputText dialogOutputText2;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;
    private RichLink masterContainerHiddenLink;
    private RichLink firstContainerHiddenLink;
    private RichLink lastContainerHiddenLink;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;

    private final static String MASTER_CONTAINER_ITEM = "mcid";
    private final static String FIRST_CONTAINER_ITEM = "fcid";
    private final static String LAST_CONTAINER_ITEM = "lcid";

    private final static String MASTER_CONTAINER_FIELD = "START_PICK.MASTER_CONTAINER";
    private final static String FIRST_CONTAINER_FIELD = "START_PICK.FIRST_CONTAINER";
    private final static String LAST_CONTAINER_FIELD = "START_PICK.LAST_CONTAINER";

    private final static String PAGE1_PAGEDEF_NAME =
        "com_ross_rdm_hotelpicking_hhcontainerpicks_view_pageDefs_Page1PageDef";

    public Page1Backing() {

    }

    public void onValueChangedMasterContainerStartPick(ValueChangeEvent vce) {
        updateModel(vce);
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        pfBean.setLinkPressedPage1(null);
        ActionEvent actionEvent = new ActionEvent(this.getMasterContainerHiddenLink());
        actionEvent.queue();
    }
    
    public String onValueChangedMasterContainerStartPickHiddenAction() {
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.callRossVMaster((String) ADFUtils.getBoundAttributeValue("MasterContainer"));
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private void callRossVMaster(String newValue) {
        // ISSUE 1595
        this.initVOAttributes();

        OperationBinding oper = ADFUtils.findOperation("rossVMaster");
        oper.getParamsMap().put("masterContainer", newValue.toUpperCase());
        List<String> errorsMessages = (List<String>) oper.execute();
        if (errorsMessages != null && errorsMessages.size() > 1) {
            this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
            this.setErrorOnMasterContainer();
        } else {
            this.hideMessagesPanel();

            this.getMasterContainer().setDisabled(true);
            this.removeErrorStyleToComponent(this.getMasterContainer());
            this.getIconMaster().setName(REQ_ICON);
            this.refreshContentOfUIComponent(this.getIconMaster());
            
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            pfBean.setFocusItemPage1(FIRST_CONTAINER_FIELD);
            this.getFirstContainer().setDisabled(false);
            this.setFocusOnUIComponent(this.getFirstContainer());
        }
    }

    public void onValueChangedFirstContainerStartPick(ValueChangeEvent vce) {
        updateModel(vce);
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        pfBean.setLinkPressedPage1(null);
        ActionEvent actionEvent = new ActionEvent(this.getFirstContainerHiddenLink());
        actionEvent.queue();
    }
    
    public String onValueChangedFirstContainerStartPickHiddenAction(){
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.resetFieldAndIcon(this.getFirstContainer(), this.getIconFirst());
                this.hideMessagesPanel();
                this.getFirstContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getFirstContainer());
                
                pfBean.setFocusItemPage1(LAST_CONTAINER_FIELD);
                this.getLastContainer().setDisabled(false);
                this.setFocusOnUIComponent(this.getLastContainer());
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    public void onValueChangedLastContainerStartPick(ValueChangeEvent vce) {
        updateModel(vce);
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        pfBean.setLinkPressedPage1(null);
        ActionEvent actionEvent = new ActionEvent(this.getLastContainerHiddenLink());
        actionEvent.queue();
    }
    
    public String onValueChangedLastContainerStartPickHiddenAction(){
        if (!this.isNavigationExecuted()) {
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.resetFieldAndIcon(this.getLastContainer(), this.getIconLast());
                this.hideMessagesPanel();
                this.getLastContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getLastContainer());
                
                pfBean.setFocusItemPage1(MASTER_CONTAINER_FIELD);
                this.getMasterContainer().setDisabled(false);
                this.setFocusOnUIComponent(this.getMasterContainer());    
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private void disableAndClearAllFields(boolean clearFields) {
        if (clearFields){
            ADFUtils.setBoundAttributeValue("MasterContainer", null);                
            this.getMasterContainer().resetValue();
            ADFUtils.setBoundAttributeValue("FirstContainer", null);
            this.getFirstContainer().resetValue();
            ADFUtils.setBoundAttributeValue("LastContainer", null);
            this.getLastContainer().resetValue();
        }
        this.getMasterContainer().setDisabled(true);
        this.getFirstContainer().setDisabled(true);
        this.getLastContainer().setDisabled(true);
        this.removeErrorStyleToComponent(this.getMasterContainer());
        this.removeErrorStyleToComponent(this.getFirstContainer());
        this.removeErrorStyleToComponent(this.getLastContainer());
    }

    //F1-Display
    public String displayAction() {
        String action = null;
        
            if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_START_PICK_F1,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                       
                // HIDE THE PREVIOUS ERROR
                this.hideMessagesPanel();
                
                String masterContainer = (String) ADFUtils.getBoundAttributeValue("MasterContainer");
                String firstContainer = (String) ADFUtils.getBoundAttributeValue("FirstContainer");
                String lastContainer = (String) ADFUtils.getBoundAttributeValue("LastContainer");

                if (StringUtils.isNotEmpty(masterContainer) &&
                    ((StringUtils.isNotEmpty(firstContainer) && StringUtils.isNotEmpty(lastContainer)) ||
                     (StringUtils.isEmpty(firstContainer) && StringUtils.isEmpty(lastContainer)))) {
                    HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                    OperationBinding oper = ADFUtils.findOperation("containerPickPage1F1");
                    ContainerPickResults result = (ContainerPickResults) oper.execute();
                    if (result != null) {
                        if (result.isOperationError()) {
                            if (result.getMessagePopUp() != null && !result.getMessagePopUp().isEmpty()) {
                                /* ISSUE 2061
                                 * if (FIRST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                    this.setErrorOnFirstContainer();
                                } else if (LAST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                    this.setErrorOnLastContainer();
                                } else {
                                    this.setErrorOnMasterContainer();
                                    this.getMasterContainer().setDisabled(true);
                                    this.refreshContentOfUIComponent(this.getMasterContainer());
                                } */
                                this.handlePopUpErrorsDisplayCall(result);
                            } else {
                                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));

                                if (FIRST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                    this.setErrorOnFirstContainer();
                                } else if (LAST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                    this.setErrorOnLastContainer();
                                } else {
                                    this.setErrorOnMasterContainer();
                                }
                            }
                        } else if (result.getIsTrExit()) {
                            // action = pfBean.getActionByBlockName(result.getNavigationBlock());
                            this.disableAndClearAllFields(true);
                            String message =
                                this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        } else if (result.getNavigationBlock() != null) {
                            action = pfBean.getActionByBlockName(result.getNavigationBlock());
                            if(HhContainerPickSBean.TO_LOCATION_ACTION.equals(action)){
                                // ISSUE 2061
                                action = null;
                            }
                            else{
                                pfBean.setNextNavigationPage1(action);
                                pfBean.setIsFocusOn(ContainerPickBaseBean.CONFIRM_CONT);
                            }
                            
                            if (result.getMessagePopUp() != null && !result.getMessagePopUp().isEmpty()) {
                                action = null;
                                this.handlePopUpErrorsDisplayCall(result);
                            }
                            else{
                                this.resetFieldAndIcon(this.getFirstContainer(), this.getIconFirst());
                                this.resetFieldAndIcon(this.getLastContainer(), this.getIconLast());
                                this.resetFieldAndIcon(this.getMasterContainer(), this.getIconMaster());
                                // ADFUtils.setBoundAttributeValue("MasterContainer", null);
                                // this.getMasterContainer().resetValue();
                                
                                pfBean.setFocusItemPage1(MASTER_CONTAINER_FIELD);
                                this.getMasterContainer().setDisabled(false);
                                this.setFocusOnUIComponent(this.getMasterContainer());
                            }
                        } else if (result.getMessagePopUp() != null && !result.getMessagePopUp().isEmpty()) {
                            /* ISSUE 2061
                             * if (FIRST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                this.setErrorOnFirstContainer();
                            } else if (LAST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                this.setErrorOnLastContainer();
                            } else {
                                this.setErrorOnMasterContainer();
                                this.getMasterContainer().setDisabled(true);
                                this.refreshContentOfUIComponent(this.getMasterContainer());
                            } */

                            this.handlePopUpErrorsDisplayCall(result);
                        } else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                            if (FIRST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                this.setErrorOnFirstContainer();
                            } else if (LAST_CONTAINER_FIELD.equals(result.getNavigationItem())) {
                                this.setErrorOnLastContainer();
                            } else {
                                this.setErrorOnMasterContainer();
                            }
                        }
                    }
                } else {
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_ENTRY", ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));

                    this.setErrorOnCurrentField();
                }
            }else{
              
                
                this.showMessagesPanel(ERROR,
                                       this.getMessage("NOT_ALLOWED", ERROR,
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            }
        
        return action;
    }
    
    private void setErrorOnCurrentField(){
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        if(MASTER_CONTAINER_FIELD.equals(pfBean.getFocusItemPage1())) {
            this.setErrorOnMasterContainer();
        } else if (FIRST_CONTAINER_FIELD.equals(pfBean.getFocusItemPage1())) {
            this.setErrorOnFirstContainer();
        } else if (LAST_CONTAINER_FIELD.equals(pfBean.getFocusItemPage1())) {
            this.setErrorOnLastContainer();
        }
    }

    private void handlePopUpErrorsDisplayCall(ContainerPickResults result){
        this.disableAndClearAllFields(false);
        
        String noPicks =
            this.getMessage("NO_PICKS", ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));

        int numMessages = result.getMessagePopUp().size();
        if (numMessages == 1) {
            if (noPicks.equals(result.getMessagePopUp().get(0))) {
                // SHOW AS INFORMATION MESSAGE
                this.getDialogOutputText2().setValue(result.getMessagePopUp().get(0));
                this.getConfirmPopup2().show(new RichPopup.PopupHints());
            } else {
                // SHOW AS WARNING MESSAGE
                this.getDialogOutputText1().setValue(result.getMessagePopUp().get(0));
                this.getConfirmPopup1().show(new RichPopup.PopupHints());
            }
        } else {
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            if (noPicks.equalsIgnoreCase(result.getMessagePopUp().get(numMessages - 1))) {
                pfBean.setPopupCountPage1(numMessages - 2);
                this.getDialogOutputText2().setValue(result.getMessagePopUp().get(numMessages - 1));
            } else {
                pfBean.setPopupCountPage1(numMessages - 1);
            }
            this.getDialogOutputText1().setValue(result.getMessagePopUp().get(0));
            this.getConfirmPopup1().show(new RichPopup.PopupHints());
        }
    }

    
    private void resetFieldAndIcon(RichInputText inputField, RichIcon icon) {  
        if(this.getMasterContainer() == inputField){
            icon.setName(REQ_ICON);
        }
        else
        {
            icon.setName("");
        }
        this.refreshContentOfUIComponent(icon);
        inputField.setDisabled(true);
        this.removeErrorStyleToComponent(inputField);
    }

    private void setErrorFieldAndIcon(RichInputText inputField, RichIcon icon) {
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        inputField.setDisabled(false);
        this.addErrorStyleToComponent(inputField);
        this.setFocusOnUIComponent(inputField);
    }

    private void setErrorOnMasterContainer() {
        this.resetFieldAndIcon(this.getFirstContainer(), this.getIconFirst());
        this.resetFieldAndIcon(this.getLastContainer(), this.getIconLast());
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        pfBean.setFocusItemPage1(MASTER_CONTAINER_FIELD);
        this.setErrorFieldAndIcon(this.getMasterContainer(), this.getIconMaster());
    }

    private void setErrorOnFirstContainer() {
        this.resetFieldAndIcon(this.getMasterContainer(), this.getIconMaster());
        this.resetFieldAndIcon(this.getLastContainer(), this.getIconLast());
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        pfBean.setFocusItemPage1(FIRST_CONTAINER_FIELD);
        this.setErrorFieldAndIcon(this.getFirstContainer(), this.getIconFirst());
    }

    private void setErrorOnLastContainer() {
        this.resetFieldAndIcon(this.getMasterContainer(), this.getIconMaster());
        this.resetFieldAndIcon(this.getFirstContainer(), this.getIconFirst());
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        pfBean.setFocusItemPage1(LAST_CONTAINER_FIELD);
        this.setErrorFieldAndIcon(this.getLastContainer(), this.getIconLast());
    }

    // F3-Exit
    public String exitAction() {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation("callPkgContainerPickingAdfTrExit");
        List<String> errorsMessages = (List<String>) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (errorsMessages != null && errorsMessages.size() == 2) {
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
            } else {
                action = this.logoutExitBTF();
                this.hideMessagesPanel();
            }
        }
        return action;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String item = (String) clientEvent.getParameters().get("item");
            // WE HAVE TO UPDATE THE MODEL...(THIS LOGIC FIRES THE VALUECHANGE EVENT!!!)
            /* if (MASTER_CONTAINER_ITEM.equals(item)) {
                ADFUtils.setBoundAttributeValue("MasterContainer", submittedValue);
            } else if (FIRST_CONTAINER_ITEM.equals(item)) {
                ADFUtils.setBoundAttributeValue("FirstContainer", submittedValue);
            } else if (LAST_CONTAINER_ITEM.equals(item)) {
                ADFUtils.setBoundAttributeValue("LastContainer", submittedValue);
            } */

            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.checkEmptyItem(component, item);
                } else {
                    if (MASTER_CONTAINER_ITEM.equals(item)) {
                        if ((StringUtils.isEmpty((String) this.getMasterContainer().getValue()) &&
                             StringUtils.isEmpty(submittedValue)) ||
                            submittedValue.equals(this.getMasterContainer().getValue())) {
                            this.onValueChangedMasterContainerStartPick(new ValueChangeEvent(this.getMasterContainer(),
                                                                                             null, submittedValue));
                        }
                    } else if (FIRST_CONTAINER_ITEM.equals(item)) {
                        if ((StringUtils.isEmpty((String) this.getFirstContainer().getValue()) &&
                             StringUtils.isEmpty(submittedValue)) ||
                            submittedValue.equals(this.getFirstContainer().getValue())) {
                            this.onValueChangedFirstContainerStartPick(new ValueChangeEvent(this.getFirstContainer(),
                                                                                            null, submittedValue));
                        }
                    } else if (LAST_CONTAINER_ITEM.equals(item)) {
                        if ((StringUtils.isEmpty((String) this.getLastContainer().getValue()) &&
                             StringUtils.isEmpty(submittedValue)) ||
                            submittedValue.equals(this.getLastContainer().getValue())) {
                            this.onValueChangedLastContainerStartPick(new ValueChangeEvent(this.getLastContainer(),
                                                                                           null, submittedValue));
                        }
                    }
                }
            } else if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1DisplayLink());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            }
        }
    }

    public void performKeyFirstPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.yesLinkConfirmFirstPopupActionListener(null);
            }
        }
    }

    public void performKeySecondPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                this.yesLinkConfirmSecondPopupActionListener(null);
            }
        }
    }

    public void yesLinkConfirmFirstPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmPopup1().hide();
        String valText2 = (String) this.getDialogOutputText2().getValue();
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        Integer count = pfBean.getPopupCountPage1();
        if (count != null) {
            pfBean.setPopupCountPage1(count - 1);
            if (pfBean.getPopupCountPage1().intValue() == 0) {
                if (null != valText2 && !valText2.isEmpty()) {
                    this.getConfirmPopup2().show(new RichPopup.PopupHints());
                } else {
                    String nav = pfBean.getNextNavigationPage1();
                    pfBean.setNextNavigationPage1(null);
                    // ISSUE 2061
                    if(HhContainerPickSBean.TO_LOCATION_ACTION.equals(nav)){
                        this.getDialogOutputText1().setValue(null);
                        this.getMasterContainer().setDisabled(false);
                        pfBean.setFocusItemPage1(MASTER_CONTAINER_FIELD);
                        this.setFocusOnUIComponent(this.getMasterContainer());
                    }
                    else{
                        ADFUtils.invokeAction(nav);
                    }
                }
            } else
                this.getConfirmPopup1().show(new RichPopup.PopupHints());
        } else {
            this.getDialogOutputText1().setValue(null);
            this.getMasterContainer().setDisabled(false);
            pfBean.setFocusItemPage1(MASTER_CONTAINER_FIELD);
            this.setFocusOnUIComponent(this.getMasterContainer());
        }
    }

    public void yesLinkConfirmSecondPopupActionListener(ActionEvent actionEvent) {
        this.getDialogOutputText2().setValue(null);
        this.getConfirmPopup2().hide();
        
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        
        this.getMasterContainer().setDisabled(false);
        pfBean.setFocusItemPage1(MASTER_CONTAINER_FIELD);
        this.setFocusOnUIComponent(this.getMasterContainer());
                
        if (pfBean.getNextNavigationPage1() != null) {
            String nav = pfBean.getNextNavigationPage1();
            pfBean.setNextNavigationPage1(null);
            // ISSUE 2061
            if(!HhContainerPickSBean.TO_LOCATION_ACTION.equals(nav)){
                ADFUtils.invokeAction(nav);
            }
        }
    }

    private void checkEmptyItem(RichInputText component, String item) {
        if (MASTER_CONTAINER_ITEM.equals(item)) {
            this.showMessagesPanel(ERROR,
                                   this.getMessage("INV_CONTAINER", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(component);
            this.getIconMaster().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconMaster());
        } else if (FIRST_CONTAINER_ITEM.equals(item)) {
            this.showMessagesPanel(ERROR,
                                   this.getMessage("INV_CONTAINER", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(component);
            this.getIconFirst().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconFirst());
        } else if (LAST_CONTAINER_ITEM.equals(item)) {
            this.showMessagesPanel(ERROR,
                                   this.getMessage("INV_CONTAINER", ERROR,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(component);
            this.getIconLast().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconLast());
        }
    }

    public void setMasterContainer(RichInputText masterContainer) {
        this.masterContainer = masterContainer;
    }

    public RichInputText getMasterContainer() {
        return masterContainer;
    }

    public void setLastContainer(RichInputText lastContainer) {
        this.lastContainer = lastContainer;
    }

    public RichInputText getLastContainer() {
        return lastContainer;
    }

    public void setFirstContainer(RichInputText firstContainer) {
        this.firstContainer = firstContainer;
    }

    public RichInputText getFirstContainer() {
        return firstContainer;
    }

    public void setIconMaster(RichIcon iconMaster) {
        this.iconMaster = iconMaster;
    }

    public RichIcon getIconMaster() {
        return iconMaster;
    }

    public void setIconFirst(RichIcon iconFirst) {
        this.iconFirst = iconFirst;
    }

    public RichIcon getIconFirst() {
        return iconFirst;
    }

    public void setIconLast(RichIcon iconLast) {
        this.iconLast = iconLast;
    }

    public RichIcon getIconLast() {
        return iconLast;
    }

    public void setF1DisplayLink(RichLink f1DisplayLink) {
        this.f1DisplayLink = f1DisplayLink;
    }

    public RichLink getF1DisplayLink() {
        return f1DisplayLink;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setConfirmPopup1(RichPopup confirmPopup1) {
        this.confirmPopup1 = confirmPopup1;
    }

    public RichPopup getConfirmPopup1() {
        return confirmPopup1;
    }

    public void setDialogOutputText1(RichOutputText dialogOutputText1) {
        this.dialogOutputText1 = dialogOutputText1;
    }

    public RichOutputText getDialogOutputText1() {
        return dialogOutputText1;
    }

    public void setConfirmPopup2(RichPopup confirmPopup2) {
        this.confirmPopup2 = confirmPopup2;
    }

    public RichPopup getConfirmPopup2() {
        return confirmPopup2;
    }

    public void setDialogOutputText2(RichOutputText dialogOutputText2) {
        this.dialogOutputText2 = dialogOutputText2;
    }

    public RichOutputText getDialogOutputText2() {
        return dialogOutputText2;
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) {
        _logger.info("onRegionLoad start");
        
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        if(pfBean.getInitErrorMsg() != null){
            String msg = pfBean.getInitErrorMsg();
            pfBean.setInitErrorMsg(null);
            this.disableAndClearAllFields(true);
            
            this.getDmsErrorOutputText().setValue(msg);
            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
        }
        else{
            if (!"HH_CONTAINER_PICK_S".equals(ADFUtils.getBoundAttributeValue("GlobalCallingForm"))) {
                ADFUtils.setBoundAttributeValue("GlobalCallingForm", "HH_CONTAINER_PICK_S");
                ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.PAGE1_BLOCK);
                // ISSUE 1678
                this.initVOAttributes();
            }    
        }
    }

    private void initVOAttributes() {
        ADFUtils.setBoundAttributeValue("PickToContainerId", null);
        ADFUtils.setBoundAttributeValue("FirstPickFlag", "Y");
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null && !currentPageDef.startsWith(PAGE1_PAGEDEF_NAME)) {
            navigation = true;
        }
        return navigation;
    }
    
    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }
    
    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }
    
    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }

    public void setMasterContainerHiddenLink(RichLink masterContainerHiddenLink) {
        this.masterContainerHiddenLink = masterContainerHiddenLink;
    }

    public RichLink getMasterContainerHiddenLink() {
        return masterContainerHiddenLink;
    }
    
    public void setFirstContainerHiddenLink(RichLink firstContainerHiddenLink) {
        this.firstContainerHiddenLink = firstContainerHiddenLink;
    }

    public RichLink getFirstContainerHiddenLink() {
        return firstContainerHiddenLink;
    }

    public void setLastContainerHiddenLink(RichLink lastContainerHiddenLink) {
        this.lastContainerHiddenLink = lastContainerHiddenLink;
    }

    public RichLink getLastContainerHiddenLink() {
        return lastContainerHiddenLink;
    }
}
