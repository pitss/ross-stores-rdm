package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.context.AdfFacesContext;

public class ContainerPickBaseBean extends RDMHotelPickingBackingBean {
    
    static final String HH_CONTAINER_PK_PAGE_FLOW_BEAN = "HhContainerPickSBean";
    static final String ERROR = "E";
    static final String WARN = "W";
    static final String INFO = "I";
    static final String SUCCESS = "S";
    static final String MSGTYPE_M = "M";
    static final String REQ_ICON = "required";
    static final String ERR_ICON = "error";
    static final String WRN_ICON = "warning";
    static final String INF_ICON = "info";
    static final String SCC_ICON = "logo";
    static final String INTRLVG  = "INTRLVG";
    static final String KEY_PRESSED = "keyPressed";
    
    static final String CONFIRM_CONT   = "CONTAINER_PICK.CONF_CONTAINER_ID";
    static final String PICK_CONTAINER = "CONTAINER_PICK.PICK_TO_CONTAINER_ID";
    static final String GEN_CONTAINER  = "CONTAINER_PICK.GENERIC_TO_CONTAINER";
    static final String FROM_LOC    = "CONTAINER_PICK.FROM_LOCATION_ID";
    
    //Attributes
    static final String ATTR_PICK_TO_CONTAINER_ID = "PickToContainerId";
    static final String ATTR_CONF_CONTAINER_ID    = "ConfFromContainerId";
    static final String ATTR_CONF_LOCATION_ID     = "ConfLocationId"; 
    static final String ATTR_GENERIC_TO_CONTAINER = "GenericToContainer";
    static final String ATTR_FROM_LOCATION_ID     = "FromLocationId";
    static final String ATTR_FACILITY_ID          = "FacilityId";
    static final String ATTR_LANGUAGE_CODE        = "LanguageCode";
    static final String ATTR_PICK_FROM_CONT_ID    = "PickFromContainerId";
    
    //Error Codes
    static final String INV_CONTAINER = "INV_CONTAINER";
    static final String NO_PICK_AISLE = "NO_PICK_AISLE";
    static final String NO_PICKS = "NO_PICKS";
    

    final static String BEAN_CONT_PICK = "HhContainerPickSBean";
    
    public ContainerPickBaseBean() {
        super();
    }
    
    String getBoundAttribute(String attrName){
       Object val = ADFUtils.getBoundAttributeValue(attrName);
       if (val != null)
           return (String)val;
       return null;
    }
    
    
    void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set){
       if (set){
           addErrorStyleToComponent(uiComponent); 
           icon.setName(ERR_ICON);             
       }
       else {
           icon.setName(REQ_ICON); 
           removeErrorStyleToComponent(uiComponent);
       }
       AdfFacesContext.getCurrentInstance().addPartialTarget(icon);
       AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }
    

}
