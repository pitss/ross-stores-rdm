package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ContainerPickTwo.jspx
// ---
// ---------------------------------------------------------------------
public class ContainerPickTwoBacking extends RDMHotelPickingBackingBean {

    private RichInputText inputLastPick;
    private RichIcon iconLastPick;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f5FullLink;
    private RichLink f7ByPassLink;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichPopup confirmPopupFMC;
    private RichOutputText dialogOutputTextFMC;
    private RichPopup exitPopup;
    private RichOutputText dialogExitOutputText;
    private RichLink exitPopUpLink;

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;
    
    private RichLink yesLinkConfirmPopup;
    private RichLink noLinkConfirmPopup;
    private RichLink yesLinkConfirmPopupFMC;
    private RichLink noLinkConfirmPopupFMC;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;

    private static final String CNNT_CNCL_START = "CNNT_CNCL_START"; 
    
    private static final String OPR_CONT_PICK_TR_EXIT = "callPkgContainerPickingAdfTrExit";
    private static final String OPR_EMTY_CHECK = "callCheckEmpty";
    private static final String OPR_MARK_LOCATION = "callMarkLocation"; 
    private static final String OPR_LOAD_SCRN_WRAP = "loadTheScrnWrap";
    private static final String OPR_VFROM_CHILD = "callVFromChild";
    private static final String OPR_NAV_TO_LOC = "navigateToLocationInContainerPick";
    private static final String OPR_INCREMENT_LABOR = "callIncrementLabor";
    private static final String OPR_VALIDATE_LAST_PICK = "callPkgContainerPickingAdfValidateLastPick";
    private static final String OPR_FROM_MASTER_CHILD4 = "containerPickFromMasterChildF4";    
    private static final String OPR_CONTAINER_PICKF5 = "containerPickContainerPickTwoF5";
    
    final static String BEAN_CONT_PICK = "HhContainerPickSBean";
    
    public ContainerPickTwoBacking() {

    }

    public void onChangedLastPickContainerPickTwo(ValueChangeEvent vce) 
    {
        String newValue = (String) vce.getNewValue();
        if(newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())){
            this.updateModel(vce);
            this.validateLastPick((String) vce.getNewValue());
        }        
    }
    
    public String exitAction() 
    {
        String action = null;
        if(YES.equals(ADFUtils.getBoundAttributeValue("DropoffRequired"))){
            this.showMessagesPanel(WARN, this.getMessage(CNNT_CNCL_START, WARN, 
                                    (String) ADFUtils.getBoundAttributeValue("FacilityId"), 
                                    (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
        }
        else{
            // TODO -> :global.error_flag := 'Y';
            ADFUtils.setBoundAttributeValue("NewPickToContainer", ADFUtils.getBoundAttributeValue("GenericToContainer"));
            OperationBinding oper = ADFUtils.findOperation(OPR_CONT_PICK_TR_EXIT);
            List<String> errorsMessages = (List<String>) oper.execute();
            if (errorsMessages != null && errorsMessages.size() == 2){
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
            }
            else{
                action = this.logoutExitBTF();
                this.hideMessagesPanel();
            }
        }
        this.setF5FullPressed(true);
        return action;
    }
    
    public String doneAction() 
    {
        String action = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_TWO_F4,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if (this.validateLastPick((String) ADFUtils.getBoundAttributeValue("LastPick"))) {
                         ADFUtils.setBoundAttributeValue("FullDone", "D");
                         if (YES.equals(ADFUtils.getBoundAttributeValue("EmptyPressed")) &&
                             ADFUtils.getBoundAttributeValue("ChildContainerScanned") == null) {
                             OperationBinding oper = ADFUtils.findOperation(OPR_EMTY_CHECK);
                             List<String> result = (List<String>) oper.execute(); 
                             if (result.size() == 1) {
                                 this.hideMessagesPanel();
                                 this.getInputLastPick().setDisabled(true);
                                 this.refreshContentOfUIComponent(this.getInputLastPick());
                                 HhContainerPickSBean pfBean = getContainerPickBean();
                                 pfBean.setIncrementLaborCalled(false);
                                 this.getDialogOutputText().setValue(result.get(0));
                                 this.getConfirmPopup().show(new RichPopup.PopupHints());
                             } else if (result.size() > 1) {
                                 this.showMessagesPanel(result.get(0), result.get(1));
                             } else {
                                 action = this.callLoadTheScrnWrap();
                             }
                         } else {
                             action = this.executeF4DoneLogic();
                         }
                     }         
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
    
        
        return action;
    }
    
    public String yesLinkConfirmPopupAction() {
        String action = null;
        this.getConfirmPopup().hide();
        OperationBinding oper = ADFUtils.findOperation(OPR_MARK_LOCATION);
        String result = (String) oper.execute();
        if("SUCCESS".equals(result)){
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
        }
        HhContainerPickSBean pfBean = getContainerPickBean();
        if(pfBean.isIncrementLaborCalled()){
            pfBean.setIncrementLaborCalled(false);
            action = this.checkLabeledReserve();
        }
        else{
            action = this.callLoadTheScrnWrap();
        }        
        return action;
    }
    
    public String noLinkConfirmPopupAction() {
        String action = null;
        HhContainerPickSBean pfBean = getContainerPickBean();
        if(pfBean.isIncrementLaborCalled()){
            pfBean.setIncrementLaborCalled(false);
            action = this.checkLabeledReserve();
        }
        else{
            action = this.callLoadTheScrnWrap();
        }
        return action;
    }
    
    private String callLoadTheScrnWrap()
    {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation(OPR_LOAD_SCRN_WRAP);
        ContainerPickResults result = (ContainerPickResults) oper.execute();
        if(result.isOperationError()){
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            this.getInputLastPick().setDisabled(false);
            this.addErrorStyleToComponent(this.getInputLastPick());
            this.setFocusOnUIComponent(this.getInputLastPick());
        }
        else if(result.getIsTrExit()){
            HhContainerPickSBean pfBean = getContainerPickBean();
            action = pfBean.getActionByBlockName(result.getNavigationBlock());
            
            if(this.logoutExitBTF().equals(action)){
                action = null;
                this.getInputLastPick().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputLastPick());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                    (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        }
        else{
            if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            }
            // TODO -> navigationBlock & navigationItem ??? Set in pageFlowBean ???
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
            if(oper.getErrors() == null || oper.getErrors().isEmpty()){
                action = this.executeF4DoneLogic();
            }
        }
        
        return action;
    }
    
    private String executeF4DoneLogic()
    {
        String action = null;
        if(YES.equals(ADFUtils.getBoundAttributeValue("SingleContainer"))
            || NO.equals(ADFUtils.getBoundAttributeValue("LabeledReserve")))
        {
            if(YES.equals(ADFUtils.getBoundAttributeValue("LabeledPicking"))
                && YES.equals(ADFUtils.getBoundAttributeValue("CatchWeight"))
                && CONF.equals(ADFUtils.getBoundAttributeValue("PickType")))
            {
                ADFUtils.setBoundAttributeValue("CallingBlock", "CONTAINER_PICK_TWO");
                ADFUtils.setBoundAttributeValue("ItemIdWeight", ADFUtils.getBoundAttributeValue("ItemIdContainerPick"));
                ADFUtils.setBoundAttributeValue("CntrQty", ADFUtils.getBoundAttributeValue("PickContainerQty"));
                ADFUtils.setBoundAttributeValue("TotalWeight", null);
                action = HhContainerPickSBean.WEIGHT_ACTION;
            }
            else{
                HhContainerPickSBean pfBean = null;
                OperationBinding oper = null;
                if(YES.equals(ADFUtils.getBoundAttributeValue("MixedDestFlag"))){
                    oper = ADFUtils.findOperation("callPkgContainerPickingAdfProcessMultiPick");
                }
                else{
                    pfBean = getContainerPickBean();
                    if("1".equals(pfBean.getPVersion())){
                        oper = ADFUtils.findOperation("callPkgContainerPickingAdfProcessSinglePick");
                    }
                    else{
                        oper = ADFUtils.findOperation("callPkgContainerPickingAdfProcessSinglePick2");
                    }
                }
                oper.getParamsMap().put("pSystemCursorField", "CONTAINER_PICK_TWO.LAST_PICK");
                List<String> result = (List<String>) oper.execute();
                if(result != null && result.size() == 2){
                    // ERROR CASE
                    this.showMessagesPanel(result.get(0), result.get(1));
                    this.getInputLastPick().setDisabled(false);
                    this.setFocusOnUIComponent(this.getInputLastPick());
                }
                else if(result != null && result.size() == 1){
                    action = result.get(0);
                    if(this.logoutExitBTF().equals(action)){
                        action = null;
                        this.getInputLastPick().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getInputLastPick());
                        String message =
                            this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                            (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                        this.getDmsErrorOutputText().setValue(message);
                        this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                    }
                }
                else{
                    ADFUtils.findOperation(OPR_INCREMENT_LABOR).execute();
                    oper = ADFUtils.findOperation(OPR_EMTY_CHECK);
                    result = (List<String>) oper.execute();
                    if (result.size() == 1){
                        this.hideMessagesPanel();
                        this.getInputLastPick().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getInputLastPick());
                        pfBean = getContainerPickBean();
                        pfBean.setIncrementLaborCalled(true);
                        this.getDialogOutputText().setValue(result.get(0));
                        this.getConfirmPopup().show(new RichPopup.PopupHints());
                    }
                    else if(result.size() > 1){
                        this.showMessagesPanel(result.get(0), result.get(1));
                        this.getInputLastPick().setDisabled(false);
                        this.setFocusOnUIComponent(this.getInputLastPick());
                    }
                    else{
                        this.checkLabeledReserve();
                    }
                }
            }
        }
        else{
            OperationBinding oper = ADFUtils.findOperation(OPR_VALIDATE_LAST_PICK);
            oper.getParamsMap().put("getCountIn", YES);
            List result = (List) oper.execute();
            if (result != null && result.size() == 2) {
                // ERROR CASE
                this.showMessagesPanel((String) result.get(0), (String) result.get(1));
                this.getIconLastPick().setName(ERR_ICON);
                this.addErrorStyleToComponent(this.getInputLastPick());
                this.refreshContentOfUIComponent(this.getIconLastPick());
                this.getInputLastPick().setDisabled(false);
                this.setFocusOnUIComponent(this.getInputLastPick());
            } 
            else {
                this.hideMessagesPanel();
                
                BigDecimal sortOrder = (BigDecimal) ADFUtils.getBoundAttributeValue("SortOrder");
                if(sortOrder.intValue() > 0){
                    action = HhContainerPickSBean.FROM_MASTER_CHILD_ACTION;
                }
            }
        }
        
        return action;
    }
    
    private String checkLabeledReserve()
    {
        String action = null;
        this.hideMessagesPanel();
        if(NO.equals(ADFUtils.getBoundAttributeValue("LabeledReserve")))
        {
            OperationBinding oper = ADFUtils.findOperation(OPR_LOAD_SCRN_WRAP);
            ContainerPickResults result = (ContainerPickResults) oper.execute();
            if(result.isOperationError()){
                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                this.getInputLastPick().setDisabled(false);
                this.addErrorStyleToComponent(this.getInputLastPick());
                this.setFocusOnUIComponent(this.getInputLastPick());
            }
            else if(result.getIsTrExit()){
                HhContainerPickSBean pfBean = getContainerPickBean();
                action = pfBean.getActionByBlockName(result.getNavigationBlock());
                
                if(this.logoutExitBTF().equals(action)){
                    action = null;
                    this.getInputLastPick().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputLastPick());
                    String message =
                        this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                    this.getDmsErrorOutputText().setValue(message);
                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                }
            }
            else if(result.getNavigationBlock() != null){
                if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                    this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                }
                oper = ADFUtils.findOperation(OPR_DO_COMMIT);
                oper.execute();
                if(oper.getErrors() == null || oper.getErrors().isEmpty()){
                    HhContainerPickSBean pfBean = getContainerPickBean();
                    if(result.getNavigationItem() != null){
                        pfBean.setIsFocusOn(result.getNavigationBlock() + "." +result.getNavigationItem());
                    }
                    action = pfBean.getActionByBlockName(result.getNavigationBlock());
                }
            }
            else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
                this.getInputLastPick().setDisabled(false);
                this.setFocusOnUIComponent(this.getInputLastPick());
            }
        }
        else{
            ADFUtils.setBoundAttributeValue("ChildFromContainer", ADFUtils.getBoundAttributeValue("ConfFromContainerId"));
            OperationBinding oper = ADFUtils.findOperation(OPR_VFROM_CHILD);
            List<String> result = (List<String>) oper.execute();
            if(result != null && result.size() == 2){
                this.showMessagesPanel(result.get(0), result.get(1));
                this.getInputLastPick().setDisabled(false);
                this.setFocusOnUIComponent(this.getInputLastPick());
            }
            else{
                ADFUtils.setBoundAttributeValue("ChildContainerScanned", YES);
                ADFUtils.setBoundAttributeValue("DropoffRequired", YES);
                
                action = this.executeF4FromMasterChild();
            }
        }
        
        return action;
    }
    
    private String executeF4FromMasterChild()
    {
        String action = null;
        if(ADFUtils.getBoundAttributeValue("ChildFromContainer") == null
            || "".equals(ADFUtils.getBoundAttributeValue("ChildFromContainer")))
        {
            if("F".equals(ADFUtils.getBoundAttributeValue("FullDone"))){
                OperationBinding oper = ADFUtils.findOperation(OPR_NAV_TO_LOC);
                oper.getParamsMap().put("fullDoneFlagIn", "F");
                List<String> results = (List<String>) oper.execute();
                if (results != null && results.size() == 2){
                    this.showMessagesPanel(results.get(0), results.get(1));
                    this.getInputLastPick().setDisabled(false);
                    this.setFocusOnUIComponent(this.getInputLastPick());
                }
                else if (results != null && results.size() == 1){
                    this.hideMessagesPanel();
                    HhContainerPickSBean pfBean = getContainerPickBean();
                    action = pfBean.getActionByBlockName(results.get(0));
                    if(this.logoutExitBTF().equals(action)){
                        action = null;
                        this.getInputLastPick().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getInputLastPick());
                        String message =
                            this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                            (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                        this.getDmsErrorOutputText().setValue(message);
                        this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                    }
                }
            }
            else{
                BigDecimal fmChQty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQtyFMC");
                if(fmChQty == null){
                    fmChQty = new BigDecimal(0);
                }
                BigDecimal cp2Qty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQty");
                if(cp2Qty == null){
                    cp2Qty = new BigDecimal(0);
                }
                
                if(!fmChQty.equals(cp2Qty)){ 
                    this.getInputLastPick().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputLastPick());
                    String message = this.getMessage(DIFF_PICK, CONF,
                                                   (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                   (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                    this.getDialogOutputTextFMC().setValue(message);
                    this.getConfirmPopupFMC().show(new RichPopup.PopupHints());
                }
                else{
                    OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD4);
                    List<String> errorsMessages = (List<String>) oper.execute();
                    if(errorsMessages != null){
                        if (errorsMessages.size() == 1){
                            this.hideMessagesPanel();
                            HhContainerPickSBean pfBean = getContainerPickBean();
                            action = pfBean.getActionByBlockName(errorsMessages.get(0));
                            
                            if(this.logoutExitBTF().equals(action)){
                                action = null;
                                this.getInputLastPick().setDisabled(true);
                                this.refreshContentOfUIComponent(this.getInputLastPick());
                                String message =
                                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                    (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                                this.getDmsErrorOutputText().setValue(message);
                                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                            }
                        }
                        else{
                            this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                            this.getInputLastPick().setDisabled(false);
                            this.setFocusOnUIComponent(this.getInputLastPick());
                        }
                    }
                }
            }
        }
        
        return action;
    }
    
    public String yesLinkConfirmPopupActionFMC() {
        String action = null;
        this.getConfirmPopupFMC().hide();
        OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD4);
        List<String> errorsMessages = (List<String>) oper.execute();
        if(errorsMessages != null){
            if (errorsMessages.size() == 1){
                this.hideMessagesPanel();
                HhContainerPickSBean pfBean = getContainerPickBean();
                action = pfBean.getActionByBlockName(errorsMessages.get(0));
                
                if(this.logoutExitBTF().equals(action)){
                    action = null;
                    this.getInputLastPick().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputLastPick());
                    String message =
                        this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                    this.getDmsErrorOutputText().setValue(message);
                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                }
            }
            else{
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                this.getInputLastPick().setDisabled(false);
                this.setFocusOnUIComponent(this.getInputLastPick());
            }
        }
        return action;
    }
    
    public String noLinkConfirmPopupActionFMC() {
        this.getConfirmPopupFMC().hide();
        this.getInputLastPick().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputLastPick());
        return null;
    }

    public String fullAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_TWO_F5,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if (this.validateLastPick((String) ADFUtils.getBoundAttributeValue("LastPick"))) {
                         OperationBinding oper = ADFUtils.findOperation(OPR_CONTAINER_PICKF5);
                         List<String> result = (List<String>) oper.execute();
                         if(result != null){
                             if (result.size() == 1) {
                                 action = getContainerPickBean().getActionByBlockName(result.get(0));
                                 this.hideMessagesPanel();
                                 this.getIconLastPick().setName("required");
                                 this.removeErrorStyleToComponent(this.getInputLastPick());
                                 this.refreshContentOfUIComponent(this.getIconLastPick());
                                 this.refreshContentOfUIComponent(this.getInputLastPick());
                                 
                                 if(this.logoutExitBTF().equals(action)){
                                     action = null;
                                     this.getInputLastPick().setDisabled(true);
                                     this.refreshContentOfUIComponent(this.getInputLastPick());
                                     String message =
                                         this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                         (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                                     this.getDmsErrorOutputText().setValue(message);
                                     this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                                 }
                             } 
                             else if (result.size() == 2){
                                 this.showMessagesPanel(result.get(0), result.get(1));
                                 this.getIconLastPick().setName(ERR_ICON);
                                 this.addErrorStyleToComponent(this.getInputLastPick());
                                 this.refreshContentOfUIComponent(this.getIconLastPick());
                                 this.setFocusOnUIComponent(this.getInputLastPick());
                             }
                             else if (result.size() == 3){
                                 /* action = ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(result.get(0));
                                 HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                                 pfBean.setExecuteActionFromMasterChild(result.get(1)); */
                                 this.hideMessagesPanel();
                                 this.getIconLastPick().setName("required");
                                 this.removeErrorStyleToComponent(this.getInputLastPick());
                                 this.refreshContentOfUIComponent(this.getIconLastPick());
                                 this.refreshContentOfUIComponent(this.getInputLastPick());
                                 
                                 action = this.executeLogicBeforeNavigate();
                             }
                         }
                     }           
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        this.setF5FullPressed(true);
        return action;
    }
    
    public void setF5FullPressed(boolean f5FullPressed) {
        ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).setF5FullPressed(f5FullPressed);
    }

    public boolean isF5FullPressed() {
        return ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).isF5FullPressed();
    }
    
    private String executeLogicBeforeNavigate()
    {
        String navigation = null;
        if(ADFUtils.getBoundAttributeValue("ChildFromContainer") == null
            || "".equals(ADFUtils.getBoundAttributeValue("ChildFromContainer")))
        {
            HhContainerPickSBean pfBean = getContainerPickBean();
            pfBean.setExecuteActionFromMasterChild(null);
            BigDecimal fmChQty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQtyFMC");
            if(fmChQty == null){
                fmChQty = new BigDecimal(0);
            }
            BigDecimal cp2Qty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQty");
            if(cp2Qty == null){
                cp2Qty = new BigDecimal(0);
            }
            
            if(!fmChQty.equals(cp2Qty)){
                pfBean.setExecuteActionFromMasterChild(DIFF_PICK);
                navigation = HhContainerPickSBean.FROM_MASTER_CHILD_ACTION;
            }
            else{
                oracle.binding.OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD4);
                List<String> results = (List<String>) oper.execute();
                if (results != null) {
                    if (results.size() == 1) {
                        navigation = results.get(0);
                        
                        if(this.logoutExitBTF().equals(navigation)){
                            navigation = null;
                            this.getInputLastPick().setDisabled(true);
                            this.refreshContentOfUIComponent(this.getInputLastPick());
                            String message =
                                this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        }
                    } 
                    else if (results.size() == 2) {
                        pfBean.setExecuteActionFromMasterChild(results.get(0) + "||" + results.get(1));
                        navigation = HhContainerPickSBean.FROM_MASTER_CHILD_ACTION;
                    } 
                    else if (results.size() == 3 && "CHECK_EMPTY".equals(results.get(0))) {
                        pfBean.setExecuteActionFromMasterChild("CHECK_EMPTY");
                        pfBean.setMessageFromMasterChild(results.get(2));
                        navigation = HhContainerPickSBean.FROM_MASTER_CHILD_ACTION;
                    }
                }
            }
        }
        
        return navigation;
    }

    public String byPassAction() {
        String action = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_TWO_F7,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if (this.validateLastPick((String) ADFUtils.getBoundAttributeValue("LastPick"))) {
                         if (ADFUtils.getBoundAttributeValue("PickFromContainerId") != null) {
                             OperationBinding oper = ADFUtils.findOperation("containerPickContainerPickTwoF7");
                             List<String> result = (List<String>) oper.execute();
                             if (result != null) {
                                 if (result.size() == 1) {
                                     HhContainerPickSBean pfBean = getContainerPickBean();
                                     action = pfBean.getActionByBlockName(result.get(0));
                                     pfBean.setDefaultFocusByAction(action);
                                     this.hideMessagesPanel();
                                 } else {
                                     String message =
                                         this.getMessage("NO_PICKS", WARN, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                                         (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
                                     
                                     if(message.equals(result.get(1))){
                                         // EXIT WITH POP UP
                                         this.getInputLastPick().setDisabled(true);
                                         this.refreshContentOfUIComponent(this.getInputLastPick());
                                         
                                         this.getDialogExitOutputText().setValue(message);
                                         this.getExitPopup().show(new RichPopup.PopupHints());
                                     }
                                     else{
                                         this.showMessagesPanel(result.get(0), result.get(1));
                                         this.getIconLastPick().setName(ERR_ICON);
                                         this.refreshContentOfUIComponent(this.getIconLastPick());
                                         this.addErrorStyleToComponent(this.getInputLastPick());                         
                                         this.setFocusOnUIComponent(this.getInputLastPick());
                                     }
                                 }
                             }
                         }
                     }     
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        return action;
    }
    
    public String f1YesConfirmLinkAction() {
        this.getExitPopup().hide();
        this.getInputLastPick().setDisabled(false);
        String action = null;
        HhContainerPickSBean pfBean = getContainerPickBean();
        if(pfBean.isCalledFromMenu()){
            action = HhContainerPickSBean.PAGE1_ACTION;
        }
        else{
            action = this.logoutExitBTF();
        }
        return action;
    }
    
    public void performKeyExitPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitPopUpLink());
                actionEvent.queue();
            }
        }
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) 
        {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            // WE HAVE TO UPDATE THE MODEL...
            ADFUtils.setBoundAttributeValue("LastPick", submittedValue);
            
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.showMessagesPanel(WARN, this.getMessage(PARTIAL_ENTRY, WARN, 
                                                                (String) ADFUtils.getBoundAttributeValue("FacilityId"), 
                                                                (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
                    
                    this.addErrorStyleToComponent(component);
                    this.getIconLastPick().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getIconLastPick());
                    this.setFocusOnUIComponent(component);
                }
                else{
                    this.onChangedLastPickContainerPickTwo(new ValueChangeEvent(this.getInputLastPick(), null, submittedValue));
                }
            }
            else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } 
            else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            }
            else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5FullLink());
                actionEvent.queue();
            } 
            else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7ByPassLink());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkConfirmPopup());
                actionEvent.queue();
            } 
            else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkConfirmPopup());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopupFMC(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkConfirmPopupFMC());
                actionEvent.queue();
            } 
            else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkConfirmPopupFMC());
                actionEvent.queue();
            }
        }
    }
    
    public void setInputLastPick(RichInputText inputLastPick) {
        this.inputLastPick = inputLastPick;
    }

    public RichInputText getInputLastPick() {
        return inputLastPick;
    }

    public void setIconLastPick(RichIcon iconLastPick) {
        this.iconLastPick = iconLastPick;
    }

    public RichIcon getIconLastPick() {
        return iconLastPick;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF5FullLink(RichLink f5FullLink) {
        this.f5FullLink = f5FullLink;
    }

    public RichLink getF5FullLink() {
        return f5FullLink;
    }

    public void setF7ByPassLink(RichLink f7ByPassLink) {
        this.f7ByPassLink = f7ByPassLink;
    }

    public RichLink getF7ByPassLink() {
        return f7ByPassLink;
    }
    
    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }
    
    public void setConfirmPopupFMC(RichPopup confirmPopupFMC) {
        this.confirmPopupFMC = confirmPopupFMC;
    }

    public RichPopup getConfirmPopupFMC() {
        return confirmPopupFMC;
    }

    public void setDialogOutputTextFMC(RichOutputText dialogOutputTextFMC) {
        this.dialogOutputTextFMC = dialogOutputTextFMC;
    }

    public RichOutputText getDialogOutputTextFMC() {
        return dialogOutputTextFMC;
    }
    
    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public void hideMessagesPanel()
    {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public boolean isErrorOnThePage(){
        boolean error = false;
        if ((ADFUtils.getBoundAttributeValue("LastPick") == null || "".equals(ADFUtils.getBoundAttributeValue("LastPick")))
            && NO.equals(ADFUtils.getBoundAttributeValue("CancelPickFlag"))) {
            error = true;
            this.showMessagesPanel(WARN, this.getMessage(PARTIAL_ENTRY, WARN,  
                                                        (String) ADFUtils.getBoundAttributeValue("FacilityId"), 
                                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            
            this.addErrorStyleToComponent(this.getInputLastPick());
            this.getIconLastPick().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconLastPick());
            this.setFocusOnUIComponent(this.getInputLastPick());
        }
        return error;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.CONTAINER_PICK_TWO_BLOCK);
    }

    private boolean validateLastPick(String newValue) {
        boolean valid = true;
        
        if (StringUtils.isEmpty(newValue) && NO.equals(ADFUtils.getBoundAttributeValue("CancelPickFlag"))) {
            this.showMessagesPanel(WARN, this.getMessage(PARTIAL_ENTRY, WARN, 
                                                        (String) ADFUtils.getBoundAttributeValue("FacilityId"), 
                                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            
            this.addErrorStyleToComponent(this.getInputLastPick());
            this.getIconLastPick().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconLastPick());
            this.setFocusOnUIComponent(this.getInputLastPick());
            valid = false;
        } 
        else {
            this.hideMessagesPanel();
            this.removeErrorStyleToComponent(this.getInputLastPick());
            this.getIconLastPick().setName("required");
            this.refreshContentOfUIComponent(this.getIconLastPick());
            
            ADFUtils.setBoundAttributeValue("ChildContainerScanned", YES);
        }

        return valid;
    }

    public void setYesLinkConfirmPopup(RichLink yesLinkConfirmPopup) {
        this.yesLinkConfirmPopup = yesLinkConfirmPopup;
    }

    public RichLink getYesLinkConfirmPopup() {
        return yesLinkConfirmPopup;
    }

    public void setNoLinkConfirmPopup(RichLink noLinkConfirmPopup) {
        this.noLinkConfirmPopup = noLinkConfirmPopup;
    }

    public RichLink getNoLinkConfirmPopup() {
        return noLinkConfirmPopup;
    }
    
    public void setYesLinkConfirmPopupFMC(RichLink yesLinkConfirmPopupFMC) {
        this.yesLinkConfirmPopupFMC = yesLinkConfirmPopupFMC;
    }

    public RichLink getYesLinkConfirmPopupFMC() {
        return yesLinkConfirmPopupFMC;
    }

    public void setNoLinkConfirmPopupFMC(RichLink noLinkConfirmPopupFMC) {
        this.noLinkConfirmPopupFMC = noLinkConfirmPopupFMC;
    }

    public RichLink getNoLinkConfirmPopupFMC() {
        return noLinkConfirmPopupFMC;
    }
    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    public void setDialogExitOutputText(RichOutputText dialogExitOutputText) {
        this.dialogExitOutputText = dialogExitOutputText;
    }

    public RichOutputText getDialogExitOutputText() {
        return dialogExitOutputText;
    }
    
    public void setExitPopUpLink(RichLink exitPopUpLink) {
        this.exitPopUpLink = exitPopUpLink;
    }

    public RichLink getExitPopUpLink() {
        return exitPopUpLink;
    }
    
    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }
    
    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }
    
    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }
    
    private HhContainerPickSBean getContainerPickBean() {
        return (HhContainerPickSBean) this.getPageFlowBean(BEAN_CONT_PICK);
    }
    
    public String getLabelFirst() {
        return subString6(JSFUtils.resolveExpressionAsString("#{resourceLabelBean.attributeLabel('FIRST')}")) + " " +
               JSFUtils.resolveExpressionAsString("#{resourceLabelBean.attributeLabel('CONTAINER_ID')}");
    }

    public String getLabelLast() {
        return subString6(JSFUtils.resolveExpressionAsString("#{resourceLabelBean.attributeLabel('LAST')}")) + " " +
               JSFUtils.resolveExpressionAsString("#{resourceLabelBean.attributeLabel('CONTAINER_ID')}");
    }

    private String subString6(String msg) {
        return msg != null  && msg.length() > 6 ? msg.substring(0, 6) : msg;
    }
}
