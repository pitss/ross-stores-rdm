package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for FromMasterChild.jspx
// ---
// ---------------------------------------------------------------------
public class FromMasterChildBacking extends RDMHotelPickingBackingBean {

    private RichInputText inputChildFromContainer;
    private RichIcon iconChildFromContainer;
    private RichLink f4DoneLink;
    private RichLink f6LpnLink;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichPopup confirmPopupCE;
    private RichOutputText dialogOutputTextCE;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;
    private RichLink yesLinkPopupCE;
    private RichLink noLinkPopupCE;

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;

    private RichDialog okGoToLocationDialog;
    private RichOutputText okToLocatioInputText;
    private RichPopup okGoToLocationPpoup;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;

    public FromMasterChildBacking() {

    }

    public void onValueChangedChildFromContainerFromMasterChild(ValueChangeEvent vce) {
        String newValue = (String)vce.getNewValue();
        if (newValue != null && !newValue.equalsIgnoreCase((String)vce.getOldValue())) {
            this.updateModel(vce);
            OperationBinding oper = ADFUtils.findOperation("validateFromMasterChild");
            oper.getParamsMap().put("childFromContainer", ADFUtils.getBoundAttributeValue("ChildFromContainer"));
            List<String> results = (List<String>)oper.execute();
            if (results != null && results.size() == 2) {
                this.showMessagesPanel(results.get(0), results.get(1));
                this.addErrorStyleToComponent(this.getInputChildFromContainer());
                this.getIconChildFromContainer().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconChildFromContainer());
            } else {
                this.showMessagesPanel(MSGTYPE_M,
                                       this.getMessage("SUCCESS_OPER", MSGTYPE_M,
                                                       (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                this.getInputChildFromContainer().resetValue();
                this.removeErrorStyleToComponent(this.getInputChildFromContainer());
                this.getIconChildFromContainer().setName("required");
                this.refreshContentOfUIComponent(this.getIconChildFromContainer());
            }
        }
    }

    public String doneAction() {
        String action = null;

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {

            if (ADFUtils.getBoundAttributeValue("ChildFromContainer") == null ||
                "".equals(ADFUtils.getBoundAttributeValue("ChildFromContainer"))) {
                if ("F".equals(ADFUtils.getBoundAttributeValue("FullDone"))) {
                    OperationBinding oper = ADFUtils.findOperation("navigateToLocationInContainerPick");
                    oper.getParamsMap().put("fullDoneFlagIn", "F");
                    List<String> results = (List<String>)oper.execute();
                    if (results != null && results.size() == 2) {
                        action = null;
                        this.showMessagesPanel(results.get(0), results.get(1));
                    } else if (results != null && results.size() == 1) {
                        this.hideMessagesPanel();
                        action =
                            ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));

                        if (this.logoutExitBTF().equals(action)) {
                            action = null;
                            this.getInputChildFromContainer().setDisabled(true);
                            this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                            String message =
                                this.getMessage(DMS_ERROR, ERROR,
                                                (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                            this.getDmsErrorOutputText().setValue(message);
                            this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                        }
                    }
                } else {
                    BigDecimal fmChQty = (BigDecimal)ADFUtils.getBoundAttributeValue("PickContainerQty");
                    if (fmChQty == null) {
                        fmChQty = new BigDecimal(0);
                    }
                    BigDecimal cp2Qty = (BigDecimal)ADFUtils.getBoundAttributeValue("PickContainerQtyCP2");
                    if (cp2Qty == null) {
                        cp2Qty = new BigDecimal(0);
                    }

                    if (!fmChQty.equals(cp2Qty)) {
                        String message =
                            this.getMessage(DIFF_PICK, CONF, (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                            (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                        this.showConfirmPopup(message);
                    } else {
                        OperationBinding oper = ADFUtils.findOperation("containerPickFromMasterChildF4");
                        List<String> results = (List<String>)oper.execute();
                        if (results != null) {
                            if (results.size() == 1) {
                                this.hideMessagesPanel();
                                action =
                                    ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));

                                if (this.logoutExitBTF().equals(action)) {
                                    action = null;
                                    this.getInputChildFromContainer().setDisabled(true);
                                    this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                                    String message =
                                        this.getMessage(DMS_ERROR, ERROR,
                                                        (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                        (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                                    this.getDmsErrorOutputText().setValue(message);
                                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                                }
                            } else if (results.size() == 2) {
                                action = null;
                                this.showMessagesPanel(results.get(0), results.get(1));
                            } else if (results.size() == 3) {
                                action = null;
                                this.showConfirmPopupCE(results.get(2));
                            } else {
                                action = this.continueF4Logic();
                            }
                        }
                    }
                }
            }
        } else {


            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }

        //this.setF5FullPressed(false);
        return action;
    }

    public void setF5FullPressed(boolean f5FullPressed) {
        ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).setF5FullPressed(f5FullPressed);
    }

    public boolean isF5FullPressed() {
        return ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).isF5FullPressed();
    }

    public String lpnAction() {
        String action = null;

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F6,
                                                RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)) {

            OperationBinding oper = ADFUtils.findOperation("callDisplayLPNs");
            oper.getParamsMap().put("callingBlock", HhContainerPickSBean.FROM_MASTER_CHILD_BLOCK);
            List<String> results = (List<String>)oper.execute();
            if (results != null) {
                if (results.size() == 1) {
                    action =
                        ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                } else {
                    this.showMessagesPanel(results.get(0), results.get(1));
                }
            }
        } else {


            this.showMessagesPanel(ERROR,
                                   this.getMessage("NOT_ALLOWED", ERROR,
                                                   (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }

        return action;
    }

    public String yesLinkConfirmPopupAction() {
        String action = null;
        this.getInputChildFromContainer().setDisabled(false);
        this.setFocusOnUIComponent(getInputChildFromContainer());
        this.getConfirmPopup().hide();
        OperationBinding oper = ADFUtils.findOperation("containerPickFromMasterChildF4");
        List<String> results = (List<String>)oper.execute();
        if (results != null) {
            if (results.size() == 1) {
                this.hideMessagesPanel();
                HhContainerPickSBean pfBean = (HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean");
                action = pfBean.getActionByBlockName(results.get(0));

                if (this.logoutExitBTF().equals(action)) {
                    action = null;
                    this.getInputChildFromContainer().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                    String message =
                        this.getMessage(DMS_ERROR, ERROR, (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                        (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                    this.getDmsErrorOutputText().setValue(message);
                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                }
            } else if (results.size() == 2) {
                this.showMessagesPanel(results.get(0), results.get(1));
            } else if (results.size() == 3) {
                this.showConfirmPopupCE(results.get(2));
            } else {
                action = this.continueF4Logic();
            }
        }
        return action;
    }

    public String noLinkConfirmPopupAction() {
        this.getInputChildFromContainer().setDisabled(false);
        this.setFocusOnUIComponent(getInputChildFromContainer());
        this.getConfirmPopup().hide();
        return null;
    }

    public String yesLinkConfirmPopupCEAction() {
        OperationBinding oper = ADFUtils.findOperation("callMarkLocation");
        String result = (String)oper.execute();
        if ("SUCCESS".equals(result)) {
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
        }
        return this.continueF4Logic();
    }

    public String noLinkConfirmPopupCEAction() {
        return this.continueF4Logic();
    }

    private String continueF4Logic() {
        String action = null;
        this.getInputChildFromContainer().setDisabled(false);
        this.setFocusOnUIComponent(getInputChildFromContainer());
        this.getConfirmPopupCE().hide();
        OperationBinding oper = ADFUtils.findOperation("loadTheScrnWrap");
        ContainerPickResults result = (ContainerPickResults)oper.execute();
        if (result.containsMessage() && (null == result.getNavigationItem()) ||
            (result.getNavigationItem()).isEmpty()) {
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
        } else if (result.getIsTrExit()) {
            HhContainerPickSBean pfBean = (HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean");
            action = pfBean.getActionByBlockName(result.getNavigationBlock());

            if (this.logoutExitBTF().equals(action)) {
                action = null;
                this.getInputChildFromContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        } else if (result.getNavigationBlock() != null) {
            if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                if (!"TO_LOCATION".equalsIgnoreCase(result.getNavigationBlock()))
                    this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            }
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                HhContainerPickSBean pfBean = (HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean");
                if (result.getNavigationItem() != null) {
                    pfBean.setIsFocusOn(result.getNavigationBlock() + "." + result.getNavigationItem());
                }
                String msgReturn = result.getMessage() == null ? "" : result.getMessage().get(1);
                if (msgReturn != null && !msgReturn.isEmpty()) {
                    if ("TO_LOCATION".equalsIgnoreCase(result.getNavigationBlock())) {
                        this.getInputChildFromContainer().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                        this.getOkToLocatioInputText().setValue(msgReturn);
                        this.getOkGoToLocationPpoup().setLauncherVar(pfBean.getActionByBlockName(result.getNavigationBlock()));
                        this.getOkGoToLocationPpoup().show(new RichPopup.PopupHints());
                        return null;
                    }
                }
                action = pfBean.getActionByBlockName(result.getNavigationBlock());
            }
        } else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
        }
        return action;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String)clientEvent.getParameters().get("submittedValue");
            // WE HAVE TO UPDATE THE MODEL...
            ADFUtils.setBoundAttributeValue("ChildFromContainer", submittedValue);

            Double keyPressed = (Double)clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText)clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_CONTAINER", ERROR,
                                                           (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                    this.addErrorStyleToComponent(component);
                    this.getIconChildFromContainer().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getIconChildFromContainer());
                } else {
                    this.onValueChangedChildFromContainerFromMasterChild(new ValueChangeEvent(this.getInputChildFromContainer(),
                                                                                              null, submittedValue));
                }
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6LpnLink());
                actionEvent.queue();
            }
        }
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double)clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }

    public void performKeyPopupCE(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double)clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopupCE());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopupCE());
                actionEvent.queue();
            }
        }
    }

    public void setIconChildFromContainer(RichIcon iconChildFromContainer) {
        this.iconChildFromContainer = iconChildFromContainer;
    }

    public RichIcon getIconChildFromContainer() {
        return iconChildFromContainer;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF6LpnLink(RichLink f6LpnLink) {
        this.f6LpnLink = f6LpnLink;
    }

    public RichLink getF6LpnLink() {
        return f6LpnLink;
    }

    public void setInputChildFromContainer(RichInputText inputChildFromContainer) {
        this.inputChildFromContainer = inputChildFromContainer;
    }

    public RichInputText getInputChildFromContainer() {
        return inputChildFromContainer;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setConfirmPopupCE(RichPopup confirmPopupCE) {
        this.confirmPopupCE = confirmPopupCE;
    }

    public RichPopup getConfirmPopupCE() {
        return confirmPopupCE;
    }

    public void setDialogOutputTextCE(RichOutputText dialogOutputTextCE) {
        this.dialogOutputTextCE = dialogOutputTextCE;
    }

    public RichOutputText getDialogOutputTextCE() {
        return dialogOutputTextCE;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if ("W".equals(messageType)) {
            this.getIconErrorMessage().setName("warning");
        } else if ("I".equals(messageType)) {
            this.getIconErrorMessage().setName("info");
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName("logo");
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public boolean isErrorOnThePage() {
        boolean error = false;
        if (this.getIconErrorMessage() != null) {
            error = this.getIconErrorMessage().isVisible() && ERR_ICON.equals(this.getIconErrorMessage().getName());
        }
        return error;
    }

    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.FROM_MASTER_CHILD_BLOCK);
        HhContainerPickSBean pfBean = (HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean");
        if (DIFF_PICK.equals(pfBean.getExecuteActionFromMasterChild())) {
            String message =
                this.getMessage(DIFF_PICK, CONF, (String)ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                (String)ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));

            showConfirmPopup(message);
        } else if (CHECK_EMPTY.equals(pfBean.getExecuteActionFromMasterChild())) {
            String message = pfBean.getMessageFromMasterChild();
            pfBean.setExecuteActionFromMasterChild(null);
            pfBean.setMessageFromMasterChild(null);

            this.showConfirmPopupCE(message);
        } else if (pfBean.getExecuteActionFromMasterChild() != null &&
                   pfBean.getExecuteActionFromMasterChild().contains("||")) {
            String executeAction = pfBean.getExecuteActionFromMasterChild();

            String[] messages = executeAction.split("||");
            this.showMessagesPanel(messages[0], messages[1]);
        }
        pfBean.setExecuteActionFromMasterChild(null);
    }

    private void showConfirmPopup(String message) {
        this.getInputChildFromContainer().setDisabled(true);
        this.refreshContentOfUIComponent(getInputChildFromContainer());
        this.getDialogOutputText().setValue(message);
        this.getConfirmPopup().show(new RichPopup.PopupHints());
    }

    private void showConfirmPopupCE(String message) {
        this.getInputChildFromContainer().setDisabled(true);
        this.refreshContentOfUIComponent(getInputChildFromContainer());
        this.getDialogOutputTextCE().setValue(message);
        this.getConfirmPopupCE().show(new RichPopup.PopupHints());
    }

    public void setOkGoToLocationDialog(RichDialog okGoToLocationDialog) {
        this.okGoToLocationDialog = okGoToLocationDialog;
    }

    public RichDialog getOkGoToLocationDialog() {
        return okGoToLocationDialog;
    }

    public void setOkToLocatioInputText(RichOutputText okToLocatioInputText) {
        this.okToLocatioInputText = okToLocatioInputText;
    }

    public RichOutputText getOkToLocatioInputText() {
        return okToLocatioInputText;
    }

    public void setOkGoToLocationPpoup(RichPopup okGoToLocationPpoup) {
        this.okGoToLocationPpoup = okGoToLocationPpoup;
    }

    public RichPopup getOkGoToLocationPpoup() {
        return okGoToLocationPpoup;
    }

    public String okToLocationAction() {
        return this.getOkGoToLocationPpoup().getLauncherVar();
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public void setYesLinkPopupCE(RichLink yesLinkPopupCE) {
        this.yesLinkPopupCE = yesLinkPopupCE;
    }

    public RichLink getYesLinkPopupCE() {
        return yesLinkPopupCE;
    }

    public void setNoLinkPopupCE(RichLink noLinkPopupCE) {
        this.noLinkPopupCE = noLinkPopupCE;
    }

    public RichLink getNoLinkPopupCE() {
        return noLinkPopupCE;
    }

    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }

    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }

    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double)clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }
}
