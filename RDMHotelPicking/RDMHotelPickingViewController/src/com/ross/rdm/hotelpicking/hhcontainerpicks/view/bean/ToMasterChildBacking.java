package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ToMasterChild.jspx
// ---
// ---------------------------------------------------------------------
public class ToMasterChildBacking extends RDMHotelPickingBackingBean 
{
    @SuppressWarnings("compatibility:-3064366186967891858")
    private static final long serialVersionUID = 1L;

    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f5FullLink;
    private RichLink f6LpnLink;
    private RichLink f7ByPassLink;
    private RichInputText inputChildToContainer;
    private RichIcon iconChildToContainer;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichPopup confirmPopupCE;
    private RichOutputText dialogOutputTextCE;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;
    private RichLink yesLinkPopupCE;
    private RichLink noLinkPopupCE;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;

    public ToMasterChildBacking() {

    }

    public void onChangedChildToContainerToMasterChild(ValueChangeEvent vce) 
    {
        String newValue = (String) vce.getNewValue();
        if(newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())){
            this.updateModel(vce);
            OperationBinding oper = ADFUtils.findOperation("callPkgContainerPickingAdfVToChild");
            List<String> errorsMessages = (List<String>) oper.execute();
            if (errorsMessages != null && errorsMessages.size() > 1){
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                
                this.addErrorStyleToComponent(this.getInputChildToContainer());
                this.getIconChildToContainer().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconChildToContainer());
                this.setFocusOnUIComponent(this.getInputChildToContainer());
            }
            else if (errorsMessages != null && errorsMessages.size() == 1){
                this.getInputChildToContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildToContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
            else{
                this.hideMessagesPanel();
                this.removeErrorStyleToComponent(this.getInputChildToContainer());
                this.getIconChildToContainer().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconChildToContainer());
                
                // SAME LOGIC THAN FORMS APP...
                ADFUtils.setBoundAttributeValue("ChildContainerScanned", YES);
                
                ADFUtils.setBoundAttributeValue("DropoffRequired", YES);
                ADFUtils.setBoundAttributeValue("ChildContainerScanned", vce.getNewValue());
            }
        }
    }
    
    public String exitAction() 
    {
        String action = null;
        if(YES.equals(ADFUtils.getBoundAttributeValue("DropoffRequired"))){
            this.showMessagesPanel(WARN, this.getMessage("CNNT_CNCL_START", WARN, 
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        }
        else{
            OperationBinding oper = ADFUtils.findOperation("containerPickToMasterChildF3");
            List<String> results = (List<String>) oper.execute();
            if (results != null) {
                if (results.size() == 1) {
                    action = ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                } else {
                    this.showMessagesPanel(results.get(0), results.get(1));
                }
            }
        }
        return action;
    }
    
    public String doneAction() 
    {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F4,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if(!new BigDecimal(0).equals(ADFUtils.getBoundAttributeValue("PickContainerQty")))
                     {
                         this.getInputChildToContainer().setDisabled(true);
                         this.refreshContentOfUIComponent(this.getInputChildToContainer());
                         
                         String message = this.getMessage(DIFF_PICK, CONF, 
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                         this.getDialogOutputText().setValue(message);
                         this.getConfirmPopup().show(new RichPopup.PopupHints());
                     }
                     else{
                         OperationBinding oper = ADFUtils.findOperation("containerPickToMasterChildF4");
                         List<String> results = (List<String>) oper.execute();
                         if (results != null) {
                             if (results.size() == 1) {
                                 this.hideMessagesPanel();
                                 action = ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                                 
                                 if(this.logoutExitBTF().equals(action)){
                                     action = null;
                                     this.getInputChildToContainer().setDisabled(true);
                                     this.refreshContentOfUIComponent(this.getInputChildToContainer());
                                     String message =
                                         this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                         (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                                     this.getDmsErrorOutputText().setValue(message);
                                     this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                                 }
                             } else if (results.size() == 2) {
                                 this.showMessagesPanel(results.get(0), results.get(1));
                             } else if (results.size() == 3) {
                                 this.getInputChildToContainer().setDisabled(true);
                                 this.refreshContentOfUIComponent(this.getInputChildToContainer());
                                 ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F4_KEY_CODE.intValue()));
                                 this.getDialogOutputTextCE().setValue(results.get(2));
                                 this.getConfirmPopupCE().show(new RichPopup.PopupHints());
                             }
                             else{
                                 action = this.continueF4Logic();
                             }
                         }
                     }      
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
       
        return action;
    }
    
    public String yesLinkConfirmPopupAction() {
        String action = null;
        this.getConfirmPopup().hide();
        OperationBinding oper = ADFUtils.findOperation("containerPickToMasterChildF4");
        List<String> results = (List<String>) oper.execute();
        if (results != null) {
            if (results.size() == 1) {
                this.hideMessagesPanel();
                HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                action = pfBean.getActionByBlockName(results.get(0));
                
                if(this.logoutExitBTF().equals(action)){
                    action = null;
                    this.getInputChildToContainer().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputChildToContainer());
                    String message =
                        this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                    this.getDmsErrorOutputText().setValue(message);
                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                }
            } else if (results.size() == 2) {
                this.showMessagesPanel(results.get(0), results.get(1));
            } else if (results.size() == 3) {
                this.getInputChildToContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildToContainer());
                ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F4_KEY_CODE.intValue()));
                this.getDialogOutputTextCE().setValue(results.get(2));
                this.getConfirmPopupCE().show(new RichPopup.PopupHints());
            }
            else{
                action = this.continueF4Logic();
            }
        }
        return action;
    }
    
    public String noLinkConfirmPopupAction() {
        this.getConfirmPopup().hide();
        return null;
    }
    
    public String fullAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F5,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     OperationBinding oper = ADFUtils.findOperation("callPkgContainerPickingAdfCheckEmptyPickToContainer");
                     List<String> results = (List<String>) oper.execute();
                     if (results != null){            
                         if (results.size() == 1) {
                             // action = this.logoutExitBTF();
                             this.getInputChildToContainer().setDisabled(true);
                             this.refreshContentOfUIComponent(this.getInputChildToContainer());
                             String message =
                                 this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                 (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                             this.getDmsErrorOutputText().setValue(message);
                             this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                         }
                         else if (results.size() == 2) {
                             this.showMessagesPanel(results.get(0), results.get(1));
                         }
                     }
                     else{
                         ADFUtils.setBoundAttributeValue("CallingBlock", HhContainerPickSBean.TO_MASTER_CHILD_BLOCK);
                         ADFUtils.findOperation("callIncrementLabor").execute();
                         oper = ADFUtils.findOperation("callCheckEmpty");
                         results = (List<String>) oper.execute();
                         if (results.size() == 1){
                             this.hideMessagesPanel();
                             this.getInputChildToContainer().setDisabled(true);
                             this.refreshContentOfUIComponent(this.getInputChildToContainer());
                             ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F5_KEY_CODE.intValue()));
                             this.getDialogOutputTextCE().setValue(results.get(0));
                             this.getConfirmPopupCE().show(new RichPopup.PopupHints());
                         }
                         else if(results.size() > 1){
                             this.showMessagesPanel(results.get(0), results.get(1));
                         }
                         else{
                             action = this.continueF5Logic();
                         }
                     }          
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        return action;
    }
    
    public String yesLinkConfirmPopupCEAction() {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation("callMarkLocation");
        String result = (String) oper.execute();
        if("SUCCESS".equals(result)){
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
        }
        Integer f4KeyCode = Integer.valueOf(F4_KEY_CODE.intValue());
        if(f4KeyCode.equals(ADFUtils.getBoundAttributeValue("KeyPressed"))){
            action = this.continueF4Logic();
        }
        else{
            action = this.continueF5Logic();
        }
        return action;
    }
    
    public String noLinkConfirmPopupCEAction() {
        String action = null;
        Integer f4KeyCode = Integer.valueOf(F4_KEY_CODE.intValue());
        if(f4KeyCode.equals(ADFUtils.getBoundAttributeValue("KeyPressed"))){
            action = this.continueF4Logic();
        }
        else{
            action = this.continueF5Logic();
        }
        return action;
    }
    
    private String continueF4Logic()
    {
        String action = null;
        this.getConfirmPopupCE().hide();
        ADFUtils.setBoundAttributeValue("KeyPressed", null);
        OperationBinding oper = ADFUtils.findOperation("loadTheScrnWrap");
        ContainerPickResults result = (ContainerPickResults) oper.execute();
        if(result.isOperationError()){
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            this.getInputChildToContainer().setDisabled(false);
            this.addErrorStyleToComponent(this.getInputChildToContainer());
            this.setFocusOnUIComponent(this.getInputChildToContainer());
        }
        else if(result.getIsTrExit()){
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            action = pfBean.getActionByBlockName(result.getNavigationBlock());
            
            if(this.logoutExitBTF().equals(action)){
                action = null;
                this.getInputChildToContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildToContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        }
        else if(result.getNavigationBlock() != null){
            if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            }
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
            if(oper.getErrors() == null || oper.getErrors().isEmpty()){
                HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                if(result.getNavigationItem() != null){
                    pfBean.setIsFocusOn(result.getNavigationBlock() + "." +result.getNavigationItem());
                }
                action = pfBean.getActionByBlockName(result.getNavigationBlock());
            }
        }
        else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            this.getInputChildToContainer().setDisabled(false);
            this.setFocusOnUIComponent(this.getInputChildToContainer());
        }
        return action;
    }
    
    private String continueF5Logic()
    {
        String action = null;
        this.getConfirmPopupCE().hide();
        ADFUtils.setBoundAttributeValue("ActualPickedQty", new BigDecimal(0));
        OperationBinding oper = ADFUtils.findOperation("navigateToLocationInContainerPick");
        oper.getParamsMap().put("fullDoneFlagIn", "F");
        List<String> results = (List<String>) oper.execute();
        if (results != null && results.size() == 2){
            this.showMessagesPanel(results.get(0), results.get(1));
            this.getInputChildToContainer().setDisabled(false);
            this.setFocusOnUIComponent(this.getInputChildToContainer());
        }
        else if (results != null && results.size() == 1){
            this.hideMessagesPanel();
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            action = pfBean.getActionByBlockName(results.get(0));
            
            if(this.logoutExitBTF().equals(action)){
                action = null;
                this.getInputChildToContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildToContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        }
        return action;
    }
    
    public String lpnAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F6,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     OperationBinding oper = ADFUtils.findOperation("callDisplayLPNs");
                     oper.getParamsMap().put("callingBlock", HhContainerPickSBean.TO_MASTER_CHILD_BLOCK);
                     List<String> errorsMessages = (List<String>) oper.execute();
                     if (errorsMessages.size() == 1){
                         action = ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(errorsMessages.get(0));
                     }
                     else{
                         this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                     } 
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
       
        return action;
    }
    
    public String byPassAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F7,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                    
                     if(NO.equals(ADFUtils.getBoundAttributeValue("ContainerPicked"))){
                         OperationBinding oper = ADFUtils.findOperation("containerPickToMasterChildF7");
                         List<String> errorsMessages = (List<String>) oper.execute();
                         if (errorsMessages.size() == 1){
                             action = ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(errorsMessages.get(0));
                         }
                         else{
                             this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                         }
                     }
                     else{
                         this.showMessagesPanel(WARN, this.getMessage("CAN_NOT_CANCEL", WARN, 
                                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                     }        
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        return action;
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) 
        {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            // WE HAVE TO UPDATE THE MODEL...
            ADFUtils.setBoundAttributeValue("ChildToContainer", submittedValue);            
            
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.showMessagesPanel(ERROR,
                                           this.getMessage("INV_CONTAINER", ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                    this.addErrorStyleToComponent(component);
                    this.getIconChildToContainer().setName(ERR_ICON);
                    this.refreshContentOfUIComponent(this.getIconChildToContainer());
                }
                else{
                    this.onChangedChildToContainerToMasterChild(new ValueChangeEvent(this.getInputChildToContainer(), null, submittedValue));
                }
            } 
            else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            }
            else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            }
            else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5FullLink());
                actionEvent.queue();
            }
            else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6LpnLink());
                actionEvent.queue();
            }
            else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7ByPassLink());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopupCE(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopupCE());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopupCE());
                actionEvent.queue();
            }
        }
    }
    
    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF5FullLink(RichLink f5FullLink) {
        this.f5FullLink = f5FullLink;
    }

    public RichLink getF5FullLink() {
        return f5FullLink;
    }

    public void setF6LpnLink(RichLink f6LpnLink) {
        this.f6LpnLink = f6LpnLink;
    }

    public RichLink getF6LpnLink() {
        return f6LpnLink;
    }

    public void setF7ByPassLink(RichLink f7ByPassLink) {
        this.f7ByPassLink = f7ByPassLink;
    }

    public RichLink getF7ByPassLink() {
        return f7ByPassLink;
    }

    public void setInputChildToContainer(RichInputText inputChildToContainer) {
        this.inputChildToContainer = inputChildToContainer;
    }

    public RichInputText getInputChildToContainer() {
        return inputChildToContainer;
    }

    public void setIconChildToContainer(RichIcon iconChildToContainer) {
        this.iconChildToContainer = iconChildToContainer;
    }

    public RichIcon getIconChildToContainer() {
        return iconChildToContainer;
    }
    
    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }
    
    public void setConfirmPopupCE(RichPopup confirmPopupCE) {
        this.confirmPopupCE = confirmPopupCE;
    }

    public RichPopup getConfirmPopupCE() {
        return confirmPopupCE;
    }

    public void setDialogOutputTextCE(RichOutputText dialogOutputTextCE) {
        this.dialogOutputTextCE = dialogOutputTextCE;
    }

    public RichOutputText getDialogOutputTextCE() {
        return dialogOutputTextCE;
    }
    
    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }
    
    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public void setYesLinkPopupCE(RichLink yesLinkPopupCE) {
        this.yesLinkPopupCE = yesLinkPopupCE;
    }

    public RichLink getYesLinkPopupCE() {
        return yesLinkPopupCE;
    }

    public void setNoLinkPopupCE(RichLink noLinkPopupCE) {
        this.noLinkPopupCE = noLinkPopupCE;
    }

    public RichLink getNoLinkPopupCE() {
        return noLinkPopupCE;
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public void hideMessagesPanel()
    {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public boolean isErrorOnThePage(){
        boolean error = false;
        if(ADFUtils.getBoundAttributeValue("ChildToContainer") == null ||
            "".equals(ADFUtils.getBoundAttributeValue("ChildToContainer"))){
            error = true;
            this.showMessagesPanel(ERROR, this.getMessage("INV_CONTAINER", ERROR, 
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(this.getInputChildToContainer());
            this.getIconChildToContainer().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconChildToContainer());
        }
        return error;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.TO_MASTER_CHILD_BLOCK);
    }
    
    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }
    
    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }
    
    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }
}
