package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Weight.jspx
// ---
// ---------------------------------------------------------------------
public class WeightBacking extends RDMHotelPickingBackingBean 
{
    @SuppressWarnings("compatibility:3779941786973128621")
    private static final long serialVersionUID = 1L;

    private RichInputText inputTotalWeight;
    private RichIcon iconTotalWeight;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichPopup confirmPopupCE;
    private RichOutputText dialogOutputTextCE;
    private RichLink yesLinkPopupCE;
    private RichLink noLinkPopupCE;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;
    
    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;
    
    private final static String CHECK_EMPTY_LP_DIFFERENT = "LAST_PICK_DIFFERENT";
    private final static String CHECK_EMPTY_LP_UNCHANGED = "LAST_PICK_UNCHANGED";

    public WeightBacking() {

    }

    public void onChangedTotalWeightWeight(ValueChangeEvent vce) {
        String newValue = (String) vce.getNewValue();
        if(newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())){
            this.updateModel(vce);
            if("TO_LOCATION".equals(ADFUtils.getBoundAttributeValue("CallingBlock"))){
                if(!new BigDecimal(0).equals(vce.getNewValue())){
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getInputTotalWeight());
                    this.getIconTotalWeight().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getIconTotalWeight());
                }
            }
        }
    }
    
    public String doneAction() 
    {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_WEIGHT_F4,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if(ADFUtils.getBoundAttributeValue("TotalWeight") == null ||
                         new BigDecimal(0).equals(ADFUtils.getBoundAttributeValue("TotalWeight"))){
                         this.showMessagesPanel(WARN, this.getMessage("ENTER_ITEM_WT", WARN, 
                                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                         
                         this.addErrorStyleToComponent(this.getInputTotalWeight());
                         this.getIconTotalWeight().setName(WRN_ICON);
                         this.refreshContentOfUIComponent(this.getIconTotalWeight());
                         this.setFocusOnUIComponent(this.getInputTotalWeight());
                     }
                     else{
                         OperationBinding oper = ADFUtils.findOperation("containerPickWeightF4");
                         List<String> results = (List<String>) oper.execute();
                         if(results.size() == 1){
                             action = ((HhContainerPickSBean)getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                             this.hideMessagesPanel();
                             
                             if(this.logoutExitBTF().equals(action)){
                                 action = null;
                                 this.getInputTotalWeight().setDisabled(true);
                                 this.refreshContentOfUIComponent(this.getInputTotalWeight());
                                 
                                 String message =
                                     this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                                 this.getDmsErrorOutputText().setValue(message);
                                 this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                             }
                         } 
                         else if(results.size() == 2){
                             this.showMessagesPanel(results.get(0), results.get(1));
                         }
                         else if(results.size() == 3){
                             HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                             pfBean.setCheckEmptyFlowWeight(results.get(1));
                             
                             this.getDialogOutputTextCE().setValue(results.get(2));
                             this.getConfirmPopupCE().show(new RichPopup.PopupHints());
                         }
                     }         
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        
        return action;
    }
    
    public String yesLinkConfirmPopupCEAction() {
        OperationBinding oper = ADFUtils.findOperation("callMarkLocation");
        String result = (String) oper.execute();
        if("SUCCESS".equals(result)){
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
        }
        return this.continueF4LogicAfterCheckEmpty();
    }
    
    public String noLinkConfirmPopupCEAction() {
        return this.continueF4LogicAfterCheckEmpty();
    }
    
    private String continueF4LogicAfterCheckEmpty(){
        String action = null;
        this.getConfirmPopupCE().hide();
        this.hideMessagesPanel();
        if(HhContainerPickSBean.TO_LOCATION_BLOCK.equals(ADFUtils.getBoundAttributeValue("CallingBlock"))){
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            
            if(CHECK_EMPTY_LP_DIFFERENT.equals(pfBean.getCheckEmptyFlowWeight())){
                pfBean.setCheckEmptyFlowWeight(null);
                
                OperationBinding oper = ADFUtils.findOperation("callLoadFirstAndLastPicks");
                oper.getParamsMap().put("navigateToBlock", ADFUtils.getBoundAttributeValue("ToLocCallingBlock"));
                List<String> result = (List<String>) oper.execute();
                if (result != null) {
                    if (result.size() == 2) {
                        // ERROR
                        this.showMessagesPanel(result.get(0), result.get(1));
                    } else {
                        action = pfBean.getActionByBlockName(result.get(0));
                    }
                }
            }
            else if(CHECK_EMPTY_LP_UNCHANGED.equals(pfBean.getCheckEmptyFlowWeight())){
                pfBean.setCheckEmptyFlowWeight(null);
                if(NO.equals(ADFUtils.getBoundAttributeValue("LabeledReserve"))
                    || YES.equals(ADFUtils.getBoundAttributeValue("SingleContainer"))){
                    action = this.callLoadTheScrnWrap();
                }
                else{
                    action = HhContainerPickSBean.FROM_MASTER_CHILD_ACTION;
                }
            }
        }
        else if(HhContainerPickSBean.CONTAINER_PICK_TWO_BLOCK.equals(ADFUtils.getBoundAttributeValue("CallingBlock"))){
            if(NO.equals(ADFUtils.getBoundAttributeValue("LabeledReserve"))){
                action = this.callLoadTheScrnWrap();
            }
            else{
                if(YES.equals(ADFUtils.getBoundAttributeValue("SingleContainer"))){
                    ADFUtils.setBoundAttributeValue("ChildFromContainer", 
                                                    ADFUtils.getBoundAttributeValue("ConfFromContainerId"));
                    OperationBinding oper = ADFUtils.findOperation("callVFromChild");
                    List<String> result = (List<String>) oper.execute();
                    if(result != null && result.size() == 2){
                        this.showMessagesPanel(result.get(0), result.get(1));
                    }
                    else{
                        ADFUtils.setBoundAttributeValue("ChildContainerScanned", YES);
                        ADFUtils.setBoundAttributeValue("DropoffRequired", YES);
                        
                        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                        pfBean.setExecuteActionFromMasterChild("F4");
                        
                        // ADFUtils.invokeAction(HhContainerPickSBean.FROM_MASTER_CHILD_ACTION);
                        action = HhContainerPickSBean.EXECUTE_LOGIC_ACTION;
                    }
                }
                else{
                    action = HhContainerPickSBean.FROM_MASTER_CHILD_ACTION;
                }
            }
        }
        else{
            action = this.callLoadTheScrnWrap();
        }
        return action;
    }
    
    private String callLoadTheScrnWrap(){
        String action = null;
        OperationBinding oper = ADFUtils.findOperation("loadTheScrnWrap");
        ContainerPickResults result = (ContainerPickResults) oper.execute();
        if(result.isOperationError()){
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
        }
        else if(result.getIsTrExit()){
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            action = pfBean.getActionByBlockName(result.getNavigationBlock());
            
            if(this.logoutExitBTF().equals(action)){
                action = null;
                this.getInputTotalWeight().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputTotalWeight());
                
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        }
        else if(result.getNavigationBlock() != null){
            if (result.getMessage() != null && !result.getMessage().isEmpty()) {
                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            }
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
            if(oper.getErrors() == null || oper.getErrors().isEmpty()){
                HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                if(result.getNavigationItem() != null){
                    pfBean.setIsFocusOn(result.getNavigationBlock() + "." +result.getNavigationItem());
                }
                action = pfBean.getActionByBlockName(result.getNavigationBlock());
            }
        }
        else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
        }
        return action;
    }
    
    public String exitAction() {
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        return pfBean.getActionByBlockName((String) ADFUtils.getBoundAttributeValue("CallingBlock"));
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) 
        {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            // WE HAVE TO UPDATE THE MODEL...
            ADFUtils.setBoundAttributeValue("TotalWeight", submittedValue);
            
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue) || "0".equals(submittedValue)) {
                    this.showMessagesPanel(WARN, this.getMessage("ENTER_ITEM_WT", WARN, 
                                                                (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                                (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                    
                    this.addErrorStyleToComponent(component);
                    this.getIconTotalWeight().setName(WRN_ICON);
                    this.refreshContentOfUIComponent(this.getIconTotalWeight());
                    this.setFocusOnUIComponent(component);
                }
                else{
                    this.onChangedTotalWeightWeight(new ValueChangeEvent(this.getInputTotalWeight(), null, submittedValue));
                }
            }
            else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } 
            else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopupCE(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopupCE());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopupCE());
                actionEvent.queue();
            }
        }
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }
    
    public void setIconTotalWeight(RichIcon iconTotalWeight) {
        this.iconTotalWeight = iconTotalWeight;
    }

    public RichIcon getIconTotalWeight() {
        return iconTotalWeight;
    }

    public void setInputTotalWeight(RichInputText inputTotalWeight) {
        this.inputTotalWeight = inputTotalWeight;
    }

    public RichInputText getInputTotalWeight() {
        return inputTotalWeight;
    }
    
    public void setConfirmPopupCE(RichPopup confirmPopupCE) {
        this.confirmPopupCE = confirmPopupCE;
    }

    public RichPopup getConfirmPopupCE() {
        return confirmPopupCE;
    }

    public void setDialogOutputTextCE(RichOutputText dialogOutputTextCE) {
        this.dialogOutputTextCE = dialogOutputTextCE;
    }

    public RichOutputText getDialogOutputTextCE() {
        return dialogOutputTextCE;
    }
    
    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setYesLinkPopupCE(RichLink yesLinkPopupCE) {
        this.yesLinkPopupCE = yesLinkPopupCE;
    }

    public RichLink getYesLinkPopupCE() {
        return yesLinkPopupCE;
    }

    public void setNoLinkPopupCE(RichLink noLinkPopupCE) {
        this.noLinkPopupCE = noLinkPopupCE;
    }

    public RichLink getNoLinkPopupCE() {
        return noLinkPopupCE;
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public void hideMessagesPanel()
    {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public boolean isErrorOnThePage(){
        boolean error = false;
        if(this.getIconErrorMessage() != null){
            error = this.getIconErrorMessage().isVisible() && ERR_ICON.equals(this.getIconErrorMessage().getName());
        }
        return error;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.WEIGHT_BLOCK);
    }
    
    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }
    
    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }
    
    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }
}
