package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.event.SelectionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ContainerId.jspx
// ---
// ---------------------------------------------------------------------
public class ContainerIdBacking extends RDMHotelPickingBackingBean 
{
    @SuppressWarnings("compatibility:3046344632632730886")
    private static final long serialVersionUID = 1L;

    private RichLink f3ExitLink;
    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText firstMessage;
    private RichOutputText lastMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;

    private final static String TO_MASTER_CHILD_BLOCK = "TO_MASTER_CHILD";
    private final static String FROM_MASTER_CHILD_BLOCK = "FROM_MASTER_CHILD";
    private final static String FROM_MASTER_CHILD2_BLOCK = "FROM_MASTER_CHILD2";
    private final static String TO_MASTER_CHILD_ACTION = "goToMasterChild";
    private final static String FROM_MASTER_CHILD_ACTION = "goFromMasterChild";
    private final static String FROM_MASTER_CHILD2_ACTION = "goFromMasterChild2";

   //Focus items
    private final static String PREV_CHILD_TO_CONTAINER = "TO_MASTER_CHILD.PREV_CHILD_TO_CONTAINER";
    private final static String CHILD_FROM_CONTAINER  = "FROM_MASTER_CHILD.CHILD_FROM_CONTAINER";
    private final static String CHILD_FROM_CONTAINER2 = "FROM_MASTER_CHILD2.CHILD_FROM_CONTAINER";
    
    //Bean
    private final static String BEAN_CONT_PICK = "HhContainerPickSBean";
    
    //Iterators
    private final static String ITR_CONT_PICK_CONTID = "HhContainerPickSContainerIdViewIterator";
    
    
    public ContainerIdBacking() {

    }

    public String exitAction() {
        String action = null;
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean(BEAN_CONT_PICK);
        if(TO_MASTER_CHILD_BLOCK.equals(ADFUtils.getBoundAttributeValue("CallingBlock"))){
            action = TO_MASTER_CHILD_ACTION;
            pfBean.setIsFocusOn(PREV_CHILD_TO_CONTAINER);
        }
        else if(FROM_MASTER_CHILD_BLOCK.equals(ADFUtils.getBoundAttributeValue("CallingBlock"))){
            action = FROM_MASTER_CHILD_ACTION;
            pfBean.setIsFocusOn(CHILD_FROM_CONTAINER);
        }
        else if(FROM_MASTER_CHILD2_BLOCK.equals(ADFUtils.getBoundAttributeValue("CallingBlock"))){
            action = FROM_MASTER_CHILD2_ACTION;
            pfBean.setIsFocusOn(CHILD_FROM_CONTAINER2);
        }
        return action;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            }
        }
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }
    
    public void setFirstMessage(RichOutputText firstMessage) {
        this.firstMessage = firstMessage;
    }

    public RichOutputText getFirstMessage() {
        return firstMessage;
    }

    public void setLastMessage(RichOutputText lastMessage) {
        this.lastMessage = lastMessage;
    }

    public RichOutputText getLastMessage() {
        return lastMessage;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.CONTAINER_ID_BLOCK);
        this.showFirstLastRecordInfo();
    }
    
    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib = ADFUtils.findIterator(ITR_CONT_PICK_CONTID);
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == trow - 1) {
            this.showMessagesPanel(MSG_LAST);
        } else if (crow == 0) {
            this.showMessagesPanel(MSG_FIRST);
        } else {
            this.hideMessagesPanel();
        }
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        JSFUtils.resolveMethodExpression("#{bindings.HhContainerPickSContainerIdView.collectionModel.makeCurrent}",
                                         SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                         selectionEvent });
        this.showFirstLastRecordInfo();
    }
    
    private void showMessagesPanel(String type) {
        if(MSG_FIRST.equals(type)){
            this.getFirstMessage().setVisible(true);
            this.getLastMessage().setVisible(false);
        }
        else{
            this.getLastMessage().setVisible(true);
            this.getFirstMessage().setVisible(false);            
        }
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    private void hideMessagesPanel() {
        this.getFirstMessage().setVisible(false);
        this.getLastMessage().setVisible(false);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
}
