package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ItemDescription.jspx
// ---
// ---------------------------------------------------------------------
public class ItemDescriptionBacking extends ContainerPickBaseBean 
{
    @SuppressWarnings("compatibility:4285464753965804156")
    private static final long serialVersionUID = 1L;

    private RichLink f3ExitLink;

    public ItemDescriptionBacking() {

    }
    
    public String exitAction() 
    {
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        // pfBean.setIsFocusOn("CONTAINER_PICK.CONF_FROM_CONTAINER_ID");        
        pfBean.setIsFocusOn(CONFIRM_CONT);
        return HhContainerPickSBean.CONTAINER_PICK_ACTION;
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            }
        }
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.ITEM_DESCRIPTION_BLOCK);
    }
}
