package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ToLocation.jspx
// ---
// ---------------------------------------------------------------------
public class ToLocationBacking extends RDMHotelPickingBackingBean 
{
    @SuppressWarnings("compatibility:-3037332418548568216")
    private static final long serialVersionUID = 1L;
    
    private RichInputText toLocationInput;
    private RichInputText pickToContainerInput;
    private RichIcon iconToLocation;
    private RichIcon iconPickContainer;
    private RichLink f3CancelLink;
    private RichLink f4DoneLink;
    private RichPopup successPopup;
    private RichOutputText dialogOutputText;

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;
    
    private final static String TO_LOCATION_ITEM = "lid";
    private final static String PICK_TO_CONTAINER_ITEM = "ncid";
    private RichLink succesPopUpLink;
    private RichPopup warningPopup;
    private RichLink warningPopUpLink;
    private RichOutputText dialogWarningOutputText;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;

    public ToLocationBacking() {

    }

    public void onChangedToLocationIdToLocation(ValueChangeEvent vce) 
    {
        String newValue = (String) vce.getNewValue();
        if(newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())){
            OperationBinding oper = ADFUtils.findOperation("callValidateToLocation2FromContainerPick");
            oper.getParamsMap().put("locationId", newValue.toUpperCase());
            List<String> errorsMessages = (List<String>) oper.execute();
            if (errorsMessages != null){
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                
                this.addErrorStyleToComponent(this.getToLocationInput());
                this.getIconToLocation().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconToLocation());
                this.setFocusOnUIComponent(this.getToLocationInput());
            }
            else{
                
                //HhContainerPickSBean hhContainerPickBean= (HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean");
                
                // if we pressed on F5 we go to the field
                if(this.isF5FullPressed()){
                    
                    this.hideMessagesPanel();
                
                    this.getToLocationInput().setDisabled(true);
                    this.removeErrorStyleToComponent(this.getToLocationInput());
                    this.getIconToLocation().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getIconToLocation());
                
                  
                    this.getPickToContainerInput().setDisabled(false); // ¿¿¿ check FullDone == 'D' ???
                    this.setFocusOnUIComponent(this.getPickToContainerInput());
                }
            }
        }
    }
    
    

    public void onChangedPickToContainerIdToLocation(ValueChangeEvent vce) 
    {
        String newValue = (String) vce.getNewValue();
        if(newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())){
            this.updateModel(vce);
            OperationBinding oper = ADFUtils.findOperation("callValidateNewContainerFromContainerPick");
            oper.getParamsMap().put("pickToContainerId", newValue.toUpperCase());
            List<String> errorsMessages = (List<String>) oper.execute();
            if (errorsMessages != null){
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                
                this.addErrorStyleToComponent(this.getPickToContainerInput());
                this.getIconPickContainer().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconPickContainer());
                this.setFocusOnUIComponent(this.getPickToContainerInput());
            }
            else{
                this.hideMessagesPanel();
                
                this.removeErrorStyleToComponent(this.getPickToContainerInput());
                this.getIconPickContainer().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconPickContainer());
                this.setFocusOnUIComponent(this.getPickToContainerInput());
            }
        }
    }
    
    public String f3CancelAction() 
    {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_LOCATION_F3,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                            
                     if(NO.equals(ADFUtils.getBoundAttributeValue("ContainerPicked"))){
                         if("F".equals(ADFUtils.getBoundAttributeValue("FullDone"))){
                             ADFUtils.findOperation("clearBlock").execute();
                             HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                             action = pfBean.getActionByBlockName((String) ADFUtils.getBoundAttributeValue("CallingBlock"));
                             if(HhContainerPickSBean.CONTAINER_PICK_ACTION.equals(action)
                                 && StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("FromLocationId"))){
                                 pfBean.setIsFocusOn(HhContainerPickSBean.PICK_CONTAINER_ITEM);
                             }
                             else{
                                 pfBean.setDefaultFocusByAction(action);
                             }
                         }
                         else if("CP".equals(ADFUtils.getBoundAttributeValue("PickType"))
                                 || YES.equals(ADFUtils.getBoundAttributeValue("ReplenFlag"))){
                             this.showMessagesPanel(WARN, this.getMessage("CAN_NOT_CANCEL", WARN, 
                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                             
                             if(!this.getToLocationInput().isDisabled()){
                                 this.addErrorStyleToComponent(this.getToLocationInput());
                                 this.getIconToLocation().setName(ERR_ICON);
                                 this.refreshContentOfUIComponent(this.getIconToLocation());
                                 this.setFocusOnUIComponent(this.getToLocationInput());
                             }
                             else{
                                 this.addErrorStyleToComponent(this.getPickToContainerInput());
                                 this.getIconPickContainer().setName(ERR_ICON);
                                 this.refreshContentOfUIComponent(this.getIconPickContainer());
                                 this.setFocusOnUIComponent(this.getPickToContainerInput());
                             }
                         }
                         else{
                             // TODO -> :global.error_flag := 'Y';
                             OperationBinding oper = ADFUtils.findOperation("callPkgContainerPickingAdfTrExit");
                             List<String> errorsMessages = (List<String>) oper.execute();
                             if (errorsMessages != null && errorsMessages.size() == 2){
                                 this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                             }
                             else{
                                 action = this.logoutExitBTF();
                                 this.setF5FullPressed(false);
                                 this.hideMessagesPanel();
                             }
                         }
                     }
                     else{
                         this.showMessagesPanel(WARN, this.getMessage("CAN_NOT_CANCEL", WARN, 
                                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                                     (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                         
                         if(!this.getToLocationInput().isDisabled()){
                             this.addErrorStyleToComponent(this.getToLocationInput());
                             this.getIconToLocation().setName(ERR_ICON);
                             this.refreshContentOfUIComponent(this.getIconToLocation());
                             this.setFocusOnUIComponent(this.getToLocationInput());
                         }
                         else{
                             this.addErrorStyleToComponent(this.getPickToContainerInput());
                             this.getIconPickContainer().setName(ERR_ICON);
                             this.refreshContentOfUIComponent(this.getIconPickContainer());
                             this.setFocusOnUIComponent(this.getPickToContainerInput());
                         }
                     }
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        
        return action;
    }

//    public void setF5FullPressedTwo(boolean f5FullPressedTwo) {
//        ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).setF5FullPressedTwo(f5FullPressedTwo);
//    }
//
//    public boolean isF5FullPressedTwo() {
//        return ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).isF5FullPressedTwo();
//    }
    
    public void setF5FullPressed(boolean f5FullPressed) {
        ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).setF5FullPressed(f5FullPressed);
    }

    public boolean isF5FullPressed() {
        return ((HhContainerPickSBean)this.getPageFlowBean("HhContainerPickSBean")).isF5FullPressed();
    }
    
    public String f4DoneAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_TO_LOCATION_F4,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if(this.checkPopupRequiredFields()){
                         if(NO.equals(ADFUtils.getBoundAttributeValue("CancelPickFlag"))){
                             OperationBinding oper = ADFUtils.findOperation("callValidateToLocation2FromContainerPick");
                             oper.getParamsMap().put("locationId", ADFUtils.getBoundAttributeValue("ToLocationId"));
                             List<String> errorsMessages = (List<String>) oper.execute();
                             if (errorsMessages != null){
                                 this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                                 
                                 if (!this.getToLocationInput().isDisabled()) {
                                     this.addErrorStyleToComponent(this.getToLocationInput());
                                     this.getIconToLocation().setName(ERR_ICON);
                                     this.refreshContentOfUIComponent(this.getIconToLocation());
                                     this.setFocusOnUIComponent(this.getToLocationInput());
                                 }
                                 else{
                                     this.addErrorStyleToComponent(this.getPickToContainerInput());
                                     this.getIconPickContainer().setName(ERR_ICON);
                                     this.refreshContentOfUIComponent(this.getIconPickContainer());
                                     this.setFocusOnUIComponent(this.getPickToContainerInput());
                                 }
                             }
                             else{
                                 //this.setF5FullPressedTwo(false);
                                 this.setF5FullPressed(false);
                                 this.hideMessagesPanel();
                                 
                                 if (!this.getToLocationInput().isDisabled()) {
                                     this.removeErrorStyleToComponent(this.getToLocationInput());
                                     this.getIconToLocation().setName(REQ_ICON);
                                     this.refreshContentOfUIComponent(this.getIconToLocation());
                                 }
                                 else{
                                     this.removeErrorStyleToComponent(this.getPickToContainerInput());
                                     this.getIconPickContainer().setName(REQ_ICON);
                                     this.refreshContentOfUIComponent(this.getIconPickContainer());
                                 }
                                 
                                 action = this.callContainerPickToLocationF4();
                             }
                         }
                         else{
                             this.setF5FullPressed(false);
                             this.hideMessagesPanel();
                             action = this.callContainerPickToLocationF4();
                         }
                     }       
                     
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        return action;
    }
    
    private String callContainerPickToLocationF4()
    {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation("containerPickToLocationF4");
        List<String> results = (List<String>) oper.execute();
        if (results != null && results.size() == 2){
            this.showMessagesPanel(results.get(0), results.get(1));
        }
        else if (results != null && results.size() == 1){
            if("EXIT_WITH_NO_ERROR".equals(results.get(0))){
                action = this.logoutExitBTF();
            }
            else if(this.logoutExitBTF().equals(results.get(0))){
                action = null;
                this.getToLocationInput().setDisabled(true);
                this.getPickToContainerInput().setDisabled(true);
                this.refreshContentOfUIComponent(this.getToLocationInput());
                this.refreshContentOfUIComponent(this.getPickToContainerInput());
                
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
            else{
                this.getToLocationInput().setDisabled(true);
                this.getPickToContainerInput().setDisabled(true);
                this.refreshContentOfUIComponent(this.getToLocationInput());
                this.refreshContentOfUIComponent(this.getPickToContainerInput());
                
                HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                String navigation = pfBean.getActionByBlockName(results.get(0));
                pfBean.setDefaultFocusByAction(navigation);
                String message = this.getMessage("SUCCESS_OPER", MSGTYPE_M, 
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDialogOutputText().setValue(message);
                this.getSuccessPopup().setLauncherVar(navigation);
                this.getSuccessPopup().show(new RichPopup.PopupHints());
            }
        }
        else if (results != null && results.size() == 5){
            if(this.logoutExitBTF().equals(results.get(4))){
                action = null;
                this.getToLocationInput().setDisabled(true);
                this.getPickToContainerInput().setDisabled(true);
                this.refreshContentOfUIComponent(this.getToLocationInput());
                this.refreshContentOfUIComponent(this.getPickToContainerInput());
                
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
            else{
                this.getToLocationInput().setDisabled(true);
                this.getPickToContainerInput().setDisabled(true);
                this.refreshContentOfUIComponent(this.getToLocationInput());
                this.refreshContentOfUIComponent(this.getPickToContainerInput());
                
                HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                pfBean.setDefaultFocusByAction(pfBean.getActionByBlockName(results.get(4)));

                this.getDialogOutputText().setValue(results.get(3));
                this.getSuccessPopup().setLauncherVar(pfBean.getActionByBlockName(results.get(4)));
                
                this.getDialogWarningOutputText().setValue(results.get(1));
                this.getWarningPopup().show(new RichPopup.PopupHints());
            }
        }
        return action;
    }
    
    private boolean checkPopupRequiredFields(){
        boolean allOk = true;
        this.removeErrorsOnFields();
        if(ADFUtils.getBoundAttributeValue("ToLocationId") == null
            || "".equals(ADFUtils.getBoundAttributeValue("ToLocationId"))){
            allOk = false;
            this.showMessagesPanel(WARN, this.getMessage(PARTIAL_ENTRY, WARN, 
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID), 
                                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            
            this.getToLocationInput().setDisabled(false);
            this.addErrorStyleToComponent(this.getToLocationInput());
            this.getIconToLocation().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconToLocation());
            this.setFocusOnUIComponent(this.getToLocationInput());
        }
        else{
            OperationBinding oper = ADFUtils.findOperation("callValidateToLocation2FromContainerPick");
            oper.getParamsMap().put("locationId", ADFUtils.getBoundAttributeValue("ToLocationId"));
            List<String> errorsMessages = (List<String>) oper.execute();
            if (errorsMessages != null){
                allOk = false;
                this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                
                this.getPickToContainerInput().setDisabled(true);
                this.refreshContentOfUIComponent(this.getPickToContainerInput());
                
                this.getToLocationInput().setDisabled(false);
                this.addErrorStyleToComponent(this.getToLocationInput());
                this.getIconToLocation().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconToLocation());
                this.setFocusOnUIComponent(this.getToLocationInput());
            }
            else{
                this.hideMessagesPanel();
                
                this.getToLocationInput().setDisabled(true);
                this.refreshContentOfUIComponent(this.getToLocationInput());
                
                String pickToContainerId = (String) ADFUtils.getBoundAttributeValue("PickToContainerId");
                if((pickToContainerId == null || "".equals(pickToContainerId))
                    && "F".equals(ADFUtils.getBoundAttributeValue("FullDone")))
                {
                    oper = ADFUtils.findOperation("callFindRemainingPicks");
                    errorsMessages = (List<String>) oper.execute();
                    if (errorsMessages != null){
                        allOk = false;
                        this.showMessagesPanel(errorsMessages.get(0), errorsMessages.get(1));
                        
                        this.addErrorStyleToComponent(this.getPickToContainerInput());
                        this.getIconPickContainer().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getIconPickContainer());
                    }
                }
                
                // ENABLING PICK_TO_CONTAINER INPUT
                this.getPickToContainerInput().setDisabled(false);
                this.setFocusOnUIComponent(this.getPickToContainerInput());
            }
        }
        return allOk;
    }
    
    private void removeErrorsOnFields(){
        this.removeErrorStyleToComponent(this.getToLocationInput());
        this.getIconToLocation().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconToLocation());
        
        this.removeErrorStyleToComponent(this.getPickToContainerInput());
        this.getIconPickContainer().setName(REQ_ICON);
        this.refreshContentOfUIComponent(this.getIconPickContainer());
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) 
        {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String item = (String) clientEvent.getParameters().get("item");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            
            if(!F3_KEY_CODE.equals(keyPressed)){
                // WE HAVE TO UPDATE THE MODEL...
                if (TO_LOCATION_ITEM.equals(item)) {
                    ADFUtils.setBoundAttributeValue("ToLocationId", submittedValue);
                } 
                else if (PICK_TO_CONTAINER_ITEM.equals(item)){
                    ADFUtils.setBoundAttributeValue("PickToContainerId", submittedValue);
                }
            }
            
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.checkEmptyItem(component, item);
                }
                else{
                    if (TO_LOCATION_ITEM.equals(item)) {
                        this.onChangedToLocationIdToLocation(new ValueChangeEvent(this.getToLocationInput(), null, submittedValue));
                    } 
                    else if (PICK_TO_CONTAINER_ITEM.equals(item)){
                        this.onChangedPickToContainerIdToLocation(new ValueChangeEvent(this.getPickToContainerInput(), null, submittedValue));
                    }
                }
            }
            else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3CancelLink());
                actionEvent.queue();
            } 
            else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            }
        }
    }
    
    private void checkEmptyItem(RichInputText component, String item) {
        if (TO_LOCATION_ITEM.equals(item)) {
            this.showMessagesPanel(ERROR,
                                   this.getMessage("INV_LOCATION", ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(component);
            this.getIconToLocation().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconToLocation());
        } 
        else if (PICK_TO_CONTAINER_ITEM.equals(item)){
            this.showMessagesPanel(ERROR,
                                   this.getMessage(PARTIAL_ENTRY, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
            this.addErrorStyleToComponent(component);
            this.getIconPickContainer().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconPickContainer());
        }
    }
    
    public void setIconToLocation(RichIcon iconToLocation) {
        this.iconToLocation = iconToLocation;
    }

    public RichIcon getIconToLocation() {
        return iconToLocation;
    }

    public void setIconPickContainer(RichIcon iconPickContainer) {
        this.iconPickContainer = iconPickContainer;
    }

    public RichIcon getIconPickContainer() {
        return iconPickContainer;
    }
    
    public void setF3CancelLink(RichLink f3CancelLink) {
        this.f3CancelLink = f3CancelLink;
    }

    public RichLink getF3CancelLink() {
        return f3CancelLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setToLocationInput(RichInputText toLocationInput) {
        this.toLocationInput = toLocationInput;
    }

    public RichInputText getToLocationInput() {
        return toLocationInput;
    }

    public void setPickToContainerInput(RichInputText pickToContainerInput) {
        this.pickToContainerInput = pickToContainerInput;
    }

    public RichInputText getPickToContainerInput() {
        return pickToContainerInput;
    }
    
    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }
    
    public void setSuccessPopup(RichPopup successPopup) {
        this.successPopup = successPopup;
    }

    public RichPopup getSuccessPopup() {
        return successPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }
    
    public void setSuccesPopUpLink(RichLink succesPopUpLink) {
        this.succesPopUpLink = succesPopUpLink;
    }

    public RichLink getSuccesPopUpLink() {
        return succesPopUpLink;
    }
    
    public void yesLinkConfirmSuccessPopupActionListener(ActionEvent actionEvent) {
        this.getSuccessPopup().hide();
        ADFUtils.invokeAction(this.getSuccessPopup().getLauncherVar());
    }
    
    public void performKeySuccessPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getSuccesPopUpLink());
                actionEvent.queue();
            }
        }
    }
    
    public void yesLinkConfirmWarningPopupActionListener(ActionEvent actionEvent) {
        this.getWarningPopup().hide();
        this.getSuccessPopup().show(new RichPopup.PopupHints());
    }
    
    public void performKeyWarningPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getWarningPopUpLink());
                actionEvent.queue();
            }
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public void hideMessagesPanel()
    {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }
    
    public boolean isErrorOnThePage(){
        boolean error = false;
        if(this.getIconErrorMessage() != null){
            error = this.getIconErrorMessage().isVisible() && ERR_ICON.equals(this.getIconErrorMessage().getName());
        }
        return error;
    }
    
    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) 
    {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.TO_LOCATION_BLOCK);
    }

    public void setWarningPopup(RichPopup warningPopup) {
        this.warningPopup = warningPopup;
    }

    public RichPopup getWarningPopup() {
        return warningPopup;
    }

    public void setWarningPopUpLink(RichLink warningPopUpLink) {
        this.warningPopUpLink = warningPopUpLink;
    }

    public RichLink getWarningPopUpLink() {
        return warningPopUpLink;
    }

    public void setDialogWarningOutputText(RichOutputText dialogWarningOutputText) {
        this.dialogWarningOutputText = dialogWarningOutputText;
    }

    public RichOutputText getDialogWarningOutputText() {
        return dialogWarningOutputText;
    }
    
    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }
    
    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }
    
    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }
}
