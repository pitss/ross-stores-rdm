package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import java.math.BigDecimal;

import java.sql.Date;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhContainerPickSBean extends RDMHotelPickingPageFlowBean {
    @SuppressWarnings("compatibility:-1932481535857666146")
    private static final long serialVersionUID = 1L;

    private String pmTaskQueueInd; // Item/Parameter hh_container_pick_s.PM_TASK_QUEUE_IND
    private String pmRowid; // Item/Parameter hh_container_pick_s.PM_ROWID
    private BigDecimal pmWaveNbr; // Item/Parameter hh_container_pick_s.PM_WAVE_NBR
    private String pmPickType; // Item/Parameter hh_container_pick_s.PM_PICK_TYPE
    private String pmStartLocation; // Item/Parameter hh_container_pick_s.PM_START_LOCATION
    private String pmCallingForm; // Item/Parameter hh_container_pick_s.PM_CALLING_FORM
    private Date pmWaveDate; // Item/Parameter hh_container_pick_s.PM_WAVE_DATE
    private String pVersion; // Item/Parameter hh_container_pick_s.P_VERSION
    private String pmUpsCode; // Item/Parameter hh_container_pick_s.PM_UPS_CODE
    private String isFocusOn;
    private boolean incrementLaborCalled;
    private String defaultView;
    private String executeActionFromMasterChild;
    private String messageFromMasterChild;
    private String checkEmptyFlowWeight;
    private String executeActionFromMasterChild2;
    private String messageFromMasterChild2;
    private boolean calledFromMenu;
    private String linkPressedContainerPick;
    private String focusItemPage1;
    private String linkPressedPage1;
    private boolean f5FullPressed;
    private boolean f5FullPressedTwo;

    


    private static ADFLogger _logger = ADFLogger.createADFLogger(HhContainerPickSBean.class);    

    public final static String PAGE1_ACTION = "goPage1";
    public final static String CONTAINER_PICK_ACTION = "goContainerPick";
    public final static String WEIGHT_ACTION = "goWeight";
    public final static String ITEM_DESCRIPTION_ACTION = "goItemDescription";
    public final static String FROM_MASTER_CHILD2_ACTION = "goFromMasterChild2";
    public final static String FROM_MASTER_CHILD_ACTION = "goFromMasterChild";
    public final static String CONTAINER_PICK_TWO_ACTION = "goContainerPickTwo";
    public final static String CONTAINER_ID_ACTION = "goContainerId";
    public final static String TO_MASTER_CHILD_ACTION = "goToMasterChild";
    public final static String REASON_ACTION = "goReason";
    public final static String TO_LOCATION_ACTION = "goToLocation";
    public final static String EXECUTE_LOGIC_ACTION = "executeLogicBeforeNavigate";
    
    public final static String PAGE1_BLOCK = "START_PICK";
    public final static String CONTAINER_PICK_BLOCK = "CONTAINER_PICK";
    public final static String WEIGHT_BLOCK = "WEIGHT";
    public final static String ITEM_DESCRIPTION_BLOCK = "ITEM_DESC";
    public final static String FROM_MASTER_CHILD_BLOCK = "FROM_MASTER_CHILD";
    public final static String FROM_MASTER_CHILD2_BLOCK = "FROM_MASTER_CHILD2";
    public final static String CONTAINER_PICK_TWO_BLOCK = "CONTAINER_PICK_TWO";
    public final static String CONTAINER_ID_BLOCK = "LPN";
    public final static String TO_MASTER_CHILD_BLOCK = "TO_MASTER_CHILD";
    public final static String REASON_BLOCK = "REASON";
    public final static String TO_LOCATION_BLOCK = "TO_LOCATION";
    
    static final String PICK_CONTAINER_ITEM = "CONTAINER_PICK.PICK_TO_CONTAINER_ID";
    static final String MASTER_CONTAINER_ITEM = "START_PICK.MASTER_CONTAINER";
        
    private Integer popupCountPage1;
    private String nextNavigationPage1;
    
    private String initErrorMsg;

    public void setNextNavigationPage1(String nextNavigationPage1) {
        this.nextNavigationPage1 = nextNavigationPage1;
    }

    public String getNextNavigationPage1() {
        return nextNavigationPage1;
    }


    public void initTaskFlow() {

        _logger.info("initTaskFlow start");
        
        this.focusItemPage1 = MASTER_CONTAINER_ITEM;
        
        if (this.getPVersion() == null){ // For call directly from menu
          this.setPVersion("1");
          this.setCalledFromMenu(true);
        }
        else{
            this.setCalledFromMenu(false);
        }
        // CALLED FROM TASK_QUEUE
        if(this.getPmTaskQueueInd() != null || this.getPmRowid() != null){
            this.setCalledFromMenu(false);
        }
        
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("initContainerPickS");
        oper.getParamsMap().put("pVersion", this.getPVersion());
        oper.getParamsMap().put("pmTaskQueueInd", this.getPmTaskQueueInd());
        oper.getParamsMap().put("pmRowid", this.getPmRowid());
        oper.getParamsMap().put("pmWaveNbr", this.getPmWaveNbr());
        oper.getParamsMap().put("pmPickType", this.getPmPickType());
        oper.getParamsMap().put("pmStartLocation", this.getPmStartLocation());
        oper.getParamsMap().put("pmCallingForm", this.getPmCallingForm());
        oper.getParamsMap().put("pmWaveDate", this.getPmWaveDate());
        oper.getParamsMap().put("pmUpsCode", this.getPmUpsCode());
        
        List<String> errorsMessages = (List<String>) oper.execute();

        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (errorsMessages != null && errorsMessages.size() == 1) 
            {
                this.setDefaultView(errorsMessages.get(0));
                if ("ContainerPick".equals(this.getDefaultView())) {
                    if ("Y".equals(ADFUtils.getBoundAttributeValue("LabeledPicking"))) 
                      this.setIsFocusOn(ContainerPickBaseBean.PICK_CONTAINER);
                    else
                        this.setIsFocusOn(ContainerPickBaseBean.GEN_CONTAINER);                      
                    if ("1".equals(this.getPVersion())) {                       
                        if (this.getPmTaskQueueInd() == null) {
                            this.setPmWaveNbr((BigDecimal) ADFUtils.getBoundAttributeValue("PmWaveNbr"));
                            this.setPmPickType((String) ADFUtils.getBoundAttributeValue("PmPickType"));
                            this.setPmStartLocation((String) ADFUtils.getBoundAttributeValue("PmStartLocation"));
                            this.setPmCallingForm((String) ADFUtils.getBoundAttributeValue("PmCallingForm"));
                        }
                    } else {
                        this.setPmPickType((String) ADFUtils.getBoundAttributeValue("PmPickType"));
                        this.setPmStartLocation((String) ADFUtils.getBoundAttributeValue("PmStartLocation"));
                        this.setPmUpsCode((String) ADFUtils.getBoundAttributeValue("PmUpsCode"));
                        this.setPmWaveDate((Date) ADFUtils.getBoundAttributeValue("PmWaveDate"));
                    }
                }
            } 
            else if (errorsMessages != null && errorsMessages.size() != 1) {
                this.setInitErrorMsg(errorsMessages.get(1));
                this.setDefaultView("Page1");
            }
        }
    }
    
    public String getActionByBlockName(String blockName){
        String action = null;
        
        if(PAGE1_BLOCK.equals(blockName)){
            action = PAGE1_ACTION;
        }
        else if(CONTAINER_PICK_BLOCK.equals(blockName)){
            action = CONTAINER_PICK_ACTION;
        }
        else if(WEIGHT_BLOCK.equals(blockName)){
            action = WEIGHT_ACTION;
        }
        else if(ITEM_DESCRIPTION_BLOCK.equals(blockName)){
            action = ITEM_DESCRIPTION_ACTION;
        }
        else if(FROM_MASTER_CHILD2_BLOCK.equals(blockName)){
            action = FROM_MASTER_CHILD2_ACTION;
        }
        else if(FROM_MASTER_CHILD_BLOCK.equals(blockName)){
            action = FROM_MASTER_CHILD_ACTION;
        }
        else if(CONTAINER_PICK_TWO_BLOCK.equals(blockName)){
            action = CONTAINER_PICK_TWO_ACTION;
        }
        else if(CONTAINER_ID_BLOCK.equals(blockName)){
            action = CONTAINER_ID_ACTION;
        }
        else if(TO_MASTER_CHILD_BLOCK.equals(blockName)){
            action = TO_MASTER_CHILD_ACTION;
        }
        else if(REASON_BLOCK.equals(blockName)){
            action = REASON_ACTION;
        }
        else if(TO_LOCATION_BLOCK.equals(blockName)){
            action = TO_LOCATION_ACTION;
        }
        else{
            action = blockName;
        }
        return action;
    }
    
    public void setDefaultFocusByAction(String action)
    {
        if(PAGE1_ACTION.equals(action)){
            this.setIsFocusOn("START_PICK.MASTER_CONTAINER");
        }
        else if(CONTAINER_PICK_ACTION.equals(action)){            
            this.setIsFocusOn("CONTAINER_PICK.CONF_CONTAINER_ID");
        }
        else if(FROM_MASTER_CHILD_ACTION.equals(action)){
            this.setIsFocusOn("FROM_MASTER_CHILD.CHILD_FROM_CONTAINER");
        }
        else if(FROM_MASTER_CHILD2_ACTION.equals(action)){
            this.setIsFocusOn("FROM_MASTER_CHILD2.CHILD_FROM_CONTAINER");
        }
        else if(TO_MASTER_CHILD_ACTION.equals(action)){
            this.setIsFocusOn("TO_MASTER_CHILD.PREV_CHILD_TO_CONTAINER");
        }
    }

    public void setPmTaskQueueInd(String pmTaskQueueInd) {
        this.pmTaskQueueInd = pmTaskQueueInd;
    }

    public String getPmTaskQueueInd() {
        return pmTaskQueueInd;
    }

    public void setPmRowid(String pmRowid) {
        this.pmRowid = pmRowid;
    }

    public String getPmRowid() {
        return pmRowid;
    }

    public void setPmWaveNbr(BigDecimal pmWaveNbr) {
        this.pmWaveNbr = pmWaveNbr;
    }

    public BigDecimal getPmWaveNbr() {
        return pmWaveNbr;
    }

    public void setPmPickType(String pmPickType) {
        this.pmPickType = pmPickType;
    }

    public String getPmPickType() {
        return pmPickType;
    }

    public void setPmStartLocation(String pmStartLocation) {
        this.pmStartLocation = pmStartLocation;
    }

    public String getPmStartLocation() {
        return pmStartLocation;
    }

    public void setPmCallingForm(String pmCallingForm) {
        this.pmCallingForm = pmCallingForm;
    }

    public String getPmCallingForm() {
        return pmCallingForm;
    }

    public void setPmWaveDate(Date pmWaveDate) {
        this.pmWaveDate = pmWaveDate;
    }

    public Date getPmWaveDate() {
        return pmWaveDate;
    }

    public void setPVersion(String pVersion) {
        this.pVersion = pVersion;
    }

    public String getPVersion() {
        return pVersion;
    }

    public void setPmUpsCode(String pmUpsCode) {
        this.pmUpsCode = pmUpsCode;
    }

    public String getPmUpsCode() {
        return pmUpsCode;
    }

    public void setDefaultView(String defaultView) {
        this.defaultView = defaultView;
    }

    public String getDefaultView() {
        return defaultView;
    }
    
    public void setIncrementLaborCalled(boolean incrementLaborCalled) {
        this.incrementLaborCalled = incrementLaborCalled;
    }

    public boolean isIncrementLaborCalled() {
        return incrementLaborCalled;
    }

    public void setIsFocusOn(String isFOcusOn) {
        this.isFocusOn = isFOcusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }
    
    public void setExecuteActionFromMasterChild(String executeActionFromMasterChild) {
        this.executeActionFromMasterChild = executeActionFromMasterChild;
    }

    public String getExecuteActionFromMasterChild() {
        return executeActionFromMasterChild;
    }
    
    public void setCheckEmptyFlowWeight(String checkEmptyFlowWeight) {
        this.checkEmptyFlowWeight = checkEmptyFlowWeight;
    }

    public String getCheckEmptyFlowWeight() {
        return checkEmptyFlowWeight;
    }
    
    public void setExecuteActionFromMasterChild2(String executeActionFromMasterChild2) {
        this.executeActionFromMasterChild2 = executeActionFromMasterChild2;
    }

    public String getExecuteActionFromMasterChild2() {
        return executeActionFromMasterChild2;
    }
    
    @Deprecated
    public String executeLogicBeforeNavigate()
    {
        String navigation = null;
        if("F4".equals(this.getExecuteActionFromMasterChild())){
            if(ADFUtils.getBoundAttributeValue("ChildFromContainer") == null
                || "".equals(ADFUtils.getBoundAttributeValue("ChildFromContainer")))
            {
                this.setExecuteActionFromMasterChild(null);
                BigDecimal fmChQty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQty");
                if(fmChQty == null){
                    fmChQty = new BigDecimal(0);
                }
                BigDecimal cp2Qty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQtyCP2");
                if(cp2Qty == null){
                    cp2Qty = new BigDecimal(0);
                }
                
                if(!fmChQty.equals(cp2Qty)){
                    this.setExecuteActionFromMasterChild("DIFF_PICK");
                    navigation = FROM_MASTER_CHILD_ACTION;
                }
                else{
                    oracle.binding.OperationBinding oper = ADFUtils.findOperation("containerPickFromMasterChildF4");
                    List<String> results = (List<String>) oper.execute();
                    if (results != null) {
                        if (results.size() == 1) {
                            navigation = results.get(0);
                        } 
                        else if (results.size() == 2) {
                            this.setExecuteActionFromMasterChild(results.get(0) + "||" + results.get(1));
                            navigation = FROM_MASTER_CHILD_ACTION;
                        } 
                        else if (results.size() == 3) {
                            this.setExecuteActionFromMasterChild("CHECK_EMPTY||" + results.get(2));
                            navigation = FROM_MASTER_CHILD_ACTION;
                        }
                    }
                }
            }
        }
        
        return navigation;
    }

    public void setPopupCountPage1(Integer popupCountPage1) {
        this.popupCountPage1 = popupCountPage1;
    }

    public Integer getPopupCountPage1() {
        return popupCountPage1;
    }
    
    public void setCalledFromMenu(boolean calledFromMenu) {
        this.calledFromMenu = calledFromMenu;
    }

    public boolean isCalledFromMenu() {
        return calledFromMenu;
    }
    
    public void setMessageFromMasterChild(String messageFromMasterChild) {
        this.messageFromMasterChild = messageFromMasterChild;
    }

    public String getMessageFromMasterChild() {
        return messageFromMasterChild;
    }
    
    public void setMessageFromMasterChild2(String messageFromMasterChild2) {
        this.messageFromMasterChild2 = messageFromMasterChild2;
    }

    public String getMessageFromMasterChild2() {
        return messageFromMasterChild2;
    }
    
    public void setLinkPressedContainerPick(String linkPressedContainerPick) {
        this.linkPressedContainerPick = linkPressedContainerPick;
    }

    public String getLinkPressedContainerPick() {
        return linkPressedContainerPick;
    }
    
    public void setInitErrorMsg(String initErrorMsg) {
        this.initErrorMsg = initErrorMsg;
    }

    public String getInitErrorMsg() {
        return initErrorMsg;
    }
    
    public void setFocusItemPage1(String focusItemPage1) {
        this.focusItemPage1 = focusItemPage1;
    }

    public String getFocusItemPage1() {
        return focusItemPage1;
    }

    public void setLinkPressedPage1(String linkPressedPage1) {
        this.linkPressedPage1 = linkPressedPage1;
    }

    public String getLinkPressedPage1() {
        return linkPressedPage1;
    }

    public void setF5FullPressed(boolean f5FullPressed) {
        this.f5FullPressed = f5FullPressed;
    }

    public boolean isF5FullPressed() {
        return f5FullPressed;
    }
    
    public void setF5FullPressedTwo(boolean f5FullPressedTwo) {
        this.f5FullPressedTwo = f5FullPressedTwo;
    }

    public boolean isF5FullPressedTwo() {
        return f5FullPressedTwo;
    }
}
