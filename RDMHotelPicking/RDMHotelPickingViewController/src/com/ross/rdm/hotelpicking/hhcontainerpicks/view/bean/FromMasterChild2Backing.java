package com.ross.rdm.hotelpicking.hhcontainerpicks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common.ContainerPickResults;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for FromMasterChild2.jspx
// ---
// ---------------------------------------------------------------------
public class FromMasterChild2Backing extends RDMHotelPickingBackingBean {
    @SuppressWarnings("compatibility:144278618268200191")
    private static final long serialVersionUID = 1L;

    private RichInputText inputChildFromContainer;
    private RichIcon iconChildFromContainer;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f5FullLink;
    private RichLink f6LpnLink;
    private RichLink f7ByPassLink;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichPopup confirmPopupCE;
    private RichOutputText dialogOutputTextCE;

    // UI COMPONENTS FOR HANDLING ERRORS
    private RichOutputText errorMessage;
    private RichIcon iconErrorMessage;
    private RichPanelGroupLayout errorPanel;
    private RichDialog okGoToLocationDialog;
    private RichOutputText okToLocatioInputText;
    private RichPopup okGoToLocationPpoup;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;
    private RichLink yesLinkPopupCE;
    private RichLink noLinkPopupCE;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;
    private RichLink dmsErrorPopUpLink;
    
    private static final String CAN_NOT_CANCEL = "CAN_NOT_CANCEL";
    private static final String INV_CONTAINER = "INV_CONTAINER"; 
    private static final String SUCCESS_OPER = "SUCCESS_OPER";
    private static final String CNNT_CNCL_START = "CNNT_CNCL_START";
    
    //Operations
    private static final String OPR_VALIDATE_FROM_MASTER_CHILD2 = "validateFromMasterChild2";
    private static final String OPR_FROM_MASTER_CHILD2_F3 = "containerPickFromMasterChild2F3";
    private static final String OPR_FROM_MASTER_CHILD2_F4 = "containerPickFromMasterChild2F4";
    private static final String OPR_FROM_MASTER_CHILD2_F7 = "containerPickFromMasterChild2F7";
    private static final String OPR_CHECK_EMPTY_PICK_TO_CONT = "callPkgContainerPickingAdfCheckEmptyPickToContainer";
    private static final String OPR_INCREMENT_LABOR = "callIncrementLabor";
    private static final String OPR_CHECK_EMPTY = "callCheckEmpty";
    private static final String OPR_MARK_LOCATION ="callMarkLocation";
    private static final String OPR_DO_COMMIT = "callDoCommit";
    private static final String OPR_LOAD_SCRN_WRAP = "loadTheScrnWrap";
    private static final String OPR_NAV_TO_LOCATION = "navigateToLocationInContainerPick";
    private static final String OPR_DISPLAY_LPN = "callDisplayLPNs";
    
    public FromMasterChild2Backing() {

    }

    public void onChangedChildFromContainerFromMasterChild2(ValueChangeEvent vce) {
        String newValue = (String) vce.getNewValue();
        if(newValue != null && !newValue.equalsIgnoreCase((String) vce.getOldValue())){
            updateModel(vce);
            OperationBinding oper = ADFUtils.findOperation(OPR_VALIDATE_FROM_MASTER_CHILD2);
            oper.getParamsMap().put("childFromContainer", vce.getNewValue());
            List<String> result = (List<String>) oper.execute();
            if(result != null && result.size() == 1){
                this.getInputChildFromContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            } 
            else if (result != null && !MSGTYPE_M.equals(result.get(0))) {
                this.showMessagesPanel(result.get(0), result.get(1));
                this.addErrorStyleToComponent(this.getInputChildFromContainer());
                this.getIconChildFromContainer().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getIconChildFromContainer());
            } 
            else {
                this.showMessagesPanel(MSGTYPE_M,
                                       this.getMessage(SUCCESS_OPER, MSGTYPE_M,
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                this.getInputChildFromContainer().resetValue();
                this.removeErrorStyleToComponent(this.getInputChildFromContainer());
                this.getIconChildFromContainer().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getIconChildFromContainer());
            }
        }
    }

    public String exitAction() {
        String action = null;
        if (YES.equals(ADFUtils.getBoundAttributeValue("DropoffRequired"))) {
            this.showMessagesPanel(WARN,
                                   this.getMessage(CNNT_CNCL_START, WARN,
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                   (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
        } else {
            OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD2_F3);
            List<String> results = (List<String>) oper.execute();
            if (results != null) {
                if (results.size() == 1) {
                    action =
                        ((HhContainerPickSBean) getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                } else if (results.size() > 1) {
                    this.showMessagesPanel(results.get(0), results.get(1));
                }
            }
        }
        return action;
    }

    public String doneAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F4,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     if (!new BigDecimal(0).equals(ADFUtils.getBoundAttributeValue("PickContainerQty"))) {
                         String message =
                             this.getMessage(DIFF_PICK, CONF, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                             (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                         this.getDialogOutputText().setValue(message);
                         this.getInputChildFromContainer().setDisabled(true);
                         this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                         this.getConfirmPopup().show(new RichPopup.PopupHints());
                     } else {
                         OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD2_F4);
                         List<String> results = (List<String>) oper.execute();
                         if (results != null) {
                             if (results.size() == 1) {
                                 this.hideMessagesPanel();
                                 action =
                                     ((HhContainerPickSBean) getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                                 
                                 if(this.logoutExitBTF().equals(action)){
                                     action = null;
                                     this.getInputChildFromContainer().setDisabled(true);
                                     this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                                     String message =
                                         this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                         (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                                     this.getDmsErrorOutputText().setValue(message);
                                     this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                                 }
                             } else if (results.size() == 2) {
                                 this.showMessagesPanel(results.get(0), results.get(1));
                             } else if (results.size() == 3 && "CHECK_EMPTY".equals(results.get(0))) {
                                 ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F4_KEY_CODE.intValue()));
                                 this.getDialogOutputTextCE().setValue(results.get(2));
                                 this.getInputChildFromContainer().setDisabled(true);
                                 this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                                 this.getConfirmPopupCE().show(new RichPopup.PopupHints());
                             } else {
                                 action = this.continueF4Logic();
                             }
                         }
                     }          
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        return action;
    }

    public String yesLinkConfirmPopupAction() {
        String action = null;
        this.getConfirmPopup().hide();
        OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD2_F4);
        List<String> results = (List<String>) oper.execute();
        if (results != null) {
            if (results.size() == 1) {
                this.hideMessagesPanel();
                this.getInputChildFromContainer().setDisabled(false);
                this.setFocusOnUIComponent(this.getInputChildFromContainer());
                HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
                action = pfBean.getActionByBlockName(results.get(0));
                
                if(this.logoutExitBTF().equals(action)){
                    action = null;
                    this.getInputChildFromContainer().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                    String message =
                        this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                        (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                    this.getDmsErrorOutputText().setValue(message);
                    this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                }
            } else if (results.size() == 2) {
                this.showMessagesPanel(results.get(0), results.get(1));
            } else if (results.size() == 3 && CHECK_EMPTY.equals(results.get(0))) {
                ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F4_KEY_CODE.intValue()));
                this.getDialogOutputTextCE().setValue(results.get(2));
                this.getConfirmPopupCE().show(new RichPopup.PopupHints());
            } else {
                action = this.continueF4Logic();
            }
        }
        return action;
    }

    public String noLinkConfirmPopupAction() {
        this.getConfirmPopup().hide();
        this.getInputChildFromContainer().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputChildFromContainer());
        return null;
    }

    public String fullAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F5,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     OperationBinding oper = ADFUtils.findOperation(OPR_CHECK_EMPTY_PICK_TO_CONT);
                     List<String> results = (List<String>) oper.execute();
                     if (results != null) {
                         if (results.size() == 1) {
                             // action = this.logoutExitBTF();
                             this.getInputChildFromContainer().setDisabled(true);
                             this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                             String message =
                                 this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                 (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                             this.getDmsErrorOutputText().setValue(message);
                             this.getDmsErrorPopup().show(new RichPopup.PopupHints());
                         }
                         else if (results.size() == 2) {
                             this.showMessagesPanel(results.get(0), results.get(1));
                         }
                     } else {
                         ADFUtils.setBoundAttributeValue("CallingBlock", HhContainerPickSBean.FROM_MASTER_CHILD2_BLOCK);
                         ADFUtils.findOperation(OPR_INCREMENT_LABOR).execute();
                         oper = ADFUtils.findOperation(OPR_CHECK_EMPTY);
                         results = (List<String>) oper.execute();
                         if (results.size() == 1) {
                             this.hideMessagesPanel();
                             ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F5_KEY_CODE.intValue()));
                             this.getDialogOutputTextCE().setValue(results.get(0));
                             this.getInputChildFromContainer().setDisabled(true);
                             this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                             this.getConfirmPopupCE().show(new RichPopup.PopupHints());
                         } else if (results.size() > 1) {
                             this.showMessagesPanel(results.get(0), results.get(1));
                         } else {
                             action = this.continueF5Logic();
                         }
                     }         
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
       
        return action;
    }

    public String yesLinkConfirmPopupCEAction() {
        String action = null;
        OperationBinding oper = ADFUtils.findOperation(OPR_MARK_LOCATION);
        String result = (String) oper.execute();
        if ("SUCCESS".equals(result)) {
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
        }
        Integer f4KeyCode = Integer.valueOf(F4_KEY_CODE.intValue());
        if (f4KeyCode.equals(ADFUtils.getBoundAttributeValue("KeyPressed"))) {
            action = this.continueF4Logic();
        } else {
            action = this.continueF5Logic();
        }
        return action;
    }

    public String noLinkConfirmPopupCEAction() {
        String action = null;
        Integer f4KeyCode = Integer.valueOf(F4_KEY_CODE.intValue());
        if (f4KeyCode.equals(ADFUtils.getBoundAttributeValue("KeyPressed"))) {
            action = this.continueF4Logic();
        } else {
            action = this.continueF5Logic();
        }
        return action;
    }

    private String continueF4Logic() {
        String action = null;
        this.getConfirmPopupCE().hide();
        
        this.getInputChildFromContainer().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputChildFromContainer());
        
        ADFUtils.setBoundAttributeValue("KeyPressed", null);
        
        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        OperationBinding oper = ADFUtils.findOperation(OPR_LOAD_SCRN_WRAP);
        ContainerPickResults result = (ContainerPickResults) oper.execute();
        if (result.isOperationError()) {
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
        } 
        else if (result.getIsTrExit()) {
            action = pfBean.getActionByBlockName(result.getNavigationBlock());
            
            if(this.logoutExitBTF().equals(action)){
                action = null;
                this.getInputChildFromContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        } 
        else if (result.getNavigationBlock() != null) {
            if (result.getMessage() != null && !result.getMessage().isEmpty() &&
                // Noor 1603
                !"TO_LOCATION".equalsIgnoreCase(result.getNavigationBlock())) {
                this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
            }
            oper = ADFUtils.findOperation(OPR_DO_COMMIT);
            oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                if (result.getNavigationItem() != null) {
                    pfBean.setIsFocusOn(result.getNavigationBlock() + "." + result.getNavigationItem());
                }
                // Noor 1603
                String msgReturn = result.getMessage() == null ? "" : result.getMessage().get(1);
                if (msgReturn != null && !msgReturn.isEmpty()) {
                    if ("TO_LOCATION".equalsIgnoreCase(result.getNavigationBlock())) {
                        this.getInputChildFromContainer().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                        this.getOkToLocatioInputText().setValue(msgReturn);
                        this.getOkGoToLocationPpoup().setLauncherVar(pfBean.getActionByBlockName(result.getNavigationBlock()));
                        this.getOkGoToLocationPpoup().show(new RichPopup.PopupHints());
                        return null;
                    }
                }
                action = pfBean.getActionByBlockName(result.getNavigationBlock());
            }
        } 
        else if (result.getMessage() != null && !result.getMessage().isEmpty()) {
            this.showMessagesPanel(result.getMessage().get(0), result.getMessage().get(1));
        }
        return action;
    }

    private String continueF5Logic() {
        String action = null;
        this.getConfirmPopupCE().hide();
        
        this.getInputChildFromContainer().setDisabled(false);
        this.setFocusOnUIComponent(this.getInputChildFromContainer());
        
        ADFUtils.setBoundAttributeValue("KeyPressed", null);
        
        ADFUtils.setBoundAttributeValue("ActualPickedQty", new BigDecimal(0));
        OperationBinding oper = ADFUtils.findOperation(OPR_NAV_TO_LOCATION);
        oper.getParamsMap().put("fullDoneFlagIn", "F");
        List<String> results = (List<String>) oper.execute();
        if (results != null && results.size() == 2) {
            this.showMessagesPanel(results.get(0), results.get(1));
        } else if (results != null && results.size() == 1) {
            this.hideMessagesPanel();
            HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
            action = pfBean.getActionByBlockName(results.get(0));
            
            if(this.logoutExitBTF().equals(action)){
                action = null;
                this.getInputChildFromContainer().setDisabled(true);
                this.refreshContentOfUIComponent(this.getInputChildFromContainer());
                String message =
                    this.getMessage(DMS_ERROR, ERROR, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                    (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));
                this.getDmsErrorOutputText().setValue(message);
                this.getDmsErrorPopup().show(new RichPopup.PopupHints());
            }
        }
        return action;
    }

    public String lpnAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F6,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                     OperationBinding oper = ADFUtils.findOperation(OPR_DISPLAY_LPN);
                     oper.getParamsMap().put("callingBlock", HhContainerPickSBean.FROM_MASTER_CHILD2_BLOCK);
                     List<String> results = (List<String>) oper.execute();
                     if (results != null) {
                         if (results.size() == 1) {
                             action =
                                 ((HhContainerPickSBean) getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                         } else {
                             this.showMessagesPanel(results.get(0), results.get(1));
                         }
                     }         
                 
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
       
        return action;
    }

    public String byPassAction() {
        String action = null;
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F7,RoleBasedAccessConstants.FORM_NAME_hh_container_pick_s)){
                            
                     if (NO.equals(ADFUtils.getBoundAttributeValue("ContainerPicked"))) {
                         OperationBinding oper = ADFUtils.findOperation(OPR_FROM_MASTER_CHILD2_F7);
                         List<String> results = (List<String>) oper.execute();
                         if (results != null) {
                             if (results.size() == 1) {
                                 action =
                                     ((HhContainerPickSBean) getPageFlowBean("HhContainerPickSBean")).getActionByBlockName(results.get(0));
                             } else {
                                 this.showMessagesPanel(results.get(0), results.get(1));
                             }
                         }
                     } else {
                         this.showMessagesPanel(WARN,
                                                this.getMessage(CAN_NOT_CANCEL, WARN,
                                                                (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                                (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                     }
                 }else{
                   
                     
                     this.showMessagesPanel(ERROR,
                                            this.getMessage("NOT_ALLOWED", ERROR,
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                            (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                 }
        
        
        return action;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed"))
        {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            // WE HAVE TO UPDATE THE MODEL...
            ADFUtils.setBoundAttributeValue("ChildFromContainer", submittedValue);
            
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                RichInputText component = (RichInputText) clientEvent.getComponent();
                if (submittedValue == null || "".equals(submittedValue)) {
                    this.showMessagesPanel(ERROR,
                                           this.getMessage(INV_CONTAINER, ERROR,
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE)));
                    this.addErrorStyleToComponent(component);
                    this.getIconChildFromContainer().setName("error");
                    this.refreshContentOfUIComponent(this.getIconChildFromContainer());
                }
                else{
                    this.onChangedChildFromContainerFromMasterChild2(new ValueChangeEvent(this.getInputChildFromContainer(), null, submittedValue));
                }
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5FullLink());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6LpnLink());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7ByPassLink());
                actionEvent.queue();
            }
        }
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }

    public void performKeyPopupCE(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopupCE());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopupCE());
                actionEvent.queue();
            }
        }
    }

    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) {
        ADFUtils.setBoundAttributeValue("MainBlock", HhContainerPickSBean.FROM_MASTER_CHILD2_BLOCK);

        HhContainerPickSBean pfBean = (HhContainerPickSBean) this.getPageFlowBean("HhContainerPickSBean");
        if (DIFF_PICK.equals(pfBean.getExecuteActionFromMasterChild2())) {
            pfBean.setExecuteActionFromMasterChild2(null);
            String message =
                this.getMessage(DIFF_PICK, CONF, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID),
                                (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE));

            this.getDialogOutputText().setValue(message);
            this.getInputChildFromContainer().setDisabled(true);
            this.refreshContentOfUIComponent(this.getInputChildFromContainer());
            this.getConfirmPopup().show(new RichPopup.PopupHints());
        } else if (CHECK_EMPTY.equals(pfBean.getExecuteActionFromMasterChild2())) {
            String message = pfBean.getMessageFromMasterChild2();
            pfBean.setExecuteActionFromMasterChild2(null);
            pfBean.setMessageFromMasterChild2(null);

            this.getDialogOutputTextCE().setValue(message);
            ADFUtils.setBoundAttributeValue("KeyPressed", Integer.valueOf(F4_KEY_CODE.intValue()));
            this.getInputChildFromContainer().setDisabled(true);
            this.refreshContentOfUIComponent(this.getInputChildFromContainer());
            this.getConfirmPopupCE().show(new RichPopup.PopupHints());
        } else if (pfBean.getExecuteActionFromMasterChild2() != null &&
                   pfBean.getExecuteActionFromMasterChild2().contains("||")) {
            String executeAction = pfBean.getExecuteActionFromMasterChild2();
            pfBean.setExecuteActionFromMasterChild2(null);

            String[] messages = executeAction.split("||");
            this.showMessagesPanel(messages[0], messages[1]);
        }
    }
    
    public void setInputChildFromContainer(RichInputText inputChildFromContainer) {
        this.inputChildFromContainer = inputChildFromContainer;
    }

    public RichInputText getInputChildFromContainer() {
        return inputChildFromContainer;
    }

    public void setIconChildFromContainer(RichIcon iconChildFromContainer) {
        this.iconChildFromContainer = iconChildFromContainer;
    }

    public RichIcon getIconChildFromContainer() {
        return iconChildFromContainer;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF5FullLink(RichLink f5FullLink) {
        this.f5FullLink = f5FullLink;
    }

    public RichLink getF5FullLink() {
        return f5FullLink;
    }

    public void setF6LpnLink(RichLink f6LpnLink) {
        this.f6LpnLink = f6LpnLink;
    }

    public RichLink getF6LpnLink() {
        return f6LpnLink;
    }

    public void setF7ByPassLink(RichLink f7ByPassLink) {
        this.f7ByPassLink = f7ByPassLink;
    }

    public RichLink getF7ByPassLink() {
        return f7ByPassLink;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setConfirmPopupCE(RichPopup confirmPopupCE) {
        this.confirmPopupCE = confirmPopupCE;
    }

    public RichPopup getConfirmPopupCE() {
        return confirmPopupCE;
    }

    public void setDialogOutputTextCE(RichOutputText dialogOutputTextCE) {
        this.dialogOutputTextCE = dialogOutputTextCE;
    }

    public RichOutputText getDialogOutputTextCE() {
        return dialogOutputTextCE;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getIconErrorMessage().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getIconErrorMessage().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getIconErrorMessage().setName(LOGO_ICON);
        } else {
            this.getIconErrorMessage().setName(ERR_ICON);
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public boolean isErrorOnThePage() {
        boolean error = false;
        if (this.getIconErrorMessage() != null) {
            error = this.getIconErrorMessage().isVisible() && "error".equals(this.getIconErrorMessage().getName());
        }
        return error;
    }

    public void setOkGoToLocationDialog(RichDialog okGoToLocationDialog) {
        this.okGoToLocationDialog = okGoToLocationDialog;
    }

    public RichDialog getOkGoToLocationDialog() {
        return okGoToLocationDialog;
    }

    public String okToLocationAction() {
        return this.getOkGoToLocationPpoup().getLauncherVar();
    }

    public void setOkToLocatioInputText(RichOutputText okToLocatioInputText) {
        this.okToLocatioInputText = okToLocatioInputText;
    }

    public RichOutputText getOkToLocatioInputText() {
        return okToLocatioInputText;
    }

    public void setOkGoToLocationPpoup(RichPopup okGoToLocationPpoup) {
        this.okGoToLocationPpoup = okGoToLocationPpoup;
    }

    public RichPopup getOkGoToLocationPpoup() {
        return okGoToLocationPpoup;
    }
    
    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }
    
    public void setYesLinkPopupCE(RichLink yesLinkPopupCE) {
        this.yesLinkPopupCE = yesLinkPopupCE;
    }

    public RichLink getYesLinkPopupCE() {
        return yesLinkPopupCE;
    }

    public void setNoLinkPopupCE(RichLink noLinkPopupCE) {
        this.noLinkPopupCE = noLinkPopupCE;
    }

    public RichLink getNoLinkPopupCE() {
        return noLinkPopupCE;
    }
    
    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }
    
    public void setDmsErrorPopUpLink(RichLink dmsErrorPopUpLink) {
        this.dmsErrorPopUpLink = dmsErrorPopUpLink;
    }

    public RichLink getDmsErrorPopUpLink() {
        return dmsErrorPopUpLink;
    }
    
    public void performKeyDmsErrorPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDmsErrorPopUpLink());
                actionEvent.queue();
            }
        }
    }
}
