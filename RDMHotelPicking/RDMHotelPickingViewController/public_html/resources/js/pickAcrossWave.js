function setInitialFocus() {
    setTimeout(function () {
        var focusOnValue = $("input[id*='focus']").val();
        if ((focusOnValue != null)) {
            switch (focusOnValue) {
                case 'StartPickLocField':
                    SetFocusOnUIcomp("input[id*='sLoc']");
                    break;
                case 'PickTypeField':
                    SetFocusOnUIcomp("input[id*='pT']");
                    break;
                case 'UpsField':
                    SetFocusOnUIcomp("input[id*='ups']");
                    break;
                case 'WaveDateField':
                    SetFocusOnUIcomp("input[id*='it1']");
                    break;
                case 'PickField':
                    SetFocusOnUIcomp("input[id*='pick']");
                    break;
                default :
                    break;
            }
        }
    },
0);
}

function customPAWFindElementById(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function onKeyPressed(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}

function checkForPick(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
        event.cancel();
    }
}

function OnOpenConfirmPopup(event) {
    OnOpen(event, 'ylp');
}

function noYesLinkConfirmPopupPAW(event) {
    fLinksKeyHandlerPAW(event, 'p1')
}

function fLinksKeyHandlerPAW(event, popupId) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
    var fLink = component;
        if (keyPressed == AdfKeyStroke.F2_KEY) {
            fLink = component.findComponent('nlp');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);
    }
}

function OnOpen(event, linkId) {
    var component = event.getSource();
    var linkComp = component.findComponent(linkId);
    linkComp.focus();
}