function custFind(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function setPage2InitialFocus() {
    setTimeout(function () {
        var focusOn = custFind('focus');
       
        if ((focusOn != null) && (focusOn.value != null)) {
         var focusOnValue = focusOn.value;
            if (focusOnValue == 'MULTI_SKU.CONTAINER_ID') {
                setFocusOrSelectOnUIcomp("input[id*='cidm']");
            }
            else {
                if (focusOnValue == 'MULTI_SKU.CONFIRM_LOC') {
                    setFocusOrSelectOnUIcomp("input[id*='clms']");
                }
                else {
                    setFocusOrSelectOnUIcomp("input[id*='cqms']");
                }
            }
        }

    },
0);
}

function setPage4InitialFocus() {
    setTimeout(function () {
        setFocusOrSelectOnUIcomp("input[id*='ccmi']");

    },
0);
}

function onKeyPressedOnconfirmLocMultiSku(event) {
    this.onKeyPressedPage2(event, 'clms');
}

function onKeyPressedOnconfirmQtyMultiSku(event) {
    this.onKeyPressedPage2(event, 'cqms');
}

function onKeyPressedOncontainerIdMultiSku(event) {
    this.onKeyPressedPage2(event, 'cidm');
}

function onKeyPressedPage2(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
        event.cancel();
    }
}

function putawaySetPage1InitialFocus() {
    setTimeout(function () {
        var focusOn = custFind('focus');
        if ((focusOn != null) && (focusOn.value != null)) {
        var focusOnValue = focusOn.value;
            if (focusOnValue == 'MAIN.CONTAINER_ID') {
                setFocusOrSelectOnUIcomp("input[id*='mcid']");
            }
            else {
                if (focusOnValue == 'MAIN.TOLOCATION_ID_R' || focusOnValue == 'MAIN.CONFIRM_CASE_QTY_R') {
                    setFocusOrSelectOnUIcomp("input[id*='mlid']");
                }
            }
        }
    },
0);
}

function onKeyPressedOnContainer(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        this.onKeyPressedPage1(event, 'mcid');
    }
}

function onKeyPressedOnToLocation(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.TAB_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY)
        this.onKeyPressedPage1(event, 'mlid');
}

function onKeyPressedOnConfirmQty(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY)
        this.onKeyPressedPage1(event, 'ccqt');
}

function onKeyPressedPage1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    var clientId = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F5_KEY) && clientId == 'mcid') {
        event.cancel();
    }

    else if (keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : item
        },
true);
        event.cancel();
    }
}
function fLinksKeyHandler(event, popupId, yesLink) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var item = component.getClientId().split(':')[3];
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else if ((keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) && popupId != 'gtp') {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F1_KEY) {
            fLink = component.findComponent(yesLink);
            fLink.focus();
        }
        AdfCustomEvent.queue(fLink, "customKeyEvent", 
        {
            keyPressed : keyPressed, item : item
        },
true);
    }
    else if (keyPressed == AdfKeyStroke.F1_KEY && popupId == 'gtp') {
        AdfActionEvent.queue(component, true);
    }
    else if (keyPressed != AdfKeyStroke.F1_KEY && popupId == 'gtp') {
    }
    else {
        var popup = component.findComponent(popupId);
        popup.hide();
        event.cancel();
    }
}

function noYesLinkConfirmPopup(event) {
    fLinksKeyHandler(event, 'cp', 'ylp')
}

function noYesLinkMarkPopup(event) {
    fLinksKeyHandler(event, 'mp', 'ybpm')
}

function okLinkGoPopup(event) {
    fLinksKeyHandler(event, 'gtp')
}

function OnOpenMarkPopup(event) {
    OnOpen(event, 'nbpm');
}

function OnOpenPopup(event) {
    OnOpen(event, 'nlp');
}

function onOpenGoToPopup(event) {
    OnOpen(event, 'oklp');
}

function OnOpen(event, linkId) {
    var component = event.getSource();
    var linkComp = component.findComponent(linkId);
    linkComp.focus();
}

function setPage3InitialFocus() {
    setTimeout(function () {
        setFocusOrSelectOnUIcomp("input[id*='lnf']");
    },
0);
}

function onKeyPressedOnCapacityNewFpl(event) {
    this.onKeyPressedPage3(event, 'cnf');
}

function onKeyPressedOnLocationIdNewFpl(event) {
    this.onKeyPressedPage3(event, 'lnf');
}

function onKeyPressedPage3(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
        event.cancel();
    }
    else {
        if (item == 'cnf') {
            if (keyPressed == 8) {

            }
            else {
                if (keyPressed < 48 || keyPressed > 57) {
                    event.cancel();
                }
            }
        }
    }
}

function fLinksExitPopup(event, popupId) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        if (keyPressed == AdfKeyStroke.F1_KEY) {
            var yesLink = component.findComponent('yel');
            AdfActionEvent.queue(yesLink, true);
        }
        else if (keyPressed == AdfKeyStroke.F2_KEY) {
            AdfActionEvent.queue(component, true);

        }

    }
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        var popup = component.findComponent(popupId);
        popup.hide();
        event.cancel();
    }

}

function onKeyPressedConfrmContainerId(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var fLink;
    switch (keyPressed) {
        case AdfKeyStroke.F7_KEY:
            fLink = component.findComponent('f7');
            break;
        case AdfKeyStroke.F4_KEY:
            fLink = component.findComponent('f4');
            break;
        case AdfKeyStroke.F3_KEY:
            fLink = component.findComponent('f3');
            break;
        default :
            break;
    }
    AdfActionEvent.queue(fLink, true);

}

function OnOpenExitPopup(event) {
    OnOpen(event, 'nel');
}

function noLinkConfirmPopup(event) {
    fLinksExitPopup(event, 'ep')
}

function yesLinkConfirmPopup(event) {
    fLinksExitPopup(event, 'ep')
}

function setReasonInitialFocus() {
    setTimeout(function () {
        var focusOn = custFind('codeLov');
        if (focusOn != null) {
            openListOfValues(focusOn);
        }
    },
0);
}

function onKeyPressedOnCodeLov(event) {
    this.onKeyPressedLov(event, 'codeLov');
}

function onKeyPressedLov(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var focusOn = custFind('codeLov');
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    else {
        switch (keyPressed) {
            case AdfKeyStroke.ARROWDOWN_KEY:
                nextSocItem(focusOn);
                break;
            case AdfKeyStroke.ARROWUP_KEY:
                prevSocItem(focusOn);
                break;
            default :
                break;
        }
    }
    event.cancel();
}

function onClickOnCodeLov(event) {
    openListOfValues(custFind('codeLov'));
}