function okLinkGoAuthPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        var yesOk = component.findComponent('okau');
        AdfActionEvent.queue(yesOk, true);
    }
    event.cancel();
}

function onOpenAuthPopup(event) {
    OnOpen(event, 'okau');
}

function onKeyAuth(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}