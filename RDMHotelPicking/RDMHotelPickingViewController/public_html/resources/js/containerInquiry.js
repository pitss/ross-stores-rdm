function onKeyPressedContainerInquiryPage1(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY ||
        keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F9_KEY ||
        keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);        
    }
    event.cancel();
}