package com.ross.rdm.hotelpicking.hhnopickpacks.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Sep 01 13:41:17 EDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhNoPickPackSNoPickPackViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        WaveNbr {
            public Object get(HhNoPickPackSNoPickPackViewRowImpl obj) {
                return obj.getWaveNbr();
            }

            public void put(HhNoPickPackSNoPickPackViewRowImpl obj, Object value) {
                obj.setWaveNbr((String) value);
            }
        }
        ,
        StartPickLoc {
            public Object get(HhNoPickPackSNoPickPackViewRowImpl obj) {
                return obj.getStartPickLoc();
            }

            public void put(HhNoPickPackSNoPickPackViewRowImpl obj, Object value) {
                obj.setStartPickLoc((String) value);
            }
        }
        ,
        PickType {
            public Object get(HhNoPickPackSNoPickPackViewRowImpl obj) {
                return obj.getPickType();
            }

            public void put(HhNoPickPackSNoPickPackViewRowImpl obj, Object value) {
                obj.setPickType((String) value);
            }
        }
        ,
        Pick {
            public Object get(HhNoPickPackSNoPickPackViewRowImpl obj) {
                return obj.getPick();
            }

            public void put(HhNoPickPackSNoPickPackViewRowImpl obj, Object value) {
                obj.setPick((String) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhNoPickPackSNoPickPackViewRowImpl object);

        public abstract void put(HhNoPickPackSNoPickPackViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int WAVENBR = AttributesEnum.WaveNbr.index();
    public static final int STARTPICKLOC = AttributesEnum.StartPickLoc.index();
    public static final int PICKTYPE = AttributesEnum.PickType.index();
    public static final int PICK = AttributesEnum.Pick.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhNoPickPackSNoPickPackViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute WaveNbr.
     * @return the WaveNbr
     */
    public String getWaveNbr() {
        return (String) getAttributeInternal(WAVENBR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute WaveNbr.
     * @param value value to set the  WaveNbr
     */
    public void setWaveNbr(String value) {
        setAttributeInternal(WAVENBR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute StartPickLoc.
     * @return the StartPickLoc
     */
    public String getStartPickLoc() {
        return (String) getAttributeInternal(STARTPICKLOC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute StartPickLoc.
     * @param value value to set the  StartPickLoc
     */
    public void setStartPickLoc(String value) {
        setAttributeInternal(STARTPICKLOC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickType.
     * @return the PickType
     */
    public String getPickType() {
        return (String) getAttributeInternal(PICKTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickType.
     * @param value value to set the  PickType
     */
    public void setPickType(String value) {
        setAttributeInternal(PICKTYPE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Pick.
     * @return the Pick
     */
    public String getPick() {
        return (String) getAttributeInternal(PICK);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Pick.
     * @param value value to set the  Pick
     */
    public void setPick(String value) {
        setAttributeInternal(PICK, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

