package com.ross.rdm.hotelpicking.hhnopickpacks.model.views;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.common.HhNoPickPackSWorkView;
import com.ross.rdm.hotelpicking.model.services.HotelPickingAppModuleImpl;

import java.math.BigDecimal;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mo Jul 27 22:27:05 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhNoPickPackSWorkViewImpl extends RDMTransientViewObjectImpl implements HhNoPickPackSWorkView {


    /**
     * This is the default constructor (do not remove).
     */
    public HhNoPickPackSWorkViewImpl() {
    }

    public void setHhNoPickPackWorkVariables() {
        HhNoPickPackSWorkViewRowImpl hhNoPickPackWorkVRowImpl = (HhNoPickPackSWorkViewRowImpl) this.createRow();
        HotelPickingAppModuleImpl applicationModule = (HotelPickingAppModuleImpl) this.getApplicationModule();
        GlobalVariablesViewRowImpl gVViewImplRowImpl =
            (GlobalVariablesViewRowImpl) applicationModule.getGlobalVariablesView().getCurrentRow();

        hhNoPickPackWorkVRowImpl.setUser(gVViewImplRowImpl.getGlobalUserId());
        hhNoPickPackWorkVRowImpl.setHeader(gVViewImplRowImpl.getGlobalHeader());
        hhNoPickPackWorkVRowImpl.setFacilityId(gVViewImplRowImpl.getGlobalFacilityId());
        hhNoPickPackWorkVRowImpl.setUserPrivilege(new BigDecimal(gVViewImplRowImpl.getGlobalUserPrivilege()));
        hhNoPickPackWorkVRowImpl.setDevice(gVViewImplRowImpl.getGlobalDevice());
        hhNoPickPackWorkVRowImpl.setCallingBlock("MAIN");
        hhNoPickPackWorkVRowImpl.setLanguageCode(gVViewImplRowImpl.getGlobalLanguageCode());

        GlobalVariablesViewRowImpl gVViewImpl =
            (GlobalVariablesViewRowImpl) applicationModule.getGlobalVariablesView().getCurrentRow();
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", gVViewImpl.getGlobalFacilityId(), gVViewImpl.getGlobalLanguageCode());

        if ("UNDEFINED".equalsIgnoreCase(hhNoPickPackWorkVRowImpl.getHeader())) {
            hhNoPickPackWorkVRowImpl.setHeader(((HotelPickingAppModuleImpl) applicationModule).callDisplayHeader(hhNoPickPackWorkVRowImpl.getFacilityId(),
                                                                                                                 hhNoPickPackWorkVRowImpl.getLanguageCode(),
                                                                                                                 "HH_NO_PICK_PACK_S"));
        }
        this.insertRow(hhNoPickPackWorkVRowImpl);
        this.setCurrentRow(hhNoPickPackWorkVRowImpl);
        //this.getDBTransaction().commit();

    }
}
