package com.ross.rdm.hotelpicking.hhcontainerpicks.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 14 16:13:58 CST 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhContainerPickSFromMasterChild2ViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        MasterFromContainer {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getMasterFromContainer();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setMasterFromContainer((String) value);
            }
        }
        ,
        ItemId {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getItemId();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setItemId((String) value);
            }
        }
        ,
        ChildFromContainer {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getChildFromContainer();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setChildFromContainer((String) value);
            }
        }
        ,
        PrevChildFromContainer {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getPrevChildFromContainer();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setPrevChildFromContainer((String) value);
            }
        }
        ,
        PickContainerQty {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getPickContainerQty();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setPickContainerQty((BigDecimal) value);
            }
        }
        ,
        FirstScan {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getFirstScan();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setFirstScan((String) value);
            }
        }
        ,
        PickToOrigToLoc {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getPickToOrigToLoc();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setPickToOrigToLoc((String) value);
            }
        }
        ,
        PickToOrigFinalLoc {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getPickToOrigFinalLoc();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setPickToOrigFinalLoc((String) value);
            }
        }
        ,
        CurrentPickToDest {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getCurrentPickToDest();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setCurrentPickToDest((BigDecimal) value);
            }
        }
        ,
        MasterMixedDestFlag {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getMasterMixedDestFlag();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setMasterMixedDestFlag((String) value);
            }
        }
        ,
        KeyPressed {
            public Object get(HhContainerPickSFromMasterChild2ViewRowImpl obj) {
                return obj.getKeyPressed();
            }

            public void put(HhContainerPickSFromMasterChild2ViewRowImpl obj, Object value) {
                obj.setKeyPressed((Integer) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhContainerPickSFromMasterChild2ViewRowImpl object);

        public abstract void put(HhContainerPickSFromMasterChild2ViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int MASTERFROMCONTAINER = AttributesEnum.MasterFromContainer.index();
    public static final int ITEMID = AttributesEnum.ItemId.index();
    public static final int CHILDFROMCONTAINER = AttributesEnum.ChildFromContainer.index();
    public static final int PREVCHILDFROMCONTAINER = AttributesEnum.PrevChildFromContainer.index();
    public static final int PICKCONTAINERQTY = AttributesEnum.PickContainerQty.index();
    public static final int FIRSTSCAN = AttributesEnum.FirstScan.index();
    public static final int PICKTOORIGTOLOC = AttributesEnum.PickToOrigToLoc.index();
    public static final int PICKTOORIGFINALLOC = AttributesEnum.PickToOrigFinalLoc.index();
    public static final int CURRENTPICKTODEST = AttributesEnum.CurrentPickToDest.index();
    public static final int MASTERMIXEDDESTFLAG = AttributesEnum.MasterMixedDestFlag.index();
    public static final int KEYPRESSED = AttributesEnum.KeyPressed.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhContainerPickSFromMasterChild2ViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute MasterFromContainer.
     * @return the MasterFromContainer
     */
    public String getMasterFromContainer() {
        return (String) getAttributeInternal(MASTERFROMCONTAINER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MasterFromContainer.
     * @param value value to set the  MasterFromContainer
     */
    public void setMasterFromContainer(String value) {
        setAttributeInternal(MASTERFROMCONTAINER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemId.
     * @return the ItemId
     */
    public String getItemId() {
        return (String) getAttributeInternal(ITEMID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemId.
     * @param value value to set the  ItemId
     */
    public void setItemId(String value) {
        setAttributeInternal(ITEMID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ChildFromContainer.
     * @return the ChildFromContainer
     */
    public String getChildFromContainer() {
        return (String) getAttributeInternal(CHILDFROMCONTAINER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ChildFromContainer.
     * @param value value to set the  ChildFromContainer
     */
    public void setChildFromContainer(String value) {
        setAttributeInternal(CHILDFROMCONTAINER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PrevChildFromContainer.
     * @return the PrevChildFromContainer
     */
    public String getPrevChildFromContainer() {
        return (String) getAttributeInternal(PREVCHILDFROMCONTAINER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PrevChildFromContainer.
     * @param value value to set the  PrevChildFromContainer
     */
    public void setPrevChildFromContainer(String value) {
        setAttributeInternal(PREVCHILDFROMCONTAINER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickContainerQty.
     * @return the PickContainerQty
     */
    public BigDecimal getPickContainerQty() {
        return (BigDecimal) getAttributeInternal(PICKCONTAINERQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickContainerQty.
     * @param value value to set the  PickContainerQty
     */
    public void setPickContainerQty(BigDecimal value) {
        setAttributeInternal(PICKCONTAINERQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute FirstScan.
     * @return the FirstScan
     */
    public String getFirstScan() {
        return (String) getAttributeInternal(FIRSTSCAN);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute FirstScan.
     * @param value value to set the  FirstScan
     */
    public void setFirstScan(String value) {
        setAttributeInternal(FIRSTSCAN, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickToOrigToLoc.
     * @return the PickToOrigToLoc
     */
    public String getPickToOrigToLoc() {
        return (String) getAttributeInternal(PICKTOORIGTOLOC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickToOrigToLoc.
     * @param value value to set the  PickToOrigToLoc
     */
    public void setPickToOrigToLoc(String value) {
        setAttributeInternal(PICKTOORIGTOLOC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickToOrigFinalLoc.
     * @return the PickToOrigFinalLoc
     */
    public String getPickToOrigFinalLoc() {
        return (String) getAttributeInternal(PICKTOORIGFINALLOC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickToOrigFinalLoc.
     * @param value value to set the  PickToOrigFinalLoc
     */
    public void setPickToOrigFinalLoc(String value) {
        setAttributeInternal(PICKTOORIGFINALLOC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CurrentPickToDest.
     * @return the CurrentPickToDest
     */
    public BigDecimal getCurrentPickToDest() {
        return (BigDecimal) getAttributeInternal(CURRENTPICKTODEST);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CurrentPickToDest.
     * @param value value to set the  CurrentPickToDest
     */
    public void setCurrentPickToDest(BigDecimal value) {
        setAttributeInternal(CURRENTPICKTODEST, value);
    }

    /**
     * Gets the attribute value for the calculated attribute MasterMixedDestFlag.
     * @return the MasterMixedDestFlag
     */
    public String getMasterMixedDestFlag() {
        return (String) getAttributeInternal(MASTERMIXEDDESTFLAG);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MasterMixedDestFlag.
     * @param value value to set the  MasterMixedDestFlag
     */
    public void setMasterMixedDestFlag(String value) {
        setAttributeInternal(MASTERMIXEDDESTFLAG, value);
    }

    /**
     * Gets the attribute value for the calculated attribute KeyPressed.
     * @return the KeyPressed
     */
    public Integer getKeyPressed() {
        return (Integer) getAttributeInternal(KEYPRESSED);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute KeyPressed.
     * @param value value to set the  KeyPressed
     */
    public void setKeyPressed(Integer value) {
        setAttributeInternal(KEYPRESSED, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

