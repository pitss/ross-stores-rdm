package com.ross.rdm.hotelpicking.hhcontainerpicks.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Sep 10 11:31:15 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhContainerPickSLpnViewImpl extends RDMTransientViewObjectImpl {

  
  /**
   * This is the default constructor (do not remove).
   */
  public HhContainerPickSLpnViewImpl() { 
  } 

    /**
     * Returns the bind variable value for pFacilityId.
     * @return bind variable value for pFacilityId
     */
    public String getpFacilityId() {
        return (String) getNamedWhereClauseParam("pFacilityId");
    }

    /**
     * Sets <code>value</code> for bind variable pFacilityId.
     * @param value value to bind as pFacilityId
     */
    public void setpFacilityId(String value) {
        setNamedWhereClauseParam("pFacilityId", value);
    }

    /**
     * Returns the bind variable value for pContainerId.
     * @return bind variable value for pContainerId
     */
    public String getpContainerId() {
        return (String) getNamedWhereClauseParam("pContainerId");
    }

    /**
     * Sets <code>value</code> for bind variable pContainerId.
     * @param value value to bind as pContainerId
     */
    public void setpContainerId(String value) {
        setNamedWhereClauseParam("pContainerId", value);
    }

    /**
     * Returns the bind variable value for pItemId.
     * @return bind variable value for pItemId
     */
    public String getpItemId() {
        return (String) getNamedWhereClauseParam("pItemId");
    }

    /**
     * Sets <code>value</code> for bind variable pItemId.
     * @param value value to bind as pItemId
     */
    public void setpItemId(String value) {
        setNamedWhereClauseParam("pItemId", value);
    }
}
