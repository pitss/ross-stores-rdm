package com.ross.rdm.hotelpicking.hhcontainerpicks.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 14 16:12:46 CST 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhContainerPickSFromMasterChildViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        MasterFromContainer {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getMasterFromContainer();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setMasterFromContainer((String) value);
            }
        }
        ,
        ItemId {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getItemId();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setItemId((String) value);
            }
        }
        ,
        ChildFromContainer {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getChildFromContainer();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setChildFromContainer((String) value);
            }
        }
        ,
        PrevChildFromContainer {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getPrevChildFromContainer();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setPrevChildFromContainer((String) value);
            }
        }
        ,
        PickContainerQty {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getPickContainerQty();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setPickContainerQty((BigDecimal) value);
            }
        }
        ,
        SortOrder {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getSortOrder();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setSortOrder((BigDecimal) value);
            }
        }
        ,
        PickOrder {
            public Object get(HhContainerPickSFromMasterChildViewRowImpl obj) {
                return obj.getPickOrder();
            }

            public void put(HhContainerPickSFromMasterChildViewRowImpl obj, Object value) {
                obj.setPickOrder((BigDecimal) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhContainerPickSFromMasterChildViewRowImpl object);

        public abstract void put(HhContainerPickSFromMasterChildViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int MASTERFROMCONTAINER = AttributesEnum.MasterFromContainer.index();
    public static final int ITEMID = AttributesEnum.ItemId.index();
    public static final int CHILDFROMCONTAINER = AttributesEnum.ChildFromContainer.index();
    public static final int PREVCHILDFROMCONTAINER = AttributesEnum.PrevChildFromContainer.index();
    public static final int PICKCONTAINERQTY = AttributesEnum.PickContainerQty.index();
    public static final int SORTORDER = AttributesEnum.SortOrder.index();
    public static final int PICKORDER = AttributesEnum.PickOrder.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhContainerPickSFromMasterChildViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute MasterFromContainer.
     * @return the MasterFromContainer
     */
    public String getMasterFromContainer() {
        return (String) getAttributeInternal(MASTERFROMCONTAINER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MasterFromContainer.
     * @param value value to set the  MasterFromContainer
     */
    public void setMasterFromContainer(String value) {
        setAttributeInternal(MASTERFROMCONTAINER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemId.
     * @return the ItemId
     */
    public String getItemId() {
        return (String) getAttributeInternal(ITEMID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemId.
     * @param value value to set the  ItemId
     */
    public void setItemId(String value) {
        setAttributeInternal(ITEMID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ChildFromContainer.
     * @return the ChildFromContainer
     */
    public String getChildFromContainer() {
        return (String) getAttributeInternal(CHILDFROMCONTAINER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ChildFromContainer.
     * @param value value to set the  ChildFromContainer
     */
    public void setChildFromContainer(String value) {
        setAttributeInternal(CHILDFROMCONTAINER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PrevChildFromContainer.
     * @return the PrevChildFromContainer
     */
    public String getPrevChildFromContainer() {
        return (String) getAttributeInternal(PREVCHILDFROMCONTAINER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PrevChildFromContainer.
     * @param value value to set the  PrevChildFromContainer
     */
    public void setPrevChildFromContainer(String value) {
        setAttributeInternal(PREVCHILDFROMCONTAINER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickContainerQty.
     * @return the PickContainerQty
     */
    public BigDecimal getPickContainerQty() {
        return (BigDecimal) getAttributeInternal(PICKCONTAINERQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickContainerQty.
     * @param value value to set the  PickContainerQty
     */
    public void setPickContainerQty(BigDecimal value) {
        setAttributeInternal(PICKCONTAINERQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute SortOrder.
     * @return the SortOrder
     */
    public BigDecimal getSortOrder() {
        return (BigDecimal) getAttributeInternal(SORTORDER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SortOrder.
     * @param value value to set the  SortOrder
     */
    public void setSortOrder(BigDecimal value) {
        setAttributeInternal(SORTORDER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickOrder.
     * @return the PickOrder
     */
    public BigDecimal getPickOrder() {
        return (BigDecimal) getAttributeInternal(PICKORDER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickOrder.
     * @param value value to set the  PickOrder
     */
    public void setPickOrder(BigDecimal value) {
        setAttributeInternal(PICKORDER, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

