package com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.common;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class ContainerPickResults implements Serializable
{
    @SuppressWarnings("compatibility:3925413583913899978")
    private static final long serialVersionUID = 1L;
    
    private String navigationBlock;
    private String navigationItem;
    private boolean isTrExit;
    private List<String> message;
    private List<String> messagePopUp;
    private BigDecimal functionResult;
    private boolean operationError;
    private boolean raiseFailure;

    public ContainerPickResults() {
        super();
        this.message = new ArrayList<String>();
        this.messagePopUp = new ArrayList<String>();
    }
    
    public ContainerPickResults(List<String> message) {
        this();
        this.message = message;
    }

    public void setNavigationBlock(String navigationBlock) {
        this.navigationBlock = navigationBlock;
    }

    public String getNavigationBlock() {
        return navigationBlock;
    }

    public void setNavigationItem(String navigationItem) {
        this.navigationItem = navigationItem;
    }

    public String getNavigationItem() {
        return navigationItem;
    }

    public void setIsTrExit(boolean isTrExit) {
        this.isTrExit = isTrExit;
    }

    public boolean getIsTrExit() {
        return isTrExit;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public List<String> getMessage() {
        return message;
    }
    
    public void setMessagePopUp(List<String> messagePopUp) {
        this.messagePopUp = messagePopUp;
    }

    public List<String> getMessagePopUp() {
        return messagePopUp;
    }
    
    public void setFunctionResult(BigDecimal functionResult) {
        this.functionResult = functionResult;
    }

    public BigDecimal getFunctionResult() {
        return functionResult;
    }
    
    public boolean isFunctionResult() {
        if (this.functionResult != null && this.functionResult.compareTo(new BigDecimal(1)) == 0)
          return true;
        else
          return false;
    }
    
    public boolean isOperationError() {
        return operationError || (message != null && !message.isEmpty() && "E".equals(message.get(0)));
    }
    
    public void setOperationError(boolean operationError){
        this.operationError = operationError;
    }
    
    public void setRaiseFailure(boolean raiseFailure) {
        this.raiseFailure = raiseFailure;
    }

    public boolean isRaiseFailure() {
        return raiseFailure;
    }
    
    public boolean containsMessage(){
        return message != null && message.size() > 1 && StringUtils.isNotEmpty(message.get(1));
    }
}
