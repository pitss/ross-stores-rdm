package com.ross.rdm.hotelpicking.model.services;

import com.ross.rdm.common.hhmenuss.model.views.GetLoginDateViewImpl;
import com.ross.rdm.common.model.services.RdmCommonAppModuleImpl;
import com.ross.rdm.common.model.views.DmsScreenOptionViewImpl;
import com.ross.rdm.common.model.views.DmsScreenOptionViewRowImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.model.views.TaskQueueViewImpl;
import com.ross.rdm.common.model.views.UserMessageViewImpl;
import com.ross.rdm.common.utils.model.base.RDMApplicationModuleImpl;
import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSBulkPickViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSBulkPickViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSLabelChildViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSLpnViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSReasonViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSToLocationViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWeightViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLaborProdViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLaborProdViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLocalViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkLocalViewRowImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkViewImpl;
import com.ross.rdm.hotelpicking.hhbulkpkacrosswvs.model.views.HhBulkPkAcrossWvSWorkViewRowImpl;
import com.ross.rdm.hotelpicking.hhcontainerinquirys.model.views.ChildContainerItemImpl;
import com.ross.rdm.hotelpicking.hhcontainerinquirys.model.views.ContainerInquiryImpl;
import com.ross.rdm.hotelpicking.hhcontainerinquirys.model.views.ContainerItemImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSContainerIdViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSContainerPickTwoViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSContainerPickViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSFromMasterChildViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSLpn2ViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSLpnViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSPickDirectiveViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSReasonViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSStartPickViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSToLocationViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSWorkLaborProdViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSWorkLocalViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSWorkLocalViewRowImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSWorkViewImpl;
import com.ross.rdm.hotelpicking.hhcontainerpicks.model.views.HhContainerPickSWorkViewRowImpl;
import com.ross.rdm.hotelpicking.hhmoveinventorys.model.views.HhMoveInventorySContainerItemSQLViewImpl;
import com.ross.rdm.hotelpicking.hhmoveinventorys.model.views.HhMoveInventorySContainerItemViewImpl;
import com.ross.rdm.hotelpicking.hhmoveinventorys.model.views.HhMoveInventorySMoveBlockViewImpl;
import com.ross.rdm.hotelpicking.hhmoveinventorys.model.views.HhMoveInventorySMoveBlockViewRowImpl;
import com.ross.rdm.hotelpicking.hhmoveinventorys.model.views.HhMoveInventorySWaveInformationViewImpl;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSNoPickPackViewImpl;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSWorkLaborProdViewImpl;
import com.ross.rdm.hotelpicking.hhnopickpacks.model.views.HhNoPickPackSWorkViewImpl;
import com.ross.rdm.hotelpicking.hhpickacrosswaves.model.views.HhPickAcrossWaveSNoPickPackViewImpl;
import com.ross.rdm.hotelpicking.hhpickacrosswaves.model.views.HhPickAcrossWaveSWorkViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainIntrlvgViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMainViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySMultiSkuViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySNewFplViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySReasonViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLaborProdNewViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLaborProdViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkLocalViewImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.HhPutawayInventorySWorkViewImpl;
import com.ross.rdm.hotelpicking.model.services.common.HotelPickingAppModule;
import com.ross.rdm.hotelpicking.rossrfauths.model.views.RossRfAuthSWorkViewImpl;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.ViewLinkImpl;
import oracle.jbo.server.ViewObjectImpl;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Sep 07 13:26:46 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HotelPickingAppModuleImpl extends RDMApplicationModuleImpl implements HotelPickingAppModule {


    /**
     * This is the default constructor (do not remove).
     */
    public HotelPickingAppModuleImpl() {
    }


    /**
     * Container's getter for HhMoveInventorySWorkLaborProdView.
     * @return HhMoveInventorySWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getHhMoveInventorySWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhMoveInventorySWorkLaborProdView");
    }

    /**
     * Container's getter for HhMoveInventorySMoveBlockView.
     * @return HhMoveInventorySMoveBlockView
     */
    public HhMoveInventorySMoveBlockViewImpl getHhMoveInventorySMoveBlockView() {
        return (HhMoveInventorySMoveBlockViewImpl) findViewObject("HhMoveInventorySMoveBlockView");
    }

    /**
     * Container's getter for HhMoveInventorySContainerItemView.
     * @return HhMoveInventorySContainerItemView
     */
    public ViewObjectImpl getHhMoveInventorySContainerItemView() {
        return (ViewObjectImpl) findViewObject("HhMoveInventorySContainerItemView");
    }

    /**
     * Container's getter for HhMoveInventorySWaveInformationView.
     * @return HhMoveInventorySWaveInformationView
     */
    public ViewObjectImpl getHhMoveInventorySWaveInformationView() {
        return (ViewObjectImpl) findViewObject("HhMoveInventorySWaveInformationView");
    }

    /**
     * Container's getter for HhMoveInventorySWorkView.
     * @return HhMoveInventorySWorkView
     */
    public RDMTransientViewObjectImpl getHhMoveInventorySWorkView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhMoveInventorySWorkView");
    }

    /**
     * Container's getter for HhMoveInventorySWorkLocalView.
     * @return HhMoveInventorySWorkLocalView
     */
    public RDMTransientViewObjectImpl getHhMoveInventorySWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhMoveInventorySWorkLocalView");
    }


    /**
     * Container's getter for HhNoPickPackSWorkLaborProdView.
     * @return HhNoPickPackSWorkLaborProdView
     */
    public HhNoPickPackSWorkLaborProdViewImpl getHhNoPickPackSWorkLaborProdView() {
        return (HhNoPickPackSWorkLaborProdViewImpl) findViewObject("HhNoPickPackSWorkLaborProdView");
    }


    /**
     * Container's getter for HhNoPickPackSWorkView.
     * @return HhNoPickPackSWorkView
     */
    public HhNoPickPackSWorkViewImpl getHhNoPickPackSWorkView() {
        return (HhNoPickPackSWorkViewImpl) findViewObject("HhNoPickPackSWorkView");
    }

    /**
     * Container's getter for HhNoPickPackSNoPickPackView.
     * @return HhNoPickPackSNoPickPackView
     */
    public HhNoPickPackSNoPickPackViewImpl getHhNoPickPackSNoPickPackView() {
        return (HhNoPickPackSNoPickPackViewImpl) findViewObject("HhNoPickPackSNoPickPackView");
    }

    /**
     * Container's getter for HhPickAcrossWaveSWorkLaborProdView.
     * @return HhPickAcrossWaveSWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getHhPickAcrossWaveSWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhPickAcrossWaveSWorkLaborProdView");
    }


    /**
     * Container's getter for HhPickAcrossWaveSWorkView.
     * @return HhPickAcrossWaveSWorkView
     */
    public HhPickAcrossWaveSWorkViewImpl getHhPickAcrossWaveSWorkView() {
        return (HhPickAcrossWaveSWorkViewImpl) findViewObject("HhPickAcrossWaveSWorkView");
    }

    /**
     * Container's getter for HhPickAcrossWaveSNoPickPackView.
     * @return HhPickAcrossWaveSNoPickPackView
     */
    public HhPickAcrossWaveSNoPickPackViewImpl getHhPickAcrossWaveSNoPickPackView() {
        return (HhPickAcrossWaveSNoPickPackViewImpl) findViewObject("HhPickAcrossWaveSNoPickPackView");
    }

    /**
     * Container's getter for HhPutawayInventorySMainIntrlvgView.
     * @return HhPutawayInventorySMainIntrlvgView
     */
    public HhPutawayInventorySMainIntrlvgViewImpl getHhPutawayInventorySMainIntrlvgView() {
        return (HhPutawayInventorySMainIntrlvgViewImpl) findViewObject("HhPutawayInventorySMainIntrlvgView");
    }

    /**
     * Container's getter for HhPutawayInventorySMainView.
     * @return HhPutawayInventorySMainView
     */
    public HhPutawayInventorySMainViewImpl getHhPutawayInventorySMainView() {
        return (HhPutawayInventorySMainViewImpl) findViewObject("HhPutawayInventorySMainView");
    }

    /**
     * Container's getter for HhPutawayInventorySReasonView.
     * @return HhPutawayInventorySReasonView
     */
    public HhPutawayInventorySReasonViewImpl getHhPutawayInventorySReasonView() {
        return (HhPutawayInventorySReasonViewImpl) findViewObject("HhPutawayInventorySReasonView");
    }


    /**
     * Container's getter for HhPutawayInventorySMultiSkuView.
     * @return HhPutawayInventorySMultiSkuView
     */
    public HhPutawayInventorySMultiSkuViewImpl getHhPutawayInventorySMultiSkuView() {
        return (HhPutawayInventorySMultiSkuViewImpl) findViewObject("HhPutawayInventorySMultiSkuView");
    }

    /**
     * Container's getter for HhPutawayInventorySNewFplView.
     * @return HhPutawayInventorySNewFplView
     */
    public HhPutawayInventorySNewFplViewImpl getHhPutawayInventorySNewFplView() {
        return (HhPutawayInventorySNewFplViewImpl) findViewObject("HhPutawayInventorySNewFplView");
    }

    /**
     * Container's getter for HhPutawayInventorySWorkView.
     * @return HhPutawayInventorySWorkView
     */
    public HhPutawayInventorySWorkViewImpl getHhPutawayInventorySWorkView() {
        return (HhPutawayInventorySWorkViewImpl) findViewObject("HhPutawayInventorySWorkView");
    }

    /**
     * Container's getter for HhPutawayInventorySWorkLaborProdView.
     * @return HhPutawayInventorySWorkLaborProdView
     */
    public HhPutawayInventorySWorkLaborProdViewImpl getHhPutawayInventorySWorkLaborProdView() {
        return (HhPutawayInventorySWorkLaborProdViewImpl) findViewObject("HhPutawayInventorySWorkLaborProdView");
    }

    /**
     * Container's getter for HhPutawayInventorySWorkLocalView.
     * @return HhPutawayInventorySWorkLocalView
     */
    public HhPutawayInventorySWorkLocalViewImpl getHhPutawayInventorySWorkLocalView() {
        return (HhPutawayInventorySWorkLocalViewImpl) findViewObject("HhPutawayInventorySWorkLocalView");
    }


    /**
     * Container's getter for RossRfAuthSWorkView.
     * @return RossRfAuthSWorkView
     */
    public RossRfAuthSWorkViewImpl getRossRfAuthSWorkView() {
        return (RossRfAuthSWorkViewImpl) findViewObject("RossRfAuthSWorkView");
    }


    /**
     * Helper Method to Simplify Invoking Stored Procedures.
     * @param stmt
     * @param bindVars
     */
    public void callStoredProcedure(String stmt, Object... bindVars) {
        DBUtils.callStoredProcedure(getDBTransaction(), stmt, bindVars);
    }

    /**
     * Helper Method to Simplify Invoking Stored Functions.
     * @param sqlReturnType
     * @param stmt
     * @param bindVars
     * @return
     */
    public Object callStoredFunction(int sqlReturnType, String stmt, Object... bindVars) {
        return DBUtils.callStoredFunction(getDBTransaction(), sqlReturnType, stmt, bindVars);
    }

    public String getUserMessagesByCode(String msgCode, String facilityId, String langCode) {
        UserMessageViewImpl userMessageView = this.getUserMessageView();
        userMessageView.setbindCodeType(msgCode);
        userMessageView.setbindFacilityId(facilityId);
        userMessageView.setbindLangCode(langCode);
        userMessageView.executeQuery();
        Row rowViewKey = this.getUserMessageView().first();
        if (rowViewKey == null) {
            return "???" + msgCode + "???";
        } else {
            return (String) rowViewKey.getAttribute("MessageText");
        }
    }

    public void setGlobalVariables() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }

        if (gVViewImplRowImpl.getGlobalHeader() == null)
            gVViewImplRowImpl.setGlobalHeader("UNDEFINED");

        if (gVViewImplRowImpl.getGlobalFacilityId() == null)
            gVViewImplRowImpl.setGlobalFacilityId(facilityId);

        if (gVViewImplRowImpl.getGlobalUserPrivilege() == null)
            gVViewImplRowImpl.setGlobalUserPrivilege("1");

        if (gVViewImplRowImpl.getGlobalDevice() == null)
            gVViewImplRowImpl.setGlobalDevice("S");

        if (gVViewImplRowImpl.getGlobalLanguageCode() == null)
            gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalUserName((String) LoginUserRow.getAttribute("UserName"));
        gVViewImplRowImpl.setGlobalUserId((String) LoginUserRow.getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");
        if (gVViewImplRowImpl.getGlobalAllowIntrlvg() == null)
            gVViewImplRowImpl.setGlobalAllowIntrlvg("N");
        if (gVViewImplRowImpl.getGlobalNoTaskFlag() == null)
            gVViewImplRowImpl.setGlobalNoTaskFlag("N");
        if (gVViewImplRowImpl.getGlobalNoPickFlag() == null)
            gVViewImplRowImpl.setGlobalNoPickFlag("N");
        gVViewImplRowImpl.setGlobalCallingForm(null);
        gVViewImplRowImpl.setGlobalNoPutFlag("N");
        gVViewImplRowImpl.setGlobalProcessByAisle("N");
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
    }
 
    //GlobalVariables for No PickPack
    public void setGlobalVariablesforNoPickPack() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        if (gVViewImplRowImpl.getGlobalHeader() == null)
            gVViewImplRowImpl.setGlobalHeader("UNDEFINED");

        if (gVViewImplRowImpl.getGlobalFacilityId() == null)
            gVViewImplRowImpl.setGlobalFacilityId(facilityId);

        if (gVViewImplRowImpl.getGlobalUserPrivilege() == null)
            gVViewImplRowImpl.setGlobalUserPrivilege("1");

        if (gVViewImplRowImpl.getGlobalDevice() == null)
            gVViewImplRowImpl.setGlobalDevice("S");

        if (gVViewImplRowImpl.getGlobalLanguageCode() == null)
            gVViewImplRowImpl.setGlobalLanguageCode(languageCode);


        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");
        Row row = this.getGetLoginDateView().getCurrentRow();
        gVViewImplRowImpl.setGlobalUserName((String) row.getAttribute("UserName"));
        gVViewImplRowImpl.setGlobalUserId((String) row.getAttribute("UserId"));
        if (gVViewImplRowImpl.getGlobalAllowIntrlvg() == null)
            gVViewImplRowImpl.setGlobalAllowIntrlvg("N");
        if (gVViewImplRowImpl.getGlobalNoTaskFlag() == null)
            gVViewImplRowImpl.setGlobalNoTaskFlag("N");
        if (gVViewImplRowImpl.getGlobalNoPickFlag() == null)
            gVViewImplRowImpl.setGlobalNoPickFlag("N");
        gVViewImplRowImpl.setGlobalStartPickLoca(null);
        gVViewImplRowImpl.setGlobalNoPutFlag("N");
        gVViewImplRowImpl.setGlobalCPickType(null);
        gVViewImplRowImpl.setGlobalCPPickType(null);
        gVViewImplRowImpl.setGlobalBPPickType(null);
        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
    }

    public void setGlobalVariablesPickAcrossWave() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }

        gVViewImplRowImpl.setGlobalUserId((String) (this.getGetLoginDateView().getCurrentRow()).getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
    }


    public void setGlobalVariablesContainerInquiry() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        gVViewImplRowImpl.setGlobalUserId((String) LoginUserRow.getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);

        this.getContainer().clearCache();
        this.getContainer().executeEmptyRowSet();
    }

    public void setGlobalVariablesChangePassword() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        gVViewImplRowImpl.setGlobalUserId((String) LoginUserRow.getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);

    }

    public void setGlobalVariablesRossRfAuth() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        gVViewImplRowImpl.setGlobalUserId((String) LoginUserRow.getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);

    }

    public Row setGlobalVariablesMoveInventory() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        variablesRow.setGlobalUserId((String) LoginUserRow.getAttribute("UserId"));
        variablesRow.setGlobalHeader("UNDEFINED");
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("R");
        variablesRow.setGlobalLanguageCode(languageCode);

        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        variablesRow.setGlobalCallingForm(null);

        gVViewImpl.insertRow(variablesRow);
        gVViewImpl.setCurrentRow(variablesRow);

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);


        return variablesRow;
    }

    public Row setGlobalVariablesContainerPickS() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        variablesRow.setGlobalUserId((String) this.getGetLoginDateView().getCurrentRow().getAttribute("UserId"));
        variablesRow.setGlobalHeader("UNDEFINED");
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("S");
        variablesRow.setGlobalLanguageCode(languageCode);
        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        //variablesRow.setGlobalCallingForm(null);
        gVViewImpl.insertRow(variablesRow);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        return variablesRow;
    }
 

    public List exitMoveInventory(String moveContainerId) {
        List<String> errors = null;

        HhMoveInventorySMoveBlockViewRowImpl currentMoveInventoryRow =
            (HhMoveInventorySMoveBlockViewRowImpl) this.getHhMoveInventorySMoveBlockView().getCurrentRow();

        if (currentMoveInventoryRow.getContainerId() != null &&
            "Y".equals(currentMoveInventoryRow.getDropoffLocWcs())) {
            SQLOutParam o_location_id = new SQLOutParam("", Types.VARCHAR);
            DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "smart_move.fn_send_message",
                                       currentMoveInventoryRow.getFacilityId(), currentMoveInventoryRow.getWhId(),
                                       currentMoveInventoryRow.getCidRefNum(), null, 0, 6, "C", o_location_id);

            currentMoveInventoryRow.setWcsToLocation((String) o_location_id.getWrappedData());
        }

        SQLOutParam p_v_return = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_msg_display = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_activityCode =
            new SQLOutParam(currentMoveInventoryRow.getActivityCode() == null ? "" :
                            currentMoveInventoryRow.getActivityCode(), Types.VARCHAR);
        SQLOutParam p_containerProcessed =
            new SQLOutParam(currentMoveInventoryRow.getContainersProcessed() == null ? new BigDecimal(0) :
                            currentMoveInventoryRow.getContainersProcessed(), Types.NUMERIC);
        SQLOutParam p_operationPreformed =
            new SQLOutParam(currentMoveInventoryRow.getOperationsPerformed() == null ? new BigDecimal(0) :
                            currentMoveInventoryRow.getOperationsPerformed(), Types.NUMERIC);
        SQLOutParam p_referenceCode = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_startTime = new SQLOutParam(new java.sql.Date(new java.util.Date().getTime()), Types.DATE);
        SQLOutParam p_unitsProcessed = new SQLOutParam(new BigDecimal(0), Types.NUMERIC);

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.TR_PRE_EXIT", p_v_return,
                                    p_msg_display, currentMoveInventoryRow.getContainerId(),
                                    currentMoveInventoryRow.getOldLocation(), currentMoveInventoryRow.getFacilityId(),
                                    p_activityCode, p_containerProcessed, p_operationPreformed, p_referenceCode,
                                    p_startTime, p_unitsProcessed, currentMoveInventoryRow.getUserId());

        if (p_v_return.getWrappedData() != null && !"RAISE_FAILURE####".equals(p_v_return.getWrappedData())) {
            errors = new ArrayList<String>();
            // p_msg_display -> type
            // p_v_return -> msg
            if (((String) p_v_return.getWrappedData()).contains("RAISE_FAILURE####")) {
                errors.add("E");
                String error = (String) p_v_return.getWrappedData();
                error = error.replace("RAISE_FAILURE####", "");
                errors.add(error);
            } else {
                errors.add((String) p_msg_display.getWrappedData());
                errors.add((String) p_v_return.getWrappedData());
            }
        }

        if (p_v_return.getWrappedData() == null ||
            !((String) p_v_return.getWrappedData()).contains("RAISE_FAILURE####")) {
            DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.TR_EXIT",
                                        currentMoveInventoryRow.getContainerId(),
                                        currentMoveInventoryRow.getFacilityId(), moveContainerId);

            currentMoveInventoryRow.remove();
        }

        return errors;
    }

    public List callDoneMoveInventory() {
        List<String> errors = null;

        HhMoveInventorySMoveBlockViewRowImpl currentMoveInventoryRow =
            (HhMoveInventorySMoveBlockViewRowImpl) this.getHhMoveInventorySMoveBlockView().getCurrentRow();

        if (currentMoveInventoryRow != null) {
            TaskQueueViewImpl vo = this.getTaskQueueView();
            vo.setbind_facilityId(currentMoveInventoryRow.getFacilityId());
            vo.setbind_containerId(currentMoveInventoryRow.getContainerId());
            vo.setbind_activityCode("BUFMVE");
            vo.executeQuery();

            if (vo.getEstimatedRowCount() > 0) {
                errors = this.getHhMoveInventorySMoveBlockView().processBufferMove();
            } else {
                if ("N".equals(currentMoveInventoryRow.getToLocStorageFlag())) {
                    if ("Y".equals(currentMoveInventoryRow.getFromLocStorageFlag())) {
                        String result =
                            this.callCheckContainerTrouble(currentMoveInventoryRow.getContainerId(),
                                                           currentMoveInventoryRow.getFacilityId(),
                                                           "HH_MOVE_INVENTORY_S");
                        if ("Y".equals(result)) {
                            String errorMsg =
                                this.getUserMessagesByCode("INV_CONT_STATUS", currentMoveInventoryRow.getFacilityId(),
                                                           currentMoveInventoryRow.getLanguageCode());
                            errors = new ArrayList<String>();
                            errors.add("W");
                            errors.add(errorMsg);
                        } else {
                            errors =
                                this.callCheckPendingTasks(currentMoveInventoryRow.getContainerId(),
                                                           currentMoveInventoryRow.getFacilityId());
                        }
                    } else {
                        errors =
                            this.callCheckPendingTasks(currentMoveInventoryRow.getContainerId(),
                                                       currentMoveInventoryRow.getFacilityId());
                    }

                    if (errors == null) {
                        errors = this.getHhMoveInventorySMoveBlockView().processMove();
                    }
                } else {
                    errors = this.getHhMoveInventorySMoveBlockView().processMove();
                }
            }

            if (errors == null && "Y".equals(currentMoveInventoryRow.getDropoffLocWcs())) {
                currentMoveInventoryRow.setDropoffLocWcs("N");
            }
        }

        return errors;
    }

    public List callCheckPendingTasks(String containerId, String facilityId) {
        List<String> errors = null;

        SQLOutParam p_v_return = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_msg_display = new SQLOutParam("", Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.check_pending_task", p_v_return,
                                    p_msg_display, containerId, facilityId);

        if (p_v_return.getWrappedData() != null) {
            errors = new ArrayList<String>();
            errors.add((String) p_msg_display.getWrappedData());
            String error = (String) p_v_return.getWrappedData();
            errors.add(error);
            errors.add("PENDING_TASK");
        }

        return errors;
    }

    public String getWareHouseId(String picLocationID, String facilityId) {
        SQLOutParam wh_id_out = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam create_divert_flag_out = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam warehouse_found_flag = new SQLOutParam("", Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "Get_Wh_Id", facilityId, picLocationID, wh_id_out,
                                    create_divert_flag_out, warehouse_found_flag);
        if (wh_id_out.getWrappedData() != null) {
            return (String) wh_id_out.getWrappedData();
        }
        return null;
    }


    public List getWaveStatus(String waveNumber, String facilityId) {

        List<String> errors = null;
        SQLOutParam p_v_return = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_msg_display = new SQLOutParam("", Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.V_CHECK_WAVE_STATUS", p_v_return,
                                    p_msg_display, waveNumber, facilityId);

        if (p_v_return.getWrappedData() != null) {
            errors = new ArrayList<String>();
            errors.add((String) p_msg_display.getWrappedData());
            String error = (String) p_v_return.getWrappedData();
            errors.add(error);
            errors.add("WAVE_STATUS_ERROR");
        }

        return errors;

    }


    public String getGscpWh(String facilityId, String whId) {


        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.CHAR, "g_scp_wh", facilityId, whId,
                                                   "select_pick_enbld");
    }


    public String getloCScpByWh(String facilityId, String startPicLoc) {
        String returnVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "loc_scp_by_wh", facilityId,
                                                startPicLoc, "interleaving_enabled");
        return returnVal;
    }

    public String gWaveType(String facilityId, String waveNumber) {
        String returnVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "g_wave_type", facilityId,
                                                waveNumber);
        return returnVal;
    }


    public boolean checkUserPrivilige(String user, String facilityId, String screenName) {
        return DBUtils.callStoredFunction(this.getDBTransaction(), Types.INTEGER, "PKG_COMMON_ADF.CK_USER_PRIV_WRP",
                                          user, facilityId, screenName) == 1 ? true : false;
    }


    public List initMoveInventory(String moveContainerId) {
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) this.setGlobalVariablesMoveInventory();

        String header =
            this.callDisplayHeader(variablesRow.getGlobalFacilityId(), variablesRow.getGlobalLanguageCode(),
                                   "HH_MOVE_INVENTORY_S");

        List errorsMessages =
            this.getHhMoveInventorySMoveBlockView().initMoveInventoryData(moveContainerId, variablesRow, header);

        this.callSetAhlInfo(variablesRow.getGlobalFacilityId(), "HH_MOVE_INVENTORY_S");

        return errorsMessages;
    }

    public boolean containsSeveralMessages(String string) {
        return StringUtils.isNotEmpty(string) && string.contains("#") &&
               StringUtils.isNotEmpty(string.replace("#", ""));
    }

    public List<String> processMessagesParams(SQLOutParam p_v_return, SQLOutParam p_msg_display) {
        List<String> errors = null;
        // p_msg_display -> type
        // p_v_return -> msg
        if (p_v_return.getWrappedData() != null && !"RAISE_FAILURE####".equals(p_v_return.getWrappedData())) {
            errors = new ArrayList<String>();
            if (this.containsSeveralMessages((String) p_v_return.getWrappedData()) &&
                this.containsSeveralMessages((String) p_msg_display.getWrappedData())) {
                // IN THIS CASE WE HAVE SEVERAL MESSAGES TO SHOW
                String p_types = (String) p_msg_display.getWrappedData();
                String[] types = p_types.split("[#]");
                String p_msgs = (String) p_v_return.getWrappedData();
                String[] messages = p_msgs.split("[#]");
                String message = "";
                for (int i = 0; i < messages.length; i++) {
                    message += messages[i];
                    if (i > 0) {
                        message += ". ";
                    }
                }
                if (types != null) {
                    errors.add(types[0]);
                } else {
                    errors.add("E");
                }
                errors.add(message);
            } else {
                errors.add((String) p_msg_display.getWrappedData());
                if (((String) p_v_return.getWrappedData()).contains("RAISE_FAILURE####")) {
                    // errors.add("E");
                    String error = (String) p_v_return.getWrappedData();
                    error = error.replace("RAISE_FAILURE####", "");
                    errors.add(error);
                } else {
                    errors.add((String) p_v_return.getWrappedData());
                }
            }
        }
        return errors;
    }

    private void initContainerPickSVOInstances() {
        this.getHhContainerPickSWorkView().initHhContainerPickSWorkViewRow();
        this.getHhContainerPickSWorkLocalView().initHhContainerPickSWorkLocalViewRow();
        this.getHhContainerPickSWorkLaborProdView().initHhContainerPickSWorkLaborProdRow();
        if (this.getHhContainerPickSStartPickView().getCurrentRow() == null) {
            this.getHhContainerPickSStartPickView().insertRow(this.getHhContainerPickSStartPickView().createRow());
        } else {
            this.getHhContainerPickSStartPickView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSContainerPickView().getCurrentRow() == null) {
            this.getHhContainerPickSContainerPickView().insertRow(this.getHhContainerPickSContainerPickView().createRow());
        } else {
            this.getHhContainerPickSContainerPickView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSContainerPickTwoView().getCurrentRow() == null) {
            this.getHhContainerPickSContainerPickTwoView().insertRow(this.getHhContainerPickSContainerPickTwoView().createRow());
        } else {
            this.getHhContainerPickSContainerPickTwoView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSFromMasterChildView().getCurrentRow() == null) {
            this.getHhContainerPickSFromMasterChildView().insertRow(this.getHhContainerPickSFromMasterChildView().createRow());
        } else {
            this.getHhContainerPickSFromMasterChildView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSFromMasterChild2View().getCurrentRow() == null) {
            this.getHhContainerPickSFromMasterChild2View().insertRow(this.getHhContainerPickSFromMasterChild2View().createRow());
        } else {
            this.getHhContainerPickSFromMasterChild2View().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSItemDescView().getCurrentRow() == null) {
            this.getHhContainerPickSItemDescView().insertRow(this.getHhContainerPickSItemDescView().createRow());
        } else {
            this.getHhContainerPickSItemDescView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSReasonView().getCurrentRow() == null) {
            this.getHhContainerPickSReasonView().insertRow(this.getHhContainerPickSReasonView().createRow());
        } else {
            this.getHhContainerPickSReasonView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSToLocationView().getCurrentRow() == null) {
            this.getHhContainerPickSToLocationView().insertRow(this.getHhContainerPickSToLocationView().createRow());
        } else {
            this.getHhContainerPickSToLocationView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSToMasterChildView().getCurrentRow() == null) {
            this.getHhContainerPickSToMasterChildView().insertRow(this.getHhContainerPickSToMasterChildView().createRow());
        } else {
            this.getHhContainerPickSToMasterChildView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
        if (this.getHhContainerPickSWeightView().getCurrentRow() == null) {
            this.getHhContainerPickSWeightView().insertRow(this.getHhContainerPickSWeightView().createRow());
        } else {
            this.getHhContainerPickSWeightView().getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
        }
    }

    public List initContainerPickS(String pVersion, String pmTaskQueueInd, String pmRowid, BigDecimal pmWaveNbr,
                                   String pmPickType, String pmStartLocation, String pmCallingForm,
                                   java.sql.Date pmWaveDate, String pmUpsCode) {
        this.initContainerPickSVOInstances();

        List<String> errorsMessages = null;
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) this.setGlobalVariablesContainerPickS();
        String header = null;
        /* if ("1".equals(pVersion)) {
            header =
                this.callDisplayHeader(variablesRow.getGlobalFacilityId(), variablesRow.getGlobalLanguageCode(),
                                       "HH_CONTAINER_PICK_S");
        } else {
            header =
                this.callDisplayHeader(variablesRow.getGlobalFacilityId(), variablesRow.getGlobalLanguageCode(),
                                       "HH_CONT_PK_ACROSS_WV_S");
        } */
        // Issue 1441
        header =
            this.callDisplayHeader(variablesRow.getGlobalFacilityId(), variablesRow.getGlobalLanguageCode(),
                                   "HH_CONTAINER_PICK_S");

        HhContainerPickSWorkLocalViewRowImpl containerPicksWorkLocalRow =
            (HhContainerPickSWorkLocalViewRowImpl) this.getHhContainerPickSWorkLocalView().getCurrentRow();
        HhContainerPickSWorkViewRowImpl containerPicksWorkRow =
            (HhContainerPickSWorkViewRowImpl) this.getHhContainerPickSWorkView().getCurrentRow();

        // INIT TASK-FLOW PARAMS INTO THE VO !!
        containerPicksWorkLocalRow.setPmVersion(pVersion);
        containerPicksWorkLocalRow.setPmTaskQueueInd(pmTaskQueueInd);
        containerPicksWorkLocalRow.setPmRowid(pmRowid);
        containerPicksWorkLocalRow.setPmWaveNbr(pmWaveNbr);
        containerPicksWorkLocalRow.setPmPickType(pmPickType);
        containerPicksWorkLocalRow.setPmStartLocation(pmStartLocation);
        containerPicksWorkLocalRow.setPmCallingForm(pmCallingForm);
        containerPicksWorkLocalRow.setPmWaveDate(pmWaveDate);
        containerPicksWorkLocalRow.setPmUpsCode(pmUpsCode);

        //The parameter pmCallingForm comes null from the menu
        variablesRow.setGlobalCallingForm(pmCallingForm);

        containerPicksWorkRow.setMainBlock("START_PICK");

        String distMethod = this.gScp(variablesRow.getGlobalFacilityId(), "dist_method");
        containerPicksWorkLocalRow.setDistMethod(distMethod);

        containerPicksWorkRow.setHeader(header);
        containerPicksWorkRow.setFacilityId(variablesRow.getGlobalFacilityId());
        containerPicksWorkRow.setLanguageCode(variablesRow.getGlobalLanguageCode());
        containerPicksWorkRow.setUser(variablesRow.getGlobalUserId());

        String t_location = this.gScp(variablesRow.getGlobalFacilityId(), "in_transit_loc");
        if (t_location != null) {
            t_location = t_location.trim();
        }
        if (!"ERROR".equals(t_location)) {
            containerPicksWorkLocalRow.setInTransitLocation(t_location);
        } else {
            SQLOutParam p_v_return = new SQLOutParam(null, Types.VARCHAR);
            SQLOutParam i_msg_display = new SQLOutParam(null, Types.VARCHAR);
            DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_COMMON_ADF.CALL_LOG_ERROR_GUI", 
                                                p_v_return, i_msg_display, variablesRow.getGlobalFacilityId(),
                                                variablesRow.getGlobalUserId(), 999, "PKG_CONTAINER_PICKING_ADF", 
                                                "Startup_Local", "Invalid SCP Parameter: in_transit_loc");
            
            p_v_return = new SQLOutParam(null, Types.VARCHAR);
            i_msg_display = new SQLOutParam(null, Types.VARCHAR);
            SQLOutParam p_clear_block = new SQLOutParam(null, Types.VARCHAR);
            DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_CONTAINER_PICKING_ADF.TR_EXIT", p_v_return,
                                        i_msg_display, p_clear_block, null, null, null,
                                        variablesRow.getGlobalFacilityId(), variablesRow.getGlobalUserId(),
                                        pmTaskQueueInd, null, null, null, null, null);
            errorsMessages = this.processMessagesParams(p_v_return, i_msg_display);
            if (errorsMessages == null) {
                errorsMessages = new ArrayList<String>();
                // errorsMessages.add("initError");
                errorsMessages.add("E");
                String message =
                    this.getUserMessagesByCode("DMS_ERROR", variablesRow.getGlobalFacilityId(),
                                               variablesRow.getGlobalLanguageCode());
                errorsMessages.add(message);
            }
        }
        String clearUser = null;
        if (errorsMessages == null) {
            containerPicksWorkRow.setVersionNumber("%I%");
            String labeledReserve = this.gScp(variablesRow.getGlobalFacilityId(), "labeled_reserve");
            containerPicksWorkLocalRow.setLabeledReserve(labeledReserve);
            String labeledPicking = this.gScp(variablesRow.getGlobalFacilityId(), "labeled_picking");
            containerPicksWorkLocalRow.setLabeledPicking(labeledPicking);
            String mixedDestId = this.gScp(variablesRow.getGlobalFacilityId(), "mixed_dest_id");
            if (mixedDestId != null) {
                containerPicksWorkLocalRow.setMixedDestId(new BigDecimal(mixedDestId));
            }
            clearUser = this.gScp(variablesRow.getGlobalFacilityId(), "clear_user");
            containerPicksWorkLocalRow.setClearUser(clearUser);
            String mldEnabled = this.gScp(variablesRow.getGlobalFacilityId(), "mld_enabled");
            containerPicksWorkLocalRow.setMldEnabled(mldEnabled);

            String callingForm = variablesRow.getGlobalCallingForm();
            if ("1".equals(pVersion)) {
                // default_value(NULL, 'GLOBAL.Calling_Form');
                //variablesRow.setGlobalCallingForm(null);
                //DEFAULT_VALUE only assigns if that global has not been initialized!
                // So we do not need that here, as it only assigns NULL

                if ("".equals(callingForm) || callingForm == null)
                    containerPicksWorkLocalRow.setLabeledPicking("Y");

                // CR 6.4 Ross Stores -> (@ANGEL: THIS LOGIC HAS NO SENSE...)
                if ("hh_no_pick_pack_s".equalsIgnoreCase(callingForm) &&
                    "Y".equals(containerPicksWorkLocalRow.getLabeledPicking())) {
                    errorsMessages = new ArrayList<String>();
                    errorsMessages.add("E");
                    String message =
                        this.getUserMessagesByCode("WRONG_PICK_TYPE", variablesRow.getGlobalFacilityId(),
                                                   variablesRow.getGlobalLanguageCode());
                    errorsMessages.add(message);

                    return errorsMessages;
                }

                //variablesRow.setGlobalCallingForm(null);
                // If using the task queue dialogue the default to none labeled picking
                if ("Y".equals(pmTaskQueueInd)) {
                    containerPicksWorkLocalRow.setLabeledPicking("N");
                }
            }

            if ("N".equals(containerPicksWorkLocalRow.getLabeledPicking()) /* no, wrong pick type! || "hh_no_pick_pack_s".equalsIgnoreCase(callingForm) */) {
                errorsMessages = new ArrayList<String>();
                errorsMessages.add("ContainerPick");
                if ("1".equals(pVersion)) {
                    this.getWaveView().ensureVariableManager().setVariableValue("bind_facilityId",
                                                                                variablesRow.getGlobalFacilityId());
                    this.getWaveView().ensureVariableManager().setVariableValue("bind_waveNbr",
                                                                                variablesRow.getGlobalWaveNbr());
                    this.getWaveView().applyViewCriteria(this.getWaveView().getViewCriteria("WaveByFacilityAndWaveNbrVC"));
                    this.getWaveView().executeQuery();
                    Row firstRow = this.getWaveView().first();
                    if (firstRow != null &&
                        (!"PRINTED".equals(firstRow.getAttribute("WaveStatus")) &&
                         !"OPEN".equals(firstRow.getAttribute("WaveStatus")))) {
                        errorsMessages = new ArrayList<String>();
                        errorsMessages.add("E");
                        String errorMsg =
                            this.getUserMessagesByCode("NOT_PRINTED", variablesRow.getGlobalFacilityId(),
                                                       variablesRow.getGlobalLanguageCode());
                        errorsMessages.add(errorMsg);
                        return errorsMessages;
                    }
                }
                
                if (errorsMessages.size() == 1) {
                    this.getLocationView().ensureVariableManager().setVariableValue("bind_facilityId",
                                                                                    variablesRow.getGlobalFacilityId());
                    this.getLocationView().ensureVariableManager().setVariableValue("bind_startPickLoc",
                                                                                    variablesRow.getGlobalStartPickLoca());
                    this.getLocationView().executeQuery();
                    Row firstRow = this.getLocationView().first();
                    String startZone = (String) firstRow.getAttribute("Zone");
                    containerPicksWorkLocalRow.setStartZone(startZone);

                    String result =
                        (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                            "ALLOW_PROCESS_BY_AISLE",
                                                            variablesRow.getGlobalFacilityId(),
                                                            variablesRow.getGlobalStartPickLoca());
                    String processByAisle = "N";
                    if ("Y".equals(result)) {
                        processByAisle =
                            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "LOC_SCP_BY_WH",
                                                                variablesRow.getGlobalFacilityId(),
                                                                variablesRow.getGlobalStartPickLoca(),
                                                                "process_by_aisle");


                    }
                    containerPicksWorkLocalRow.setProcessByAisle(processByAisle);

                    if ("1".equals(pVersion)) {
                        // NVL(:PARAMETER.PM_Task_Queue_Ind,'N') = 'N'
                        if (pmTaskQueueInd == null) {
                            if (variablesRow.getGlobalWaveNbr() != null) {
                                containerPicksWorkLocalRow.setPmWaveNbr(new BigDecimal(variablesRow.getGlobalWaveNbr()));
                            }
                            containerPicksWorkLocalRow.setPmPickType(variablesRow.getGlobalNoPickPackPickType());
                            containerPicksWorkLocalRow.setPickType(variablesRow.getGlobalNoPickPackPickType()); //TODO: Check this assignment
                            containerPicksWorkLocalRow.setPmStartLocation(variablesRow.getGlobalStartPickLoca());
                            containerPicksWorkLocalRow.setPmCallingForm(variablesRow.getGlobalCallingForm());
                        }
                    } else {
                        containerPicksWorkLocalRow.setPmPickType(variablesRow.getGlobalNoPickPackPickType());
                        containerPicksWorkLocalRow.setPickType(variablesRow.getGlobalNoPickPackPickType()); //TODO: Check this assignment
                        containerPicksWorkLocalRow.setPmStartLocation(variablesRow.getGlobalStartPickLoca());
                        containerPicksWorkLocalRow.setPmUpsCode(variablesRow.getGlobalUnitPickSystemCode());
                        containerPicksWorkLocalRow.setPmWaveDate(variablesRow.getGlobalWaveDate());
                    }
                }
            } else {
                if ("1".equals(pVersion))
                    containerPicksWorkLocalRow.setProcessByAisle("N");

                errorsMessages = new ArrayList<String>();
                errorsMessages.add("Page1");
            }
        }

        if (errorsMessages == null || errorsMessages.size() == 1) {
            DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.UNLOCK_PICKS",
                                        variablesRow.getGlobalFacilityId(), clearUser, variablesRow.getGlobalUserId());
            if ("1".equals(pVersion))
                this.callSetAhlInfo(variablesRow.getGlobalFacilityId(), "HH_CONTAINER_PICK_S");
            else
                this.callSetAhlInfo(variablesRow.getGlobalFacilityId(), "HH_CONT_PK_ACROSS_WV_S");

        }
        return errorsMessages;
    }

    public String callCheckContainerTrouble(String containerId, String facilityId, String formName) {
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "CHECK_CONTAINER_TROUBLE",
                                                   containerId, facilityId, formName);
    }

    public List<String> callCheckMaxUsers() {
        SQLOutParam outErrorCode = new SQLOutParam("");
        List<String> listReturnedValues = null;
        String returnVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_COMMON_ADF.CHECK_MAX_USERS", outErrorCode);
        if ("N".equals(returnVal)) {
            listReturnedValues = new ArrayList<String>();
            listReturnedValues.add(returnVal);
            listReturnedValues.add((String) outErrorCode.getWrappedData());
        }
        return listReturnedValues;
    }

    public String callDisplayHeader(String facilityId, String langCode, String formName) {
        String header =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "PKG_COMMON_ADF.DISPLAY_HEADER",
                                                facilityId, langCode, formName);
        if (header != null) {
            header = header.replace(facilityId, "").trim();
        }
        return header;
    }

    public void callSetAhlInfo(String facilityId, String formName) {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "AHL.SET_AHL_INFO", facilityId, formName);
    }

    public void callInitializeFacilitySetup(String facilityId) {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.INITIALIZE", facilityId);
    }

    public List callStartUpLocalSecurity(String facilityId, String formName, String userId) {
        List<String> disabledItems = new ArrayList<String>();

        DmsScreenOptionViewImpl vo = this.getDmsScreenOptionView();
        vo.setbind_facilityId(facilityId);
        vo.setbind_screenName(formName);
        vo.executeQuery();

        RowSetIterator rsi = vo.createRowSetIterator(null);
        DmsScreenOptionViewRowImpl row = null;
        String result = null;
        while (rsi.hasNext()) {
            row = (DmsScreenOptionViewRowImpl) rsi.next();
            result = this.callCheckScreenOptionPriv(facilityId, userId, formName, row.getOptionName());

            if ("OFF".equals(result)) {
                disabledItems.add(row.getOptionName());
            }
        }
        return disabledItems;
    }

    public String callCheckScreenOptionPriv(String facilityId, String userId, String formName, String optionName) {
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                   "PKG_COMMON_ADF.CHECK_SCREEN_OPTION_PRIV", facilityId, userId,
                                                   formName, optionName);
    }

    /**
     * Container's getter for GlobalVariablesView1.
     * @return GlobalVariablesView1
     */
    public GlobalVariablesViewImpl getGlobalVariablesView() {
        return  this.getRdmCommonAppModule().getGlobalVariablesView();
    }

    /**
     * Container's getter for UserMessageView1.
     * @return UserMessageView1
     */
    public UserMessageViewImpl getUserMessageView() {
        return  this.getRdmCommonAppModule().getUserMessageView();
    }

    /**
     * Container's getter for GetLoginDateView1.
     * @return GetLoginDateView1
     */
    public GetLoginDateViewImpl getGetLoginDateView() {
        return this.getRdmCommonAppModule().getGetLoginDateView();
    }


    /**
     * Container's getter for DmsScreenOptionView1.
     * @return DmsScreenOptionView1
     */
    public DmsScreenOptionViewImpl getDmsScreenOptionView() {
        return (DmsScreenOptionViewImpl) findViewObject("DmsScreenOptionView");
    }


    /**
     * Container's getter for TaskQueueView1.
     * @return TaskQueueView1
     */
    public TaskQueueViewImpl getTaskQueueView() {
        return (TaskQueueViewImpl) findViewObject("TaskQueueView");
    }


    /**
     * Database part of CHECK_MAX_USERS code from the original Forms application
     *
     * @return configured maximum number of logged-in users
     */
    public int getMaxUserCount() {
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String encryptVal = gScp(facilityId, "ulc");
        if (!encryptVal.substring(0, 3).equals("Z4n"))
            return -2; // the data is corrupted
        return 100 * convertEncryptedNum(encryptVal.charAt(3)) + 10 * convertEncryptedNum(encryptVal.charAt(6)) +
               convertEncryptedNum(encryptVal.charAt(8));
    }

    /**
     * Checks for the disabled access
     */
    public boolean checkLoginDate(String facilityId, String userId) {
        int loginDisabled = Integer.valueOf(gScp(facilityId, "login_disabled"));
        Date disabledAccess = offsetCurrentDate(loginDisabled);

        Date loginDate = getGetLoginDateView().getLoginDate(facilityId, userId);
        if (null == loginDate)
            return false;
        return !disabledAccess.after(loginDate);
    }

    /**
     * Check if user should change password
     */
    public int shouldChangePassword(String facilityId) {
        Date logDate = getGetLoginDateView().getCurrentLogDate();
        if (null == logDate) {
            return -1;
        }

        int intPasswordOld = Integer.parseInt(gScp(facilityId, "password_old"));
        Date passwordOld = offsetCurrentDate(intPasswordOld);

        int intPasswordExpire = Integer.parseInt(gScp(facilityId, "password_expire"));
        Date passwordExpire = offsetCurrentDate(intPasswordExpire);

        if (passwordExpire.after(logDate)) { // password expired
            return -2;
        } else if (passwordOld.after(logDate)) { // old password
            return -3;
        }

        return 0;
    }

    /**
     * Checks the privilige of the user and updates the login date of the user
     */
    public void checkUserPrivilege(String facilityId, String screenName, String userId) {
        GetLoginDateViewImpl vo = getGetLoginDateView();
        vo.setBindUserId(userId);
        vo.executeQuery();
        Row loginRow = vo.first();
        if (loginRow != null) {
            Row row = vo.first();
            int userPrivilege = (Integer) row.getAttribute("UserPrivilege");

            if (9 == userPrivilege) {
                logAuditTrail(facilityId, screenName, userId);
            }
            getGetLoginDateView().getCurrentRow().setAttribute("LoginDate", new java.sql.Date((new Date()).getTime()));
            getTransaction().commit();
        }
    }

    // Begin BULK PICKING CHANGES


    public String getBulkPickAcrossWvFacilityId() {
        HhBulkPkAcrossWvSWorkViewRowImpl workRow =
            (HhBulkPkAcrossWvSWorkViewRowImpl) this.getHhBulkPkAcrossWvSWorkView().getCurrentRow();
        return workRow.getFacilityId();
    }


    /**
     * getPickDirective
     * @param pmWaveDate
     * @param pmPickType
     * @param pmStartLocation
     * @param pmUpsCode
     * @param pmUpsGroupInd
     * @return
     */
    public String getPickDirective(String pmPickType, String pmStartLocation) {
        HhBulkPkAcrossWvSBulkPickViewRowImpl row =
            (HhBulkPkAcrossWvSBulkPickViewRowImpl) this.getHhBulkPkAcrossWvSBulkPickView().getCurrentRow();
        HhBulkPkAcrossWvSWorkViewRowImpl workRow =
            (HhBulkPkAcrossWvSWorkViewRowImpl) this.getHhBulkPkAcrossWvSWorkView().getCurrentRow();
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocalRow =
            (HhBulkPkAcrossWvSWorkLocalViewRowImpl) this.getHhBulkPkAcrossWvSWorkLocalView().getCurrentRow();
        HhBulkPkAcrossWvSWorkLaborProdViewRowImpl wLabProdRow =
            (HhBulkPkAcrossWvSWorkLaborProdViewRowImpl) this.getHhBulkPkAcrossWvSWorkLaborProdView().getCurrentRow();

        SQLOutParam V_RETURN = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam I_MSG_DISPLAY = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Pick_From_Container_ID = new SQLOutParam(row.getPickFromContainerId(), Types.VARCHAR);
        SQLOutParam p_pick_type = new SQLOutParam(wLocalRow.getPickType(), Types.VARCHAR);
        SQLOutParam p_first_pick_flag = new SQLOutParam(wLocalRow.getFirstPickFlag(), Types.VARCHAR);
        SQLOutParam p_pick_to_container_id = new SQLOutParam(row.getPickToContainerId(), Types.VARCHAR);
        SQLOutParam p_zone = new SQLOutParam(wLocalRow.getZone(), Types.VARCHAR);
        SQLOutParam p_container_qty = new SQLOutParam(row.getContainerQty(), Types.NUMERIC);
        SQLOutParam p_reference_code = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Container_Qty2 = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_dest_id = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_Conf_Location_ID = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Conf_Container_ID = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Empty_Pressed = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_To_Loc_Entered = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_old_to_cid = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_wave_nbr = new SQLOutParam(wLocalRow.getWaveNbr(), Types.NUMERIC);
        SQLOutParam p_Bulk_Group = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_original_pick_qty = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Distro_Nbr = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Pick_Container_Qty = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_Description = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_From_Location_Id = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_itemId = new SQLOutParam("", Types.VARCHAR);

        callStoredProcedure("PKG_PICKING.get_pick_directive", V_RETURN, I_MSG_DISPLAY, p_Conf_Container_ID,
                            p_Conf_Location_ID, p_Pick_Container_Qty, p_Description, p_From_Location_Id, p_itemId,
                            p_Pick_From_Container_ID, p_pick_to_container_id, p_reference_code, p_Bulk_Group,
                            p_Container_Qty2, p_dest_id, p_Distro_Nbr, p_Empty_Pressed, p_first_pick_flag, p_old_to_cid,
                            p_original_pick_qty, p_container_qty, p_pick_type, p_To_Loc_Entered, p_wave_nbr, p_zone,
                            workRow.getFacilityId(), workRow.getUser(), wLocalRow.getDistroTs(),
                            wLocalRow.getLabeledReserve(), wLocalRow.getProcessByAisle(), pmPickType, pmStartLocation);

        if (p_Pick_From_Container_ID != null)
            row.setPickFromContainerId((String) p_Pick_From_Container_ID.getWrappedData());

        if (p_pick_type != null)
            wLocalRow.setPickType((String) p_pick_type.getWrappedData());


        if (p_first_pick_flag != null)
            wLocalRow.setFirstPickFlag((String) p_first_pick_flag.getWrappedData());

        if (p_pick_to_container_id != null) {
            wLocalRow.setPickToContainerId((String) p_pick_to_container_id.getWrappedData());
            row.setPickToContainerId((String) p_pick_to_container_id.getWrappedData());
        }

        if (p_zone != null)
            wLocalRow.setZone((String) p_zone.getWrappedData());

        if (p_container_qty.getWrappedData() != null)
            row.setContainerQty((p_container_qty.getWrappedData()).toString());

        if (p_reference_code != null)
            wLabProdRow.setReferenceCode((String) p_reference_code.getWrappedData());

        if (p_Container_Qty2 != null)
            wLocalRow.setContainerQty2((BigDecimal) p_Container_Qty2.getWrappedData());

        if (p_dest_id != null)
            wLocalRow.setDestId((BigDecimal) p_dest_id.getWrappedData());
        if (p_Conf_Location_ID != null)
            row.setConfLocationId((String) p_Conf_Location_ID.getWrappedData());

        if (p_Conf_Container_ID != null)
            row.setConfContainerId((String) p_Conf_Container_ID.getWrappedData());

        if (p_Empty_Pressed != null)
            wLocalRow.setEmptyPressed((String) p_Empty_Pressed.getWrappedData());

        if (p_To_Loc_Entered != null)
            wLocalRow.setToLocEntered((String) p_To_Loc_Entered.getWrappedData());

        if (p_old_to_cid != null)
            wLocalRow.setOldToCid((String) p_old_to_cid.getWrappedData());

        if (p_wave_nbr != null)
            wLocalRow.setWaveNbr((BigDecimal) p_wave_nbr.getWrappedData());

        if (p_Bulk_Group != null)
            wLocalRow.setBulkGroup((String) p_Bulk_Group.getWrappedData());

        if (p_original_pick_qty != null)
            wLocalRow.setOriginalPickQty((BigDecimal) p_original_pick_qty.getWrappedData());

        if (p_Distro_Nbr != null)
            wLocalRow.setDistroNbr((String) p_Distro_Nbr.getWrappedData());

        if (p_Pick_Container_Qty != null)
            wLocalRow.setPickContainerQty((BigDecimal) p_Pick_Container_Qty.getWrappedData());

        if (p_Description != null)
            row.setDescription((String) p_Description.getWrappedData());

        if (p_From_Location_Id != null)
            row.setFromLocationId((String) p_From_Location_Id.getWrappedData());


        String errorMessage = null;
        if (I_MSG_DISPLAY != null) {
            errorMessage = (String) I_MSG_DISPLAY.getWrappedData();
        }
        return errorMessage;
    }

    /**
     * getGenericPickDirective
     * @param pmWaveDate
     * @param pmPickType
     * @param pmStartLocation
     * @param pmUpsCode
     * @param pmUpsGroupInd
     * @return
     */
    public Map getGenericPickDirective(BigDecimal pVersion, BigDecimal pmWaveNbr, Date pmWaveDate, String pmPickType,
                                       String pmStartLocation, String pmUpsCode, String pmUpsGroupInd,
                                       String pmTaskQueueInd, String pmRowid, String pmAllowIntrlvg,
                                       String pmNoPickFlag, String pmNoTaskFlag, String pmPickTypeC,
                                       String pmPickTypeCP) {
        Map returnValues = new HashMap();
        BigDecimal ONE = new BigDecimal(1);
        HhBulkPkAcrossWvSBulkPickViewRowImpl row =
            (HhBulkPkAcrossWvSBulkPickViewRowImpl) this.getHhBulkPkAcrossWvSBulkPickView().getCurrentRow();
        HhBulkPkAcrossWvSWorkViewRowImpl workRow =
            (HhBulkPkAcrossWvSWorkViewRowImpl) this.getHhBulkPkAcrossWvSWorkView().getCurrentRow();
        HhBulkPkAcrossWvSWorkLocalViewRowImpl wLocalRow =
            (HhBulkPkAcrossWvSWorkLocalViewRowImpl) this.getHhBulkPkAcrossWvSWorkLocalView().getCurrentRow();
        HhBulkPkAcrossWvSWorkLaborProdViewRowImpl wLabProdRow =
            (HhBulkPkAcrossWvSWorkLaborProdViewRowImpl) this.getHhBulkPkAcrossWvSWorkLaborProdView().getCurrentRow();

        SQLOutParam p_Pick_From_Container_ID = new SQLOutParam(row.getPickFromContainerId(), Types.VARCHAR);
        SQLOutParam p_Pick_From_Container_ID2 = new SQLOutParam(wLocalRow.getPickFromContainerId(), Types.VARCHAR);
        SQLOutParam p_PM_Ups_Group_Ind = new SQLOutParam(pmUpsGroupInd, Types.VARCHAR);
        SQLOutParam p_pick_type = new SQLOutParam(wLocalRow.getPickType(), Types.VARCHAR);
        SQLOutParam p_Generic_To_Container = new SQLOutParam(row.getGenericToContainer(), Types.VARCHAR);
        SQLOutParam p_first_pick_flag = new SQLOutParam(wLocalRow.getFirstPickFlag(), Types.VARCHAR);
        SQLOutParam V_RETURN = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam I_MSG_DISPLAY = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_pick_to_container_id = new SQLOutParam(row.getPickToContainerId(), Types.VARCHAR);
        SQLOutParam p_pick_to_container_id2 = new SQLOutParam(wLocalRow.getPickToContainerId(), Types.VARCHAR);
        SQLOutParam p_zone = new SQLOutParam(wLocalRow.getZone(), Types.VARCHAR);
        SQLOutParam p_container_qty = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_reference_code = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Container_Qty2 = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_Conf_Location_ID = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Conf_Container_ID = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Empty_Pressed = new SQLOutParam(wLocalRow.getEmptyPressed(), Types.VARCHAR);
        SQLOutParam p_To_Loc_Entered = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_old_to_cid = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_wave_nbr = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_Bulk_Group = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_original_pick_qty = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_Distro_Nbr = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Pick_Container_Qty = new SQLOutParam(0, Types.NUMERIC);
        SQLOutParam p_Description = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_From_Location_Id = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_goto_field = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_pm_no_pick_flag = new SQLOutParam(pmNoPickFlag, Types.VARCHAR);
        SQLOutParam p_pm_no_task_flag = new SQLOutParam(pmNoTaskFlag, Types.VARCHAR);
        SQLOutParam P_Global_Intrlvg = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Item_Id = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam p_Dest_id = new SQLOutParam(0, Types.NUMERIC);

        if (ONE.equals(pVersion)) { //new version

            callStoredProcedure("PKG_PICKING.get_generic_pick_directive", workRow.getFacilityId(), pmWaveDate,
                                wLocalRow.getProcessByAisle(), pmPickTypeC, pmPickTypeCP, pmPickType, pmStartLocation,
                                pmUpsCode, wLocalRow.getLabeledPicking(), workRow.getUser(), pmAllowIntrlvg,
                                wLabProdRow.getOperationsPerformed(), wLabProdRow.getStartTime(),
                                wLabProdRow.getUnitsProcessed(), wLabProdRow.getContainersProcessed(),
                                p_Pick_From_Container_ID, p_Pick_From_Container_ID2, p_PM_Ups_Group_Ind, p_pick_type,
                                p_Generic_To_Container, p_first_pick_flag, V_RETURN, I_MSG_DISPLAY,
                                p_pick_to_container_id, p_pick_to_container_id2, p_zone, p_container_qty,
                                p_reference_code, p_Container_Qty2, p_Conf_Location_ID, p_Conf_Container_ID,
                                p_Empty_Pressed, p_To_Loc_Entered, p_old_to_cid, p_wave_nbr, p_Bulk_Group,
                                p_original_pick_qty, p_Distro_Nbr, p_Pick_Container_Qty, p_Description,
                                p_From_Location_Id, p_goto_field, p_Item_Id, p_Dest_id, p_pm_no_pick_flag,
                                p_pm_no_task_flag, P_Global_Intrlvg);


        } else { 
            if (pmStartLocation == null) pmStartLocation = " ";
            DBUtils.callStoredProcedureWithNulls(getDBTransaction(),"PKG_PICKING.get_generic_pick_directive2", workRow.getFacilityId(), pmTaskQueueInd,
                                pmWaveNbr, pmRowid, wLocalRow.getDistroTs(), wLocalRow.getProcessByAisle(), pmPickTypeC,
                                pmPickTypeCP, pmPickType, pmStartLocation, wLocalRow.getLabeledPicking(),
                                workRow.getUser(), pmAllowIntrlvg, wLabProdRow.getOperationsPerformed(),
                                wLabProdRow.getStartTime(), wLabProdRow.getUnitsProcessed(),
                                wLabProdRow.getContainersProcessed(), p_Pick_From_Container_ID,
                                p_Pick_From_Container_ID2, p_PM_Ups_Group_Ind, p_pick_type, p_Generic_To_Container,
                                p_first_pick_flag, V_RETURN, I_MSG_DISPLAY, p_pick_to_container_id,
                                p_pick_to_container_id2, p_zone, p_container_qty, p_reference_code, p_Container_Qty2,
                                p_Conf_Location_ID, p_Conf_Container_ID, p_Empty_Pressed, p_To_Loc_Entered,
                                p_old_to_cid, p_wave_nbr, p_Bulk_Group, p_original_pick_qty, p_Distro_Nbr,
                                p_Pick_Container_Qty, p_Description, p_From_Location_Id, p_goto_field, p_Item_Id,
                                p_Dest_id, p_pm_no_pick_flag, p_pm_no_task_flag, P_Global_Intrlvg); 
        }

        if (p_Item_Id != null)
            row.setItemId((String) p_Item_Id.getWrappedData());

        if (p_Dest_id != null)
            wLocalRow.setDestId((BigDecimal) p_Dest_id.getWrappedData());

        if (p_Pick_From_Container_ID != null)
            row.setPickFromContainerId((String) p_Pick_From_Container_ID.getWrappedData());

        if (p_Pick_From_Container_ID2 != null)
            wLocalRow.setPickFromContainerId((String) p_Pick_From_Container_ID2.getWrappedData());

        if (p_pick_type != null)
            wLocalRow.setPickType((String) p_pick_type.getWrappedData());

        if (p_Generic_To_Container != null)
            row.setGenericToContainer((String) p_Generic_To_Container.getWrappedData());

        if (p_first_pick_flag != null)
            wLocalRow.setFirstPickFlag((String) p_first_pick_flag.getWrappedData());

        if (p_pick_to_container_id.getWrappedData() != null) {
            row.setPickToContainerId((String) p_pick_to_container_id.getWrappedData());
        }

        if (p_pick_to_container_id2.getWrappedData() != null) {
            wLocalRow.setPickToContainerId((String) p_pick_to_container_id2.getWrappedData());
        }

        if (p_zone != null)
            wLocalRow.setZone((String) p_zone.getWrappedData());

        if (p_container_qty.getWrappedData() != null)
            row.setContainerQty((p_container_qty.getWrappedData()).toString());

        if (p_reference_code != null)
            wLabProdRow.setReferenceCode((String) p_reference_code.getWrappedData());

        if (p_Container_Qty2.getWrappedData() != null)
            wLocalRow.setContainerQty2((BigDecimal) p_Container_Qty2.getWrappedData());

        if (p_Conf_Location_ID != null)
            row.setConfLocationId((String) p_Conf_Location_ID.getWrappedData());

        if (p_Conf_Container_ID != null)
            row.setConfContainerId((String) p_Conf_Container_ID.getWrappedData());

        if (p_Empty_Pressed.getWrappedData() != null)
            wLocalRow.setEmptyPressed((String) p_Empty_Pressed.getWrappedData());

        if (p_To_Loc_Entered != null)
            wLocalRow.setToLocEntered((String) p_To_Loc_Entered.getWrappedData());

        if (p_old_to_cid != null)
            wLocalRow.setOldToCid((String) p_old_to_cid.getWrappedData());

        if (p_wave_nbr != null)
            wLocalRow.setWaveNbr((BigDecimal) p_wave_nbr.getWrappedData());

        if (p_Bulk_Group != null)
            wLocalRow.setBulkGroup((String) p_Bulk_Group.getWrappedData());

        if (p_original_pick_qty.getWrappedData() != null)
            wLocalRow.setOriginalPickQty((BigDecimal) p_original_pick_qty.getWrappedData());

        if (p_Distro_Nbr != null)
            wLocalRow.setDistroNbr((String) p_Distro_Nbr.getWrappedData());

        if (p_Pick_Container_Qty.getWrappedData() != null)
            wLocalRow.setPickContainerQty((BigDecimal) p_Pick_Container_Qty.getWrappedData());

        if (p_Description != null)
            row.setDescription((String) p_Description.getWrappedData());

        if (p_From_Location_Id != null)
            row.setFromLocationId((String) p_From_Location_Id.getWrappedData());

        if (p_goto_field != null) {
            workRow.setGotoField((String) p_goto_field.getWrappedData());
        }

        //Parameter no pick flag to be set, no task flag needs to be set
        List<String> errors = processMessagesParams(V_RETURN, I_MSG_DISPLAY);

        returnValues.put("ErrorMessage", errors);
        returnValues.put("NoPickFlag", p_pm_no_pick_flag.getWrappedData());
        returnValues.put("NoTaskFlag", p_pm_no_task_flag.getWrappedData());
        returnValues.put("GlobalIntrlvg", P_Global_Intrlvg.getWrappedData());

        //Cleanup, needed for return from putaway
        try {
            if (getHhBulkPkAcrossWvSToLocationView().getCurrentRow() != null) {
                getHhBulkPkAcrossWvSToLocationView().removeCurrentRow();
            }

        } catch (Exception e) {

        }

        return returnValues;
    }


    public String allowProcessByAisle(String facilityId, String startPickLoc) {
        return (String) callStoredFunction(Types.VARCHAR, "allow_process_by_aisle", facilityId, startPickLoc);
    }

    /**
     * @param facilityId
     * @param startPickLoc
     * @param scpName
     * @return Y/N
     */
    public String locScpByWarehouse(String facilityId, String startPickLoc, String scpName) {
        return (String) callStoredFunction(Types.VARCHAR, "loc_scp_by_wh", facilityId, startPickLoc, scpName);
    }


    public void setGlobalVariablesBulkPick(String pVersion) {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        Row LoginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) LoginUserRow.getAttribute("FacilityId");
        String languageCode = (String) LoginUserRow.getAttribute("LanguageCode");
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(variablesRow);
        } 
        variablesRow.setGlobalUserId((String) (this.getGetLoginDateView().getCurrentRow()).getAttribute("UserId"));
        if (variablesRow.getGlobalHeader() == null)
            variablesRow.setGlobalHeader("UNDEFINED");

        if (variablesRow.getGlobalFacilityId() == null)
            variablesRow.setGlobalFacilityId(facilityId);

        if (variablesRow.getGlobalUserPrivilege() == null)
            variablesRow.setGlobalUserPrivilege("1");

        if (variablesRow.getGlobalDevice() == null)
            variablesRow.setGlobalDevice("S");

        if (variablesRow.getGlobalLanguageCode() == null)
            variablesRow.setGlobalLanguageCode(languageCode);

        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");

        //From program unit Startup_Local in form
        if (variablesRow.getGlobalAllowIntrlvg() == null)
            variablesRow.setGlobalAllowIntrlvg("N");
        if (variablesRow.getGlobalNoTaskFlag() == null)
            variablesRow.setGlobalNoTaskFlag("N");
        if (variablesRow.getGlobalNoPickFlag() == null)
            variablesRow.setGlobalNoPickFlag("N");
        //       variablesRow.setGlobalStartPickLoca("RL04C021"); //tesst
        //       variablesRow.setGlobalNoPickPackPickType("BP");
        //        variablesRow.setGlobalno(null);
        //DEFAULT_VALUE(NULL, 'GLOBAL.no_pick_pack_pick_type'); missing variable in global


        callStoredProcedure("PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);

    }
    // END BULK PICKING Changes;


    /**
     * Java wrapper for the database stored function 'g_scp'
     */
    public String gScp(String facility, String name) {
        return (String) callStoredFunction(Types.VARCHAR, "g_scp", facility, name);
    }

    /**
     * Java wrapper for the database stored function 'log_audit_trail'
     */
    private void logAuditTrail(String facilityId, String screenName, String userId) {
        callStoredProcedure("log_audit_trail", facilityId, screenName, userId, null, null, null, null);
    }

    /**
     * Equivalent of PL/SQL call:
     * TRANSLATE(SUBSTR(encrypt_val,4,1), 'ABCDEFGHIJ', '0123456789')
     */
    private int convertEncryptedNum(char alpha) {
        return (int) Character.toUpperCase(alpha) - (int) 'A';
    }

    /**
     * Subtracts given number of days from the current date
     */
    private Date offsetCurrentDate(int numDays) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -numDays);
        return cal.getTime();
    }

    /*
     * doCommit
     *
     */
    public void callDoCommit() {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_COMMON_ADF.doCommit");
    }

    public void callDoRollback() {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_COMMON_ADF.doRollback");
    }


    /**
     * Container's getter for HhContainerPickSStartPickView.
     * @return HhContainerPickSStartPickView
     */
    public HhContainerPickSStartPickViewImpl getHhContainerPickSStartPickView() {
        return (HhContainerPickSStartPickViewImpl) findViewObject("HhContainerPickSStartPickView");
    }

    /**
     * Container's getter for HhContainerPickSContainerPickView.
     * @return HhContainerPickSContainerPickView
     */
    public RDMTransientViewObjectImpl getHhContainerPickSContainerPickView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhContainerPickSContainerPickView");
    }

    /**
     * Container's getter for HhContainerPickSContainerPickTwoView.
     * @return HhContainerPickSContainerPickTwoView
     */
    public HhContainerPickSContainerPickTwoViewImpl getHhContainerPickSContainerPickTwoView() {
        return (HhContainerPickSContainerPickTwoViewImpl) findViewObject("HhContainerPickSContainerPickTwoView");
    }

    /**
     * Container's getter for HhContainerPickSToLocationView.
     * @return HhContainerPickSToLocationView
     */
    public HhContainerPickSToLocationViewImpl getHhContainerPickSToLocationView() {
        return (HhContainerPickSToLocationViewImpl) findViewObject("HhContainerPickSToLocationView");
    }

    /**
     * Container's getter for HhContainerPickSFromMasterChildView.
     * @return HhContainerPickSFromMasterChildView
     */
    public RDMViewObjectImpl getHhContainerPickSFromMasterChildView() {
        return (RDMViewObjectImpl) findViewObject("HhContainerPickSFromMasterChildView");
    }

    /**
     * Container's getter for HhContainerPickSFromMasterChild2View.
     * @return HhContainerPickSFromMasterChild2View
     */
    public RDMTransientViewObjectImpl getHhContainerPickSFromMasterChild2View() {
        return (RDMTransientViewObjectImpl) findViewObject("HhContainerPickSFromMasterChild2View");
    }

    /**
     * Container's getter for HhContainerPickSToMasterChildView.
     * @return HhContainerPickSToMasterChildView
     */
    public RDMTransientViewObjectImpl getHhContainerPickSToMasterChildView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhContainerPickSToMasterChildView");
    }

    /**
     * Container's getter for HhContainerPickSWeightView.
     * @return HhContainerPickSWeightView
     */
    public RDMTransientViewObjectImpl getHhContainerPickSWeightView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhContainerPickSWeightView");
    }

    /**
     * Container's getter for HhContainerPickSLpnView.
     * @return HhContainerPickSLpnView
     */
    public HhContainerPickSLpnViewImpl getHhContainerPickSLpnView() {
        return (HhContainerPickSLpnViewImpl) findViewObject("HhContainerPickSLpnView");
    }

    /**
     * Container's getter for HhContainerPickSItemDescView.
     * @return HhContainerPickSItemDescView
     */
    public RDMTransientViewObjectImpl getHhContainerPickSItemDescView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhContainerPickSItemDescView");
    }

    /**
     * Container's getter for HhContainerPickSWorkLocalView.
     * @return HhContainerPickSWorkLocalView
     */
    public HhContainerPickSWorkLocalViewImpl getHhContainerPickSWorkLocalView() {
        return (HhContainerPickSWorkLocalViewImpl) findViewObject("HhContainerPickSWorkLocalView");
    }

    /**
     * Container's getter for HhContainerPickSWorkView.
     * @return HhContainerPickSWorkView
     */
    public HhContainerPickSWorkViewImpl getHhContainerPickSWorkView() {
        return (HhContainerPickSWorkViewImpl) findViewObject("HhContainerPickSWorkView");
    }

    /**
     * Container's getter for HhContainerPickSWorkLaborProdView.
     * @return HhContainerPickSWorkLaborProdView
     */
    public HhContainerPickSWorkLaborProdViewImpl getHhContainerPickSWorkLaborProdView() {
        return (HhContainerPickSWorkLaborProdViewImpl) findViewObject("HhContainerPickSWorkLaborProdView");
    }

    /**
     * Container's getter for HhContainerPickSReasonView.
     * @return HhContainerPickSReasonView
     */
    public HhContainerPickSReasonViewImpl getHhContainerPickSReasonView() {
        return (HhContainerPickSReasonViewImpl) findViewObject("HhContainerPickSReasonView");
    }

    /**
     * Container's getter for HhContainerPickSReasonCodeView.
     * @return HhContainerPickSReasonCodeView
     */
    public RDMTransientViewObjectImpl getHhContainerPickSReasonCodeView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhContainerPickSReasonCodeView");
    }


    /**
     * Container's getter for HhBulkPkAcrossWvSReasonView.
     * @return HhBulkPkAcrossWvSReasonView
     */
    public HhBulkPkAcrossWvSReasonViewImpl getHhBulkPkAcrossWvSReasonView() {
        return (HhBulkPkAcrossWvSReasonViewImpl) findViewObject("HhBulkPkAcrossWvSReasonView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSReasonCodeView.
     * @return HhBulkPkAcrossWvSReasonCodeView
     */
    public RDMTransientViewObjectImpl getHhBulkPkAcrossWvSReasonCodeView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhBulkPkAcrossWvSReasonCodeView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSLpnView.
     * @return HhBulkPkAcrossWvSLpnView
     */
    public HhBulkPkAcrossWvSLpnViewImpl getHhBulkPkAcrossWvSLpnView() {
        return (HhBulkPkAcrossWvSLpnViewImpl) findViewObject("HhBulkPkAcrossWvSLpnView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSLabelChildView.
     * @return HhBulkPkAcrossWvSLabelChildView
     */
    public HhBulkPkAcrossWvSLabelChildViewImpl getHhBulkPkAcrossWvSLabelChildView() {
        return (HhBulkPkAcrossWvSLabelChildViewImpl) findViewObject("HhBulkPkAcrossWvSLabelChildView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSWeightView.
     * @return HhBulkPkAcrossWvSWeightView
     */
    public HhBulkPkAcrossWvSWeightViewImpl getHhBulkPkAcrossWvSWeightView() {
        return (HhBulkPkAcrossWvSWeightViewImpl) findViewObject("HhBulkPkAcrossWvSWeightView");
    }


    /**
     * Container's getter for HhBulkPkAcrossWvSBulkPickView.
     * @return HhBulkPkAcrossWvSBulkPickView
     */
    public HhBulkPkAcrossWvSBulkPickViewImpl getHhBulkPkAcrossWvSBulkPickView() {
        return (HhBulkPkAcrossWvSBulkPickViewImpl) findViewObject("HhBulkPkAcrossWvSBulkPickView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSToLocationView.
     * @return HhBulkPkAcrossWvSToLocationView
     */
    public HhBulkPkAcrossWvSToLocationViewImpl getHhBulkPkAcrossWvSToLocationView() {
        return (HhBulkPkAcrossWvSToLocationViewImpl) findViewObject("HhBulkPkAcrossWvSToLocationView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSContainerView.
     * @return HhBulkPkAcrossWvSContainerView
     */
    public RDMTransientViewObjectImpl getHhBulkPkAcrossWvSContainerView() {
        return (RDMTransientViewObjectImpl) findViewObject("HhBulkPkAcrossWvSContainerView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSWorkView.
     * @return HhBulkPkAcrossWvSWorkView
     */
    public HhBulkPkAcrossWvSWorkViewImpl getHhBulkPkAcrossWvSWorkView() {
        return (HhBulkPkAcrossWvSWorkViewImpl) findViewObject("HhBulkPkAcrossWvSWorkView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSWorkLocalView.
     * @return HhBulkPkAcrossWvSWorkLocalView
     */
    public HhBulkPkAcrossWvSWorkLocalViewImpl getHhBulkPkAcrossWvSWorkLocalView() {
        return (HhBulkPkAcrossWvSWorkLocalViewImpl) findViewObject("HhBulkPkAcrossWvSWorkLocalView");
    }

    /**
     * Container's getter for HhBulkPkAcrossWvSWorkLaborProdView.
     * @return HhBulkPkAcrossWvSWorkLaborProdView
     */
    public HhBulkPkAcrossWvSWorkLaborProdViewImpl getHhBulkPkAcrossWvSWorkLaborProdView() {
        return (HhBulkPkAcrossWvSWorkLaborProdViewImpl) findViewObject("HhBulkPkAcrossWvSWorkLaborProdView");
    }


    /**
     * Container's getter for WaveView
     * @return WaveView1
     */
    public RDMViewObjectImpl getWaveView() {
        return (RDMViewObjectImpl) findViewObject("WaveView");
    }


    /**
     * Container's getter for LocationView1.
     * @return LocationView1
     */
    public RDMViewObjectImpl getLocationView() {
        return (RDMViewObjectImpl) findViewObject("LocationView");
    }

    /**
     * Container's getter for ChildContainerView1.
     * @return ChildContainerView1
     */
    public RDMViewObjectImpl getScannedChildExists() {
        return (RDMViewObjectImpl) findViewObject("ScannedChildExists");
    }

    /**
     * Container's getter for ChildContainerView1.
     * @return ChildContainerView1
     */
    public RDMViewObjectImpl getUnScannedChildExists() {
        return (RDMViewObjectImpl) findViewObject("UnScannedChildExists");
    }

    /**
     * Container's getter for Container1.
     * @return Container1
     */
    public ContainerInquiryImpl getContainer() {
        return (ContainerInquiryImpl) findViewObject("Container");
    }

    /**
     * Container's getter for ContainerRoute1.
     * @return ContainerRoute1
     */
    public RDMViewObjectImpl getContainerRoute() {
        return (RDMViewObjectImpl) findViewObject("ContainerRoute");
    }

    /**
     * Container's getter for Container2ContainerRoute1.
     * @return Container2ContainerRoute1
     */
    public ViewLinkImpl getContainer2ContainerRoute1() {
        return (ViewLinkImpl) findViewLink("Container2ContainerRoute1");
    }


    public String getMldEnabled(String facilityId) {
        return (String) callStoredFunction(Types.VARCHAR, "g_scp", facilityId, "MLD_ENABLED");
    }

    public void clearRossEventcodeUseridTmp() {
        executeCommand("DELETE FROM ROSS_EVENTCODE_USERID_TMP");
    }

    public void updateRossEventcodeUseridTmp(String user, String locId) {
        clearRossEventcodeUseridTmp();
        executeCommand("INSERT INTO ROSS_EVENTCODE_USERID_TMP (EVENT_CD, USER_ID, LOCATION_ID) " + "VALUES (82, '" +
                       user + "', '" + locId + "')");
    }


    /**
     * Container's getter for ContainerView1.
     * @return ContainerView1
     */
    public RDMViewObjectImpl getContainerQtyROView() {
        return (RDMViewObjectImpl) findViewObject("ContainerQtyROView");
    }

    /**
     * Container's getter for CheckForChildrenView1.
     * @return CheckForChildrenView1
     */
    public RDMViewObjectImpl getCheckForChildren() {
        return (RDMViewObjectImpl) findViewObject("CheckForChildren");
    }

    /**
     * Container's getter for CountSysChildView1.
     * @return CountSysChildView1
     */
    public RDMViewObjectImpl getCountSysChild() {
        return (RDMViewObjectImpl) findViewObject("CountSysChild");
    }

    /**
     * Container's getter for HhContainerPickSPickDirectiveView.
     * @return HhContainerPickSPickDirectiveView
     */
    public HhContainerPickSPickDirectiveViewImpl getHhContainerPickSPickDirectiveView() {
        return (HhContainerPickSPickDirectiveViewImpl) findViewObject("HhContainerPickSPickDirectiveView");
    }

    /**
     * Container's getter for HhContainerPickSContainerIdView1.
     * @return HhContainerPickSContainerIdView1
     */
    public HhContainerPickSContainerIdViewImpl getHhContainerPickSContainerIdView() {
        return (HhContainerPickSContainerIdViewImpl) findViewObject("HhContainerPickSContainerIdView");
    }


    /**
     * Container's getter for RdmCommonAppModule1.
     * @return RdmCommonAppModule1
     */
    public RdmCommonAppModuleImpl getRdmCommonAppModule() {
        return (RdmCommonAppModuleImpl) findApplicationModule("RdmCommonAppModule");
    }


    /**
     * Container's getter for HhContainerPickSLpn2View1.
     * @return HhContainerPickSLpn2View1
     */
    public HhContainerPickSLpn2ViewImpl getHhContainerPickSLpn2View() {
        return (HhContainerPickSLpn2ViewImpl) findViewObject("HhContainerPickSLpn2View");
    }

    /**
     * Container's getter for HhMoveInventorySContainerItemSQLView1.
     * @return HhMoveInventorySContainerItemSQLView1
     */
    public HhMoveInventorySContainerItemSQLViewImpl getHhMoveInventorySContainerItemSQLView() {
        return (HhMoveInventorySContainerItemSQLViewImpl) findViewObject("HhMoveInventorySContainerItemSQLView");
    }

    /**
     * Container's getter for HhPutawayInventorySWorkLaborProdNewView1.
     * @return HhPutawayInventorySWorkLaborProdNewView1
     */
    public HhPutawayInventorySWorkLaborProdNewViewImpl getHhPutawayInventorySWorkLaborProdNewView() {
        return (HhPutawayInventorySWorkLaborProdNewViewImpl) findViewObject("HhPutawayInventorySWorkLaborProdNewView");
    }


    /**
     * Container's getter for ContainerItem1.
     * @return ContainerItem1
     */
    public ContainerItemImpl getContainerItem() {
        return (ContainerItemImpl) findViewObject("ContainerItem");
    }

    /**
     * Container's getter for Container2ContainerItem1.
     * @return Container2ContainerItem1
     */
    public ViewLinkImpl getContainer2ContainerItem1() {
        return (ViewLinkImpl) findViewLink("Container2ContainerItem1");
    }

    /**
     * Container's getter for ChildContainerItem1.
     * @return ChildContainerItem1
     */
    public ChildContainerItemImpl getChildContainerItem() {
        return (ChildContainerItemImpl) findViewObject("ChildContainerItem");
    }

    /**
     * Container's getter for Container2ChildContainerItem1.
     * @return Container2ChildContainerItem1
     */
    public ViewLinkImpl getContainer2ChildContainerItem1() {
        return (ViewLinkImpl) findViewLink("Container2ChildContainerItem1");
    }
}
