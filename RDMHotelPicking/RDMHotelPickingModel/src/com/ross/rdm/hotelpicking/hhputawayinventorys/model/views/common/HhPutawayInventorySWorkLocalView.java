package com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 11 13:48:12 EDT 2015
// ---------------------------------------------------------------------
public interface HhPutawayInventorySWorkLocalView extends ViewObject {
    void setHhPutawayInventorySWorkLocalVariables();
}

