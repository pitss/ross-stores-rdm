package com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Mar 16 11:26:46 EDT 2016
// ---------------------------------------------------------------------
public interface HhPutawayInventorySWorkLaborProdNewView extends ViewObject {
    void setHhPutawayInventoryWorkLaborProdVariables();
}

