package com.ross.rdm.hotelpicking.hhputawayinventorys.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.common.HhPutawayInventorySWorkLaborProdView;

import java.math.BigDecimal;

import java.util.Calendar;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mo Jul 27 22:27:47 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhPutawayInventorySWorkLaborProdViewImpl extends RDMTransientViewObjectImpl implements HhPutawayInventorySWorkLaborProdView {


    /**
     * This is the default constructor (do not remove).
     */
    public HhPutawayInventorySWorkLaborProdViewImpl() {
    }

  

    public void setHhPutawayInventoryWorkLaborProdVariables() {
         HhPutawayInventorySWorkLaborProdViewRowImpl hhPutInvWorkLaborProdVRowImpl =
            (HhPutawayInventorySWorkLaborProdViewRowImpl) this.createRow();
        hhPutInvWorkLaborProdVRowImpl.setStartTime(new java.sql.Date((Calendar.getInstance()).getTimeInMillis()));
        hhPutInvWorkLaborProdVRowImpl.setUnitsProcessed(new BigDecimal(0));
        hhPutInvWorkLaborProdVRowImpl.setContainersProcessed(new BigDecimal(0));
        hhPutInvWorkLaborProdVRowImpl.setOperationsPerformed(new BigDecimal(0));
        hhPutInvWorkLaborProdVRowImpl.setActivityCode("PUTAWAY");
        hhPutInvWorkLaborProdVRowImpl.setReferenceCode("---------");
        this.insertRow(hhPutInvWorkLaborProdVRowImpl);
        this.setCurrentRow(hhPutInvWorkLaborProdVRowImpl);


    }


}
