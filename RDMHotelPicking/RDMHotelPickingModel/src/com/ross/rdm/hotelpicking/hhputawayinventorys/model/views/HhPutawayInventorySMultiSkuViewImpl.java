package com.ross.rdm.hotelpicking.hhputawayinventorys.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.common.HhPutawayInventorySMultiSkuView;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.jbo.ApplicationModule;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mo Jul 27 22:27:46 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhPutawayInventorySMultiSkuViewImpl extends RDMTransientViewObjectImpl implements HhPutawayInventorySMultiSkuView {


    /**
     * This is the default constructor (do not remove).
     */
    public HhPutawayInventorySMultiSkuViewImpl() {
    }
 
    public List<String> callVPutawayLocation() {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            (HhPutawayInventorySMultiSkuViewRowImpl) this.getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();

        SQLOutParam P_msg_type = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_error_msg = new SQLOutParam(EMPTY_STRING);
        List<String> codesList = null;
        String hotReplenFlag = EMPTY_STRING;
        String pickType = EMPTY_STRING;
        String multiSkuLocationId = EMPTY_STRING;
        if (hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag() != null)
            hotReplenFlag = hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag();
        if (null != hhPutawayInventorySWorkLocalViewRow.getPickType())
            pickType = hhPutawayInventorySWorkLocalViewRow.getPickType();
        if (null != hhPutawayInventorySMultiSkuViewRow && null != hhPutawayInventorySMultiSkuViewRow.getLocationId())
            multiSkuLocationId = hhPutawayInventorySMultiSkuViewRow.getLocationId();
        String returnCode =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_HOTEL_PICKING_ADF.V_PUTAWAY_LOCATION", P_error_msg, P_msg_type,
                                                hhPutawayInventorySMainViewRow.getContainerId(),
                                                hhPutawayInventorySMainViewRow.getLocationId(),
                                                hhPutawayInventorySMainViewRow.getSuggestedLocId(), multiSkuLocationId,
                                                hhPutInvWorkVRowImpl.getFacilityId(), hotReplenFlag, pickType,
                                                "HH_PUTAWAY_INVENTORY_S", "MULTI_SKU",
                                                hhPutawayInventorySMultiSkuViewRow.getConfirmLoc());
        if ("N".equalsIgnoreCase(returnCode)) {
            codesList = new ArrayList<String>();
            codesList.add((String) P_msg_type.getWrappedData());
            codesList.add((String) P_error_msg.getWrappedData());
            return codesList;
        } else {
            codesList =
                this.callCheckForPutException(hhPutawayInventorySWorkLocalViewRow.getProcessByAisle(),
                                              hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                              hhPutawayInventorySMainViewRow.getToLocationId(),
                                              hhPutInvWorkVRowImpl.getFacilityId());
        }
        return codesList;
    }

    public List<String> callCheckForPutException(String wrokLocalProcessByAisle, String mainSuggestedLocId,
                                                 String mainToLocationId, String workFacilityId) {

        SQLOutParam P_msg_type = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_error_msg = new SQLOutParam(EMPTY_STRING);
        SQLOutParam work_local_calling_item = new SQLOutParam(EMPTY_STRING);
        SQLOutParam go_block = new SQLOutParam(EMPTY_STRING);
        SQLOutParam work_local_reason_code = new SQLOutParam(EMPTY_STRING);
        List<String> codesList = null;
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.CHECK_FOR_PUT_EXCEPTION",
                                    wrokLocalProcessByAisle, mainSuggestedLocId, mainToLocationId, workFacilityId,
                                    work_local_calling_item, go_block, work_local_reason_code, P_msg_type, P_error_msg);
        if ("REASON".equalsIgnoreCase((String) go_block.getWrappedData())) {
            HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
                (HhPutawayInventorySWorkLocalViewRowImpl) this.getApplicationModule().findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
            hhPutawayInventorySWorkLocalViewRow.setCallingItem("ENTER");
            codesList = new ArrayList<String>();
            codesList.add((String) go_block.getWrappedData());
        } else {
            if (null != P_error_msg) {
                codesList = new ArrayList<String>();
                codesList.add((String) P_msg_type.getWrappedData());
                codesList.add((String) P_error_msg.getWrappedData());
            }
        }
        return codesList;
    }

    public List<String> vPutQty() {
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            (HhPutawayInventorySMultiSkuViewRowImpl) this.getCurrentRow();
        List<String> codesList = null;
        codesList = new ArrayList<String>();
        if (hhPutawayInventorySMultiSkuViewRow.getConfirmQty() == null) {
            codesList.add("N");
            codesList.add("W");
            codesList.add("PARTIAL_ENTRY");
            return codesList;
        } else {
            if (!hhPutawayInventorySMultiSkuViewRow.getConfirmQty().equals(hhPutawayInventorySMultiSkuViewRow.getLastConfirmQty())) {
                hhPutawayInventorySMultiSkuViewRow.setLastConfirmQty(hhPutawayInventorySMultiSkuViewRow.getConfirmQty());
                codesList.add("N");
                codesList.add("W");
                codesList.add("QTY_MISMATCH");
                return codesList;
            }
        }
        codesList.add("Y");
        return codesList;
    }

    public List<String> callvContainerId() {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            (HhPutawayInventorySMultiSkuViewRowImpl) this.getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        SQLOutParam msgType = new SQLOutParam(EMPTY_STRING);
        SQLOutParam msgError = new SQLOutParam(EMPTY_STRING);
        String returnVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_HOTEL_PICKING_ADF.V_CONTAINER_ID1", msgError, msgType,
                                                hhPutawayInventorySMainViewRow.getContainerId(),
                                                hhPutawayInventorySMultiSkuViewRow.getItemId(),
                                                hhPutInvWorkVRowImpl.getFacilityId(),
                                                hhPutawayInventorySMultiSkuViewRow.getContainerId());
        List<String> codesList = null;
        if ("N".equalsIgnoreCase(returnVal)) {
            codesList = new ArrayList<String>();
            codesList.add((String) msgType.getWrappedData());
            codesList.add((String) msgError.getWrappedData());
            return codesList;
        }
        return codesList;

    }

    public List<String> callTrExit(String containerId) {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        HhPutawayInventorySWorkLaborProdNewViewRowImpl hhPutInvWorkLaborProdVRowImpl =
            (HhPutawayInventorySWorkLaborProdNewViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLaborProdNewView").getCurrentRow();
        SQLOutParam msgType = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam msgError = new SQLOutParam(null, Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.TR_EXIT", msgType, msgError,
                                    hhPutawayInventorySMainViewRow.getContainerId(),
                                    hhPutawayInventorySMainViewRow.getItemId(),
                                    hhPutawayInventorySMainViewRow.getLocationId(),
                                    hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                    hhPutInvWorkVRowImpl.getFacilityId(),
                                    hhPutInvWorkLaborProdVRowImpl.getActivityCode(),
                                    hhPutInvWorkLaborProdVRowImpl.getContainersProcessed(),
                                    hhPutInvWorkLaborProdVRowImpl.getOperationsPerformed(),
                                    hhPutInvWorkLaborProdVRowImpl.getReferenceCode(),
                                    hhPutInvWorkLaborProdVRowImpl.getStartTime(),
                                    hhPutInvWorkLaborProdVRowImpl.getUnitsProcessed(),
                                    hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag(),
                                    hhPutawayInventorySWorkLocalViewRow.getHotReplenUnitQty(),
                                    hhPutInvWorkVRowImpl.getUser(), containerId, EMPTY_STRING);
        List<String> codesList = null;
        if (msgType.getWrappedData() != null) {
            codesList = new ArrayList<String>();
            codesList.add((String) msgError.getWrappedData());
        }
        return codesList;
    }

    public void callPupdatePendingQtys() {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            (HhPutawayInventorySMultiSkuViewRowImpl) this.getCurrentRow();
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.P_UPDATE_PENDING_QTYS",
                                    hhPutInvWorkVRowImpl.getFacilityId(),
                                    hhPutawayInventorySWorkLocalViewRow.getPendingCube(),
                                    hhPutawayInventorySWorkLocalViewRow.getPendingUnits(),
                                    hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                    hhPutawayInventorySMultiSkuViewRow.getItemId());

    }

    public List<Object> callPutawayMerchandise(String locationId, String containerId, String itemId, String blockName) {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            (HhPutawayInventorySMultiSkuViewRowImpl) this.getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        SQLOutParam workLocalHotReplenUnitQty = new SQLOutParam(new BigDecimal(0));
        SQLOutParam putUnitQtyOut = new SQLOutParam(new BigDecimal(0));
        SQLOutParam putCaseQtyOut = new SQLOutParam(new BigDecimal(0));
        SQLOutParam p_status_check = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_error_msg = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_msg_type = new SQLOutParam(EMPTY_STRING);
        List<Object> codesList = null;
        String flag =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_HOTEL_PICKING_ADF.putaway_merchandise", locationId, containerId,
                                                itemId, hhPutInvWorkVRowImpl.getFacilityId(),
                                                hhPutawayInventorySMainViewRow.getContainerId(),
                                                hhPutawayInventorySWorkLocalViewRow.getEventCode(),
                                                hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag(),
                                                hhPutawayInventorySWorkLocalViewRow.getChildrenPresentFlag(),
                                                hhPutawayInventorySWorkLocalViewRow.getPickType(),
                                                hhPutawayInventorySWorkLocalViewRow.getPutContainerQty(),
                                                hhPutawayInventorySWorkLocalViewRow.getPutUnitQty(),
                                                hhPutawayInventorySMultiSkuViewRow.getItemId(),
                                                hhPutawayInventorySMultiSkuViewRow.getConfirmQty(),
                                                hhPutawayInventorySMultiSkuViewRow.getContainerQty(),
                                                hhPutawayInventorySMultiSkuViewRow.getUnitQty(),
                                                workLocalHotReplenUnitQty,
                                                hhPutawayInventorySWorkLocalViewRow.getReplenEntireContFlag(),
                                                hhPutawayInventorySWorkLocalViewRow.getLabeledReserveFlag(),
                                                hhPutInvWorkVRowImpl.getUser(),
                                                hhPutawayInventorySMainViewRow.getConfirmCaseQty(),
                                                hhPutawayInventorySMainViewRow.getItemId(), blockName,
                                                hhPutawayInventorySMainViewRow.getSuggestedLocId(),
                                                hhPutawayInventorySMainViewRow.getToLocationId(),
                                                "HH_PUTAWAY_INVENTORY_S", p_status_check, P_msg_type, P_error_msg,
                                                putUnitQtyOut, putCaseQtyOut);
        if (putUnitQtyOut.getWrappedData() == null)
            putUnitQtyOut.setWrappedData(new BigDecimal(0));
        if (putCaseQtyOut.getWrappedData() == null)
            putCaseQtyOut.setWrappedData(new BigDecimal(0));

        hhPutawayInventorySWorkLocalViewRow.setHotReplenUnitQty((BigDecimal) workLocalHotReplenUnitQty.getWrappedData());
        codesList = new ArrayList<Object>();
        codesList.add(flag);
        codesList.add(putUnitQtyOut.getWrappedData());
        codesList.add(putCaseQtyOut.getWrappedData());
        return codesList;
    }


    public String callAdjustPflValues(String locationId, String itemId) {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        SQLOutParam P_error_msg = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_msg_type = new SQLOutParam(EMPTY_STRING);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.Adjust_PFL_Values", P_error_msg,
                                    P_msg_type, hhPutInvWorkVRowImpl.getFacilityId(), locationId, itemId,
                                    hhPutawayInventorySWorkLocalViewRow.getHotReplenFlag());

        if (P_error_msg.getWrappedData() != null)
            return (String) P_error_msg.getWrappedData();
        return null;
    }

    public List<String> callDisplayPutDirective() {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
            (HhPutawayInventorySMultiSkuViewRowImpl) this.getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        ;
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        HhPutawayInventorySNewFplViewRowImpl hhPutawayInventorySNewFplRowView =
            (HhPutawayInventorySNewFplViewRowImpl) ((applicationModule.findViewObject("HhPutawayInventorySNewFplView")).getCurrentRow());


        SQLOutParam p_multi_sku_unit_qty =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getUnitQty() == null ? new BigDecimal(0) :
                            hhPutawayInventorySMultiSkuViewRow.getUnitQty(), Types.NUMERIC);
        SQLOutParam p_work_local_put_container_qty =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getPutContainerQty() == null ? new BigDecimal(0) :
                            hhPutawayInventorySWorkLocalViewRow.getPutContainerQty(), Types.NUMERIC);
        SQLOutParam p_work_local_put_unit_qty =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getPutUnitQty() == null ? new BigDecimal(0) :
                            hhPutawayInventorySWorkLocalViewRow.getPutUnitQty(), Types.NUMERIC);
        SQLOutParam p_multi_sku_container_qty =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getConfirmQty() == null ? new BigDecimal(0) :
                            hhPutawayInventorySMultiSkuViewRow.getConfirmQty(), Types.NUMERIC);
        SQLOutParam p_multi_sku_location_id =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getLocationId(), Types.VARCHAR);
        SQLOutParam p_multi_sku_item_id =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getItemId(), Types.VARCHAR);
        SQLOutParam P_work_local_prev_kit_loc_id =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getPrevKittingLocId(), Types.VARCHAR);
        SQLOutParam P_multi_sku_confirm_loc =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getConfirmLoc(), Types.VARCHAR);
        SQLOutParam p_work_local_childr_present_f =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getChildrenPresentFlag(), Types.VARCHAR);
        SQLOutParam p_multi_sku_container_id =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getContainerId(), Types.VARCHAR);
        SQLOutParam p_work_local_repl_entir_cont_f =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getReplenEntireContFlag(), Types.VARCHAR);
        SQLOutParam p_new_fpl_item_desc =
            new SQLOutParam(hhPutawayInventorySNewFplRowView.getItemDesc(), Types.VARCHAR);
        SQLOutParam p_multi_sku_last_confirm_qty =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getLastConfirmQty() == null ? new BigDecimal(0) :
                            hhPutawayInventorySMultiSkuViewRow.getLastConfirmQty(), Types.NUMERIC);
        SQLOutParam p_multi_sku_description =
            new SQLOutParam(hhPutawayInventorySMultiSkuViewRow.getDescription(), Types.VARCHAR);
        SQLOutParam p_new_fpl_item_id = new SQLOutParam(hhPutawayInventorySNewFplRowView.getItemId(), Types.VARCHAR);
        SQLOutParam p_go_block = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_error_msg = new SQLOutParam(EMPTY_STRING);
        SQLOutParam P_msg_type = new SQLOutParam(EMPTY_STRING);
        SQLOutParam p_status_check = new SQLOutParam(EMPTY_STRING);
        List<String> codesList = new ArrayList<String>();
        String returnVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_HOTEL_PICKING_ADF.display_put_directive",
                                                hhPutInvWorkVRowImpl.getFacilityId(),
                                                hhPutawayInventorySMainViewRow.getContainerId(),
                                                hhPutawayInventorySWorkLocalViewRow.getPickType() == null ? EMPTY_STRING :
                                                hhPutawayInventorySWorkLocalViewRow.getPickType(), p_multi_sku_unit_qty,
                                                p_work_local_put_container_qty, p_work_local_put_unit_qty,
                                                p_multi_sku_container_qty, p_multi_sku_location_id, p_multi_sku_item_id,
                                                P_work_local_prev_kit_loc_id, P_multi_sku_confirm_loc,
                                                p_work_local_childr_present_f, p_multi_sku_container_id,
                                                p_work_local_repl_entir_cont_f, p_new_fpl_item_desc,
                                                p_multi_sku_last_confirm_qty, p_multi_sku_description,
                                                p_new_fpl_item_id, p_go_block, P_error_msg, P_msg_type, p_status_check);
        if ("INFO".equalsIgnoreCase((String) p_status_check.getWrappedData())) {
            codesList.add(returnVal);
            codesList.add((String) p_status_check.getWrappedData());
            codesList.add((String) P_error_msg.getWrappedData());
            codesList.add((String) p_go_block.getWrappedData());

        } else {
            codesList.add(returnVal);
        }
        return codesList;
    }

    public List<String> callMarkLoc(String locationId) {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        String error =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "mark_location", locationId,
                                                hhPutInvWorkVRowImpl.getFacilityId());
        List<String> codesList = null;
        codesList = new ArrayList<String>();
        if ("SUCCESS".equalsIgnoreCase(error)) {
            codesList.add("SUCCESS");
            //this.getDBTransaction().commit();
        } else {
            codesList.add("FAILED");
            codesList.add("E");
            codesList.add("OPER_FAILED");

        }
        return codesList;
    }

    public void setCurrentRow() {
        if (this.first() == null || this.getCurrentRow() == null) {
            HhPutawayInventorySMultiSkuViewRowImpl hhPutawayInventorySMultiSkuViewRow =
                (HhPutawayInventorySMultiSkuViewRowImpl) this.createRow();
            this.insertRow(hhPutawayInventorySMultiSkuViewRow);
            this.setCurrentRow(hhPutawayInventorySMultiSkuViewRow);
            //this.getDBTransaction().commit();
        }

    }
}
