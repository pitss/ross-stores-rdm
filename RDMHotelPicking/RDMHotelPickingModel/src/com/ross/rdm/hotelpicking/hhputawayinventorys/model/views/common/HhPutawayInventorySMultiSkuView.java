package com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.common;

import java.util.List;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 25 16:39:37 EDT 2015
// ---------------------------------------------------------------------
public interface HhPutawayInventorySMultiSkuView extends ViewObject {
    void setCurrentRow();

    List<String> callVPutawayLocation();


    List<String> callvContainerId();

    List<String> callTrExit(String containerId);

    List<String> vPutQty();

    String callAdjustPflValues(String locationId, String itemId);

    List<String> callDisplayPutDirective();

    void callPupdatePendingQtys();

    List<Object> callPutawayMerchandise(String locationId, String containerId, String itemId, String blockName);

    List<String> callMarkLoc(String locationId);
}

