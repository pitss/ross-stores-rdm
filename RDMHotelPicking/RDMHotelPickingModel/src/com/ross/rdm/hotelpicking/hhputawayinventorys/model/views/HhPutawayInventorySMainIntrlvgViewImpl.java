package com.ross.rdm.hotelpicking.hhputawayinventorys.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.hotelpicking.hhputawayinventorys.model.views.common.HhPutawayInventorySMainIntrlvgView;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.jbo.ApplicationModule;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mo Jul 27 22:27:45 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhPutawayInventorySMainIntrlvgViewImpl extends RDMTransientViewObjectImpl implements HhPutawayInventorySMainIntrlvgView {


    /**
     * This is the default constructor (do not remove).
     */
    public HhPutawayInventorySMainIntrlvgViewImpl() {
    }

    public void callPrepareExitPutaway() {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkLaborProdNewViewRowImpl hhPutInvWorkLaborProdVRowImpl =
            (HhPutawayInventorySWorkLaborProdNewViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLaborProdNewView").getCurrentRow();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        SQLOutParam ficilityId = new SQLOutParam(hhPutInvWorkVRowImpl.getFacilityId(), Types.VARCHAR);
        SQLOutParam user = new SQLOutParam(hhPutInvWorkVRowImpl.getUser(), Types.VARCHAR);
        SQLOutParam operationsPerformed =
            new SQLOutParam(hhPutInvWorkLaborProdVRowImpl.getOperationsPerformed(), Types.NUMERIC);
        SQLOutParam containersProcessed =
            new SQLOutParam(hhPutInvWorkLaborProdVRowImpl.getContainersProcessed(), Types.NUMERIC);
        SQLOutParam activityCode = new SQLOutParam(hhPutInvWorkLaborProdVRowImpl.getActivityCode(), Types.VARCHAR);
        SQLOutParam referenceCode = new SQLOutParam(hhPutInvWorkLaborProdVRowImpl.getReferenceCode(), Types.VARCHAR);
        SQLOutParam startTime = new SQLOutParam(hhPutInvWorkLaborProdVRowImpl.getStartTime(), Types.DATE);
        SQLOutParam unitsProcessed =
            new SQLOutParam(hhPutInvWorkLaborProdVRowImpl.getContainersProcessed(), Types.NUMERIC);
        SQLOutParam errorMsg = new SQLOutParam("");
        SQLOutParam msgType = new SQLOutParam("");
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.PREPARE_EXIT_PUTAWAY_INTRLVG",
                                    errorMsg, msgType, ficilityId, activityCode, containersProcessed,
                                    operationsPerformed, referenceCode, startTime, unitsProcessed, user);
    }

    public String callVSuggestedMpl() {

        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        HhPutawayInventorySMainIntrlvgViewRowImpl HhPutawayInventorySMainIntrlvgViewRow =
            (HhPutawayInventorySMainIntrlvgViewRowImpl) this.getCurrentRow();
        SQLOutParam P_msg_type = new SQLOutParam("");
        SQLOutParam P_error_msg = new SQLOutParam("");
        SQLOutParam p_work_local_pick_type =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getPickType(), Types.VARCHAR);
        String returnCode =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_HOTEL_PICKING_ADF.V_SUGGESTED_MLP", P_error_msg, P_msg_type,
                                                hhPutawayInventorySMainViewRow.getContainerId(),
                                                hhPutInvWorkVRowImpl.getFacilityId(), p_work_local_pick_type,
                                                HhPutawayInventorySMainIntrlvgViewRow.getConfrmContainerId(),
                                                HhPutawayInventorySMainIntrlvgViewRow.getContainerId());
        return returnCode;
    }


    public String callTrKeyInCursors(String containerId, String facilityId) {
        SQLOutParam pLocationIdOut = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pReasonCount = new SQLOutParam(null, Types.NUMERIC);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.TR_KEY_IN_CURSORS", facilityId,
                                    containerId, pLocationIdOut, pReasonCount);
        return (String) pLocationIdOut.getWrappedData();
    }

    public void setCurrentRow() {
        if (this.first() == null || this.getCurrentRow() == null) {
            HhPutawayInventorySMainIntrlvgViewRowImpl HhPutawayInventorySMainIntrlvgViewRow =
                (HhPutawayInventorySMainIntrlvgViewRowImpl) this.createRow();
            this.insertRow(HhPutawayInventorySMainIntrlvgViewRow);
            this.setCurrentRow(HhPutawayInventorySMainIntrlvgViewRow);
        } else if (this.first() != null)
            this.setCurrentRow(this.first());

    }

    public List<String> callPopulatePutaway(String locationId, String containerId, String facilityId,
                                            String pmAllowaIntrlvg, String pmStartLocation, String pmodein) {
        ApplicationModule applicationModule = this.getApplicationModule();
        HhPutawayInventorySWorkLocalViewRowImpl hhPutawayInventorySWorkLocalViewRow =
            (HhPutawayInventorySWorkLocalViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkLocalView").getCurrentRow();
        HhPutawayInventorySMainIntrlvgViewRowImpl HhPutawayInventorySMainIntrlvgViewRow =
            (HhPutawayInventorySMainIntrlvgViewRowImpl) this.createRow();
        HhPutawayInventorySWorkViewRowImpl hhPutInvWorkVRowImpl =
            (HhPutawayInventorySWorkViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySWorkView").getCurrentRow();
        HhPutawayInventorySMainViewRowImpl hhPutawayInventorySMainViewRow =
            (HhPutawayInventorySMainViewRowImpl) applicationModule.findViewObject("HhPutawayInventorySMainView").getCurrentRow();
        SQLOutParam p_work_local_seq_id =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getSeqId() == null ? new BigDecimal(0) :
                            hhPutawayInventorySWorkLocalViewRow.getPutContainerQty(), Types.NUMERIC);
        SQLOutParam p_mIntrlv_storage_category =
            new SQLOutParam(hhPutawayInventorySMainViewRow.getStorageCategory() == null ? "" :
                            hhPutawayInventorySMainViewRow.getStorageCategory(), Types.VARCHAR);
        SQLOutParam p_work_local_dedicated_zone_flag =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getDedicatedZoneFlag() == null ? "" :
                            hhPutawayInventorySWorkLocalViewRow.getDedicatedZoneFlag(), Types.VARCHAR);
        SQLOutParam p_work_local_item_id =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getItemId() == null ? "" :
                            hhPutawayInventorySWorkLocalViewRow.getItemId(), Types.VARCHAR);
        SQLOutParam p_work_local_has_food =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getHasFood() == null ? "" :
                            hhPutawayInventorySWorkLocalViewRow.getHasFood(), Types.VARCHAR);
        SQLOutParam p_work_local_has_supplement =
            new SQLOutParam(hhPutawayInventorySWorkLocalViewRow.getHasSupplement() == null ? "" :
                            hhPutawayInventorySWorkLocalViewRow.getHasSupplement(), Types.VARCHAR);
        String mode_in = pmodein;
        SQLOutParam P_msg_code = new SQLOutParam("");
        SQLOutParam P_msg_mode = new SQLOutParam("");
        SQLOutParam P_msg_type = new SQLOutParam("");
        SQLOutParam p_v_return = new SQLOutParam("");
        SQLOutParam p_main_intrlvg_location_id = new SQLOutParam("");
        SQLOutParam p_main_intrlvg_container_id = new SQLOutParam("");
        SQLOutParam p_work_local_pick_type = new SQLOutParam("");
        List<String> codesList = null;
        String pickType = hhPutawayInventorySWorkLocalViewRow.getPickType();
        if (null != pickType)
            p_work_local_pick_type.setWrappedData(hhPutawayInventorySWorkLocalViewRow.getPickType());
        String error =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                "PKG_HOTEL_PICKING_ADF.Populate_Putaway", locationId, containerId,
                                                mode_in, facilityId, pmAllowaIntrlvg, pmStartLocation,
                                                hhPutawayInventorySMainViewRow.getContainerId(),
                                                hhPutawayInventorySWorkLocalViewRow.getProcessByAisle(),
                                                hhPutInvWorkVRowImpl.getUser(), p_main_intrlvg_location_id,
                                                p_main_intrlvg_container_id, p_work_local_pick_type,
                                                hhPutawayInventorySWorkLocalViewRow.getPendingCube(),
                                                hhPutawayInventorySWorkLocalViewRow.getPendingUnits(), P_msg_code,
                                                P_msg_mode, P_msg_type, p_v_return, p_mIntrlv_storage_category,
                                                p_work_local_seq_id, p_work_local_dedicated_zone_flag,
                                                p_work_local_item_id, p_work_local_has_food,
                                                p_work_local_has_supplement);
        hhPutawayInventorySWorkLocalViewRow.setSeqId((BigDecimal) p_work_local_seq_id.getWrappedData());
        hhPutawayInventorySWorkLocalViewRow.setDedicatedZoneFlag((String) p_work_local_dedicated_zone_flag.getWrappedData());
        hhPutawayInventorySWorkLocalViewRow.setItemId((String) p_work_local_item_id.getWrappedData());
        hhPutawayInventorySWorkLocalViewRow.setHasFood((String) p_work_local_has_food.getWrappedData());
        hhPutawayInventorySWorkLocalViewRow.setHasSupplement((String) p_work_local_has_supplement.getWrappedData());
        HhPutawayInventorySMainIntrlvgViewRow.setStorageCategory((String) p_mIntrlv_storage_category.getWrappedData());
        HhPutawayInventorySMainIntrlvgViewRow.setContainerId((String) p_main_intrlvg_container_id.getWrappedData());
        HhPutawayInventorySMainIntrlvgViewRow.setLocationId((String) p_main_intrlvg_location_id.getWrappedData());
        this.insertRow(HhPutawayInventorySMainIntrlvgViewRow);
        if ("MSG".equalsIgnoreCase(error)) {
            codesList = new ArrayList<String>();
            codesList.add((String) P_msg_code.getWrappedData());
            codesList.add((String) P_msg_mode.getWrappedData());
            codesList.add(error);
        }
        return codesList;
    }
}
