package com.ross.rdm.hotelpicking.hhcontainerinquirys.model.views.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jul 22 09:50:33 CEST 2016
// ---------------------------------------------------------------------
public interface ContainerItem extends ViewObject {
    void executeQueryContainerItem(String facility, String container);
}

