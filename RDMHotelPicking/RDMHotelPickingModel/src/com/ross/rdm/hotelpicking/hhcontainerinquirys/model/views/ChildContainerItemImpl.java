package com.ross.rdm.hotelpicking.hhcontainerinquirys.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.hotelpicking.hhcontainerinquirys.model.views.common.ChildContainerItem;

import java.sql.ResultSet;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Mar 22 10:23:55 CET 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ChildContainerItemImpl extends RDMViewObjectImpl implements ChildContainerItem {
    /**
     * This is the default constructor (do not remove).
     */
    public ChildContainerItemImpl() {
    }

    public void executeQueryChildContainerItem(String facility, String container) {
//        setbind_facility(facility);
//        setbind_container(container);
        executeQuery();
        setCurrentRow(first());
    }
}

