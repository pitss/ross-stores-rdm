package com.ross.rdm.common.utils.view.bean;

import java.util.Map;

import javax.annotation.PostConstruct;

import oracle.adf.view.rich.render.ClientEvent;

import com.ross.rdm.common.utils.view.util.JSFUtils;

import java.io.Serializable;

import org.apache.myfaces.trinidad.context.Agent;
import org.apache.myfaces.trinidad.context.RequestContext;

public class AgentInfoBean implements Serializable {
    @SuppressWarnings("compatibility:-166981773491214690")
    private static final long serialVersionUID = 1L;
    private static final String SCREEN_WIDTH_KEY = "screenWidth";
    private static final String SCREEN_HEIGHT_KEY = "screenHeight";
    private static final String DEVICE_PIXEL_RATIO_KEY = "devicePixelRatio";
    private double screenWidth = 0;
    private double screenHeight = 0;
    private double devicePixelRatio = 0;
    private String mainMenuName;
    private String cylinderMenuName;


    public AgentInfoBean() {
        super();
    }

    /**
     * Check if request contains screenWidth, screenHeight devicePixelRati params. If so, store
     * the values in corresponding properties
     */
    @PostConstruct
    public void init() {
        String width = (String) JSFUtils.getRequestParam(SCREEN_WIDTH_KEY);
        String height = (String) JSFUtils.getRequestParam(SCREEN_HEIGHT_KEY);
        String ratio = (String) JSFUtils.getRequestParam(DEVICE_PIXEL_RATIO_KEY);
        if (width != null) {
            screenWidth = new Double(width);
        }
        if (height != null) {
            screenHeight = new Double(height);
        }
        if (ratio != null && !"undefined".equals(ratio)) {
            devicePixelRatio = new Double(ratio);
        }
    }


    public boolean isLandscape() {
        return getScreenWidth() > getScreenHeight();
    }

    /**
     * Return true when device has touch screen and either screen width or height is < 400px.
     * We check either screen width or height depending on the device orientation
     * @return
     */
    public boolean isSmartPhone() {
        double size = isLandscape() ? getScreenHeight() : getScreenWidth();
        return (size <= 400);
    }

    public boolean isTablet() {
        return isTouchScreen() && !isSmartPhone();
    }

    public boolean isTouchScreen() {
        return !"none".equals(getAgent().getCapabilities().get("touchScreen"));
    }

    /**
     * If screen width and height are not yet set through request param
     * read in init method, we store the width and height with this method
     * and do a redirect to self so the dimesnions can be used to determine
     * UI rendering like page template
     * @param clientEvent
     */
    public void setWindowSize(ClientEvent clientEvent) {
        if (getScreenWidth() == 0) {
            Map<String, Object> map = clientEvent.getParameters();
            Double width = (Double) map.get(SCREEN_WIDTH_KEY);
            Double height = (Double) map.get(SCREEN_HEIGHT_KEY);
            Double ratio = (Double) map.get(DEVICE_PIXEL_RATIO_KEY);
            this.setScreenHeight(height);
            this.setScreenWidth(width);
            if (ratio != null) {
                setDevicePixelRatio(ratio);
            }
            JSFUtils.redirectToSelf();
        }
    }

    public Agent getAgent() {
        RequestContext context = RequestContext.getCurrentInstance();
        Agent agent = context.getAgent();
        return agent;
    }

    public boolean isTruckMount() {
        double hight = this.getScreenHeight();
        double width = this.getScreenWidth();
        return (hight >= 250 && hight <= 480) && (width >= 400 && width <= 800);
    }

    public boolean isScanGun() {
        return this.getScreenWidth() == 240 && this.getScreenHeight() == 320;
    }

    public boolean isGladiator() {
        return this.getScreenWidth() == 320 && this.getScreenHeight() == 240;
    }

    public boolean isDesktop() {
        return this.getScreenWidth() > 800 && this.getScreenHeight() > 480;
    }

    public String getRunningDevice() {
        if (this.isTruckMount())
            return "TruckMount";
        else if (this.isScanGun())
            return "ScanGun";
        else if (this.isGladiator())
            return "Gladiator";
        else
            return "Desktop";
    }

    public void setCylinderMenuName(String cylinderMenuName) {
        switch (this.getRunningDevice()) {
        case "Desktop":
            cylinderMenuName += "_IE";
            break;
        case "ScanGun":
            cylinderMenuName += "_RF";
            break;
        case "Gladiator":
            cylinderMenuName += "_SG";
            break;
        case "TruckMount":
            cylinderMenuName += "_TM";
            break;
        default:
            break;
        }
        this.cylinderMenuName = cylinderMenuName;
    }


    public void setMainMenuName(String mainMenuName) {
        this.mainMenuName = mainMenuName;
    }

    public String getMainMenuName() {
        return mainMenuName;
    }

    public void setScreenWidth(double screenWidth) {
        this.screenWidth = screenWidth;
    }

    public double getScreenWidth() {
        return screenWidth;
    }

    public void setScreenHeight(double screenHeight) {
        this.screenHeight = screenHeight;
    }

    public double getScreenHeight() {
        return screenHeight;
    }

    public static AgentInfoBean getInstance() {
        return (AgentInfoBean) JSFUtils.getExpressionValue("#{agentInfo}");
    }

    public void setDevicePixelRatio(double devicePixelRatio) {
        this.devicePixelRatio = devicePixelRatio;
    }

    public double getDevicePixelRatio() {
        return devicePixelRatio;
    }


    public String getCylinderMenuName() {
        return cylinderMenuName;
    }

}
