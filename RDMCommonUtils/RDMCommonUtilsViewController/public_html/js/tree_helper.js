/*
 * The af:tree component does not have a dedicated server-side double-click 
 * event, but it can be simulated by queuing a custom event.
 * This may be useful when porting WHEN-TREE-NODE-ACTIVATED trigger code
 * from Forms to ADF.
 */
function treeDblClick(event) {
    var source = event.getSource();
    AdfCustomEvent.queue( source, "treeNodeActivated", {}, false);
}

/*
 * The af:tree component does not have a dedicated server-side key-press event,
 * but it can be simulated by queuing a custom event.
 * This may be useful when porting code from Forms to ADF, where pressing
 * the enter key fires the WHEN-TREE-NODE-ACTIVATED trigger.
 */
function treeEnterKeyPressed(event) {
    if ( AdfKeyStroke.ENTER_KEY == event.getKeyCode() ) {
        var source = event.getSource();
        AdfCustomEvent.queue( source, "treeNodeActivated", {}, false);    
    }
}