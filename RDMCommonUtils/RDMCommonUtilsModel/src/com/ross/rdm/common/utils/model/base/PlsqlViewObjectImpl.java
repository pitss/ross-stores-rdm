package com.ross.rdm.common.utils.model.base;

import oracle.jbo.server.ViewDefImpl;

/**
 * Custom ADF Framework Extension Class for PlsqlViewObjectImpl.
 */
public class PlsqlViewObjectImpl extends RDMViewObjectImpl {

      public PlsqlViewObjectImpl() {
        super();
    }
      public PlsqlViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }
}
