package com.ross.rdm.common.utils.model.base;

import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;

import oracle.jbo.server.ApplicationModuleImpl;

/**
 * Custom ADF Framework Extension Class for RDMApplicationModuleImpl.
 */
public class RDMApplicationModuleImpl extends ApplicationModuleImpl {

      public RDMApplicationModuleImpl() {
        super();
    }
  /**
     * Helper Method to Simplify Invoking Stored Procedures 
     */
      public void callStoredProcedure(
                                        String stmt,
                                       Object... bindVars)  {
                 DBUtils.callStoredProcedure(getDBTransaction(), 
                                          stmt, bindVars);
    }
  /**
     * Helper Method to Simplify Invoking Stored Functions 
     */
      public Object callStoredFunction(int sqlReturnType,
                                        String stmt,
                                       Object... bindVars)  {
                 return DBUtils.callStoredFunction(getDBTransaction(), sqlReturnType,
                                          stmt, bindVars);
    }
}
