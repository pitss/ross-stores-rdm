package com.ross.rdm.common.utils.model.base;

import oracle.jbo.server.ViewRowImpl;

/**
 * Custom ADF Framework Extension Class for RDMViewRowImpl.
 */
public class RDMViewRowImpl extends ViewRowImpl {

    @Override
    public void setAttribute(int i, Object object) { 
        super.setAttribute(i, getCustomValue(object));
    }
    
    private Object getCustomValue(Object val){
        if (val != null && val instanceof String) {
            return val.toString().toUpperCase();
        }
        return val;
    }
}
