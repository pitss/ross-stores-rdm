package com.ross.rdm.common.utils.model.base;

import oracle.jbo.server.EntityImpl;

/**
 * Custom ADF Framework Extension Class for RDMEntityImpl.
 */
public class RDMEntityImpl extends EntityImpl {

      public RDMEntityImpl() {
        super();
    }
}
