package com.ross.rdm.common.utils.model.util.dbcall;


import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.SQLData;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import oracle.sql.ORADataFactory;

public class SQLOutParam {
    private Object wrapped;
    private int sqlType;
    private String sqlTypeName;
    private ORADataFactory oraDataFactory;

    public SQLOutParam(Object wrapped) {
        this.wrapped = wrapped;
        guessDefaultTypeForObject(wrapped);
    }

    public SQLOutParam(Object wrapped, int sqlType) {
        this.wrapped = wrapped;
        this.sqlType = sqlType;
    }

    public SQLOutParam(Object wrapped, int sqlType, String sqlTypeName, ORADataFactory oraDataFactory) {
        this.wrapped = wrapped;
        this.sqlType = sqlType;
        this.sqlTypeName = sqlTypeName;
        this.oraDataFactory = oraDataFactory;
    }

    public void setWrappedData(Object newValue) {
        this.wrapped = newValue;
    }

    public Object getWrappedData() {
        return wrapped;
    }

    public int getSqlType() {
        return sqlType;
    }

    public void setSqlTypeName(String sqlTypeName) {
        this.sqlTypeName = sqlTypeName;
    }

    public String getSqlTypeName() {
        return sqlTypeName;
    }

    public void setOraDataFactory(ORADataFactory oraDataFactory) {
        this.oraDataFactory = oraDataFactory;
    }

    public ORADataFactory getOraDataFactory() {
        return oraDataFactory;
    }

    private void guessDefaultTypeForObject(Object o) {
        if (o instanceof SQLData) {
            sqlType = Types.STRUCT;
        } else if (o instanceof String) {
            sqlType = Types.VARCHAR;
        } else if (o instanceof Boolean) {
            sqlType = Types.BOOLEAN;
        } else if (o instanceof Byte) {
            sqlType = Types.TINYINT;
        } else if (o instanceof java.math.BigDecimal) {
            sqlType = Types.NUMERIC;
        } else if (o instanceof Short) {
            sqlType = Types.SMALLINT;
        } else if (o instanceof Integer) {
            sqlType = Types.INTEGER;
        } else if (o instanceof Long) {
            sqlType = Types.BIGINT;
        } else if (o instanceof Float) {
            sqlType = Types.REAL;
        } else if (o instanceof Double) {
            sqlType = Types.DOUBLE;
        } else if (o instanceof Byte[]) {
            sqlType = Types.BINARY;
        } else if (o instanceof Date) {
            sqlType = Types.DATE;
        } else if (o instanceof Time) {
            sqlType = Types.TIME;
        } else if (o instanceof Timestamp) {
            sqlType = Types.TIMESTAMP;
        } else if (o instanceof Clob) {
            sqlType = Types.CLOB;
        } else if (o instanceof Blob) {
            sqlType = Types.BLOB;
        }
    }

    public ORADataFactory getORADataFactory() {
        if ((this.oraDataFactory == null) && (Types.ARRAY == sqlType || Types.STRUCT == sqlType)) {
            if ((this.wrapped instanceof ORADataFactory) || (this.wrapped instanceof oracle.jbo.domain.Struct)) {
                for (Method method : this.wrapped.getClass().getDeclaredMethods()) {
                    if (method.getName().equals("getORADataFactory")) {
                        try {
                            oraDataFactory = (ORADataFactory) method.invoke(this.wrapped);
                        } catch (IllegalAccessException e) {
                            throw new IllegalArgumentException("ORADataFactory should be provided.");
                        } catch (InvocationTargetException e) {
                            throw new IllegalArgumentException("ORADataFactory should be provided.");
                        }
                    }
                }
            }
        }
        return this.oraDataFactory;
    }
}
