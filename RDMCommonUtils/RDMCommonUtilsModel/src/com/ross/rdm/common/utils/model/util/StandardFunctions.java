package com.ross.rdm.common.utils.model.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * implementation of several functions from the package "STANDARD", which do not
 * have equivalents in Java and cannot be easily translated "in-situ"
 */
public final class StandardFunctions {
    
    // not instantiable
    private StandardFunctions() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // character related functions
    ////////////////////////////////////////////////////////////////////////////
    
    public static String initcap(String input) {        
        Pattern p = Pattern.compile("((\\w)+)");
        Matcher m = p.matcher(input);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, firstUpper(m.group()));
        }
        m.appendTail(sb);
        return sb.toString();
    }  
    
    /*
     * input is the string to pad characters to (the left-hand side).
       len is the number of characters to return. If the padded_length is smaller than the original string, 
            the lpad function will truncate the string to the size of padded_length.
        pad is optional. This is the string that will be padded to the left-hand side of string1. If this parameter is omitted, the lpad function will pad spaces to the left-side of string1.

       lpad('tech', 7); 	            would return '   tech'
       lpad('tech', 2); 	            would return 'te'
       lpad('tech', 8, '0'); 	            would return '0000tech'
       lpad('tech on the net', 15, 'z');    would return 'tech on the net'
       lpad('tech on the net', 16, 'z');    would return 'ztech on the net'
     */
    public static String lpad(String input, int len, String pad) {
        int inputLen = input.length();
        if ( inputLen >= len ) {
            return input.substring(0, len);
        }
        int padLen = pad.length();        
        
        StringBuilder sb = new StringBuilder(len);
        for (int i = inputLen; i < len; i += padLen) {            
            if ( i + padLen > len ) {
                sb.append(pad.substring(0, i + padLen - len));
            } else {
                sb.append(pad);
            }            
        }
        sb.append(input);
        return sb.toString();        
    }
    
    public static String rpad(String input, int len, String pad) {
        int inputLen = input.length();
        if ( inputLen >= len ) {
            return input.substring(0, len);
        }        
        int padLen = pad.length();
        
        StringBuilder sb = new StringBuilder();
        sb.append(input);
        for (int i = inputLen; i < len; i += padLen) {
            if ( i + padLen > len ) {
                sb.append(pad.substring(0, i + padLen - len));
            } else {
                sb.append(pad);
            }                        
        }
        return sb.toString();                
    }
    
    public static String ltrim(String input, String chSet) { 
        int index;
        int len = input.length();
        
        for( index = 0; index < len; index++ ) {
            if ( chSet.indexOf(input.charAt(index)) == -1 )
                break;
        }
        
        return input.substring(index);
    }
    
    public static String rtrim(String input, String chSet) {
        int index;
        int len = input.length();
        
        for( index = len - 1; index >= 0; index-- ) {
            if ( chSet.indexOf(input.charAt(index)) == -1 )
                break;
        }
        
        return input.substring(0, index + 1);
    }
    
    public static String translate(String input, String toReplace, String replaceWith) {
        String result = input;
        int replaceLen = toReplace.length();
        for (int i = 0; i < replaceLen; i++) {
            result = result.replace(toReplace.charAt(i), replaceWith.charAt(i));
        }
        
        return result;
    }
        
    ////////////////////////////////////////////////////////////////////////////
    // private helper functions
    ////////////////////////////////////////////////////////////////////////////
    
    private static String firstUpper(String s) {
        return Character.toUpperCase(s.charAt(0))+s.substring(1).toLowerCase();
    }
}
