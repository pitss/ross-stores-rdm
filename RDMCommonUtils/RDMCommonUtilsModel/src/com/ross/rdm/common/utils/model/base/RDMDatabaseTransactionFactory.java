package com.ross.rdm.common.utils.model.base;

import oracle.jbo.server.DBTransactionImpl2;
import oracle.jbo.server.DatabaseTransactionFactory;

/**
 * Custom ADF Framework Extension Class for RDMDatabaseTransactionFactory.
 */
public class RDMDatabaseTransactionFactory extends DatabaseTransactionFactory {

  /**
 * Return an instance of custom RDMDBTransactionImpl class
 * instead of the default implementation.
 *
 * @return An instance of custom RDMDBTransactionImpl implementation.
 */
    public DBTransactionImpl2 create() {
    return new RDMDBTransactionImpl();
  }
}
