package com.ross.rdm.common.utils.model.util.dbcall.customTypes;

public class RDMDefaultDbSchemaResolverImpl implements RDMDbSchemaResolver {

    @Override
    public String resolveObjectName(String dbObjectName) {
        String schemaName = System.getProperty("raf.dbschema.resolver.default.schema.name");
        if ((schemaName == null) || (schemaName.isEmpty())) {
            return dbObjectName.toUpperCase();
        }
        return (schemaName + "." + dbObjectName).toUpperCase();
    }
}
