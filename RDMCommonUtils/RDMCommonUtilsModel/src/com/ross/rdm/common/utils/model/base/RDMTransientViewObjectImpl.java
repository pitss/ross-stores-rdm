package com.ross.rdm.common.utils.model.base;

import java.sql.ResultSet;

import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewRowImpl;
import oracle.jbo.server.ViewRowSetImpl;

public class RDMTransientViewObjectImpl extends RDMViewObjectImpl {
    public RDMTransientViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }

    public RDMTransientViewObjectImpl( ) {
        super( );
    }
    /**
     * getQueryHitCount - overridden for custom java data source support.
     */
    public long getQueryHitCount(ViewRowSetImpl viewRowSet) {
      // TODO call necessary database function to get the count of retrieved rows.
      //long value = super.getQueryHitCount(viewRowSet);
      //return value;
      return 1;
    }
    
    /**
     * create - overridden for custom java data source support.
     * remove all traces of a SQL query for this View Object.
     */
    protected void create() {
      getViewDef().setQuery(null);
      getViewDef().setSelectClause(null);
      setQuery(null);
    }
    
    /**
     * executeQueryForCollection - overridden for custom java data source support.
     */
    protected void executeQueryForCollection(Object qc, Object[] params,
                                             int noUserParams) {
      super.executeQueryForCollection(qc, params, noUserParams);
    }
    
    /**
     * hasNextForCollection - overridden for custom java data source support.
     */
    protected boolean hasNextForCollection(Object qc) {
      boolean bRet = super.hasNextForCollection(qc);
      return bRet;
    }
    
    /**
     * createRowFromResultSet - overridden for custom java data source support.
     */
    protected ViewRowImpl createRowFromResultSet(Object qc,
                                                 ResultSet resultSet) {
      ViewRowImpl value = super.createRowFromResultSet(qc, resultSet);
      return value;
    }
}
