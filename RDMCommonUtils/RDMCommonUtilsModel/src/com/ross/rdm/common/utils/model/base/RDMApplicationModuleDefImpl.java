package com.ross.rdm.common.utils.model.base;

import oracle.jbo.server.ApplicationModuleDefImpl;

/**
 * Custom ADF Framework Extension Class for RDMApplicationModuleDefImpl.
 */
public class RDMApplicationModuleDefImpl extends ApplicationModuleDefImpl {

      public RDMApplicationModuleDefImpl() {
        super();
    }
      public RDMApplicationModuleDefImpl(int defScope, String name) {
        super(defScope, name);
    }
      public RDMApplicationModuleDefImpl(String name) {
        super(name);
    }
}
