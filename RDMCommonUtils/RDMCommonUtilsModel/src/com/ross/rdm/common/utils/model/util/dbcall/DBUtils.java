package com.ross.rdm.common.utils.model.util.dbcall;


import java.sql.CallableStatement;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.JboException;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

/**
 * A series of utility functions for calling database stored procedures and functions.
 *
 * @author PITSS
 *
 */
public class DBUtils {
    private static ADFLogger _logger = ADFLogger.createADFLogger(DBUtils.class);

    /**
     * Helper Method to Simplify Invoking Stored Procedures.
     * @param dbTransaction
     * @param stmt
     * @param bindVars
     */
    public static void callStoredProcedure(DBTransaction dbTransaction, String stmt, Object... bindVars) {
        long lStartTime = new Date().getTime(); // start time

        CallableStatement st = null;
        try {
            // Build the paramater string
            String paramPart = buildParamPart(bindVars != null ? bindVars.length : 0);
            // Create a JDBC CallabledStatement
            st = dbTransaction.createCallableStatement("begin " + stmt + paramPart + "; end;", 0);

            // Loop over values for the bind variables passed in, if any
            int index = 1;
            if (null != bindVars) {
                // Register Out Parameters, if any
                // Set the value of each bind variable in the statement, if any
                for (Object bindVar : bindVars) {
                    if (bindVar instanceof SQLOutParam) {
                        SQLOutParam o = (SQLOutParam) bindVar;
                        if (Types.ARRAY == o.getSqlType()) {
                            st.registerOutParameter(index, Types.ARRAY, o.getSqlTypeName());
                            if (o.getWrappedData() != null) {
                                st.setObject(index, o.getWrappedData());
                            } else {
                                st.setNull(index, o.getSqlType(), o.getSqlTypeName());
                            }
                        } else if (Types.STRUCT == o.getSqlType()) {
                            String sqlType = getTypeNameOnly((SQLData) o.getWrappedData());
                            st.registerOutParameter(index, Types.STRUCT, sqlType);
                        } else {
                            st.registerOutParameter(index, o.getSqlType());
                        }
                        st.setObject(index, o.getWrappedData());

                    } else {
                        st.setObject(index, bindVar);
                        _logger.info("Input parameter index " + index + " bindvar " + bindVar);
                    }
                    index++;
                }
            }

            // Execute the statement
            st.executeUpdate();

            // Loop over values for the bind variables passed in
            // Get the value of Out parameters, if any
            index = 1;
            if (null != bindVars) {
                SQLOutParam param = null;
                for (Object bindVar : bindVars) {
                    if (bindVar instanceof SQLOutParam) {
                        param = (SQLOutParam) bindVar;
                        Object wrappedParam = param.getWrappedData();
                        if (wrappedParam instanceof SQLData) {
                            SQLBridge.Register((SQLData) wrappedParam);
                            Map<String, Class<?>> typeMap = new HashMap<String, Class<?>>();
                            typeMap.put(getTypeNameOnly((SQLData) wrappedParam), SQLBridge.class);
                            st.getObject(index, typeMap);
                        } else if (param.getORADataFactory() != null) {
                            OracleCallableStatement ocs = (OracleCallableStatement) st;
                            param.setWrappedData(ocs.getORAData(index, param.getORADataFactory()));
                        } else {
                            param.setWrappedData(st.getObject(index));
                        }
                    }
                    index++;
                }
            }

        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    // Close the statement
                    st.close();
                } catch (SQLException e) {
                    _logger.severe(e.getMessage());
                }
            }
        }
        long lEndTime = new Date().getTime(); // end time

        long difference = lEndTime - lStartTime; // check different
        _logger.info("Elapsed time for method call  " + stmt + " milliseconds: " + difference);

    }

    /**
     * Helper Method to Simplify Invoking Stored Procedures.
     * @param dbTransaction
     * @param stmt
     * @param bindVars
     */
    public static void callStoredProcedureWithNulls(DBTransaction dbTransaction, String stmt, Object... bindVars) {
        long lStartTime = new Date().getTime(); // start time

        CallableStatement st = null;
        try {
            // Build the paramater string
            String paramPart = buildParamPart(bindVars != null ? bindVars.length : 0);
            // Create a JDBC CallabledStatement
            st = dbTransaction.createCallableStatement("begin " + stmt + paramPart + "; end;", 0);

            // Loop over values for the bind variables passed in, if any
            int index = 1;
            if (null != bindVars) {
                // Register Out Parameters, if any
                // Set the value of each bind variable in the statement, if any
                for (Object bindVar : bindVars) {
                    if (bindVar instanceof SQLOutParam) {
                        SQLOutParam o = (SQLOutParam) bindVar;
                        if (Types.ARRAY == o.getSqlType()) {
                            st.registerOutParameter(index, Types.ARRAY, o.getSqlTypeName());
                            if (o.getWrappedData() != null) {
                                st.setObject(index, o.getWrappedData());
                            } else {
                                st.setNull(index, o.getSqlType(), o.getSqlTypeName());
                            }
                        } else if (Types.STRUCT == o.getSqlType()) {
                            String sqlType = getTypeNameOnly((SQLData) o.getWrappedData());
                            st.registerOutParameter(index, Types.STRUCT, sqlType);
                            if (o.getWrappedData() != null) {
                                st.setObject(index, o.getWrappedData());
                            } else {
                                st.setNull(index, o.getSqlType(), sqlType);
                            }
                        } else {
                            if (null == o.getWrappedData()) {
                                st.registerOutParameter(index, o.getSqlType());
                                st.setNull(index, o.getSqlType());
                            } else {
                                st.registerOutParameter(index, o.getSqlType());
                                st.setObject(index, o.getWrappedData());
                            }
                        }
                    } else {
                        st.setObject(index, bindVar);
                        _logger.info("Input parameter index " + index + " bindvar " + bindVar);
                    }
                    index++;
                }
            }

            // Execute the statement
            st.executeUpdate();

            // Loop over values for the bind variables passed in
            // Get the value of Out parameters, if any
            index = 1;
            if (null != bindVars) {
                SQLOutParam param = null;
                for (Object bindVar : bindVars) {
                    if (bindVar instanceof SQLOutParam) {
                        param = (SQLOutParam) bindVar;
                        Object wrappedParam = param.getWrappedData();
                        if (wrappedParam instanceof SQLData) {
                            SQLBridge.Register((SQLData) wrappedParam);
                            Map<String, Class<?>> typeMap = new HashMap<String, Class<?>>();
                            typeMap.put(getTypeNameOnly((SQLData) wrappedParam), SQLBridge.class);
                            st.getObject(index, typeMap);
                        } else if (param.getORADataFactory() != null) {
                            OracleCallableStatement ocs = (OracleCallableStatement) st;
                            param.setWrappedData(ocs.getORAData(index, param.getORADataFactory()));
                        } else {
                            param.setWrappedData(st.getObject(index));
                        }
                    }
                    index++;
                }
            }

        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    // Close the statement
                    st.close();
                } catch (SQLException e) {
                    _logger.severe(e.getMessage());
                }
            }
        }
        long lEndTime = new Date().getTime(); // end time

        long difference = lEndTime - lStartTime; // check different
        _logger.info("Elapsed time for method call  " + stmt + " milliseconds: " + difference);

    }

    /**
     * Helper Method to Simplify Invoking Stored Functions.
     * @param dbTransaction
     * @param sqlReturnType
     * @param stmt
     * @param bindVars
     * @return
     */
    public static Object callStoredFunction(DBTransaction dbTransaction, int sqlReturnType, String stmt,
                                            Object... bindVars) {
        long lStartTime = new Date().getTime(); // start time

        CallableStatement st = null;
        try {
            // Build the paramater string
            String paramPart = buildParamPart(bindVars != null ? bindVars.length : 0);
            // Create a JDBC CallabledStatement
            st = dbTransaction.createCallableStatement("begin ? := " + stmt + paramPart + "; end;", 0);
            // Register the first bind variable for the return value
            st.registerOutParameter(1, sqlReturnType);

            // Loop over values for the bind variables passed in, if any
            int index = 2;
            if (null != bindVars) {
                // Register Out Parameters, if any
                // Set the value of each bind variable in the statement, if any
                for (Object bindVar : bindVars) {
                    if (bindVar instanceof SQLOutParam) {
                        SQLOutParam o = (SQLOutParam) bindVar;
                        if (Types.ARRAY == o.getSqlType()) {
                            st.registerOutParameter(index, Types.ARRAY, o.getSqlTypeName());
                            if (o.getWrappedData() != null) {
                                st.setObject(index, o.getWrappedData());
                            } else {
                                st.setNull(index, o.getSqlType(), o.getSqlTypeName());
                            }
                        } else if (Types.STRUCT == o.getSqlType()) {
                            String sqlType = getTypeNameOnly((SQLData) o.getWrappedData());
                            st.registerOutParameter(index, Types.STRUCT, sqlType);
                        } else {
                            st.registerOutParameter(index, o.getSqlType());
                        }
                        st.setObject(index, o.getWrappedData());
                    } else {
                        st.setObject(index, bindVar);
                        _logger.info("index " + index + " bindvar " + bindVar);
                    }
                    index++;
                }
            }

            // Execute the statement
            st.executeUpdate();

            // Loop over values for the bind variables passed in
            // Get the value of Out parameters, if any
            index = 2;
            if (null != bindVars) {
                SQLOutParam param = null;
                for (Object bindVar : bindVars) {
                    if (bindVar instanceof SQLOutParam) {
                        param = (SQLOutParam) bindVar;
                        Object wrappedParam = param.getWrappedData();
                        if (wrappedParam instanceof SQLData) {
                            SQLBridge.Register((SQLData) wrappedParam);
                            Map<String, Class<?>> typeMap = new HashMap<String, Class<?>>();
                            typeMap.put(getTypeNameOnly((SQLData) wrappedParam), SQLBridge.class);
                            st.getObject(index, typeMap);
                        } else if (param.getORADataFactory() != null) {
                            OracleCallableStatement ocs = (OracleCallableStatement) st;
                            param.setWrappedData(ocs.getORAData(index, param.getORADataFactory()));
                        } else {
                            param.setWrappedData(st.getObject(index));
                        }
                    }
                    index++;
                }
            }
            long lEndTime = new Date().getTime(); // end time

            long difference = lEndTime - lStartTime; // check different

            _logger.info("Elapsed time for method call  " + stmt + " milliseconds: " + difference);

            // Return the value of the first bind variable
            return st.getObject(1);
        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    // Close the statement
                    st.close();
                } catch (SQLException e) {
                    _logger.severe(e.getMessage());
                }
            }
        }

    }

    /**
     * Helper Method to build parameter list in SQL Statement.
     */
    private static String buildParamPart(int paramCount) {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        boolean bFirst = true;
        for (int i = 0; i < paramCount; i++) {
            if (bFirst) {
                bFirst = false;
            } else {
                builder.append(",");
            }
            builder.append("?");
        }
        builder.append(")");
        return builder.toString();
    }

    /**
     * Helper Method returns Type Name of SQL Data Type.
     */
    private static String getTypeNameOnly(SQLData struct) throws SQLException {
        String fullName = struct.getSQLTypeName();
        int dotIndex = fullName.indexOf(".");
        if (-1 == dotIndex)
            return fullName;
        return fullName.substring(dotIndex + 1);
    }
}
