package com.ross.rdm.common.utils.model.util.dbcall.customTypes;

public abstract interface RDMDbSchemaResolver {
    public abstract String resolveObjectName(String paramString);
}
