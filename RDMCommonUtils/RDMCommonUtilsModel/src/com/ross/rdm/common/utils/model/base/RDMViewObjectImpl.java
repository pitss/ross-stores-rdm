package com.ross.rdm.common.utils.model.base;

import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;

import java.util.ArrayList;
import java.util.List;

import oracle.jbo.AttributeDef;
import oracle.jbo.server.ViewAttributeDefImpl;
import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewObjectImpl;

/**
 * Custom ADF Framework Extension Class for RDMViewObjectImpl.
 */
public class RDMViewObjectImpl extends ViewObjectImpl {
    public static final String EMPTY_STRING = "";
    public static final String RAISE_FAILURE =  "RAISE_FAILURE####";
    public static final String ERROR = "E";
    public static final String YES = "Y";
    public RDMViewObjectImpl() {
        super();
    }

    public RDMViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }

    public void clearBlock() {
        if (this.getCurrentRow() != null) {
            int count = 0;
            ViewAttributeDefImpl[] attrDefs = this.getViewAttributeDefImpls();

            for (ViewAttributeDefImpl attrDef : attrDefs) {
                byte attrKind = attrDefs[count].getAttributeKind();
                if (attrKind != AttributeDef.ATTR_ASSOCIATED_ROW &&
                    attrKind != AttributeDef.ATTR_ASSOCIATED_ROWITERATOR) {
                    String columnName = attrDef.getName();
                    this.getCurrentRow().setAttribute(columnName, null);
                }
            }
        }
    }
    
    
    public List<String> processMessagesParams(SQLOutParam p_v_return, SQLOutParam p_msg_display)
    {
        List<String> errors = null;
        if(p_v_return.getWrappedData() != null && !RAISE_FAILURE.equals(p_v_return.getWrappedData())){
            errors = new ArrayList<String>();
            // p_msg_display -> type
            // p_v_return -> msg
            if(((String)p_v_return.getWrappedData()).contains(RAISE_FAILURE)){
                errors.add(ERROR);
                String error = (String) p_v_return.getWrappedData();
                error = error.replace(RAISE_FAILURE, EMPTY_STRING);
                errors.add(error);
            }
            else{               
                errors.add(p_msg_display != null ? (String) p_msg_display.getWrappedData() : ERROR);
                errors.add((String) p_v_return.getWrappedData());
            }
        }
        return errors;
    }
}
