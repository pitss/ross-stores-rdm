package com.ross.rdm.common.utils.model.util.dbcall;


import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class SQLBridge implements SQLData {
  private static SQLData internal = null;
  
  public static void Register(SQLData param) {
    internal = param;
  }

  public String getSQLTypeName() {
    return null;
  }

  public void readSQL(SQLInput stream, String typeName)
                          throws SQLException {
    internal.readSQL(stream, typeName);
  }

  public void writeSQL(SQLOutput stream) {
  }
}