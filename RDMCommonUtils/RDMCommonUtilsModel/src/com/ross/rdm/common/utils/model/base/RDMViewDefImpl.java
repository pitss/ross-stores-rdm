package com.ross.rdm.common.utils.model.base;

import oracle.jbo.server.ViewDefImpl;

/**
 * Custom ADF Framework Extension Class for RDMViewDefImpl.
 */
public class RDMViewDefImpl extends ViewDefImpl {

      public RDMViewDefImpl() {
        super();
    }
      public RDMViewDefImpl(int defScope, String name) {
        super(defScope, name);
    }
      public RDMViewDefImpl(int defScope, String name, String baseViewDefName) {
        super(defScope, name, baseViewDefName);
    }
      public RDMViewDefImpl(String name) {
        super(name);
    }
}
