package com.ross.rdm.common.utils.model.base;

import oracle.jbo.RowInconsistentException;
import oracle.jbo.server.TransactionEvent;

/**
 * Custom ADF Framework Extension Class for PlsqlEntityImpl.
 */
public class PlsqlEntityImpl extends RDMEntityImpl {

      public PlsqlEntityImpl() {
        super();
    }
  /**
 * Override methods in a subclass to change the default processing
 */
      protected void doDML(int operation, TransactionEvent transactionEvent) {
        //super.doDML(operation, transactionEvent);
        if ( operation == DML_INSERT )
            callInsertProcedure(transactionEvent);
        else if ( operation == DML_UPDATE )
            callUpdateProcedure(transactionEvent);
        else if ( operation == DML_DELETE )
            callDeleteProcedure(transactionEvent);
    }
  /**
 * Override in a subclass to change the default processing
 */
      protected void callInsertProcedure(TransactionEvent transactionEvent) {
        super.doDML(DML_INSERT, transactionEvent);
    }
  /**
 * Override in a subclass to change the default processing
 */
      protected void callUpdateProcedure(TransactionEvent transactionEvent) {
        super.doDML(DML_UPDATE, transactionEvent);
    }
  /**
 * Override in a subclass to change the default processing
 */
      protected void callDeleteProcedure(TransactionEvent transactionEvent) {
        super.doDML(DML_DELETE, transactionEvent);
    }
  /**
 * Override methods in a subclass to change the default lock processing
 */
      protected void doSelect(boolean lock) {
        //super.doSelect(b);
        if ( lock ) {
            callLockProcedureAndCheckForRowConsistency();
        } else {
            callSelectProcedure();
        }
    }
  /**
 * Override in a subclass to call lock procedure and check consistency
 * Lock a Row by Primary Key
 */
      protected void callLockProcedureAndCheckForRowConsistency() {
        super.doSelect(true);
    }
  /**
 * Select a Row by Primary Key 
 */
      protected void callSelectProcedure() {
        super.doSelect(false);
    }
  /**
 * Helper method for old-value versus new-value attribute comparison 
 */
      protected void compareOldAttrTo(int attrIndex, Object newVal) {
      if ((getPostedAttribute(attrIndex) == null && newVal != null) || 
          (getPostedAttribute(attrIndex) != null && newVal == null) || 
          (getPostedAttribute(attrIndex) != null && newVal != null && 
           !getPostedAttribute(attrIndex).equals(newVal))) {
        throw new RowInconsistentException(getKey());
      }
    }
}
