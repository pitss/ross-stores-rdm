package com.ross.rdm.common.utils.model.util.dbcall.customTypes;

public class RDMDbSchemaResolverFactory {
    public static RDMDbSchemaResolver getResolver() {
        String customResolverClassName = System.getProperty("raf.dbschema.resolver.custom.impl");
        if ((customResolverClassName != null) && (!customResolverClassName.isEmpty())) {
            try {
                Class c = Class.forName(customResolverClassName);
                return (RDMDbSchemaResolver) c.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return new RDMDefaultDbSchemaResolverImpl();
    }
}
