package com.ross.rdm.processing.hhworkprocessings.model.views;

import com.ross.rdm.common.model.entities.ContainerImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.sql.Date;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Apr 28 12:08:19 CEST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ContainerRowImpl extends RDMViewRowImpl {

    public static final int ENTITY_CONTAINER = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        FacilityId,
        ContainerId,
        ReceiptDate;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int FACILITYID = AttributesEnum.FacilityId.index();
    public static final int CONTAINERID = AttributesEnum.ContainerId.index();
    public static final int RECEIPTDATE = AttributesEnum.ReceiptDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ContainerRowImpl() {
    }

    /**
     * Gets Container entity object.
     * @return the Container
     */
    public ContainerImpl getContainer() {
        return (ContainerImpl) getEntity(ENTITY_CONTAINER);
    }

    /**
     * Gets the attribute value for RECEIPT_DATE using the alias name ReceiptDate.
     * @return the RECEIPT_DATE
     */
    public Date getReceiptDate() {
        return (Date) getAttributeInternal(RECEIPTDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for RECEIPT_DATE using the alias name ReceiptDate.
     * @param value value to set the RECEIPT_DATE
     */
    public void setReceiptDate(Date value) {
        setAttributeInternal(RECEIPTDATE, value);
    }

    /**
     * Gets the attribute value for FACILITY_ID using the alias name FacilityId.
     * @return the FACILITY_ID
     */
    public String getFacilityId() {
        return (String) getAttributeInternal(FACILITYID);
    }

    /**
     * Sets <code>value</code> as attribute value for FACILITY_ID using the alias name FacilityId.
     * @param value value to set the FACILITY_ID
     */
    public void setFacilityId(String value) {
        setAttributeInternal(FACILITYID, value);
    }

    /**
     * Gets the attribute value for CONTAINER_ID using the alias name ContainerId.
     * @return the CONTAINER_ID
     */
    public String getContainerId() {
        return (String) getAttributeInternal(CONTAINERID);
    }

    /**
     * Sets <code>value</code> as attribute value for CONTAINER_ID using the alias name ContainerId.
     * @param value value to set the CONTAINER_ID
     */
    public void setContainerId(String value) {
        setAttributeInternal(CONTAINERID, value);
    }
}

