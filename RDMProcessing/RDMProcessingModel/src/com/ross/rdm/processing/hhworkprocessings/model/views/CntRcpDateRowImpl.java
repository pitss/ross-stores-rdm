package com.ross.rdm.processing.hhworkprocessings.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.sql.Date;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Apr 28 12:06:18 CEST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CntRcpDateRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ReceiptDate,
        Act;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int RECEIPTDATE = AttributesEnum.ReceiptDate.index();
    public static final int ACT = AttributesEnum.Act.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CntRcpDateRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute ReceiptDate.
     * @return the ReceiptDate
     */
    public Date getReceiptDate() {
        return (Date) getAttributeInternal(RECEIPTDATE);
    }

    /**
     * Gets the attribute value for the calculated attribute Act.
     * @return the Act
     */
    public Date getAct() {
        return (Date) getAttributeInternal(ACT);
    }
}

