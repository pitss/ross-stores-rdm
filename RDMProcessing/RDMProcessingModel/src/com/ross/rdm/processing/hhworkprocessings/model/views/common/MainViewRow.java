package com.ross.rdm.processing.hhworkprocessings.model.views.common;

import oracle.jbo.Row;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Apr 29 15:30:10 CEST 2016
// ---------------------------------------------------------------------
public interface MainViewRow extends Row {

    String startWork();

    String resumeWork();

    String completeWork();

    String pauseWork();
}

