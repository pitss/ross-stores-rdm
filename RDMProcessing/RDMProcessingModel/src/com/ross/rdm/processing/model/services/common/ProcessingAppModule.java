package com.ross.rdm.processing.model.services.common;

import java.util.List;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jan 20 13:30:41 CET 2016
// ---------------------------------------------------------------------
public interface ProcessingAppModule extends ApplicationModule {
    void clearRossEventcodeUseridTmp();

    void setAhlInfo(String facilityId, String currentForm);

    void updateRossEventcodeUseridTmp(String user, String locId);

    void initMarkWipManagement();


    List callStartUpLocalSecurityMarkWipManagement();

    String callCheckScreenOptionPrivMarkWip(String optionName);

    void initProcessWip();
}

