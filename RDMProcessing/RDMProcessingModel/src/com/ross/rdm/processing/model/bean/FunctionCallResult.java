package com.ross.rdm.processing.model.bean;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class FunctionCallResult implements Serializable {
    @SuppressWarnings("compatibility:-6373263318220008991")
    private static final long serialVersionUID = 1L;
    private List messages;
    private String setItemProperty;
    private Boolean goNextItem;

    public FunctionCallResult() {
        super();
        this.messages = new ArrayList();
        this.setItemProperty = "N";
        this.goNextItem = Boolean.valueOf(true);
    }

    public void setMessages(List messages) {
        this.messages = messages;
    }

    public List getMessages() {
        return messages;
    }

    public void setSetItemProperty(String setItemProperty) {
        this.setItemProperty = setItemProperty;
    }

    public String getSetItemProperty() {
        return setItemProperty;
    }

    public void setGoNextItem(Boolean goNextItem) {
        this.goNextItem = goNextItem;
    }

    public Boolean getGoNextItem() {
        return goNextItem;
    }
}
