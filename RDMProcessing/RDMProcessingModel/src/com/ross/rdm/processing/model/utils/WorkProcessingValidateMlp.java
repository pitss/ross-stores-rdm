package com.ross.rdm.processing.model.utils;

import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;

import java.sql.Types;

import oracle.jbo.server.DBTransaction;

/**
 * Helper class to hold out parameter values and stored function return value
 * for processing by the ADF VO multistage APIs
 */
public class WorkProcessingValidateMlp {
    
    private SQLOutParam io_mlp; // container.container_id
    private SQLOutParam o_item_id = new SQLOutParam("", Types.VARCHAR); // item_master.item_id
    private SQLOutParam o_mlp_total_units = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.units_worked
    private SQLOutParam o_mlp_total_cont = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.container_worked
    private SQLOutParam o_worked_units = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.units_worked
    private SQLOutParam o_worked_cont = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.container_worked
    private SQLOutParam o_remaining_units = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.units_remaining
    private SQLOutParam o_remaining_cont = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.container_remaining
    private SQLOutParam o_prev_work_status = new SQLOutParam("", Types.VARCHAR); // work_activity_audit.status
    private SQLOutParam o_ppk_qty = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.prepack_qty
    private SQLOutParam o_nested_qty = new SQLOutParam(0, Types.INTEGER); // work_activity_audit.nested_qty
    private SQLOutParam o_zone = new SQLOutParam("", Types.VARCHAR); // location.zone
    private SQLOutParam o_wh_id = new SQLOutParam("", Types.VARCHAR); // wh.wh_id
    private SQLOutParam o_dc_id = new SQLOutParam("", Types.VARCHAR); // dc.dc_id
    private SQLOutParam o_location_id = new SQLOutParam("", Types.VARCHAR); // location.location_id
    
    private SQLOutParam o_stand_alone_cnt = new SQLOutParam("N", Types.VARCHAR);
    private SQLOutParam o_non_unq_cnt_qty = new SQLOutParam("", Types.VARCHAR); 
    private SQLOutParam o_prev_operator_id = new SQLOutParam("", Types.VARCHAR);
    private SQLOutParam o_supervisor_id = new SQLOutParam("", Types.VARCHAR);
    private SQLOutParam o_unq_qty = new SQLOutParam("", Types.VARCHAR);
    private SQLOutParam o_error_msg = new SQLOutParam("", Types.VARCHAR);
    private SQLOutParam o_error_msg_typ = new SQLOutParam("", Types.VARCHAR);
    
    private int result;
    
    private String facilityId;
    private String activityCd;
    private String operatorId;
    
    public WorkProcessingValidateMlp(String facilityId, String activityCd, String operatorId) {
        super();
        this.facilityId = facilityId;
        this.activityCd = activityCd;
        this.operatorId = operatorId;
    }
    
    public void executeStoredUnit(DBTransaction transaction, String facilityId, String containerId,
                                  String activityCd, String user) {
        io_mlp = new SQLOutParam(containerId);
        result = (Integer)DBUtils.callStoredFunction(transaction, Types.INTEGER, "PKG_PROCESSING_ADF.VALIDATE_MLP",
                                                            facilityId, io_mlp,
                                                            o_item_id, o_mlp_total_units, o_mlp_total_cont, o_worked_units,
                                                            o_worked_cont, o_remaining_units, o_remaining_cont, o_prev_work_status,
                                                            o_ppk_qty, o_nested_qty, o_zone, o_wh_id, o_dc_id, o_location_id, 
                                                            activityCd, "hh_work_processing_s", user,
                                                            o_stand_alone_cnt, o_non_unq_cnt_qty, o_prev_operator_id,
                                                            o_supervisor_id, o_unq_qty, o_error_msg, o_error_msg_typ);
    }
    
    public String getFacilityId() {
        return facilityId;
    }
    
    public String getActivityCd() {
        return activityCd;
    }
    
    public String getOperatorId() {
        return operatorId;
    }
        
    public int getResult() {
        return result;
    }
    
    public boolean hasMoreData() {        
        return result-- > 0;
    }
    
    public String getMlp() {
        return (String)io_mlp.getWrappedData();
    }
    
    public String getItemId() {
        return (String)o_item_id.getWrappedData();
    }
    
    public Integer getMlpTotalUnits() {
        return (Integer)o_mlp_total_units.getWrappedData();
    }
    
    public Integer getMlpTotalCont() {
        return (Integer)o_mlp_total_cont.getWrappedData();
    }
    
    public Integer getWorkedUnits() {
        return (Integer)o_worked_units.getWrappedData();
    }
    
    public Integer getWorkedCont() {
        return (Integer)o_worked_cont.getWrappedData();
    }
    
    public Integer getRemainingUnits() {
        return (Integer)o_remaining_units.getWrappedData();
    }
    
    public Integer getRemainingCont() {
        return (Integer)o_remaining_cont.getWrappedData();
    }
    
    public String getPrevWorkStatus() {
        return (String)o_prev_work_status.getWrappedData();
    }
    
    public Integer getPpkQty() {
        return (Integer)o_ppk_qty.getWrappedData();
    }
    
    public Integer getNestedQty() {
        return (Integer)o_nested_qty.getWrappedData();
    }
    
    public String getZone() {
        return (String)o_zone.getWrappedData();
    }
    
    public String getWhId() {
        return (String)o_wh_id.getWrappedData();
    }
    
    public String getDcId() {
        return (String)o_dc_id.getWrappedData();
    }
    
    public String getLocationId() {
        return (String)o_location_id.getWrappedData();
    }
    
    public String getStandAloneCnt() {
        return (String)o_stand_alone_cnt.getWrappedData();
    }
    
    public String getNonUnqCntQty() {
        return (String)o_non_unq_cnt_qty.getWrappedData();
    }
    
    public String getPrevOperatorId() {
        return (String)o_prev_operator_id.getWrappedData();
    }
    
    public String getSupervisorId() {
        return (String)o_supervisor_id.getWrappedData();
    }
    
    public String getUnqQty() {
        return (String)o_unq_qty.getWrappedData();
    }
    
    public String getErrorMsg() {
        return (String)o_error_msg.getWrappedData();
    }
    
    public String getErrorMsgTyp() {
        return (String)o_error_msg_typ.getWrappedData();
    }
}
