package com.ross.rdm.processing.hhprocesswips.view.bean;

import com.ross.rdm.common.view.framework.ErrorMessageModelBase;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------

public class HhProcessWipSBean extends ErrorMessageModelBase  {
    
    // container ID user input
    private String containerId;
    // true -> INSTRUCTIONS, false -> PERSONALIZATIONS
    private boolean instruction;
    
    // ADF quirk workaround: empty fields backing
    private String emptyField;
    
    private String page1Focus;

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerId() {
        return containerId;
    }
    
    public void setInstruction(boolean instruction) {
        this.instruction = instruction;
    }

    public boolean isInstruction() {
        return instruction;
    }

    public void setEmptyField(String emptyField) {
        this.emptyField = emptyField;
    }

    public String getEmptyField() {
        return emptyField;
    }

    public void setPage1Focus(String page1Focus) {
        this.page1Focus = page1Focus;
    }

    public String getPage1Focus() {
        return page1Focus;
    }
}
