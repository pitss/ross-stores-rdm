package com.ross.rdm.processing.hhprocesswips.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.processing.view.framework.RDMProcessingBackingBean;

import javax.faces.event.ActionEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page2.jspx
// ---    
// ---------------------------------------------------------------------
public class Page2Backing extends RDMProcessingBackingBean {
  
    private static ADFLogger logger = ADFLogger.createADFLogger(Page2Backing.class);
    
    private RichIcon iconErrorMessage;
    private RichOutputText textErrorMessage;

  public Page2Backing() {
  }
  
    public void nextAction(ActionEvent actionEvent) { 
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_INST_PRSNLZ_F8,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    if (isOperationEnabled("Next")) {
                        executeDerivedOperation("Next");
                    } else { // cycle through to the first detail again            
                        if (isOperationEnabled("First")) {
                            executeDerivedOperation("First");
                        }
                    } 
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        
        
      
    }
    
    /**
     * If the given operation is enabled, we know there are data we can navigato to.
     * Need to differentiate between inbound/outbound data sets, depending on what
     * are we showing to the user.
     */
    private boolean isOperationEnabled(String name) {        
        return (Boolean)JSFUtils.getExpressionValue("#{bindings." + getDerivedOperation(name) + ".enabled}");        
    }
    
    private void executeDerivedOperation(String name) {
        String operationName = getDerivedOperation(name);
        logger.finest("Execution operation '" + operationName + "' ...");
        executeOperation(operationName);
    }
    
    /**
     *  Calculate the name of the operation binding, depending on the status of the container
     *  currently being processed. (inbound/outbound)
     */
    private String getDerivedOperation(String operation) {
        return operation + (isInbound() ? "In" : "Out");
    }
    
    /**
     *  Is the current container inbound or outbound?     
     */
    private boolean isInbound() {
        String containerStatus = (String)ADFUtils.getBoundAttributeValue("ContainerStatus");
        return "I".equals(containerStatus);
    }

    public String backAction() {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_INST_PRSNLZ_F3,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    return "Back";
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        // Add event code here...
        return null;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setTextErrorMessage(RichOutputText textErrorMessage) {
        this.textErrorMessage = textErrorMessage;
    }

    public RichOutputText getTextErrorMessage() {
        return textErrorMessage;
    }
    
    @Override
    protected void showMessage(String type, String code, RichInputText input) {
        getIconErrorMessage().setName("W".equals(type) ? "warning" : "error");
        getTextErrorMessage().setValue(getMessage(code, null, null, null));
        refreshContentOfUIComponent(getAllMessagesPanel());
    }
}
