package com.ross.rdm.processing.hhprocesswips.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.processing.hhworkprocessings.view.bean.WorkProcessingBaseBean;

import java.sql.Date;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends WorkProcessingBaseBean {

    //private static final String FOCUS_START = "sta1";
    private static final String FOCUS_POPUP = "POPUP";
    private static final String FOCUS_CONTAINER = null;
    private RichInputText containerIdCtrl;
    //private RichInputText startTsCtrl;
    private RichPopup confirmationPopup;
    private RichPopup wipFinishedPopup;
    private RichLink yesLinkPopup;

    private static ADFLogger logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichIcon iconContainerId;
    //private RichIcon iconStartTs;
    private RichIcon iconErrorMessage;
    private RichOutputText textErrorMessage;
    private RichPanelFormLayout mainPanel;

    public Page1Backing() {
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        hideMessages();
        clearBlock();
        updateModel(vce);
        String containerId = (String) vce.getNewValue();
        if (null != containerId && containerId.length() > 0) {
            if (!executeOperation("checkContainer")) {
                logger.severe("'checkContainer' execution failed!");
                return;
            }
            String result = (String) getOperationResult("checkContainer");
            if (null != result) {
                showMessage("E", result, containerIdCtrl);
                this.setContainerIcon("E");
                //this.setStartTsIcon(null);
                return;
            }
            if (!loadData(containerId))
                return;
            if (!hasData()) {
                clearBlock();
                showMessage("M", "NO_WIP_CONT", containerIdCtrl);
                this.setContainerIcon("E");
                //this.setStartTsIcon(null);
                return;
            } else {
                boolean isGiftWip = (Boolean) ADFUtils.getBoundAttributeValue("IsGiftCardWip");
                if (isGiftWip) {
                    clearBlock();
                    showMessage("E", "NEXT_WIP_GIFT", containerIdCtrl);
                    this.setContainerIcon("E");
                    //this.setStartTsIcon(null);
                    return;
                }

                boolean isReturnWip = (Boolean) ADFUtils.getBoundAttributeValue("IsReturnWip");
                if (isReturnWip) {
                    clearBlock();
                    showMessage("E", "NEXT_WIP_RET", containerIdCtrl);
                    this.setContainerIcon("E");
                    //this.setStartTsIcon(null);
                    return;
                }
            }
        } else {
            showMessage("E", "INV_CONTAINER", containerIdCtrl);
            this.setContainerIcon("E");
            //this.setStartTsIcon(null);
            return;
        }
        switchFocusTo(FOCUS_CONTAINER);
    }

    private void clearBlock() {
        logger.finest("Clearing block...");
        executeOperation("clearData");
        HhProcessWipSBean pageFlow = getPageFlowBean();
        pageFlow.setContainerId(null);
        this.setContainerIcon(null);
        //this.setStartTsIcon(null);
        switchFocusTo(FOCUS_CONTAINER);
        this.refreshContentOfUIComponent(this.getMainPanel());
    }

    private boolean loadData(String containerId) {
          ///////////////////////
         /// OTM ISSUE: 3371 ///
        ///////////////////////
        /* if (!executeOperation("ExecuteWithParams")) {
            logger.severe("'ExecuteWithParams' execution failed!");
            return false;
        }
        return true; */
        boolean containerLoaded = false;
        if(containerId != null){
            containerLoaded = true;
            OperationBinding op = ADFUtils.findOperation("loadData");
            op.getParamsMap().put("containerId", containerId);        
            op.execute();
            List errors = op.getErrors();
            if(errors != null && !errors.isEmpty()){
                String msg = "";
                for (Object error : errors) {
                    if (error != null) {
                        msg = msg.concat(error.toString() + " ");
                    }
                }
                if (StringUtils.isNotEmpty(msg)) {
                    showMessageRaw("E", msg, containerIdCtrl);
                    containerLoaded = false;
                }
            }
        }
        this.refreshContentOfUIComponent(this.getMainPanel());
        return containerLoaded;
    }

    @Override
    protected void hideMessages() {
        super.hideMessages();
        removeErrorStyleToComponent(containerIdCtrl);
    }

    private boolean hasData() {
        return null != ADFUtils.getBoundAttributeValue("ContainerIdWip");
    }

    public void prevAction(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F6,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    if (!hasData())
                        return;
                    logger.finest("Looking for previous WIP...");
                    hideMessages();
                    boolean hasPrev = (Boolean) JSFUtils.getExpressionValue("#{bindings.Previous.enabled}");
                    if (!hasPrev) {
                        showMessage("M", "NO_WIP_PREV", null);
                        this.setContainerIcon(null);
                        //this.setStartTsIcon(null);
                        return;
                    }
                    executeOperation("Previous");
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        
       
    }

    public void nextAction(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F5,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    if (!hasData())
                        return;
                    logger.finest("Looking for next WIP...");
                    hideMessages();
                    boolean hasNext = (Boolean) JSFUtils.getExpressionValue("#{bindings.Next.enabled}");
                    if (!hasNext) {
                        showMessage("M", "NO_WIP_NEXT", null);
                        this.setContainerIcon(null);
                        //this.setStartTsIcon(null);
                        return;
                    }
                    executeOperation("Next");
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        
       
    }

    public void instrAction(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F7,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    if (!hasData())
                        return;
                    logger.finest("Looking for INSTRUCTIONS...");
                    hideMessages();
                    String containerStatus = (String) ADFUtils.getBoundAttributeValue("ContainerStatus");
                    if ("I".equals(containerStatus)) {
                        executeOperation("ExecuteInboundWithParams");
                    } else if ("D".equals(containerStatus)) {
                        executeOperation("ExecuteOutboundWithParams");
                    }
                    navigateToAction("Detail");
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        
       
    }

    public void prsnlAction(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F8,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    if (!hasData())
                        return;
                    logger.finest("Looking for PERSONALIZATIONS...");
                    hideMessages();
                    String containerStatus = (String) ADFUtils.getBoundAttributeValue("ContainerStatus");
                    if ("I".equals(containerStatus)) {
                        executeOperation("noResults");
                    } else if ("D".equals(containerStatus)) {
                        executeOperation("ExecuteOutboundWithParams");
                    }
                    navigateToAction("Detail");
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        
       
    }

    private void reportError(String type, String errorCode) {
        executeOperation("Rollback");
        showMessage(type, errorCode, null);
        this.setContainerIcon(null);
        //this.setStartTsIcon(null);
    }

    private void startTs() {
        setTs();
        logger.finest("Commiting...");
        executeOperation("Commit");
    }

    private void setTs() {
        logger.finest("Setting StartTS ...");
        ADFUtils.setBoundAttributeValue("StartTs", new Date((new java.util.Date()).getTime()));
    }

    private boolean executeWipOperation(String name) {
        executeOperation(name);
        String result = (String) getOperationResult(name);
        if (null != result) {
            reportError("M", result);
            return false;
        }
        return true;
    }

    private boolean wipStarted() {
        // Check and see if WIP has already been started
        if (null != ADFUtils.getBoundAttributeValue("StartTs")) {
            showMessage("E", "WIP_STARTED", null);
            this.setContainerIcon(null);
            //this.setStartTsIcon(null);
            return true;
        }
        return false;
    }

    private void startWip(String startWipTr) {
          ///////////////////////
         /// OTM ISSUE: 2871 ///
        ///////////////////////
        
        /* if (wipStarted())
            return;
        if (!executeWipOperation("startContainerWip"))
            return;
        executeOperation("Commit");
        loadData(); */
        
        OperationBinding op = ADFUtils.findOperation("callStartWip");
        op.getParamsMap().put("startWipTr", startWipTr);        
        op.getParamsMap().put("userId", JSFUtils.resolveExpression("#{CurrentUser.userId}"));
        List<String> result = (List<String>) op.execute();
        if(op.getErrors() == null || op.getErrors().isEmpty()){
            if(result != null){
                showMessageRaw(result.get(0), result.get(1), null);
            }
            else{
                loadData(this.getPageFlowBean().getContainerId());
            }
        }
    }

    public void startWipAction(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F1,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                          if (!hasData())
                              return;
                          logger.finest("Starting WIP...");
                          hideMessages();
                          // Check if the container_id entered is an MLP.
                          boolean isMlp = Boolean.TRUE.equals(ADFUtils.getBoundAttributeValue("IsMlp"));
                          if (isMlp) { // If the container_id entered is an MLP, the user is asked to start the WIP on all LPNs.
                              switchFocusTo(FOCUS_POPUP);
                              askUser(confirmationPopup, false, "START_WIP_MLP", getBeanListener("startWipMlpListener"));
                          } else { // The container_id entered is not an MLP.
                              String masterContainerId = (String) ADFUtils.getBoundAttributeValue("MasterContainerId");
                              if (!"NONE".equals(masterContainerId)) { // If the LPN is not stand alone, the user is asked to start the WIP on all LPNs on the MLP.
                                  switchFocusTo(FOCUS_POPUP);
                                  askUser(confirmationPopup, true, "LPN_ON_MLP_H", getBeanListener("startLpnOnMlpHListener"),
                                          masterContainerId);
                              } else { // If the LPN is stand alone then start the WIP.
                                  startWip(null);
                              }
                          }   
                      }else{
                          this.showMessage("E", "NOT_ALLOWED", null);
                      }
        
        
    }

    public void finishWipAction(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F4,RoleBasedAccessConstants.FORM_NAME_hh_process_wip_s)){
                    if (!hasData())
                        return;
                    logger.finest("Finishing WIP...");
                    hideMessages();
                    
                      ///////////////////////
                     /// OTM ISSUE: 2872 ///
                    ///////////////////////
                    /* if (!executeOperation("updateRossEventcodeUseridTmp")) {
                        logger.severe("Failed executing 'updateRossEventcodeUseridTmp'!");
                        return;
                    }
                    if (null == ADFUtils.getBoundAttributeValue("WipCode")) {
                        clearBlock();
                        reportError("M", "NO_WIP_CONT");
                        return;
                    }
                    if (null == ADFUtils.getBoundAttributeValue("StartTs")) {
                        setTs();
                    } */

                    // Check if the container_id entered is an MLP.
                    boolean isMlp = Boolean.TRUE.equals(ADFUtils.getBoundAttributeValue("IsMlp"));
                    if (isMlp) { // If the container_id entered is an MLP, the user is asked to start the WIP on all LPNs.
                        switchFocusTo(FOCUS_POPUP);
                        askUser(confirmationPopup, false, "FINISH_WIP_MLP", getBeanListener("finishWipMlpListener"));
                    } else { // The container_id entered is not an MLP.
                        String masterContainerId = (String) ADFUtils.getBoundAttributeValue("MasterContainerId");
                        if (!"NONE".equals(masterContainerId)) { // If the LPN is not stand alone, the user is asked to finish the WIP on all LPNs on the MLP.
                            switchFocusTo(FOCUS_POPUP);
                            askUser(confirmationPopup, true, "LPN_ON_MLP_H", getBeanListener("finishLpnOnMlpHListener"),
                                    masterContainerId);
                        } else { 
                            // If the LPN is stand alone then finish the WIP.
                            finishContainerWip(null);
                        }
                    } 
                }else{
                    this.showMessage("E", "NOT_ALLOWED", null);
                }
        
       
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // startWip confirmation listeners
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void startLpnOnMlpHListener(DialogEvent dialogEvent) {
        askUser(confirmationPopup, false, "START_WIPTR", getBeanListener("startWiptrListener"));
    }

    public void startWipMlpListener(DialogEvent dialogEvent) {
        switchFocusTo(FOCUS_CONTAINER);
        if (!dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) { // If the user selects N then clear the block.
            clearBlock();
        } else { // If the user selects Y then loop through LPNs on MLP.
              ///////////////////////
             /// OTM ISSUE: 2871 ///
            ///////////////////////
            /* if (!executeWipOperation("processLpns"))
                return;
            refreshChangedRows();
            startTs(); */
            startWip(null);
        }
    }

    public void startWiptrListener(DialogEvent dialogEvent) {
        switchFocusTo(FOCUS_CONTAINER);
        if (!dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) { // If the user selects N then ask if the WIP on the LPN entered will be started.
            switchFocusTo(FOCUS_POPUP);
            askUser(confirmationPopup, false, "START_WIP_1_LPN", getBeanListener("startWip1LpnListener"));
        } else { // If the user selects Y, then the WIP is started on all LPNs on MLP. Loop through all LPNs.            
              ///////////////////////
             /// OTM ISSUE: 2871 ///
            ///////////////////////
            /* if (!executeWipOperation("processMasterLpns"))
                return;
            refreshChangedRows();
            startTs(); */
            startWip("Y");
        }
    }

    public void startWip1LpnListener(DialogEvent dialogEvent) {
        switchFocusTo(FOCUS_CONTAINER);
        if (!dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) { // If the user selects N then clear the block.
            clearBlock();
        } else { 
            // If the user selects Y then start the WIP on LPN.
              ///////////////////////
             /// OTM ISSUE: 2871 ///
            ///////////////////////
            startWip("N");
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // finishWip confirmation listeners
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void finishLpnOnMlpHListener(DialogEvent dialogEvent) {
        // The user is asked if the WIP on all LPNs will be finished.
        askUser(confirmationPopup, false, "FIN_WIPTR", getBeanListener("finishFinWiptrListener"));
    }

    public void finishWipMlpListener(DialogEvent dialogEvent) {
        switchFocusTo(FOCUS_CONTAINER);
        if (!dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) { // If the user selects N then clear the block.
            clearBlock();
        } else { 
            // If the user selects Y then loop through LPNs on MLP.
              ///////////////////////
             /// OTM ISSUE: 2872 ///
            ///////////////////////
            /* if (!executeWipOperation("finishLpns"))
                return; 
            finalizeWip(true);*/
            this.finishContainerWip(null);
        }
    }

    public void finishFinWiptrListener(DialogEvent dialogEvent) {
        switchFocusTo(FOCUS_CONTAINER);
        if (!dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) { // If the user selects N then ask if the WIP on the LPN entered will be finished.
            switchFocusTo(FOCUS_POPUP);
            askUser(confirmationPopup, false, "FIN_WIP_1_LPN", getBeanListener("finishFinWip1LpnListener"));
        } else { 
            // If the user selects Y, then the WIP is finished on all LPNs on MLP. Loop through all LPNs.
              ///////////////////////
             /// OTM ISSUE: 2872 ///
            ///////////////////////
            /* if (!executeWipOperation("finishMasterLpns"))
                return; 
            finalizeWip(true);*/
            this.finishContainerWip("Y");
        }
    }

    public void finishFinWip1LpnListener(DialogEvent dialogEvent) {
        switchFocusTo(FOCUS_CONTAINER);
        if (!dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) { // If the user selects N then clear the block.
            clearBlock();
        } else { 
            // If the user selects Y then finish the WIP on MLP.
              ///////////////////////
             /// OTM ISSUE: 2872 ///
            ///////////////////////
            finishContainerWip("N");
        }
    }

    private void finishContainerWip(String finWipTr) {
         ///////////////////////
         /// OTM ISSUE: 2872 ///
        ///////////////////////
        /* if (!executeWipOperation("finishContainerWip")) {
            logger.severe("Execution of 'finishContainerWip' failed!");
            return;
        }
        finalizeWip(false); */
        
        OperationBinding op = ADFUtils.findOperation("callFinishWip");
        op.getParamsMap().put("finWipTr", finWipTr);        
        op.getParamsMap().put("userId", JSFUtils.resolveExpression("#{CurrentUser.userId}"));
        List<String> result = (List<String>) op.execute();
        if(op.getErrors() == null || op.getErrors().isEmpty()){
            if(result != null){
                if (result.size() == 2) {
                    showMessageRaw(result.get(0), result.get(1), null);
                }
                else{
                    showMessageRaw(result.get(0), result.get(1), containerIdCtrl);
                    this.setContainerIcon("E");
                    //this.setStartTsIcon(null);
                }
            }
            else{
                // OTM ISSUE 2650
                this.callDisplayWipBlock();
            }
        }
    }
    
    @Deprecated
    private void finalizeWip(boolean refresh) {
        if (!executeOperation("createLaborProductivity")) {
            logger.severe("Execution of 'createLaborProductivity' failed!");
            return;
        }
        String result = (String) getOperationResult("createLaborProductivity");
        if (null != result) {
            logger.severe("Inserting LABOR_PRODUCTIVITY failed: " + result);
            return;
        }

        if (refresh) { // workaround database changes
            refreshChangedRows();
        }

        if (!executeOperation("Commit")) {
            logger.severe("Execution of 'Commit' failed!");
            return;
        }
        
        // OTM ISSUE 2650
        this.callDisplayWipBlock();
    }
    
    private void callDisplayWipBlock(){
        OperationBinding op = ADFUtils.findOperation("callDisplayWipBlock");
        op.getParamsMap().put("containerId", this.getPageFlowBean().getContainerId());
        op.getParamsMap().put("wipCode", ADFUtils.getBoundAttributeValue("WipCode"));
        op.getParamsMap().put("wipSeqNbr", ADFUtils.getBoundAttributeValue("WipSeqNbr"));
        op.getParamsMap().put("keyPressed", "F4");
        List<String> results = (List<String>) op.execute();
        if(op.getErrors() == null || op.getErrors().isEmpty()){
            if(results != null){
                if(results.size() == 1){
                    // OTM ISSUE 2649
                    this.setContainerIcon(null);
                    //this.setStartTsIcon(null);
                    ADFUtils.setBoundAttributeValue("StartTs", null);
                    switchFocusTo(FOCUS_POPUP);
                    this.getWipFinishedPopup().setLauncherVar("WIP_BLOCK.CONTAINER_ID");
                    this.getWipFinishedPopup().show(new RichPopup.PopupHints());
                }
                else if(results.size() == 2){                                        
                    showMessage(results.get(0), results.get(1), this.getContainerIdCtrl());
                    //this.setStartTsIcon(null);
                    this.setContainerIcon(null);
                    
                    this.getIconErrorMessage().setVisible(false);
                    this.getTextErrorMessage().setVisible(false);
                    this.refreshContentOfUIComponent(this.getAllMessagesPanel());
                    switchFocusTo(FOCUS_POPUP);
                    this.getWipFinishedPopup().setLauncherVar("WIP_BLOCK.CONTAINER_ID");
                    this.getWipFinishedPopup().show(new RichPopup.PopupHints());
                }
                else if(results.size() == 3){                    
                    if("WIP_BLOCK.CONTAINER_ID".equals(results.get(2))){
                        clearBlock();
                        showMessage(results.get(0), results.get(1), containerIdCtrl);
                    }
                    /* else{
                        showMessage(results.get(0), results.get(1), this.getStartTs());                        
                    } */
                    //this.setStartTsIcon(null);
                    this.setContainerIcon(null);
                    
                    this.getIconErrorMessage().setVisible(false);
                    this.getTextErrorMessage().setVisible(false);
                    this.refreshContentOfUIComponent(this.getAllMessagesPanel());
                    switchFocusTo(FOCUS_POPUP);
                    this.getWipFinishedPopup().setLauncherVar(results.get(2));
                    this.getWipFinishedPopup().show(new RichPopup.PopupHints());
                }
            }
            else{
                // OTM ISSUE 2649
                this.setContainerIcon(null);
                //this.setStartTsIcon(null);
                ADFUtils.setBoundAttributeValue("StartTs", null);
                switchFocusTo(FOCUS_POPUP);
                this.getWipFinishedPopup().setLauncherVar("WIP_BLOCK.CONTAINER_ID");
                this.getWipFinishedPopup().show(new RichPopup.PopupHints());
            }            
        }
    }
    
    private void setContainerIcon(String errorType){
        if("E".equals(errorType)){
            this.getIconContainerId().setName("error");
        }
        else if("W".equals(errorType)){
            this.getIconContainerId().setName("warning");
        }
        else{
            this.getIconContainerId().setName("required");
        }
        this.refreshContentOfUIComponent(this.getIconContainerId());
    }
    
    /* private void setStartTsIcon(String errorType){
        if("E".equals(errorType)){
            this.getIconStartTs().setName("error");
        }
        else if("W".equals(errorType)){
            this.getIconStartTs().setName("warning");
        }
        else{
            this.getIconStartTs().setName("required");
        }
        this.refreshContentOfUIComponent(this.getIconStartTs());
    } */

    private void clearEventcodes() {
        logger.finest("Clearing RossEventcodeUseridTmp ...");
        if (!executeOperation("clearRossEventcodeUseridTmp")) {
            logger.severe("Could not execute 'clearRossEventcodeUseridTmp'!");
            return;
        }
        if (!executeOperation("Commit")) {
            logger.severe("Execution of 'Commit' failed!");
            return;
        }
    }

    private void refreshChangedRows() {
        executeOperation("refreshFromDB");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected HhProcessWipSBean getPageFlowBean() {
        return (HhProcessWipSBean) getPageFlowBean("HhProcessWipSBean");
    }

    /**
     * A helper method to construct a bean-qualified method name.
     * We need the name of this bean as configured in the task flow.
     */
    private String getBeanListener(String methodName) {
        return "backingBeanScope.HhProcessWipSPage1Backing." + methodName;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    /* public void setStartTs(RichInputText startTs) {
        this.startTsCtrl = startTs;
    }

    public RichInputText getStartTs() {
        return startTsCtrl;
    } */

    public void setContainerIdCtrl(RichInputText containerIdCtrl) {
        this.containerIdCtrl = containerIdCtrl;
    }

    public RichInputText getContainerIdCtrl() {
        return containerIdCtrl;
    }

    private void switchFocusTo(String focus) {
        getPageFlowBean().setPage1Focus(focus);

        if (focus == null) {
            setFocusOnUIComponent(getContainerIdCtrl());
        //} else if (FOCUS_START.equals(focus)) {
            //setFocusOnUIComponent(getStartTs());
        }
        refreshContentOfUIComponent(getContainerIdCtrl());
        //refreshContentOfUIComponent(getStartTs());
    }
    
    /////////////////////
    // OTM ISSUE 2476  //
    /////////////////////
    /* public void onChangedStartTs(ValueChangeEvent vce) {
        this.updateModel(vce);
        switchFocusTo(FOCUS_CONTAINER);
    } */
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                /* if(FOCUS_CONTAINER.equals(item)){
                    switchFocusTo(FOCUS_CONTAINER);
                }
                else{ */
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        this.onChangedContainerId(new ValueChangeEvent(field, null, submittedVal));
                    }
                //}
            }
        }
    }
        
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    public String yesLinkWipFinishedPopupAction() {
        String itemId = this.getWipFinishedPopup().getLauncherVar();
        this.getWipFinishedPopup().hide();
        
        this.getIconErrorMessage().setVisible(true);
        this.getTextErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        if("WIP_BLOCK.CONTAINER_ID".equals(itemId)){
            if(this.getPageFlowBean().getMessageText() != null){
                this.setContainerIcon("E");
            }
            switchFocusTo(FOCUS_CONTAINER);            
        }
        else{
            if(this.getPageFlowBean().getMessageText() != null){
                //this.setStartTsIcon("E");
            }
            switchFocusTo(FOCUS_CONTAINER);
        }
        return null;
    }
    
    public void setWipFinishedPopup(RichPopup wipFinishedPopup) {
        this.wipFinishedPopup = wipFinishedPopup;
    }

    public RichPopup getWipFinishedPopup() {
        return wipFinishedPopup;
    }
    
    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setIconContainerId(RichIcon iconContainerId) {
        this.iconContainerId = iconContainerId;
    }

    public RichIcon getIconContainerId() {
        return iconContainerId;
    }

    /* public void setIconStartTs(RichIcon iconStartTs) {
        this.iconStartTs = iconStartTs;
    }

    public RichIcon getIconStartTs() {
        return iconStartTs;
    } */

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setTextErrorMessage(RichOutputText textErrorMessage) {
        this.textErrorMessage = textErrorMessage;
    }

    public RichOutputText getTextErrorMessage() {
        return textErrorMessage;
    }

    public void setMainPanel(RichPanelFormLayout mainPanel) {
        this.mainPanel = mainPanel;
    }

    public RichPanelFormLayout getMainPanel() {
        return mainPanel;
    }
}
