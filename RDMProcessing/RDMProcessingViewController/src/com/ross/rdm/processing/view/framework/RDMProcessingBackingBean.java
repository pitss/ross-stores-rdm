package com.ross.rdm.processing.view.framework;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMBackingBeanBase;

import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.jbo.ApplicationModule;
import oracle.jbo.server.DBTransaction;

public class RDMProcessingBackingBean extends RDMBackingBeanBase  {
  private static String _appModuleName = "ProcessingAppModule";
  private static String _appModuleDataControlName = "ProcessingAppModuleDataControl";
  
  public RDMProcessingBackingBean() {
  }
  /**
 * Return name of the Application Module 
 * set in static class attribute _appModuleName .
 *
 * @return _appModuleName 
 */
  public String getAppModuleName() {
    return _appModuleName;
  }
  /**
 * Return name of the Application Module Data Control Name
 * set in static class attribute _appModuleDataControlName .
 *
 * @return _appModuleDataControlName 
 */
  public String getAppModuleDataControlName() {
    return _appModuleDataControlName;
  }
  /**
 * Return Application Module 
 *
 * @return application module 
 */
  public ApplicationModule getAppModule() {
    return ADFUtils.getApplicationModuleForDataControl(getAppModuleDataControlName());
  }
  /**
 * Return BindingContainer of page
 *
 * @return current page binding container 
 */
  public BindingContainer getBindings() {
    return BindingContext.getCurrent().getCurrentBindingsEntry();
  }
  /**
 * Return current DBTransaction 
 *
 * @return current DBTransaction 
 */
  public DBTransaction getDBTransaction() {
    return (DBTransaction) getAppModule().getTransaction();
  }
  public String logoutExitBTF() {
    // PITSS.CON NOTE : If you want to navigate to the calling Task Flow/Page Flow, 
    //   then please comment out the following code and replace with  
    //   return "backGlobalHome";  
    return "backGlobalHome";  
    //ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
    //HttpSession session = (HttpSession)ectx.getSession(false);
    //String temp = ControllerContext.getInstance().getGlobalViewActivityURL("exit");
    //try {
    //   ectx.redirect(temp);
    //   session.invalidate();
    //} catch (Exception ex) {
    //  ex.printStackTrace();
    //}
    //return null;
  }    
                   
    public Object getPageFlowBean(String pageFlowBeanName) {
        return AdfFacesContext.getCurrentInstance().getPageFlowScope().get(pageFlowBeanName);
    }

    public Object getSessionScopeBean(String beanName) {
        return ADFContext.getCurrent().getSessionScope().get(beanName);
    }    
}
