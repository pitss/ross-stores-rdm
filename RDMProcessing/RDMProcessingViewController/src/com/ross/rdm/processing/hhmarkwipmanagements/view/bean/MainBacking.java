package com.ross.rdm.processing.hhmarkwipmanagements.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.processing.model.bean.FunctionCallResult;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Main.jspx
// ---
// ---------------------------------------------------------------------
public class MainBacking extends MarkWipMgmtBaseBean {
    private static final String PF_VARIOUS_MESSAGES = "variousMessagesToShow";
    private RichPopup variousMessagesPopup;
    private RichInputText mlp;
    private RichIcon mlpIcon;
    private RichPanelFormLayout pflMain;
    private RichLink f1Link;
    private RichLink f2Link;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichPopup popupStatus;
    private RichPopup confirmPopup;
    private RichOutputText confirmPopupText;
    private RichLink confirmPopupYesLink;
    private RichLink confirmPopupNoLink;
    private RichInputText nextMarkStatus;
    private RichIcon nextMarkStatusIcon;
    private RichLink okLinkVariousMessages;

    public MainBacking() {

    }

    public void onChangedMlp(ValueChangeEvent vce) {
        updateModel(vce);
        validateMlp(true);
    }

    private void validateMlp(boolean executeKeyNextItem) {
        removeErrorOfField(getMlp(), getMlpIcon());
        FunctionCallResult result =
            (FunctionCallResult) ADFUtils.findOperation("callPkgMarkWipMgmtAdfValidateMlp").execute();
        refreshContentOfUIComponent(getPflMain());
       
        if (result != null) {
            getMarkWipFlowBean().setSetItemProperty(result.getSetItemProperty());
            if (Boolean.TRUE.equals(result.getGoNextItem())) {
                if (executeKeyNextItem) {
                    getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.NEXT_MARK_STATUS);
                    setFocusOnUIComponent(getNextMarkStatus());
                    
                    // OTM 3590 
                    //Show message/s in popup and after aknowledge them, go to next field
                    if (result.getMessages() != null && !result.getMessages().isEmpty()) {
                        ADFContext.getCurrent().getPageFlowScope().put(PF_VARIOUS_MESSAGES, result.getMessages());
                        showPopupAndDisableFields(getVariousMessagesPopup());
                    }
                }
            } else if (result.getMessages() != null && !result.getMessages().isEmpty()) {
                List message = (List) result.getMessages().get(0);
                showMessagesPanel((String) message.get(0), (String) message.get(1));
                setErrorOnField(getMlp(), getMlpIcon());
            }
        }
    }

    public void setVariousMessagesPopup(RichPopup variousMessagesPopup) {
        this.variousMessagesPopup = variousMessagesPopup;
    }

    public RichPopup getVariousMessagesPopup() {
        return variousMessagesPopup;
    }

    private List getCurrentMessageFromPageFlow() {
        List currentMessage = null;
        List messages = (List) ADFContext.getCurrent().getPageFlowScope().get(PF_VARIOUS_MESSAGES);
        if (messages != null && messages.size() > 0) {
            currentMessage = (List) messages.get(0);
        }
        return currentMessage;
    }

    /**
     * This method returns the type of the next message to show
     */
    public String getCurrentMessageType() {
        List currentMessage = getCurrentMessageFromPageFlow();
        String type = (String) (currentMessage != null ? currentMessage.get(0) : null);
        return INFO.equals(type) ? INF_ICON : WRN_ICON;
    }

    /**
     * This method returns the text of the next message to show
     */
    public String getCurrentMessageText() {
        List currentMessage = getCurrentMessageFromPageFlow();
        return (String) (currentMessage != null ? currentMessage.get(1) : null);
    }

    public String okVariousMessagesPopupAction() {
        getVariousMessagesPopup().hide();
        List messages = (List) ADFContext.getCurrent().getPageFlowScope().get(PF_VARIOUS_MESSAGES);
        if (messages != null && messages.size() > 1) { //More messages to show
            List allElementsButFirst = new ArrayList();
            for (int i = 1; i < messages.size(); i++) {
                allElementsButFirst.add(messages.get(i));
            }
            ADFContext.getCurrent().getPageFlowScope().put(PF_VARIOUS_MESSAGES, allElementsButFirst);
            getVariousMessagesPopup().show(new RichPopup.PopupHints());
        } else {
            getMarkWipFlowBean().setDisableAllFields(false);
            focusSelectedField();
            refreshContentOfUIComponent(getPflMain());
        }
        return null;
    }

    private void showPopupAndDisableFields(RichPopup richPopup) {
        richPopup.show(new RichPopup.PopupHints());
        getMarkWipFlowBean().setDisableAllFields(true);
    }

    public void setMlp(RichInputText mlp) {
        this.mlp = mlp;
    }

    public RichInputText getMlp() {
        return mlp;
    }

    public void setMlpIcon(RichIcon mlpIcon) {
        this.mlpIcon = mlpIcon;
    }

    public RichIcon getMlpIcon() {
        return mlpIcon;
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    public String f1ViewPvAction() {
    String nav = null;
    if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mark_wip_management_s_MAIN_F1,RoleBasedAccessConstants.FORM_NAME_hh_mark_wip_management_s)){
         if (isEnabledLink("MAIN.F1") && validateForm()) {
            ADFUtils.findOperation("displayPv").execute();
            nav = "goMarkPv";
        }
        }else{
        showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
         }
        return nav;
    }

    public String f4DoneAction() {
        String nav = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mark_wip_management_s_MAIN_F4,RoleBasedAccessConstants.FORM_NAME_hh_mark_wip_management_s)){
        if (isEnabledLink("MAIN.F4")) {
            List errorMessages = (List) ADFUtils.findOperation("f4Done").execute();
            removeErrors();
            refreshContentOfUIComponent(getPflMain());
            if (errorMessages != null && !errorMessages.isEmpty()) {
                if (errorMessages.size() == 2) { //ONLY MESSAGE
                    if (CONF.equals(errorMessages.get(0))) { //CONFIRMATION POPUP
                        getMarkWipFlowBean().setDisableAllFields(true);
                        getConfirmPopupText().setValue(errorMessages.get(1));
                        getConfirmPopup().show(new RichPopup.PopupHints());
                    } else {
                        setErrorInSelectedField();
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                } else if (errorMessages.size() == 3) { //MESSAGE AND FIELD TO FOCUS
                    showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    getMarkWipFlowBean().setCurrentFocus((String) errorMessages.get(2));
                    setErrorInSelectedField();
                } else if (errorMessages.size() == 1) {
                    getMarkWipFlowBean().setCurrentFocusMarkQty(HhMarkWipManagementSBean.MARKER_ID);
                    nav = (String) errorMessages.get(0);
                }
            } else {
                getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.MLP);
                setFocusOnUIComponent(getMlp());
            }
        }
        }else{
        showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
         }
        return nav;
    }

    public String f3ExitAction() {
        String nav = null;
        if (isEnabledLink("MAIN.F3")) {
            nav = logoutExitBTF();
        }
        return nav;
    }

    public String f2StatusAction() {
      if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mark_wip_management_s_MAIN_F2,RoleBasedAccessConstants.FORM_NAME_hh_mark_wip_management_s)){
          if (isEnabledLink("MAIN.F2") && validateForm()) {
              removeErrors();
              ADFUtils.setBoundAttributeValue("NextMarkStatusLov", null);
              getMarkWipFlowBean().setDisableAllFields(true);
              refreshContentOfUIComponent(getPflMain());
              getPopupStatus().show(new RichPopup.PopupHints());
          }    
      }else{
          showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
                      
      }
        return null;
    }

    private void removeErrors() {
        removeErrorOfField(getMlp(), getMlpIcon());
        removeErrorOfField(getNextMarkStatus(), getNextMarkStatusIcon());
        hideMessagesPanel();
    }

    public void setF1Link(RichLink f1Link) {
        this.f1Link = f1Link;
    }

    public RichLink getF1Link() {
        return f1Link;
    }

    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setPopupStatus(RichPopup popupStatus) {
        this.popupStatus = popupStatus;
    }

    public RichPopup getPopupStatus() {
        return popupStatus;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmPopupText(RichOutputText confirmPopupText) {
        this.confirmPopupText = confirmPopupText;
    }

    public RichOutputText getConfirmPopupText() {
        return confirmPopupText;
    }

    public void setConfirmPopupYesLink(RichLink confirmPopupYesLink) {
        this.confirmPopupYesLink = confirmPopupYesLink;
    }

    public RichLink getConfirmPopupYesLink() {
        return confirmPopupYesLink;
    }

    public void setConfirmPopupNoLink(RichLink confirmPopupNoLink) {
        this.confirmPopupNoLink = confirmPopupNoLink;
    }

    public RichLink getConfirmPopupNoLink() {
        return confirmPopupNoLink;
    }

    public String confirmPopupYesAction() {
        getMarkWipFlowBean().setDisableAllFields(false);
        getConfirmPopup().hide();
        ADFUtils.findOperation("goToMarkQty").execute();
        getMarkWipFlowBean().setCurrentFocusMarkQty(HhMarkWipManagementSBean.MARKER_ID);
        return "goMarkQty";
    }

    public String confirmPopupNoAction() {
        getMarkWipFlowBean().setDisableAllFields(false);
        focusSelectedField();
        getConfirmPopup().hide();
        refreshContentOfUIComponent(getPflMain());
        return null;
    }

    private void focusSelectedField() {
        if (HhMarkWipManagementSBean.MLP.equals(getMarkWipFlowBean().getCurrentFocus())) {
            setFocusOnUIComponent(getMlp());
        } else if (HhMarkWipManagementSBean.NEXT_MARK_STATUS.equals(getMarkWipFlowBean().getCurrentFocus())) {
            setFocusOnUIComponent(getNextMarkStatus());
        }
    }

    public void setNextMarkStatus(RichInputText nextMarkStatus) {
        this.nextMarkStatus = nextMarkStatus;
    }

    public RichInputText getNextMarkStatus() {
        return nextMarkStatus;
    }

    public void setNextMarkStatusIcon(RichIcon nextMarkStatusIcon) {
        this.nextMarkStatusIcon = nextMarkStatusIcon;
    }

    public RichIcon getNextMarkStatusIcon() {
        return nextMarkStatusIcon;
    }

    public void performKeySelectStatus(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                if (StringUtils.isNotEmpty(submittedValue)) {
                    ADFUtils.setBoundAttributeValue("NextMarkStatus", submittedValue);
                    getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.MLP);
                }

                getMarkWipFlowBean().setDisableAllFields(false);
                getPopupStatus().hide();
                focusSelectedField();
                refreshContentOfUIComponent(getPflMain());
            }
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1Link());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2Link());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("mlp1".equals(item)) {
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedMlp(vce);
                    }
                } else if ("nextSt".equals(item)) {
                    removeErrorOfField(getNextMarkStatus(), getNextMarkStatusIcon());
                    getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.MLP);
                    refreshContentOfUIComponent(getPflMain());
                    setFocusOnUIComponent(getMlp());
                }
            }
        }
    }

    /**
     * This method will simulate the ENTER procedure of Forms, which validates the fields of the block. In this case, only MLP.
     */
    private boolean validateForm() {
        if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("Mlp"))) {
            showMessagesPanel(ERROR, this.getMessage("INV_CONTAINER"));
            setErrorOnField(getMlp(), getMlpIcon());
        }else if(HhMarkWipManagementSBean.MLP.equals(getMarkWipFlowBean().getCurrentFocus())){
            validateMlp(false);
        }

        return !ERR_ICON.equals(getMlpIcon().getName()) &&
               !getMarkWipFlowBean().isDisableAllFields(); //MLP field is the only one with trigger in Forms
    }

    public void performKeyConfirmPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmPopupYesLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmPopupNoLink());
                actionEvent.queue();
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getMarkWipFlowBean().getDisabledMlp()) {
            setFocusOnUIComponent(getMlp());
        }
    }

    public void performKeyMessagesPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOkLinkVariousMessages());
                actionEvent.queue();
            }
        }
    }

    public void setOkLinkVariousMessages(RichLink okLinkVariousMessages) {
        this.okLinkVariousMessages = okLinkVariousMessages;
    }

    public RichLink getOkLinkVariousMessages() {
        return okLinkVariousMessages;
    }

    private void setErrorInSelectedField() {
        if (HhMarkWipManagementSBean.MLP.equals(getMarkWipFlowBean().getCurrentFocus())) {
            setErrorOnField(getMlp(), getMlpIcon());
        } else if (HhMarkWipManagementSBean.NEXT_MARK_STATUS.equals(getMarkWipFlowBean().getCurrentFocus())) {
            setErrorOnField(getNextMarkStatus(), getNextMarkStatusIcon());
        }
    }
}
