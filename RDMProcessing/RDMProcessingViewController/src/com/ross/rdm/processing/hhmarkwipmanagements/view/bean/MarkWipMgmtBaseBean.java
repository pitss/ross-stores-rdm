package com.ross.rdm.processing.hhmarkwipmanagements.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

public class MarkWipMgmtBaseBean extends RDMHotelPickingBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    public MarkWipMgmtBaseBean() {
        super();
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    protected boolean isMessageShown() {
        return getErrorWarnInfoMessage() != null &&
               StringUtils.isNotEmpty((String) getErrorWarnInfoMessage().getValue());
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    protected String getMessage(String messageCode) {
        return this.getMessage(messageCode, WARN, (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                               (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
    }

    protected void setErrorOnField(RichInputText input, RichIcon icon) {
        this.addErrorStyleToComponent(input);
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        this.setFocusOnUIComponent(input);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon) {
        this.hideMessagesPanel();
        this.removeErrorOfField(input, icon, REQ_ICON);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon, String name) {
        this.removeErrorStyleToComponent(input);
        icon.setName(name);
        this.refreshContentOfUIComponent(icon);
    }

    protected HhMarkWipManagementSBean getMarkWipFlowBean() {
        return (HhMarkWipManagementSBean) this.getPageFlowBean("HhMarkWipManagementSBean");
    }

    protected boolean isEnabledLink(String item) {
        boolean enabled = true;
        OperationBinding op = ADFUtils.findOperation("callCheckScreenOptionPrivMarkWip");
        op.getParamsMap().put("optionName", item);
        if ("OFF".equals(op.execute())) {
            enabled = false;
            showMessagesPanel(WARN, getMessage("NOT_ALLOWED"));
        }
        return enabled;
    }


    protected boolean isNumericValue(String submittedVal, RichInputText inputText, RichIcon icon) {
        boolean valid = true;
        if (StringUtils.isNotEmpty(submittedVal) && !submittedVal.matches("[0-9]+")) {
            this.setErrorOnField(inputText, icon);
            this.showMessagesPanel(WARN, this.getMessage("MUST_BE_NUMERIC"));
            valid = false;
        }
        return valid;
    }
}
