package com.ross.rdm.processing.hhmarkwipmanagements.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for MarkQty.jspx
// ---
// ---------------------------------------------------------------------
public class MarkQtyBacking extends MarkWipMgmtBaseBean {
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichInputText qtyMarked;
    private RichInputText ctnMarked;
    private RichInputText markerId;
    private RichIcon markerIdIcon;
    private RichIcon qtyMarkedIcon;
    private RichIcon ctnMarkedIcon;
    private RichPanelFormLayout pflMain;
    private RichLink validateMarkerHiddenLink;

    public MarkQtyBacking() {

    }

    public void onChangedMarkerId(ValueChangeEvent vce) {
        updateModel(vce);
        getMarkWipFlowBean().setExecutedKey(false);
        ActionEvent actionEvent = new ActionEvent(this.getValidateMarkerHiddenLink());
        actionEvent.queue();
    }

    private List validateUser() {
        return (List) ADFUtils.findOperation("callPkgMarkWipMgmtAdfValidateUser").execute();
    }

    public String f3ExitAction() {
        String nav = null;
        if (isEnabledLink("MARK_QTY.F3")) {
            ADFUtils.findOperation("clearMarkQtyBlock").execute();
            getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.MLP);
            nav = "goMain";
        }
        return nav;
    }

    public String f4DoneAction() {
        String navigation = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mark_wip_management_s_MARK_QTY_F4,RoleBasedAccessConstants.FORM_NAME_hh_mark_wip_management_s)){
        if (isEnabledLink("MARK_QTY.F4")) {
            if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("MarkerId"))) {
                highlightField(HhMarkWipManagementSBean.MARKER_ID);
                showMessagesPanel("E", this.getMessage("PARTIAL_ENTRY"));
            } else {
                List errorMessages = validateUser();
                if (errorMessages != null) {
                    if (errorMessages.size() == 2) {
                        highlightField(HhMarkWipManagementSBean.MARKER_ID);
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                } else {
                    errorMessages = (List) ADFUtils.findOperation("f4DoneMarkQty").execute();
                    if (errorMessages != null) {
                        if (errorMessages.size() == 1) {
                            getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.MLP);
                            navigation = (String) errorMessages.get(0);

                        } else if (errorMessages.size() > 1) {
                            if (errorMessages.size() > 2) {
                                highlightField((String) errorMessages.get(2));
                            }
                            showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                        }
                    }
                }
            }
        }
        }else{
        showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
         }
        return navigation;
    }

    private void highlightField(String field) {
        removeErrors();
        getMarkWipFlowBean().setCurrentFocusMarkQty(field);
        refreshContentOfUIComponent(getPflMain());
        switch (field) {
        case HhMarkWipManagementSBean.MARKER_ID:
            setErrorOnField(getMarkerId(), getMarkerIdIcon());
            break;
        case HhMarkWipManagementSBean.QTY_MARKED:
            setErrorOnField(getQtyMarked(), getQtyMarkedIcon());
            break;
        case HhMarkWipManagementSBean.CTN_MARKED:
            setErrorOnField(getCtnMarked(), getCtnMarkedIcon());
            break;
        default:
        }
        
    }

    private void removeErrors() {
        removeErrorOfField(getMarkerId(), getMarkerIdIcon());
        removeErrorOfField(getQtyMarked(), getQtyMarkedIcon());
        removeErrorOfField(getCtnMarked(), getCtnMarkedIcon());
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setQtyMarked(RichInputText qtyMarked) {
        this.qtyMarked = qtyMarked;
    }

    public RichInputText getQtyMarked() {
        return qtyMarked;
    }

    public void setCtnMarked(RichInputText ctnMarked) {
        this.ctnMarked = ctnMarked;
    }

    public RichInputText getCtnMarked() {
        return ctnMarked;
    }

    public void setMarkerId(RichInputText markerId) {
        this.markerId = markerId;
    }

    public RichInputText getMarkerId() {
        return markerId;
    }

    public void setMarkerIdIcon(RichIcon markerIdIcon) {
        this.markerIdIcon = markerIdIcon;
    }

    public RichIcon getMarkerIdIcon() {
        return markerIdIcon;
    }

    public void setQtyMarkedIcon(RichIcon qtyMarkedIcon) {
        this.qtyMarkedIcon = qtyMarkedIcon;
    }

    public RichIcon getQtyMarkedIcon() {
        return qtyMarkedIcon;
    }

    public void setCtnMarkedIcon(RichIcon ctnMarkedIcon) {
        this.ctnMarkedIcon = ctnMarkedIcon;
    }

    public RichIcon getCtnMarkedIcon() {
        return ctnMarkedIcon;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("marker".equals(item)) {
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedMarkerId(vce);
                    }
                } else if ("qty".equals(item) && isNumericValue(submittedVal, getQtyMarked(), getQtyMarkedIcon())) {
                    removeErrorOfField(getQtyMarked(), getQtyMarkedIcon());
                    getMarkWipFlowBean().setCurrentFocusMarkQty(HhMarkWipManagementSBean.CTN_MARKED);
                    refreshContentOfUIComponent(getPflMain());
                } else if ("ctn".equals(item) && isNumericValue(submittedVal, getCtnMarked(), getCtnMarkedIcon())) {
                    removeErrorOfField(getCtnMarked(), getCtnMarkedIcon());
                    getMarkWipFlowBean().setCurrentFocusMarkQty(HhMarkWipManagementSBean.MARKER_ID);
                    refreshContentOfUIComponent(getPflMain());
                }
            }
        }
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    public String validateMarkerHiddenAction() {
        if (!getMarkWipFlowBean().getExecutedKey()) {
            boolean nextField = true;
            if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("MarkerId"))) {
                List errorMessages = validateUser();
                if (errorMessages != null && errorMessages.size() == 2) {
                    highlightField(HhMarkWipManagementSBean.MARKER_ID);
                    showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    nextField = false;
                }
            }

            if (nextField) {
                removeErrorOfField(getMarkerId(), getMarkerIdIcon());
                getMarkWipFlowBean().setCurrentFocusMarkQty(HhMarkWipManagementSBean.QTY_MARKED);
                refreshContentOfUIComponent(getPflMain());
            }
        }
        getMarkWipFlowBean().setExecutedKey(null);
        return null;
    }

    public void setValidateMarkerHiddenLink(RichLink validateMarkerHiddenLink) {
        this.validateMarkerHiddenLink = validateMarkerHiddenLink;
    }

    public RichLink getValidateMarkerHiddenLink() {
        return validateMarkerHiddenLink;
    }
}
