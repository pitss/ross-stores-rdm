package com.ross.rdm.processing.hhmarkwipmanagements.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhMarkWipManagementSBean extends RDMHotelPickingPageFlowBean {
    @SuppressWarnings("compatibility:438807621034296242")
    private static final long serialVersionUID = 1L;
    public static final String MLP = "MLP";
    public static final String NEXT_MARK_STATUS = "NEXT_MARK_STATUS";
    public static final String MARKER_ID = "MARKER_ID";
    public static final String QTY_MARKED = "QTY_MARKED";
    public static final String CTN_MARKED = "CTN_MARKED";
    private List itemsToDisable = null;
    private String currentFocus = null;
    private String currentFocusMarkQty = null;
    private String setItemProperty;
    private boolean disableAllFields = false;
    private Boolean executedKey = Boolean.FALSE;

    public void initTaskFlow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("initMarkWipManagement");
        List<String> errorsMessages = (List<String>) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (errorsMessages != null && errorsMessages.size() == 2) {
                if ("W".equals(errorsMessages.get(0))) {
                    FacesContext fc = FacesContext.getCurrentInstance();
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, errorsMessages.get(1), "");
                    fc.addMessage(fc.getViewRoot().getId(), fm);
                } else if ("I".equals(errorsMessages.get(0))) {
                    JSFUtils.addFacesInformationMessage(errorsMessages.get(1));
                } else {
                    JSFUtils.addFacesErrorMessage(errorsMessages.get(1));
                }
            }
        }
    }

    public void setCurrentFocus(String currentFocus) {
        this.currentFocus = currentFocus;
    }

    public String getCurrentFocus() {
        return currentFocus;
    }

    public Boolean getDisabledMlp() {
        return shouldDisableItem("MAIN.MLP") || disableAllFields || (currentFocus != null && !MLP.equals(currentFocus));
    }

    public Boolean getDisabledNextMarkStatus() {
        return shouldDisableItem("MAIN.NEXT_MARK_STATUS") || disableAllFields || !NEXT_MARK_STATUS.equals(currentFocus);
    }

    public void setSetItemProperty(String setItemProperty) {
        this.setItemProperty = setItemProperty;
    }

    public String getSetItemProperty() {
        return setItemProperty;
    }

    public void setDisableAllFields(boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public boolean isDisableAllFields() {
        return disableAllFields;
    }

    public void setCurrentFocusMarkQty(String currentFocusMarkQty) {
        this.currentFocusMarkQty = currentFocusMarkQty;
    }

    public String getCurrentFocusMarkQty() {
        return currentFocusMarkQty;
    }

    public Boolean getDisabledMarkerId() {
        return shouldDisableItem("MARK_QTY.MARKER_ID") ||
               (currentFocusMarkQty != null && !MARKER_ID.equals(currentFocusMarkQty));
    }

    public Boolean getDisabledQtyMarked() {
        return shouldDisableItem("MARK_QTY.QTY_MARKED") || !QTY_MARKED.equals(currentFocusMarkQty);
    }

    public Boolean getDisabledCtnMarked() {
        return shouldDisableItem("MARK_QTY.CTN_MARKED") || !CTN_MARKED.equals(currentFocusMarkQty);
    }

    public void setItemsToDisable(List itemsToDisable) {
        this.itemsToDisable = itemsToDisable;
    }

    public List getItemsToDisable() {
        return itemsToDisable;
    }

    public void callStartUpLocalSecurity() {
        List items = (List) ADFUtils.findOperation("callStartUpLocalSecurityMarkWipManagement").execute();
        this.setItemsToDisable(items);
    }

    private boolean shouldDisableItem(String item) {
        return getItemsToDisable() != null && getItemsToDisable().contains(item);
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
}
