package com.ross.rdm.processing.hhmarkwipmanagements.view.bean;

import com.ross.rdm.processing.view.framework.RDMProcessingBackingBean;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for MarkPv.jspx
// ---
// ---------------------------------------------------------------------
public class MarkPvBacking extends MarkWipMgmtBaseBean {
    public MarkPvBacking() {

    }

    public String f3ExitAction() {
        String nav = null;
        if (isEnabledLink("MARK_PV.F3")) {
            getMarkWipFlowBean().setCurrentFocus(HhMarkWipManagementSBean.MLP);
            nav = "goMain";
        }
        return nav;
    }
}
