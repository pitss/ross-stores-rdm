package com.ross.rdm.processing.hhworkprocessings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.processing.view.framework.RDMProcessingBackingBean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for WorkQty.jspx
// ---
// ---------------------------------------------------------------------
public class WorkQtyBacking extends WorkProcessingBaseBean {

    private static final String WORKQTY_WORKEDCONT = "WorkQtyWorkedCont";
    private static final String MAIN_REMAINING_CONT = "RemainingCont";
    private static final String WORKQTY_WORKEDUNITS = "WorkQtyWorkedUnits";
    private static final String WORKQTY_REMAININGCONT = "WorkQtyRemainingCont";
    private static final String WORKQTY_REMAININGUNITS = "WorkQtyRemainingUnits";
    private static final String MAIN_REMAININGUNITS = "RemainingUnits";
    private static final String WORKQTY_CTNREMAIN = "CtnRemain";
    private static final String WORKQTY_UNITREMAIN = "UnitRemain";
    private RichInputText operatorControl;
    private RichInputText ctnRemainControl;
    private RichInputText unitRemainControl;

    // "special" value for remainingUnits, remainingCont in "C" case
    private Integer staticNull = 0;

    private static ADFLogger logger = ADFLogger.createADFLogger(WorkQtyBacking.class);
    private RichLink f3Link;
    private RichLink f4Link;
    private RichPanelFormLayout pflMain;

    public WorkQtyBacking() {
    }

    public void onChangedOperatorId(ValueChangeEvent vce) {
        hideMessages();
        updateModel(vce);
        if (validateUser()) {
            String nextWorkStatus = (String) ADFUtils.getBoundAttributeValue("NextWorkStatus");
            if ("C".equals(nextWorkStatus)) {
                focusField(HhWorkProcessingSBean.OPERATOR);
            } else {
                focusField(HhWorkProcessingSBean.CTN_REMAIN);
            }
        }
    }

    public void onChangedCtnRemain(ValueChangeEvent vce) {
        hideMessages();
        updateModel(vce);
        if (validateCtnRemain()) {
            String nonUnqCntQty = (String) ADFUtils.getBoundAttributeValue("NonUnqCntQty");
            if ("Y".equals(nonUnqCntQty)) {
                focusField(HhWorkProcessingSBean.UNIT_REMAIN);
            } else {
                focusField(HhWorkProcessingSBean.OPERATOR);
            }
        }
    }

    public void onChangedUnitRemain(ValueChangeEvent vce) {
        hideMessages();
        updateModel(vce);
        validateUnitRemain();
    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_WORK_QTY_F4,RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)){
                          hideMessages();

                          String nextWorkStatus = (String) ADFUtils.getBoundAttributeValue("NextWorkStatus");
                          String nonUnqCntQty = (String) ADFUtils.getBoundAttributeValue("NonUnqCntQty");

                          if (!validateCtnRemain()) {
                              return;
                          } else {
                              if ("Y".equals(nonUnqCntQty)) {
                                  if (!validateUnitRemain()) {
                                      return;
                                  }
                              }
                              if ("P".equals(nextWorkStatus)) {
                                  if (!executeWorkStatus("pauseWork"))
                                      return;
                              } else if ("C".equals(nextWorkStatus)) {
                                  if (!executeWorkStatus("completeWork"))
                                      return;
                              }
                          }
                          clearBlock();
                          getPageFlowBean().setMainFocus(HhWorkProcessingSBean.CID);
                          navigateToAction("goMain");   
                      }else{
                          this.showMessage("E", "NOT_ALLOWED", null);
                      }
        
       
    }

    /**
     *  Helper method to DRY business logic invocations
     */
    private boolean executeWorkStatus(String operation) {
        if (!executeOperation(operation)) {
            logger.severe("Could not execute operation '" + operation + "'");
            return false;
        }

        String result = (String) getOperationResult(operation);
        if (null != result) {
            showMessage(result.substring(0, 1), result.substring(1), null);
            return false;
        }

        executeOperation("clearData");
        getPageFlowBean().setContainerId("");

        return true;
    }

    @Override
    protected HhWorkProcessingSBean getPageFlowBean() {
        return (HhWorkProcessingSBean) getPageFlowBean("HhWorkProcessingSBean");
    }

    @Override
    protected void hideMessages() {
        super.hideMessages();
        removeErrorStyleToComponent(operatorControl);
        removeErrorStyleToComponent(ctnRemainControl);
        removeErrorStyleToComponent(unitRemainControl);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////

    public void setOperatorControl(RichInputText operatorControl) {
        this.operatorControl = operatorControl;
    }

    public RichInputText getOperatorControl() {
        return operatorControl;
    }

    public void setCtnRemainControl(RichInputText ctnRemainControl) {
        this.ctnRemainControl = ctnRemainControl;
    }

    public RichInputText getCtnRemainControl() {
        return ctnRemainControl;
    }

    public void setUnitRemainControl(RichInputText unitRemainControl) {
        this.unitRemainControl = unitRemainControl;
    }

    public RichInputText getUnitRemainControl() {
        return unitRemainControl;
    }

    public void setStaticNull(Integer remaining) {
        this.staticNull = remaining;
    }

    public Integer getStaticNull() {
        return staticNull;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledOperator()) {
            setFocusOnUIComponent(getOperatorControl());
        } else if (!getPageFlowBean().getDisabledCtnRemain()) {
            setFocusOnUIComponent(getCtnRemainControl());
        } else if (!getPageFlowBean().getDisabledUnitRemain()) {
            setFocusOnUIComponent(getUnitRemainControl());
        }
    }

    private void focusField(String focus) {
        getPageFlowBean().setWorkQtyFocus(focus);
        onRegionLoad(null); //FORCE FOCUS
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("ope1".equals(item)) {
                    if (valueNotChanged(submittedVal, field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedOperatorId(vce);
                    }
                } else if ("ctn1".equals(item)) {
                    if (valueNotChanged(submittedVal, field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedCtnRemain(vce);
                    }
                } else if ("uni1".equals(item)) {
                    if (valueNotChanged(submittedVal, field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedUnitRemain(vce);
                    }
                }
            }
        }
    }

    private boolean valueNotChanged(String submittedVal, Object fieldValue) {
        if (fieldValue instanceof String) {
            return (StringUtils.isEmpty((String) fieldValue) && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase((String) fieldValue);
        } else {
            return (fieldValue == null && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase(fieldValue == null ? "" : fieldValue.toString());
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    private boolean validateCtnRemain() {
        boolean valid = true;
        String ctnRemainString = (String) ADFUtils.getBoundAttributeValue(WORKQTY_CTNREMAIN);
        if (!isNumericValue(ctnRemainString)) {
            valid = false;
            focusField(HhWorkProcessingSBean.CTN_REMAIN);
            showMessage("W", "MUST_BE_NUMERIC", getCtnRemainControl());
        } else {
            Integer ctnRemain = ctnRemainString != null ? Integer.valueOf(ctnRemainString) : null;
            String nextWorkStatus = (String) ADFUtils.getBoundAttributeValue("NextWorkStatus");
            String unqQtyString = (String) ADFUtils.getBoundAttributeValue("UnqQty");
            Integer unqQty = unqQtyString != null ? Integer.valueOf(unqQtyString) : null;
            if (!"C".equals(nextWorkStatus) && ctnRemain == null) {
                focusField(HhWorkProcessingSBean.CTN_REMAIN);
                showMessage("E", "PARTIAL_ENTRY", getCtnRemainControl());
                valid = false;
            } else if (ctnRemain != null && Integer.valueOf(0).compareTo(ctnRemain) > 0) {
                focusField(HhWorkProcessingSBean.CTN_REMAIN);
                showMessage("E", "INV_NUM_NEG", getCtnRemainControl());
                valid = false;
            } else if (ctnRemain != null && Integer.valueOf(0).compareTo(ctnRemain) == 0) {
                focusField(HhWorkProcessingSBean.CTN_REMAIN);
                showMessage("E", "ZERO_ENTRY_E", getCtnRemainControl());
                valid = false;
            } else if (ctnRemain != null &&
                       ((Integer) ADFUtils.getBoundAttributeValue(MAIN_REMAINING_CONT)).compareTo(ctnRemain) <= 0) {
                focusField(HhWorkProcessingSBean.CTN_REMAIN);
                showMessage("E", "INV_CONT_COUNT", getCtnRemainControl());
                valid = false;
            }

            if (valid) {
                if (!"Y".equals(ADFUtils.getBoundAttributeValue("NonUnqCntQty"))) {
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDCONT,
                                                    ctnRemain == null ? null :
                                                    (Integer) ADFUtils.getBoundAttributeValue(MAIN_REMAINING_CONT) -
                                                    ctnRemain);
                    Integer workedCont = (Integer) ADFUtils.getBoundAttributeValue(WORKQTY_WORKEDCONT);
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDUNITS,
                                                    workedCont == null ? null : workedCont * unqQty);
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGCONT, ctnRemain);
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGUNITS,
                                                    ctnRemain == null ? null : ctnRemain * unqQty);
                }
                if ("C".equals(nextWorkStatus)) {
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDUNITS,
                                                    ADFUtils.getBoundAttributeValue(MAIN_REMAININGUNITS));
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDCONT,
                                                    ADFUtils.getBoundAttributeValue(MAIN_REMAINING_CONT));
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGUNITS, Integer.valueOf(0));
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGCONT, Integer.valueOf(0));

                }
            }
            refreshContentOfUIComponent(getPflMain());
        }


        return valid;
    }

    private boolean validateUnitRemain() {
        boolean valid = true;
        String unitRemainString = (String) ADFUtils.getBoundAttributeValue(WORKQTY_UNITREMAIN);
        String ctnRemainString = (String) ADFUtils.getBoundAttributeValue(WORKQTY_CTNREMAIN);
        if (!isNumericValue(unitRemainString)) {
            valid = false;
            focusField(HhWorkProcessingSBean.UNIT_REMAIN);
            showMessage("W", "MUST_BE_NUMERIC", getUnitRemainControl());
        } else if (!isNumericValue(ctnRemainString)) {
            valid = false;
            focusField(HhWorkProcessingSBean.CTN_REMAIN);
            showMessage("W", "MUST_BE_NUMERIC", getCtnRemainControl());
        } else {
            Integer unitRemain = unitRemainString != null ? Integer.valueOf(unitRemainString) : null;
            Integer ctnRemain = unitRemainString != null ? Integer.valueOf(unitRemainString) : null;
            String nonUnqCntQty = (String) ADFUtils.getBoundAttributeValue("NonUnqCntQty");
            String nextWorkStatus = (String) ADFUtils.getBoundAttributeValue("NextWorkStatus");
            if (!"C".equals(nextWorkStatus) && "Y".equals(nonUnqCntQty) && unitRemain == null) {
                focusField(HhWorkProcessingSBean.UNIT_REMAIN);
                showMessage("E", "PARTIAL_ENTRY", getUnitRemainControl());
                valid = false;
            } else if (unitRemain != null && Integer.valueOf(0).compareTo(unitRemain) > 0) {
                focusField(HhWorkProcessingSBean.UNIT_REMAIN);
                showMessage("E", "INV_NUM_NEG", getUnitRemainControl());
                valid = false;
            } else if (unitRemain != null && Integer.valueOf(0).compareTo(unitRemain) == 0) {
                focusField(HhWorkProcessingSBean.UNIT_REMAIN);
                showMessage("E", "ZERO_ENTRY_E", getUnitRemainControl());
                valid = false;
            } else if (unitRemain != null &&
                       ((Integer) ADFUtils.getBoundAttributeValue(MAIN_REMAININGUNITS)).compareTo(unitRemain) <= 0) {
                focusField(HhWorkProcessingSBean.UNIT_REMAIN);
                showMessage("E", "INV_QTY_COUNT", getUnitRemainControl());
                valid = false;
            }

            if (valid) {
                if ("Y".equals(ADFUtils.getBoundAttributeValue("NonUnqCntQty"))) {
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDCONT,
                                                    ctnRemain == null ? null :
                                                    (Integer) ADFUtils.getBoundAttributeValue(MAIN_REMAINING_CONT) -
                                                    ctnRemain);
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDUNITS,
                                                    unitRemain == null ? null :
                                                    (Integer) ADFUtils.getBoundAttributeValue(MAIN_REMAININGUNITS) -
                                                    unitRemain);
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGCONT, ctnRemain);
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGUNITS, unitRemain);
                }
                if ("C".equals(nextWorkStatus)) {
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDUNITS,
                                                    ADFUtils.getBoundAttributeValue(MAIN_REMAININGUNITS));
                    ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDCONT,
                                                    ADFUtils.getBoundAttributeValue(MAIN_REMAINING_CONT));
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGUNITS, Integer.valueOf(0));
                    ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGCONT, Integer.valueOf(0));

                }
                focusField(HhWorkProcessingSBean.OPERATOR);
            }
            refreshContentOfUIComponent(getPflMain());
        }

        return valid;
    }

    private boolean validateUser() {
        Boolean isValid = (Boolean) ADFUtils.findOperation("validateUser").execute();

        if (!isValid) {
            focusField(HhWorkProcessingSBean.OPERATOR);
            showMessage("E", "INV_USER_ID", getOperatorControl());
        }
        return isValid;
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    private boolean isNumericValue(String submittedVal) {
        return StringUtils.isEmpty(submittedVal) || submittedVal.matches("[0-9]+");
    }

    public String f3Action() {
        clearBlock();
        hideMessages();
        return "goMain";
    }

    private void clearBlock() {
        ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDCONT, null);
        ADFUtils.setBoundAttributeValue(WORKQTY_WORKEDUNITS, null);
        ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGCONT, null);
        ADFUtils.setBoundAttributeValue(WORKQTY_REMAININGUNITS, null);
        ADFUtils.setBoundAttributeValue(WORKQTY_CTNREMAIN, null);
        ADFUtils.setBoundAttributeValue(WORKQTY_UNITREMAIN, null);
        ADFUtils.setBoundAttributeValue("OperatorId", null);
    }
}
