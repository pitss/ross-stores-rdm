package com.ross.rdm.processing.hhworkprocessings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.IErrorMessageModel;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Main.jspx
// ---
// ---------------------------------------------------------------------
public class MainBacking extends WorkProcessingBaseBean {
    private RichInputText containerIdControl;
    private RichInputText nextWorkStatusControl;
    private RichInputText itemId;
    private RichPopup lovPopup;

    private static ADFLogger logger = ADFLogger.createADFLogger(MainBacking.class);
    private RichLink changeContainerHiddenLink;
    private RichLink changeWorkStatusHiddenLink;
    private RichLink f1Link;
    private RichLink f2Link;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichOutputText infoPopupText;
    private RichPopup infoPopup;

    private RichPanelFormLayout panelForm;

    private static final String WORKQTY_WORKEDCONT = "WorkQtyWorkedCont";
    private static final String MAIN_REMAINING_CONT = "RemainingCont";
    private static final String WORKQTY_WORKEDUNITS = "WorkQtyWorkedUnits";
    private static final String WORKQTY_REMAININGCONT = "WorkQtyRemainingCont";
    private static final String WORKQTY_REMAININGUNITS = "WorkQtyRemainingUnits";
    private static final String MAIN_REMAININGUNITS = "RemainingUnits";


    public MainBacking() {
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeContainerHiddenLink());
            actionEvent.queue();
        }
    }

    public void onChangedNextWorkStatus(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeWorkStatusHiddenLink());
            actionEvent.queue();
        }
    }

    public void f1ViewPvActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_MAIN_F1,
                                                RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)) {
            if (HhWorkProcessingSBean.CID.equals(getPageFlowBean().getMainFocus()) &&
                StringUtils.isEmpty((String) getContainerIdControl().getValue())) {
                showMessage("E", "PARTIAL_ENTRY", containerIdControl);
            } else {
                hideMessages();
                displayPv();
                navigateToAction("goWorkPv");
            }
        } else {
            this.showMessage("E", "NOT_ALLOWED", null);
        }


    }

    public void f2StatusActionListener(ActionEvent actionEvent) {

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_MAIN_F2,
                                                RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)) {
            if (HhWorkProcessingSBean.CID.equals(getPageFlowBean().getMainFocus()) &&
                StringUtils.isEmpty((String) getContainerIdControl().getValue())) {
                showMessage("E", "PARTIAL_ENTRY", containerIdControl);
            } else {
                hideMessages();
                getPageFlowBean().setDisableAllFields(true);
                refreshContentOfUIComponent(containerIdControl);
                refreshContentOfUIComponent(nextWorkStatusControl);
                lovPopup.show(new RichPopup.PopupHints());
            }
        } else {
            this.showMessage("E", "NOT_ALLOWED", null);
        }


    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_MAIN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)) {
            hideMessages();

            String nextWorkStatus = (String) ADFUtils.getBoundAttributeValue("NextWorkStatusRaw");
            if (HhWorkProcessingSBean.CID.equals(getPageFlowBean().getMainFocus()) &&
                StringUtils.isEmpty((String) getContainerIdControl().getValue())) {
                showMessage("E", "PARTIAL_ENTRY", getContainerIdControl());
            } else if (StringUtils.isEmpty(nextWorkStatus)) {
                focusField(HhWorkProcessingSBean.NWS);
                showMessage("E", "PARTIAL_ENTRY", getNextWorkStatusControl());
            } else {
                OperationBinding oper = ADFUtils.findOperation("checkWorkStatus");
                oper.getParamsMap().put("status", nextWorkStatus);
                Boolean isValid = (Boolean) oper.execute();
                if (Boolean.TRUE.equals(isValid)) {
                    if ("S".equals(nextWorkStatus)) {
                        if (!executeWorkStatus("startWork"))
                            return;
                        clearBlock();

                    } else if ("P".equals(nextWorkStatus)) {
                        goToWorkQty(HhWorkProcessingSBean.CTN_REMAIN);
                    } else if ("U".equals(nextWorkStatus)) {
                        if (!executeWorkStatus("resumeWork"))
                            return;
                        clearBlock();
                    } else if ("C".equals(nextWorkStatus)) {
                        goToWorkQty(HhWorkProcessingSBean.OPERATOR);
                    }
                } else {
                    focusField(HhWorkProcessingSBean.NWS);
                    showMessage("E", "STATUS_INV", getNextWorkStatusControl());
                }
            }
        } else {
            this.showMessage("E", "NOT_ALLOWED", null);
        }


    }

    private void clearBlock() {
        if (ADFUtils.findIterator("HhWorkProcessingSMainViewIterator") != null) {
            ADFUtils.findIterator("HhWorkProcessingSMainViewIterator").getViewObject().setNamedWhereClauseParam("bContainerId",
                                                                                                                null);
        }
        executeOperation("clearData");
        this.refreshContentOfUIComponent(this.panelForm);
    }

    private void goToWorkQty(String focus) {
        String nextWorkStatus = (String) ADFUtils.getBoundAttributeValue("NextWorkStatusRaw");
        String prevWorkStatus = nvl((String) ADFUtils.getBoundAttributeValue("PrevWorkStatus"), "~");
        if ("C".equals(nextWorkStatus)) {
            if (prevWorkStatus.equals("P")) {
                showMessage("E", "WRK_INV_CMP1", getFocusedInput());
                return;
            } else if (prevWorkStatus.equals("C")) {
                showMessage("E", "WRK_FINISHED_E", getFocusedInput());
                return;
            } else if (!prevWorkStatus.equals("S") && !prevWorkStatus.equals("U")) {
                showMessage("E", "WRK_INV_CMP2", getFocusedInput());
                return;
            }
        } else if ("P".equals(nextWorkStatus)) {
            if (prevWorkStatus.equals("P")) {
                showMessage("E", "WRK_INV_PAUSE", getFocusedInput());
                return;
            } else if (prevWorkStatus.equals("C")) {
                showMessage("E", "WRK_FINISHED_E", getFocusedInput());
                return;
            } else if (!prevWorkStatus.equals("S") && !prevWorkStatus.equals("U")) {
                showMessage("E", "WRK_INV_PAUSE1", getFocusedInput());
                return;
            }
        }

        String prevOperatorId = (String) ADFUtils.getBoundAttributeValue("PrevOperatorId");
        if (prevOperatorId != null && !getCurrentUser().getUserId().equals(prevOperatorId)) {
            showMessage("E", "WRK_IN_PRG", getFocusedInput());
            return;
        }

        if ("C".equals(nextWorkStatus)) {
            ADFUtils.setBoundAttributeValue("WorkQtyWorkedUnits", ADFUtils.getBoundAttributeValue("RemainingUnits"));
            ADFUtils.setBoundAttributeValue("WorkQtyWorkedCont", ADFUtils.getBoundAttributeValue("RemainingCont"));
            ADFUtils.setBoundAttributeValue("WorkQtyRemainingUnits", Integer.valueOf(0));
            ADFUtils.setBoundAttributeValue("WorkQtyRemainingCont", Integer.valueOf(0));
        } else if ("P".equals(nextWorkStatus)) {
            ADFUtils.setBoundAttributeValue("WorkQtyWorkedUnits", ADFUtils.getBoundAttributeValue("WorkedUnits"));
            ADFUtils.setBoundAttributeValue("WorkQtyWorkedCont", ADFUtils.getBoundAttributeValue("WorkedCont"));
            ADFUtils.setBoundAttributeValue("WorkQtyRemainingUnits", ADFUtils.getBoundAttributeValue("RemainingUnits"));
            ADFUtils.setBoundAttributeValue("WorkQtyRemainingCont", ADFUtils.getBoundAttributeValue("RemainingCont"));
        }

        if (focus != null) {
            getPageFlowBean().setWorkQtyFocus(focus);
        }
        hideMessages();
        removeErrorStyleToComponent(getNextWorkStatusControl());
        navigateToAction("goWorkQty");
    }


    /**
     * Check whether we have valid data to work with
     */
    private boolean checkValidData() {
        if (!hasData()) {
            if (fieldErrorExists())
                return false;
            showMessage("E", "PARTIAL_ENTRY", containerIdControl);
            return false;
        }

        return true;
    }

    @Override
    protected void hideMessages() {
        super.hideMessages();
        removeErrorStyleToComponent(containerIdControl);
        removeErrorStyleToComponent(nextWorkStatusControl);
    }

    // do we have a valid container?
    private boolean hasData() {
        return null != ADFUtils.getBoundAttributeValue("ContainerId");
    }

    // tests whether one of the input fields has error
    private boolean fieldErrorExists() {
        IErrorMessageModel pageFlowBean = getPageFlowBean();
        return "error".equals(pageFlowBean.getMessageIcon()) && null != pageFlowBean.getFocusControl();
    }

    @Override
    protected HhWorkProcessingSBean getPageFlowBean() {
        return (HhWorkProcessingSBean) getPageFlowBean("HhWorkProcessingSBean");
    }

    /**
     *  Helper method to DRY business logic invocations
     */
    private boolean executeWorkStatus(String operation) {
        if (!executeOperation(operation)) {
            logger.severe("Could not execute operation '" + operation + "'");
            return false;
        }

        String result = (String) getOperationResult(operation);
        if (null != result) {
            showMessage(result.substring(0, 1), result.substring(1), getFocusedInput());
            return false;
        }

        executeOperation("clearData");
        getPageFlowBean().setContainerId("");
        focusField(HhWorkProcessingSBean.CID);

        return true;
    }

    public void lovPopupCanceled(PopupCanceledEvent popupCanceledEvent) {
        reenableFields();
    }

    public void lovDialogListener(DialogEvent dialogEvent) {
        reenableFields();
    }

    private void reenableFields() {
        getPageFlowBean().setDisableAllFields(false);
        refreshContentOfUIComponent(nextWorkStatusControl);
        refreshContentOfUIComponent(containerIdControl);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////

    public void setContainerIdControl(RichInputText containerIdControl) {
        this.containerIdControl = containerIdControl;
    }

    public RichInputText getContainerIdControl() {
        return containerIdControl;
    }

    public void setNextWorkStatusControl(RichInputText nextWorkStatusControl) {
        this.nextWorkStatusControl = nextWorkStatusControl;
    }

    public RichInputText getNextWorkStatusControl() {
        return nextWorkStatusControl;
    }

    public void setLovPopup(RichPopup lovPopup) {
        this.lovPopup = lovPopup;
    }

    public RichPopup getLovPopup() {
        return lovPopup;
    }

    public void changeContainerHiddenAction(ActionEvent actionEvent) {
        if (!getPageFlowBean().getExecutedKey()) {
            if (StringUtils.isNotEmpty(getPageFlowBean().getContainerId())) {
                hideMessages();
                if (!executeOperation("ExecuteWithParams")) {
                    logger.severe("Could not execute operation 'ExecuteWithParams'");
                    return;
                }

                int result = (Integer) ADFUtils.getBoundAttributeValue("Result");
                String errorMsgTyp = (String) ADFUtils.getBoundAttributeValue("ErrorMsgTyp");
                String errorMsg = (String) ADFUtils.getBoundAttributeValue("ErrorMsg");

                //OTM2775, 2873 (change LPN to its MLP)

                getPageFlowBean().setContainerId((String) ADFUtils.getBoundAttributeValue("ContainerId"));
                refreshContentOfUIComponent(getContainerIdControl());
                refreshContentOfUIComponent(getItemId());
                this.refreshContentOfUIComponent(this.getPanelForm());

                if (0 == result) { // error
                    if ("E".equals(errorMsgTyp)) {
                        getPageFlowBean().setContainerId("");
                        showMessage("E", errorMsg, containerIdControl);
                    } else {
                        getPageFlowBean().setDisableAllFields(true);
                        refreshContentOfUIComponent(getContainerIdControl());
                        getInfoPopupText().setValue(this.getMessage(errorMsg, null, null,
                                                                    null) /* JSFUtils.resolveExpression("#{msgBundle['" + errorMsg + "']}") */);
                        getInfoPopup().show(new RichPopup.PopupHints());
                        //OTM 2530
                        /* showMessage("I", errorMsg, null);
                        getPageFlowBean().setReturnTo("goMain");
                        navigateToAction("goWorkEditPv"); */
                    }
                } else {
                    focusField(HhWorkProcessingSBean.NWS);
                }
            } else {
                showMessage("E", "PARTIAL_ENTRY", getContainerIdControl());
            }
        }
    }

    public void changeWorkStatusHiddenAction(ActionEvent actionEvent) {
        if (!getPageFlowBean().getExecutedKey()) {
            hideMessages();
            focusField(HhWorkProcessingSBean.CID);
            refreshContentOfUIComponent(nextWorkStatusControl);
        }
    }

    public void setChangeContainerHiddenLink(RichLink changeContainerHiddenLink) {
        this.changeContainerHiddenLink = changeContainerHiddenLink;
    }

    public RichLink getChangeContainerHiddenLink() {
        return changeContainerHiddenLink;
    }

    public void setChangeWorkStatusHiddenLink(RichLink changeWorkStatusHiddenLink) {
        this.changeWorkStatusHiddenLink = changeWorkStatusHiddenLink;
    }

    public RichLink getChangeWorkStatusHiddenLink() {
        return changeWorkStatusHiddenLink;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF1Link());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2Link());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("cid".equals(item)) {
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedContainerId(vce);
                    }
                } else if ("nex".equals(item)) {
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        ValueChangeEvent vce = new ValueChangeEvent(field, null, submittedVal);
                        this.onChangedNextWorkStatus(vce);
                    }
                }
            }
        }
    }

    public void setF1Link(RichLink f1Link) {
        this.f1Link = f1Link;
    }

    public RichLink getF1Link() {
        return f1Link;
    }

    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    private void focusField(String focus) {
        getPageFlowBean().setMainFocus(focus);
        onRegionLoad(null); //FORCE FOCUS
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledCid()) {
            setFocusOnUIComponent(getContainerIdControl());
        } else if (!getPageFlowBean().getDisabledNws()) {
            setFocusOnUIComponent(getNextWorkStatusControl());
        }
    }

    public void performKeySelectStatus(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                if (StringUtils.isNotEmpty(submittedValue)) {
                    ADFUtils.setBoundAttributeValue("NextWorkStatusRaw", submittedValue);
                }
                getPageFlowBean().setMainFocus(HhWorkProcessingSBean.NWS);
                reenableFields();
                getLovPopup().hide();
                onRegionLoad(null); //FORCE FOCUS
            }
        }
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_processing_hhworkprocessings_view_pageDefs_MainPageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public void setInfoPopupText(RichOutputText infoPopupText) {
        this.infoPopupText = infoPopupText;
    }

    public RichOutputText getInfoPopupText() {
        return infoPopupText;
    }

    public void setInfoPopup(RichPopup infoPopup) {
        this.infoPopup = infoPopup;
    }

    public RichPopup getInfoPopup() {
        return infoPopup;
    }

    public String okInfoPopupAction() {
        getInfoPopup().hide();
        getPageFlowBean().setDisableAllFields(false);
        getPageFlowBean().setReturnTo("goMain");
        return "goWorkEditPv";
    }

    private RichInputText getFocusedInput() {
        return !getPageFlowBean().getDisabledCid() ? getContainerIdControl() : getNextWorkStatusControl();
    }

    private String nvl(String value, String defaultValue) {
        return value != null ? value : defaultValue;
    }

    public void setItemId(RichInputText itemId) {
        this.itemId = itemId;
    }

    public RichInputText getItemId() {
        return itemId;
    }

    public void setPanelForm(RichPanelFormLayout panelForm) {
        this.panelForm = panelForm;
    }

    public RichPanelFormLayout getPanelForm() {
        return panelForm;
    }
}
