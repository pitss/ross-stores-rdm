package com.ross.rdm.processing.hhworkprocessings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.processing.view.framework.RDMProcessingBackingBean;

import javax.faces.event.ActionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for WorkPv.jspx
// ---
// ---------------------------------------------------------------------
public class WorkPvBacking extends RDMProcessingBackingBean {

    public WorkPvBacking() {
    }

    public void f2EditActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_WORK_PV_F2,RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)){
                          hideMessages();
                          String prevWorkStatus = (String) ADFUtils.getBoundAttributeValue("PrevWorkStatus");
                          if ("C".equals(prevWorkStatus)) {
                              showMessage("E", "OPER_NOT_ALLOW", null);
                              return;
                          }

                          String pvCode = (String) ADFUtils.findOperation("getPvCode").execute();
                          if (pvCode != null) {
                              pvCode = pvCode.replace(' ', (char) 10);
                          }
                          ADFUtils.setBoundAttributeValue("PvCode", pvCode);

                          getPageFlowBean().setReturnTo("goWorkPv");
                          navigateToAction("goWorkEditPv");  
                      }else{
                          this.showMessage("E", "NOT_ALLOWED", null);
                      }
        
       
    }

    @Override
    protected HhWorkProcessingSBean getPageFlowBean() {
        return (HhWorkProcessingSBean) getPageFlowBean("HhWorkProcessingSBean");
    }

    public String f3Action() {
        hideMessages();
        getPageFlowBean().setMainFocus(HhWorkProcessingSBean.CID);
        return "goMain";
    }
}
