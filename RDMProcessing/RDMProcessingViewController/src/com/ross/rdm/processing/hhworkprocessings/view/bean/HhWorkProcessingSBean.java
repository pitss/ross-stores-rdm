package com.ross.rdm.processing.hhworkprocessings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.ErrorMessageModelBase;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------

public class HhWorkProcessingSBean extends ErrorMessageModelBase {
    public static final String CID = "CID";
    public static final String NWS = "NWS";
    public static final String OPERATOR = "OPERATOR";
    public static final String CTN_REMAIN = "CTN_REMAIN";
    public static final String UNIT_REMAIN = "UNIT_REMAIN";
    public static final String SUPERVISOR = "SUPERVISOR";
    public static final String PASSWORD = "PASSWORD";

    // input parameter
    //Defect 2522 - FBWRAP instead of MRKING
    // private String activityCd = "MRKING";
    private String activityCd = "FBWRAP";

    // search fields
    private String containerId;

    // ADF quirk workaround: empty fields backing
    private String emptyField;

    // where should we return from PV_EDIT screen? ("goMain" or "goWorkPv")
    private String returnTo;

    // This is used as a flag so we don't execute valueChangeListener if a F-Key is pressed (OTM 2200)
    private Boolean executedKey = Boolean.FALSE;

    private String mainFocus = CID;

    private String workQtyFocus = null;

    private String userAuthFocus = null;

    private boolean disableAllFields = false;
    
    private String newPvs;

    // global setting
    public void initializeActivityCd() {
        String globalActivityId = (String) ADFUtils.getBoundAttributeValue("ActivityCode");
        if (null != globalActivityId) {
            setActivityCd(globalActivityId);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setEmptyField(String emptyField) {
        this.emptyField = emptyField;
    }

    public String getEmptyField() {
        return emptyField;
    }

    public void setActivityCd(String activityCd) {
        this.activityCd = activityCd;
    }

    public String getActivityCd() {
        return activityCd;
    }

    public void setReturnTo(String returnTo) {
        this.returnTo = returnTo;
    }

    public String getReturnTo() {
        return returnTo;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }

    public void setMainFocus(String mainFocus) {
        this.mainFocus = mainFocus;
    }

    public String getMainFocus() {
        return mainFocus;
    }

    public Boolean getDisabledCid() {
        return disableAllFields || !CID.equals(getMainFocus());
    }

    public Boolean getDisabledNws() {
        return disableAllFields || !NWS.equals(getMainFocus());
    }

    public void setDisableAllFields(boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public boolean isDisableAllFields() {
        return disableAllFields;
    }

    public void setWorkQtyFocus(String workQtyFocus) {
        this.workQtyFocus = workQtyFocus;
    }

    public String getWorkQtyFocus() {
        return workQtyFocus;
    }

    public Boolean getDisabledOperator() {
        return !OPERATOR.equals(getWorkQtyFocus());
    }

    public Boolean getDisabledCtnRemain() {
        return !CTN_REMAIN.equals(getWorkQtyFocus());
    }

    public Boolean getDisabledUnitRemain() {
        return !UNIT_REMAIN.equals(getWorkQtyFocus());
    }

    public void setUserAuthFocus(String userAuthFocus) {
        this.userAuthFocus = userAuthFocus;
    }

    public String getUserAuthFocus() {
        return userAuthFocus;
    }

    public Boolean getDisabledSupervisor() {
        return !SUPERVISOR.equals(getUserAuthFocus());
    }

    public Boolean getDisabledPassword() {
        return !PASSWORD.equals(getUserAuthFocus());
    }
    
    public void setNewPvs(String newPvs) {
        this.newPvs = newPvs;
    }

    public String getNewPvs() {
        return newPvs;
    }
}
