package com.ross.rdm.processing.hhworkprocessings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for WorkEditPv.jspx
// ---    
// ---------------------------------------------------------------------
public class WorkEditPvBacking extends WorkProcessingBaseBean {
    
    private static ADFLogger logger = ADFLogger.createADFLogger(WorkEditPvBacking.class);
    private RichInputText pvCodeControl;

    public WorkEditPvBacking() {
  }

    public void f1ClearActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_WORK_EDIT_PV_F1,RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)){
                          hideMessages();
                          ADFUtils.setBoundAttributeValue("PvCode", null);
                          setFocusOnUIComponent(getPvCodeControl());
                      }else{
                          this.showMessage("E", "NOT_ALLOWED", null);
                      }
   }
    
    public void f3ExitActionListener(ActionEvent actionEvent) {
        hideMessages();
        if ("goMain".equals(getPageFlowBean().getReturnTo())) {
            getPageFlowBean().setContainerId(null);
            executeOperation("clearData");
        }
        navigateBack();
    }    

    public void f4DoneActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_WORK_EDIT_PV_F4,RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)){
                          hideMessages();
                          processPvDone("Y"); //NS:9/1/2016, 3837 prod issue, processPvDone will return CODES_MISSING with commit flag = N
                          
                          String result = (String)getOperationResult("processPvDone");       
                          if (null != result) {
                              showMessage(result.substring(0, 1), result.substring(1), pvCodeControl);
                              this.selectTextOnUIComponent(pvCodeControl);
                              return;
                          }
                          
                          String standAloneCnt = (String)ADFUtils.getBoundAttributeValue("StandAloneCnt");
                          String workPvEdit = (String)ADFUtils.getBoundAttributeValue("WorkPvEdit");
                          if (!"Y".equals(standAloneCnt) && "Y".equals(workPvEdit)) {
                              getPageFlowBean().setUserAuthFocus(HhWorkProcessingSBean.SUPERVISOR);
                              getPageFlowBean().setNewPvs((String) getPvCodeControl().getValue());
                              navigateToAction("goUserAuth");
                          } else {
                              processPvDone("Y");
                              if (!executeOperation("Commit"))
                                  return; // stay on this page
                              if ("goWorkPv".equals(getPageFlowBean().getReturnTo())) {
                                  //executeOperation("Execute");
                                  // reload the changed value(s)
                                  displayPv();
                              }
                              navigateBack();          
                          } 
                      }else{
                          this.showMessage("E", "NOT_ALLOWED", null);
                      }
        
       
    }

    private void processPvDone(String commit) {
        OperationBinding oper = ADFUtils.findOperation("processPvDone");
        oper.getParamsMap().put("facility", JSFUtils.resolveExpression("#{CurrentUser.facilityId}"));
        oper.getParamsMap().put("container", JSFUtils.resolveExpression("#{pageFlowScope.HhWorkProcessingSBean.containerId}"));
        oper.getParamsMap().put("activity", JSFUtils.resolveExpression("#{pageFlowScope.HhWorkProcessingSBean.activityCd}"));
        oper.getParamsMap().put("item", JSFUtils.resolveExpression("#{bindings.ItemId.inputValue}"));
        oper.getParamsMap().put("pvCode", getPvCodeControl().getValue());
        oper.getParamsMap().put("commit", commit);
        oper.execute();
    }
    
    private void navigateBack() {
        if ("goMain".equals(getPageFlowBean().getReturnTo())) {
            getPageFlowBean().setFocusControl("nex");
        }
        navigateToAction(getPageFlowBean().getReturnTo());          
    }
    
    @Override    
    protected HhWorkProcessingSBean getPageFlowBean() {
        return (HhWorkProcessingSBean)getPageFlowBean("HhWorkProcessingSBean");
    }
    
    @Override
    protected void hideMessages() {        
        super.hideMessages();
        removeErrorStyleToComponent(pvCodeControl);
    }    
    
    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////                

    public void setPvCodeControl(RichInputText pvCodeControl) {
        this.pvCodeControl = pvCodeControl;
    }

    public RichInputText getPvCodeControl() {
        return pvCodeControl;
    }
    
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        setFocusOnUIComponent(getPvCodeControl());
    }
    
    @Override
    protected void setFocusOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("SetFocusOnUIcomp(textInput);"); //changed selectFocusOnComp to selectFocusonUIComp fo IE using jquery
        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }
}
