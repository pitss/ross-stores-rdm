package com.ross.rdm.processing.hhworkprocessings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.processing.view.framework.RDMProcessingBackingBean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for UserAuth.jspx
// ---
// ---------------------------------------------------------------------
public class UserAuthBacking extends WorkProcessingBaseBean {

    private static ADFLogger logger = ADFLogger.createADFLogger(UserAuthBacking.class);
    private RichInputText supervisorControl;
    private RichInputText passwordControl;
    private RichLink f3Link;
    private RichLink f4Link;

    public UserAuthBacking() {
    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_work_processing_s_USER_AUTH_F4,RoleBasedAccessConstants.FORM_NAME_hh_work_processing_s)){
                          if (!executeOperation("validateUser")) {
                              logger.severe("Could not execute operation 'validateUser'");
                              return;
                          }

                          if (!(Boolean) getOperationResult("validateUser")) {
                              ADFUtils.setBoundAttributeValue("SupervisorId", null);
                              showMessage("E", "INV_USER_ID", getFocusedField());
                              return;
                          }

                          if (!executeOperation("checkUserPassword")) {
                              logger.severe("Could not execute operation 'checkUserPassword'");
                              return;
                          }

                          String result = (String) getOperationResult("checkUserPassword");
                          if (null != result) {
                              if ("INV_UID_PASS".equals(result)) {
                                  ADFUtils.setBoundAttributeValue("SupervisorId", "");
                                  ADFUtils.setBoundAttributeValue("Password", "");
                                  focusField(HhWorkProcessingSBean.SUPERVISOR);
                                  showMessage("E", "INV_UID_PASS", supervisorControl);
                                  return;
                              } else if ("ACCESS_DENIED".equals(result)) {
                                  showMessage("E", "ACCESS_DENIED", getFocusedField());
                                  return;
                              }
                          }
                          processPvDone("Y");
                          ADFUtils.findOperation("updateWorkActivityAudit").execute();
                          ADFUtils.setBoundAttributeValue("MainSupervisorId", ADFUtils.getBoundAttributeValue("SupervisorId"));
                          ADFUtils.findOperation("displayPv").execute();

                          navigateToAction(getPageFlowBean().getReturnTo());   
                      }else{
                          this.showMessage("E", "NOT_ALLOWED", null);
                      }
        
        
        
    }

    @Override
    protected HhWorkProcessingSBean getPageFlowBean() {
        return (HhWorkProcessingSBean) getPageFlowBean("HhWorkProcessingSBean");
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////

    public void setSupervisorControl(RichInputText supervisorControl) {
        this.supervisorControl = supervisorControl;
    }

    public RichInputText getSupervisorControl() {
        return supervisorControl;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledSupervisor()) {
            setFocusOnUIComponent(getSupervisorControl());
        } else {
            setFocusOnUIComponent(getPasswordControl());
        }
    }

    public void setPasswordControl(RichInputText passwordControl) {
        this.passwordControl = passwordControl;
    }

    public RichInputText getPasswordControl() {
        return passwordControl;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("sup1".equals(item)) {
                    focusField(HhWorkProcessingSBean.PASSWORD);
                } else if ("pas1".equals(item)) {
                    focusField(HhWorkProcessingSBean.SUPERVISOR);
                }
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    @Override
    protected void hideMessages() {
        super.hideMessages();
        removeErrorStyleToComponent(getSupervisorControl());
        removeErrorStyleToComponent(getPasswordControl());
    }

    public String f3Action() {
        ADFUtils.findOperation("rollbackToSavepoingWorkpv").execute();
        hideMessages();
        return "goWorkEditPv";
    }

    private void focusField(String focus) {
        hideMessages();
        getPageFlowBean().setUserAuthFocus(focus);
        refreshContentOfUIComponent(getSupervisorControl());
        refreshContentOfUIComponent(getPasswordControl());
    }

    private RichInputText getFocusedField() {
        return !getPageFlowBean().getDisabledSupervisor() ? getSupervisorControl() : getPasswordControl();
    }
    
    private void processPvDone(String commit) {
        OperationBinding oper = ADFUtils.findOperation("processPvDone");
        oper.getParamsMap().put("facility", JSFUtils.resolveExpression("#{CurrentUser.facilityId}"));
        oper.getParamsMap().put("container", JSFUtils.resolveExpression("#{pageFlowScope.HhWorkProcessingSBean.containerId}"));
        oper.getParamsMap().put("activity", JSFUtils.resolveExpression("#{pageFlowScope.HhWorkProcessingSBean.activityCd}"));
        oper.getParamsMap().put("item", JSFUtils.resolveExpression("#{bindings.ItemId.inputValue}"));
        oper.getParamsMap().put("pvCode", getPageFlowBean().getNewPvs());
        oper.getParamsMap().put("commit", commit);
        oper.execute();
    }
}
