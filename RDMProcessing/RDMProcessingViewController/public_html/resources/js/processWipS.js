function onKeyPressedProcessWipView(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
        true);
    }
    else{
        // "normal" handling
        onHotKeyPressed(event);
    }
    event.cancel();
}

function onOpenPopupOnProcessWipView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup');
    linkComp.focus();
}

function onKeyPressedProcessWipViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}