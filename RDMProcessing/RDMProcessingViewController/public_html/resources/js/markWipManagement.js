function onKeyPressedOnStatusLovMarkWipMgmt(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    //Workaround to fix ENTER not working in IE 11 Enterprise Mode
    if(keyPressed == 0 && navigator.userAgent.indexOf("Trident/4.0") != -1){
        keyPressed = AdfKeyStroke.ENTER_KEY;
    }

    if(keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY){
        if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
            AdfCustomEvent.queue(component, "customKeyEvent", 
            {
                keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
            },
    true);
        }
        event.cancel();
    }
}

function onOpenPopupMarkStatus(event) {
    var component = ("[id*='socStat']");
    $(component).attr('size', 8);
    SetFocusOnUIcomp(comp);
}

function onKeyPressedMarkWipMainView(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyPressedMarkWipMarkQtyView(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onOpenConfirmPopupMarkWip(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup');
    linkComp.focus();
}

function onOpenVariousMessagesPopupMarkWip(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup2');
    linkComp.focus();
}

function onKeyPressedMarkWipMainViewConfirmPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent",  { keyPressed : keyPressed }, true);
    }
    event.cancel();
}

function setMarkPvInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedOnF3Exit(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('f3'), true);
        event.cancel();
    }
}

function onKeyPressedMarkWipMainViewMessagesPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent",  { keyPressed : keyPressed }, true);
    }
    event.cancel();
}