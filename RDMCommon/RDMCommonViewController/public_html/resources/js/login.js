 
function loginKeyHandler(evt) {
    var srcComponent = evt.getSource();
    var keyPressed = evt.getKeyCode();
    var userIdField = srcComponent.findComponent('it1');
    var passField = srcComponent.findComponent('it2');
    var inputHasFocusid = (AdfPage.PAGE.getActiveComponent()).getClientId();
    switch (keyPressed) {
        case (AdfKeyStroke.ENTER_KEY):
            if ('it1' == inputHasFocusid) {
                setFocusByKey(inputHasFocusid, passField, userIdField);
            }
            else if ('it2' == inputHasFocusid) {
                var logLink = srcComponent.findComponent('cb1');
                AdfActionEvent.queue(logLink, true);
            }
            evt.cancel();
            break;
        case (AdfKeyStroke.TAB_KEY):
            if ('it1' == inputHasFocusid) {
                setFocusByKey(inputHasFocusid, passField, userIdField);
            }
            else if ('it2' == inputHasFocusid) {
                var fink = srcComponent.findComponent('cb1');
                AdfActionEvent.queue(fink, true);
            }
            evt.cancel();
            break;
        case (AdfKeyStroke.ARROWDOWN_KEY):
            setFocusByKey(inputHasFocusid, passField, userIdField);
            break;
        case (AdfKeyStroke.ARROWUP_KEY):
            setFocusByKey(inputHasFocusid, passField, userIdField);
            evt.cancel();
            break;
        case (AdfKeyStroke.F3_KEY):
            var f3Exit = srcComponent.findComponent('cb14');
            AdfActionEvent.queue(f3Exit, true);
            evt.cancel();
            break;
        case (AdfKeyStroke.F1_KEY):
            var exitPopup = srcComponent.findComponent('p1');
            var fLink = srcComponent.findComponent('cb1');
            if (exitPopup.isPopupVisible())
                fLink = srcComponent.findComponent('ylp');
            AdfActionEvent.queue(fLink, true);
            evt.cancel();
            break;
        case (AdfKeyStroke.F2_KEY):
            fLink = srcComponent.findComponent('nlp');
            AdfActionEvent.queue(fLink, true);
            userIdField.focus();
            evt.cancel();
            break;
        default :
            break;
    }
}
function hidePopupLog(evt) {
    var srcComponent = evt.getSource();
    var confirmExitPopup = srcComponent.findComponent('p1');
    if (confirmExitPopup != null) {
        confirmExitPopup.hide();
         var userIdField = srcComponent.findComponent('it1');
         userIdField.focus();
    }
    evt.cancel();
}

function hidePopupLogonExit(evt) {
    var srcComponent = evt.getSource();
    var confirmExitPopup = srcComponent.findComponent('p1');
    if (confirmExitPopup != null) {
        confirmExitPopup.hide();
         
    }
    evt.cancel();
}
function onPopupOpened(event) {
    var srcComponent = event.getSource();
    var popup = srcComponent.findComponent('p1');
    popup.focus();
}

function setFocusByKey(inputHasFocusid, passField, userIdField) {
    if ('it1' == inputHasFocusid) {
        passField.focus();
    }
    else 
        userIdField.focus();
}
function suppressAutoComplete(evt){
 var domElement =
     AdfRichUIPeer.getDomContentElementForComponent(evt.getSource());
 domElement.setAttribute("autocomplete", "off" ); 
}


function setWindowSize() {
    var height = document.documentElement.clientHeight, width = document.documentElement.clientWidth;
    var ratio = window.devicePixelRatio;
    comp = AdfPage.PAGE.findComponentByAbsoluteId('d112');
    AdfCustomEvent.queue(comp, "setScreenSize", 
    {
        'screenWidth' : width, 'screenHeight' : height, 'devicePixelRatio' : ratio
    },
true);
}

function okLinkGoPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('ylp1'), true);
    }else if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfActionEvent.queue(component.findComponent('nlp1'), true);
    }
    event.cancel();
}

function onPopupOpened2(event) {
    var srcComponent = event.getSource();
    var popup = srcComponent.findComponent('p2');
    popup.focus();
}