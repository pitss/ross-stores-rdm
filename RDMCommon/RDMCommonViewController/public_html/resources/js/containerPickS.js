// PAGE1 VIEW JS --------------------------------------------------
function onKeyPressedOnMasterContainer(event) {
    this.onKeyPressedContainerPickPage1(event, 'mcid');
}

function onKeyPressedOnFirstContainer(event) {
    this.onKeyPressedContainerPickPage1(event, 'fcid');
}

function onKeyPressedOnLastContainer(event) {
    this.onKeyPressedContainerPickPage1(event, 'lcid');
}

function onKeyPressedContainerPickPage1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyPressedOnPage1ViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenPopup1Page1View(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup1');
    linkComp.focus();
}

function onOpenPopup2Page1View(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup2');
    linkComp.focus();
}
// END PAGE1 VIEW JS --------------------------------------------------

// TO_LOCATION VIEW JS --------------------------------------------------
function onKeyPressedOnToLocationId(event) {
    this.onKeyPressedContainerPickToLocation(event, 'lid');
}

var globalKeyPressedToLocation = null;
function onKeyPressedOnPickToContainerId(event) {
    var keyPressed = event.getKeyCode();
    if(globalKeyPressedToLocation == AdfKeyStroke.F4_KEY && (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY)){
        globalKeyPressedToLocation = null;
    }
    else if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        globalKeyPressedToLocation = event.getKeyCode();
        this.onKeyPressedContainerPickToLocation(event, 'ncid');
    }
    else{
        globalKeyPressedToLocation = null;
    }
}

function onKeyPressedContainerPickToLocation(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function setJavaScriptGlobalKeyPressedToLocation(event){
    globalKeyPressedToLocation = AdfKeyStroke.F4_KEY;
}

function onKeyPressedOnToLocationViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenPopupCEOnToLocationView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopupCE');
    linkComp.focus();
}

function onOpenSuccessPopupToLocationView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkExitPopup');
    linkComp.focus();
}

function onOpenWarningPopupToLocationView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkWarningPopup');
    linkComp.focus();
}

function onKeyPressedOnToLocationViewSuccessPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}
// END TO_LOCATION VIEW JS --------------------------------------------------
// REASON VIEW JS --------------------------------------------------
function onClickOnCodeLovContainerPickReason(event) {
    openListOfValues(findLovContainerPickReason());
}

function findLovContainerPickReason() {
    var lovIdcodeLovCP = 'codeLovCP';
    var lovIdcodeLovCPMark = 'codeLovCPMark';
    var codeLovCpLov = ("[id*='" + lovIdcodeLovCP + "']");
    var codeLovCpMarkLov = ("[id*='" + lovIdcodeLovCPMark + "']");
    if ($(codeLovCpLov).size() > 0){
        openListOfValues(codeLovCpLov);
        return $(codeLovCpLov);
    }
    else{
        openListOfValues(codeLovCpMarkLov);
        return $(codeLovCpMarkLov);
    }    
}

function setReasonInitialFocusContainerPick() {
    setTimeout(function () {
        var focusOn = findLovContainerPickReason();
        if (focusOn != null) {
            openListOfValues(focusOn);
        }
    },
0);
}

function onKeyPressedOnCodeLovContainerPick(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if(keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY){
        if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
            AdfCustomEvent.queue(component, "customKeyEvent", 
            {
                keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
            },
            true);
        }
        event.cancel();
    }
}

function onOpenExitPopupContainerPickReasonView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkExitPopup');
    linkComp.focus();
}

function onKeyPressedOnContainerPickReasonViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

/* method called on pageload from backingBean onRegionLoad method */
function expandSelectOneChoiceContainerPickReason(inputComp) {
  setTimeout(function () {
      var elem = findInputElementById(inputComp);
      openListOfValues(elem); 
  },0);
} 
// END REASON VIEW JS --------------------------------------------------
// FROM_MASTER_CHILD VIEW JS --------------------------------------------------
function onKeyPressedOnFromMasterChildView(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
    }
    event.cancel();
}

function onKeyPressedOnFromMasterChildViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenPopupOnFromMasterChildView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopup');
    linkComp.focus();
}

function onOpenPopupCEOnFromMasterChildView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopupCE');
    linkComp.focus();
}
// END FROM_MASTER_CHILD VIEW JS --------------------------------------------------
// FROM_MASTER_CHILD_2 VIEW JS --------------------------------------------------
function onKeyPressedOnFromMasterChild2View(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
    }
    event.cancel();
}

function onKeyPressedOnFromMasterChildView2PopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenPopupOnFromMasterChild2View(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopup');
    linkComp.focus();
}

function onOpenPopupCEOnFromMasterChild2View(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopupCE');
    linkComp.focus();
}
// END FROM_MASTER_CHILD_2 VIEW JS --------------------------------------------------
// TO_MASTER_CHILD VIEW JS --------------------------------------------------
function onKeyPressedOnToMasterChildContainerPickS(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
    }
    event.cancel();
}

function onKeyPressedOnToMasterChildViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenPopupOnToMasterChildView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopup');
    linkComp.focus();
}

function onOpenPopupCEOnToMasterChildView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopupCE');
    linkComp.focus();
}
// END TO_MASTER_CHILD VIEW JS --------------------------------------------------
// WEIGHT VIEW JS --------------------------------------------------
function onKeyPressedOnTotalWeight(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
    }
    event.cancel();
}

function onOpenPopupCEOnWeightView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopupCE');
    linkComp.focus();
}

function onKeyPressedOnWeightViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}
// END WEIGHT VIEW JS --------------------------------------------------
// ITEM_DESCRIPTION VIEW JS --------------------------------------------------
function onKeyPressedOnItemDescriptionView(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}
// END ITEM_DESCRIPTION VIEW JS --------------------------------------------------
// CONTAINER_PICK_TWO VIEW JS --------------------------------------------------
function onKeyPressedOnLastPickContainerPick2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
    }
    event.cancel();
}

function onKeyPressedOnContainerPickTwoViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenPopupOnContainerPickTwoView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopup');
    linkComp.focus();
}

function onOpenPopupFMCOnContainerPickTwoView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('noLinkPopupFMC');
    linkComp.focus();
}

function onOpenExitPopupContainerPickTwoView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkExitPopup');
    linkComp.focus();
}

function onKeyPressedOnContainerPickTwoViewExitPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}
// END CONTAINER_PICK_TWO VIEW JS --------------------------------------------------
// CONTAINER_PICK VIEW JS --------------------------------------------------
function onKeyPressedGenericToContainer(event) {
    this.onKeyPressedContainerPickView(event, 'gtocid');
}

function onKeyPressedPickToContainerId(event) {
    this.onKeyPressedContainerPickView(event, 'ptcid');
}

function onKeyPressedConfFromContainerId(event) {
    this.onKeyPressedContainerPickView(event, 'cfcid');
}

function onKeyPressedContainerPickView(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
        true);
    }
    event.cancel();
}

function onKeyPressedOnContainerPickViewPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onOpenExitPopupContainerPickView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkExitPopup');
    linkComp.focus();
}

function onF1PressedPopUpContainerPick(){
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('yesLinkExitPopup'), true);
    }
    event.cancel();
}

function onOpenSuccessPopupContainerPickView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkExitPopup2');
    linkComp.focus();
}

function onOpenDmsErrorPopupContainerPickView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesDMSLink');
    linkComp.focus();
}
// END CONTAINER_PICK VIEW JS --------------------------------------------------
// CONTAINER_ID VIEW JS --------------------------------------------------
function onKeyPressedOnContainerIdView(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('f3'), true);        
    }
    event.cancel();
}
// END CONTAINER_ID VIEW JS --------------------------------------------------
// Go To Location confirm popup
function onOpenToLocContainerPick(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('oklp');
    linkComp.focus();
}

function okLinkGoPopupContainerPick(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('oklp'), true);
    }
    event.cancel();
}
//End