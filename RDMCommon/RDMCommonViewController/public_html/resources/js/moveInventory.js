function customFindElementByIdMove(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function setPage1MoveFocus() {
    setTimeout(function () {
        var focusOn = customFindElementByIdMove('focus');       
        if ((focusOn != null) && (focusOn.value != null)) {
            var focusOnValue = focusOn.value;
            if (focusOnValue == 'inittask' || focusOnValue == 'containerId') {
                setFocusOrSelectOnUIcomp(customFindElementByIdMove('it2'));
            }
            else if (focusOnValue == 'toLocation') {
                setFocusOrSelectOnUIcomp(customFindElementByIdMove('it7'));
            }
        }
    },
0);
}

function setPage2MoveInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementByIdMove('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setPage3MoveInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementByIdMove('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}
var globalKeyPressed = null;
function onKeyPressedOnContainerMoveInventory(event) {
    var keyPressed = event.getKeyCode();
    if(globalKeyPressed == AdfKeyStroke.F4_KEY && keyPressed == AdfKeyStroke.ENTER_KEY){
        globalKeyPressed = null;
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F7_KEY|| keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY) {
        globalKeyPressed = event.getKeyCode();
        this.onKeyPressedMoveInventory(event, 'containerId');
    }
    else{
        globalKeyPressed = null;
    }
}

function setJavaScriptGlobalKeyPressed(event){
    globalKeyPressed = AdfKeyStroke.F4_KEY;
}

function onKeyPressedOnLocationMoveInventory(event) {
    this.onKeyPressedMoveInventory(event, 'locationTo');
}

function onKeyPressedMoveInventory(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F7_KEY|| keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);        
    }
    event.cancel();
}

function fLinksKeyHandlerMove(event, popupId) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var item = component.getClientId().split(':')[3];
    if ((keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY)) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, item : item
        },
        true);
    }
    event.cancel();
}

function noYesLinkConfirmPopupMove(event) {
    fLinksKeyHandlerMove(event, 'p1')
}

function noYesLinkConfirmPopup2Move(event) {
    fLinksKeyHandlerMove(event, 'p2')
}

function OnOpenConfirmPopupMove(event) {
    putFocusOnLinkMove(event, 'nlp');
}

function OnOpenConfirmPopup2Move(event) {
    putFocusOnLinkMove(event, 'nolp2');
}

function putFocusOnLinkMove(event, linkId) {
    var component = event.getSource();
    var linkComp = component.findComponent(linkId);
    linkComp.focus();
}

function onKeyPressedOnF3ExitMove(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
        event.cancel();
    }
}