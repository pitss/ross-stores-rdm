function onKeyPressedOnWaveNumber(event) {
    this.onKeyPressedNpp(event, 'wnpp');
}

function onKeyPressedOnstartPickLoc(event) {
    this.onKeyPressedNpp(event, 'splp');
}

function onKeyPressedOnpickType(event) {
    this.onKeyPressedNpp(event, 'ptnn');
}

function onKeyPressedOnpick(event) {
    this.onKeyPressedNpp(event, 'pnpp');
}

function onKeyPressedNpp(event, item) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.TAB_KEY || keyPressed == AdfKeyStroke.ENTER_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : item
        },
true);
        event.cancel();
    }
}

function customFindElement(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function setPage1InitialFocus() {
    setTimeout(function () {
        var focusOnValue = $("input[id*='focus']").val();
        if ((focusOnValue != null)) {
            console.log(focusOnValue);
            switch (focusOnValue) {
                case 'waveNbrNoPickPack':
                    setFocusOrSelectOnUIcomp("input[id*='wnpp']");
                    break;
                case 'pickTypeNoPickPack':
                    setFocusOrSelectOnUIcomp("input[id*='ptnn']");
                    break;
                case 'startPickLocNoPickPack':
                    setFocusOrSelectOnUIcomp("input[id*='splp']");
                    break;
                case 'pickNoPickPack':
                    setFocusOrSelectOnUIcomp("input[id*='pnpp']");
                    break;
                default :
                    break;
            }
        }
    },
0);
}

function OnOpenConfirmPopup(event) {
    OnOpen(event, 'nlp');
}

function noYesLinkConfirmPopupNPP(event) {
    fLinksKeyHandlerNPP(event, 'p1')
}

function fLinksKeyHandlerNPP(event, popupId) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F1_KEY) {
            fLink = component.findComponent('ylp');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);
    }
}

function OnOpen(event, linkId) {
    var component = event.getSource();
    var linkComp = component.findComponent(linkId);
    linkComp.focus();
}