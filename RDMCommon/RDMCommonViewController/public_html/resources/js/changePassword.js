function customCPFindElementById(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function okLinkGoPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY) {
        event.cancel();
    }
    else {
        AdfActionEvent.queue(component, true);
        event.cancel();
    }
}

function onKeyPressed(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
true);
        event.cancel();
    }
    else {
        event.cancel();
    }
}

function initFocusCpass() {
    setTimeout(function () {
    var field1 = 'newPass';
        SetFocusOnUIcomp("[id*='" + field1 + "']");
    },
0);
}