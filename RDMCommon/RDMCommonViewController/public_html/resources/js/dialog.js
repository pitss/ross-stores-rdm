// popup dialogs hot key handling

// simulate dialog yes event
function dialogYes(event) {
    var component = event.getSource();
    var target = component.findComponent('cfmDlg');
    var dialogEvent = new AdfDialogEvent(target, AdfDialogEvent.OUTCOME_YES);
    dialogEvent.queue(target, true);
}

// simulate dialog no event
function dialogNo(event) {
    var component = event.getSource();
    var target = component.findComponent('cfmDlg');
    var dialogEvent = new AdfDialogEvent(target, AdfDialogEvent.OUTCOME_NO);    
    dialogEvent.queue(target, true);
}

// set default focus in confirmation dialog to NO link
function defaultNoFocus(event) {
    var component = event.getSource();
    var noLink = component.findComponent('nlp');
    if (null == noLink) // confirmation only case
        noLink = component.findComponent('okay');
    noLink.focus();    
}

function dialogCancel(event) {
    var component = event.getSource();
    var target = component.findComponent('cfmDlg');
    var dialogEvent = new AdfDialogEvent(target, AdfDialogEvent.OUTCOME_CANCEL);    
    dialogEvent.queue(target, true);
}

/**
 * Generalized hotkey handler for Yes/No/Ok dialogs
 */
function onDialogKey(event) {
     var component = event.getSource();
     var keyPressed = event.getKeyCode();
     var linkComp = null;
     if (keyPressed == AdfKeyStroke.F1_KEY) {
         linkComp = component.findComponent('ylp');
         if (null == linkComp)
            linkComp = component.findComponent('okay');
     } else if (keyPressed == AdfKeyStroke.F2_KEY){
         linkComp = component.findComponent('no');
     } else if (keyPressed == AdfKeyStroke.ARROWDOWN_KEY || keyPressed == AdfKeyStroke.ARROWUP_KEY) {
         var activeLink = AdfPage.PAGE.getActiveComponent();
         if (null == activeLink)
            return;        
        var other;
         if (~activeLink.getClientId().indexOf('ylp')) {
             other = component.findComponent('no');
         } else {
            other = component.findComponent('ylp');
         }  
         if (null != other) {            
             other.focus();
         }
         return;
     }
     
     if (null != linkComp)
        AdfActionEvent.queue(linkComp, true);
}

/**
 * To cancel dialogEvents in order to prevent ENTER/ESC keys being propagated to the server
 * Add inside af:dialog as: <af:clientListener method="checkCloseDialog" type="dialog"/>
 * Also use keyDown event type for clientListeners of popup links
 */
function checkCloseDialog(event) {
        event.cancel();
    }
    
function checkCloseDialogWip(event) {
    var eventOutcome = event.getOutcome();
    if (eventOutcome == AdfDialogEvent.OUTCOME_CANCEL) {
        event.cancel();
    }
}