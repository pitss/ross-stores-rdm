/**
 * Helper function to take care of navigating to the next
 * control when ENTER key is pressed. 
 */
function enterNavigationPrefilter(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY) {         
        var nextInput = findNextInput(component);
        if (null != nextInput)
            nextInput.focus();
        event.cancel();
    } else {
        // "normal" handling
        onHotKeyPressed(event);
    }    
}

// value of an input field on focus
var oldValue = null;

// remember old value of the field currently becoming focus
function preFocusValue(event) {    
    // if previous field had an error, return back
    if (null != previousFocusControl) {        
        if (hasError(previousFocusControl))
            return; // let the focusKeeper do the job
        else {
            previousFocusControl = null;  
            clearTimeout(focusKeeper);
        }
    }
    
     var component = event.getSource();
     oldValue = component.getSubmittedValue();     
}

// helper function to clear trouble code error when nulled out
function trbCodeEnterNavigation(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY) { 
        if (!component.getValue()) {
            AdfCustomEvent.queue(component, "customClearEvent",{ keyPressed : keyPressed}, true);
        }
    } 
    enterNavigationPrefilter(event);
}

function findNextInput(component) {
    if (oldValue !== component.getSubmittedValue())
        return; // let valueChangeListener do the job
        
    // is it highlighted in error?
    if (hasError(component))
        return null; // do not navigate away

    // currently focused control
    var current = findInputElementById(component.getClientId());
            
    // find the next control to navigate to
    var inputs = $("input:visible:enabled");
    var currentIndex = inputs.index(current);
    var next = inputs.filter(function (index) { return index == (currentIndex + 1) % inputs.length; });
    return next;
}

// is the given inputField highlighted in error?
function hasError(component) {
    var styleClass = component.getStyleClass();
    return styleClass && styleClass.indexOf("p_AFError") > -1;
}

// focus keeping
var previousFocusControl = null;
var focusKeeper = null;

// called in all text field onBlur events to
// remember the previous focus control to return to
// when a non-text element is selected
function markFocus(event) {    
    previousFocusControl = event.getSource();
    focusKeeper = setTimeout(pickLostFocus, 1);     
}

// something other than a text field got focus
function pickLostFocus() {    
    if (null != previousFocusControl) { 
        var currFocus = document.activeElement.id;
        var previousId = previousFocusControl.getClientId();
        if (currFocus.indexOf(":ylp") > -1 || currFocus.indexOf(":nlp") > -1 || currFocus.indexOf(":okay") > -1) { // popup should get it's focus 
            ;
        } else  {            
            refocusInputElement(previousId);
        }        
        previousFocusControl = null;    
    }
    focusKeeper = null;
}

// open the combobox
function popOut(event) { 
    var component = event.getSource();
    var element = $("select[id*='" + component.getClientId() + "']")[0];
    setTimeout(function () { openListOfValues(element); }, 0);  
    preFocusValue(event);
}