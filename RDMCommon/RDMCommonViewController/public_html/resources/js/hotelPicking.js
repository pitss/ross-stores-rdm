function inputOnBlurFocusHanlder(event) {
    setTimeout(function () {
        var elem = findInputElementById(event.getSource().getClientId());
        $(elem).focus();
        $(elem).select();
    },
0);
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function onBlurLinkPopup(event) {
    (event.getSource()).focus();
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}

function closeWindow(evt) {
    evt.cancel();

    location.href = "ida:IDA_PROGRAM_EXIT";
    window.close();

}

function SetFocusOnUIcompByEvent(event) {
    var id = (event.getSource().getClientId()).split(':')[3];
    setTimeout(function () {
        $(id).focus();
    },
0);
}

function openListOfValues(comp) {
    $(comp).attr('size', 5);
    SetFocusOnUIcomp(comp);
}

function onBlurHanlderListOfValues(event) {
    var id = (event.getSource().getClientId()).split(':');
    SetFocusOnUIcomp(customFindElementById(id[3]));
}

function onBlurHandlerUIcomponent(event) {
    var id = (event.getSource().getClientId()).split(':');
    SetFocusOnUIcomp(customFindElementById(id[3]));
}

function customFindElementById(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function nextSocItem(compLov) {
    $(compLov + ' > option:selected').prop('selected', false).next().prop('selected', true);
}

function prevSocItem(compLov) {
    $(compLov + ' > option:selected').prop('selected', false).prev().prop('selected', true);
}

function findInputElementById(elem) {
    return document.querySelector('[id="' + elem + '::content"]');
}

function focusInputElement(event) {
    var elem = findInputElementById(event.getSource().getClientId());
    $(elem).focus();
}

function refocusInputElement(clientId) {
    var elem = findInputElementById(clientId);
    $(elem).focus();
}

function OnBlurTableHandler(event) {
    var elem = document.querySelector('[id="' + event.getSource().getClientId() + '"]');
    $(elem).focus();
}

/**
 * On some pages, there is only one active control (af:inputText or af:table),
 * which should be kept focused no matter what (keyboard control on handhelds).
 * This function takes care of that and 'refocuses' the control again.
 *
 * @param event component which is about to loose focus
 */
function lockFocus(event) {
    SetFocusOnUIcomp(event.getSource());
}

/**
 * Generalized handler for hot keys
 */
function onHotKeyPressed(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed < AdfKeyStroke.F1_KEY || keyPressed > AdfKeyStroke.F12_KEY)
        return;// not one of our hot keys  
    var linkIndex = keyPressed - AdfKeyStroke.F1_KEY + 1;
    linkComp = component.findComponent('f' + linkIndex);
    if (linkComp) {
        AdfActionEvent.queue(linkComp, true);
    }
    else {
        event.cancel();
    }
}

/**
 * Sets the focus on one of the components, which
 * is currently visible (rendered).
 * Needed in scenarios with ADF conditional rendering.
 */
function selectiveFocus(compOne, compTwo) {
    if (0 == $("[id*='" + compTwo + "']").size()) {
        SetFocusOnUIcomp("[id*='" + compOne + "']");
    }
    else {
        SetFocusOnUIcomp("[id*='" + compTwo + "']");
    }
}

function onOpenExceptionExitPopup(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('f1exit');
    linkComp.focus();
}

function onKeyPressedOnExceptionExitPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component, true);
    }
    event.cancel();
}