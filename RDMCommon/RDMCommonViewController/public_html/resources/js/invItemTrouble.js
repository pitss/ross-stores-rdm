function setInvItemPage1InitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('con1');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setInvItemPage2InitialFocus() {
    setTimeout(function () {
        var tblTroubleId = 'trbl';
        var tblTroubleCode = ("[id*='" + tblTroubleId + "']");
        if ($(tblTroubleCode).size() > 0)
            $(tblTroubleCode).focus();
    },
0);
}

function onKeyPressed(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
true);
        event.cancel();
    }
}

function inputOnBlurFocusHandler(event) {
    setFocusOrSelectOnUIcomp(event.getSource());
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function onKeyPressedOnF3Exit(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
        event.cancel();
    }
}
