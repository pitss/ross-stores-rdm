
  function onKeyPressedContainer(event) { 
      this.onKeyPressedBP(event, 'CID');
  }

  function onKeyPressedConfirmLocation(event) { 
      this.onKeyPressedBP(event, 'ConfLocationId');
  }
 
  function onKeyPressedConfirmContainer(event) { 
      this.onKeyPressedBP(event, 'ConfirmContainerId');
  }
  function onKeyPressedRequiredQty(event) { 
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F4_KEY){
         AdfActionEvent.queue(component.findComponent('f4'), true); 
        event.cancel();
    }
    else
      this.onKeyPressedBP(event, 'RequiredQty');
  }
 
function onKeyPressedBP(event, item) {  
      var component = event.getSource();
      var keyPressed = event.getKeyCode(); 
      if (keyPressed == AdfKeyStroke.F4_KEY){
          event.cancel();
      }
      else if (keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.F3_KEY  || 
          keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY  ) {
          AdfCustomEvent.queue(component, "customKeyEventBP", 
          { keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item }, true);
         event.cancel();
      }
 
    event.cancel();   
  }
  
//Page 2 scripts  
function onKeyPressedToLocation(event) {
          var component = event.getSource();
          var keyPressed = event.getKeyCode();  
          if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY ||  keyPressed == AdfKeyStroke.ENTER_KEY  ) {
              AdfCustomEvent.queue(component, "customKeyEvent", 
              { keyPressed : keyPressed, submittedValue : component.getSubmittedValue() }, true);
              event.cancel();
          }
}  


//page 3 scripts
function onKeyPressedContainer3(event) { 
      var component = event.getSource();
      var keyPressed = event.getKeyCode();  
      if ( keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || 
          keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY ) {
          AdfCustomEvent.queue(component, "customKeyEvent", 
          { keyPressed : keyPressed, submittedValue : component.getSubmittedValue()  }, true);
          event.cancel();
      } 
      else if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY){
          bpConfEmptyAction(event);
      }
  }

function bpConfDiffPickOpenFocus(event) { 
      onOpenFocusLinkBP(event,'ylp'); 
  }  
function bpConfDiffPickAction(event){ 
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY )  
        onPopupLinkActionBP(event,'ylp'); 
    else if (keyPressed == AdfKeyStroke.F2_KEY ) 
        onPopupLinkActionBP(event,'nlp');
    
     event.cancel();
  } 
  
function bpConfEmptyOpenFocus(event) {  
      onOpenFocusLinkBP(event,'yce'); 
  }  
function bpConfEmptyAction(event){ 
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY ){    
        onPopupLinkActionBP(event,'yce');}
    else if (keyPressed == AdfKeyStroke.F2_KEY ){   
        onPopupLinkActionBP(event,'nce');}
    
    event.cancel();
  }
  
function goPopupOpenFocusLink(event) { 
      onOpenFocusLinkBP(event,'goOk'); 
  }
function mlPopupOpenFocusLink(event) { 
      onOpenFocusLinkBP(event,'mlOk'); 
  }
function okLinkGoPopup(event){
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY ){
         AdfActionEvent.queue(component, true);
    }else{
        event.cancel();
    }
  }  
 
function onPopupLinkActionBP (event,item){
     var component = event.getSource();
      var linkComp = component.findComponent(item);
      linkComp.focus();
      AdfActionEvent.queue(linkComp, true);
      }
      
function onOpenFocusLinkBP(event, item) { 
      var component = event.getSource();
      var linkComp = component.findComponent(item);
       
      linkComp.focus();
  }
function onBlurPopupHanlderBP(event) {
    var component = event.getSource();
    var keyPressed;
    try {
        keyPressed = event.getKeyCode();
    }
    catch (err) {
       var linkComp = component.findComponent('yce');       
       linkComp.focus();
    }
    event.cancel();
}  
function mlOkLinkGoPopup(event){
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY )  
         AdfActionEvent.queue(component, true);
  } 
  
//page 4 scripts
  function onKeyPressedTotalWeight(event ) {
      var component = event.getSource();
      var keyPressed = event.getKeyCode();  
      if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY ||  keyPressed == AdfKeyStroke.ENTER_KEY  ) {
          AdfCustomEvent.queue(component, "customKeyEvent", 
          { keyPressed : keyPressed, submittedValue : component.getSubmittedValue()}, true);
          event.cancel();
      }                 
  }
  
//page 5 scripts
function onKeyPressedGenericChild(event) { 
  this.onKeyPressedPg5(event, 'GenericChild');
}
function onKeyPressedPickChild(event) { 
  this.onKeyPressedPg5(event, 'PickChild');
} 
function onKeyPressedContainerQty(event) {
  this.onKeyPressedPg5(event, 'ContainerQty');
}

function onKeyPressedPg5(event, item) {
  var component = event.getSource();
  var keyPressed = event.getKeyCode();  
  if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY ||  keyPressed == AdfKeyStroke.ENTER_KEY  ) {
      AdfCustomEvent.queue(component, "customKeyEvent", 
      { keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item }, true);
      event.cancel();
  }
  else {
      if (item == 'ContainerQty') {
       var elem =  findInputElementById(event.getSource().getClientId()); 
          if (!$.isNumeric( $(elem).val() )) {
              $(elem).val('');
               event.cancel();
    }}}
   
}
//Reson page script
    function onKeyPressedReason(event) {
      var component = event.getSource();
      var focusOn = findInputElementById(component.getClientId());
      var keyPressed = event.getKeyCode();
      if (keyPressed == AdfKeyStroke.ENTER_KEY){
           $(focusOn).attr('size', 1);
      }
      else if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY ) {         
          AdfCustomEvent.queue(component, "customKeyEvent", 
          {keyPressed : keyPressed, submittedValue : component.getSubmittedValue()},true);
      }
      else {
              switch (keyPressed) {
                  case AdfKeyStroke.F6_KEY:
                  // Open a soc
                  // openListOfValues : function inside hotelPicking.js
                      openListOfValues(focusOn);
                      break;
                  case AdfKeyStroke.ARROWDOWN_KEY:
                  // Next option in soc
                  // nextSocItem : function inside hotelPicking.js
                      nextSocItem(focusOn);
                      break;
                  case AdfKeyStroke.ARROWUP_KEY:
                  // Previous option in soc
                  // prevSocItem : function inside hotelPicking.js
                      prevSocItem(focusOn);
                      break;
                  default :
                      break;
              } 
          }
          event.cancel();  
    }
    /* method called on pageload from backingBean onRegionLoad method */
    function expandSelectOneChoice(inputComp) {
      setTimeout(function () {
          var elem = findInputElementById(inputComp);
          openListOfValues(elem); 
      },0);
    } 
//LPN Key
   function onKeyPressedLPN(event){
      var component = event.getSource(); 
      var keyPressed = event.getKeyCode();      
      if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.ENTER_KEY) { 
          AdfActionEvent.queue(component.findComponent('b2'), true); 
          event.cancel();
      }
   }


function onBlurGoToPopupBP(event) {
    onBlurPopupHanlderBP(event, 'goOk');
}
function onBlurConfEmptyPopupBP(event) {
    onBlurPopupHanlderBP(event, 'yce');
}
function onBlurConfDiffPopupBP(event) {
    onBlurPopupHanlderBP(event, 'ylp');
}

function onBlurPopupHanlderBP(event, item) {
    var component = event.getSource();
    var keyPressed;
    try {
        keyPressed = event.getKeyCode();
    }
    catch (err) {
       var linkComp = component.findComponent(item);       
       linkComp.focus();
    }
    event.cancel();
}

function inputOnBlurFocusHanlderBP(event) {
    var component = event.getSource();
    var keyPressed;
    try {
        keyPressed = event.getKeyCode();
    }
    catch (err) {
         var submittedValue = component.getSubmittedValue();
        if (submittedValue) { 
            AdfValueChangeEvent.queue(component, null, submittedValue, true);
        }
       inputOnBlurFocusHanlder(event);
    }
    event.cancel();
}   