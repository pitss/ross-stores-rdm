function onMenuPopupOpen(event) {
    onMenuSubMenuPopupOpen(event, 'nlpmm');
}

function onSubMenuPopupOpen(event) {
    onMenuSubMenuPopupOpen(event, 'nlpsm');
}

function menuSubMenuPopupLink(event, popupId, yesLinkId) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(event.getSource().findComponent(yesLinkId), true);
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.hidePopupMenus(event, popupId);
    event.cancel();
}

function onYesMenuSubMenuClick(event, popupId) {
    this.hidePopupMenus(event, popupId);
    location.href = "ida:IDA_PROGRAM_EXIT";
    window.close();
}

function onNoMenuSubMenuClick(event, popupId) {
    this.hidePopupMenus(event, popupId);
}

function onNoMenuClick(event, popupId) {
    this.onNoMenuSubMenuClick(event, 'p1mm');
}

function onYesMenuClick(event, popupId) {
    this.onYesMenuSubMenuClick(event, 'p1mm')
}

function onNoSubMenuClick(event, popupId) {
    this.onNoMenuSubMenuClick(event, 'p1sm');
}

function onYesSubMenuClick(event, popupId) {
    this.onYesMenuSubMenuClick(event, 'p1sm')
}

function menuPopupLink(event) {
    this.menuSubMenuPopupLink(event, 'p1mm', 'ylpmm')
}

function subMenuPopupLink(event) {
    this.menuSubMenuPopupLink(event, 'p1sm', 'ylpsm')
}

function hidePopupMenus(event, idPopup) {
    var srcComponent = event.getSource();
    var hidePopup = srcComponent.findComponent(idPopup);
    hidePopup.hide();
}

function hidePopup(evt) {
    var srcComponent = evt.getSource();
    var hidePopup = srcComponent.findComponent('p1mm');
    hidePopup.hide();
}

function onMenuSubMenuPopupOpen(event, id) {
    var component = event.getSource();
    var linkComp = component.findComponent(id);
    linkComp.focus();
}

function initFocusMenu() {
    setTimeout(function () {
        var tabe1 = 'table1';
        SetFocusOnUIcomp("[id*='" + tabe1 + "']");
    },
0);
}

function initFocusSubMenu() {
    setTimeout(function () {
        var tabe2 = 'table2';
        SetFocusOnUIcomp("[id*='" + tabe2 + "']");
    },
0);
}

function find(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function OnBlurSubMenuHandler(event) {
    SetFocusOnUIcomp(find('table2'));
}

function closeWindow(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component, true);
        event.cancel();
    }
    location.href = "ida:IDA_PROGRAM_EXIT";
    window.close();

}

function hidePopupSm(evt) {
    srcComponent = evt.getSource();
    var confirmExitPopup = srcComponent.findComponent('p1sm');
    confirmExitPopup.hide();
    return;
}

function noYesLinkConfirmPopupMM(event) {
    fLinksKeyHandlerMM(event)
}

function fLinksKeyHandlerMM(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        var fLink = null;
        if (keyPressed == AdfKeyStroke.F1_KEY) {
            fLink = component.findComponent('ylpmm');
            fLink.focus();
        }
        else {
            fLink = component.findComponent('nlpmm');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);
        event.cancel();
    }
}

function noYesLinkConfirmPopupSM(event) {
    fLinksKeyHandlerSM(event)
}

function fLinksKeyHandlerSM(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F2_KEY) {
            fLink = component.findComponent('nlpsm');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);(component.findComponent('p1sm')).hide();
        event.cancel();
    }
}

function OnBlurMenuHandler(event) {
    SetFocusOnUIcomp(find('table1'));
}

function onKeyPressedMenu(event) {
    var srcComponent = event.getSource();
    var keyPressed = event.getKeyCode();
    var confirmExitPopup = srcComponent.findComponent('p1mm');
    var fLink;
    if (confirmExitPopup.isPopupVisible()) {
        switch (keyPressed) {
            case AdfKeyStroke.F1_KEY:
                fLink = srcComponent.findComponent('ylpmm');
                break;
            case AdfKeyStroke.F2_KEY:
                fLink = srcComponent.findComponent('nlpmm');
                break;
            case AdfKeyStroke.ENTER_KEY:
                event.cancel();
                break;
            default :
                event.cancel();
                break;
        }
        confirmExitPopup.hide();
    }
    else if (AdfKeyStroke.F3_KEY == keyPressed)
        fLink = srcComponent.findComponent('bextmm');
    else if (AdfKeyStroke.F2_KEY == keyPressed)
        fLink = srcComponent.findComponent('bpremm');
    else if (AdfKeyStroke.ENTER_KEY == keyPressed) {
        $("[id*='gotosubmenu']").click();
        return;
    }
    if (fLink != null) {
        fLink.focus();
        AdfActionEvent.queue(fLink, true);
    }
}

function onKeyPressedSubMenu(event) {
    var srcComponent = event.getSource();
    var keyPressed = event.getKeyCode();
    var confirmExitPopup = srcComponent.findComponent('p1sm');
    var fLink = null;
    if (confirmExitPopup.isPopupVisible()) {
        switch (keyPressed) {
            case AdfKeyStroke.F1_KEY:
                fLink = srcComponent.findComponent('ylpsm');
                break;
            case AdfKeyStroke.F2_KEY:
                fLink = srcComponent.findComponent('nlpsm');
                break;
            case AdfKeyStroke.ENTER_KEY:
                event.cancel();
                return;
            default :
                event.cancel();
                break;
        }
        confirmExitPopup.hide();
    }
    else {
        switch (keyPressed) {
            case AdfKeyStroke.F3_KEY:
                fLink = srcComponent.findComponent('bext');
                break;
            case AdfKeyStroke.F2_KEY:
                fLink = srcComponent.findComponent('bpre');
                break;
            case AdfKeyStroke.ENTER_KEY:
                $("[id*='gotosubmenu']").click();
                break;
            default :
                event.cancel();
                break;
        }

    }
    if (fLink != null) {
        fLink.focus();
        AdfActionEvent.queue(fLink, true);
    }
    event.cancel();
}