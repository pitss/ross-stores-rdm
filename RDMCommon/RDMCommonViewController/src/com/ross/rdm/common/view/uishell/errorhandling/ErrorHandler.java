package com.ross.rdm.common.view.uishell.errorhandling;

import com.ross.rdm.common.view.uishell.errorhandling.PageTemplateErrorHandler;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;

public class ErrorHandler {
    public ErrorHandler() {
        super();
    }

    private PageTemplateErrorHandler pageTemplateErrorHandler;

    public void handleException() {
        // TODO clean Debug - Mechanism!
        //System.out.println("Handle Exception (de.pitss.adf.common.view.taskflowtempaltes.pitssbasetemplate.beans.ErroHandler)");
        ControllerContext context = ControllerContext.getInstance();
        ViewPortContext currentRootViewPort = context.getCurrentViewPort();
        Exception exceptionData = currentRootViewPort.getExceptionData();
        if (currentRootViewPort.isExceptionPresent()) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
        }
        this.pageTemplateErrorHandler = getPageTemplateErrorHandlerBean();
        String errorMsg = pageTemplateErrorHandler.mapErrorMessage(exceptionData.getMessage());
        if (errorMsg != null && !errorMsg.equals("")) {
            pageTemplateErrorHandler.setErrorMessage(errorMsg);
            pageTemplateErrorHandler.setStackTrace(getStacktrace());
            pageTemplateErrorHandler.showErrorPopUp();
        }
    }

    public String getStacktrace() {
        ControllerContext cc = ControllerContext.getInstance();
        if (cc.getCurrentViewPort().getExceptionData() != null) {
            StringWriter sw = new StringWriter();
            //PrintWriter pw = new PrintWriter(sw);
            //cc.getCurrentViewPort().getExceptionData().printStackTrace(pw);
            return sw.toString();
        }
        return null;
    }

    public void setPageTemplateErrorHandler(PageTemplateErrorHandler pageTemplateErrorHandler) {
        this.pageTemplateErrorHandler = pageTemplateErrorHandler;
    }

    public PageTemplateErrorHandler getPageTemplateErrorHandler() {
        return pageTemplateErrorHandler;
    }

    public PageTemplateErrorHandler getPageTemplateErrorHandlerBean() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        Application application = fctx.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext context = fctx.getELContext();
        ValueExpression createValueExpression =
            expressionFactory.createValueExpression(context, "#{pageTemplateErrorHandler}", Object.class);
        PageTemplateErrorHandler pageTemplateErrorHandler =
            (PageTemplateErrorHandler)createValueExpression.getValue(context);
        return pageTemplateErrorHandler;
    }
}
