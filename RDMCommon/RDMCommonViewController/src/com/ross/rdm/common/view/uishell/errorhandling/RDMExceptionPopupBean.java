package com.ross.rdm.common.view.uishell.errorhandling;

import java.io.IOException;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.fragment.RichRegion;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

public class RDMExceptionPopupBean {
    private static final ADFLogger logger = ADFLogger.createADFLogger(RDMExceptionPopupBean.class);
    private String messageToShow = "Unexpected exception caught. Press F1=OK to exit.";
    private RichPopup exitPopup;
    private RichRegion contentRegion;

    public RDMExceptionPopupBean() {
        super();
    }

    public void showExitPopup() {
        if (getContentRegion() != null) {
            disableAllFields(getContentRegion().getChildren());
        }
        if (getExitPopup() != null) {
            getExitPopup().show(new RichPopup.PopupHints());
        }
    }

    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    private void disableAllFields(List<UIComponent> listaComp) {
        for (UIComponent comp : listaComp) {
            if (comp.getChildCount() > 0) {
                disableAllFields(comp.getChildren());
            } else if (comp.isRendered()) {
                if (comp instanceof RichInputText) {
                    ((RichInputText) comp).setDisabled(true);
                } else if (comp instanceof RichInputDate) {
                    ((RichInputDate) comp).setDisabled(true);
                } else if (comp instanceof RichSelectOneChoice) {
                    ((RichSelectOneChoice) comp).setDisabled(true);
                }
            }
        }
    }

    public void setContentRegion(RichRegion contentRegion) {
        this.contentRegion = contentRegion;
    }

    public RichRegion getContentRegion() {
        return contentRegion;
    }

    public String exitAction() {
        //Same behaviour as logout
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext ectx = facesContext.getExternalContext();
        try {
            String url = ectx.getRequestContextPath() + "/adfAuthentication?logout=true&end_url=/faces/login.jsf";
            ectx.redirect(url);
        } catch (IOException e) {
            logger.severe("Error while trying to exit:");
            logger.severe(e);
            if (getExitPopup() != null) {
                getExitPopup().hide();
            }
        }
        facesContext.responseComplete();
        return null;
    }
}
