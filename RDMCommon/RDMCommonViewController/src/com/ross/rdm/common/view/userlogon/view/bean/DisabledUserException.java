package com.ross.rdm.common.view.userlogon.view.bean;


public class DisabledUserException extends Exception {
    public DisabledUserException(String string, Throwable throwable, boolean b, boolean b1) {
        super(string, throwable, b, b1);
    }

    public DisabledUserException(Throwable throwable) {
        super(throwable);
    }

    public DisabledUserException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public DisabledUserException(String string) {
        super(string);
    }

    public DisabledUserException() {
        super();
    }
}

