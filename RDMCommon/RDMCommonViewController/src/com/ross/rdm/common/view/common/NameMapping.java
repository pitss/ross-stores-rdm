package com.ross.rdm.common.view.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class calculates the ID of the task flow which will be called to show the app-screen, depending on the original
 * Form-ID reference stored in the column OPTION_COMMAND of the menu tables and the naming convention used
 * to generate ADF application.
 *
 * I.e: 'hh_putaway_inventory_s' will be transformed to:
 * "/WEB-INF/com/ross/rdm/hotelpicking/hhputawayinventorys/view/taskflow/HhPutawayInventorySTaskFlow.xml#HhPutawayInventorySTaskFlow"
 *
 */
public class NameMapping {

    // static methods only
    private NameMapping() {
        super();
    }

    // useful constants, based on the naming convention used by the ADF Assistant to generate the ADF application
    private static final String rootPath = "/WEB-INF/com/ross/rdm/";
    private static final String middlePath = "/view/taskflow/";
    private static final String docNamePart = "TaskFlow.xml#";
    private static final String idNamePart = "TaskFlow";

    /**
     * For the name of the original Forms application, calculate the ID of the corresponding ADF task flow.
     *
     * @param formName the name of the original form
     * @return ID of the ADF task flow, which implements this form
     */
    public static String calculateTaskFlowId(String formName) {
        String camelCased = camelCase(formName);
        return rootPath + allLowerCase(formName) + middlePath + camelCased + docNamePart + camelCased + idNamePart;
    }
    public static String calculateTaskFlowId(String rootPatha, String formName) {
        String camelCased = camelCase(formName);
        return rootPatha + allLowerCase(formName) + middlePath + camelCased + docNamePart + camelCased + idNamePart;        
    }
    public static String calculateDynamicTaskFlowId(String formName, String dynRootPath) {
        String camelCased = camelCase(formName);
        return rootPath + dynRootPath + allLowerCase(formName) + middlePath + camelCased + docNamePart + camelCased +
               idNamePart;
    }


    /**
     * Camel cases the original form name. I.e: from 'hh_putaway_inventory_s' produces 'HhPutawayInventoryS'
     *
     * @param formName original form name
     * @return camelcased version of the name
     */
    private static String camelCase(String formName) {
        // transform to a sequence of words
        formName = formName.replace('_', ' ');
        return initcap(formName);
    }

    /**
     * Transforms the name of the original form into all lowercase and drops all underscores.
     * I.e: from 'hh_putaway_inventory_s' makes 'hhputawayinventorys'
     *
     * @param formName original form name
     * @return all-lowercase version of the name
     */
    private static String allLowerCase(String formName) {
        // remove all underscores
        formName = formName.replace("_", "");
        return formName.toLowerCase();
    }

    private static String initcap(String input) {
        Pattern p = Pattern.compile("((\\w)+)");
        Matcher m = p.matcher(input);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, firstUpper(m.group()));
        }
        m.appendTail(sb);
        return sb.toString().replaceAll(" ", "");
    }

    /**
     * Returns the given string with the first character in uppercase and all the rest in lowercase
     *
     * @param s string to transform
     * @return transformed string
     */
    private static String firstUpper(String s) {
        return Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
    }
}
