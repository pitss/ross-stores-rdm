package com.ross.rdm.common.view.hhmenuss.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import java.io.Serializable;

import java.util.ArrayList;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.share.ADFContext;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhMenusSBean extends RDMHotelPickingPageFlowBean implements Serializable {
    @SuppressWarnings("compatibility:-8599409030850553384")
    private static final long serialVersionUID = 1L;

    /**
     * Task flow parameters
     */
    private boolean isMainMenu;
    private boolean focusOnMenu;
    private String facilityId;
    private String menuName;
    private int userPrivilege;
    private String languageCode;
    private String mainMenuOrTaskFlowUrl;
    private ArrayList previousMenusNames = null;
    private int menuLevel;


    public void iniTaskFlow() {
        if (this.getPreviousMenusNames() == null) {
            this.setPreviousMenusNames(new ArrayList<String>());
            this.getPreviousMenusNames().add(this.getMenuName());
        }
    }

    public TaskFlowId getScreenTaskFlow() {
        return TaskFlowId.parse(this.getMainMenuOrTaskFlowUrl());
    }

    public void setFocusOnMenu(boolean focusOnMenu) {
        this.focusOnMenu = focusOnMenu;
    }

    public boolean isFocusOnMenu() {
        return focusOnMenu;
    }

    public void setIsMainMenu(boolean isSubMenu) {
        this.isMainMenu = isSubMenu;
    }

    public boolean getIsMainMenu() {
        return isMainMenu;
    }
    private String rootPath;

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setUserPrivilege(int userPrivilege) {
        this.userPrivilege = userPrivilege;
    }

    public int getUserPrivilege() {
        return userPrivilege;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setMainMenuOrTaskFlowUrl(String menuOrTaskFlow) {
        this.mainMenuOrTaskFlowUrl = menuOrTaskFlow;
    }

    public String getMainMenuOrTaskFlowUrl() {
        return mainMenuOrTaskFlowUrl;
    }


    public void setPreviousMenusNames(ArrayList previousMenusNames) {
        this.previousMenusNames = previousMenusNames;
    }

    public ArrayList getPreviousMenusNames() {
        return previousMenusNames;
    }

    public void setMenuLevel(int menuLevel) {
        this.menuLevel = menuLevel;
    }

    public int getMenuLevel() {
        return menuLevel;
    }

}
