package com.ross.rdm.common.view.framework;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;

import com.ross.rdm.common.view.common.CurrentUser;

import java.io.Serializable;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.metadata.ActivityId;
import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichMessage;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.controller.activity.TaskFlowReturnActivityLogic;
import oracle.adfinternal.controller.state.AdfcContext;
import oracle.adfinternal.controller.state.PageFlowStack;
import oracle.adfinternal.controller.state.PageFlowStackEntry;
import oracle.adfinternal.controller.state.ViewPortContextImpl;

import oracle.binding.BindingContainer;

import oracle.jbo.ApplicationModule;
import oracle.jbo.server.DBTransaction;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class RDMMobileBaseBackingBean extends RDMMobileRoleBasedBackingBean implements Serializable {
    @SuppressWarnings("compatibility:6474302760025093865")
    private static final long serialVersionUID = 1L;
    private static final String GENERIC_PAGEDEFINITION = "com_ross_rdm_common_view_welcomePageDef";

    private static String _appModuleName = "HotelPickingAppModule";
    private static String _appModuleDataControlName = "HotelPickingAppModuleDataControl";

    private final static String ERROR_STYLE_CLASS = "p_AFError";
    private final static String PINK_BACKGROUND_CSS = "background-color: #FAFDFF;";
    private final static String RED_FONT_CSS = "color: Red;";
    private final static String BLACK_FONT_CSS = "color: Black;";

    public final static Double ENTER_KEY_CODE = Double.valueOf(13);
    public final static Double TAB_KEY_CODE = Double.valueOf(9);
    public final static Double F1_KEY_CODE = Double.valueOf(112);
    public final static Double F2_KEY_CODE = Double.valueOf(113);
    public final static Double F3_KEY_CODE = Double.valueOf(114);
    public final static Double F4_KEY_CODE = Double.valueOf(115);
    public final static Double F5_KEY_CODE = Double.valueOf(116);
    public final static Double F6_KEY_CODE = Double.valueOf(117);
    public final static Double F7_KEY_CODE = Double.valueOf(118);
    public final static Double F8_KEY_CODE = Double.valueOf(119);
    public final static Double F9_KEY_CODE = Double.valueOf(120);
    public final static Double F10_KEY_CODE = Double.valueOf(121);
    public final static Double F11_KEY_CODE = Double.valueOf(122);
    public final static Double F12_KEY_CODE = Double.valueOf(123);

    public static final String TR_EXIT = "TR_EXIT";

    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String ERROR = "E";
    public static final String WARN = "W";
    public static final String INFO = "I";
    public static final String CONF = "C";
    public static final String SUCCESS = "S";
    public static final String MSGTYPE_M = "M";
    public static final String REQ_ICON = "required";
    public static final String ERR_ICON = "error";
    public static final String WRN_ICON = "warning";
    public static final String INF_ICON = "info";
    public static final String LOGO_ICON = "logo";
    public static final String PARTIAL_ENTRY = "PARTIAL_ENTRY";
    public static final String DIFF_PICK = "DIFF_PICK";
    public static final String CHECK_EMPTY = "CHECK_EMPTY";
    public static final String DMS_ERROR = "DMS_ERROR";

    //Strings for messages
    public static final String MSG_FIRST = "FIRST";
    public static final String MSG_LAST = "LAST";
    public static final String MSG_INV_CONTAINER = "INV_CONTAINER";

    //Strings for Operations
    public static final String OPR_COMMIT = "Commit";
    public static final String OPR_DO_COMMIT = "callDoCommit";

    //Strings for bind attributes
    public static final String ATTR_FACILITY_ID = "FacilityId";
    public static final String ATTR_LANGUAGE_CODE = "LanguageCode";

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    public RDMMobileBaseBackingBean() {
    }

    /**
     * Return name of the Application Module
     * set in static class attribute _appModuleName .
     *
     * @return _appModuleName
     */
    public String getAppModuleName() {
        return _appModuleName;
    }

    /**
     * Return name of the Application Module Data Control Name
     * set in static class attribute _appModuleDataControlName .
     *
     * @return _appModuleDataControlName
     */
    public String getAppModuleDataControlName() {
        return _appModuleDataControlName;
    }


    public int getPageFlowStackDepth() {
        int depth = 0;
        try {
            ViewPortContextImpl viewPort = (ViewPortContextImpl) ControllerContext.getInstance().getCurrentViewPort();
            PageFlowStack stack = viewPort.getPageFlowStack();
            depth = stack.size();
        } catch (Exception e) {
        }
        return depth;
    }

    public void popTaskFlow(int depth) {

        AdfcContext adfcContext = AdfcContext.getCurrentInstance();
        ViewPortContextImpl currViewPort = adfcContext.getCurrentViewPort();
        try {

            ViewPortContextImpl viewPort = (ViewPortContextImpl) ControllerContext.getInstance().getCurrentViewPort();
            adfcContext.getControllerState().setCurrentViewPort(adfcContext, viewPort.getViewPortId());
            viewPort.makeCurrent(adfcContext);

            PageFlowStack stack = viewPort.getPageFlowStack();
            PageFlowStackEntry entry = null;

            for (int i = 1; i < depth; i++) {
                (new TaskFlowReturnActivityLogic()).abandonTaskFlow(adfcContext, stack.peek());
                entry = stack.pop(adfcContext);
            }
            ActivityId newViewActivityId = entry.getCallingViewActivity();
            viewPort.setViewActivityId(adfcContext, newViewActivityId);

        } finally {
            adfcContext.getControllerState().setCurrentViewPort(adfcContext, currViewPort.getViewPortId());
            currViewPort.makeCurrent(adfcContext);
        }
    }


    /**
     * Return Application Module
     *
     * @return application module
     */
    public ApplicationModule getAppModule() {
        return ADFUtils.getApplicationModuleForDataControl(getAppModuleDataControlName());
    }

    /**
     * Return BindingContainer of page
     *
     * @return current page binding container
     */
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public ResourceBundle getResourceBundle() {
        return ResourceBundle.getBundle("com.ross.rdm.hotelpicking.view.RDMHotelPickingViewControllerBundle",
                                        ADFContext.getCurrent().getLocale());
    }

    public boolean validateRequiredField(Object value, RichInputText uiComponent) {
        boolean validateOK = true;
        if (value == null || "".equals(value)) {
            validateOK = false;
            this.addErrorStyleToComponent(uiComponent);
            this.setFocusOnUIComponent(uiComponent);
        } else {
            this.removeErrorStyleToComponent(uiComponent);
        }
        return validateOK;
    }

    public void addErrorStyleToComponent(RichInputText uiComponent) {
        String styleClass = uiComponent.getStyleClass();
        if (styleClass != null && !styleClass.contains(ERROR_STYLE_CLASS)) {
            uiComponent.setStyleClass(styleClass.concat(" " + ERROR_STYLE_CLASS));
        } else {
            uiComponent.setStyleClass(ERROR_STYLE_CLASS);
        }

        String contentStyle = uiComponent.getContentStyle();
        if (contentStyle != null) {
            if (contentStyle.contains(BLACK_FONT_CSS)) {
                contentStyle = contentStyle.replace(BLACK_FONT_CSS, RED_FONT_CSS);
            } else {
                contentStyle = contentStyle.concat(RED_FONT_CSS);
            }
            contentStyle = contentStyle.concat(PINK_BACKGROUND_CSS);
            uiComponent.setContentStyle(contentStyle);
        } else {
            uiComponent.setContentStyle(RED_FONT_CSS + " " + PINK_BACKGROUND_CSS);
        }
        this.refreshContentOfUIComponent(uiComponent);
    }

    public void removeErrorStyleToComponent(RichInputText uiComponent) {
        String styleClass = uiComponent.getStyleClass();
        if (styleClass != null) {
            uiComponent.setStyleClass(styleClass.replace(ERROR_STYLE_CLASS, ""));
        }

        String contentStyle = uiComponent.getContentStyle();
        if (contentStyle != null) {
            contentStyle = contentStyle.replace(RED_FONT_CSS, BLACK_FONT_CSS);
            contentStyle = contentStyle.replace(PINK_BACKGROUND_CSS, "");
            uiComponent.setContentStyle(contentStyle);
        }

        this.refreshContentOfUIComponent(uiComponent);
    }

    public void addErrorStyleToChoiceComponent(RichSelectOneChoice uiComponent) {
        String styleClass = uiComponent.getStyleClass();
        if (styleClass != null && !styleClass.contains(ERROR_STYLE_CLASS)) {
            uiComponent.setStyleClass(styleClass.concat(" " + ERROR_STYLE_CLASS));
        } else {
            uiComponent.setStyleClass(ERROR_STYLE_CLASS);
        }

        String contentStyle = uiComponent.getContentStyle();
        if (contentStyle != null) {
            if (contentStyle.contains(BLACK_FONT_CSS)) {
                contentStyle = contentStyle.replace(BLACK_FONT_CSS, RED_FONT_CSS);
            } else {
                contentStyle = contentStyle.concat(RED_FONT_CSS);
            }
            contentStyle = contentStyle.concat(PINK_BACKGROUND_CSS);
            uiComponent.setContentStyle(contentStyle);
        } else {
            uiComponent.setContentStyle(RED_FONT_CSS + " " + PINK_BACKGROUND_CSS);
        }
        this.refreshContentOfUIComponent(uiComponent);
    }

    public void removeErrorStyleToChoiceComponent(RichSelectOneChoice uiComponent) {
        String styleClass = uiComponent.getStyleClass();
        if (styleClass != null) {
            uiComponent.setStyleClass(styleClass.replace(ERROR_STYLE_CLASS, ""));
        }

        String contentStyle = uiComponent.getContentStyle();
        if (contentStyle != null) {
            contentStyle = contentStyle.replace(RED_FONT_CSS, BLACK_FONT_CSS);
            contentStyle = contentStyle.replace(PINK_BACKGROUND_CSS, "");
            uiComponent.setContentStyle(contentStyle);
        }

        this.refreshContentOfUIComponent(uiComponent);
    }

    /**
     * sets the cursor to the given component id
     *
     * @param  componentId of item on page
     */
    public void setFocusOnUIComponent(String componentId) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);

        UIComponent uiComponent = facesContext.getViewRoot().findComponent(componentId);
        service.addScript(facesContext,
                          " var t=document.getElementById('" + uiComponent.getClientId(facesContext) + "::content'); " +
                          " t.focus();");
    }

    public void setFocusOnUIComponent1(String componentId) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        UIComponent uiComponent =
            JSFUtils.findComponentInRoot(componentId); // facesContext.getViewRoot().findComponent(componentId);
        service.addScript(facesContext,
                          " var t = document.getElementById('" + uiComponent.getClientId(facesContext) +
                          "'); t.focus(); ");
        AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }


    public void setFocusOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("setFocusOrSelectOnUIcomp(textInput);"); //changed selectFocusOnComp to selectFocusonUIComp fo IE using jquery
        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }

    public void selectTextOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("if(textInput != null){textInput.select();}");

        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }

    public void writeJavaScriptToClient(String script) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        service.addScript(facesContext, script);
    }

    /**
     * sends a partial page submit for the given component
     *
     * @param  component on page
     */
    public void refreshContentOfUIComponent(UIComponent component) {
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(component);
    }

    /**
     * Return current DBTransaction
     *
     * @return current DBTransaction
     */
    public DBTransaction getDBTransaction() {
        return (DBTransaction) getAppModule().getTransaction();
    }

    public String logoutExitBTF() {
        // PITSS.CON NOTE : If you want to navigate to the calling Task Flow/Page Flow,
        //   then please comment out the following code and replace with
        //   return "backGlobalHome";
        return "backGlobalHome";
        //ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
        //HttpSession session = (HttpSession)ectx.getSession(false);
        //String temp = ControllerContext.getInstance().getGlobalViewActivityURL("exit");
        //try {
        //   ectx.redirect(temp);
        //   session.invalidate();
        //} catch (Exception ex) {
        //  ex.printStackTrace();
        //}
        //return null;
    }

    public void setFocusOnField(RichInputText field) {
        this.setFocusOnUIComponent1(field.getId());
    }

    public void clearField(RichInputText field) {
        field.resetValue();
        this.refreshContentOfUIComponent(field);
    }


    public void setErrorMessages(RichOutputText errorMessage, String errMsg) {
        errorMessage.setValue(errMsg);
        this.refreshContentOfUIComponent(errorMessage);
    }


    public void setErrorAfMessages(RichMessage errorAfMessage, String errMsg) {
        errorAfMessage.setMessage(errMsg);
        errorAfMessage.setVisible(true);
        this.refreshContentOfUIComponent(errorAfMessage);
    }

    public void setWarningAfMessages(RichMessage warningAfMessage, String warnMsg) {
        warningAfMessage.setMessage(warnMsg);
        warningAfMessage.setVisible(true);
        this.refreshContentOfUIComponent(warningAfMessage);
    }

    public void setInfoAfMessages(RichMessage infoAfMessage, String infoMsg) {
        infoAfMessage.setMessage(infoMsg);
        infoAfMessage.setVisible(true);
        this.refreshContentOfUIComponent(infoAfMessage);
    }

    public void setInfoMessages(RichOutputText infoMessage, String infoMsg) {
        infoMessage.setValue(infoMsg);
        this.refreshContentOfUIComponent(infoMessage);
    }

    public void setWarningMessages(RichOutputText warningMessage, String warnMsg) {
        warningMessage.setValue(warnMsg);
        this.refreshContentOfUIComponent(warningMessage);
    }

    public String getMessage(String msgCode, String msgType, String facilityId, String langCode) {
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        if ("AM".equalsIgnoreCase(currentUser.getLanguageCode())) {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseMessageBundleAm", msgCode);
        } else {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseMessageBundleSp", msgCode);
        }
    }
    
    private String getValueFromBundle(String bundle, String attrCode) {
        String value = null;
        try {
            value = ResourceBundle.getBundle(bundle).getString(attrCode);
        } catch (MissingResourceException e) {
            value = attrCode;
        }
        return value;
    }

    public Object getPageFlowBean(String pageFlowBeanName) {
        return AdfFacesContext.getCurrentInstance().getPageFlowScope().get(pageFlowBeanName);
    }

    public Object getSessionScopeBean(String beanName) {
        return ADFContext.getCurrent().getSessionScope().get(beanName);
    }

    public void updateModel(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }

    public void navigateToAction(String action) {
        FacesContext fc = FacesContext.getCurrentInstance();
        NavigationHandler nh = fc.getApplication().getNavigationHandler();
        nh.handleNavigation(fc, null, action);
    }

    public void callTaskViewActitvity(String taskFlowUrl) {
        AdfcContext adfcContext = AdfcContext.getCurrentInstance();
        ViewPortContextImpl currViewPort = adfcContext.getCurrentViewPort();
        try {
            ViewPortContextImpl viewPort = (ViewPortContextImpl) ControllerContext.getInstance().getCurrentViewPort();
            adfcContext.getControllerState().setCurrentViewPort(adfcContext, viewPort.getViewPortId());
            viewPort.makeCurrent(adfcContext);
            PageFlowStack stack = viewPort.getPageFlowStack();
            PageFlowStackEntry entry = null;
            boolean found = false;
            while (!found) {
                (new TaskFlowReturnActivityLogic()).abandonTaskFlow(adfcContext, stack.peek());
                entry = stack.pop(adfcContext);
                found = taskFlowUrl.equalsIgnoreCase(entry.getCallingViewActivity().toString());
            }
            if (entry != null) {
                ActivityId newViewActivityId = entry.getCallingViewActivity();
                viewPort.setViewActivityId(adfcContext, newViewActivityId);
            }
        } finally {
            adfcContext.getControllerState().setCurrentViewPort(adfcContext, currViewPort.getViewPortId());
            currViewPort.makeCurrent(adfcContext);
        }
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if ("W".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("warning");
        } else if ("I".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("info");
        } else if ("M".equals(messageType)) {
            this.getErrorWarnInfoIcon().setName("logo");
        } else {
            this.getErrorWarnInfoIcon().setName("error");
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    protected String getMessage(String messageCode) {
        return this.getMessage(messageCode, "W", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                               (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
    }

    protected void setErrorOnField(RichInputText input, RichIcon icon) {
        this.addErrorStyleToComponent(input);
        icon.setName("error");
        this.refreshContentOfUIComponent(icon);
        this.setFocusOnUIComponent(input);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon) {
        this.removeErrorOfField(input, icon, "required");
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon, String name) {
        this.removeErrorStyleToComponent(input);
        icon.setName(name);
        this.refreshContentOfUIComponent(icon);
    }

    protected boolean isMessageShown() {
        return StringUtils.isNotEmpty((String) this.getErrorWarnInfoMessage().getValue());
    }
    
    protected void clearCurrentUser() {
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        currentUser.setFacilityId(null);
        currentUser.setLanguageCode(null);
        currentUser.setUserId(null);
        currentUser.setUserName(null);
        currentUser.setUserPrivilege(0);
        
//        com.ross.rdm.common.view.CurrentUser currentUser2 = (com.ross.rdm.common.view.CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
//        currentUser2.setFacilityId(null);
//        currentUser2.setLanguageCode(null);
//        currentUser2.setUserId(null);
//        currentUser2.setUserPrivilege(0);
    }
}
