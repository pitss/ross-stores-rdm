package com.ross.rdm.common.view.uishell.errorhandling;


import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.event.DialogEvent;


public class PageTemplateErrorHandler {

    private String errorMessage;
    private String stackTrace;
    private static final ADFLogger logger =
        ADFLogger.createADFLogger(PageTemplateErrorHandler.class);
    public static final String OK = "ok";
    
    private static final String ID_POPUP_ERROR = "popupDialog";
    private static final String ID_POPUP_STACKTRACE = "stackTracePopup";

    public static final String HEADER_MAILERROR_TEXT_DEFAULT = "Possible mail error";
    public static final String MESSAGE_MAILERROR_TEXT_DEFAULT = "Error while sending email. Email may not have been send.";

    public static final String EL_HEADER_MAILERROR = "#{nls['email.senderror.header']}";
    public static final String EL_MESSAGE_MAILERROR = "#{nls['email.senderror.message']}";

    public PageTemplateErrorHandler() {
        super();
    }

    public void sampleDialogListener(DialogEvent dialogEvent) {
        if (OK.equals(dialogEvent.getOutcome().toString())) {
           
            RichPopup source = (RichPopup)findComponent(ID_POPUP_STACKTRACE);
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            source.show(hints);
        }
    }

    public void showErrorPopUp() {
        logger.severe("popUp with errormessage: " + errorMessage);
            //temporary systout the stacktracke:
            logger.fine("Stacktracke is: " + stackTrace);

        FacesContext context = FacesContext.getCurrentInstance();
        RichPopup source = (RichPopup)findComponent(ID_POPUP_ERROR);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();

        if (source != null) {
            source.show(hints);
        } else {
            logger.severe("RichPopup popupDialog (hardcoded) not found " +
                          "- stacktrace : " + stackTrace);
            String errorMsgStackTrc = errorMessage + " - " + stackTrace;
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsgStackTrc,
                                 null);
            context.addMessage(null, msg);
        }

    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getStackTrace() {
        return stackTrace;
    }
    
    /**
     * aggregating information for output, e.g. for email
     * @return concatenation of ErrorMessage and StackTrace
     */
    public String getMessageAndStack(){
        String newLine = System.getProperty("line.separator");
        String user = ADFContext.getCurrent().getSecurityContext().getUserName();
        StringBuffer sb = new StringBuffer();

        sb.append("PITSS SEME Error Handler reporting automatically"+newLine);
        sb.append("Application : "+getAppName()+newLine);
        sb.append("-- more information could be delivererd here -- "+newLine);
        sb.append("User is logged in as : "+user+newLine);
        sb.append(newLine);
        sb.append(getErrorMessage()+newLine);
        sb.append(newLine);
        sb.append(getStackTrace());

        //logger.finest("Concateneation results in : "+newLine+sb);
        return sb.toString();
    }
    
    /**
     *  Application Name without brackets for email sender e.g.
     */
    public String getAppName(){
        String appName =  ADFContext.getCurrent().getApplicationName();
        //version is appended in brackets eg "MasterApp(V2.0)"
        appName=appName.trim();
        appName=appName.replace(" ", "");
        //brackets are interpreted as name of sender by outlook, removing them
        appName=appName.replace("(","_");
        appName=appName.replace(")","_");
        //if last char is underscore remove it
        if (appName.charAt(appName.length()-1)=='_') 
            appName = appName.substring(0,appName.length()-1);
        return appName;
    }
    
    private void closePopup(UIComponent popup){
        if (popup != null && (popup instanceof RichPopup)){
            RichPopup rp = (RichPopup)popup;
            if (rp.isRendered()){
                rp.hide();
            }
        }
    }

    public UIComponent findComponentInRoot(String id) {
        UIComponent component = null;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            UIComponent root = facesContext.getViewRoot();
            component = findComponent(root, id);
        }
        return component;
    }

    public UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId()))
            return base;

        UIComponent children = null;
        UIComponent result = null;
        Iterator childrens = base.getFacetsAndChildren();
        while (childrens.hasNext() && (result == null)) {
            children = (UIComponent)childrens.next();
            if (id.equals(children.getId())) {
                result = children;
                break;
            }
            result = findComponent(children, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public UIComponent findComponent(String id) {
        return findComponent(FacesContext.getCurrentInstance().getViewRoot(),
                             id);
    }

    public String mapErrorMessage(String errorMessage) {
        String mappedError = null;
        //ComponentContainerService componentContainerService =
        //           (ComponentContainerService)ADFUtils.getApplicationModule("ComponentContainerServiceDataControl");
        try {
            mappedError =
                    errorMessage; //componentContainerService.errorMappingService(errorMessage);
        } catch (Exception e) {  
            logger.severe(e.getMessage());
        }

        return mappedError;
    }

}
