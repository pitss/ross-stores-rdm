package com.ross.rdm.common.view.userlogon.view.bean;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class UserLogonBean  {
  private String rp2rrodesformat;  // Item/Parameter user_logon.RP2RRODESFORMAT
  private String rp2rrodesname;  // Item/Parameter user_logon.RP2RRODESNAME
  private String rp2rrodestype;  // Item/Parameter user_logon.RP2RRODESTYPE
  private String rp2rroreportserver;  // Item/Parameter user_logon.RP2RROREPORTSERVER
  private String rp2rrovirtualdir;  // Item/Parameter user_logon.RP2RROVIRTUALDIR
  private String rp2rroreportsinterface;  // Item/Parameter user_logon.RP2RROREPORTSINTERFACE
  private String rp2rrousequeuetables;  // Item/Parameter user_logon.RP2RROUSEQUEUETABLES
  private String rp2rrouseorarrp;  // Item/Parameter user_logon.RP2RROUSEORARRP
  private String orarrpphysicaldir;  // Item/Parameter user_logon.ORARRPPHYSICALDIR
  private String orarrpvirtualdir;  // Item/Parameter user_logon.ORARRPVIRTUALDIR
  private String orarrphostname;  // Item/Parameter user_logon.ORARRPHOSTNAME

      public void initTaskFlow() {

            //Forms  Module : user_logon
            //Object        : WHEN-NEW-FORM-INSTANCE
            //Source Code   : 
            //
            //:SYSTEM.MESSAGE_LEVEL := 5;
            //--SELECT USER INTO :blk1.user_ID FROM DUAL;
            //

}

  public void setRp2rrodesformat(String rp2rrodesformat)  {
    this.rp2rrodesformat = rp2rrodesformat;
  }

  public String getRp2rrodesformat()  {
    return rp2rrodesformat;
  }
  public void setRp2rrodesname(String rp2rrodesname)  {
    this.rp2rrodesname = rp2rrodesname;
  }

  public String getRp2rrodesname()  {
    return rp2rrodesname;
  }
  public void setRp2rrodestype(String rp2rrodestype)  {
    this.rp2rrodestype = rp2rrodestype;
  }

  public String getRp2rrodestype()  {
    return rp2rrodestype;
  }
  public void setRp2rroreportserver(String rp2rroreportserver)  {
    this.rp2rroreportserver = rp2rroreportserver;
  }

  public String getRp2rroreportserver()  {
    return rp2rroreportserver;
  }
  public void setRp2rrovirtualdir(String rp2rrovirtualdir)  {
    this.rp2rrovirtualdir = rp2rrovirtualdir;
  }

  public String getRp2rrovirtualdir()  {
    return rp2rrovirtualdir;
  }
  public void setRp2rroreportsinterface(String rp2rroreportsinterface)  {
    this.rp2rroreportsinterface = rp2rroreportsinterface;
  }

  public String getRp2rroreportsinterface()  {
    return rp2rroreportsinterface;
  }
  public void setRp2rrousequeuetables(String rp2rrousequeuetables)  {
    this.rp2rrousequeuetables = rp2rrousequeuetables;
  }

  public String getRp2rrousequeuetables()  {
    return rp2rrousequeuetables;
  }
  public void setRp2rrouseorarrp(String rp2rrouseorarrp)  {
    this.rp2rrouseorarrp = rp2rrouseorarrp;
  }

  public String getRp2rrouseorarrp()  {
    return rp2rrouseorarrp;
  }
  public void setOrarrpphysicaldir(String orarrpphysicaldir)  {
    this.orarrpphysicaldir = orarrpphysicaldir;
  }

  public String getOrarrpphysicaldir()  {
    return orarrpphysicaldir;
  }
  public void setOrarrpvirtualdir(String orarrpvirtualdir)  {
    this.orarrpvirtualdir = orarrpvirtualdir;
  }

  public String getOrarrpvirtualdir()  {
    return orarrpvirtualdir;
  }
  public void setOrarrphostname(String orarrphostname)  {
    this.orarrphostname = orarrphostname;
  }

  public String getOrarrphostname()  {
    return orarrphostname;
  }
}
