package com.ross.rdm.common.view.hhchangepasswords.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingPageFlowBean;

import java.util.Map;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------

public class HhChangePasswordSBean extends RDMHotelPickingPageFlowBean {
    @SuppressWarnings("compatibility:-3909994477013807110")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhChangePasswordSBean.class);

    //todo was this suppress implemented correctly?
    private String isFocusOn;
    private String storePassword;
    private boolean passwordIsStored = false;

    public void initTaskFlowChangePassword() {
        _logger.info("initTaskFlowChangePassword() Start");
        this.initGlobalVariablesChangePassword();
        this.initHhChangePasswordWorkVariables();
        this.setHhChangePasswordMainCurrentRow();
        this.callCalcPasswordAge();
        this.setIsFocusOn("newPasswordField");
        _logger.info("initTaskFlowChangePassword() End");
    } 

    private void initGlobalVariablesChangePassword() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesChangePassword");
        oper.execute();
    }

    private void initHhChangePasswordWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhChangePasswordWorkVariables");
        oper.execute();
    }
    
    private void callCalcPasswordAge() {
        _logger.info("callCalcPasswordAge() Start");
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String userId = (String) ADFUtils.getBoundAttributeValue("UserId");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCalcPasswordAge");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("userId", userId);
        oper.execute();
        _logger.info("callCalcPasswordAge() End");
    }

    public void setHhChangePasswordMainCurrentRow() {
        ADFUtils.findOperation("setChangePasswordMainCurrentRow").execute();
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setStorePassword(String storePassword) {
        this.storePassword = storePassword;
    }

    public String getStorePassword() {
        return storePassword;
    }

    public void setPasswordIsStored(boolean passwordIsStored) {
        this.passwordIsStored = passwordIsStored;
    }

    public boolean isPasswordIsStored() {
        return passwordIsStored;
    }


}
