package com.ross.rdm.common.view.userlogon.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;
import com.ross.rdm.common.view.common.SessionManager;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.io.Serializable;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import weblogic.security.URLCallbackHandler;

import weblogic.servlet.security.ServletAuthentication;

public class UserLoginBean extends RDMHotelPickingBackingBean implements Serializable {
    @SuppressWarnings("compatibility:-8968417704477696234")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(UserLoginBean.class);
    private String userName;
    private String password;
    private RichOutputText errorOutputText;
    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichPanelFormLayout panelFormLayoutBinding;
    private RichInputText userNameBinding;
    private RichInputText passwordBinding;
    private int loginFailed = 0;
    private String taskFlowId =
        "/WEB-INF/com/ross/rdm/hotelpicking/hhchangepasswords/view/taskflow/HhChangePasswordSTaskFlow.xml#HhChangePasswordSTaskFlow";
    private String isFocusOn;
    
    private RichPopup changePasswordPopup;


    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setLoginFailed(int loginFailed) {
        this.loginFailed = loginFailed;
    }

    public int getLoginFailed() {
        return loginFailed;
    }

    public UserLoginBean() {
        super();
    }

    public void setUserName(String userName) {
        if (userName != null)
            this.userName = userName.toUpperCase();
     }

    public String getUserName() {
        return userName;
    }

    public void setPassward(String passward) {
        this.password = passward;
    }

    public String getPassward() {
        return password;
    }

    public void doLogin(ActionEvent actionEvent) {
        _logger.info("doLogin() Start");
        //null check the username and password
        if (null != this.getUserName() && null != this.getPassward()) {
            FacesContext fctx = FacesContext.getCurrentInstance();
            ExternalContext ectx = fctx.getExternalContext();
            HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
            //Construct user login CallBackHandler - compiling username and login information
            CallbackHandler handler = new URLCallbackHandler(this.getUserName(), this.getPassward().getBytes());
            try {
                Subject sub = weblogic.security.services.Authentication.login(handler);
                weblogic.servlet.security.ServletAuthentication.runAs(sub, request);
                this.checkLoginDate();
                int shouldChangePassword = this.shouldChangePassword();
                HttpSession session = (HttpSession) ectx.getSession(true);
                session.setAttribute("shouldChangePassword", "N");
                if (shouldChangePassword == -1 || shouldChangePassword == -2) {
                    session.setAttribute("shouldChangePassword", "Y");
                    continueLoggingIn();
                }
                else if(shouldChangePassword == -3){
                    this.getChangePasswordPopup().show(new RichPopup.PopupHints());
                }
                else{
                    continueLoggingIn();
                }
                
                
            } catch (Exception e) {
                //If general Exception occured, something else is wrong
                _logger.severe("Exception occured during login attempt : " + e.getMessage());
                //e.printStackTrace();
                //do invalid login logic
                invalidLogin();
            }
        } else {
            //do invalid login logic
            invalidLogin();
        }
        _logger.info("doLogin() End");
    }
    
    public void continueLoggingIn(){
        
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
        //Construct user login CallBackHandler - compiling username and login information
        CallbackHandler handler = new URLCallbackHandler(this.getUserName(), this.getPassward().getBytes());
        try {
            Subject sub = weblogic.security.services.Authentication.login(handler);
            weblogic.servlet.security.ServletAuthentication.runAs(sub, request);
            this.checkUserPrivilege();
            this.setUpCurrentUser();
            //Initiate authentication
            ServletAuthentication.generateNewSessionID(request);
            String successUrl = "/adfAuthentication?success_url=/faces/welcome";
            HttpServletResponse response = (HttpServletResponse) fctx.getExternalContext().getResponse();
            RequestDispatcher dispatcher = request.getRequestDispatcher(successUrl);
            dispatcher.forward(request, response);
            fctx.responseComplete();
        } catch (FailedLoginException fle) {
            //To catch invalid login attempts from Authenticator
            _logger.warning("FailedLoginException during login attempt : " + fle.getMessage());
            invalidLogin();
        } catch (LoginException le) {
            //Catch Potential Authenticator Issue - this exception is thrown in certain cases.
            _logger.warning("LoginException occured during login attempt : " + le.getMessage());
            //le.printStackTrace();
            // do invalid login logic
            invalidLogin();
        } catch (Exception e) {
            //If general Exception occured, something else is wrong
            _logger.severe("Exception occured during login attempt : " + e.getMessage());
            //e.printStackTrace();
            //do invalid login logic
            invalidLogin();
        }
    }
    
    public void yesLinkChangePasswordActionListener(ActionEvent actionEvent) {
        
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpSession session = (HttpSession) ectx.getSession(true);
        session.setAttribute("shouldChangePassword", "Y");
        this.getChangePasswordPopup().hide();
        continueLoggingIn();
        
    }
    
    public void noLinkChangePasswordActionListener(ActionEvent actionEvent) {
        
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpSession session = (HttpSession) ectx.getSession(true);
        session.setAttribute("shouldChangePassword", "N");
        this.getChangePasswordPopup().hide();
        continueLoggingIn();
        
    }

    private void invalidLogin() {
        _logger.info("invalidLogin start()");
        loginFailed = Integer.valueOf(loginFailed + 1);
        _logger.warning("Login Failed with user id: " + getUserName());
        _logger.fine("loginFailed value: " + loginFailed);
        this.showMessagesPanel("E", "Invalid Login");
        //Set UIComponent Focus
        /*2015-11-12: Gavin: Test cases require focus on Password now..
            this.setFocusOnUIComponent(this.getUserNameBinding());
            this.selectTextOnUIComponent(this.getUserNameBinding());
            */
        AdfFacesContext.getCurrentInstance().getViewScope().put("password", null);
        //set password to null
        this.setFocusOnUIComponent(this.getPasswordBinding());
        this.setPassward(null);
        this.refreshContentOfUIComponent(this.getPasswordBinding());
        if (loginFailed >= 3) {
            _logger.warning("user login attempts greater or equal than 3. Closing Enterprise Browser");

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExtendedRenderKitService service =
                Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
            service.addScript(facesContext, " location.href = \"ida:IDA_PROGRAM_EXIT\" ;window.close(); ");
        }
        _logger.info("invalidLogin end()");
    }

    private void showMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed", "Invalid Badge and Password");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, fm);
    }

    private boolean checkMaxUsers() throws Exception {
        _logger.info("checkMaxUsers() Start");
        executeOperation("getMaxUserCount");
        int maxUsers = (Integer) getOperationResult("getMaxUserCount");
        if (maxUsers < SessionManager.getSessionCount()) {
            throw new Exception("MAX users");
        }
        _logger.info("checkMaxUsers() End");
        return true;
    }

    private boolean checkLoginDate() throws Exception {
        executeOperation("checkLoginDate");
        return (Boolean) getOperationResult("checkLoginDate");
    }

    private int shouldChangePassword() throws Exception {
        executeOperation("shouldChangePassword");
        return (Integer) getOperationResult("shouldChangePassword");
    }

    private void checkUserPrivilege() throws Exception {
        executeOperation("checkUserPrivilege");
    }

    private boolean executeOperation(String operationName) throws Exception {
        _logger.info("executeOperation() Start");
        OperationBinding operation = ADFUtils.findOperation(operationName);
        operation.execute();
        List errors = operation.getErrors();
        if (null != errors && errors.size() > 0) {
            throw new Exception(operationName + " : " + errors.get(0));
        }
        _logger.info("executeOperation() End");
        return true;
    }

    private Object getOperationResult(String operationName) {
        OperationBinding operation = ADFUtils.findOperation(operationName);
        return operation.getResult();
    }

    private void setUpCurrentUser() {
        _logger.info("setUpCurrentUser() Start");
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        currentUser.setFacilityId((String) ADFUtils.getBoundAttributeValue("FacilityId"));
        currentUser.setUserId((String) ADFUtils.getBoundAttributeValue("UserId"));
        currentUser.setLanguageCode((String) ADFUtils.getBoundAttributeValue("LanguageCode"));
        currentUser.setUserPrivilege((Integer) ADFUtils.getBoundAttributeValue("UserPrivilege"));
        currentUser.setUserName((String) ADFUtils.getBoundAttributeValue("UserName"));
        _logger.info("setUpCurrentUser() End");
    }

    public void setErrorOutputText(RichOutputText errorOutputText) {
        this.errorOutputText = errorOutputText;
    }

    public RichOutputText getErrorOutputText() {
        return errorOutputText;
    }

    private void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        // CLEAR PREVIOUS MESSAGES
        this.getErrorOutputText().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getErrorPanel().setVisible(false);

        if ("W".equals(messageType)) {
            this.getIconErrorMessage().setName("warning");
        } else if ("I".equals(messageType) || "M".equals(messageType)) {
            this.getIconErrorMessage().setName("info");
        } else {
            this.getIconErrorMessage().setName("error");
        }
        this.getErrorOutputText().setValue(msg);
        this.getErrorOutputText().setVisible(true);
        this.getErrorPanel().setVisible(true);
        this.getIconErrorMessage().setVisible(true);
        this.refreshContentOfUIComponent(this.getIconErrorMessage());
        this.refreshContentOfUIComponent(this.getErrorPanel());
        this.refreshContentOfUIComponent(this.getErrorOutputText());
        _logger.info("showMessagesPanel() End");
    }

    public void refreshContentOfUIComponent(UIComponent component) {
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(component);
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void cancelLogin(ActionEvent actionEvent) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        service.addScript(facesContext, "window.open('', '_parent', ''); window.close();");
    }


    public void setPanelFormLayoutBinding(RichPanelFormLayout panelFormLayoutBinding) {
        this.panelFormLayoutBinding = panelFormLayoutBinding;
    }

    public RichPanelFormLayout getPanelFormLayoutBinding() {
        return panelFormLayoutBinding;
    }

    public void setUserNameBinding(RichInputText userNameBinding) {
        this.userNameBinding = userNameBinding;
    }

    public RichInputText getUserNameBinding() {
        return userNameBinding;
    }

    public void setPasswordBinding(RichInputText passwordBinding) {
        this.passwordBinding = passwordBinding;
    }

    public RichInputText getPasswordBinding() {
        return passwordBinding;
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public void setChangePasswordPopup(RichPopup changePasswordPopup) {
        this.changePasswordPopup = changePasswordPopup;
    }

    public RichPopup getChangePasswordPopup() {
        return changePasswordPopup;
    }


}
