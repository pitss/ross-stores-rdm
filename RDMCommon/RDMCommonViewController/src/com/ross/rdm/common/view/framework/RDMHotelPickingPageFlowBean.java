package com.ross.rdm.common.view.framework;

import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpSession;

import oracle.adf.share.ADFContext;

public class RDMHotelPickingPageFlowBean extends RDMMobileBaseBackingBean implements Serializable{
    @SuppressWarnings("compatibility:8563706124832025506")
    private static final long serialVersionUID = 1L;

    public RDMHotelPickingPageFlowBean() {
        super();
    }
    
    public String onLogout() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        
        String url = ectx.getRequestContextPath() +
                 "/adfAuthentication?logout=true&end_url=/faces/login.jsf"; 
        HttpSession session = (HttpSession)ectx.getSession(false); 
        
        this.clearCurrentUser();
        ectx.getSessionMap().clear(); //OTM 4310
        session.invalidate(); 
        
        try {
            ectx.redirect(url);
        } catch (IOException e) {
            //e.printStackTrace();
        }
        fctx.responseComplete();
        return null;
    }
    
    public Object getSessionScopeBean(String beanName){
        return ADFContext.getCurrent().getSessionScope().get(beanName);
    }
}
