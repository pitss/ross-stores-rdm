package com.ross.rdm.common.view.uishell.errorhandling;


import com.ross.rdm.common.utils.view.util.JSFUtils;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.ExceptionHandler;


public class ExceptionHandlerBean extends ExceptionHandler {
    private static final ADFLogger logger = ADFLogger.createADFLogger(ExceptionHandlerBean.class);

    public ExceptionHandlerBean() {
        super();
    }

    public void handleException(FacesContext facesContext, Throwable throwable, PhaseId phaseId) {
        logger.severe("Exception handled via ExceptionHandlerBean:");
        logger.severe(throwable);
        RDMExceptionPopupBean exceptionPopupBean =
            (RDMExceptionPopupBean) JSFUtils.resolveExpression("#{RDMExceptionPopupBean}");
        if (exceptionPopupBean != null) {
            exceptionPopupBean.showExitPopup();
        }
    }
}
