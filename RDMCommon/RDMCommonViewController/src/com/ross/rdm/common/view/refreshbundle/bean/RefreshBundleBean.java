package com.ross.rdm.common.view.refreshbundle.bean;

import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;

import java.io.IOException;

import java.util.ResourceBundle;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpSession;

import oracle.adf.share.logging.ADFLogger;

public class RefreshBundleBean extends RDMMobileBaseBackingBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(RefreshBundleBean.class);
    private final String[] bundleNames = new String[] {
        "com.ross.rdm.common.view.framework.DatabaseMessageBundleAm",
        "com.ross.rdm.common.view.framework.DatabaseMessageBundleSp",
        "com.ross.rdm.common.view.framework.DatabaseResourceBundleAm",
        "com.ross.rdm.common.view.framework.DatabaseResourceBundleSp",
    };

    public RefreshBundleBean() {
        super();
    }

    public String refreshBundleAction() {
        ResourceBundle.clearCache();
        _logger.info("Cache cleared");

        for (String resourcebundle : bundleNames) {
            _logger.info("Refreshing bundle: " + resourcebundle);
            ResourceBundle refreshedBundle = ResourceBundle.getBundle(resourcebundle);
            String refreshedKey = refreshedBundle.getString("CONTINUE");
            _logger.info("RefreshedKey: " + refreshedKey);
        }

        showMessagesPanel("M", "The resource bundles have been refreshed");

        return null;
    }

    public String logout() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        String url = ectx.getRequestContextPath() + "/adfAuthentication?logout=true&end_url=/faces/login.jsf";
        HttpSession session = (HttpSession) ectx.getSession(false);
        
        this.clearCurrentUser();
        ectx.getSessionMap().clear(); //OTM 4310
        session.invalidate();

        try {
            ectx.redirect(url);
        } catch (IOException e) {
            //e.printStackTrace();
        }
        fctx.responseComplete();
        return null;
    }
}
