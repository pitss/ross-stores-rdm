package com.ross.rdm.common.view.framework;


/**
 * Implementaton of custom RDM error handling model.
 * Intended as base for derivation for page flow scope beans.
 */
public class ErrorMessageModelBase implements IErrorMessageModel {
    
    private String messageIcon;
    private String messageText;
    private String messageId;
    
    private String focusControl;
    
    public ErrorMessageModelBase() {
        super();
    }
    
    public void setMessageIcon(String messageIcon) {
        this.messageIcon = messageIcon;
    }

    public String getMessageIcon() {
        return messageIcon;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setFocusControl(String focusControlId) {
        this.focusControl = focusControlId;
    }

    public String getFocusControl() {
        return focusControl;
    }

    @Override
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @Override
    public String getMessageId() {
        return messageId;
    }
}
