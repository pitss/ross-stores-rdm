package com.ross.rdm.common.view.uishell.errorhandling;

import com.ross.rdm.common.utils.view.util.JSFUtils;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.share.logging.ADFLogger;

public class TaskFlowErrorHandler {
    private static final ADFLogger logger = ADFLogger.createADFLogger(TaskFlowErrorHandler.class);

    public TaskFlowErrorHandler() {
        super();
    }

    public void handleException() {
        ControllerContext context = ControllerContext.getInstance();
        ViewPortContext currentRootViewPort = context.getCurrentViewPort();
        Exception exceptionData = currentRootViewPort.getExceptionData();

        logger.severe("Exception handled via TaskFlowErrorHandler:");
        logger.severe(exceptionData);
        RDMExceptionPopupBean exceptionPopupBean =
            (RDMExceptionPopupBean) JSFUtils.resolveExpression("#{RDMExceptionPopupBean}");
        if (exceptionPopupBean != null) {
            exceptionPopupBean.showExitPopup();
        }
    }
}
