package com.ross.rdm.common.view.framework;


/**
 * Generalized interface to help implement the non-standard error handling
 * in RDM applications.
 */
public interface IErrorMessageModel {    
    public void setMessageIcon(String messageIcon);
    public String getMessageIcon();

    public void setMessageText(String messageText);
    public String getMessageText();
    
    public void setFocusControl(String controlId);
    public String getFocusControl();
    
    public void setMessageId(String messageId);
    public String getMessageId();
}
