package com.ross.rdm.common.view.hhmenuss.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.io.Serializable;

import java.util.ArrayList;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichLink;


public class MenuBacking extends RDMHotelPickingBackingBean implements Serializable {
    @SuppressWarnings("compatibility:-5633137286294205826")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(MenuBacking.class);
    private RichButton gotosubmenuBean;
    private RichButton gotosubmenuBean1;
    private String isFocusOn;
    private RichTable menuTable;
    private RichTable subMenuTable;
    private RichPopup popupExit;
    private RichPopup popupExitSM;
    private RichLink popupExitYesLink;
    private RichLink popupExitNoLink;
    private RichLink popupExitNoLinkSM;

    public MenuBacking() {
    }


    public void saveMenuName() {
        HhMenusSBean pfb = (HhMenusSBean) this.getPageFlowBean("HhMenusSBean");
        ArrayList listMenusNames = pfb.getPreviousMenusNames();
        listMenusNames.add((String) ADFUtils.getBoundAttributeValue("OptionCommand"));
        pfb.setPreviousMenusNames(listMenusNames);
        pfb.setMenuLevel(listMenusNames.size());
    }


    public String goSubMenuButton() {
        return this.executeButtonLogic();
    }

    public String goSubMenuButton1() {
        return this.executeButtonLogic();
    }

    private HhMenusSBean getMenuPageFlowBean() {
        return (HhMenusSBean) this.getPageFlowBean("HhMenusSBean");
    }

    private String executeButtonLogic() {
        String recType = (String) ADFUtils.getBoundAttributeValue("RecordType");
        String opton = (String) ADFUtils.getBoundAttributeValue("OptionCommand");
        if ("M".equalsIgnoreCase(recType)) {
            this.saveMenuName();
            this.setMainMenu(opton);
            this.setMainMenuOrTaskFlowUrl(opton);
        } else
            this.setMainMenuOrTaskFlowUrl((String) ADFUtils.getBoundAttributeValue("TaskFlowUrl"));
        this.setRecordType(recType);
        return "SubMenu";
    }

    public String previousMenuAction() {
        HhMenusSBean menusSBean = this.getMenuPageFlowBean();
        if (menusSBean != null) {
            ArrayList listMenus = menusSBean.getPreviousMenusNames();
            if (listMenus != null) {
                int listMenuSize = listMenus.size();
                if (listMenuSize >= 2) {
                    String previousMenuName = (String) listMenus.get(listMenuSize - 2);
                    this.setMainMenu(previousMenuName);
                    this.setMainMenuOrTaskFlowUrl(previousMenuName);
                    listMenus.remove(listMenuSize - 1);
                    menusSBean.setMenuLevel(listMenus.size());
                }
            }
        }
        return "backMenu";
    }

    private void setMainMenuOrTaskFlowUrl(String command) {
        HhMenusSBean menusSBean = this.getMenuPageFlowBean();
        menusSBean.setMainMenuOrTaskFlowUrl(command);
    }

    private void setMainMenu(String command) {
        HhMenusSBean menusSBean = this.getMenuPageFlowBean();
        menusSBean.setMenuName(command);
    }

    public void gotoSubMenuLink(ActionEvent action) {
        ActionEvent actionEvent = new ActionEvent(this.getGotosubmenuBean());
        actionEvent.queue();
    }

    public void gotoSubMenuLink1(ActionEvent action) {
        ActionEvent actionEvent = new ActionEvent(this.getGotosubmenuBean1());
        actionEvent.queue();
    }

    private void setRecordType(String recType) {
        ADFContext.getCurrent().getPageFlowScope().put("RecordType", recType);
    }

    public void setGotosubmenuBean(RichButton gotosubmenuBean) {
        this.gotosubmenuBean = gotosubmenuBean;
        this.writeJavaScriptToClient(" SetFocusOnUIcomp(customFindElementById('table1'));");
    }

    public void setGotosubmenuBean1(RichButton gotosubmenuBean) {
        this.gotosubmenuBean1 = gotosubmenuBean;
        this.writeJavaScriptToClient(" SetFocusOnUIcomp(customFindElementById('table2'));");
    }

    public void onMenuRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onMenuRegionLoad() Start");
        this.setLocalFocusOnMenu(this.getMenuTable(), "initFocusMenu();");
        _logger.info("onMenuRegionLoad() End");
    }

    public void onSubMenuRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onSubMenuRegionLoad() Start");
        if (!isNavigationExecuted()) {
            this.setLocalFocusOnMenu(this.getSubMenuTable(), "initFocusSubMenu();");
        }
        _logger.info("onSubMenuRegionLoad() End");

    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_common_view_SubMenu_SubMenu_SubMenuMenuPageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public void setLocalFocusOnMenu(UIComponent component, String focusFunc) {
        _logger.info("setLocalFocusOnMenu() Start");
        _logger.fine("component value: " + component + "\n" + "focus value: " + focusFunc);
        StringBuilder script = new StringBuilder("");
        script.append(focusFunc);
        this.writeJavaScriptToClient(script.toString());
        _logger.info("setLocalFocusOnMenu() End");
    }


    public void setPopupExit(RichPopup popupExit) {
        this.popupExit = popupExit;
    }

    public RichPopup getPopupExit() {
        return popupExit;
    }

    public void setPopupExitSM(RichPopup popupExitSM) {
        this.popupExitSM = popupExitSM;
    }

    public RichPopup getPopupExitSM() {
        return popupExitSM;
    }

    public void setPopupExitYesLink(RichLink popupExitYesLink) {
        this.popupExitYesLink = popupExitYesLink;
    }

    public RichLink getPopupExitYesLink() {
        return popupExitYesLink;
    }

    public void setPopupExitNoLink(RichLink popupExitNoLink) {
        this.popupExitNoLink = popupExitNoLink;
    }

    public RichLink getPopupExitNoLink() {
        return popupExitNoLink;
    }

    public void setPopupExitNoLinkSM(RichLink popupExitNoLinkSM) {
        this.popupExitNoLinkSM = popupExitNoLinkSM;
    }

    public RichLink getPopupExitNoLinkSM() {
        return popupExitNoLinkSM;
    }


    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public RichButton getGotosubmenuBean() {
        return gotosubmenuBean;
    }

    public void setMenuTable(RichTable menuTable) {
        this.menuTable = menuTable;
    }

    public RichTable getMenuTable() {
        return menuTable;
    }

    public RichButton getGotosubmenuBean1() {
        return gotosubmenuBean1;
    }

    public void setSubMenuTable(RichTable subMenuTable) {
        this.subMenuTable = subMenuTable;
    }

    public RichTable getSubMenuTable() {
        return subMenuTable;
    }
    
    public String yesCloseActionMenu(){
        getPopupExit().hide();
        return "logOut";
    }
    
    public String yesCloseActionSubMenu(){
        getPopupExitSM().hide();
        return "logOut";
    }
}
