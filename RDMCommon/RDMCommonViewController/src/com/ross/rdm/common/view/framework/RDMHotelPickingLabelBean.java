package com.ross.rdm.common.view.framework;

import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class RDMHotelPickingLabelBean {
    private static final String GENERIC_PAGEDEFINITION = "com_ross_rdm_common_view_welcomePageDef";

    public RDMHotelPickingLabelBean() {
        super();
    }

    public String attributeLabel(String attrCode) {
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        if ("AM".equalsIgnoreCase(currentUser.getLanguageCode())) {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseResourceBundleAm", attrCode);
        } else {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseResourceBundleSp", attrCode);
        }
    }

    public String attributeLabel(String facilityId, String langCode, String attrCode) {
        return attributeLabel(attrCode);
    }

    public String databaseMessage(String attrCode) {
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        if ("AM".equalsIgnoreCase(currentUser.getLanguageCode())) {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseMessageBundleAm", attrCode);
        } else {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseMessageBundleSp", attrCode);
        }
    }

    private String getValueFromBundle(String bundle, String attrCode) {
        String value = null;
        try {
            value = ResourceBundle.getBundle(bundle).getString(attrCode);
        } catch (MissingResourceException e) {
            value = attrCode;
        }
        return value;
    }
}
