package com.ross.rdm.common.view.framework;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ListResourceBundle;
import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.jbo.uicli.binding.JUCtrlActionBinding;

public class DatabaseResourceBundle extends ListResourceBundle {
    private Object[][] stored = null;
    private final String GENERIC_PAGEDEFINITION = "com_ross_rdm_common_view_welcomePageDef";
    private final String FACILITY_PR = "PR";
    private final String SPANISH_LOCALE = "SP";

    protected Object[][] getContents() {
        if (stored == null) {
            initResourceBundle();
        }
        return stored;
    }

    /** synchronized initialization of reading resource bundle */
    public synchronized void initResourceBundle() {
        Map map = getResourceBundle(getLocaleCode());
       //System.out.println("~~~~~~~~~~~ Initializing Resource bundle ~~~~~~~~~~~ LOCALE: " + getLocaleCode());
        // copy all values from map in object array
        if (map.size() == 0) {
            stored = new Object[0][0];
        } else {
            stored = new Object[map.size()][2];
            int ii = 0;
            for (Iterator keyIter = map.keySet().iterator(); keyIter.hasNext();) {
                Object key = keyIter.next();
                stored[ii][0] = key;
                stored[ii][1] = map.get(key);
                ii++;
            }
        }
    }


    public String getLocaleCode() {
        return "SP";
    }

    private Map getResourceBundle(String locale) {
        Map map = new HashMap();
        BindingContext bindingContext = BindingContext.getCurrent();
        if (bindingContext == null) {
            throw new RuntimeException("No ADFM Binding Context found!!");
        }
        DCBindingContainer container = bindingContext.findBindingContainer(GENERIC_PAGEDEFINITION);
        if (container != null) {
            JUCtrlActionBinding resBind = (JUCtrlActionBinding) container.findCtrlBinding("getResourceBundle");
            resBind.getParamsMap().put("locale", SPANISH_LOCALE.equalsIgnoreCase(locale) ? locale : "AM");
            Object result = resBind.execute();
            if (result instanceof Map) {
                map = (Map) result;
            }
        }
        return map;
    }
}
