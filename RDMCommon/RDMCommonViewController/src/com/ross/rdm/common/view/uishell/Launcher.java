package com.ross.rdm.common.view.uishell;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import java.util.Set;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.nav.RichCommandNavigationItem;
import oracle.adf.view.rich.event.ItemEvent;


/**
 * Launcher is a backingBean-scope managed bean. The public methods are
 * available to EL. The methods call TabContext APIs available to the
 * Dynamic Tab Shell Template. The boolean value for _launchActivity
 * determines whether another tab instance is created or selected. Each tab
 * (i.e., task flow) is tracked by ID. The title is the tab label.
 */
public class Launcher {
    public static final String KEY_TASK_FLOW_PARM = "taskFlowPar";

    private int currentTabId = 0;
    private String titleString = "";

    private static final ADFLogger logger = ADFLogger.createADFLogger(Launcher.class);

    public HashMap splitTaskFlowParameters(String raw){
        HashMap result = new HashMap();
        String myRaw = raw.trim();
        
        String[] parameters = myRaw.split(KEY_TASK_FLOW_PARM);
        
        if (parameters != null){
            String k,v;
            int index;
            for (String par:parameters){
                if (par.isEmpty()) continue;
                par = par.trim();
                index = par.indexOf("=");
                if (index == 0) continue;
                k = ((index > 0) ? par.substring(0,index) : par).trim();
                v = ((par.length()>(index+1)&&(index>=0)) ? par.substring(index+1).trim() : null);
                result.put(k, v);
            }
        }
        
        return result;
    }
    
    private HashMap extractTaskFlowParameters(Map<String,Object> attributes){
        HashMap result = new HashMap();
        
        if (attributes != null){
            Set keys = attributes.keySet();
            Iterator keyIterator = keys.iterator();
            while (keyIterator.hasNext()){
                String key = (String)keyIterator.next();
                if (key.startsWith(KEY_TASK_FLOW_PARM)){
                    String parName = key.substring(KEY_TASK_FLOW_PARM.length());
                    Object parValue = attributes.get(key);
                    if ((parValue instanceof String) && ((String)parValue).startsWith(KEY_TASK_FLOW_PARM)){
                        HashMap params = splitTaskFlowParameters((String)parValue);
                        result.putAll(params);
                    }
                    else{
                        result.put(parName, parValue);
                    }
                }
            }
        }    
        return result;
    }
    
    public void multipleInstanceActivity(ActionEvent actionEvent) {
        /*
         * Read the MenuitemAttribute to get the information 
         * which flow should get opened.
         */

        UIComponent link = actionEvent.getComponent();
        titleString = (String) link.getAttributes().get("componentLabel");
        
        /*
         * Extended the possible attributes for calling this method.
         * alternatively to flowName+moduleName to construct the taskFlowUrl
         * the taskFlowUrl can be handed over directly to avoid the specific syntax/path conventions
         */
        
        /*
         * 
         * /WEB-INF/de/pitss/master/<moduleName>/view/flows/<flowName>.xml#<flowName>
         * 
         * Example:
         *  module: XY
         *  Dialog: MY-DIALOG
         *
         */

        String taskFlowUrl = (String) link.getAttributes().get("taskFlowUrl");
        if (taskFlowUrl==null) {
            String flowName    = (String) link.getAttributes().get("flowName");
            String moduleName  = (String) link.getAttributes().get("moduleName");
            
            taskFlowUrl = "/WEB-INF/de/pitss/master/" + moduleName + "/view/flows/"+ flowName + ".xml#"+flowName;
        }
        logger.warning("Open Task Flow: " + taskFlowUrl + "  -  title: " + titleString);

        /**
        * Example method when called repeatedly, will open another instance as
        * oppose to selecting a previously opened tab instance. Note the boolean
        * to create another tab instance is set to true.
        */
        
        //_launchActivity(titleString, ConfigFileLoader.getTASK_FLOW_URL(), true);
        _launchActivity(titleString, taskFlowUrl, true);

    }

    public void launchSimpleParamsTask(ActionEvent actionEvent) {
        UIComponent link = actionEvent.getComponent();
        titleString = (String) link.getAttributes().get("componentLabel");
        
        String taskFlowUrl = (String) link.getAttributes().get("taskFlowUrl");
        
        HashMap<String, Object> m = this.extractTaskFlowParameters(link.getAttributes());
        
        _launchActivity(titleString, taskFlowUrl, m);
    }


    public void closeCurrentActivity(ActionEvent actionEvent) {
        TabContext tabContext = TabContext.getCurrentInstance();
        int tabIndex = tabContext.getSelectedTabIndex();
        if (tabIndex != -1) {
            // only remove if active
            if (tabContext.getTabs().get(tabIndex).isActive()) tabContext.removeTab(tabIndex);
        }
    }

    /**
     *  copied from Summit ADF UI Shell, extended / corrected.
     *  closes all opened tabs.
     **/
    public void closeAllTabs(ActionEvent actionEvent){       
        List<Tab> tabs = TabContext.getCurrentInstance().getTabs();        
        if(tabs != null && tabs.size()> 0){
            for(Tab t : tabs){
                // only remove active ones - bugfixed
                if (t.isActive())
                    TabContext.getCurrentInstance().removeTab(t.getIndex());
            }              
        }
    }
    
    /**
    *  copied from Summit ADF UI Shell, extended / corrected.
    *  closes all opened tabs except the current.
    **/
    public void closeAllButCurrent(ActionEvent actionEvent){        
        List<Tab> tabs = TabContext.getCurrentInstance().getTabs();        
        if(tabs != null && tabs.size()> 0){
            int selectedTabIndex = TabContext.getCurrentInstance().getSelectedTabIndex();
            for (int i = 0; i < tabs.size(); i++) {
                if (selectedTabIndex != i) {
                    // only remove active ones - bugfixed
                   if (tabs.get(i).isActive())
                        TabContext.getCurrentInstance().removeTab(i);
                }
            }      
        }
    }


    public void currentTabDirty(ActionEvent e) {
        /**
        * When called, marks the current tab "dirty". Only at the View level
        * is it possible to mark a tab dirty since the model level does not
        * track to which tab data belongs.
        */
        TabContext tabContext = TabContext.getCurrentInstance();
        tabContext.markCurrentTabDirty(true);
    }

    public void currentTabClean(ActionEvent e) {
        TabContext tabContext = TabContext.getCurrentInstance();
        tabContext.markCurrentTabDirty(false);
    }

    private void _launchActivity(String title, String taskflowId, boolean newTab) {
        try {
            if (newTab) {
                // debug output            
                int tabIndex = TabContext.getCurrentInstance().getSelectedTabIndex() + 1;
                if (titleString == null || titleString.equals("")) {
                    titleString = "Activity " + tabIndex;    
                }                
                setCurrentTabId(tabIndex);
                TabContext.getCurrentInstance().addTab(titleString, taskflowId);
            } else {
                TabContext.getCurrentInstance().addOrSelectTab(title, taskflowId);
            }
        } catch (TabContext.TabOverflowException toe) {
            // causes a dialog to be displayed to the user saying that there are
            // too many tabs open - the new tab will not be opened...
            toe.handleDefault();
        }
    }
    
    private void _launchActivity(String titleString, String taskflowId, Map<String, Object> taskFlowParam) {
        try {           
                // debug output            
                int tabIndex = TabContext.getCurrentInstance().getSelectedTabIndex() + 1;
                if (titleString == null || titleString.equals("")) {
                    titleString = "Activity " + tabIndex;    
                }                
                setCurrentTabId(tabIndex);
            TabContext.getCurrentInstance().addTab(titleString, taskflowId, taskFlowParam);
           
        } catch (TabContext.TabOverflowException toe) {
            // causes a dialog to be displayed to the user saying that there are
            // too many tabs open - the new tab will not be opened...
            toe.handleDefault();
        }
    }

    public void launchFirstReplaceNPlace(ActionEvent actionEvent) {
        TabContext tabContext = TabContext.getCurrentInstance();
        try {
            tabContext.setMainContent("/WEB-INF/com/ross/rdm/common/view/uishell/blank.xml#blank"); //ConfigFileLoader.getTASK_FLOW_URL());
        } catch (TabContext.TabContentAreaDirtyException toe) {
            // warning user TabContext api needed for this use case.
        }
    }

    public void handleCloseTabItem(ItemEvent itemEvent) {
        if (itemEvent.getType().equals(ItemEvent.Type.remove)) {
            TabContext tabContext = TabContext.getCurrentInstance();
            Object item = itemEvent.getSource();
            if (item instanceof RichCommandNavigationItem) {
                RichCommandNavigationItem tabItem = (RichCommandNavigationItem)item;
                Object tabIndex = tabItem.getAttributes().get("tabIndex"); // NOTRANS
                tabContext.removeTab((Integer)tabIndex);
                // do other desired functionality here ...
            }
        }
    }
    
    /**
     *  remove Panel by CommandButton storing the tabIndex.
     *     moved from DashboardBean
     */
    public void removePanel(ActionEvent e) {
        Integer panelKey = _getAssociatedPanelKey(e.getComponent());
        TabContext tabContext = TabContext.getCurrentInstance();
        tabContext.removeTab(panelKey);
    }

    /** 
     *  helper for removePanel(ActionEvent)
     *     moved from DashboardBean
     * @param component CommandButton closing the tab/panel
     * @return Integer tabIndex, added to the component by the f:attribute tag
     */
    private Integer _getAssociatedPanelKey(UIComponent component) {
        Map<String, Object> attrs = component.getAttributes();
        return (Integer)attrs.get("tabIndex");
    }
    
    public int getCurrentTabId() {
        return currentTabId;
    }

    public void setCurrentTabId(int currentTabId) {
        this.currentTabId = currentTabId;
    }
    
}
