package com.ross.rdm.common.view.common;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Helper class to manage the handling of app-server user sessions.
 * Needed to implement the functionality like CHECK_MAX_USERS from
 * the original Forms applications.
 */
public class SessionManager implements HttpSessionListener {
    
    private static int activeSessions = 0;
    
    public SessionManager() {
        super();
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {        
        activeSessions++;        
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        activeSessions--;
    }
    
    public static int getSessionCount() {
        return activeSessions;
    }
}
