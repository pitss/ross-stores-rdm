package com.ross.rdm.common.view.framework;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;

import java.util.List;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.JboException;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class RDMBackingBeanBase extends RDMMobileRoleBasedBackingBean{
    
    private final static String ERROR_STYLE_CLASS = "p_AFError";
    private final static String BLACK_FONT_CSS = "color: Black;";
    private final static String RED_FONT_CSS = "color: Red;";
    private final static String PINK_BACKGROUND_CSS = "background-color: #FAFDFF;";    
    
    public RDMBackingBeanBase() {
        super();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////
    // error handling
    ////////////////////////////////////////////////////////////////////////////////////
    
    public RichPanelGroupLayout allMessagesPanel;    
    
    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
      this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel()  {
      return allMessagesPanel;
    }
    
    protected void showMessageRaw(String type, String rawText, RichInputText control) {
        setMessageId(null);
        showMessageCommon(type, rawText, control);
    }
    
    protected void showMessage(String type, String id, RichInputText control) {
     setMessageId(id);
     showMessageCommon(type,  getMessage(id, type, getFacilityId(), getLangCode()), control);
    }

    private void showMessageCommon(String type, String message, RichInputText control) {
     showMessagesPanel(type, message);
     if (null != control) {
         addErrorStyleToComponent(control);            
         setFocusOnUIComponent(control); 
         markFocus(control.getId());
     }        
    }
    
    protected void showMessagesPanel(String messageType, String msg) {
     if ("W".equals(messageType)) {
         setMessageIcon("warning");
     } else if ("I".equals(messageType)) {
         setMessageIcon("info");
     } else if ("M".equals(messageType)) {
         setMessageIcon("warning");
     } else if ("S".equals(messageType)) {
         setMessageIcon("logo");
     } else {
         setMessageIcon("error");
     }
     setMessageText(msg);     
     refreshContentOfUIComponent(getAllMessagesPanel());
    }
    
    protected void hideMessages() {
     setMessageText(null);
     setMessageIcon(null);
     markFocus(null);
     setMessageId(null);
     refreshContentOfUIComponent(getAllMessagesPanel());
    }
    
    protected void setMessageIcon(String name) {
     IErrorMessageModel pageFlowBean = getPageFlowBean();
     pageFlowBean.setMessageIcon(name);
    }
    
    protected void setMessageText(String text) {
     IErrorMessageModel pageFlowBean = getPageFlowBean();
     pageFlowBean.setMessageText(text);
    }
    
    protected void markFocus(String ctrlId) {
        IErrorMessageModel pageFlowBean = getPageFlowBean();
        pageFlowBean.setFocusControl(ctrlId);
    }
    
    protected void setMessageId(String messageId) {
        IErrorMessageModel pageFlowBean = getPageFlowBean();
        pageFlowBean.setMessageId(messageId);
    }
    
    // override in the derived class
    protected IErrorMessageModel getPageFlowBean() {
     return null;
    }
    
    protected void showFormatMessage(String type, String id1, String id2, String value, RichInputText control) {
     String firstPart = getMessage(id1, type, getFacilityId(), getLangCode());
     String secondPart = null;
     if (null != id2) {
         secondPart = getMessage(id2, type, getFacilityId(), getLangCode());
     }
     String message = firstPart + " " + value;
     if (null == secondPart) {
         message += ".";
     } else {
         message += " " + secondPart + ".";
     }
     
     setMessageId(id1);
     showMessageCommon(type, message, control);
    }
    
    protected void addErrorStyleToComponent(RichInputText uiComponent) {
        String styleClass = uiComponent.getStyleClass();
        if (styleClass != null && !styleClass.contains(ERROR_STYLE_CLASS)) {
            uiComponent.setStyleClass(styleClass.concat(" " + ERROR_STYLE_CLASS));
        } else {
            uiComponent.setStyleClass(ERROR_STYLE_CLASS);
        }

        String contentStyle = uiComponent.getContentStyle();
        if (contentStyle != null) {
            if (contentStyle.contains(BLACK_FONT_CSS)) {
                contentStyle = contentStyle.replace(BLACK_FONT_CSS, RED_FONT_CSS);
            } else {
                contentStyle = contentStyle.concat(RED_FONT_CSS);
            }
            contentStyle = contentStyle.concat(PINK_BACKGROUND_CSS);
            uiComponent.setContentStyle(contentStyle);
        } else {
            uiComponent.setContentStyle(RED_FONT_CSS + " " + PINK_BACKGROUND_CSS);
        }
        refreshContentOfUIComponent(uiComponent);
    }
    
    protected void removeErrorStyleToComponent(RichInputText uiComponent) {
        String styleClass = uiComponent.getStyleClass();
        if (styleClass != null) {
            uiComponent.setStyleClass(styleClass.replace(ERROR_STYLE_CLASS, ""));
        }

        String contentStyle = uiComponent.getContentStyle();
        if (contentStyle != null) {
            contentStyle = contentStyle.replace(RED_FONT_CSS, BLACK_FONT_CSS);
            contentStyle = contentStyle.replace(PINK_BACKGROUND_CSS, "");
            uiComponent.setContentStyle(contentStyle);
        }

        this.refreshContentOfUIComponent(uiComponent);
    }      
    
    /**
    *  To be able to use model validations with the non-standard error
    *  reporting in this project.
    */
    protected boolean checkValidationError(RichInputText control) {
     hideMessages();
     DCBindingContainer bindContainer = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
     if (!bindContainer.hasExceptions())
         return false;  // no validation error
     List exceptions = bindContainer.getExceptionsList();
     Object obj = exceptions.get(0);
     if (!(obj instanceof JboException))
         return false; // some other error we cannot handle
     JboException ex = (JboException)obj;
     showMessage("E", ex.getBaseMessage(), control);
     bindContainer.resetInputState(); // clear exceptions
     return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////
    // dialog framework
    ////////////////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Dynamic creation of a dialog listener.
     */
    protected MethodExpression makeDialogListener(String name) {
        Class[] argtypes = new Class[1];
        argtypes[0] = DialogEvent.class;
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        Application app = facesCtx.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesCtx.getELContext();
        return elFactory.createMethodExpression(elContext, name, null, argtypes);
    }   
    
    protected void askUser(RichPopup confirmationPopup, boolean isConfirmationOnly, String question, String dialogListener) {                  
        askUser(confirmationPopup, isConfirmationOnly, question, dialogListener, null);
    }
    
    protected void askUser(RichPopup confirmationPopup, boolean isConfirmationOnly, String question, String dialogListener, String parameter) {
        RichDialog dialog = (RichDialog)confirmationPopup.getChildren().get(0);        
          
        // setup dynamic dialog listener
        if (null != dialogListener) {
              MethodExpression listener = makeDialogListener("#{" + dialogListener + "}");
              dialog.setDialogListener(listener);
        }
        
        RichPanelLabelAndMessage plam = (RichPanelLabelAndMessage)dialog.getChildren().get(0);
        UIComponent facet = plam.getFacet("end");
        RichOutputText message = (RichOutputText)facet.findComponent("ot4");
        String translation = getMessage(question, "C", getFacilityId(), getLangCode());
        if (null != parameter) {
            translation += " " + parameter + ".";
        }
        message.setValue(translation);
        
        JSFUtils.setExpressionValue("#{viewScope.isConfirmationOnly}", isConfirmationOnly);
          
        confirmationPopup.show(new RichPopup.PopupHints());                
    }          
    
    ////////////////////////////////////////////////////////////////////////////////////
    // utilities
    ////////////////////////////////////////////////////////////////////////////////////    
    
    protected void writeJavaScriptToClient(String script) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        service.addScript(facesContext, script);
    } 
    
    protected void navigateToAction(String action) {
        FacesContext fc = FacesContext.getCurrentInstance();
        NavigationHandler nh = fc.getApplication().getNavigationHandler();
        nh.handleNavigation(fc, null, action);
    }
    
    protected void updateModel(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    } 
    
    protected boolean executeOperation(String operationName) {
        oracle.binding.OperationBinding operation = ADFUtils.findOperation(operationName);
        operation.execute();
        List errors = operation.getErrors();
        if (null != errors && errors.size() > 0) {            
            return false;
        }
        
        return true;        
    }
    
    protected Object getOperationResult(String operationName) {
        oracle.binding.OperationBinding operation = ADFUtils.findOperation(operationName);
        return operation.getResult();
    }      
    
    protected String getMessage(String msgCode, String msgType, String facilityId, String langCode) {
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        if ("AM".equalsIgnoreCase(currentUser.getLanguageCode())) {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseMessageBundleAm", msgCode);
        } else {
            return getValueFromBundle("com.ross.rdm.common.view.framework.DatabaseMessageBundleSp", msgCode);
        }
    }
    
    private String getValueFromBundle(String bundle, String attrCode) {
        String value = null;
        try {
            value = ResourceBundle.getBundle(bundle).getString(attrCode);
        } catch (MissingResourceException e) {
            value = attrCode;
        }
        return value;
    }
    
    /**
    * sends a partial page submit for the given component
    *
    * @param  component on page
    */
    public void refreshContentOfUIComponent(UIComponent component) {
      AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
      adfFacesContext.addPartialTarget(component);
    }
    
    protected void setFocusOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("if(textInput != null){textInput.select();}");
        writeJavaScriptToClient(script.toString());

        //refreshContentOfUIComponent(component);
    }         
    
    ////////////////////////////////////////////////////////////////////////////////////
    // current user
    ////////////////////////////////////////////////////////////////////////////////////        
    
    protected CurrentUser getCurrentUser() {
        return (CurrentUser)JSFUtils.getManagedBeanValue("CurrentUser");
    }
    
    protected String getFacilityId() {        
        return getCurrentUser().getFacilityId();        
    }
    
    protected String getLangCode() {
        return getCurrentUser().getLanguageCode();
    }  
    
    
}
