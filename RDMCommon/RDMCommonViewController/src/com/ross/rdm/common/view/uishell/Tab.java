package com.ross.rdm.common.view.uishell;

import java.io.Serializable;

import java.util.Collections;

import java.util.List;

import java.util.Map;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCExecutableBindingDef;


/**
 * This class is not meant for public API.
 *
 * Copyright 2010, Oracle USA, Inc.
 */
public class Tab implements Serializable {
  public void setTitle(String title) {
    _localizedName = title;
  }
  
  public String getTitle() {
    return _localizedName;
  }
  
  public int getIndex() {
    return _index;
  }
  /**
   * This will return the binding container associated with the current tab.And this should return a non-null value 
   * unless user invokes it from a execution context whose bindingContainer does't have a reference to dynamic tab shell page template.
   * @return binding container associated with current tab
   */
  public DCBindingContainer getBinding() {

    DCBindingContainer bindingContainer =
      (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry().get("r" + _index); // NOTRANS
    //If tab.getBinding() invoked from actual template then BindingContext.getCurrent().getCurrentBindingsEntry() will return template's pagedefinition file
    //but if it is invoked from a consuming page then BindingContext.getCurrent().getCurrentBindingsEntry() will refer to the consuming page's pagedef file
    //Then we need to look at all executable bindings which are refering to "oracle.ui.pattern.dynamicShell.model.dynamicTabShellDefinition" 
    //and use that to retrieve the tab binding.
    if (bindingContainer == null) {
      List execBindings =
        ((DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry()).getExecutableBindings();
      if (execBindings != null) {
        for (Object binding: execBindings) {
          DCExecutableBindingDef execDef = ((DCExecutableBindingDef) ((DCBindingContainer) binding).getExecutableDef());
          if (execDef != null &&
              "oracle.ui.pattern.dynamicShell.model.dynamicTabShellDefinition".equals(execDef.getFullName())) // NOTRANS
          {
            return (DCBindingContainer) ((DCBindingContainer) binding).get("r" + _index); // NOTRANS
          }
        }
      }
    }
    return bindingContainer;
  }
  
  public void setActive(boolean rendered) {
    _isActive = rendered;
    
    if (!_isActive)
      setDirty(false);
  }
  
  public boolean isActive() {
    return _isActive;
  }
  
  public void setDirty(boolean isDirty) {
    _isDirty = isDirty;
  }
  
  public boolean isDirty() {
    return _isDirty;
  }

  public void setTaskflowId(TaskFlowId id) {
    _taskflowId = id;
  }
  
  public TaskFlowId getTaskflowId() {
    return _taskflowId;
  }
  
  public void setParameters(Map<String,Object> parameters) {
    _parameters = parameters;    
  }
  
  public Map<String,Object> getParameters() {
    return _parameters;
  }
  
  public List<Tab> getChildren() {
    return Collections.emptyList();
  }

  Tab(int index, TaskFlowId id) {
    _index = index;
    _taskflowId = id;
  }
  
  private final int _index;
  
  private boolean _isActive = false;
  private boolean _isDirty = false;
  private String _localizedName;
  private TaskFlowId  _taskflowId;
  private Map<String,Object> _parameters;
}
