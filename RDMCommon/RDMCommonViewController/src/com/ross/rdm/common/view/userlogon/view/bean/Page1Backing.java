package com.ross.rdm.common.view.userlogon.view.bean;

import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import javax.faces.event.ValueChangeEvent;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page1.jspx
// ---    
// ---------------------------------------------------------------------
public class Page1Backing extends RDMHotelPickingBackingBean {

  public Page1Backing() {

  }

  public void onChangedUserPasswordBlk1(ValueChangeEvent vce) {
    //Forms  Module : user_logon
    //Object        : BLK1.USER_PASSWORD.KEY-NEXT-ITEM
    //Source Code   : 
    //
    //Checking1;


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : user_logon
    //Object      : BLK1.USER_PASSWORD.KEY-NEXT-ITEM
    //
    //
    //checking1;
    // --------------------------------------------------------------------------------
  }

  public void onChangedFacilityIdBlk1(ValueChangeEvent vce) {
    //Forms  Module : user_logon
    //Object        : BLK1.FACILITY_ID.KEY-NEXT-ITEM
    //Source Code   : 
    //
    //CHECK_FACILITY;


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : user_logon
    //Object      : BLK1.FACILITY_ID.KEY-NEXT-ITEM
    //
    //
    //check_facility;
    // --------------------------------------------------------------------------------
  }

}
