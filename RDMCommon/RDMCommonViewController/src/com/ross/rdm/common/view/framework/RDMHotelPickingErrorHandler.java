package com.ross.rdm.common.view.framework;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.JboException;

public class RDMHotelPickingErrorHandler {
    private static ADFLogger _logger = ADFLogger.createADFLogger(RDMHotelPickingErrorHandler.class);
    private static final String EXCEPTION_OCCURED = "Exception occured <> ";

    public void handleException() {
        ControllerContext controllerContext = ControllerContext.getInstance();
        ViewPortContext currentViewPort = controllerContext.getCurrentViewPort();

        if (currentViewPort.isExceptionPresent()) {
            _logger.severe(EXCEPTION_OCCURED + currentViewPort.getViewId(), currentViewPort.getExceptionData());
            Exception ex = controllerContext.getCurrentViewPort().getExceptionData();
            JboException jboException = null;

            if (ex instanceof JboException) {
                jboException = (JboException) ex;
            } else {
                jboException = new JboException(ex);
            }

            String errorMessage = jboException.getMessage();
            FacesContext fc = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, null);
            fc.addMessage(null, facesMessage);
//            controllerContext.getCurrentRootViewPort().clearException();
            fc.renderResponse();
        }
    }

}