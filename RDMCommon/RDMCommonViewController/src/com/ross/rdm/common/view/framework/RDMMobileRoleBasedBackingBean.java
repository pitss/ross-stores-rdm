package com.ross.rdm.common.view.framework;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.common.CurrentUser;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import oracle.binding.OperationBinding;

public class RDMMobileRoleBasedBackingBean  extends RoleBasedAccessConstants{
    public RDMMobileRoleBasedBackingBean() {
        super();
    }
    
    protected boolean callCheckScreenOptionPrivilege(String optionName, String formName) {
        CurrentUser currentUser = (CurrentUser) JSFUtils.getManagedBeanValue("CurrentUser");
        OperationBinding oper = ADFUtils.findOperation("callCheckScreenOptionPriv");
        oper.getParamsMap().put("facilityId", currentUser.getFacilityId());
        oper.getParamsMap().put("userId", currentUser.getUserId());
        oper.getParamsMap().put("formName", formName);
        oper.getParamsMap().put("optionName", optionName);
        String result = (String) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if ("OFF".equals(result)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
