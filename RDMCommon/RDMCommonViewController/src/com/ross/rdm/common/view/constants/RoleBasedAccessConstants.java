package com.ross.rdm.common.view.constants;

public class RoleBasedAccessConstants {
    public RoleBasedAccessConstants() {
        super();
    }
      public static final String      FORM_NAME_hh_xref_container_s="hh_xref_container_s";
      public static final String      FORM_NAME_hh_container_inquiry="hh_container_inquiry";
      public static final String      FORM_NAME_hh_inv_item_trouble_s="hh_inv_item_trouble_s";
      public static final String      FORM_NAME_hh_invalid_container_s="hh_invalid_container_s";
      public static final String      FORM_NAME_hh_inventory_verification_s="hh_inventory_verification_s";
      public static final String      FORM_NAME_hh_ross_cont_inquiry_s="hh_ross_cont_inquiry_s";
      public static final String      FORM_NAME_hh_move_inventory_s="hh_move_inventory_s";
      public static final String      FORM_NAME_hh_putaway_inventory_s="hh_putaway_inventory_s";
      public static final String      FORM_NAME_hh_ross_build_container_s="hh_ross_build_container_s";
      public static final String      FORM_NAME_hh_siphon_mlp_s="hh_siphon_mlp_s";
      public static final String      FORM_NAME_hh_move_siphon_inventory_s="hh_move_siphon_inventory_s";
      public static final String      FORM_NAME_hh_split_container_s="hh_split_container_s";
      public static final String      FORM_NAME_hh_add_to_container_s="hh_add_to_container_s";
      public static final String      FORM_NAME_hh_task_queue_s="hh_task_queue_s";
      public static final String      FORM_NAME_hh_container_pick_s="hh_container_pick_s";
      public static final String      FORM_NAME_hh_pick_across_wv_s="hh_pick_across_wv_s";
      public static final String      FORM_NAME_hh_no_pick_pack_s="hh_no_pick_pack_s";
      public static final String      FORM_NAME_sg_pts_pick_print_s="sg_pts_pick_print_s";
      public static final String      FORM_NAME_hh_work_processing_s="hh_work_processing_s";
      public static final String      FORM_NAME_hh_mark_wip_management_s="hh_mark_wip_management_s";
      public static final String      FORM_NAME_hh_process_wip_s="hh_process_wip_s";
      public static final String      FORM_NAME_sg_pts_close_cntr_s="sg_pts_close_cntr_s";
      public static final String      FORM_NAME_sg_pts_picking_s="sg_pts_picking_s";
      public static final String      FORM_NAME_sg_container_re_induct_s="sg_container_re_induct_s";
      public static final String      FORM_NAME_hh_dctodc_rcvng_s="hh_dctodc_rcvng_s";
      public static final String      FORM_NAME_hh_freight_bill_verify_s="hh_freight_bill_verify_s";
      public static final String      FORM_NAME_hh_mlp_change_item_s="hh_mlp_change_item_s";
      public static final String      FORM_NAME_hh_initiate_unload_s="hh_initiate_unload_s";
      public static final String      FORM_NAME_hh_conveyor_cutoff_s="hh_conveyor_cutoff_s";
      public static final String      FORM_NAME_hh_load_container_s="hh_load_container_s";
      public static final String      FORM_NAME_hh_print_ship_label_s="hh_print_ship_label_s";
      public static final String      FORM_NAME_hh_qc_outbound_audit_s="hh_qc_outbound_audit_s";
      public static final String      FORM_NAME_hh_reset_trailer_s="hh_reset_trailer_s";
      public static final String      FORM_NAME_hh_ship_trailer_s="hh_ship_trailer_s";
      public static final String      FORM_NAME_hh_shipping_repack_s="hh_shipping_repack_s";
      public static final String      FORM_NAME_hh_unload_container_s="hh_unload_container_s";
      public static final String      FORM_NAME_sg_stager_release_s="sg_stager_release_s";
      public static final String      FORM_NAME_sg_stager_release_mlp_s="sg_stager_release_mlp_s";
      public static final String      FORM_NAME_sg_stager_move_s="sg_stager_move_s";
      public static final String      FORM_NAME_sg_stager_move_all_s="sg_stager_move_all_s";
      public static final String      FORM_NAME_hh_location_inquiry_s="hh_location_inquiry_s";
      public static final String      FORM_NAME_hh_move_trailer_s="hh_move_trailer_s";
      public static final String      FORM_NAME_hh_shuttle_trailer_s="hh_shuttle_trailer_s";
      public static final String      FORM_NAME_hh_trailer_check_in_s="hh_trailer_check_in_s";
      public static final String      FORM_NAME_hh_trailer_check_out_s="hh_trailer_check_out_s";
      public static final String      FORM_NAME_hh_trailer_inquiry_s="hh_trailer_inquiry_s";
      public static final String      FORM_NAME_hh_cont_pk_across_wv_s="hh_cont_pk_across_wv_s";
      public static final String      FORM_NAME_hh_bulk_pick_s="hh_bulk_pick_s";
      public static final String      FORM_NAME_hh_bulk_pk_across_wv_s="hh_bulk_pk_across_wv_s";


      
      public static final String              OPTION_NAME_hh_xref_container_s_SPLIT_CONTAINER_F8="SPLIT_CONTAINER.F8";
      public static final String              OPTION_NAME_hh_container_inquiry_MAIN_F2="MAIN.F2";
      public static final String              OPTION_NAME_hh_container_inquiry_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_container_inquiry_MAIN_F5="MAIN.F5";
      public static final String              OPTION_NAME_hh_container_inquiry_MAIN_F8="MAIN.F8";
      public static final String              OPTION_NAME_hh_container_inquiry_MAIN_F9="MAIN.F9";
      public static final String              OPTION_NAME_hh_inv_item_trouble_s_CONTAINER_F4="CONTAINER.F4";
      public static final String              OPTION_NAME_hh_inv_item_trouble_s_CONTAINER_F9="CONTAINER.F9";
      public static final String              OPTION_NAME_hh_inv_item_trouble_s_ITEM_TROUBLE_CODE_F4="ITEM_TROUBLE_CODE.F4";
      public static final String              OPTION_NAME_hh_invalid_container_s_LPN_F2="LPN.F2";
      public static final String              OPTION_NAME_hh_invalid_container_s_LPN_F4="LPN.F4";
      public static final String              OPTION_NAME_hh_invalid_container_s_LPN_F5="LPN.F5";
      public static final String              OPTION_NAME_hh_invalid_container_s_DETAIL_F4="DETAIL.F4";
      public static final String              OPTION_NAME_hh_invalid_container_s_DETAIL_F9="DETAIL.F9";
      public static final String              OPTION_NAME_hh_invalid_container_s_DETAIL_F10="DETAIL.F10";
      public static final String              OPTION_NAME_hh_inventory_verification_s_INV_VERIFICATION_F4="INV_VERIFICATION.F4";
      public static final String              OPTION_NAME_hh_inventory_verification_s_INV_VERIFICATION_F5="INV_VERIFICATION.F5";
      public static final String              OPTION_NAME_hh_ross_cont_inquiry_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_2_F7="PAGE_2.F7";
      public static final String              OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_2_F8="PAGE_2.F8";
      public static final String              OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_2_F9="PAGE_2.F9";
      public static final String              OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_3_F7="PAGE_3.F7";
      public static final String              OPTION_NAME_hh_ross_cont_inquiry_s_PAGE_3_F8="PAGE_3.F8";
      public static final String              OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F2="MOVE_BLOCK.F2";
      public static final String              OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F4="MOVE_BLOCK.F4";
      public static final String              OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F5="MOVE_BLOCK.F5";
      public static final String              OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F6="MOVE_BLOCK.F6";
      public static final String              OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F7="MOVE_BLOCK.F7";
      public static final String              OPTION_NAME_hh_move_inventory_s_MOVE_BLOCK_F8="MOVE_BLOCK.F8";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MAIN_F5="MAIN.F5";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MAIN_F8="MAIN.F8";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MULTI_SKU_F4="MULTI_SKU.F4";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MULTI_SKU_F8="MULTI_SKU.F8";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_NEW_FPL_F4="NEW_FPL.F4";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MAIN_INTRLVG_F4="MAIN_INTRLVG.F4";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_MAIN_INTRLVG_F7="MAIN_INTRLVG.F7";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_REASON_F4="REASON.F4";
      public static final String              OPTION_NAME_hh_putaway_inventory_s_REASON_F6="REASON.F6";
      public static final String              OPTION_NAME_hh_ross_build_container_s_CONTAINER_F4="CONTAINER.F4";
      public static final String              OPTION_NAME_hh_siphon_mlp_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_siphon_mlp_s_MAIN_F9="MAIN.F9";
      public static final String              OPTION_NAME_hh_siphon_mlp_s_PAGE_2_F4="PAGE_2.F4";
      public static final String              OPTION_NAME_hh_siphon_mlp_s_PAGE_2_F7="PAGE_2.F7";
      public static final String              OPTION_NAME_hh_siphon_mlp_s_PAGE_2_F8="PAGE_2.F8";
      public static final String              OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F2="MOVE_BLOCK.F2";
      public static final String              OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F4="MOVE_BLOCK.F4";
      public static final String              OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F6="MOVE_BLOCK.F6";
      public static final String              OPTION_NAME_hh_move_siphon_inventory_s_MOVE_BLOCK_F8="MOVE_BLOCK.F8";
      public static final String              OPTION_NAME_hh_split_container_s_SPLIT_CONTAINER_F4="SPLIT_CONTAINER.F4";
      public static final String              OPTION_NAME_hh_split_container_s_SPLIT_CONTAINER_F8="SPLIT_CONTAINER.F8";
      public static final String              OPTION_NAME_hh_split_container_s_SPLIT_CONTAINER_F9="SPLIT_CONTAINER.F9";
      public static final String              OPTION_NAME_hh_split_container_s_RETURN_REASON_F4="RETURN_REASON.F4";
      public static final String              OPTION_NAME_hh_add_to_container_s_CONTAINER_F4="CONTAINER.F4";
      public static final String              OPTION_NAME_hh_add_to_container_s_CONTAINER_F5="CONTAINER.F5";
      public static final String              OPTION_NAME_hh_add_to_container_s_POPUP_EDITOR_F3="POPUP_EDITOR.F3";
      public static final String              OPTION_NAME_hh_add_to_container_s_POPUP_EDITOR_F4="POPUP_EDITOR.F4";
      public static final String              OPTION_NAME_hh_task_queue_s_B_SEARCH_F1="B_SEARCH.F1";
      public static final String              OPTION_NAME_hh_task_queue_s_B_TASK_QUEUE_F1="B_TASK_QUEUE.F1";
      public static final String              OPTION_NAME_hh_task_queue_s_B_TASK_QUEUE_F4="B_TASK_QUEUE.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_START_PICK_F1="START_PICK.F1";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F5="CONTAINER_PICK.F5";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F6="CONTAINER_PICK.F6";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F7="CONTAINER_PICK.F7";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_F8="CONTAINER_PICK.F8";
      public static final String              OPTION_NAME_hh_container_pick_s_TO_LOCATION_F3="TO_LOCATION.F3";
      public static final String              OPTION_NAME_hh_container_pick_s_TO_LOCATION_F4="TO_LOCATION.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_TWO_F4="CONTAINER_PICK_TWO.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_TWO_F5="CONTAINER_PICK_TWO.F5";
      public static final String              OPTION_NAME_hh_container_pick_s_CONTAINER_PICK_TWO_F7="CONTAINER_PICK_TWO.F7";
      public static final String              OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD_F4="FROM_MASTER_CHILD.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD_F6="FROM_MASTER_CHILD.F6";
      public static final String              OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F4="TO_MASTER_CHILD.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F5="TO_MASTER_CHILD.F5";
      public static final String              OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F6="TO_MASTER_CHILD.F6";
      public static final String              OPTION_NAME_hh_container_pick_s_TO_MASTER_CHILD_F7="TO_MASTER_CHILD.F7";
      public static final String              OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F4="FROM_MASTER_CHILD2.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F5="FROM_MASTER_CHILD2.F5";
      public static final String              OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F6="FROM_MASTER_CHILD2.F6";
      public static final String              OPTION_NAME_hh_container_pick_s_FROM_MASTER_CHILD2_F7="FROM_MASTER_CHILD2.F7";
      public static final String              OPTION_NAME_hh_container_pick_s_WEIGHT_F4="WEIGHT.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_REASON_F4="REASON.F4";
      public static final String              OPTION_NAME_hh_container_pick_s_REASON_F6="REASON.F6";
      public static final String              OPTION_NAME_hh_pick_across_wv_s_NO_PICK_PACK_F4="NO_PICK_PACK.F4";
      public static final String              OPTION_NAME_hh_no_pick_pack_s_NO_PICK_PACK_F4="NO_PICK_PACK.F4";
      public static final String              OPTION_NAME_sg_pts_pick_print_s_MAIN_F6="MAIN.F6";
      public static final String              OPTION_NAME_sg_pts_pick_print_s_PICK_PRINT_F4="PICK_PRINT.F4";
      public static final String              OPTION_NAME_sg_pts_pick_print_s_LPN_F4="LPN.F4";
      public static final String              OPTION_NAME_hh_work_processing_s_MAIN_F1="MAIN.F1";
      public static final String              OPTION_NAME_hh_work_processing_s_MAIN_F2="MAIN.F2";
      public static final String              OPTION_NAME_hh_work_processing_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_work_processing_s_WORK_QTY_F4="WORK_QTY.F4";
      public static final String              OPTION_NAME_hh_work_processing_s_WORK_PV_F2="WORK_PV.F2";
      public static final String              OPTION_NAME_hh_work_processing_s_WORK_EDIT_PV_F1="WORK_EDIT_PV.F1";
      public static final String              OPTION_NAME_hh_work_processing_s_WORK_EDIT_PV_F4="WORK_EDIT_PV.F4";
      public static final String              OPTION_NAME_hh_work_processing_s_USER_AUTH_F4="USER_AUTH.F4";
      public static final String              OPTION_NAME_hh_work_processing_s_EDIT_CODE_USER_AUTH="EDIT_CODE_USER_AUTH";
      public static final String              OPTION_NAME_hh_mark_wip_management_s_MAIN_F1="MAIN.F1";
      public static final String              OPTION_NAME_hh_mark_wip_management_s_MAIN_F2="MAIN.F2";
      public static final String              OPTION_NAME_hh_mark_wip_management_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_mark_wip_management_s_MARK_QTY_F4="MARK_QTY.F4";
      public static final String              OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F1="WIP_BLOCK.F1";
      public static final String              OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F4="WIP_BLOCK.F4";
      public static final String              OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F5="WIP_BLOCK.F5";
      public static final String              OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F6="WIP_BLOCK.F6";
      public static final String              OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F7="WIP_BLOCK.F7";
      public static final String              OPTION_NAME_hh_process_wip_s_WIP_BLOCK_F8="WIP_BLOCK.F8";
      public static final String              OPTION_NAME_hh_process_wip_s_INST_PRSNLZ_F3="INST_PRSNLZ.F3";
      public static final String              OPTION_NAME_hh_process_wip_s_INST_PRSNLZ_F8="INST_PRSNLZ.F8";
      public static final String              OPTION_NAME_sg_pts_close_cntr_s_B_PTS_CLOSE_F4="B_PTS_CLOSE.F4";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F1="B_PTS_PICKING.F1";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F2="B_PTS_PICKING.F2";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F4="B_PTS_PICKING.F4";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F5="B_PTS_PICKING.F5";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_PTS_LOC_F4="B_PTS_LOC.F4";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_ZONE_SELECT_F2="B_ZONE_SELECT.F2";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_ZONE_SELECT_F7="B_ZONE_SELECT.F7";
      public static final String              OPTION_NAME_sg_pts_picking_s_B_ZONE_SELECT_F8="B_ZONE_SELECT.F8";
      public static final String              OPTION_NAME_sg_pts_picking_s_PTS_PICKING_OVERAGE="PTS_PICKING_OVERAGE";
      public static final String              OPTION_NAME_sg_container_re_induct_s_B_RE_INDUCT_F4="B_RE_INDUCT.F4";
      public static final String              OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F5="DCTODC_RECEIVE.F5";
      public static final String              OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F6="DCTODC_RECEIVE.F6";
      public static final String              OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F7="DCTODC_RECEIVE.F7";
      public static final String              OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F8="DCTODC_RECEIVE.F8";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_FREIGHT_BILL_F4="FREIGHT_BILL.F4";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_FREIGHT_BILL_F6="FREIGHT_BILL.F6";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_FREIGHT_BILL_F7="FREIGHT_BILL.F7";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_CODES_F4="CODES.F4";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_CODES_F7="CODES.F7";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_CODES_F8="CODES.F8";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_SPLIT_FBS_F4="SPLIT_FBS.F4";
      public static final String              OPTION_NAME_hh_freight_bill_verify_s_GEN_FB_F4="GEN_FB.F4";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_MAIN_F2="MAIN.F2";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_MAIN_F5="MAIN.F5";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_MAIN_F9="MAIN.F9";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_PAGE_2_F4="PAGE_2.F4";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_PAGE_2_F7="PAGE_2.F7";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_PAGE_2_F8="PAGE_2.F8";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_PAGE_3_F4="PAGE_3.F4";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_PAGE_3_F7="PAGE_3.F7";
      public static final String              OPTION_NAME_hh_mlp_change_item_s_PAGE_3_F8="PAGE_3.F8";
      public static final String              OPTION_NAME_hh_initiate_unload_s_APPOINTMENT_F4="APPOINTMENT.F4";
      public static final String              OPTION_NAME_hh_initiate_unload_s_APPOINTMENT_F5="APPOINTMENT.F5";
      public static final String              OPTION_NAME_hh_initiate_unload_s_APPOINTMENT_F7="APPOINTMENT.F7";
      public static final String              OPTION_NAME_hh_conveyor_cutoff_s_CONVEYOR_F1="CONVEYOR.F1";
      public static final String              OPTION_NAME_hh_load_container_s_LOAD_CONTAINER_F3="LOAD_CONTAINER.F3";
      public static final String              OPTION_NAME_hh_print_ship_label_s_MAIN_F6="MAIN.F6";
      public static final String              OPTION_NAME_hh_print_ship_label_s_PRINT_SHIP_LABEL_REPRINT_AUTH_F6="PRINT_SHIP_LABEL_REPRINT_AUTH.F6";
      public static final String              OPTION_NAME_hh_qc_outbound_audit_s_QC_AUDIT_F4="QC_AUDIT.F4";
      public static final String              OPTION_NAME_hh_reset_trailer_s_RESET_TRAILER_F4="RESET_TRAILER.F4";
      public static final String              OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_YARD_AUTH_F5="SHIP_TRAILER_YARD_AUTH.F5";
      public static final String              OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_F7="SHIP_TRAILER.F7";
      public static final String              OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_F8="SHIP_TRAILER.F8";
      public static final String              OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_F9="SHIP_TRAILER.F9";
      public static final String              OPTION_NAME_hh_ship_trailer_s_USER_PASS_F4="USER_PASS.F4";
      public static final String              OPTION_NAME_hh_shipping_repack_s_B_REPACK_F8="B_REPACK.F8";
      public static final String              OPTION_NAME_hh_unload_container_s_UNLOAD_CONTAINER_F3="UNLOAD_CONTAINER.F3";
      public static final String              OPTION_NAME_sg_stager_release_s_B_PAGE_2_F5="B_PAGE_2.F5";
      public static final String              OPTION_NAME_sg_stager_release_s_CONTROL_F4="CONTROL.F4";
      public static final String              OPTION_NAME_sg_stager_release_s_B_PAGE_6_F4="B_PAGE_6.F4";
      public static final String              OPTION_NAME_sg_stager_release_s_B_PAGE_6_F6="B_PAGE_6.F6";
      public static final String              OPTION_NAME_sg_stager_release_mlp_s_B_PAGE_2_F5="B_PAGE_2.F5";
      public static final String              OPTION_NAME_sg_stager_release_mlp_s_CONTROL_F4="CONTROL.F4";
      public static final String              OPTION_NAME_sg_stager_release_mlp_s_B_PAGE_6_F4="B_PAGE_6.F4";
      public static final String              OPTION_NAME_sg_stager_release_mlp_s_B_PAGE_6_F6="B_PAGE_6.F6";
      public static final String              OPTION_NAME_sg_stager_move_s_B_STAGER_MOVE_F4="B_STAGER_MOVE.F4";
      public static final String              OPTION_NAME_sg_stager_move_all_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_location_inquiry_s_MAIN_F3="MAIN.F3";
      public static final String              OPTION_NAME_hh_move_trailer_s_MAIN_F1="MAIN.F1";
      public static final String              OPTION_NAME_hh_move_trailer_s_MAIN_F4="MAIN.F4";
      public static final String              OPTION_NAME_hh_shuttle_trailer_s_SHUTTLE_LOAD_WAVED_CONT="SHUTTLE_LOAD_WAVED_CONT";
      public static final String              OPTION_NAME_hh_shuttle_trailer_s_SHUTTLE_TRAILER_F6="SHUTTLE_TRAILER.F6";
      public static final String              OPTION_NAME_hh_shuttle_trailer_s_SHUTTLE_TRAILER_F9="SHUTTLE_TRAILER.F9";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F2="CHECK_TRAILER.F2";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F4="CHECK_TRAILER.F4";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F5="CHECK_TRAILER.F5";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F9="CHECK_TRAILER.F9";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CODES_F4="CODES.F4";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CODES_F7="CODES.F7";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_CODES_F8="CODES.F8";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_BOL_POP_F2="BOL_POP.F2";
      public static final String              OPTION_NAME_hh_trailer_check_in_s_BOL_POP_F4="BOL_POP.F4";
      public static final String              OPTION_NAME_hh_trailer_check_out_s_TRAILER_CHECKOUT_F4="TRAILER_CHECKOUT.F4";
      public static final String              OPTION_NAME_hh_trailer_inquiry_s_TRAILER_INQUIRY_F2="TRAILER_INQUIRY.F2";
      public static final String              OPTION_NAME_hh_trailer_inquiry_s_SHUTTLE_INQUIRY_F2="SHUTTLE_INQUIRY.F2";
      public static final String              OPTION_NAME_hh_bulk_pk_across_wv_s_BULK_PICK_F4="BULK_PICK.F4";
      public static final String              OPTION_NAME_hh_bulk_pk_across_wv_s_BULK_PICK_F6="BULK_PICK.F6";
      public static final String              OPTION_NAME_hh_bulk_pk_across_wv_s_BULK_PICK_F7="BULK_PICK.F7";
      public static final String              OPTION_NAME_hh_bulk_pk_across_wv_s_REASON_F4="REASON.F4";
      public static final String              OPTION_NAME_hh_bulk_pk_across_wv_s_REASON_F6="REASON.F6";
      public static final String              OPTION_NAME_hh_bulk_pk_across_wv_s_TO_LOCATION_F4="TO_LOCATION.F4";
      public static final String              OPTION_NAME_hh_xref_container_s_MAIN_F2="SPLIT_CONTAINER.F8";
      public static final String              OPTION_NAME_hh_invalid_container_s_DETAIL_F2="DETAIL.F10";
      public static final String              OPTION_NAME_hh_inventory_verification_s_INV_VERIFICATION_F2="INV_VERIFICATION.F4";
  }
