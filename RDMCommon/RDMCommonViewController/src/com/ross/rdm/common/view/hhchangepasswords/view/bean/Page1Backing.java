package com.ross.rdm.common.view.hhchangepasswords.view.bean;


import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.util.Map;

import javax.faces.application.NavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpSession;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMHotelPickingBackingBean {
    @SuppressWarnings("compatibility:12684113575320380")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private final static String NEW_PASSWORD = "NewPassword";
    private final static String CONFIRM_PASSWORD = "ConfirmPassword";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String USER_ID_ATTR = "UserId";
    private final static String PASSWORD_CHANGED_ATTR = "GlobalPasswordChanged";
    private final static String HH_CHANGE_PASSWORD_FLOW_BEAN = "HhChangePasswordSBean";
    private RichInputText newPasswordMain;
    private RichInputText confirmPasswordMain;
    private RichInputText passwordAgeMain;
    private RichPanelFormLayout changePasswordPanel;
    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichOutputText errorMessage;
    private RichPopup exitPopup;
    private RichDialog confirmGoToDialog;
    private RichOutputText confirmGoToOutputtext;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichIcon newPasswordIcon;
    private RichIcon confirmPasswordIcon;

    public Page1Backing() {

    }

    public void onChangedNewPasswordMain(ValueChangeEvent vce) {
        _logger.info("onChangedNewPasswordMain() Start");
        _logger.fine("vce value: " + vce);
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String newPassword = ((String) vce.getNewValue());
            String returnVal = callCheckUserPassword(newPassword);
            if (returnVal.equalsIgnoreCase("ON")) {
                this.showErrorPanel(this.getMessage("PASSWORD_SAME", "E", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()));
                this.getNewPasswordIcon().setName("error");
                this.refreshContentOfUIComponent(this.getNewPasswordIcon());
                this.addErrorStyleToComponent(this.getNewPasswordMain());
                this.setFocusNewPassword();
                this.selectTextOnUIComponent(this.getNewPasswordMain());
            } else {
                Boolean validPassword = this.callCheckNewPassword(newPassword);
                if (validPassword) {
                    ADFUtils.setBoundAttributeValue(NEW_PASSWORD, newPassword);
                    this.hideMessagesPanel();
                    this.getNewPasswordIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getNewPasswordIcon());
                    this.removeErrorStyleToComponent(this.getNewPasswordMain());
                    this.refreshContentOfUIComponent(this.getNewPasswordMain());
                    this.setFocusConfirmPassword();
                } else {
                    this.showErrorPanel(this.getMessage("INV_PASSWORD", "E", this.getFacilityIdAttrValue(),
                                                        this.getLangCodeAttrValue()));
                    this.getNewPasswordIcon().setName("error");
                    this.refreshContentOfUIComponent(this.getNewPasswordIcon());
                    this.addErrorStyleToComponent(this.getNewPasswordMain());
                    this.selectTextOnUIComponent(this.getNewPasswordMain());
                    this.setFocusNewPassword();
                }
            }
        }
        _logger.info("onChangedNewPasswordMain() End");

    }

    private String callCheckUserPassword(String newPassword) {
        _logger.info("callCheckUserPassword() Start");
        _logger.fine("New Password value: " + newPassword);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckUserPassword");
        Map map = oper.getParamsMap();
        map.put("facilityId", this.getFacilityIdAttrValue());
        map.put("userId", this.getUserIdAttrValue());
        map.put("newPassword", newPassword);
        oper.execute();
        String privilege = (String) oper.getResult();
        _logger.info("callCheckUserPassword() End");
        return privilege;
    }


    private boolean callCheckNewPassword(String newPassword) {
        _logger.info("callCheckNewPassword() Start");
        _logger.fine("New Password value: " + newPassword);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckNewPassword");
        Map map = oper.getParamsMap();
        map.put("facilityId", this.getFacilityIdAttrValue());
        map.put("userId", this.getUserIdAttrValue());
        map.put("newPassword", newPassword);
        oper.execute();
        boolean validPassword = (Boolean) oper.getResult();
        _logger.fine("validPassword value: " + validPassword);
        _logger.info("callCheckNewPassword() End");
        return validPassword;
    }

    private boolean callChangePassword(String newPassword) {
        _logger.info("callChangePassword() Start");
        _logger.fine("New Password value: " + newPassword);
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callChangePassword");
        Map map = oper.getParamsMap();
        map.put("facilityId", this.getFacilityIdAttrValue());
        map.put("userId", this.getUserIdAttrValue());
        map.put("newPassword", newPassword);
        oper.execute();
        Boolean success = (Boolean) oper.getResult();
        _logger.fine("success value: " + success);
        _logger.info("callChangePassword() End");
        return success;
    }

    public void onChangedConfirmPasswordMain(ValueChangeEvent vce) {
        this.updateModel(vce);
        if (null != vce.getNewValue()) {
            String enteredConfirmPasswordValue = ((String) vce.getNewValue());
            this.verifyConfirmPassword(enteredConfirmPasswordValue);
        }
    }

    public boolean verifyConfirmPassword(String enteredConfirmPasswordValue) {
        _logger.info("verifyConfirmPassword() Start");
        _logger.fine("Confirm Password value: " + enteredConfirmPasswordValue);
        RichInputText uiComponent = this.getConfirmPasswordMain();
        String newPassword = (String) ADFUtils.getBoundAttributeValue(NEW_PASSWORD);

        if (newPassword.equals(enteredConfirmPasswordValue) || newPassword.equals(this.getHhChangePasswordSBeanPageFlowBean().getStorePassword())) {
            ADFUtils.setBoundAttributeValue(CONFIRM_PASSWORD, enteredConfirmPasswordValue);
            this.hideMessagesPanel();
            this.getConfirmPasswordIcon().setName("required");
            this.refreshContentOfUIComponent(this.getConfirmPasswordIcon());
            this.removeErrorStyleToComponent(uiComponent);
            this.refreshContentOfUIComponent(uiComponent);
            if(!this.getHhChangePasswordSBeanPageFlowBean().isPasswordIsStored()){
            this.getHhChangePasswordSBeanPageFlowBean().setStorePassword(enteredConfirmPasswordValue);
            this.getHhChangePasswordSBeanPageFlowBean().setPasswordIsStored(true);
            }
            System.out.println("New getconfirmedpassword = " + this.getHhChangePasswordSBeanPageFlowBean().getStorePassword());
            _logger.info("verifyConfirmPassword() End");
            return true;
        } else {
            this.refreshContentOfUIComponent(uiComponent);
            this.showErrorPanel(this.getMessage("PASSWORD_DIFF", "E", this.getFacilityIdAttrValue(),
                                                this.getLangCodeAttrValue()));
            this.getConfirmPasswordIcon().setName("error");
            this.refreshContentOfUIComponent(this.getConfirmPasswordIcon());
            this.addErrorStyleToComponent(uiComponent);
            this.selectTextOnUIComponent(uiComponent);
            _logger.info("verifyConfirmPassword() End");
            return false;
            }
    }

    private void clearAllErrors() {
        this.hideMessagesPanel();
        this.getNewPasswordIcon().setName("required");
        this.refreshContentOfUIComponent(this.getNewPasswordIcon());
        this.removeErrorStyleToComponent(this.getNewPasswordMain());
        this.refreshContentOfUIComponent(this.getNewPasswordMain());
        this.getConfirmPasswordIcon().setName("required");
        this.refreshContentOfUIComponent(this.getConfirmPasswordIcon());
        this.removeErrorStyleToComponent(this.getConfirmPasswordMain());
        this.refreshContentOfUIComponent(this.getConfirmPasswordMain());
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        _logger.fine("Key value: " + key);
        if ("F3".equalsIgnoreCase(key)) {
            this.clearAllErrors();
            String message =
                this.getMessage("PASSWRD_EXPIRED", "W", this.getFacilityIdAttrValue(), this.getLangCodeAttrValue());
            this.getConfirmGoToOutputtext().setValue(message);
            this.getExitPopup().show(new RichPopup.PopupHints());
            this.setFocusF3Link();
            this.refreshContentOfUIComponent(this.getChangePasswordPanel());
        } else if ("F4".equalsIgnoreCase(key)) {
            String newPassword = (String) ADFUtils.getBoundAttributeValue(NEW_PASSWORD);
            String confirmPassword = (String) ADFUtils.getBoundAttributeValue(CONFIRM_PASSWORD);
            String currentFocus = this.getHhChangePasswordSBeanPageFlowBean().getIsFocusOn();
            if ("newPasswordField".equals(currentFocus) && (newPassword == null || newPassword.isEmpty())) {
                this.showErrorPanel(this.getMessage("PARTIAL_ENTRY", "E", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()));
                this.getNewPasswordIcon().setName("error");
                this.refreshContentOfUIComponent(this.getNewPasswordIcon());
                this.addErrorStyleToComponent(this.getNewPasswordMain());
                this.setFocusNewPassword();
                this.selectTextOnUIComponent(this.getNewPasswordMain());
                this.refreshContentOfUIComponent(this.getChangePasswordPanel());
                _logger.info("trKeyIn() End");
                return;
            } else if ("confirmPasswordField".equals(currentFocus) &&
                       (confirmPassword == null || confirmPassword.isEmpty())) {
                this.showErrorPanel(this.getMessage("PARTIAL_ENTRY", "E", this.getFacilityIdAttrValue(),
                                                    this.getLangCodeAttrValue()));
                this.getConfirmPasswordIcon().setName("error");
                this.refreshContentOfUIComponent(this.getConfirmPasswordIcon());
                this.addErrorStyleToComponent(this.getConfirmPasswordMain());
                this.setFocusConfirmPassword();
                this.selectTextOnUIComponent(this.getConfirmPasswordMain());
                this.refreshContentOfUIComponent(this.getChangePasswordPanel());
                _logger.info("trKeyIn() End");
                return;
            }

            if (null != newPassword && !newPassword.isEmpty() && null != confirmPassword &&
                !confirmPassword.isEmpty() && this.verifyConfirmPassword(confirmPassword)) {
                if (this.callChangePassword(newPassword)) {
                    ADFUtils.setBoundAttributeValue(PASSWORD_CHANGED_ATTR, "Y");
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    ExternalContext ectx = fctx.getExternalContext();
                    HttpSession session = (HttpSession) ectx.getSession(true);
                    session.setAttribute("shouldChangePassword", "N");
                    invokeAction("goToMenu");
                } else {
                    //todo log sql error in here
                }
            }
        }
        _logger.info("trKeyIn() End");

    }

    private void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void showErrorPanel(String error) {
        _logger.info("showErrorPanel() Start");
        _logger.fine("Error: " + error);
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getErrorPanel().setVisible(false);
        this.getErrorMessage().setValue(error);
        this.getIconErrorMessage().setName("error");
        this.getErrorPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
        _logger.info("showErrorPanel() End");
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String inputId = clientEvent.getComponent().getId();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (StringUtils.isNotEmpty(submittedValue)) {
                    if ("newPass".equalsIgnoreCase(inputId)) {
                        this.onChangedNewPasswordMain(new ValueChangeEvent(this.getNewPasswordMain(), null,
                                                                           submittedValue));
                    } else if ("confirm".equalsIgnoreCase(inputId)) {
                        this.onChangedConfirmPasswordMain(new ValueChangeEvent(this.getConfirmPasswordMain(), null,
                                                                               submittedValue));

                    }
                }
            }
        }
        _logger.info("performKey() End");
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F4");
    }

    public void okLinkGotoPopupActionListener(ActionEvent actionEvent) {
        ADFUtils.setBoundAttributeValue(PASSWORD_CHANGED_ATTR, "");
        invokeAction("logOut");
    }

    public void confirmGoToDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            invokeAction("logOut");
        }
    }

    public static void invokeAction(String action) {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationHandler handler = context.getApplication().getNavigationHandler();
        handler.handleNavigation(context, "", action);
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private String getUserIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(USER_ID_ATTR);
    }

    private String getNewPasswordAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(NEW_PASSWORD);
    }

    private String getConfirmPasswordAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONFIRM_PASSWORD);
    }

    public void setChangePasswordPanel(RichPanelFormLayout changePasswordPanel) {
        this.changePasswordPanel = changePasswordPanel;
    }

    private void setFocusNewPassword() {
        this.getHhChangePasswordSBeanPageFlowBean().setIsFocusOn("newPasswordField");
        this.setFocusOnUIComponent(this.getNewPasswordMain());
    }

    private void setFocusConfirmPassword() {
        this.getHhChangePasswordSBeanPageFlowBean().setIsFocusOn("confirmPasswordField");
        this.setFocusOnUIComponent(this.getConfirmPasswordMain());
    }

    private void setFocusF3Link() {
        this.getHhChangePasswordSBeanPageFlowBean().setIsFocusOn("exitPopup");
        this.setFocusOnUIComponent(this.getF3Link());
    }

    private HhChangePasswordSBean getHhChangePasswordSBeanPageFlowBean() {
        return ((HhChangePasswordSBean) this.getPageFlowBean(HH_CHANGE_PASSWORD_FLOW_BEAN));
    }

    public RichPanelFormLayout getChangePasswordPanel() {
        return changePasswordPanel;
    }

    public void setNewPasswordMain(RichInputText newPasswordMain) {
        this.newPasswordMain = newPasswordMain;
    }

    public RichInputText getNewPasswordMain() {
        return newPasswordMain;
    }

    public void setConfirmPasswordMain(RichInputText confirmPasswordMain) {
        this.confirmPasswordMain = confirmPasswordMain;
    }

    public RichInputText getConfirmPasswordMain() {
        return confirmPasswordMain;
    }

    public void setPasswordAgeMain(RichInputText passwordAgeMain) {
        this.passwordAgeMain = passwordAgeMain;
    }

    public RichInputText getPasswordAgeMain() {
        return passwordAgeMain;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    public void setConfirmGoToDialog(RichDialog confirmGoToDialog) {
        this.confirmGoToDialog = confirmGoToDialog;
    }

    public RichDialog getConfirmGoToDialog() {
        return confirmGoToDialog;
    }

    public void setConfirmGoToOutputtext(RichOutputText confirmGoToOutputtext) {
        this.confirmGoToOutputtext = confirmGoToOutputtext;
    }

    public RichOutputText getConfirmGoToOutputtext() {
        return confirmGoToOutputtext;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setNewPasswordIcon(RichIcon newPasswordIcon) {
        this.newPasswordIcon = newPasswordIcon;
    }

    public RichIcon getNewPasswordIcon() {
        return newPasswordIcon;
    }

    public void setConfirmPasswordIcon(RichIcon confirmPasswordIcon) {
        this.confirmPasswordIcon = confirmPasswordIcon;
    }

    public RichIcon getConfirmPasswordIcon() {
        return confirmPasswordIcon;
    }

    public void onCpassRegionLoad(ComponentSystemEvent componentSystemEvent) {
        StringBuilder script = new StringBuilder("");
        script.append("initFocusCpass();");
        this.writeJavaScriptToClient(script.toString());
        this.refreshContentOfUIComponent(this.getNewPasswordMain());
    }
}
