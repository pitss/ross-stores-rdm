package com.ross.rdm.common.view;

import java.io.Serializable;

/**
 * Session-scope data of the currently logged-in user.
 * Roughly equivalent to the part of the :WORK block in the original Forms application.
 */
public class CurrentUser implements Serializable
{
    @SuppressWarnings("compatibility:-7715891987738204749")
    private static final long serialVersionUID = 1L;

    private String userId;
    private String facilityId;
    private String languageCode;
    private int userPrivilege;
    
    public CurrentUser() {
        super();
    }    

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setUserPrivilege(int userPrivilege) {
        this.userPrivilege = userPrivilege;
    }

    public int getUserPrivilege() {
        return userPrivilege;
    }    
}
