package com.ross.rdm.common.hhmenuss.model.views;

import com.ross.rdm.common.hhmenuss.model.views.common.HhMenusSMenuSView;
import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;

import java.math.BigDecimal;

import java.sql.ResultSet;

import oracle.jbo.Row;
import oracle.jbo.server.ViewRowImpl;
import oracle.jbo.server.ViewRowSetImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Sep 23 16:03:46 EDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhMenusSMenuSViewImpl extends RDMViewObjectImpl implements HhMenusSMenuSView {
    /**
     * This is the default constructor (do not remove).
     */
    public HhMenusSMenuSViewImpl() {
    }

    /**
     * Returns the bind variable value for bWorkFacilityId.
     * @return bind variable value for bWorkFacilityId
     */
    public String getbWorkFacilityId() {
        return (String) getNamedWhereClauseParam("bWorkFacilityId");
    }

    /**
     * Sets <code>value</code> for bind variable bWorkFacilityId.
     * @param value value to bind as bWorkFacilityId
     */
    public void setbWorkFacilityId(String value) {
        setNamedWhereClauseParam("bWorkFacilityId", value);
    }

    /**
     * Returns the bind variable value for bWorkUserPrivilege.
     * @return bind variable value for bWorkUserPrivilege
     */
    public BigDecimal getbWorkUserPrivilege() {
        return (BigDecimal) getNamedWhereClauseParam("bWorkUserPrivilege");
    }

    /**
     * Sets <code>value</code> for bind variable bWorkUserPrivilege.
     * @param value value to bind as bWorkUserPrivilege
     */
    public void setbWorkUserPrivilege(BigDecimal value) {
        setNamedWhereClauseParam("bWorkUserPrivilege", value);
    }

    /**
     * Returns the bind variable value for bWorkLocalMenuName.
     * @return bind variable value for bWorkLocalMenuName
     */
    public String getbWorkLocalMenuName() {
        return (String) getNamedWhereClauseParam("bWorkLocalMenuName");
    }

    /**
     * Sets <code>value</code> for bind variable bWorkLocalMenuName.
     * @param value value to bind as bWorkLocalMenuName
     */
    public void setbWorkLocalMenuName(String value) {
        setNamedWhereClauseParam("bWorkLocalMenuName", value);
    }

    @Override
    protected void create() {
        // TODO Implement this method
        super.create();
        this.setManageRowsByKey(true);
    }

    /**
     * executeQueryForCollection - overridden for custom java data source support.
     */
    @Override
    protected void executeQueryForCollection(Object qc, Object[] params, int noUserParams) {
        super.executeQueryForCollection(qc, params, noUserParams);
        this.setbWorkLocalMenuName(null);
    }

    /**
     * hasNextForCollection - overridden for custom java data source support.
     */
    @Override
    protected boolean hasNextForCollection(Object qc) {
        boolean bRet = super.hasNextForCollection(qc);
        return bRet;
    }

    /**
     * createRowFromResultSet - overridden for custom java data source support.
     */
    @Override
    protected ViewRowImpl createRowFromResultSet(Object qc, ResultSet resultSet) {
        ViewRowImpl value = super.createRowFromResultSet(qc, resultSet);
        return value;
    }

    /**
     * getQueryHitCount - overridden for custom java data source support.
     */
    @Override
    public long getQueryHitCount(ViewRowSetImpl viewRowSet) {
        long value = super.getQueryHitCount(viewRowSet);
        return value;
    }

    /**
     * getCappedQueryHitCount - overridden for custom java data source support.
     */
    @Override
    public long getCappedQueryHitCount(ViewRowSetImpl viewRowSet, Row[] masterRows, long oldCap, long cap) {
        long value = super.getCappedQueryHitCount(viewRowSet, masterRows, oldCap, cap);
        return value;
    }

    public void setDefaultCurrentRow() {
        if (this.first() != null) {
            this.setCurrentRow(this.first());
        }
    }

    /**
     * Returns the bind variable value for bLanguage.
     * @return bind variable value for bLanguage
     */
    public String getbLanguage() {
        return (String) getNamedWhereClauseParam("bLanguage");
    }

    /**
     * Sets <code>value</code> for bind variable bLanguage.
     * @param value value to bind as bLanguage
     */
    public void setbLanguage(String value) {
        setNamedWhereClauseParam("bLanguage", value);
    }
}

