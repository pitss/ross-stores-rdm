package com.ross.rdm.common.hhmenuss.model.views;

import com.ross.rdm.common.utils.model.base.RDMEntityImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jan 05 13:02:59 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhMenusSMenuSViewRowImpl extends RDMViewRowImpl {


    public static final int ENTITY_DMSMENU = 0;
    public static final int ENTITY_DMSLANGUAGEMENU = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        OptionTitle {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getOptionTitle();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setOptionTitle((String) value);
            }
        }
        ,
        MenuTitle {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getMenuTitle();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setMenuTitle((String) value);
            }
        }
        ,
        MenuName {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getMenuName();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setMenuName((String) value);
            }
        }
        ,
        OptionNumber {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getOptionNumber();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setOptionNumber((Integer) value);
            }
        }
        ,
        OptionCommand {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getOptionCommand();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setOptionCommand((String) value);
            }
        }
        ,
        UserPrivilege {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getUserPrivilege();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setUserPrivilege((Integer) value);
            }
        }
        ,
        RecordType {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getRecordType();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setRecordType((String) value);
            }
        }
        ,
        FacilityId {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getFacilityId();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setFacilityId((String) value);
            }
        }
        ,
        rowKey {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getrowKey();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setrowKey((String) value);
            }
        }
        ,
        TaskFlowUrl {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getTaskFlowUrl();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setTaskFlowUrl((String) value);
            }
        }
        ,
        OptionText {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getOptionText();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setOptionText((String) value);
            }
        }
        ,
        FacilityId1 {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getFacilityId1();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setFacilityId1((String) value);
            }
        }
        ,
        MenuName1 {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getMenuName1();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setMenuName1((String) value);
            }
        }
        ,
        OptionTitle1 {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getOptionTitle1();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setOptionTitle1((String) value);
            }
        }
        ,
        LanguageCode {
            public Object get(HhMenusSMenuSViewRowImpl obj) {
                return obj.getLanguageCode();
            }

            public void put(HhMenusSMenuSViewRowImpl obj, Object value) {
                obj.setLanguageCode((String) value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(HhMenusSMenuSViewRowImpl object);

        public abstract void put(HhMenusSMenuSViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int OPTIONTITLE = AttributesEnum.OptionTitle.index();
    public static final int MENUTITLE = AttributesEnum.MenuTitle.index();
    public static final int MENUNAME = AttributesEnum.MenuName.index();
    public static final int OPTIONNUMBER = AttributesEnum.OptionNumber.index();
    public static final int OPTIONCOMMAND = AttributesEnum.OptionCommand.index();
    public static final int USERPRIVILEGE = AttributesEnum.UserPrivilege.index();
    public static final int RECORDTYPE = AttributesEnum.RecordType.index();
    public static final int FACILITYID = AttributesEnum.FacilityId.index();
    public static final int ROWKEY = AttributesEnum.rowKey.index();
    public static final int TASKFLOWURL = AttributesEnum.TaskFlowUrl.index();
    public static final int OPTIONTEXT = AttributesEnum.OptionText.index();
    public static final int FACILITYID1 = AttributesEnum.FacilityId1.index();
    public static final int MENUNAME1 = AttributesEnum.MenuName1.index();
    public static final int OPTIONTITLE1 = AttributesEnum.OptionTitle1.index();
    public static final int LANGUAGECODE = AttributesEnum.LanguageCode.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhMenusSMenuSViewRowImpl() {
    }

    /**
     * Gets DmsMenu entity object.
     * @return the DmsMenu
     */
    public RDMEntityImpl getDmsMenu() {
        return (RDMEntityImpl) getEntity(ENTITY_DMSMENU);
    }

    /**
     * Gets DmsLanguageMenu entity object.
     * @return the DmsLanguageMenu
     */
    public RDMEntityImpl getDmsLanguageMenu() {
        return (RDMEntityImpl) getEntity(ENTITY_DMSLANGUAGEMENU);
    }

    /**
     * Gets the attribute value for OPTION_TITLE using the alias name OptionTitle.
     * @return the OPTION_TITLE
     */
    public String getOptionTitle() {
        return (String) getAttributeInternal(OPTIONTITLE);
    }

    /**
     * Sets <code>value</code> as attribute value for OPTION_TITLE using the alias name OptionTitle.
     * @param value value to set the OPTION_TITLE
     */
    public void setOptionTitle(String value) {
        setAttributeInternal(OPTIONTITLE, value);
    }

    /**
     * Gets the attribute value for MENU_TITLE using the alias name MenuTitle.
     * @return the MENU_TITLE
     */
    public String getMenuTitle() {
        return (String) getAttributeInternal(MENUTITLE);
    }

    /**
     * Sets <code>value</code> as attribute value for MENU_TITLE using the alias name MenuTitle.
     * @param value value to set the MENU_TITLE
     */
    public void setMenuTitle(String value) {
        setAttributeInternal(MENUTITLE, value);
    }

    /**
     * Gets the attribute value for MENU_NAME using the alias name MenuName.
     * @return the MENU_NAME
     */
    public String getMenuName() {
        return (String) getAttributeInternal(MENUNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for MENU_NAME using the alias name MenuName.
     * @param value value to set the MENU_NAME
     */
    public void setMenuName(String value) {
        setAttributeInternal(MENUNAME, value);
    }

    /**
     * Gets the attribute value for OPTION_NUMBER using the alias name OptionNumber.
     * @return the OPTION_NUMBER
     */
    public Integer getOptionNumber() {
        return (Integer) getAttributeInternal(OPTIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OPTION_NUMBER using the alias name OptionNumber.
     * @param value value to set the OPTION_NUMBER
     */
    public void setOptionNumber(Integer value) {
        setAttributeInternal(OPTIONNUMBER, value);
    }

    /**
     * Gets the attribute value for OPTION_COMMAND using the alias name OptionCommand.
     * @return the OPTION_COMMAND
     */
    public String getOptionCommand() {
        return (String) getAttributeInternal(OPTIONCOMMAND);
    }

    /**
     * Sets <code>value</code> as attribute value for OPTION_COMMAND using the alias name OptionCommand.
     * @param value value to set the OPTION_COMMAND
     */
    public void setOptionCommand(String value) {
        setAttributeInternal(OPTIONCOMMAND, value);
    }

    /**
     * Gets the attribute value for USER_PRIVILEGE using the alias name UserPrivilege.
     * @return the USER_PRIVILEGE
     */
    public Integer getUserPrivilege() {
        return (Integer) getAttributeInternal(USERPRIVILEGE);
    }

    /**
     * Sets <code>value</code> as attribute value for USER_PRIVILEGE using the alias name UserPrivilege.
     * @param value value to set the USER_PRIVILEGE
     */
    public void setUserPrivilege(Integer value) {
        setAttributeInternal(USERPRIVILEGE, value);
    }

    /**
     * Gets the attribute value for RECORD_TYPE using the alias name RecordType.
     * @return the RECORD_TYPE
     */
    public String getRecordType() {
        return (String) getAttributeInternal(RECORDTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for RECORD_TYPE using the alias name RecordType.
     * @param value value to set the RECORD_TYPE
     */
    public void setRecordType(String value) {
        setAttributeInternal(RECORDTYPE, value);
    }

    /**
     * Gets the attribute value for FACILITY_ID using the alias name FacilityId.
     * @return the FACILITY_ID
     */
    public String getFacilityId() {
        return (String) getAttributeInternal(FACILITYID);
    }

    /**
     * Sets <code>value</code> as attribute value for FACILITY_ID using the alias name FacilityId.
     * @param value value to set the FACILITY_ID
     */
    public void setFacilityId(String value) {
        setAttributeInternal(FACILITYID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute rowKey.
     * @return the rowKey
     */
    public String getrowKey() {
        return (String) getAttributeInternal(ROWKEY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute rowKey.
     * @param value value to set the  rowKey
     */
    public void setrowKey(String value) {
        setAttributeInternal(ROWKEY, value);
    }

    /**
     * Gets the attribute value for TASK_FLOW_URL using the alias name TaskFlowUrl.
     * @return the TASK_FLOW_URL
     */
    public String getTaskFlowUrl() {
        return (String) getAttributeInternal(TASKFLOWURL);
    }

    /**
     * Sets <code>value</code> as attribute value for TASK_FLOW_URL using the alias name TaskFlowUrl.
     * @param value value to set the TASK_FLOW_URL
     */
    public void setTaskFlowUrl(String value) {
        setAttributeInternal(TASKFLOWURL, value);
    }

    /**
     * Gets the attribute value for OPTION_TEXT using the alias name OptionText.
     * @return the OPTION_TEXT
     */
    public String getOptionText() {
        return (String) getAttributeInternal(OPTIONTEXT);
    }

    /**
     * Sets <code>value</code> as attribute value for OPTION_TEXT using the alias name OptionText.
     * @param value value to set the OPTION_TEXT
     */
    public void setOptionText(String value) {
        setAttributeInternal(OPTIONTEXT, value);
    }

    /**
     * Gets the attribute value for FACILITY_ID using the alias name FacilityId1.
     * @return the FACILITY_ID
     */
    public String getFacilityId1() {
        return (String) getAttributeInternal(FACILITYID1);
    }

    /**
     * Sets <code>value</code> as attribute value for FACILITY_ID using the alias name FacilityId1.
     * @param value value to set the FACILITY_ID
     */
    public void setFacilityId1(String value) {
        setAttributeInternal(FACILITYID1, value);
    }

    /**
     * Gets the attribute value for MENU_NAME using the alias name MenuName1.
     * @return the MENU_NAME
     */
    public String getMenuName1() {
        return (String) getAttributeInternal(MENUNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for MENU_NAME using the alias name MenuName1.
     * @param value value to set the MENU_NAME
     */
    public void setMenuName1(String value) {
        setAttributeInternal(MENUNAME1, value);
    }

    /**
     * Gets the attribute value for OPTION_TITLE using the alias name OptionTitle1.
     * @return the OPTION_TITLE
     */
    public String getOptionTitle1() {
        return (String) getAttributeInternal(OPTIONTITLE1);
    }

    /**
     * Sets <code>value</code> as attribute value for OPTION_TITLE using the alias name OptionTitle1.
     * @param value value to set the OPTION_TITLE
     */
    public void setOptionTitle1(String value) {
        setAttributeInternal(OPTIONTITLE1, value);
    }

    /**
     * Gets the attribute value for LANGUAGE_CODE using the alias name LanguageCode.
     * @return the LANGUAGE_CODE
     */
    public String getLanguageCode() {
        return (String) getAttributeInternal(LANGUAGECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for LANGUAGE_CODE using the alias name LanguageCode.
     * @param value value to set the LANGUAGE_CODE
     */
    public void setLanguageCode(String value) {
        setAttributeInternal(LANGUAGECODE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

