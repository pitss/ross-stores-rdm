package com.ross.rdm.common.reports;

import java.io.Serializable;

public class ReportConfig  implements Serializable {
    public ReportConfig() {
        super();
    }


    @SuppressWarnings("compatibility:1236205624317974783")
    static final long serialVersionUID = 12345678;
        
    private String reportServerHost;
    private String reportServerPort;
    private String reportServerName;
    private String desType;
    private String desName;
    private String desFormat;
    private String orientation;
    private String pageSize;
    private String reportName;
    private String connectString;
    private String reportFile;
    private String keyMap;
    private Integer timeOut;

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getReportFile() {
        return reportFile;
    }


    public void setReportServerHost(String reportServerHost) {
        this.reportServerHost = reportServerHost;
    }

    public String getReportServerHost() {
        return reportServerHost;
    }

    public void setReportServerPort(String reportServerPort) {
        this.reportServerPort = reportServerPort;
    }

    public String getReportServerPort() {
        return reportServerPort;
    }

    public void setReportServerName(String reportServerName) {
        this.reportServerName = reportServerName;
    }

    public String getReportServerName() {
        return reportServerName;
    }

    public void setDesType(String desType) {
        this.desType = desType;
    }

    public String getDesType() {
        return desType;
    }

    public void setDesName(String desName) {
        this.desName = desName;
    }

    public String getDesName() {
        return desName;
    }


    public void setDesFormat(String desFormat) {
        this.desFormat = desFormat;
    }

    public String getDesFormat() {
        return desFormat;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }

    public void setConnectString(String connectString) {
        this.connectString = connectString;
    }

    public String getConnectString() {
        return connectString;
    }


    public void setKeyMap(String keyMap) {
        this.keyMap = keyMap;
    }

    public String getKeyMap() {
        return keyMap;
    }
}


