package com.ross.rdm.common.reports.model;


import com.ross.rdm.common.reports.ReportConfig;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;

import java.io.IOException;

import java.math.BigDecimal;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.sql.Types;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.net.ssl.SSLContext;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.server.DBTransaction;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.conn.NoopIOSessionStrategy;
import org.apache.http.nio.conn.SchemeIOSessionStrategy;
import org.apache.http.nio.conn.ssl.SSLIOSessionStrategy;

public class RunOracleReport {
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(RunOracleReport.class);
    
    public RunOracleReport() {
        super();        
        this.reportConfig = new ReportConfig();
    }
    private ReportConfig reportConfig;  
    
    private OracleReportURLBuilder oracleURLBuilder;
    
    private String status;
    private String message;
    
    public void setReportConfig(ReportConfig reportConfig) {
        this.reportConfig = reportConfig;
    }

    public ReportConfig getReportConfig() {
        return reportConfig;
    }

    public void setOracleURLBuilder(OracleReportURLBuilder oracleURLBuilder) {
        this.oracleURLBuilder = oracleURLBuilder;
    }

    public OracleReportURLBuilder getOracleURLBuilder() {
        return oracleURLBuilder;
    }


    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void initalize(DBTransaction dbTransaction, String facilityId, String language, String reportName) {

        String clientType = "";
        SQLOutParam hostName = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam port = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam timeOut = new SQLOutParam(new BigDecimal(0), Types.NUMERIC);
        SQLOutParam reportServer = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam keyName = new SQLOutParam("", Types.VARCHAR);        
        SQLOutParam desType = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam desName = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam desFormat = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam orientation = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam pageSize = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam reportFile = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam reportType = new SQLOutParam("", Types.VARCHAR);
        
        DBUtils.callStoredProcedure(dbTransaction, "PKG_REPORTS_ADF.INITIALIZE", facilityId, language,
                                    reportName, clientType, hostName, port, timeOut, reportServer, keyName,
                                    desType, desName, desFormat, orientation, pageSize,
                                    reportFile, reportType);
        
        reportConfig.setReportServerHost((String) hostName.getWrappedData());
        reportConfig.setReportServerPort((String) port.getWrappedData());
        BigDecimal _timeOut = (BigDecimal) timeOut.getWrappedData();
        reportConfig.setTimeOut(_timeOut.intValue());
        reportConfig.setReportServerName((String) reportServer.getWrappedData());
        reportConfig.setDesType((String) desType.getWrappedData());
        reportConfig.setDesName((String) desName.getWrappedData());
        reportConfig.setDesFormat((String) desFormat.getWrappedData());
        reportConfig.setOrientation((String) orientation.getWrappedData());
        reportConfig.setPageSize((String) pageSize.getWrappedData());
        reportConfig.setReportFile((String)reportFile.getWrappedData());
        reportConfig.setKeyMap((String) keyName.getWrappedData());

        oracleURLBuilder = new OracleReportURLBuilder(this.reportConfig.getReportServerHost(),this.reportConfig.getReportServerPort(), null);  // host, port
        oracleURLBuilder.setReportParameter("p_facility_id",facilityId);
        oracleURLBuilder.setReportParameter("p_language",language);               
       
    }


    public String getReportURL() {
        //String targetURL = "http://192.168.100.54:9022/reports/rwservlet?server=RptSvr_ScottPrinter&report=reptest&destype=printer&desname=HP%20LaserJet%20M2727%20MFP%20Series%20PCL%206&userid=webutil/webutil@pitss";
        //The params will be invoked here in case they got overridden by the caller
        oracleURLBuilder.setKeyMap(this.reportConfig.getKeyMap());
        oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_SERVER,this.reportConfig.getReportServerName());
        oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_DESTYPE, this.reportConfig.getDesType());
        oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_DESNAME, this.reportConfig.getDesName());
        if (reportConfig.getDesFormat() != null)
           oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_DESFORMAT, this.reportConfig.getDesFormat());
        if (reportConfig.getPageSize() != null)
           oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_PAGESIZE, this.reportConfig.getPageSize());
        if (reportConfig.getOrientation() != null)
           oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_ORIENTATION, this.reportConfig.getOrientation());
        oracleURLBuilder.setReportServerParam(OracleReportURLBuilder.RS_PARAM_REPORT, this.reportConfig.getReportFile());
        
        String reportServerURL = oracleURLBuilder.getReportServerURL();
        System.out.println("Report URL is " + reportServerURL); /*TODO: remove*/
        _logger.info("Report URL is " + reportServerURL);
        
        return reportServerURL;
        
    }


    public void sendReportJobAsyc() {
        ExecutorService es = Executors.newFixedThreadPool(3);
        final Future future = es.submit(
            new Callable() {
                public Object call() throws Exception {
                    new RunOracleReport().sendReportJob();
                    return null;
                }            
            }                                       
        );
    }


    
    public void sendReportJob(){
        //System.out.println("Start sendReportJob: " + new java.util.Date() ); /*TODO: remove*/
        _logger.info("Start sendReportJob: " + new java.util.Date());
        
        String reportServerURL = this.getReportURL();
     
        /* Build and send Reports Job - on backend to reports server 
         * wait for response */
        try { //TODO: comment block in
            //build http request - reports job
            
            SSLContext sslcontext = SSLContexts.custom()
                    .useTLS()
                    .loadTrustMaterial(null, new TrustStrategy()
                    {
                        @Override
                        public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException
                        {
                            return true;
                        }
                    })
                    .build();
            SSLIOSessionStrategy sslSessionStrategy = new SSLIOSessionStrategy(sslcontext, new AllowAllHostnameVerifier());

            Registry<SchemeIOSessionStrategy> sessionStrategyRegistry = RegistryBuilder.<SchemeIOSessionStrategy>create()
                    .register("http", NoopIOSessionStrategy.INSTANCE)
                    .register("https", sslSessionStrategy)
                    .build();
            
            
            RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(reportConfig.getTimeOut())
                .setConnectTimeout(reportConfig.getTimeOut()).build();
          final     CloseableHttpAsyncClient client  = HttpAsyncClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setSSLStrategy(sslSessionStrategy)
                .build();            
            
            //CloseableHttpAsyncClient client = HttpAsyncClients.createDefault(); 
           client.start();
            final HttpGet request = new HttpGet(reportServerURL);
            
            //send reports job, get response
             client.execute(request, new FutureCallback<HttpResponse>() {

                    @Override
                    public void completed(final HttpResponse response) {
                    try {
                          client.close();
                    } catch (IOException e) {
                        _logger.severe(e.getMessage());
                    }

                }
                    @Override
                    public void failed(final Exception ex) {
                  
                        try {
                           
                            client.close();
                        } catch (IOException e) {
                            _logger.severe(e.getMessage());
                        }
                    }

                    @Override
                    public void cancelled() {
                        try {
                            client.close();
                        } catch (IOException e) {
                            _logger.severe(e.getMessage());
                        }
                       
                    }

                });

            
           // COmmented blocking code
         // HttpResponse response = future.get();
            //System.out.println("Response is: "+ response.getStatusLine() + " " + new java.util.Date()); //TODO: remove
//            _logger.info("Response is: "+ response.getStatusLine() + " " + new java.util.Date());
//
//            if (response.getStatusLine().getStatusCode() == 200) {
//                String responseString = new BasicResponseHandler().handleResponse(response);
//                int int1 = responseString.indexOf("REP-"); // assuming server does not send null response
//                if (int1 != -1) {
//                    message = responseString.substring(int1, responseString.length());
//                    message = message.substring(0, message.indexOf("</"));
//                    status = "E";
//                    _logger.severe(message);
//                    //System.out.println(message);
//                } else {
//                    message = "Report processed successfully";
//                    status = "I";   }             
//            } else
//            {  message = "HTTP Error " + response.getStatusLine();
//               status = "E";
//               _logger.severe(message);
//               //System.out.println(message); 
//            }

         //   client.close();
            
        } catch (Exception e) {
           // e.printStackTrace();  
            _logger.severe(e.getMessage());
            message = e.getMessage();
            status = "E";            
        }
    }    
    
}

