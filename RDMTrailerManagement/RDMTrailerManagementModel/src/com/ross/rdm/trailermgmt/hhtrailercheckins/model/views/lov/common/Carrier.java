package com.ross.rdm.trailermgmt.hhtrailercheckins.model.views.lov.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Feb 01 09:47:27 CET 2016
// ---------------------------------------------------------------------
public interface Carrier extends ViewObject {
    boolean codeExists(String facilityId, String carrierCode);

    void setbCarrierCode(String value);

    void setbFacilityId(String value);
}

