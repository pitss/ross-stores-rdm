package com.ross.rdm.trailermgmt.hhtrailercheckins.model.views;

import com.ross.rdm.common.utils.model.base.RDMEntityImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.sql.Date;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Feb 09 08:53:31 CET 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AppointmentRowImpl extends RDMViewRowImpl {

    public static final int ENTITY_APPOINTMENT = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ApptNbr {
            public Object get(AppointmentRowImpl obj) {
                return obj.getApptNbr();
            }

            public void put(AppointmentRowImpl obj, Object value) {
                obj.setApptNbr((Integer) value);
            }
        }
        ,
        FacilityId {
            public Object get(AppointmentRowImpl obj) {
                return obj.getFacilityId();
            }

            public void put(AppointmentRowImpl obj, Object value) {
                obj.setFacilityId((String) value);
            }
        }
        ,
        TrailerId {
            public Object get(AppointmentRowImpl obj) {
                return obj.getTrailerId();
            }

            public void put(AppointmentRowImpl obj, Object value) {
                obj.setTrailerId((String) value);
            }
        }
        ,
        CarrierCode {
            public Object get(AppointmentRowImpl obj) {
                return obj.getCarrierCode();
            }

            public void put(AppointmentRowImpl obj, Object value) {
                obj.setCarrierCode((String) value);
            }
        }
        ,
        ApptStatus {
            public Object get(AppointmentRowImpl obj) {
                return obj.getApptStatus();
            }

            public void put(AppointmentRowImpl obj, Object value) {
                obj.setApptStatus((String) value);
            }
        }
        ,
        ApptStartTs {
            public Object get(AppointmentRowImpl obj) {
                return obj.getApptStartTs();
            }

            public void put(AppointmentRowImpl obj, Object value) {
                obj.setApptStartTs((Date) value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(AppointmentRowImpl object);

        public abstract void put(AppointmentRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int APPTNBR = AttributesEnum.ApptNbr.index();
    public static final int FACILITYID = AttributesEnum.FacilityId.index();
    public static final int TRAILERID = AttributesEnum.TrailerId.index();
    public static final int CARRIERCODE = AttributesEnum.CarrierCode.index();
    public static final int APPTSTATUS = AttributesEnum.ApptStatus.index();
    public static final int APPTSTARTTS = AttributesEnum.ApptStartTs.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AppointmentRowImpl() {
    }

    /**
     * Gets Appointment entity object.
     * @return the Appointment
     */
    public RDMEntityImpl getAppointment() {
        return (RDMEntityImpl) getEntity(ENTITY_APPOINTMENT);
    }

    /**
     * Gets the attribute value for APPT_NBR using the alias name ApptNbr.
     * @return the APPT_NBR
     */
    public Integer getApptNbr() {
        return (Integer) getAttributeInternal(APPTNBR);
    }

    /**
     * Sets <code>value</code> as attribute value for APPT_NBR using the alias name ApptNbr.
     * @param value value to set the APPT_NBR
     */
    public void setApptNbr(Integer value) {
        setAttributeInternal(APPTNBR, value);
    }

    /**
     * Gets the attribute value for FACILITY_ID using the alias name FacilityId.
     * @return the FACILITY_ID
     */
    public String getFacilityId() {
        return (String) getAttributeInternal(FACILITYID);
    }

    /**
     * Sets <code>value</code> as attribute value for FACILITY_ID using the alias name FacilityId.
     * @param value value to set the FACILITY_ID
     */
    public void setFacilityId(String value) {
        setAttributeInternal(FACILITYID, value);
    }

    /**
     * Gets the attribute value for TRAILER_ID using the alias name TrailerId.
     * @return the TRAILER_ID
     */
    public String getTrailerId() {
        return (String) getAttributeInternal(TRAILERID);
    }

    /**
     * Sets <code>value</code> as attribute value for TRAILER_ID using the alias name TrailerId.
     * @param value value to set the TRAILER_ID
     */
    public void setTrailerId(String value) {
        setAttributeInternal(TRAILERID, value);
    }

    /**
     * Gets the attribute value for CARRIER_CODE using the alias name CarrierCode.
     * @return the CARRIER_CODE
     */
    public String getCarrierCode() {
        return (String) getAttributeInternal(CARRIERCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CARRIER_CODE using the alias name CarrierCode.
     * @param value value to set the CARRIER_CODE
     */
    public void setCarrierCode(String value) {
        setAttributeInternal(CARRIERCODE, value);
    }

    /**
     * Gets the attribute value for APPT_STATUS using the alias name ApptStatus.
     * @return the APPT_STATUS
     */
    public String getApptStatus() {
        return (String) getAttributeInternal(APPTSTATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for APPT_STATUS using the alias name ApptStatus.
     * @param value value to set the APPT_STATUS
     */
    public void setApptStatus(String value) {
        setAttributeInternal(APPTSTATUS, value);
    }

    /**
     * Gets the attribute value for APPT_START_TS using the alias name ApptStartTs.
     * @return the APPT_START_TS
     */
    public Date getApptStartTs() {
        return (Date) getAttributeInternal(APPTSTARTTS);
    }

    /**
     * Sets <code>value</code> as attribute value for APPT_START_TS using the alias name ApptStartTs.
     * @param value value to set the APPT_START_TS
     */
    public void setApptStartTs(Date value) {
        setAttributeInternal(APPTSTARTTS, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

