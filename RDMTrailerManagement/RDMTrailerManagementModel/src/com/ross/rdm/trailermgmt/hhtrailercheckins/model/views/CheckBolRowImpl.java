package com.ross.rdm.trailermgmt.hhtrailercheckins.model.views;

import com.ross.rdm.common.utils.model.base.RDMEntityImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 10 10:42:52 CET 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CheckBolRowImpl extends RDMViewRowImpl {

    public static final int ENTITY_TMSLOADSTATUS = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ShipmentId {
            public Object get(CheckBolRowImpl obj) {
                return obj.getShipmentId();
            }

            public void put(CheckBolRowImpl obj, Object value) {
                obj.setShipmentId((String) value);
            }
        }
        ,
        PoNbr {
            public Object get(CheckBolRowImpl obj) {
                return obj.getPoNbr();
            }

            public void put(CheckBolRowImpl obj, Object value) {
                obj.setPoNbr((String) value);
            }
        }
        ,
        Selected {
            public Object get(CheckBolRowImpl obj) {
                return obj.getSelected();
            }

            public void put(CheckBolRowImpl obj, Object value) {
                obj.setSelected((String) value);
            }
        }
        ,
        CarrierCode {
            public Object get(CheckBolRowImpl obj) {
                return obj.getCarrierCode();
            }

            public void put(CheckBolRowImpl obj, Object value) {
                obj.setCarrierCode((String) value);
            }
        }
        ,
        RossBol {
            public Object get(CheckBolRowImpl obj) {
                return obj.getRossBol();
            }

            public void put(CheckBolRowImpl obj, Object value) {
                obj.setRossBol((String) value);
            }
        }
        ,
        TrailerId {
            public Object get(CheckBolRowImpl obj) {
                return obj.getTrailerId();
            }

            public void put(CheckBolRowImpl obj, Object value) {
                obj.setTrailerId((String) value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(CheckBolRowImpl object);

        public abstract void put(CheckBolRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int SHIPMENTID = AttributesEnum.ShipmentId.index();
    public static final int PONBR = AttributesEnum.PoNbr.index();
    public static final int SELECTED = AttributesEnum.Selected.index();
    public static final int CARRIERCODE = AttributesEnum.CarrierCode.index();
    public static final int ROSSBOL = AttributesEnum.RossBol.index();
    public static final int TRAILERID = AttributesEnum.TrailerId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CheckBolRowImpl() {
    }

    /**
     * Gets TmsLoadStatus entity object.
     * @return the TmsLoadStatus
     */
    public RDMEntityImpl getTmsLoadStatus() {
        return (RDMEntityImpl) getEntity(ENTITY_TMSLOADSTATUS);
    }

    /**
     * Gets the attribute value for SHIPMENT_ID using the alias name ShipmentId.
     * @return the SHIPMENT_ID
     */
    public String getShipmentId() {
        return (String) getAttributeInternal(SHIPMENTID);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIPMENT_ID using the alias name ShipmentId.
     * @param value value to set the SHIPMENT_ID
     */
    public void setShipmentId(String value) {
        setAttributeInternal(SHIPMENTID, value);
    }

    /**
     * Gets the attribute value for PO_NBR using the alias name PoNbr.
     * @return the PO_NBR
     */
    public String getPoNbr() {
        return (String) getAttributeInternal(PONBR);
    }

    /**
     * Sets <code>value</code> as attribute value for PO_NBR using the alias name PoNbr.
     * @param value value to set the PO_NBR
     */
    public void setPoNbr(String value) {
        setAttributeInternal(PONBR, value);
    }

    /**
     * Gets the attribute value for SELECTED using the alias name Selected.
     * @return the SELECTED
     */
    public String getSelected() {
        return (String) getAttributeInternal(SELECTED);
    }

    /**
     * Sets <code>value</code> as attribute value for SELECTED using the alias name Selected.
     * @param value value to set the SELECTED
     */
    public void setSelected(String value) {
        setAttributeInternal(SELECTED, value);
    }

    /**
     * Gets the attribute value for CARRIER_CODE using the alias name CarrierCode.
     * @return the CARRIER_CODE
     */
    public String getCarrierCode() {
        return (String) getAttributeInternal(CARRIERCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CARRIER_CODE using the alias name CarrierCode.
     * @param value value to set the CARRIER_CODE
     */
    public void setCarrierCode(String value) {
        setAttributeInternal(CARRIERCODE, value);
    }

    /**
     * Gets the attribute value for ROSS_BOL using the alias name RossBol.
     * @return the ROSS_BOL
     */
    public String getRossBol() {
        return (String) getAttributeInternal(ROSSBOL);
    }

    /**
     * Sets <code>value</code> as attribute value for ROSS_BOL using the alias name RossBol.
     * @param value value to set the ROSS_BOL
     */
    public void setRossBol(String value) {
        setAttributeInternal(ROSSBOL, value);
    }

    /**
     * Gets the attribute value for TRAILER_ID using the alias name TrailerId.
     * @return the TRAILER_ID
     */
    public String getTrailerId() {
        return (String) getAttributeInternal(TRAILERID);
    }

    /**
     * Sets <code>value</code> as attribute value for TRAILER_ID using the alias name TrailerId.
     * @param value value to set the TRAILER_ID
     */
    public void setTrailerId(String value) {
        setAttributeInternal(TRAILERID, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

