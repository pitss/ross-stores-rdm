package com.ross.rdm.trailermgmt.hhresettrailers.model.views.common;

import java.util.Map;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Feb 25 16:00:22 PST 2016
// ---------------------------------------------------------------------
public interface HhResetTrailerSResetTrailerView extends ViewObject {
    void addRecord();

    Map validateDoorId();

    Map validateRoute();

    Map validateServiceCode();

    Map validateTrailer();

    Map resetTrailer();
}

