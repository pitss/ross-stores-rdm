package com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.common;

import java.util.List;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Apr 25 14:56:45 EDT 2016
// ---------------------------------------------------------------------
public interface HhTrailerInquirySTrailerInquiryView extends ViewObject {

    List<String> callCheckTrailerInquiry(String pFacilityId, String pTrailerId, String pCursorBlock);


    List<String> callTrailerIdValidation(String pTrailerId, String pFacilityId);
}

