package com.ross.rdm.trailermgmt.model.utils;

import java.sql.Date;


public class ModelUtils {
    public ModelUtils() {
        super();
    }
    
    /**
     * Helper function to check if the given value equals to one of the comparators.
     * Similar in functionality to the PL/SQL IN function.
     */
    public static boolean in(String value, String... comparators) {
        for (String comparator : comparators) {
            if (comparator.equals(value))
                return true;
        }
        return false;
    }
    
    /**
     * Helper function to get the current date
     * (ugly conversion business)
     */
    public static Date getCurrentDate() {
        return new Date((new java.util.Date()).getTime());
    }
}
