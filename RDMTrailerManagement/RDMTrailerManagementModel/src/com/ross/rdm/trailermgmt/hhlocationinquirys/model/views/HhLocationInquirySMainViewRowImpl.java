package com.ross.rdm.trailermgmt.hhlocationinquirys.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.sql.Date;
import java.sql.Timestamp;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Feb 19 14:54:58 PST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhLocationInquirySMainViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        LocationId {
            public Object get(HhLocationInquirySMainViewRowImpl obj) {
                return obj.getLocationId();
            }

            public void put(HhLocationInquirySMainViewRowImpl obj, Object value) {
                obj.setLocationId((String) value);
            }
        }
        ,
        TrailerId {
            public Object get(HhLocationInquirySMainViewRowImpl obj) {
                return obj.getTrailerId();
            }

            public void put(HhLocationInquirySMainViewRowImpl obj, Object value) {
                obj.setTrailerId((String) value);
            }
        }
        ,
        TrailerStatus {
            public Object get(HhLocationInquirySMainViewRowImpl obj) {
                return obj.getTrailerStatus();
            }

            public void put(HhLocationInquirySMainViewRowImpl obj, Object value) {
                obj.setTrailerStatus((String) value);
            }
        }
        ,
        LastUseDate {
            public Object get(HhLocationInquirySMainViewRowImpl obj) {
                return obj.getLastUseDate();
            }

            public void put(HhLocationInquirySMainViewRowImpl obj, Object value) {
                obj.setLastUseDate((String) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhLocationInquirySMainViewRowImpl object);

        public abstract void put(HhLocationInquirySMainViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int LOCATIONID = AttributesEnum.LocationId.index();
    public static final int TRAILERID = AttributesEnum.TrailerId.index();
    public static final int TRAILERSTATUS = AttributesEnum.TrailerStatus.index();
    public static final int LASTUSEDATE = AttributesEnum.LastUseDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhLocationInquirySMainViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute LocationId.
     * @return the LocationId
     */
    public String getLocationId() {
        return (String) getAttributeInternal(LOCATIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LocationId.
     * @param value value to set the  LocationId
     */
    public void setLocationId(String value) {
        setAttributeInternal(LOCATIONID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TrailerId.
     * @return the TrailerId
     */
    public String getTrailerId() {
        return (String) getAttributeInternal(TRAILERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TrailerId.
     * @param value value to set the  TrailerId
     */
    public void setTrailerId(String value) {
        setAttributeInternal(TRAILERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TrailerStatus.
     * @return the TrailerStatus
     */
    public String getTrailerStatus() {
        return (String) getAttributeInternal(TRAILERSTATUS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TrailerStatus.
     * @param value value to set the  TrailerStatus
     */
    public void setTrailerStatus(String value) {
        setAttributeInternal(TRAILERSTATUS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute LastUseDate.
     * @return the LastUseDate
     */
    public String getLastUseDate() {
        return (String) getAttributeInternal(LASTUSEDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LastUseDate.
     * @param value value to set the  LastUseDate
     */
    public void setLastUseDate(String value) {
        setAttributeInternal(LASTUSEDATE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

