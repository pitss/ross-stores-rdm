package com.ross.rdm.trailermgmt.hhmovetrailers.model.views.common;

import java.util.List;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Feb 29 11:22:16 PST 2016
// ---------------------------------------------------------------------
public interface HhMoveTrailerSMainView extends ViewObject {
    String callVTrailerId(String facilityId, String trailerId);

    String callVBadStatus(String facilityId, String trailerId);

    void setGlobalVariablesMoveTrailer();

    List<String> callNewLocValidation(String iNewLocation, String iFacilityId, String iTaskManagement,
                                      String pNewTrailerLocation);

    List<String> callMTDisplayForm(String facilityId, String trailerId);

    String callGetWhId(String facilityId, String locationId);

    void callUpdateTrailer(String newLocation, String trailerId, String facilityId, String userId);

    void callDeleteTask(String newLocation, String trailerId, String facilityId, String userId);

    String callGetWhDescription(String facilityId, String newLocationWhId);

    void callTrExit();
}

