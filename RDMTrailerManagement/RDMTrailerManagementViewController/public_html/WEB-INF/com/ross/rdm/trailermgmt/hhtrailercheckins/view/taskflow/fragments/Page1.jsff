<?xml version="1.0" encoding="UTF-8"?>
<ui:composition xmlns:f="http://java.sun.com/jsf/core" xmlns:af="http://xmlns.oracle.com/adf/faces/rich"
                xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:ui="http://java.sun.com/jsf/facelets">
    <c:set var="facilityId" value="#{CurrentUser.facilityId}"/>
    <c:set var="languageCode" value="#{CurrentUser.languageCode}"/>
    <c:set var="dataAvailable" value="#{not empty bindings.TrailerId.inputValue}"/>
    <c:set var="disabled"
           value="#{dataAvailable and not bindings.IsNew.inputValue and not bindings.CheckInFlag.inputValue and not bindings.ProcessTrailerExecuted.inputValue}"/>
    <c:set var="shuttleTrailer" value="#{bindings.ShuttleTrailer.inputValue}"/>
    <c:set var="firstTime" value="#{pageFlowScope.HhTrailerCheckInSBean.firstTime}"/>
    <c:set var="isError" value="#{pageFlowScope.HhTrailerCheckInSBean.messageIcon eq 'error'}"/>
    <c:set var="focusControl" value="#{pageFlowScope.HhTrailerCheckInSBean.focusControl}"/>
    <af:panelGroupLayout id="pg1" layout="scroll">
        <af:resource type="javascript" source="/resources/js/trailerCheckIn-min.js"/>
        <af:resource type="javascript">
          SetFocusOnUIcomp("input[id*='${pageFlowScope.HhTrailerCheckInSBean.focusControl}']");
        </af:resource>
        <af:panelGridLayout id="pg2" styleClass="headerPanel">
            <af:gridRow id="gr1" height="auto">
                <af:gridCell id="gc1" halign="start" width="100%" valign="bottom">
                    <af:outputText id="ot1" value="#{CurrentUser.userName}"></af:outputText>
                </af:gridCell>
                <af:gridCell id="gc2" halign="center" width="100%">
                    <af:outputText id="ot2"
                                   value="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'TRAILER_CHECK_IN')}"/>
                </af:gridCell>
                <af:gridCell id="gc3" halign="end" width="100%">
                    <af:statusIndicator id="wsi"/>
                </af:gridCell>
            </af:gridRow>
        </af:panelGridLayout>
        <af:panelGridLayout id="pg4">
            <af:gridRow id="gr2" height="auto">
                <af:gridCell id="gc4" halign="#{agentInfo.smartPhone?'start':'center'}" width="100%">
                    <af:panelFormLayout id="pag" labelAlignment="start" partialTriggers="f2 f3 f4 f5 f9 f10">
                        <af:panelLabelAndMessage id="pcar"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'CARRIER')}"
                                                 for="car">
                            <!-- disabled="#{disabled}"  -->
                            <af:inputText id="car" value="#{pageFlowScope.HhTrailerCheckInSBean.carrierCode}"
                                          columns="21" simple="true" autoSubmit="false"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedCarrierCode}"
                                          clientComponent="true" disabled="false"
                                          maximumLength="#{bindings.CarrierCode.hints.precision}"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.carrierControl}"
                                          converter="upperCaseConverter">
                                <af:clientListener type="blur" method="markFocus"/>
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <!-- <af:clientListener type="keyUp" method="enterNavigationPrefilter"/> -->
                                <af:clientListener type="keyUp" method="onKeyPressedCarrierCodeTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'car' ? 'error' : 'required'}" id="i1"
                                         styleClass="keyLinks" partialTriggers="car"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="ptra"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'TRAILER')}"
                                                 for="tra">
                            <!-- disabled="#{not dataAvailable or disabled}" -->
                            <af:inputText id="tra" value="#{pageFlowScope.HhTrailerCheckInSBean.trailerId}" columns="21"
                                          simple="true"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedTrailerId}"
                                          autoSubmit="false" disabled="true"
                                          maximumLength="#{bindings.TrailerId.hints.precision}"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.trailerControl}"
                                          clientComponent="true" converter="upperCaseConverter">
                                <af:clientListener type="blur" method="markFocus"/>
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="keyUp" method="onKeyPressedTrailerIdTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'tra' ? 'error' : 'required'}" id="i2"
                                         styleClass="keyLinks" partialTriggers="tra"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="pzon2"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'ZONE')}"
                                                 for="zon2">
                            <!-- disabled="#{not dataAvailable or disabled}" -->
                            <af:inputText id="zon2"
                                          value="#{pageFlowScope.HhTrailerCheckInSBean.zone}"
                                          required="#{bindings.Zone.hints.mandatory}" columns="21" maximumLength="2"
                                          shortDesc="#{bindings.Zone.hints.tooltip}" simple="true"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedZone}"
                                          autoSubmit="false" disabled="true"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.zoneControl}"
                                          clientComponent="true" converter="upperCaseConverter">
                                <af:clientListener type="blur" method="markFocus"/>
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <!-- <af:clientListener type="keyUp" method="enterNavigationPrefilter"/> -->
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <f:validator binding="#{bindings.Zone.validator}"/>
                                <af:clientListener type="keyUp" method="onKeyPressedZoneTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'zon2' ? 'error' : 'required'}" id="i3"
                                         styleClass="keyLinks" partialTriggers="zon2"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="pinO"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'IN_OUT')}"
                                                 for="inO">
                            <!-- disabled="#{not dataAvailable or disabled or shuttleTrailer}" clientComponent="true" -->
                            <af:inputText id="inO"
                                          value="#{pageFlowScope.HhTrailerCheckInSBean.inOut}"
                                          required="#{bindings.InOut.hints.mandatory}" columns="21" maximumLength="1"
                                          shortDesc="#{bindings.InOut.hints.tooltip}" simple="true"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedInOut}"
                                          autoSubmit="false" disabled="true"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.inOutControl}"
                                          converter="upperCaseConverter">
                                <f:validator binding="#{bindings.InOut.validator}"/>
                                <!-- <af:clientListener type="blur" method="markFocus"/> -->
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <!-- <af:clientListener type="keyUp" method="enterNavigationPrefilter"/> -->
                                <af:clientListener type="blur" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="keyUp" method="onKeyPressedInOutTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'inO' ? 'error' : 'required'}" id="i4"
                                         styleClass="keyLinks" partialTriggers="inO"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="psea"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'SEAL_NBR')}">
                            <!-- disabled="#{not dataAvailable or disabled or shuttleTrailer}" -->
                            <af:inputText id="sea"
                                          value="#{pageFlowScope.HhTrailerCheckInSBean.sealNbr}"
                                          required="#{bindings.SealNbr.hints.mandatory}" columns="21"
                                          maximumLength="#{bindings.SealNbr.hints.precision}"
                                          shortDesc="#{bindings.SealNbr.hints.tooltip}" simple="true"
                                          clientComponent="true" disabled="true"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.sealNbrControl}"
                                          autoSubmit="false"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedSealNbr}">
                                <f:validator binding="#{bindings.SealNbr.validator}"/>
                                <!-- <af:clientListener type="blur" method="markFocus"/> -->
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <!-- <af:clientListener type="keyUp" method="enterNavigationPrefilter"/> -->
                                <af:clientListener type="blur" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="keyUp" method="onKeyPressedSealNbrTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'sea' ? 'error' : 'required'}" id="i5"
                                         styleClass="keyLinks" partialTriggers="sea"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="psea5"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'SEAL_INTACT')}">
                            <!-- disabled="#{not dataAvailable or disabled}" -->
                            <af:inputText id="slInt"
                                          value="#{pageFlowScope.HhTrailerCheckInSBean.sealIntact}"
                                          required="#{bindings.SealIntact.hints.mandatory}" columns="21"
                                          maximumLength="#{bindings.SealIntact.hints.precision}"
                                          shortDesc="#{bindings.SealIntact.hints.tooltip}" simple="true"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedSealIntact}"
                                          autoSubmit="false" disabled="true" clientComponent="true"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.sealIntactControl}"
                                          converter="upperCaseConverter">
                                <f:validator binding="#{bindings.SealIntact.validator}"/>
                                <!-- <af:clientListener type="blur" method="markFocus"/> -->
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <!-- <af:clientListener type="keyUp" method="enterNavigationPrefilter"/> -->
                                <af:clientListener type="blur" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="keyUp" method="onKeyPressedSealIntactTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'slInt' ? 'error' : 'required'}" id="i6"
                                         styleClass="keyLinks" partialTriggers="slInt"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="ptro"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'TRB')}"
                                                 for="tro">
                            <!-- disabled="#{not dataAvailable or (disabled and not empty bindings.TroubleCode.inputValue)}" -->
                            <af:inputText id="tro"
                                          value="#{dataAvailable ? bindings.TroubleCode.inputValue : pageFlowScope.HhTrailerCheckInSBean.troubleCode}"
                                          required="#{bindings.TroubleCode.hints.mandatory}" columns="21"
                                          maximumLength="#{bindings.TroubleCode.hints.precision}"
                                          shortDesc="#{bindings.TroubleCode.hints.tooltip}" simple="true"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedTroubleCode}"
                                          autoSubmit="false" disabled="true"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.troubleCodeControl}"
                                          clientComponent="true" converter="upperCaseConverter">
                                <f:validator binding="#{bindings.TroubleCode.validator}"/>
                                <af:clientListener type="blur" method="markFocus"/>
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <!-- <af:clientListener type="keyUp" method="trbCodeEnterNavigation"/> -->
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="keyUp" method="onKeyPressedTroubleCodeTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'tro' ? 'error' : ''}" id="i8"
                                         styleClass="keyLinks" partialTriggers="tro"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                        <af:panelLabelAndMessage id="pyar1"
                                                 label="#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'LOCATION_ID')}"
                                                 for="yar1">
                            <!-- disabled="#{not dataAvailable or disabled || firstTime}" -->
                            <af:inputText id="yar1"
                                          value="#{pageFlowScope.HhTrailerCheckInSBean.locationId}"
                                          required="#{bindings.LocationId.hints.mandatory}" columns="21"
                                          maximumLength="#{bindings.LocationId.hints.precision}"
                                          shortDesc="#{bindings.LocationId.hints.tooltip}" simple="true"
                                          valueChangeListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onChangedYardLocationId}"
                                          autoSubmit="false" clientComponent="true" disabled="true"
                                          binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.yardLocationControl}">
                                <f:validator binding="#{bindings.LocationId.validator}"/>
                                <af:clientListener type="blur" method="markFocus"/>
                                <!-- <af:clientListener type="focus" method="preFocusValue"/> -->
                                <!-- <af:clientListener type="keyUp" method="enterNavigationPrefilter"/> -->
                                <af:clientListener type="focus" method="inputOnBlurFocusHanlder"/>
                                <af:clientListener type="keyUp" method="onKeyPressedLocationIdTrailerCheckIn"/>
                                <af:serverListener type="customKeyEvent"
                                                   method="#{backingBeanScope.HhTrailerCheckInSPage1Backing.performKey}"/>
                            </af:inputText>
                            <f:facet name="end">
                                <af:icon name="#{isError and focusControl eq 'yar1' ? 'error' : ''}" id="i9"
                                         styleClass="keyLinks" partialTriggers="yar1"/>
                            </f:facet>
                        </af:panelLabelAndMessage>
                    </af:panelFormLayout>
                </af:gridCell>
            </af:gridRow>
            <af:gridRow id="gr3" height="auto">
                <af:gridCell id="gc5" halign="#{agentInfo.smartPhone?'start':'center'}" width="100%">
                    <af:spacer id="sp2" height="5"/>
                    <af:panelGroupLayout id="pg5" valign="top" halign="left" visible="true"
                                         binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.allMessagesPanel}"
                                         styleClass="errorPanel" layout="vertical">
                        <af:panelGroupLayout id="pg6" layout="horizontal" styleClass="AFStretchWidth">
                            <af:icon id="ic1" name="#{pageFlowScope.HhTrailerCheckInSBean.messageIcon}"/>
                            <af:spacer id="sp3" width="10"/>
                            <af:outputText id="info" value="#{pageFlowScope.HhTrailerCheckInSBean.messageText}"
                                           clientComponent="true"/>
                        </af:panelGroupLayout>
                    </af:panelGroupLayout>
                    <af:panelGroupLayout id="pg10" layout="vertical" halign="left" styleClass="pad-left-8">
                        <af:panelGridLayout id="pgl7">
                            <af:gridRow height="auto" id="gr4">
                                <af:gridCell width="33%" id="gc11">
                                    <af:link id="f2"
                                             text="F2=#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'BOL')}"
                                             styleClass="keyLinks" clientComponent="true"
                                             actionListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.f2BolActionListener}"
                                             partialSubmit="true"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.bolLink}">
                                        <af:setPropertyListener type="action" from="#{true}"
                                                                to="#{pageFlowScope.HhTrailerCheckInSBean.executedKey}"/>
                                    </af:link>
                                </af:gridCell>
                                <af:gridCell width="33%" id="gc9">
                                    <af:link id="f3"
                                             text="F3=#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'EXIT')}"
                                             styleClass="keyLinks" clientComponent="true" action="Back"
                                             partialSubmit="true" immediate="true"
                                             actionListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.f3ActionListener}"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.exitLink}"></af:link>
                                </af:gridCell>
                                <af:gridCell width="33%" id="gc12">
                                    <af:link id="f4"
                                             text="F4=#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'DONE')}"
                                             styleClass="keyLinks" clientComponent="true" partialSubmit="true"
                                             actionListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.f4DoneActionListener}"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.doneLink}">
                                        <af:setPropertyListener type="action" from="#{true}"
                                                                to="#{pageFlowScope.HhTrailerCheckInSBean.executedKey}"/>
                                    </af:link>
                                </af:gridCell>
                            </af:gridRow>
                            <af:gridRow height="auto" marginBottom="5px" id="gr6">
                                <af:gridCell width="33%" id="gc7">
                                    <af:link id="f5"
                                             text="F5=#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'CARRIER')}"
                                             styleClass="keyLinks" clientComponent="true" partialSubmit="true"
                                             disabled="#{bindings.IsNew.inputValue eq false}" partialTriggers="f4"
                                             actionListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.f5CarrierActionListener}"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.carrierLink}">
                                        <af:setPropertyListener from="true"
                                                                to="#{pageFlowScope.HhTrailerCheckInSBean.isCarrierCode}"
                                                                type="action"/>
                                    </af:link>
                                </af:gridCell>
                                <af:gridCell width="33%" id="gc8">
                                    <af:link id="f9"
                                             text="F9=#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'TRBL')}"
                                             styleClass="keyLinks" clientComponent="true" partialSubmit="true"
                                             partialTriggers="tro"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.trblLink}"
                                             actionListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.f9TroubleCodeActionListener}">
                                        <af:setPropertyListener from="false"
                                                                to="#{pageFlowScope.HhTrailerCheckInSBean.isCarrierCode}"
                                                                type="action"/>
                                    </af:link>
                                </af:gridCell>
                                <af:gridCell width="33%" id="gc10">
                                    <af:link id="f10"
                                             text="F10=#{resourceLabelBean.attributeLabel(facilityId, languageCode, 'TROUBLE_CLEAR')}"
                                             styleClass="keyLinks" clientComponent="true" partialSubmit="true"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.trblClrLink}"
                                             actionListener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.f10TroubleClearActionListener}">
                                        <af:setPropertyListener type="action" from="#{true}"
                                                                to="#{pageFlowScope.HhTrailerCheckInSBean.executedKey}"/>
                                    </af:link>
                                    <af:link id="l1" partialSubmit="true" visible="false"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.changeTrailerHiddenLink}"
                                             action="#{backingBeanScope.HhTrailerCheckInSPage1Backing.changeTrailerHiddenAction}"/>
                                    <af:link id="hidTro" partialSubmit="true" visible="false"
                                             binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.changeTroubleHiddenLink}"
                                             action="#{backingBeanScope.HhTrailerCheckInSPage1Backing.changeTroubleHiddenAction}"/>
                                </af:gridCell>
                            </af:gridRow>
                        </af:panelGridLayout>
                    </af:panelGroupLayout>
                </af:gridCell>
            </af:gridRow>
        </af:panelGridLayout>
        <af:popup id="p1" clientComponent="true"
                  binding="#{backingBeanScope.HhTrailerCheckInSPage1Backing.confirmationPopup}"
                  contentDelivery="lazyUncached">
            <af:clientListener type="popupOpened" method="defaultNoFocus"/>
            <af:dialog id="cfmDlg" closeIconVisible="false" title="CONFIRM" type="none" clientComponent="true">
                        <af:clientListener method="checkCloseDialogWip" type="dialog"/>
                <af:panelLabelAndMessage id="plam12">
                    <af:icon name="#{viewScope.isConfirmationOnly ? 'info' : 'confirmation'}" id="i7"/>
                    <f:facet name="end">
                        <af:outputText id="ot4"/>
                    </f:facet>
                </af:panelLabelAndMessage>
                <f:facet name="buttonBar">
                    <af:panelGroupLayout id="pgl12" halign="center" layout="horizontal">
                        <af:link text="F1=Yes" rendered="#{not viewScope.isConfirmationOnly}" id="ylp"
                                 styleClass="keyLinks" partialSubmit="true" clientComponent="true">
                            <af:clientListener type="action" method="dialogYes"/>
                            <af:clientListener type="keyDown" method="onDialogKey"/>
                        </af:link>
                        <af:link text="F2=No" rendered="#{not viewScope.isConfirmationOnly}" id="no"
                                 partialSubmit="true" styleClass="keyLinks" clientComponent="true">
                            <af:clientListener type="action" method="dialogNo"/>
                            <af:clientListener type="keyDown" method="onDialogKey"/>
                            <af:clientListener type="blur" method="onBlurHanlderListOfValues"/>
                        </af:link>
                        <af:link text="" rendered="#{not viewScope.isConfirmationOnly}" id="nlp"
                                 partialSubmit="true" styleClass="keyLinks" clientComponent="true">
                            <af:clientListener type="action" method="dialogCancel"/>
                            <af:clientListener type="keyDown" method="onDialogKey"/>
                            <af:clientListener type="blur" method="onBlurHanlderListOfValues"/>
                        </af:link>
                        <af:link text="F1=Ok" rendered="#{viewScope.isConfirmationOnly}" id="okay" partialSubmit="true"
                                 styleClass="keyLinks" clientComponent="true">
                            <af:clientListener type="action" method="dialogYes"/>
                            <af:clientListener type="keyDown" method="onDialogKey"/>
                        </af:link>
                        <f:facet name="separator">
                            <af:spacer width="5" id="s5"/>
                        </f:facet>
                    </af:panelGroupLayout>
                </f:facet>
            </af:dialog>
        </af:popup>
        <af:resource type="css" source="/resources/css/TrailerManagement-min.css"/>
        <f:event listener="#{backingBeanScope.HhTrailerCheckInSPage1Backing.onRegionLoad}" type="preRenderComponent"/>
    </af:panelGroupLayout>
</ui:composition>
