function onF3KeyPressedLocInq(event) {
    var component = event.getSource();
    linkComp = component.findComponent('f3');
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY ){
        AdfActionEvent.queue(linkComp, true);
    }
    else if (keyPressed == AdfKeyStroke.TAB_KEY || keyPressed == AdfKeyStroke.ENTER_KEY  ){ 
        linkComp = component.findComponent('eb1'); 
        AdfActionEvent.queue(linkComp, true);
    }
    event.cancel();
}