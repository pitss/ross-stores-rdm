function onF1OkKeyPressedResetTrailer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        linkComp = component.findComponent('goOk');
        AdfActionEvent.queue(linkComp, true);
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY) {
        linkComp = component.findComponent('goNo');
        AdfActionEvent.queue(linkComp, true);
    }
    event.cancel();
}

function onConfirmKeyPressedResetTrailer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        linkComp = component.findComponent('ylp');
        AdfActionEvent.queue(linkComp, true);
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY) {
        linkComp = component.findComponent('nlp');
        AdfActionEvent.queue(linkComp, true);
    }
    event.cancel();
}

function onFuncKeyPressedResetTrailer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        linkComp = component.findComponent('f3');
        AdfActionEvent.queue(linkComp, true);
    }
    else if (keyPressed == AdfKeyStroke.F4_KEY) {
        linkComp = component.findComponent('f4');
        AdfActionEvent.queue(linkComp, true);
    }
    else if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        //Perform value change
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
    }

    event.cancel();
}

function confirmPopupFocusResetTrailer(event) {
    focusItemResetTrailer(event, 'nlp');
}

function successPopupFocusResetTrailer(event) {
    focusItemResetTrailer(event, 'goNo');
}

function focusItemResetTrailer(event, item) {
    var component = event.getSource();
    var linkComp = component.findComponent(item);
    linkComp.focus();
}

function selectInputElementResetTrailer(event) {
    var elem = findInputElementById(event.getSource().getClientId());
    $(elem).select();
}

function inputOnBlurFocusHandlerRT(event) {
    try {
        event.getKeyCode();
    }
    catch (err) {
        inputOnBlurFocusHanlder(event);
        event.cancel();
    }
}

// Shuttle Trailer functions
var oldValST = null;//original value of input text set on focus
function onFocusST(event) {
    var component = event.getSource();
    oldValST = component.getSubmittedValue();
}

function onTabEnterNumCopiesShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "NumCopies", true);
}

function onTabEnterQueueShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "Queue", true);
}

function onTabEnterTrailerShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "TrailerId", true);
}

function onTabEnterDoorShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "Door", true);
}

function onTabEnterTypeIOShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "TypeIo", true);
}

function onTabEnterTypeMLPShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "MlpYn", true);
}

function onTabEnterDestShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "DestWhId", true);
}

function onTabEnterWrtLocShuttleTrailer(event) {
    onTabEnterShuttleTrailer(event, "WrtLoc", true);
}

function onTabEnterSealNbrShuttleTrailer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, item : "SealNbr"
        },
true);
        event.cancel();
    }
}

function onTabEnterShuttleTrailer(event, item, compareVal) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        var newValST = component.getSubmittedValue();
        if (newValST && (!compareVal || (compareVal && newValST != oldValST))) {
            // enterNavigationPrefilter(event);
        }
        else {
            AdfCustomEvent.queue(component, "customKeyEvent", 
            {
                keyPressed : keyPressed, item : item
            },
true);
            event.cancel();
        }
    }
}

function OnOpenConfirmPopup(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('ylp');
    linkComp.focus();
}

function onBlurShuttleTrailerPopup(event) {
    onBlurPopupHanlderTrailer(event, 'ylp');
}

function onBlurPopupHanlderTrailer(event, item) {
    var component = event.getSource();
    var keyPressed;
    try {
        keyPressed = event.getKeyCode();
    }
    catch (err) {
        var linkComp = component.findComponent(item);
        linkComp.focus();
    }
    event.cancel();
}

function onKeyPressedShuttleTrailerPage1(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function setFocusShuttlePage4() {
    setTimeout(function () {
        var focusOn = customFindElementById('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedShuttlePage4(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY) {
        AdfActionEvent.queue(component.findComponent('f4'), true);
        event.cancel();
    }
}

function onKeyPressedShuttleTrailerPage5(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyPressedShuttleTrailerPage3(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyPressedShuttleTrailerPage2(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyPressPendingTaskShuttlePage2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('yesLinkPopup1'), true);
    }
    else if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfActionEvent.queue(component.findComponent('noLinkPopup1'), true);
    }
    event.cancel();
}

function onOpenPendingTaskShuttlePage2(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup1');
    linkComp.focus();
}

function onKeyPressSuccessOperShuttlePage2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('okLinkPopup1'), true);
    }
    event.cancel();
}

function onOpenSuccessOperShuttlePage2(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('okLinkPopup1');
    linkComp.focus();
}