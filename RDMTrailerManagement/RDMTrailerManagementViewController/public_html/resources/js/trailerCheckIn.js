function setTrailerCheckInInitialFocusP2() {
    setTimeout(function() {
    var focusOn = customFindElementByIdContainer('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedOnTrailerCheckInTable(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        if (keyPressed == AdfKeyStroke.F3_KEY) {
            AdfActionEvent.queue(component.findComponent('f3'), true);
        }
        if (keyPressed == AdfKeyStroke.F4_KEY) {
            AdfActionEvent.queue(component.findComponent('f4'), true);
        }
        if (keyPressed == AdfKeyStroke.F7_KEY) {
            AdfActionEvent.queue(component.findComponent('f7'), true);
        }
        if (keyPressed == AdfKeyStroke.F8_KEY) {
            AdfActionEvent.queue(component.findComponent('f8'), true);
        }
        event.cancel();
    }
}

function customFindElementByIdContainer(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}
function onKeyPressedCarrierCodeTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'car');
}

function onKeyPressedTrailerIdTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'tra');
}

function onKeyPressedZoneTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'zon2');
}

function onKeyPressedInOutTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'inO');
}

function onKeyPressedSealNbrTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'sea');
}

function onKeyPressedSealIntactTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'slInt');
}

function onKeyPressedTroubleCodeTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'tro');
}

function onKeyPressedLocationIdTrailerCheckIn(event) {
    this.onKeyPressedTrailerCheckInPage1(event, 'yar1');
}

function onKeyPressedTrailerCheckInPage1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.F10_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
        true);
    }
    event.cancel();
}

function onKeyPressedBolNbrTrailerCheckIn(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
        true);
    }
    event.cancel();
}