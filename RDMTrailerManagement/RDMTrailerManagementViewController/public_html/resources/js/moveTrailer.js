function setInitialFocus() {
    setTimeout(function () {
        var focusOn = customMTFindElementById('tra');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function customMTFindElementById(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}

function inputOnBlurFocusHandler(event) {
    SetFocusOnUIcomp(event.getSource());
}

function onKeyPressedOnTrailerId(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : "trailerId"
        },
true);
        event.cancel();
    }
}

function onKeyPressedNewLocation(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : "newLocation"
        },
true);
        event.cancel();
    }
}

function onOpenGoToPopup(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('oklp');
    linkComp.focus();
}

function OnOpenConfirmPopup(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('ylp');
    linkComp.focus();
}

function okLinkGoPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY) {
        event.cancel();
    }
    else {
        AdfActionEvent.queue(component, true);
    }
}

function noYesLinkConfirmPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var item = component.getClientId().split(':')[3];
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F1_KEY) {
            fLink = component.findComponent('ylp');
            fLink.focus();
        }
        AdfCustomEvent.queue(fLink, "customKeyEvent", 
        {
            keyPressed : keyPressed, item : item
        },
true);
    }
    event.cancel();
}