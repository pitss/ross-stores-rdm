function setTrailerInqPage1InitialFocus() {
    var focusId = 'tra';
    var focusIdCode = ("[id*='" + focusId + "']");
    SetFocusOnUIcomp(focusIdCode);
}

function onKeyPressedOnTrailerId(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : "trailerId"
        },
true);
        event.cancel();
    }
}

function onKeyPressedTrailerInquiryP2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode()
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b3'), true);
    } else if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function onKeyPressedTrailerInquiryP3(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode()
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function inputOnBlurFocusHandlerTI(event) {
    SetFocusOnUIcomp(event.getSource());
    setFocusOrSelectOnUIcomp(event.getSource())
}