package com.ross.rdm.trailermgmt.hhlocationinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page1.jspx
// ---    
// ---------------------------------------------------------------------
public class Page1Backing extends RDMTrailerManagementBackingBean {
  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon errorWarnInfoIcon;
  private RichOutputText errorWarnInfoMessage;
  private static final String OPR_DISPLAY_FORM = "displayForm";
  private static final String ATTR_LOCATION_ID = "LocationId";
  private  static final String ERR_ICON   = "error";
  private  static final String REQ_ICON   = "required";
    
    private RichInputText locationIdBean;
    private RichIcon locationIdIcon;

    public Page1Backing() {

  }

  public void onChangedLocationId(ValueChangeEvent vce) {
    //Forms  Module : hh_location_inquiry_s
    //Object        : MAIN.LOCATION_ID.KEY-NEXT-ITEM
    //Source Code   : 
    //
    //PKG_HOTEL_INV.DISPLAY_FORM(P_DISPLAY.V_DISPLAY_STRING ,:MAIN.LAST_USE_DATE ,:MAIN.LOCATION_ID ,:MAIN.TRAILER_ID ,:MAIN.TRAILER_STATUS ,:WORK.FACILITY_ID  );
    //IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
   if( vce != null){
      String lVal = (String)vce.getNewValue();
      if (lVal != null) lVal = lVal.toUpperCase();
      ADFUtils.setBoundAttributeValue(ATTR_LOCATION_ID, lVal);
   }
   OperationBinding ob = ADFUtils.findOperation(OPR_DISPLAY_FORM);
   Map retunVal = (Map)ob.execute();
   List<String> errors = (List<String>) retunVal.get("ErrorMessage");
   if (errors != null && errors.size()> 0){
       showMessagePanel(errors.get(1));
       setErrorStyleOnComponent(getLocationIdBean(),getLocationIdIcon(),true);
   }
   else {
       hideMessagePanel();
       setErrorStyleOnComponent(getLocationIdBean(),getLocationIdIcon(),false);
   }
      
   
  }
    public void validateEmptyLocation(ActionEvent actionEvent) {
      Object val =  ADFUtils.getBoundAttributeValue(ATTR_LOCATION_ID);
      if (val == null) { //Validate only if null, else validation method will take care
        onChangedLocationId (null);
      }
    }
    
  private void showMessagePanel(String msg){
      errorWarnInfoIcon.setName(ERR_ICON);
      errorWarnInfoMessage.setValue(msg);
      refreshContentOfUIComponent(allMessagesPanel);
  }
  private void hideMessagePanel(){
      errorWarnInfoIcon.setName(null);
      errorWarnInfoMessage.setValue(null);
      refreshContentOfUIComponent(allMessagesPanel);
  }
    void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set){
        if (set){
            addErrorStyleToComponent(uiComponent); 
            icon.setName(ERR_ICON);              
        }
        else {
            icon.setName(REQ_ICON); 
            removeErrorStyleToComponent(uiComponent);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(icon);
        AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }
    
  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()  {
    return allMessagesPanel;
  }
  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()  {
    return errorWarnInfoIcon;
  }
  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()  {
    return errorWarnInfoMessage;
  }


    public void onRegionLoad(ComponentSystemEvent cse) {
        setFocusOnUIComponent(getLocationIdBean());
    }

    public void setLocationIdBean(RichInputText locationIdBean) {
        this.locationIdBean = locationIdBean;
    }

    public RichInputText getLocationIdBean() {
        return locationIdBean;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }
    
    public String exitAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_location_inquiry_s_MAIN_F3,
                                                RoleBasedAccessConstants.FORM_NAME_hh_location_inquiry_s)) {
            action = "backGlobalHome";
        } else {
            showMessagePanel(getMessage("NOT_ALLOWED", null, null, null));
        }
        return action;
    }
}
