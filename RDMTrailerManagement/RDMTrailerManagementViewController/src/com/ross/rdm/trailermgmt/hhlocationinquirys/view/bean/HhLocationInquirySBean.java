package com.ross.rdm.trailermgmt.hhlocationinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.binding.OperationBinding;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhLocationInquirySBean  {
private static final String OPR_ADD_RECORD = "addRecord";
      public void initTaskFlow() {
            //Forms  Module : hh_location_inquiry_s
            //Object        : WHEN-NEW-FORM-INSTANCE
            //Source Code   : Tr_Startup; Startup_Local;
           OperationBinding ob = ADFUtils.findOperation( OPR_ADD_RECORD);
           ob.execute();
        }

}
