package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page3.jspx
// ---
// ---------------------------------------------------------------------
public class Page3Backing extends ShuttleTrailerBaseBean {


    private static final String OPR_QUEUE_KEY_NEXT = "queueKeyNext";
    private static final String OPR_CLEAR_PRINT_BOL = "clearPrintBol";
    private static final String OPR_SEND_MAIL = "sendEmail";
    private static final String OPR_PRINT_BOL = "printBol";

    private RichInputText queue;
    private RichIcon queueIcon;
    private RichInputText numCopies;
    private RichIcon numCopiesIcon;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f6Link;
    private RichLink f7Link;

    public Page3Backing() {

    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        String printerName = getShuttleTrailerBean().getPrinterName();
        if (printerName != null) {
            ADFUtils.setBoundAttributeValue(ATTR_QUEUE, printerName);
            getShuttleTrailerBean().setPrinterName(null);

            ValueChangeEvent vce = new ValueChangeEvent(getQueue(), null, printerName);
            getQueue().queueEvent(vce);
        }

        String focus = getShuttleTrailerBean().getIsFocusOn();
        if (focus == null || GOTO_QUEUE.equals(focus))
            setFocusOnUIComponent(getQueue());
        else if (GOTO_NUM_COPIES.equals(focus))
            setFocusOnUIComponent(getNumCopies());

    }

    public void onChangedQueue(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            hideMessagePanel();
            setValidationError(false);
            setErrorStyleOnComponent(getQueue(), getQueueIcon(), false);
            updateModel(vce);
            OperationBinding ob = ADFUtils.findOperation(OPR_QUEUE_KEY_NEXT);
            Map resultVal = (Map) ob.execute();
            if (resultVal != null) {
                String msgCode = (String) resultVal.get("ErrorCode");
                String msgMesg = (String) resultVal.get("ErrorMesg");
                if (msgMesg != null) {
                    setValidationError(true);
                    showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                      MSG_TYPE_E, msgMesg);
                    return;
                }
                if (msgCode != null) {
                    setValidationError(true);
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            MSG_TYPE_E, msgCode);
                    setErrorStyleOnComponent(getQueue(), getQueueIcon(), true);
                    return;
                }
            }
            getShuttleTrailerBean().setIsFocusOn(GOTO_NUM_COPIES);
        }
    }

    public void onChangedNumberOfCopies(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            hideMessagePanel();
            setErrorStyleOnComponent(getNumCopies(), getNumCopiesIcon(), false);
            setValidationError(false);
            updateModel(vce);
            String numCopies = (String) vce.getNewValue();
            if (StringUtils.isNotEmpty(numCopies) && !numCopies.replace("-", "").matches("[0-9]+")) {
                setValidationError(true);
                showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                  getMessage("MUST_BE_NUMERIC", MSG_TYPE_W));
                setErrorStyleOnComponent(getNumCopies(), getNumCopiesIcon(), true);
                return;
            } else if (StringUtils.isNotEmpty(numCopies)) {
                boolean valid = true;
                try {
                    BigDecimal numberValue = new BigDecimal(numCopies);
                    if (numberValue.intValue() < 0) {
                        valid = false;
                    }
                } catch (Exception e) {
                    valid = false;
                }
                if (!valid) {
                    setValidationError(true);
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            MSG_TYPE_E, MSG_INV_VALUE);
                    setErrorStyleOnComponent(getNumCopies(), getNumCopiesIcon(), true);
                    return;
                }
            }
            getShuttleTrailerBean().setIsFocusOn(GOTO_QUEUE);
        }
    }


    public String f3ExitListener() {
        OperationBinding ob = ADFUtils.findOperation(OPR_CLEAR_PRINT_BOL);
        ob.execute();
        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
        return "return";
    }

    public String f4DoneListener() {
        if (isValidationError())
            return null; //return if attribute validation error already exists.
        hideMessagePanel();

        setErrorStyleOnComponent(getQueue(), getQueueIcon(), false);
        String queue = (String) ADFUtils.getBoundAttributeValue(ATTR_QUEUE);
        if (StringUtils.isEmpty(queue)) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_QUEUE);
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getQueue(), getQueueIcon(), true);
            return null;
        }

        setErrorStyleOnComponent(getNumCopies(), getNumCopiesIcon(), false);
        String numCopies = (String) ADFUtils.getBoundAttributeValue(ATTR_NUM_COPIES);
        if (StringUtils.isEmpty(numCopies)) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_NUM_COPIES);
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getNumCopies(), getNumCopiesIcon(), true);
            return null;
        }
        return processPrintBol(queue, numCopies);
    }

    public String f7SkipListener() {
        if (isValidationError())
            return null; //return if attribute validation error already exists.
        hideMessagePanel();
        return processPrintBol(null, "0");
    }

    private String processPrintBol(String queue, String numCopies) {
        //print bol 
        OperationBinding obp = ADFUtils.findOperation(OPR_PRINT_BOL);
        obp.getParamsMap().put("queueName", queue);
        obp.getParamsMap().put("numberOfCopies", numCopies);
        Map resultMap = (Map) obp.execute();
        
        if (resultMap != null) {
           String msgMesg = (String) resultMap.get("ErrorMessage");        
           if (msgMesg != null) {
              showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                msgMesg);
              return null;
        }        
        }
        //send email
        OperationBinding ob1 = ADFUtils.findOperation(OPR_SEND_MAIL);
        String resultVal = (String) ob1.execute();
        if (resultVal != null) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, resultVal);
            return null;
        }
        //clear block
        OperationBinding ob = ADFUtils.findOperation(OPR_CLEAR_PRINT_BOL);
        ob.execute();
        // go to move to yard
        getShuttleTrailerBean().setIsFocusOn(GOTO_ZONE);
        return "moveToYard";
    }

    public void setQueue(RichInputText queue) {
        this.queue = queue;
    }

    public RichInputText getQueue() {
        return queue;
    }

    public void setQueueIcon(RichIcon queueIcon) {
        this.queueIcon = queueIcon;
    }

    public RichIcon getQueueIcon() {
        return queueIcon;
    }


    public void setNumCopies(RichInputText numCopies) {
        this.numCopies = numCopies;
    }

    public RichInputText getNumCopies() {
        return numCopies;
    }

    public void setNumCopiesIcon(RichIcon numCopiesIcon) {
        this.numCopiesIcon = numCopiesIcon;
    }

    public RichIcon getNumCopiesIcon() {
        return numCopiesIcon;
    }

    public String goToPrintQueue() {
        getShuttleTrailerBean().setIsFocusOn(GOTO_NUM_COPIES);
        return "queue";
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if (valueNotChanged(submittedVal, field.getValue())) {
                    if ("que1".equals(field.getId())) {
                        onChangedQueue(new ValueChangeEvent(field, null, submittedVal));
                    } else if ("num".equals(field.getId())) {
                        onChangedNumberOfCopies(new ValueChangeEvent(field, null, submittedVal));
                    }
                }
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    @Override
    public void setFocusOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("setFocusOrSelectOnUIcomp(textInput);"); //changed selectFocusOnComp to selectFocusonUIComp fo IE using jquery
        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }
    
    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_trailermgmt_hhshuttletrailers_view_pageDefs_Page3PageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
