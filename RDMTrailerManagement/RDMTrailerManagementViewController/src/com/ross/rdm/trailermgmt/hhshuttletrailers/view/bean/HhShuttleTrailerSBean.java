package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.common.NameMapping;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhShuttleTrailerSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhShuttleTrailerSBean.class);
    private static final String OPR_INIT_TASKFLOW = "initTaskflow";
    private String isFocusOn;
    private Boolean manifoldFound = false;
    private String prevLoadType;
    private Boolean successMesg = false;
    private String printerName;
    private Boolean executedKey = Boolean.FALSE;

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setSuccessMesg(Boolean successMesg) {
        this.successMesg = successMesg;
    }

    public Boolean getSuccessMesg() {
        return successMesg;
    }

    public void setPrevLoadType(String prevLoadType) {
        this.prevLoadType = prevLoadType;
    }

    public String getPrevLoadType() {
        return prevLoadType;
    }

    public void setPrevSubLoadType(String prevSubLoadType) {
        this.prevSubLoadType = prevSubLoadType;
    }

    public String getPrevSubLoadType() {
        return prevSubLoadType;
    }
    private String prevSubLoadType;


    public void initTaskFlow() {
        //Forms  Module : hh_shuttle_trailer_s
        //Object        : WHEN-NEW-FORM-INSTANCE
        //Source Code   : Tr_Startup;
        //AHL.SET_AHL_INFO(:WORK.FACILITY_ID,:SYSTEM.CURRENT_FORM);
        OperationBinding ob = ADFUtils.findOperation(OPR_INIT_TASKFLOW);
        ob.execute();
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setManifoldFound(Boolean manifoldFound) {
        this.manifoldFound = manifoldFound;
    }

    public Boolean getManifoldFound() {
        return manifoldFound;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
    
    public TaskFlowId getMoveInventorySTaskFlowId(){
        // return "/WEB-INF/com/ross/rdm/hotelpicking/hhmoveinventorys/view/taskflow/HhMoveInventorySTaskFlow.xml";
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_move_inventory_s", "hotelpicking/"));
    }
}
