package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page5.jspx
// ---
// ---------------------------------------------------------------------
public class Page5Backing extends ShuttleTrailerBaseBean {

    private static final String OPR_VALIDATE_YARD_ZONE = "validateYardZone";
    private static final String OPR_VALIDATE_YARD_LOC = "validateYardLocation";
    private static final String OPR_INSERT_TASK_QUEUE = "insertTaskQueue";

    private RichIcon yardLocationIcon;
    private RichInputText yardLocation;
    private RichInputText yardZone;
    private RichIcon yardZoneIcon;
    private RichLink f4Link;
    private RichLink f7Link;

    public Page5Backing() {

    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        String focus = getShuttleTrailerBean().getIsFocusOn();
        if (focus == null || GOTO_ZONE.equals(focus))
            setFocusOnUIComponent(getYardZone());
        else if (GOTO_YARD_LOC.equals(focus))
            setFocusOnUIComponent(getYardLocation());
    }

    public void onChangedZone(ValueChangeEvent vce) {
        updateModel(vce);
        hideMessagePanel();
        setErrorStyleOnComponent(getYardZone(), getYardZoneIcon(), false);
        OperationBinding ob = ADFUtils.findOperation(OPR_VALIDATE_YARD_ZONE);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            String msgMesg = (String) resultVal.get("ErrorMesg");
            if (msgMesg != null) {
                showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                  msgMesg);
                return;
            }
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                setErrorStyleOnComponent(getYardZone(), getYardZoneIcon(), true);
                return;
            }
        }
        getShuttleTrailerBean().setIsFocusOn(GOTO_YARD_LOC);
    }

    public void onChangedYardLocation(ValueChangeEvent vce) {
        updateModel(vce);
        hideMessagePanel();
        setErrorStyleOnComponent(getYardLocation(), getYardLocationIcon(), false);
        OperationBinding ob = ADFUtils.findOperation(OPR_VALIDATE_YARD_LOC);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            String msgMesg = (String) resultVal.get("ErrorMesg");
            if (msgMesg != null) {
                showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                  msgMesg);
                return;
            }
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                setErrorStyleOnComponent(getYardLocation(), getYardLocationIcon(), true);
                return;
            }
        }
        getShuttleTrailerBean().setIsFocusOn("MOVE_TO_YARD.ZONE");
    }


    public void f4MoveListener(ActionEvent actionEvent) {
        if (isValidationError())
            return;
        hideMessagePanel();
        setErrorStyleOnComponent(getYardZone(), getYardZoneIcon(), false);
        setErrorStyleOnComponent(getYardLocation(), getYardLocationIcon(), false);
        if (ADFUtils.getBoundAttributeValue(ATTR_YARD_ZONE) == null) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_ZONE);
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getYardZone(), getYardZoneIcon(), true);
            return;
        } else if (ADFUtils.getBoundAttributeValue(ATTR_YARD_LOC) == null) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_YARD_LOC);
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getYardLocation(), getYardLocationIcon(), true);
            return;
        }
        OperationBinding ob = ADFUtils.findOperation(OPR_INSERT_TASK_QUEUE);
        ob.execute();

        getShuttleTrailerBean().setSuccessMesg(true);
        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
        ADFUtils.invokeAction("init");
    }

    public void f7SkipListener(ActionEvent actionEvent) {
        if (isValidationError())
            return;
        getShuttleTrailerBean().setSuccessMesg(true);
        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
        ADFUtils.invokeAction("init");
    }


    public void setYardLocationIcon(RichIcon yardLocationIcon) {
        this.yardLocationIcon = yardLocationIcon;
    }

    public RichIcon getYardLocationIcon() {
        return yardLocationIcon;
    }

    public void setYardLocation(RichInputText yardLocation) {
        this.yardLocation = yardLocation;
    }

    public RichInputText getYardLocation() {
        return yardLocation;
    }

    public void setYardZone(RichInputText yardZone) {
        this.yardZone = yardZone;
    }

    public RichInputText getYardZone() {
        return yardZone;
    }

    public void setYardZoneIcon(RichIcon yardZoneIcon) {
        this.yardZoneIcon = yardZoneIcon;
    }

    public RichIcon getYardZoneIcon() {
        return yardZoneIcon;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if (valueNotChanged(submittedVal, field.getValue())) {
                    if ("zon1".equals(field.getId())) {
                        onChangedZone(new ValueChangeEvent(field, null, submittedVal));
                    } else if ("yar1".equals(field.getId())) {
                        onChangedYardLocation(new ValueChangeEvent(field, null, submittedVal));
                    }
                }
            }
        }
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }
}
