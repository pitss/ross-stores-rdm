package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.jbo.Row;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page4.jspx
// ---
// ---------------------------------------------------------------------
public class Page4Backing extends ShuttleTrailerBaseBean {

    private RichTable printQueueTable;

    public Page4Backing() {

    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        DCIteratorBinding iter = ADFUtils.findIterator("HhShuttleTrailerSPrinterQueueViewIterator");
        Row [] rows = iter.getAllRowsInRange();
        if (rows.length > 0) {
           iter.setCurrentRowWithKey(rows[0].getKey().toStringFormat(true));
        }
    }

    public String queueSelection() {
        String printerName = (String) ADFUtils.getBoundAttributeValue(ATTR_PRINTER);
        if (printerName != null) {
            getShuttleTrailerBean().setPrinterName(printerName);
        }
        return "return";
    }

    public void setPrintQueueTable(RichTable printQueueTable) {
        this.printQueueTable = printQueueTable;
    }

    public RichTable getPrintQueueTable() {
        return printQueueTable;
    }
}
