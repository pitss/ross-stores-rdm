package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends ShuttleTrailerBaseBean {

    private RichIcon doorIcon;
    private RichIcon trailerIdIcon;
    private RichIcon typeIoIcon;
    private RichIcon mlpYnIcon;
    private RichIcon wrtLocIcon;
    private RichIcon sealNbrIcon;
    private RichInputText trailerId;
    private RichInputText door;
    private RichInputText typeIo;
    private RichInputText mlpYn;
    private RichIcon destWhIdIcon;
    private RichInputText destWhId;
    private RichInputText wrtLoc;
    private RichInputText sealNbr;
    private static final String MSG_SUCCESS_OPER = "SUCCESS_OPER";
    private static final String OPR_TRAILER_KEY_NEXT = "trailerKeyNext";
    private static final String OPR_DOOR_KEY_NEXT = "doorKeyNext";
    private static final String OPR_TYPE_IO_KEY_NEXT = "typeIoKeyNext";
    private static final String OPR_DEST_WHID_KEY_NEXT = "destWhIdKeyNext";
    private static final String OPR_WRT_LOC_KEY_NEXT = "wrtLocKeyNext";
    private static final String OPR_OPEN_TRAILER = "openTrailer";
    private static final String OPR_RESET_TRAILER = "resetTrailer";
    private static final String OPR_LOAD_TRAILER = "loadTrailer";
    private static final String OPR_CHECK_SCREEN_PRIV = "callCheckScreenOptionPriv";
    private static final String OPR_SAVE_LABOR_PROD = "saveLaborProd";
    private static final String OPR_CLOSE_TRAILER_ERR = "closeTrailerError";
    private static final String OPR_CLOSE_TRAILER_I = "closeTrailerTypeI";
    private static final String OPR_SET_MLP_COUNT = "setMlpCount";

    private static final String FORM_NAME_MOVE_INV = "hh_move_inventory_s";

    private static final String OFF = "OFF";
    private RichPopup emptyTrailerPopup;
    private RichLink f3Link;
    private RichLink f5Link;
    private RichLink f6Link;
    private RichLink f7Link;
    private RichLink f8Link;
    private RichLink f9Link;
    private RichLink validateTrailerHiddenLink;
    private RichLink validateDoorHiddenLink;
    private RichLink validateTypeHiddenLink;
    private RichLink validateMlpHiddenLink;
    private RichLink validateDestHiddenLink;
    private RichLink validateWrtHiddenLink;
    private RichLink validateSealHiddenLink;
    private RichPanelFormLayout panelForm;
    private RichPopup confirmPopup;
    private String confirmMessage;


    public Page1Backing() {

    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        if (getShuttleTrailerBean() == null)
            return; //not in correct phase yet
        if (getShuttleTrailerBean().getSuccessMesg()) {
            getShuttleTrailerBean().setSuccessMesg(false);
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), SUCCESS,
                                    MSG_SUCCESS_OPER);
        }
        String focus = getShuttleTrailerBean().getIsFocusOn();
        if (focus == null || GOTO_TRAILER_ID.equals(focus)) {
            if (focus == null)
                getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
            setFocusOnUIComponent(getTrailerId());
        } else if (GOTO_DOOR.equals(focus)) {
            setFocusOnUIComponent(getDoor());
        } else if (GOTO_TYPE_IO.equals(focus)) {
            setFocusOnUIComponent(getTypeIo());
        } else if (GOTO_MLP_YN.equals(focus)) {
            setFocusOnUIComponent(getMlpYn());
        } else if (GOTO_DEST_WH_ID.equals(focus)) {
            setFocusOnUIComponent(getDestWhId());
        } else if (GOTO_WRT_LOC.equals(focus)) {
            setFocusOnUIComponent(getWrtLoc());
        } else if (GOTO_SEAL_NBR.equals(focus)) {
            setFocusOnUIComponent(getSealNbr());
        }
    }

    private boolean validatePartialEntry() {
        if (isValidationError())
            return false; //return if attribute validation error already exists.
        hideMessagePanel();

        //Trailer
        setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), false);
        Object trailerId = ADFUtils.getBoundAttributeValue(ATTR_TRAILER_ID);
        if (trailerId == null || "".equals(trailerId)) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
            return false;
        }

        //Door
        setErrorStyleOnComponent(getDoor(), getDoorIcon(), false);
        Object door = ADFUtils.getBoundAttributeValue(ATTR_DOOR);
        if (door == null) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getDoor(), getDoorIcon(), true);
            return false;
        }

        //Type IO
        setErrorStyleOnComponent(getTypeIo(), getTypeIoIcon(), false);
        Object typeio = ADFUtils.getBoundAttributeValue(ATTR_TYPE_IO);
        if (typeio == null) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getTypeIo(), getTypeIoIcon(), true);
            return false;
        }

        return true;
    }

    public void f5OpenListener(ActionEvent actionEvent) {
        if (!doKeyNextItem())
            return;

        if (validatePartialEntry()) {
            OperationBinding ob = ADFUtils.findOperation(OPR_OPEN_TRAILER);
            Map resultVal = (Map) ob.execute();
            if (resultVal != null) {
                String msgCode = (String) resultVal.get("ErrorCode");
                String msgMesg = (String) resultVal.get("ErrorMesg");
                if (msgMesg != null) {
                    showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                      MSG_TYPE_E, msgMesg);
                    getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                    return;
                }
                Object gotoitem = resultVal.get("GotoField");
                if (msgCode != null) {
                    if (MSG_SUCCESS_OPER.equals(msgCode)) {
                        getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(),
                                                getAllMessagesPanel(), SUCCESS, msgCode);
                        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                    } else {
                        getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(),
                                                getAllMessagesPanel(), MSG_TYPE_E, msgCode);
                        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                        if (gotoitem != null) {
                            if (BigDecimal.valueOf(2).equals(gotoitem)) {
                                getShuttleTrailerBean().setIsFocusOn(GOTO_DOOR);
                                setErrorStyleOnComponent(getDoor(), getDoorIcon(), true);
                            } else if (BigDecimal.valueOf(5).equals(gotoitem)) {
                                getShuttleTrailerBean().setIsFocusOn(GOTO_DEST_WH_ID);
                                setErrorStyleOnComponent(getDestWhId(), getDestWhIdIcon(), true);
                            }
                        }
                    }
                    return;
                }
            }
        }
    }

    public void f6ResetListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_shuttle_trailer_s_SHUTTLE_TRAILER_F6,RoleBasedAccessConstants.FORM_NAME_hh_shuttle_trailer_s)){
                 if (!doKeyNextItem())
            return;

        OperationBinding ob = ADFUtils.findOperation(OPR_RESET_TRAILER);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            String msgMesg = (String) resultVal.get("ErrorMesg");
            if (msgMesg != null) {
                showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                  msgMesg);
            }
            if (msgCode != null) {
                if (MSG_SUCCESS_OPER.equals(msgCode)) {
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            SUCCESS, msgCode);
                    ADFUtils.setBoundAttributeValue("Door", null);
                    ADFUtils.setBoundAttributeValue("TypeIo", null);
                    ADFUtils.setBoundAttributeValue("MlpYn", null);
                    ADFUtils.setBoundAttributeValue("MlpCount", null);
                    refreshContentOfUIComponent(getPanelForm());
                    
                    getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                } else {
                    setErrorOnFocusedField();
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            MSG_TYPE_E, msgCode);
                }
                return;
            }
        }
        }else{
            showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,getMessage("NOT_ALLOWED",MSG_TYPE_E));
             }
    }

    public void f7LoadListener(ActionEvent actionEvent) {
      
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_shuttle_trailer_s_SHUTTLE_LOAD_WAVED_CONT,RoleBasedAccessConstants.FORM_NAME_hh_shuttle_trailer_s)){
                   if (!doKeyNextItem())
                       return;

                   OperationBinding ob = ADFUtils.findOperation(OPR_LOAD_TRAILER);
                   Map resultVal = (Map) ob.execute();
                   if (resultVal != null) {
                       String msgCode = (String) resultVal.get("ErrorCode");
                       String msgMesg = (String) resultVal.get("ErrorMesg");
                       BigDecimal gotoitem = (BigDecimal) resultVal.get("GotoField");
                       if (msgMesg != null) {
                           showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                             msgMesg);
                           return;
                       }
                       if (msgCode != null) {
                           getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                                   MSG_TYPE_E, msgCode);
                           if (gotoitem != null) {
                               int gotointval = ((BigDecimal) gotoitem).intValue();
                               if (gotointval == 1) {
                                   getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                                   setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
                               } else if (gotointval == 2) {
                                   getShuttleTrailerBean().setIsFocusOn(GOTO_DOOR);
                                   setErrorStyleOnComponent(getDoor(), getDoorIcon(), true);
                               } else if (gotointval == 4) {
                                   getShuttleTrailerBean().setIsFocusOn(GOTO_WRT_LOC);
                                   setErrorStyleOnComponent(getWrtLoc(), getWrtLocIcon(), true);
                               }
                           }
                           return;
                       }
                       if (gotoitem != null && BigDecimal.TEN.equals(gotoitem)) {
                           getShuttleTrailerBean().setIsFocusOn(GOTO_CID);
                           //NS: Before navigating set the mlpCount.
                           //This is different from Forms where it is set on exit from LOAD_TRAILER
                           OperationBinding ob2 = ADFUtils.findOperation(OPR_SET_MLP_COUNT);
                           ob2.execute();
                           ADFUtils.invokeAction("loadTrailer");
                       }
                   } 
               }else{
                   showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,getMessage("NOT_ALLOWED",MSG_TYPE_E));
                     }
    }

    public void f8MoveListener(ActionEvent actionEvent) {
        hideMessagePanel();
        OperationBinding ob = ADFUtils.findOperation(OPR_CHECK_SCREEN_PRIV);
        ob.getParamsMap().put("formName", FORM_NAME_MOVE_INV);
        ob.getParamsMap().put("optionName", FORM_NAME_MOVE_INV);

        String enabled = (String) ob.execute();

        if (OFF.equals(enabled)) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_ACCESS_DENIED);
            setValidationError(true);
            return;
        }
        OperationBinding ob1 = ADFUtils.findOperation(OPR_SAVE_LABOR_PROD);
        Map resultVal = (Map) ob1.execute();
        if (resultVal != null) {
            List<String> msgMesg = (List<String>) resultVal.get("ErrorMessage");
            if (msgMesg != null && !msgMesg.isEmpty()) {
                showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), msgMesg.get(0),
                                  msgMesg.get(1));
                return;
            }
        }
        //Success so far, navigate to move inventory
        ADFUtils.invokeAction("moveInventory");
    }

    public void f9CloseListener(ActionEvent actionEvent) {
     if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_shuttle_trailer_s_SHUTTLE_TRAILER_F9,RoleBasedAccessConstants.FORM_NAME_hh_shuttle_trailer_s)){
         
        if (!doKeyNextItem())
            return;

        OperationBinding ob = ADFUtils.findOperation(OPR_CLOSE_TRAILER_ERR);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            String message = (String) resultVal.get("ErrorMesg");
            BigDecimal gotoField = (BigDecimal) resultVal.get("GotoField");
            BigDecimal builtins = (BigDecimal) resultVal.get("BuiltIns");
            if (builtins != null && 99 == builtins.intValue()) {
                //show question popup
                getEmptyTrailerPopup().show(new RichPopup.PopupHints());
            }
            if (msgCode != null && message != null) {
                if (gotoField != null && gotoField.intValue() == 6) { //show message in popup not panel. On ok of popup move to yard
                    setConfirmMessage(message);
                    getConfirmPopup().show(new RichPopup.PopupHints());
                    return;
                } else
                    showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), msgCode,
                                      message);
            }
            if (msgCode != null && message == null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
            }
            if (msgCode != null || message != null) {
                setValidationError(true);
                if (gotoField != null && gotoField.intValue() == 1) {
                    getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                    setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
                } else if (gotoField != null && gotoField.intValue() == 2) {
                    getShuttleTrailerBean().setIsFocusOn(GOTO_TYPE_IO);
                    setErrorStyleOnComponent(getTypeIo(), getTypeIoIcon(), true);
                } else if (gotoField != null && gotoField.intValue() == 3) {
                    getShuttleTrailerBean().setIsFocusOn(GOTO_DOOR);
                    setErrorStyleOnComponent(getDoor(), getDoorIcon(), true);
                } else if (gotoField != null && gotoField.intValue() == 4) {
                    getShuttleTrailerBean().setIsFocusOn(GOTO_DEST_WH_ID);
                    setErrorStyleOnComponent(getDestWhId(), getDestWhIdIcon(), true);
                } else if (gotoField != null && gotoField.intValue() == 5) {
                    getShuttleTrailerBean().setIsFocusOn(GOTO_SEAL_NBR);
                    setErrorStyleOnComponent(getSealNbr(), getSealNbrIcon(), true);
                }
                return;
            }

            if (gotoField != null && gotoField.intValue() == 6) {
                getShuttleTrailerBean().setIsFocusOn(GOTO_ZONE);
                ADFUtils.invokeAction("moveToYard");
            } else if (gotoField != null && gotoField.intValue() == 7) {
                getShuttleTrailerBean().setIsFocusOn(GOTO_QUEUE);
                ADFUtils.invokeAction("printBol");
            }

        }
        }else{
         showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,getMessage("NOT_ALLOWED",MSG_TYPE_E));
        
        }
    }

    public String getTrailerEmptyMessage() {
        return getMessage(MSG_TRAILER_EMPTY, MSG_TYPE_W);
    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateTrailerHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateTrailer() {
        setValidationError(false);
        setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), false);
        hideMessagePanel();
        OperationBinding ob = ADFUtils.findOperation(OPR_TRAILER_KEY_NEXT);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
                setValidationError(true);
                return;
            }

            String gotoField = (String) resultVal.get("GotoField");
            if (gotoField != null) {
                getShuttleTrailerBean().setIsFocusOn(gotoField);
            }

            Boolean manifoldFound = (Boolean) resultVal.get("ManifoldFound");
            if (manifoldFound) {
                Object typeIO = resultVal.get("TypeIO");
                if (INFO.equals(typeIO))
                    getShuttleTrailerBean().setIsFocusOn(GOTO_WRT_LOC);
                else
                    getShuttleTrailerBean().setIsFocusOn(GOTO_DEST_WH_ID);
            } else {
                getShuttleTrailerBean().setIsFocusOn(GOTO_DOOR);
            }
            //getShuttleTrailerBean().setManifoldFound(manifoldFound);
        }
    }

    public void onChangedDoor(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateDoorHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateDoor() {
        setValidationError(false);
        setErrorStyleOnComponent(getDoor(), getDoorIcon(), false);
        hideMessagePanel();
        OperationBinding ob = ADFUtils.findOperation(OPR_DOOR_KEY_NEXT);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                String gotoField = (String) resultVal.get("GotoField");
                if (GOTO_TRAILER_ID.equals(gotoField)) {
                    getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                    setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
                } else {
                    setErrorStyleOnComponent(getDoor(), getDoorIcon(), true);
                }
                setValidationError(true);
                return;
            } else {
                getShuttleTrailerBean().setIsFocusOn(GOTO_TYPE_IO);
            }
        }
    }

    public void onChangedTypeIo(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateTypeHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateType() {
        setValidationError(false);
        setErrorStyleOnComponent(getTypeIo(), getTypeIoIcon(), false);
        hideMessagePanel();
        OperationBinding ob = ADFUtils.findOperation(OPR_TYPE_IO_KEY_NEXT);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                setErrorStyleOnComponent(getTypeIo(), getTypeIoIcon(), true);
                setValidationError(true);
                return;
            }
            String gotoField = (String) resultVal.get("GotoField");
            if (gotoField != null) {
                getShuttleTrailerBean().setIsFocusOn(gotoField);
            } else
                getShuttleTrailerBean().setIsFocusOn(GOTO_MLP_YN);
        }
    }

    public void onChangedMlpYn(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateMlpHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateMlp() {
        setValidationError(false);
        setErrorStyleOnComponent(getMlpYn(), getMlpYnIcon(), false);
        hideMessagePanel();
        Object mlpYn = ADFUtils.getBoundAttributeValue(ATTR_MLP_YN);
        if (mlpYn == null || "".equals(mlpYn)) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_PARTIAL_ENTRY);
            setErrorStyleOnComponent(getMlpYn(), getMlpYnIcon(), true);
            setValidationError(true);
            return;
        } else if (!YES.equals(mlpYn) && !NO.equals(mlpYn)) {
            getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                    MSG_TYPE_E, MSG_INV_FLAG);
            setErrorStyleOnComponent(getMlpYn(), getMlpYnIcon(), true);
            setValidationError(true);
            return;
        }
        getShuttleTrailerBean().setIsFocusOn(GOTO_DEST_WH_ID);
    }

    public void onChangedDestWhId(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateDestHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateDest() {
        setValidationError(false);
        resetErrorStyleOnComponent(getDestWhId(), getDestWhIdIcon());
        hideMessagePanel();
        if (ADFUtils.getBoundAttributeValue(ATTR_DEST_WH_ID) == null) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_WRT_LOC);
            return;
        }
        OperationBinding ob = ADFUtils.findOperation(OPR_DEST_WHID_KEY_NEXT);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                setErrorStyleOnComponent(getDestWhId(), getDestWhIdIcon(), true);
                setValidationError(true);
                return;
            }

            getShuttleTrailerBean().setIsFocusOn(GOTO_WRT_LOC);
        }
    }

    public void onChangedWrtLoc(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateWrtHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateWrt() {
        setValidationError(false);
        resetErrorStyleOnComponent(getWrtLoc(), getWrtLocIcon());
        hideMessagePanel();
        if (ADFUtils.getBoundAttributeValue(ATTR_WRT_LOC) == null) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_SEAL_NBR);
            return;
        }
        OperationBinding ob = ADFUtils.findOperation(OPR_WRT_LOC_KEY_NEXT);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            if (msgCode != null) {
                getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                        MSG_TYPE_E, msgCode);
                setErrorStyleOnComponent(getWrtLoc(), getWrtLocIcon(), true);
                setValidationError(true);
                return;
            }
        }
        getShuttleTrailerBean().setIsFocusOn(GOTO_SEAL_NBR);
    }

    public void onChangeSealNbr(ValueChangeEvent vce) {
        if (!this.isNavigationExecuted()) {
            if (vce != null) {
                updateModel(vce);
            }
            getShuttleTrailerBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateSealHiddenLink());
            actionEvent.queue();
        }
    }

    private void validateSeal() {
        setValidationError(false);
        resetErrorStyleOnComponent(getSealNbr(), getSealNbrIcon());
        hideMessagePanel();
        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF9Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    private void queueValueChangeEvent(RichInputText field, String submittedVal) {
        switch (field.getId()) {
        case "tra":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangedTrailerId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "doo":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangedDoor(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "typ":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangedTypeIo(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "mlp":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangedMlpYn(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "des":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangedDestWhId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "wrt1":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangedWrtLoc(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "sea1":
            if (valueNotChanged(submittedVal, field.getValue())) {
                onChangeSealNbr(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        default:

        }
    }


    public String trailerEmptyYesAction() {
        return trailerEmptyListener(YES);
    }

    public String trailerEmptyNoAction() {
        return trailerEmptyListener(NO);
    }

    private String trailerEmptyListener(String vTrailerMsg) {
        String nav = null;
        getEmptyTrailerPopup().hide();
        //close_trailer_type_I
        OperationBinding ob = ADFUtils.findOperation(OPR_CLOSE_TRAILER_I);
        ob.getParamsMap().put("message", vTrailerMsg);
        Map resultVal = (Map) ob.execute();
        List<String> msgMesg = (List<String>) resultVal.get("ErrorMessage");
        if (msgMesg != null && !msgMesg.isEmpty()) {
            showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), msgMesg.get(0),
                              msgMesg.get(1));
            return nav;
        }

        BigDecimal gotoField = (BigDecimal) resultVal.get("GotoField");
        if (gotoField != null && gotoField.intValue() == 1) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
            //setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
            refreshContentOfUIComponent(getPanelForm());
        } else if (gotoField != null && gotoField.intValue() == 6) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_ZONE);
            nav = "moveToYard";
        }
        return nav;
    }

    public void setDoorIcon(RichIcon doorIcon) {
        this.doorIcon = doorIcon;
    }

    public RichIcon getDoorIcon() {
        return doorIcon;
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setTypeIoIcon(RichIcon typeIoIcon) {
        this.typeIoIcon = typeIoIcon;
    }

    public RichIcon getTypeIoIcon() {
        return typeIoIcon;
    }

    public void setMlpYnIcon(RichIcon mlpYnIcon) {
        this.mlpYnIcon = mlpYnIcon;
    }

    public RichIcon getMlpYnIcon() {
        return mlpYnIcon;
    }


    public void setWrtLocIcon(RichIcon wrtLocIcon) {
        this.wrtLocIcon = wrtLocIcon;
    }

    public RichIcon getWrtLocIcon() {
        return wrtLocIcon;
    }

    public void setSealNbrIcon(RichIcon sealNbrIcon) {
        this.sealNbrIcon = sealNbrIcon;
    }

    public RichIcon getSealNbrIcon() {
        return sealNbrIcon;
    }


    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setDoor(RichInputText door) {
        this.door = door;
    }

    public RichInputText getDoor() {
        return door;
    }

    public void setTypeIo(RichInputText typeIo) {
        this.typeIo = typeIo;
    }

    public RichInputText getTypeIo() {
        return typeIo;
    }

    public void setMlpYn(RichInputText mlpYn) {
        this.mlpYn = mlpYn;
    }

    public RichInputText getMlpYn() {
        return mlpYn;
    }

    public void setDestWhIdIcon(RichIcon destWhIdIcon) {
        this.destWhIdIcon = destWhIdIcon;
    }

    public RichIcon getDestWhIdIcon() {
        return destWhIdIcon;
    }

    public void setDestWhId(RichInputText destWhId) {
        this.destWhId = destWhId;
    }

    public RichInputText getDestWhId() {
        return destWhId;
    }

    public void setWrtLoc(RichInputText wrtLoc) {
        this.wrtLoc = wrtLoc;
    }

    public RichInputText getWrtLoc() {
        return wrtLoc;
    }

    public void setSealNbr(RichInputText sealNbr) {
        this.sealNbr = sealNbr;
    }

    public RichInputText getSealNbr() {
        return sealNbr;
    }


    public void setEmptyTrailerPopup(RichPopup emptyTrailerPopup) {
        this.emptyTrailerPopup = emptyTrailerPopup;
    }

    public RichPopup getEmptyTrailerPopup() {
        return emptyTrailerPopup;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    public void setF9Link(RichLink f9Link) {
        this.f9Link = f9Link;
    }

    public RichLink getF9Link() {
        return f9Link;
    }

    private void setErrorOnFocusedField() {
        String focus = getShuttleTrailerBean().getIsFocusOn();
        if (focus == null || GOTO_TRAILER_ID.equals(focus)) {
            if (focus == null)
                getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
            setErrorStyleOnComponent(getTrailerId(), getTrailerIdIcon(), true);
        } else if (GOTO_DOOR.equals(focus)) {
            setErrorStyleOnComponent(getDoor(), getDoorIcon(), true);
        } else if (GOTO_TYPE_IO.equals(focus)) {
            setErrorStyleOnComponent(getTypeIo(), getTypeIoIcon(), true);
        } else if (GOTO_MLP_YN.equals(focus)) {
            setErrorStyleOnComponent(getMlpYn(), getMlpYnIcon(), true);
        } else if (GOTO_DEST_WH_ID.equals(focus)) {
            setErrorStyleOnComponent(getDestWhId(), getDestWhIdIcon(), true);
        } else if (GOTO_WRT_LOC.equals(focus)) {
            setErrorStyleOnComponent(getWrtLoc(), getWrtLocIcon(), true);
        } else if (GOTO_SEAL_NBR.equals(focus)) {
            setErrorStyleOnComponent(getSealNbr(), getSealNbrIcon(), true);
        }
    }

    private boolean doKeyNextItem() {
        String focus = getShuttleTrailerBean().getIsFocusOn();
        if (focus == null || GOTO_TRAILER_ID.equals(focus)) {
            if (focus == null)
                getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
            validateTrailer();
        } else if (GOTO_DOOR.equals(focus)) {
            validateDoor();
        } else if (GOTO_TYPE_IO.equals(focus)) {
            validateType();
        } else if (GOTO_MLP_YN.equals(focus)) {
            validateMlp();
        } else if (GOTO_DEST_WH_ID.equals(focus)) {
            validateDest();
        } else if (GOTO_WRT_LOC.equals(focus)) {
            validateWrt();
        } else if (GOTO_SEAL_NBR.equals(focus)) {
            validateSeal();
        }
        return !isValidationError();
    }

    public void setValidateTrailerHiddenLink(RichLink validateTrailerHiddenLink) {
        this.validateTrailerHiddenLink = validateTrailerHiddenLink;
    }

    public RichLink getValidateTrailerHiddenLink() {
        return validateTrailerHiddenLink;
    }

    public void setValidateDoorHiddenLink(RichLink validateDoorHiddenLink) {
        this.validateDoorHiddenLink = validateDoorHiddenLink;
    }

    public RichLink getValidateDoorHiddenLink() {
        return validateDoorHiddenLink;
    }

    public void setValidateTypeHiddenLink(RichLink validateTypeHiddenLink) {
        this.validateTypeHiddenLink = validateTypeHiddenLink;
    }

    public RichLink getValidateTypeHiddenLink() {
        return validateTypeHiddenLink;
    }

    public void setValidateMlpHiddenLink(RichLink validateMlpHiddenLink) {
        this.validateMlpHiddenLink = validateMlpHiddenLink;
    }

    public RichLink getValidateMlpHiddenLink() {
        return validateMlpHiddenLink;
    }

    public void setValidateDestHiddenLink(RichLink validateDestHiddenLink) {
        this.validateDestHiddenLink = validateDestHiddenLink;
    }

    public RichLink getValidateDestHiddenLink() {
        return validateDestHiddenLink;
    }

    public void setValidateWrtHiddenLink(RichLink validateWrtHiddenLink) {
        this.validateWrtHiddenLink = validateWrtHiddenLink;
    }

    public RichLink getValidateWrtHiddenLink() {
        return validateWrtHiddenLink;
    }

    public void setValidateSealHiddenLink(RichLink validateSealHiddenLink) {
        this.validateSealHiddenLink = validateSealHiddenLink;
    }

    public RichLink getValidateSealHiddenLink() {
        return validateSealHiddenLink;
    }

    public String validateTrailerHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateTrailer();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public String validateDoorHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateDoor();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public String validateTypeHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateType();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public String validateMlpHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateMlp();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public String validateDestHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateDest();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public String validateWrtHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateWrt();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public String validateSealHiddenAction() {
        if (!this.isNavigationExecuted()) {
            if (!getShuttleTrailerBean().getExecutedKey()) {
                validateSeal();
            }
            getShuttleTrailerBean().setExecutedKey(null);
        }
        return null;
    }

    public void setPanelForm(RichPanelFormLayout panelForm) {
        this.panelForm = panelForm;
    }

    public RichPanelFormLayout getPanelForm() {
        return panelForm;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_trailermgmt_hhshuttletrailers_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    @Override
    public void setFocusOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("setFocusOrSelectOnUIcomp(textInput);"); //changed selectFocusOnComp to selectFocusonUIComp fo IE using jquery
        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmMessage(String confirmMessage) {
        this.confirmMessage = confirmMessage;
    }

    public String getConfirmMessage() {
        return confirmMessage;
    }

    public String confirmPopupAction() {
        getShuttleTrailerBean().setIsFocusOn(GOTO_ZONE);
        ADFUtils.invokeAction("moveToYard");
        return null;
    }
}
