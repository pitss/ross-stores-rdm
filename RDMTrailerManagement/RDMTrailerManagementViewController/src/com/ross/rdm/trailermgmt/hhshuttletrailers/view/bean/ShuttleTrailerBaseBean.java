package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.commons.lang.StringUtils;

public class ShuttleTrailerBaseBean extends RDMTrailerManagementBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    
    static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    static final String ERROR = "E";
    static final String WARN = "W";
    static final String INFO = "I";
    static final String CONF = "C";
    static final String SUCCESS = "S";
    
    static final String OPR_DISPLAY_FORM = "displayForm"; 
    
    static final String MSG_TYPE_W = "W";
    static final String MSG_TYPE_E = "E";
    static final String MSG_TYPE_I = "I";
    static final String MSG_TYPE_C = "C";
    static final String MSG_TYPE_M = "M";
    
    static final String WARN_ICON  = "warning";
    static final String ERROR_ICON = "error";
    static final String INFO_ICON  = "info";
    static final String REQ_ICON   = "required";
    static final String NO_ICON    = "none";
    static final String SCC_ICON   = "logo";
    
    static final String ATTR_FACILITY_ID= "FacilityId";
    static final String ATTR_LANG_CODE  = "LanguageCode";
    static final String ATTR_TRAILER_ID = "TrailerId";
    static final String ATTR_DOOR       = "Door";
    static final String ATTR_TYPE_IO    = "TypeIo"; 
    static final String ATTR_MLP_YN     = "MlpYn"; 
    static final String ATTR_DEST_WH_ID = "DestWhId";  
    static final String ATTR_WRT_LOC    = "WrtLoc";  
    static final String ATTR_SEAL_NBR   = "SealNbr";  
    static final String ATTR_CID        = "Cid"; 
    static final String ATTR_QTY        = "Qty"; 
    static final String ATTR_LOAD_TYPE  = "LoadType"; 
    static final String ATTR_SUBLOAD_TYPE  = "SubLoadType"; 
    static final String ATTR_ACTIVITY_CD   = "ActivityCode"; 
    static final String ATTR_NUM_COPIES = "NumberOfCopies";
    static final String ATTR_YARD_LOC   = "YardLocation";
    static final String ATTR_YARD_ZONE  = "Zone";
    static final String ATTR_QUEUE      = "Queue"; 
    static final String ATTR_PRINTER    = "PrinterName"; 
    
    
    static final String GOTO_TRAILER_ID = "SHUTTLE_TRAILER.TRAILER_ID";
    static final String GOTO_DOOR       = "SHUTTLE_TRAILER.DOOR";
    static final String GOTO_TYPE_IO    = "SHUTTLE_TRAILER.TYPE_IO"; 
    static final String GOTO_MLP_YN     = "SHUTTLE_TRAILER.MLP_YN"; 
    static final String GOTO_DEST_WH_ID = "SHUTTLE_TRAILER.DEST_WH_ID";  
    static final String GOTO_WRT_LOC    = "SHUTTLE_TRAILER.WRT_LOC";  
    static final String GOTO_SEAL_NBR   = "SHUTTLE_TRAILER.SEAL_NBR";  
    
    static final String GOTO_CID        = "LOAD_TRAILER.CID"; 
    static final String GOTO_ZONE       = "MOVE_TO_YARD.ZONE"; 
    static final String GOTO_YARD_LOC   = "MOVE_TO_YARD.YARD_LOCATION"; 
    static final String GOTO_QUEUE      = "PRINT_BOL.QUEUE"; 
    static final String GOTO_NUM_COPIES = "PRINT_BOL.NUMBER_OF_COPIES";
    
    static final String GOTO_POPUP = "POPUP";
    
    static final String MSG_PARTIAL_ENTRY = "PARTIAL_ENTRY";
    static final String MSG_INV_FLAG      = "INV_FLAG";
    static final String MSG_INV_VALUE     = "INV_VALUE";
    static final String MSG_REQ_DEST_WH   = "REQ_DEST_WH";
    static final String MSG_ACCESS_DENIED = "ACCESS_DENIED";
    static final String MSG_SUCCESS_OPER  = "SUCCESS_OPER";
    static final String MSG_TRAILER_EMPTY = "TRAILER_EMPTY";
    static final String MSG_CONT_LD_CHNG  = "CONT_LD_CHNG";
    
    static final String VAL_MIXED = "MIXED";
    static final String VAL_PKMIX = "PKMIX";
    static final String VAL_PK    = "PK";
    static final String VAL_CNTLDG= "CNTLDG";
     
    static final String YES = "Y";
    static final String NO  = "N";
     
    
    private  static final String HH_SHUTTLE_TRAILER_BEAN = "HhShuttleTrailerSBean";                                                            
    public ShuttleTrailerBaseBean() {
        super();
    }   
    
    static HhShuttleTrailerSBean getShuttleTrailerBean() {
        Object bean = AdfFacesContext.getCurrentInstance().getPageFlowScope().get(HH_SHUTTLE_TRAILER_BEAN ); 
        if (bean != null)
          return (HhShuttleTrailerSBean)bean;
        else
          return null;
    }

    void getAndShowMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon, RichPanelGroupLayout allMessagesPanel,
                                 String messageType, String msgCode ) {
        String msg =   getMessage(msgCode, messageType);
        showMessagesPanel(errorWarnInfoMessage ,errorWarnInfoIcon ,allMessagesPanel, messageType, msg);
    }
    String getMessage(String msgCode, String messageType){
        return   getMessage(msgCode, messageType, getBoundAttribute(ATTR_FACILITY_ID), getBoundAttribute(ATTR_LANG_CODE));          
    }
    void showMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon, RichPanelGroupLayout allMessagesPanel, String messageType, String msg) {
       // CLEAR PREVIOUS MESSAGES
       errorWarnInfoMessage.setValue(null);
       errorWarnInfoIcon.setName(null);
       allMessagesPanel.setVisible(false);

       if (WARN.equals(messageType)) {
           errorWarnInfoIcon.setName(WARN_ICON);
       } else if (INFO.equals(messageType) || MSG_TYPE_M.equals(messageType)) {
           errorWarnInfoIcon.setName(INFO_ICON);
       } else {
           if (ERROR.equals(messageType)) {
               errorWarnInfoIcon.setName(ERROR_ICON);
           } 
           else {
               if (SUCCESS.equals(messageType)) {
                   errorWarnInfoIcon.setName(SCC_ICON);
               }
           }
       } 
       errorWarnInfoMessage.setValue(msg);
       allMessagesPanel.setVisible(true);
       refreshContentOfUIComponent(allMessagesPanel);
    }
    
    private String getBoundAttribute(String attrName){
       Object val = ADFUtils.getBoundAttributeValue(attrName);
       if (val != null)
           return (String)val;
       return null;
    }
    
    public void hideMessagePanel(){
        errorWarnInfoIcon.setName(null);
        errorWarnInfoMessage.setValue(null);
        refreshContentOfUIComponent(allMessagesPanel);
    } 
    
    
    void resetErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon){
      icon.setName(NO_ICON); 
      removeErrorStyleToComponent(uiComponent);
      
       AdfFacesContext.getCurrentInstance().addPartialTarget(icon);
       AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }
    void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set){
       if (set){
           addErrorStyleToComponent(uiComponent); 
           icon.setName(ERROR_ICON); 
       }
       else {
           icon.setName(REQ_ICON); 
           removeErrorStyleToComponent(uiComponent);
       }
       AdfFacesContext.getCurrentInstance().addPartialTarget(icon);
       AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }
    
    public void setValidationError(boolean validationError) {
     ADFContext.getCurrent().getViewScope().put("ValidationError", validationError);

    }

    public boolean isValidationError() {
        Object valErr = ADFContext.getCurrent().getViewScope().get("ValidationError");
        if (valErr == null) return false;
        
        return (Boolean)valErr;
    }
    
    
    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
      this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel()  {
      return allMessagesPanel;
    }
    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
      this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon()  {
      return errorWarnInfoIcon;
    }
    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
      this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage()  {
      return errorWarnInfoMessage;
    }    
    
    protected boolean valueNotChanged(String submittedVal, Object fieldValue) {
        if (fieldValue instanceof String) {
            return (StringUtils.isEmpty((String) fieldValue) && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase((String) fieldValue);
        } else {
            return (fieldValue == null && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase(fieldValue == null ? "" : fieldValue.toString());
        }
    }
}
