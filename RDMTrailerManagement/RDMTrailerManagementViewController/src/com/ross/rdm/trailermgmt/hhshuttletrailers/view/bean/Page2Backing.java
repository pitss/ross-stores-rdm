package com.ross.rdm.trailermgmt.hhshuttletrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;

import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends ShuttleTrailerBaseBean {

    private static final String MSG_PENDING_TASK = "PENDING_TASK";
    private RichIcon cidIcon;
    private static final String OPR_PROCESS_MANIFEST_SUBLOAD = "processManifestSubload";
    private static final String OPR_LOAD_TO_TRAILER = "loadToTrailer";
    private static final String OPR_CID_KEY_NEXT = "cidKeyNext";
    private static final String OPR_CID_KEY_NEXT2 = "cidKeyNext2";
    private static final String OPR_PROCESS_LOAD = "processLoad";
    private RichPopup cidChangePopup;
    private RichPopup pendingTaskPopup;
    private RichPopup successOperPopup;
    private RichInputText cid;
    private RichLink exitLink;
    private RichLink doneLink;

    public Page2Backing() {

    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        String focus = getShuttleTrailerBean().getIsFocusOn();
        if (focus == null || GOTO_CID.equals(focus)) {
            if (focus == null)
                getShuttleTrailerBean().setIsFocusOn(GOTO_CID);
            setFocusOnUIComponent(getCid());
        }
    }

    public void onChangedCid(ValueChangeEvent vce) {
        setValidationError(false);
        getShuttleTrailerBean().setPrevLoadType(null);
        getShuttleTrailerBean().setPrevSubLoadType(null);
        hideMessagePanel();
        setErrorStyleOnComponent(getCid(), getCidIcon(), false);

        this.updateModel(vce);
        OperationBinding ob = ADFUtils.findOperation(OPR_CID_KEY_NEXT);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgCode = (String) resultVal.get("ErrorCode");
            if (msgCode != null) {
                if (!MSG_PENDING_TASK.equals(msgCode)) {
                    setValidationError(true);
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            MSG_TYPE_E, msgCode);
                    setErrorStyleOnComponent(getCid(), getCidIcon(), true);
                    //ADFUtils.setBoundAttributeValue(ATTR_CID, null);
                    return;
                } else {
                    //show popup PENDING_TASK
                    disableCidField(true);
                    getPendingTaskPopup().show(new RichPopup.PopupHints());
                    return;
                }
            }
            getShuttleTrailerBean().setPrevLoadType((String) resultVal.get("prevLoadType"));
            getShuttleTrailerBean().setPrevSubLoadType((String) resultVal.get("prevSubLoadType"));
            checkShowPopupCntLdChng();
        }
    }

    private void checkShowPopupCntLdChng() {
        String prevLoadType = getShuttleTrailerBean().getPrevLoadType();
        Object loadType = ADFUtils.getBoundAttributeValue(ATTR_LOAD_TYPE);
        if (prevLoadType != null && !prevLoadType.equals(loadType)) {
            //show popup CONT_LD_CHNG
            disableCidField(true);
            getCidChangePopup().show(new RichPopup.PopupHints());
            return;
        } else {
            processLoad();
        }
    }

    public void cidChangeYesListener(ActionEvent actionEvent) {
        cidChangeListener(YES);
    }

    public void cidChangeNoListener(ActionEvent actionEvent) {
        cidChangeListener(NO);
    }

    public void cidChangeListener(String response) {
        getCidChangePopup().hide();
        disableCidField(false);
        if (YES.equals(response)) {
            Object loadType = ADFUtils.getBoundAttributeValue(ATTR_LOAD_TYPE);
            Object prevLoadType = getShuttleTrailerBean().getPrevLoadType();
            if (loadType != null && VAL_PK.equals(loadType.toString().substring(0, 2)) && prevLoadType != null &&
                VAL_PK.equals(prevLoadType.toString().substring(0, 2)))
                ADFUtils.setBoundAttributeValue(ATTR_LOAD_TYPE, VAL_PKMIX);
            else
                ADFUtils.setBoundAttributeValue(ATTR_LOAD_TYPE, VAL_MIXED);

            processLoad();
        } else {
            ADFUtils.setBoundAttributeValue(ATTR_LOAD_TYPE, getShuttleTrailerBean().getPrevLoadType());
            ADFUtils.setBoundAttributeValue(ATTR_SUBLOAD_TYPE, getShuttleTrailerBean().getPrevSubLoadType());
            ADFUtils.setBoundAttributeValue(ATTR_CID, null);
            return;
        }
    }

    private void processLoad() {
        Object subLoadType = ADFUtils.getBoundAttributeValue(ATTR_SUBLOAD_TYPE);
        String prevSubLoadType = getShuttleTrailerBean().getPrevSubLoadType();
        if (prevSubLoadType != null && !prevSubLoadType.equals(subLoadType)) {
            if (VAL_MIXED.equals(subLoadType))
                ADFUtils.setBoundAttributeValue(ATTR_SUBLOAD_TYPE, VAL_MIXED);
        }
        //Continue with method call processLoad
        OperationBinding ob = ADFUtils.findOperation(OPR_PROCESS_LOAD);
        Map resultVal = (Map) ob.execute();
        String msgMesg = (String) resultVal.get("ErrorMesg");
        if (msgMesg != null) {
            showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                              msgMesg);
            return;
        }
        ADFUtils.setBoundAttributeValue(ATTR_CID, null);
        setFocusOnUIComponent(getCid());
        ADFUtils.setBoundAttributeValue(ATTR_ACTIVITY_CD, VAL_CNTLDG);
    }


    public void setCidIcon(RichIcon cidIcon) {
        this.cidIcon = cidIcon;
    }

    public RichIcon getCidIcon() {
        return cidIcon;
    }

    public void f4DoneListener(ActionEvent event) {
        OperationBinding ob = ADFUtils.findOperation(OPR_LOAD_TO_TRAILER);
        Map resultVal = (Map) ob.execute();
        if (resultVal != null) {
            String msgMesg = (String) resultVal.get("ErrorMesg");
            if (msgMesg != null) {
                showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(), MSG_TYPE_E,
                                  msgMesg);
                return;
            }
            BigDecimal builtins = (BigDecimal) resultVal.get("BuiltIns");
            if (builtins != null && 1 == builtins.intValue()) {
                getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
                refreshContentOfUIComponent(getCid());
                getSuccessOperPopup().show(new RichPopup.PopupHints());
            }
        }
    }

    public void f3ExitListener(ActionEvent event) {
        ADFUtils.setBoundAttributeValue(ATTR_CID, null);
        ADFUtils.setBoundAttributeValue(ATTR_QTY, null);
        Object wrtLoc = ADFUtils.getBoundAttributeValue(ATTR_WRT_LOC);
        if (wrtLoc == null) {
            OperationBinding ob = ADFUtils.findOperation(OPR_PROCESS_MANIFEST_SUBLOAD);
            Map resultVal = (Map) ob.execute();
            if (resultVal != null) {
                String msgMesg = (String) resultVal.get("ErrorMesg");
                if (msgMesg != null) {
                    showMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                      MSG_TYPE_E, msgMesg);
                    return;
                }
            }
        }
        getShuttleTrailerBean().setIsFocusOn(GOTO_TRAILER_ID);
        ADFUtils.invokeAction("return");
    }

    public String getCidChangeMessage() {
        return getMessage(MSG_CONT_LD_CHNG, MSG_TYPE_W);
    }

    public String getPendingTaskMessage() {
        return getMessage(MSG_PENDING_TASK, MSG_TYPE_W);
    }

    public String getSuccessOperMessage() {
        return getMessage(MSG_SUCCESS_OPER, MSG_TYPE_M);
    }

    public void setCidChangePopup(RichPopup cidChangePopup) {
        this.cidChangePopup = cidChangePopup;
    }

    public RichPopup getCidChangePopup() {
        return cidChangePopup;
    }

    public void setCid(RichInputText cid) {
        this.cid = cid;
    }

    public RichInputText getCid() {
        return cid;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                    submittedVal.equals(field.getValue())) {
                    this.onChangedCid(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }

    public void setPendingTaskPopup(RichPopup pendingTaskPopup) {
        this.pendingTaskPopup = pendingTaskPopup;
    }

    public RichPopup getPendingTaskPopup() {
        return pendingTaskPopup;
    }

    public void pendingTaskYesListener(ActionEvent actionEvent) {
        confirmPendingTask(YES);
    }

    public void pendingTaskNoListener(ActionEvent actionEvent) {
        confirmPendingTask(NO);
    }

    private void disableCidField(boolean disable) {
        if (disable) {
            getShuttleTrailerBean().setIsFocusOn(GOTO_POPUP);
            refreshContentOfUIComponent(getCid());
        } else {
            getShuttleTrailerBean().setIsFocusOn(GOTO_CID);
            refreshContentOfUIComponent(getCid());
        }
    }

    private void confirmPendingTask(String response) {
        getPendingTaskPopup().hide();
        disableCidField(false);
        if (YES.equals(response)) {
            getShuttleTrailerBean().setPrevLoadType((String) ADFUtils.getBoundAttributeValue(ATTR_LOAD_TYPE));
            getShuttleTrailerBean().setPrevSubLoadType((String) ADFUtils.getBoundAttributeValue(ATTR_SUBLOAD_TYPE));

            OperationBinding ob = ADFUtils.findOperation(OPR_CID_KEY_NEXT2);
            Map resultVal = (Map) ob.execute();
            if (resultVal != null) {
                String msgCode = (String) resultVal.get("ErrorCode");
                if (msgCode != null) {
                    setValidationError(true);
                    getAndShowMessagesPanel(getErrorWarnInfoMessage(), getErrorWarnInfoIcon(), getAllMessagesPanel(),
                                            MSG_TYPE_E, msgCode);
                    setErrorStyleOnComponent(getCid(), getCidIcon(), true);
                }
            }
            checkShowPopupCntLdChng();
        } else {
            ADFUtils.setBoundAttributeValue("Cid", null);
            setFocusOnUIComponent(getCid());
        }
    }

    public void setSuccessOperPopup(RichPopup successOperPopup) {
        this.successOperPopup = successOperPopup;
    }

    public RichPopup getSuccessOperPopup() {
        return successOperPopup;
    }

    public String confirmSuccessOperAction() {
        ADFUtils.setBoundAttributeValue(ATTR_CID, null);
        ADFUtils.setBoundAttributeValue(ATTR_QTY, null);
        return "return";
    }
}
