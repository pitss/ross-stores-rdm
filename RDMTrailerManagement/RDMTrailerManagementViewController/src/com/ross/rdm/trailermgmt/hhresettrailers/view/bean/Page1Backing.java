package com.ross.rdm.trailermgmt.hhresettrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMTrailerManagementBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichInputText trailerIdBean;

    private static final String ERR_FLAG = "ERR_FLAG";
    private static final String VALUE_Y = "Y";
    private static final String MSG_RTV_TRAILER_REQ = "RTV_TRAILER_REQ";
    private static final String MSG_RTV_SERVICE_REQ = "RTV_SERVICE_REQ";
    private static final String MSG_RTV_ROUTE_REQ = "RTV_ROUTE_REQ";
    private static final String MSG_RTV_DOOR_REQ = "RTV_DOOR_REQ";
    private static final String MSG_PARTIAL_ENTRY = "PARTIAL_ENTRY";
    private static final String MSG_CONFIRM_RESET = "CONFIRM_RESET";
    private static final String MSG_RESET_SUCCESS = "RESET_SUCCESS";

    private static final String MSG_TYPE_W = "W";
    private static final String MSG_TYPE_E = "E";
    private static final String MSG_TYPE_I = "I";
    private static final String MSG_TYPE_C = "C";

    private static final String WARN_ICON = "warning";
    private static final String ERROR_ICON = "error";
    private static final String INFO_ICON = "info";
    private static final String REQ_ICON = "required";

    private static final String ATTR_FACILITY = "FacilityId";
    private static final String ATTR_LANG_CODE = "LanguageCode";
    private static final String ATTR_TRAILER_ID = "TrailerId";
    private static final String ATTR_SERV_CODE = "ServiceCode";
    private static final String ATTR_ROUTE = "Route";
    private static final String ATTR_DOOR_ID = "DoorId";

    private static final String OPR_VALIDATE_TRAILER = "validateTrailer";
    private static final String OPR_VALIDATE_SERVICE = "validateServiceCode";
    private static final String OPR_VALIDATE_ROUTE = "validateRoute";
    private static final String OPR_VALIDATE_DOORID = "validateDoorId";
    private static final String OPR_RESET_TRAILER = "resetTrailer";

    private static final String FOCUS_TRAILER = "TR";
    private static final String FOCUS_SERVICE = "SR";
    private static final String FOCUS_ROUTE = "RT";
    private static final String FOCUS_DOOR = "DR";
    private static final String FOCUS_DUMMY = "DM";
    private static final String FOCUS_ATTR = "FocusAttribute";
    private static final String HH_RESET_TRAILER_BEAN = "HhResetTrailerSBean";

    private RichOutputText confirmResetMsg;
    private RichPopup confirmResetPopup;
    private RichPopup infoSuccessPopup;
    private RichOutputText infoSuccessMsg;
    private RichIcon trailerIdIcon;
    private RichIcon serviceCodeIcon;
    private RichIcon routeIcon;
    private RichIcon doorIdIcon;
    private RichInputText doorIdBean;
    private RichInputText routeBean;
    private RichInputText serviceCodeBean;
    private RichPanelFormLayout pflMain;

    public Page1Backing() {

    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        if (vce.getNewValue() == null || "".equals(vce.getNewValue())) {
            String msg =
                getMessage(MSG_RTV_TRAILER_REQ, MSG_TYPE_W, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
            showMessagePanel(msg, MSG_TYPE_E);
            setErrorStyleOnComponent(getTrailerIdBean(), getTrailerIdIcon(), true);
        } else {
            hideMessagePanel();
            setErrorStyleOnComponent(getTrailerIdBean(), getTrailerIdIcon(), false);
            String lVal = (String) vce.getNewValue();
            if (lVal != null)
                lVal = lVal.toUpperCase();
            ADFUtils.setBoundAttributeValue(ATTR_TRAILER_ID, lVal);
            OperationBinding ob = ADFUtils.findOperation(OPR_VALIDATE_TRAILER);

            Map retunVal = (Map) ob.execute();
            List<String> errors = (List<String>) retunVal.get("ErrorMessage");
            if (errors != null && errors.size() > 0) {
                showMessagePanel(errors.get(1), errors.get(0));
                setErrorStyleOnComponent(getTrailerIdBean(), getTrailerIdIcon(), true);
            } else {
                hideMessagePanel();
                setErrorStyleOnComponent(getTrailerIdBean(), getTrailerIdIcon(), false);
                switchFocusTo(getServiceCodeBean(), FOCUS_SERVICE);
            }
        }

    }

    public void onChangedServiceCode(ValueChangeEvent vce) {
        if (vce.getNewValue() == null || "".equals(vce.getNewValue())) {
            String msg =
                getMessage(MSG_RTV_SERVICE_REQ, MSG_TYPE_W, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
            showMessagePanel(msg, MSG_TYPE_E);
            setErrorStyleOnComponent(getServiceCodeBean(), getServiceCodeIcon(), true);
        } else {
            hideMessagePanel();
            setErrorStyleOnComponent(getServiceCodeBean(), getServiceCodeIcon(), false);
            String lVal = (String) vce.getNewValue();
            if (lVal != null)
                lVal = lVal.toUpperCase();
            ADFUtils.setBoundAttributeValue(ATTR_SERV_CODE, lVal);
            OperationBinding ob = ADFUtils.findOperation(OPR_VALIDATE_SERVICE);

            Map retunVal = (Map) ob.execute();
            List<String> errors = (List<String>) retunVal.get("ErrorMessage");
            if (errors != null && errors.size() > 0) {
                showMessagePanel(errors.get(1), errors.get(0));
                setErrorStyleOnComponent(getServiceCodeBean(), getServiceCodeIcon(), true);
            } else {
                hideMessagePanel();
                setErrorStyleOnComponent(getServiceCodeBean(), getServiceCodeIcon(), false);
                switchFocusTo(getRouteBean(), FOCUS_ROUTE);
            }
        }
    }

    public void onChangedRoute(ValueChangeEvent vce) {
        if (vce.getNewValue() == null || "".equals(vce.getNewValue())) {
            String msg =
                getMessage(MSG_RTV_ROUTE_REQ, MSG_TYPE_W, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
            showMessagePanel(msg, MSG_TYPE_E);
            setErrorStyleOnComponent(getRouteBean(), getRouteIcon(), true);
        } else {

            hideMessagePanel();
            setErrorStyleOnComponent(getRouteBean(), getRouteIcon(), false);
            String lVal = (String) vce.getNewValue();
            if (lVal != null)
                lVal = lVal.toUpperCase();
            ADFUtils.setBoundAttributeValue(ATTR_ROUTE, lVal);
            OperationBinding ob = ADFUtils.findOperation(OPR_VALIDATE_ROUTE);

            Map retunVal = (Map) ob.execute();
            List<String> errors = (List<String>) retunVal.get("ErrorMessage");
            if (errors != null && errors.size() > 0) {
                showMessagePanel(errors.get(1), errors.get(0));
                setErrorStyleOnComponent(getRouteBean(), getRouteIcon(), true);
            } else {
                hideMessagePanel();
                setErrorStyleOnComponent(getRouteBean(), getRouteIcon(), false);
                switchFocusTo(getDoorIdBean(), FOCUS_DOOR);
            }
        }
    }

    public void onChangedDoorId(ValueChangeEvent vce) {
        if (StringUtils.isNotEmpty((String) vce.getNewValue())) {
            hideMessagePanel();
            setErrorStyleOnComponent(getDoorIdBean(), getDoorIdIcon(), false);
            String lVal = (String) vce.getNewValue();
            if (lVal != null)
                lVal = lVal.toUpperCase();
            ADFUtils.setBoundAttributeValue(ATTR_DOOR_ID, lVal);
            OperationBinding ob = ADFUtils.findOperation(OPR_VALIDATE_DOORID);

            Map retunVal = (Map) ob.execute();
            List<String> errors = (List<String>) retunVal.get("ErrorMessage");
            if (errors != null && errors.size() > 0) {
                showMessagePanel(errors.get(1), errors.get(0));
                setErrorStyleOnComponent(getDoorIdBean(), getDoorIdIcon(), true);
            } else {
                hideMessagePanel();
                setErrorStyleOnComponent(getDoorIdBean(), getDoorIdIcon(), false);
                switchFocusTo(getTrailerIdBean(), FOCUS_TRAILER);
            }
        }
    }

    public void confirmResetDialogYes(ActionEvent actionEvent) {
        this.getConfirmResetPopup().hide();
        OperationBinding ob = ADFUtils.findOperation(OPR_RESET_TRAILER);

        Map retunVal = (Map) ob.execute();
        List<String> errors = (List<String>) retunVal.get("ErrorMessage");
        if (errors != null && errors.size() > 0) {
            showMessagePanel(errors.get(1), errors.get(0));
            return;
        } else
            hideMessagePanel();
        //IF Call_Msg_Window('RESET_SUCCESS', 'I') = 'Y' THEN
        String msg =
            getMessage(MSG_RESET_SUCCESS, MSG_TYPE_W, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                       (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
        this.getInfoSuccessMsg().setValue(msg);
        this.getInfoSuccessPopup().show(new RichPopup.PopupHints());
    }

    public void confirmResetDialogNo(ActionEvent actionEvent) {
        this.getConfirmResetPopup().hide();
        refreshContentOfUIComponent(getPflMain());
        getResetTrailerBean().setIsFocusOn(FOCUS_TRAILER);//OTM2838
        /* Object val = ADFContext.getCurrent().getPageFlowScope().get(FOCUS_ATTR);
        if (val != null) {
            getResetTrailerBean().setIsFocusOn((String) val);
        } */
        onRegionLoad(null); //Force focus
    }

    //	EXIT_FORM (NO_VALIDATE);
    public void infoSuccessDialogOk(ActionEvent actionEvent) {
        this.getInfoSuccessPopup().hide();
        ADFUtils.invokeAction("backGlobalHome");
    }

    //CLEAR_BLOCK(NO_VALIDATE);
    public void infoSuccessDialogNo(ActionEvent actionEvent) {
        this.getInfoSuccessPopup().hide();
        ADFUtils.setBoundAttributeValue(ATTR_TRAILER_ID, null);
        ADFUtils.setBoundAttributeValue(ATTR_SERV_CODE, null);
        ADFUtils.setBoundAttributeValue(ATTR_ROUTE, null);
        ADFUtils.setBoundAttributeValue(ATTR_DOOR_ID, null);
        switchFocusTo(getTrailerIdBean(), FOCUS_TRAILER);
    }

    /**
     * invoked on load of the region to set focus on trailer id input
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) {
        if (getResetTrailerBean().getIsFocusOn() == null || FOCUS_TRAILER.equals(getResetTrailerBean().getIsFocusOn()))
            setFocusOnUIComponent(getTrailerIdBean());
        else if (FOCUS_SERVICE.equals(getResetTrailerBean().getIsFocusOn()))
            setFocusOnUIComponent(getServiceCodeBean());
        else if (FOCUS_ROUTE.equals(getResetTrailerBean().getIsFocusOn()))
            setFocusOnUIComponent(getRouteBean());
        else if (FOCUS_DOOR.equals(getResetTrailerBean().getIsFocusOn()))
            setFocusOnUIComponent(getDoorIdBean());
    }


    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();

            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    private void queueValueChangeEvent(RichInputText textFields, Object newValue) {
        ValueChangeEvent vce = new ValueChangeEvent(textFields, null, newValue);
        String id = textFields.getId();
        switch (id) {
        case "tra1":
            if (StringUtils.isNotEmpty((String) newValue)) {
                this.onChangedTrailerId(vce);
            }
            break;
        case "ser1":
            if (StringUtils.isNotEmpty((String) newValue)) {
                this.onChangedServiceCode(vce);
            }
            break;
        case "rou1":
            if (StringUtils.isNotEmpty((String) newValue)) {
                this.onChangedRoute(vce);
            }
            break;
        case "doo1":
            if (StringUtils.isEmpty((String) newValue)) {
                String msg =
                    getMessage(MSG_RTV_DOOR_REQ, MSG_TYPE_W, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                               (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
                showMessagePanel(msg, MSG_TYPE_E);
                setErrorStyleOnComponent(getDoorIdBean(), getDoorIdIcon(), true);
            } else {
                this.onChangedDoorId(vce);
            }
            break;
        default:
        }
    }

    /**
     * Called on F4-Reset on page.
     * @param actionEvent
     */
    public void resetTrailerListener(ActionEvent ae) {
        // ISSUE 2717
        
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_reset_trailer_s_RESET_TRAILER_F4,RoleBasedAccessConstants.FORM_NAME_hh_reset_trailer_s)){
                   Object val = AdfFacesContext.getCurrentInstance().getViewScope().get(ERR_FLAG);
                   if (VALUE_Y.equals(val))
                       return; //Error present, no further processing.
                   hideMessagePanel();
                   if (checkRequiredField(ATTR_TRAILER_ID) && checkRequiredField(ATTR_SERV_CODE) &&
                       checkRequiredField(ATTR_ROUTE) && checkRequiredField(ATTR_DOOR_ID)) {
                       hideMessagePanel();
                       ADFContext.getCurrent().getPageFlowScope().put(FOCUS_ATTR, getResetTrailerBean().getIsFocusOn());
                       getResetTrailerBean().setIsFocusOn(FOCUS_DUMMY);
                       //disableEnableField(true);
                       //Show confirm popup, when confirm then call reset trailer.
                       String msg =
                           getMessage(MSG_CONFIRM_RESET, MSG_TYPE_C, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                                      (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
                       this.getConfirmResetMsg().setValue(msg);
                       this.getConfirmResetPopup().show(new RichPopup.PopupHints());
                   }    
               }else{
                   showMessagePanel(getMessage("NOT_ALLOWED",MSG_TYPE_E,(String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE)),MSG_TYPE_E);
                  
               }
    }

    /**
     *
     * @param disable true will disable field, false will enable it.
     */
    private void disableEnableField(boolean disable) {
        if (getResetTrailerBean().getIsFocusOn() == null || FOCUS_TRAILER.equals(getResetTrailerBean().getIsFocusOn()))
            getTrailerIdBean().setDisabled(disable);
        else if (FOCUS_SERVICE.equals(getResetTrailerBean().getIsFocusOn()))
            getServiceCodeBean().setDisabled(disable);
        else if (FOCUS_ROUTE.equals(getResetTrailerBean().getIsFocusOn()))
            getRouteBean().setDisabled(disable);
        else if (FOCUS_DOOR.equals(getResetTrailerBean().getIsFocusOn()))
            getDoorIdBean().setDisabled(disable);
    }

    private void showMessagePanel(String msg, String msgType) {
        if (MSG_TYPE_W.equals(msgType))
            errorWarnInfoIcon.setName(WARN_ICON);
        else if (MSG_TYPE_E.equals(msgType))
            errorWarnInfoIcon.setName(ERROR_ICON);
        else if (MSG_TYPE_I.equals(msgType))
            errorWarnInfoIcon.setName(INFO_ICON);

        errorWarnInfoMessage.setValue(msg);
        refreshContentOfUIComponent(allMessagesPanel);
    }

    private void hideMessagePanel() {
        errorWarnInfoIcon.setName(null);
        errorWarnInfoMessage.setValue(null);
        refreshContentOfUIComponent(allMessagesPanel);
    }

    void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set) {
        if (set) {
            AdfFacesContext.getCurrentInstance().getViewScope().put(ERR_FLAG, VALUE_Y);
            addErrorStyleToComponent(uiComponent);
            icon.setName(ERROR_ICON);
        } else {
            AdfFacesContext.getCurrentInstance().getViewScope().put(ERR_FLAG, null);
            icon.setName(REQ_ICON);
            removeErrorStyleToComponent(uiComponent);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(icon);
        AdfFacesContext.getCurrentInstance().addPartialTarget(uiComponent);
    }

    static HhResetTrailerSBean getResetTrailerBean() {
        return (HhResetTrailerSBean) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(HH_RESET_TRAILER_BEAN);
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setTrailerIdBean(RichInputText trailerIdBean) {
        this.trailerIdBean = trailerIdBean;
    }

    public RichInputText getTrailerIdBean() {
        return trailerIdBean;
    }

    public void setConfirmResetMsg(RichOutputText confirmResetMsg) {
        this.confirmResetMsg = confirmResetMsg;
    }

    public RichOutputText getConfirmResetMsg() {
        return confirmResetMsg;
    }

    public void setConfirmResetPopup(RichPopup confirmResetPopup) {
        this.confirmResetPopup = confirmResetPopup;
    }

    public RichPopup getConfirmResetPopup() {
        return confirmResetPopup;
    }

    public void setInfoSuccessPopup(RichPopup infoSuccessPopup) {
        this.infoSuccessPopup = infoSuccessPopup;
    }

    public RichPopup getInfoSuccessPopup() {
        return infoSuccessPopup;
    }

    public void setInfoSuccessMsg(RichOutputText infoSuccessMsg) {
        this.infoSuccessMsg = infoSuccessMsg;
    }

    public RichOutputText getInfoSuccessMsg() {
        return infoSuccessMsg;
    }


    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setServiceCodeIcon(RichIcon serviceCodeIcon) {
        this.serviceCodeIcon = serviceCodeIcon;
    }

    public RichIcon getServiceCodeIcon() {
        return serviceCodeIcon;
    }

    public void setRouteIcon(RichIcon routeIcon) {
        this.routeIcon = routeIcon;
    }

    public RichIcon getRouteIcon() {
        return routeIcon;
    }

    public void setDoorIdIcon(RichIcon doorIdIcon) {
        this.doorIdIcon = doorIdIcon;
    }

    public RichIcon getDoorIdIcon() {
        return doorIdIcon;
    }

    public void setDoorIdBean(RichInputText doorIdBean) {
        this.doorIdBean = doorIdBean;
    }

    public RichInputText getDoorIdBean() {
        return doorIdBean;
    }

    public void setRouteBean(RichInputText routeBean) {
        this.routeBean = routeBean;
    }

    public RichInputText getRouteBean() {
        return routeBean;
    }

    public void setServiceCodeBean(RichInputText serviceCodeBean) {
        this.serviceCodeBean = serviceCodeBean;
    }

    public RichInputText getServiceCodeBean() {
        return serviceCodeBean;
    }

    private void switchFocusTo(RichInputText input, String focus) {
        getResetTrailerBean().setIsFocusOn(focus);
        setFocusOnUIComponent(input);
        refreshContentOfUIComponent(getPflMain());
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    private void setErrorOnField(RichInputText input, RichIcon icon) {
        this.addErrorStyleToComponent(input);
        icon.setName("error");
        this.refreshContentOfUIComponent(icon);
        this.setFocusOnUIComponent(input);
    }

    private boolean checkRequiredField(String attribute) {
        boolean valid = true;
        if (attribute != null && ADFUtils.getBoundAttributeValue(attribute) == null) {
            String msg =
                getMessage(MSG_PARTIAL_ENTRY, MSG_TYPE_W, (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY),
                           (String) ADFUtils.getBoundAttributeValue(ATTR_LANG_CODE));
            showMessagePanel(msg, MSG_TYPE_W);
            RichInputText input = null;
            RichIcon icon = null;
            if (getResetTrailerBean().getIsFocusOn() == null ||
                FOCUS_TRAILER.equals(getResetTrailerBean().getIsFocusOn())) {
                input = getTrailerIdBean();
                icon = getTrailerIdIcon();
            } else if (FOCUS_SERVICE.equals(getResetTrailerBean().getIsFocusOn())) {
                input = getServiceCodeBean();
                icon = getServiceCodeIcon();
            } else if (FOCUS_ROUTE.equals(getResetTrailerBean().getIsFocusOn())) {
                input = getRouteBean();
                icon = getRouteIcon();
            } else if (FOCUS_DOOR.equals(getResetTrailerBean().getIsFocusOn())) {
                input = getDoorIdBean();
                icon = getDoorIdIcon();
            }
            setErrorOnField(input, icon);
            valid = false;
            refreshContentOfUIComponent(getPflMain());
        }
        return valid;
    }
}
