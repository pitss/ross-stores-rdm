package com.ross.rdm.trailermgmt.hhresettrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.binding.OperationBinding;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhResetTrailerSBean  {
    private static final String OPR_ADD_RECORD = "addRecord";
    private String isFocusOn;



    public void initTaskFlow() {

            //Forms  Module : hh_reset_trailer_s
            //Object        : WHEN-NEW-FORM-INSTANCE
            //Source Code   : 
            //
            //Tr_Startup;
            //
            //startup_local;
            //AHL.SET_AHL_INFO(:WORK.FACILITY_ID,:SYSTEM.CURRENT_FORM);
            OperationBinding ob = ADFUtils.findOperation( OPR_ADD_RECORD);
            ob.execute();
            }
    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

}
