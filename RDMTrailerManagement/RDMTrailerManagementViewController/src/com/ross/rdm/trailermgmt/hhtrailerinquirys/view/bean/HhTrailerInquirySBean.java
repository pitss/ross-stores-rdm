package com.ross.rdm.trailermgmt.hhtrailerinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhTrailerInquirySBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhTrailerInquirySBean.class);

    private Boolean isTrailerIdValidated;
    private String isFocusOn;
    private Boolean isError;
    private Boolean firstInit = true;

    public void initTaskFlow() {
        _logger.info("initTaskFlow() Start");
        initTaskFlowTrailerInquiry();
        firstInit = false;
        this.setIsFocusOn("TrailerId");
        this.setIsTrailerIdValidated(false);
        _logger.info("initTaskFlow() End");
    }

    private void initTaskFlowTrailerInquiry() {
        ADFUtils.findOperation("initTaskFlowTrailerInquiry").execute();
    }

    public void setIsTrailerIdValidated(Boolean isTrailerIdValidated) {
        this.isTrailerIdValidated = isTrailerIdValidated;
    }

    public Boolean getIsTrailerIdValidated() {
        return isTrailerIdValidated;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }
}
