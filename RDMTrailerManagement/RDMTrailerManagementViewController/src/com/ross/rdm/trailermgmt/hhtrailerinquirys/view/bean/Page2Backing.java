package com.ross.rdm.trailermgmt.hhtrailerinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.HhTrailerInquirySShuttleInquiryViewRowImpl;
import com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.HhTrailerInquirySTrailerInquiryViewRowImpl;
import com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.HhTrailerInquirySWorkViewRowImpl;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMTrailerManagementBackingBean {
    private final static String HH_TRAILER_INQUIRY_FLOW_BEAN = "HhTrailerInquirySBean";
    private final static String SHUTTLE_INQUIRY_ITERATOR = "HhTrailerInquirySShuttleInquiryViewIterator";
    private final static String WORK_VIEW_ITERATOR = "HhTrailerInquirySWorkViewIterator";
    private final static String TRAILER_INQUIRY = "TRAILER INQUIRY";
    private final static String SUBLOAD = "SUB-LOAD";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String FACILITY_ID_ATTR = "FacilityId";

    private RichInputText trailerId;
    private RichInputText loadType;
    private RichInputText sentDate;
    private RichInputText arrivedDate;

    private RichPanelFormLayout trailerInquiryPanel;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon trailerIdIcon;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichLink f3Link;
    private RichLink f2Link;

    public Page2Backing() {

    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            this.clearFields(true);
            this.getHhTrailerInquirySWorkViewRow().setHeader(TRAILER_INQUIRY);
            ADFUtils.invokeAction("Page1");
        } else if ("F2".equalsIgnoreCase(key)){
            if(initSubloadPage()){
                this.getHhTrailerInquirySWorkViewRow().setHeader(SUBLOAD);
                ADFUtils.invokeAction("Page3");
            }
        }
    }
    
    private boolean initSubloadPage(){
        List<String> msgs = null;
        msgs = (List<String>) ADFUtils.findOperation("initSubloadPage").execute();
        if (msgs != null && msgs.size() == 2 && msgs.get(1) != null ) {
            showMessagesPanel("E",  msgs.get(1));
            return false;
        }
        return true;
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2Link());
                actionEvent.queue();
            }
            this.refreshContentOfUIComponent(this.getTrailerInquiryPanel());
        }
    }
    
    private void clearFields(boolean clearAll) {
        _logger.info("clearFields() Start");
        this.clearCurrentRow();
        this.getLoadType().resetValue();
        this.getSentDate().resetValue();
        this.getArrivedDate().resetValue();
        this.refreshContentOfUIComponent(this.getLoadType());
        this.refreshContentOfUIComponent(this.getSentDate());
        this.refreshContentOfUIComponent(this.getArrivedDate());
        //Invalidate containerId and TroubleCode
        this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(false);
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow() {
        _logger.info("clearCurrentRow() Start");
        HhTrailerInquirySShuttleInquiryViewRowImpl hhRowImpl = this.getHhTrailerInquirySShuttleInquiryViewRow();
        if (hhRowImpl != null) {
            hhRowImpl.setTrailerId(null);
            hhRowImpl.setLoadType(null);
            hhRowImpl.setSentDate(null);
            hhRowImpl.setArrivedDate(null);
        }
        _logger.info("clearCurrentRow() End");
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("tra1")) {
                this.getTrailerIdIcon().setName("error");
                this.refreshContentOfUIComponent(this.getTrailerIdIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("tra1")) {
                this.getTrailerIdIcon().setName("required");
                this.refreshContentOfUIComponent(this.getTrailerIdIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        if (getErrorWarnInfoMessage()!= null && getErrorWarnInfoIcon() != null){
            getErrorWarnInfoMessage().setValue(null);
            getErrorWarnInfoIcon().setName(null);
            getErrorWarnInfoIcon().setVisible(false);
        }

        if ("W".equals(messageType)) {
            getErrorWarnInfoIcon().setName("warning");
        } else if ("I".equals(messageType)) {
            getErrorWarnInfoIcon().setName("info");
        } else if ("M".equals(messageType)) {
            getErrorWarnInfoIcon().setName("logo");
        } else {
            getErrorWarnInfoIcon().setName("error");
        }
        getErrorWarnInfoMessage().setValue(msg);
        getErrorWarnInfoIcon().setVisible(true);
        refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }
    
    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }
    
    private HhTrailerInquirySWorkViewRowImpl getHhTrailerInquirySWorkViewRow() {
        return ((HhTrailerInquirySWorkViewRowImpl) ADFUtils.findIterator(WORK_VIEW_ITERATOR).getCurrentRow());
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f2ActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_inquiry_s_SHUTTLE_INQUIRY_F2,RoleBasedAccessConstants.FORM_NAME_hh_trailer_inquiry_s)){
                     this.trKeyIn("F2");
                 }else{
                     this.showMessagesPanel("E",
                                          this.getMessage("NOT_ALLOWED", "E", this.getFacilityIdAttrValue(),
                                                          this.getLangCodeAttrValue()));
                 }
    }
    
    private HhTrailerInquirySBean getHhTrailerInquirySPageFlowBean() {
        return ((HhTrailerInquirySBean) this.getPageFlowBean(HH_TRAILER_INQUIRY_FLOW_BEAN));
    }

    private HhTrailerInquirySShuttleInquiryViewRowImpl getHhTrailerInquirySShuttleInquiryViewRow() {
        return (HhTrailerInquirySShuttleInquiryViewRowImpl) ADFUtils.findIterator(SHUTTLE_INQUIRY_ITERATOR).getCurrentRow();
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setLoadType(RichInputText loadType) {
        this.loadType = loadType;
    }

    public RichInputText getLoadType() {
        return loadType;
    }

    public void setSentDate(RichInputText sentDate) {
        this.sentDate = sentDate;
    }

    public RichInputText getSentDate() {
        return sentDate;
    }

    public void setArrivedDate(RichInputText arrivedDate) {
        this.arrivedDate = arrivedDate;
    }

    public RichInputText getArrivedDate() {
        return arrivedDate;
    }

    public void setTrailerInquiryPanel(RichPanelFormLayout trailerInquiryPanel) {
        this.trailerInquiryPanel = trailerInquiryPanel;
    }

    public RichPanelFormLayout getTrailerInquiryPanel() {
        return trailerInquiryPanel;
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }
}
