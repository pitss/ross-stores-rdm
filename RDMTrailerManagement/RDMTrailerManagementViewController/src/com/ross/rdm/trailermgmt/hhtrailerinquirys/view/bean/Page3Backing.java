package com.ross.rdm.trailermgmt.hhtrailerinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.HhTrailerInquirySWorkViewRowImpl;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.event.SelectionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page3.jspx
// ---
// ---------------------------------------------------------------------
public class Page3Backing extends RDMTrailerManagementBackingBean {
    private static final String FIRST_RECORD = "FIRST RECORD";
    private static final String LAST_RECORD = "LAST RECORD";
    private final static String SHUTTLE_INQUIRY_HEADER = "SHUTTLE INQUIRY";
    private final static String WORK_VIEW_ITERATOR = "HhTrailerInquirySWorkViewIterator";

    private RichPanelGroupLayout errorPanel;
    private RichIcon iconErrorMessage;
    private RichOutputText errorMessage;
    private RichLink f3Link;
    private RichTable itemTable;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class);

    public Page3Backing() {

    }
    
    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib = ADFUtils.findIterator("CGetLoadDetailsIterator");
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == trow - 1) {
            this.showMessagesPanel("I", LAST_RECORD);
        } else if (crow == 0) {
            this.showMessagesPanel("I", FIRST_RECORD);
        } else {
            this.hideMessagesPanel();
        }
    }
    
    public void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);

        if ("W".equals(messageType)) {
            this.getIconErrorMessage().setName("warning");
        } else if ("I".equals(messageType) || "M".equals(messageType)) {
            this.getIconErrorMessage().setName("info");
        } else {
            this.getIconErrorMessage().setName("error");
        }
        this.getErrorMessage().setValue(msg);
        this.getIconErrorMessage().setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("showMessagesPanel() End");
    }
    
    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorMessage().setValue(null);
        this.getIconErrorMessage().setName(null);
        this.getIconErrorMessage().setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("hideMessagesPanel() End");
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        JSFUtils.resolveMethodExpression("#{bindings.CGetLoadDetails.collectionModel.makeCurrent}",
                                         SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                         selectionEvent });
        this.showFirstLastRecordInfo();
    }
    
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        this.showFirstLastRecordInfo();
        _logger.info("onRegionLoad End");
    }
    
    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            this.getHhTrailerInquirySWorkViewRow().setHeader(SHUTTLE_INQUIRY_HEADER);
            ADFUtils.invokeAction("Page2");
        }
    }
    
    private HhTrailerInquirySWorkViewRowImpl getHhTrailerInquirySWorkViewRow() {
        return ((HhTrailerInquirySWorkViewRowImpl) ADFUtils.findIterator(WORK_VIEW_ITERATOR).getCurrentRow());
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setIconErrorMessage(RichIcon iconErrorMessage) {
        this.iconErrorMessage = iconErrorMessage;
    }

    public RichIcon getIconErrorMessage() {
        return iconErrorMessage;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setItemTable(RichTable itemTable) {
        this.itemTable = itemTable;
    }

    public RichTable getItemTable() {
        return itemTable;
    }
    
    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }
}
