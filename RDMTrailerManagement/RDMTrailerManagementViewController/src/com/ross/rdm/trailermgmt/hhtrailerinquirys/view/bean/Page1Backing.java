package com.ross.rdm.trailermgmt.hhtrailerinquirys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.HhTrailerInquirySTrailerInquiryViewRowImpl;
import com.ross.rdm.trailermgmt.hhtrailerinquirys.model.views.HhTrailerInquirySWorkViewRowImpl;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.Row;



// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMTrailerManagementBackingBean {

    private final static String HH_TRAILER_INQUIRY_FLOW_BEAN = "HhTrailerInquirySBean";
    private final static String TRAILER_INQUIRY_ITERATOR = "HhTrailerInquirySTrailerInquiryViewIterator";
    private final static String WORK_VIEW_ITERATOR = "HhTrailerInquirySWorkViewIterator";
    private final static String TRAILER_ID_ATTR = "TrailerId";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String FORM_NAME = "HH_TRAILER_INQUIRY_S";
    private final static String F2_ITEM_KEY = "TRAILER_INQUIRY_BLOCK.F2";
    private final static String TRAILER_INQUIRY_CURSOR = "TRAILER_INQUIRY";
    private final static String SHUTTLE_INQUIRY_HEADER = "SHUTTLE INQUIRY";

    private RichInputText trailerId;
    private RichInputText carrier;
    private RichInputText trailerType;
    private RichInputText status;
    private RichInputText location;
    private RichInputText sealNbr;
    private RichInputText lastUse;

    private RichPanelFormLayout trailerInquiryPanel;
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon trailerIdIcon;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private RichLink f3Link;
    private RichLink f2Link;


    public Page1Backing() {
        
    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.callValidateTrailerId(enteredContainerIdValue);
        } else {
            this.getTrailerIdIcon().setName("error");
            this.refreshContentOfUIComponent(this.getTrailerIdIcon());
            this.refreshContentOfUIComponent(this.getTrailerId());
            this.addErrorStyleToComponent(this.getTrailerId());
            this.setFocusTrailerId();
            this.selectTextOnUIComponent(this.getTrailerId());
            this.showMessagesPanel("E",
                                   this.getMessage("INV_TRAILER", "E", this.getFacilityIdAttrValue(),
                                                   this.getLangCodeAttrValue()));
            this.clearFields(false);
        }
    }

    public boolean callValidateTrailerId(String trailerId) {
        _logger.info("validateTrailerId() Start");
        if (!this.getTrailerId().isDisabled()) {
            if (trailerId == null) {
                this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(false);
                _logger.info("validateTrailerId() End");
                return false;
            } else {
                trailerId = trailerId.toUpperCase();
                this.getHhTrailerInquirySPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getTrailerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callTrailerIdValidation");
                oper.getParamsMap().put("trailerId", trailerId);
                oper.getParamsMap().put("facilityId", this.getFacilityIdAttrValue());

                oper.execute();
                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if ("E".equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel("E",
                                                   this.getMessage(msg, "E", this.getFacilityIdAttrValue(),
                                                                   this.getLangCodeAttrValue()));
                            this.clearFields(false);
                        } else {
                            this.showMessagesPanel("E", errorCodeList.get(0));
                            this.clearFields(false);
                        }
                        this.getTrailerIdIcon().setName("error");
                        this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                        this.refreshContentOfUIComponent(this.getTrailerId());
                        this.addErrorStyleToComponent(this.getTrailerId());
                        this.setFocusTrailerId();
                        this.selectTextOnUIComponent(this.getTrailerId());
                        this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(false);
                        _logger.info("validateTrailerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(TRAILER_ID_ATTR, trailerId);
                        this.getTrailerIdIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getTrailerId());
                        this.refreshContentOfUIComponent(this.getTrailerInquiryPanel());
                        this.setFocusTrailerId();
                        this.selectTextOnUIComponent(this.getTrailerId());
                    }
                }
            }
        }
        this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(true);
        _logger.info("validateTrailerId() End");
        return true;
    }

    private boolean callCheckTrailerInquiry(String trailerId, String cursorBlock) {
        _logger.info("callCheckTrailerInquiry() Start");
        if (!this.getTrailerId().isDisabled()) {
            if (trailerId == null) {
                this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(false);
                _logger.info("callCheckTrailerInquiry() End");
                return false;
            } else {
                trailerId = trailerId.toUpperCase();
                this.getHhTrailerInquirySPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getTrailerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckTrailerInquiry");
                oper.getParamsMap().put("pTrailerId", trailerId);
                oper.getParamsMap().put("pFacilityId", getFacilityIdAttrValue());
                oper.getParamsMap().put("pCursorBlock", cursorBlock);

                oper.execute();
                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if ("E".equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel("E", msg);
                        } else {
                            this.showMessagesPanel("E", errorCodeList.get(0));
                            this.clearFields(false);
                        }
                        this.getTrailerIdIcon().setName("error");
                        this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                        this.refreshContentOfUIComponent(this.getTrailerId());
                        this.addErrorStyleToComponent(this.getTrailerId());
                        this.setFocusTrailerId();
                        this.selectTextOnUIComponent(this.getTrailerId());
                        this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(false);
                        _logger.info("callCheckTrailerInquiry() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(TRAILER_ID_ATTR, trailerId);
                        this.getTrailerIdIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getTrailerId());
                        this.refreshContentOfUIComponent(this.getTrailerInquiryPanel());
                        this.setFocusTrailerId();
                        this.selectTextOnUIComponent(this.getTrailerId());
                    }
                }
            }
        }
        this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(true);
        _logger.info("callCheckTrailerInquiry() End");
        return true;
    }

    private String callCheckScreenOptionPriv(String optionName, String formName) {
        _logger.info("callCheckScreenOptionPriv() Start");
        String result = "OFF";
        Row currentRow = this.getHhTrailerInquirySTrailerInquiryViewRow();
        if (currentRow != null) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckScreenOptionPriv");
            oper.getParamsMap().put("facilityId", this.getFacilityIdAttrValue());
            oper.getParamsMap().put("userId", ADFContext.getCurrent().getSecurityContext().getUserName());
            oper.getParamsMap().put("formName", (formName == null) ? FORM_NAME : formName);
            oper.getParamsMap().put("optionName", optionName);
            result = (String) oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {

                if ("OFF".equals(result)) {
                    this.getHhTrailerInquirySPageFlowBean().setIsError(true);
                    this.showMessagesPanel("W",
                                           this.getMessage("NOT_ALLOWED", "W",
                                                           (String) currentRow.getAttribute("FacilityId"),
                                                           (String) currentRow.getAttribute("LanguageCode")));
                }
            }
        }
        _logger.info("callCheckScreenOptionPriv() End");
        return result;
    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            this.clearFields(true);
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F2".equalsIgnoreCase(key) &&
                   !"OFF".equalsIgnoreCase(this.callCheckScreenOptionPriv(F2_ITEM_KEY, null))) {
            //Validate TrailerId
            this.onChangedTrailerId(new ValueChangeEvent(this.getTrailerId(), null, this.getTrailerId().getValue()));
            // Fire F2 Event
            if (this.getHhTrailerInquirySPageFlowBean().getIsTrailerIdValidated()) {
                boolean chkTrailerInqFlg =
                    callCheckTrailerInquiry((String) this.getTrailerId().getValue(), TRAILER_INQUIRY_CURSOR);
                if (chkTrailerInqFlg) {
                    this.getHhTrailerInquirySWorkViewRow().setHeader(SHUTTLE_INQUIRY_HEADER);
                    ADFUtils.invokeAction("Page2");
                }
            }
        }
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = null;
        if (clientEvent !=null)
            submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getHhTrailerInquirySPageFlowBean().getIsFocusOn();

        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF2Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("TrailerId".equals(currentFocus)) {
                    this.setErrorStyleClass(this.getTrailerId(), false);
                    this.onChangedTrailerId(new ValueChangeEvent(this.getTrailerId(), null, submittedValue));
                }
            }
            this.refreshContentOfUIComponent(this.getTrailerInquiryPanel());
        }
    }

    private void clearFields(boolean clearAll) {
        _logger.info("clearFields() Start");

        this.clearCurrentRow(clearAll);
        if (clearAll) {
            this.getTrailerId().resetValue();
            this.refreshContentOfUIComponent(this.getTrailerId());
        }
        this.getCarrier().resetValue();
        this.getTrailerType().resetValue();
        this.getStatus().resetValue();
        this.getLocation().resetValue();
        this.getSealNbr().resetValue();
        this.getLastUse().resetValue();
        this.refreshContentOfUIComponent(this.getCarrier());
        this.refreshContentOfUIComponent(this.getTrailerType());
        this.refreshContentOfUIComponent(this.getStatus());
        this.refreshContentOfUIComponent(this.getLocation());
        this.refreshContentOfUIComponent(this.getSealNbr());
        this.refreshContentOfUIComponent(this.getLastUse());

        //Invalidate containerId and TroubleCode
        this.getHhTrailerInquirySPageFlowBean().setIsTrailerIdValidated(false);
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow(boolean clearAll) {
        _logger.info("clearCurrentRow() Start");
        HhTrailerInquirySTrailerInquiryViewRowImpl hhRowImpl = this.getHhTrailerInquirySTrailerInquiryViewRow();
        if (hhRowImpl != null) {
            if (clearAll){
                hhRowImpl.setTrailerId(null);
            }
            hhRowImpl.setCarrier(null);
            hhRowImpl.setTrailerType(null);
            hhRowImpl.setStatus(null);
            hhRowImpl.setLocation(null);
            hhRowImpl.setSealNbr(null);
            hhRowImpl.setLastUse(null);
        }

        _logger.info("ClearCurrentRow() End");
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con1")) {
                this.getTrailerIdIcon().setName("error");
                this.refreshContentOfUIComponent(this.getTrailerIdIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con1")) {
                this.getTrailerIdIcon().setName("required");
                this.refreshContentOfUIComponent(this.getTrailerIdIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if ("W".equals(messageType)) {
            this.getErrorMessageIcon().setName("warning");
        } else if ("I".equals(messageType)) {
            this.getErrorMessageIcon().setName("info");
        } else if ("M".equals(messageType)) {
            this.getErrorMessageIcon().setName("logo");
        } else {
            this.getErrorMessageIcon().setName("error");
        }
        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f2ActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_inquiry_s_TRAILER_INQUIRY_F2,RoleBasedAccessConstants.FORM_NAME_hh_trailer_inquiry_s)){
                     this.trKeyIn("F2");
                 }else{
                       this.showMessagesPanel("E",
                                            this.getMessage("NOT_ALLOWED", "E", this.getFacilityIdAttrValue(),
                                                            this.getLangCodeAttrValue()));
                 }
        
        
       
    }

    private void setFocusTrailerId() {
        this.getHhTrailerInquirySPageFlowBean().setIsFocusOn(TRAILER_ID_ATTR);
        this.setFocusOnUIComponent(this.getTrailerId());
    }

    private HhTrailerInquirySBean getHhTrailerInquirySPageFlowBean() {
        return ((HhTrailerInquirySBean) this.getPageFlowBean(HH_TRAILER_INQUIRY_FLOW_BEAN));
    }
    
    private HhTrailerInquirySWorkViewRowImpl getHhTrailerInquirySWorkViewRow() {
        return ((HhTrailerInquirySWorkViewRowImpl) ADFUtils.findIterator(WORK_VIEW_ITERATOR).getCurrentRow());
    }

    private HhTrailerInquirySTrailerInquiryViewRowImpl getHhTrailerInquirySTrailerInquiryViewRow() {
        return (HhTrailerInquirySTrailerInquiryViewRowImpl) ADFUtils.findIterator(TRAILER_INQUIRY_ITERATOR).getCurrentRow();
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public void setTrailerInquiryPanel(RichPanelFormLayout trailerInquiryPanel) {
        this.trailerInquiryPanel = trailerInquiryPanel;
    }

    public RichPanelFormLayout getTrailerInquiryPanel() {
        return trailerInquiryPanel;
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }

    public void setCarrier(RichInputText carrier) {
        this.carrier = carrier;
    }

    public RichInputText getCarrier() {
        return carrier;
    }

    public void setTrailerType(RichInputText trailerType) {
        this.trailerType = trailerType;
    }

    public RichInputText getTrailerType() {
        return trailerType;
    }

    public void setStatus(RichInputText status) {
        this.status = status;
    }

    public RichInputText getStatus() {
        return status;
    }

    public void setLocation(RichInputText location) {
        this.location = location;
    }

    public RichInputText getLocation() {
        return location;
    }

    public void setSealNbr(RichInputText sealNbr) {
        this.sealNbr = sealNbr;
    }

    public RichInputText getSealNbr() {
        return sealNbr;
    }

    public void setLastUse(RichInputText lastUse) {
        this.lastUse = lastUse;
    }

    public RichInputText getLastUse() {
        return lastUse;
    }
}
