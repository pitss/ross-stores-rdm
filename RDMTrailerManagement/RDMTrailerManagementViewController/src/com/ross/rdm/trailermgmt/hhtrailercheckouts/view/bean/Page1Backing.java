package com.ross.rdm.trailermgmt.hhtrailercheckouts.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.IErrorMessageModel;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMTrailerManagementBackingBean {

    private RichInputText trailerIdControl;

    public Page1Backing() {
    }

    public void onChangedTrailer(ValueChangeEvent vce) {
        hideMessages();
        updateModel(vce);
        executeOperation("findCheckoutTrailer");
        String result = (String)getOperationResult("findCheckoutTrailer");
        if (null != result) {
            //getPageFlowBean().setTrailerId(null);
            showMessage("E", result, trailerIdControl);                    
        }
    }
    
    protected HhTrailerCheckOutSBean getPageFlowBean() {
        return (HhTrailerCheckOutSBean)getPageFlowBean("HhTrailerCheckOutSBean");
    }
    
    // do we have a valid container?
    private boolean hasData() {
        return null != ADFUtils.getBoundAttributeValue("TrailerId");
    }
    
    // tests whether one of the input fields has error
    private boolean fieldErrorExists() {
        IErrorMessageModel pageFlowBean = getPageFlowBean();
        return "error".equals(pageFlowBean.getMessageIcon()) && null != pageFlowBean.getFocusControl();        
    }    
    
    private void clearBlock() {        
        executeOperation("clearData");
        getPageFlowBean().setTrailerId(null);
        setFocusOnUIComponent(trailerIdControl);
    }

    public void f4DoneActionListener(ActionEvent actionEvent) { 
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_out_s_TRAILER_CHECKOUT_F4,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_out_s)){
                     if (!hasData()) {
                         if (fieldErrorExists())
                             return;
                         showMessage("E", "PARTIAL_ENTRY", trailerIdControl); 
                         return;
                     }
                     
                     hideMessages();        
                     // OTM 3270 
                     /* if (!executeOperation("checkoutTrailer"))
                         return;
                     String res = (String)getOperationResult("checkoutTrailer"); */
                     OperationBinding op = ADFUtils.findOperation("callCheckOutTrailer");
                     List<String> result = (List<String>) op.execute();
                     if(op.getErrors() == null || op.getErrors().isEmpty()){
                         if (result != null) {
                             showMessage(result.get(0), result.get(1), null);
                         } else {
                             /* OTM 3270
                              * if (!executeOperation("Commit"))
                                 return; */
                             clearBlock();
                             showMessage("S", "SUCCESS_OPER", null);
                         }
                     }
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
        
      
    }


    @Override
    protected void hideMessages() {        
        super.hideMessages();
        removeErrorStyleToComponent(trailerIdControl);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////    

    public void setTrailerIdControl(RichInputText trailerIdControl) {
        this.trailerIdControl = trailerIdControl;
    }

    public RichInputText getTrailerIdControl() {
        return trailerIdControl;
    }
}
