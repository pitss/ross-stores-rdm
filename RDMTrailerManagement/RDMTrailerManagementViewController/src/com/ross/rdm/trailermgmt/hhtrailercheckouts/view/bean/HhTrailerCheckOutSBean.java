package com.ross.rdm.trailermgmt.hhtrailercheckouts.view.bean;

import com.ross.rdm.common.view.framework.ErrorMessageModelBase;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------

public class HhTrailerCheckOutSBean extends ErrorMessageModelBase  {
    
    // search field
    private String trailerId;
        
    // ADF quirk workaround: empty fields backing
    private String emptyField;    
          
    //////////////////////////////////////////////////////////////////////////
    // accessors
    //////////////////////////////////////////////////////////////////////////      

    public void setTrailerId(String trailerId) {
        this.trailerId = null != trailerId ? trailerId.toUpperCase() : null;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setEmptyField(String emptyField) {
        this.emptyField = emptyField;
    }

    public String getEmptyField() {
        return emptyField;
    }
}
