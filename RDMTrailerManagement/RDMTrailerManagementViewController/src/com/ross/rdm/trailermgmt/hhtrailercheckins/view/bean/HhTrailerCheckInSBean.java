package com.ross.rdm.trailermgmt.hhtrailercheckins.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.ErrorMessageModelBase;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhTrailerCheckInSBean extends ErrorMessageModelBase {

    // search fields
    private String carrierCode;
    private String trailerId;

    // ADF quirk workaround: empty fields backing
    private String emptyField;

    // switcher for displaying codes
    private boolean isCarrierCode = true;

    // first time flag
    private boolean firstTime = true;
    private int recordsAccepted = 0;
    private boolean notAllowed = false;

    private String brokenSealTroubleCode;

    private String forceFocusPage1;
    private String lastFocus;

    private String zone;
    private String inOut;
    private String sealNbr;
    private String sealIntact;
    private String troubleCode;
    private String locationId;

    private Boolean executedKey = Boolean.FALSE;
    private boolean disableInOutSealNbr = false;

    public HhTrailerCheckInSBean() {
        setFocusControl("car");
    }

    //////////////////////////////////////////////////////////////////////////
    // accessors
    //////////////////////////////////////////////////////////////////////////

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setEmptyField(String emptyField) {
        this.emptyField = emptyField;
    }

    public String getEmptyField() {
        return emptyField;
    }

    public void setIsCarrierCode(boolean isCarrierCode) {
        this.isCarrierCode = isCarrierCode;
    }

    public boolean isIsCarrierCode() {
        return isCarrierCode;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public void setBrokenSealTroubleCode(String brokenSealTroubleCode) {
        this.brokenSealTroubleCode = brokenSealTroubleCode;
    }

    public String getBrokenSealTroubleCode() {
        return brokenSealTroubleCode;
    }

    public void setRecordsAccepted(int recordsAccepted) {
        this.recordsAccepted = recordsAccepted;
    }

    public int getRecordsAccepted() {
        return recordsAccepted;
    }

    public void setForceFocusPage1(String forceFocusPage1) {
        this.forceFocusPage1 = forceFocusPage1;
    }

    public String getForceFocusPage1() {
        return forceFocusPage1;
    }

    public void setTroubleCode(String troubleCode) {
        if (hasData()) {
            ADFUtils.setBoundAttributeValue("TroubleCode", troubleCode);
        } else {
            this.troubleCode = troubleCode;
        }
    }

    public String getTroubleCode() {
        if (hasData()) {
            return (String) ADFUtils.getBoundAttributeValue("TroubleCode");
        } else {
            return troubleCode;
        }
    }

    public void setZone(String zone) {
        if (hasData()) {
            ADFUtils.setBoundAttributeValue("Zone", zone);
        } else {
            this.zone = zone;
        }
    }

    public String getZone() {
        if (hasData()) {
            return (String) ADFUtils.getBoundAttributeValue("Zone");
        } else {
            return zone;
        }
    }

    public void setInOut(String inOut) {
        if (hasData()) {
            ADFUtils.setBoundAttributeValue("InOut", inOut);
        } else {
            this.inOut = inOut;
        }
    }

    public String getInOut() {
        if (hasData()) {
            return (String) ADFUtils.getBoundAttributeValue("InOut");
        } else {
            return inOut;
        }
    }

    public void setSealNbr(String sealNbr) {
        if (hasData()) {
            ADFUtils.setBoundAttributeValue("SealNbr", sealNbr);
        } else {
            this.sealNbr = sealNbr;
        }
    }

    public String getSealNbr() {
        if (hasData()) {
            return (String) ADFUtils.getBoundAttributeValue("SealNbr");
        } else {
            return sealNbr;
        }
    }

    public void setSealIntact(String sealIntact) {
        if (hasData()) {
            ADFUtils.setBoundAttributeValue("SealIntact", sealIntact);
        } else {
            this.sealIntact = sealIntact;
        }
    }

    public String getSealIntact() {
        if (hasData()) {
            return (String) ADFUtils.getBoundAttributeValue("SealIntact");
        } else {
            return sealIntact;
        }
    }

    public void setLocationId(String locationId) {
        if (hasData()) {
            ADFUtils.setBoundAttributeValue("LocationId", locationId);
        } else {
            this.locationId = locationId;
        }
    }

    public String getLocationId() {
        if (hasData()) {
            return (String) ADFUtils.getBoundAttributeValue("LocationId");
        } else {
            return locationId;
        }
    }

    // do we have valid trailer data?
    private boolean hasData() {
        return null != ADFUtils.getBoundAttributeValue("TrailerId");
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
    
    public void restoreTroubleCode(){
        if(this.troubleCode != null){
            this.setTroubleCode(troubleCode);
            this.troubleCode = null;
        }
    }

    public void setLastFocus(String lastFocus) {
        this.lastFocus = lastFocus;
    }

    public String getLastFocus() {
        return lastFocus;
    }

    public void setDisableInOutSealNbr(boolean disableInOutSealNbr) {
        this.disableInOutSealNbr = disableInOutSealNbr;
    }

    public boolean isDisableInOutSealNbr() {
        return disableInOutSealNbr;
    }

    public void setNotAllowed(boolean notAllowed) {
        this.notAllowed = notAllowed;
    }

    public boolean isNotAllowed() {
        return notAllowed;
    }
}
