package com.ross.rdm.trailermgmt.hhtrailercheckins.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page3.jspx
// ---    
// ---------------------------------------------------------------------
public class Page3Backing extends RDMTrailerManagementBackingBean {
  
    private RichInputText bolNbrControl;
    private RichPopup confirmationPopup;
    
    private RichLink acceptLink;
    private RichLink exitLink;
    private RichLink doneLink;
    
    private static ADFLogger logger = ADFLogger.createADFLogger(Page3Backing.class);

    public Page3Backing() {
    }
    
    private void clearBolNbr() {
        ADFUtils.setBoundAttributeValue("BolNbr", null);
    }    
  
    public void f2AcceptActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_BOL_POP_F2,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)){
                     hideMessages();
                     String bolNbr = (String)ADFUtils.getBoundAttributeValue("BolNbr");
                     if (null == bolNbr) {
                         showMessage("E", "PARTIAL_ENTRY", bolNbrControl);            
                         return;
                     }
                     
                     if (!executeOperation("acceptBol")) {
                         logger.severe("Could not execute 'acceptBol'");
                         return;
                     }
                     String result = (String)getOperationResult("acceptBol");
                     if ("Y".equals(result)) {
                         incRecordsAccepted();
                     } else if ("X".equals(result)) {
                         clearBolNbr();
                         showMessage("E", "TC_BOL_ACCEPTED", bolNbrControl);             
                     } else if ("N".equals(result)) {
                         // do nothing??
                     } else if ("TC_BOL_NOTEXIST".equals(result)) {
                         clearBolNbr();
                         showMessage("E", result, bolNbrControl);
                     } else { // can only be APPT_NBR (CheckFreight)
                         clearBolNbr();
                         showFormatMessage("E", "TC_BOL_ON_APPT", null, result, bolNbrControl);            
                     }            
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
        
        
       
    }
        
    public void f3ExitActionListener(ActionEvent actionEvent) {
        hideMessages();
        int recordsAccepted = getRecordsAccepted();
        if (0 == recordsAccepted) {
            clearBolNbr();
            HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
            pageFlowBean.setFirstTime(false);
            clearBlock();
            navigateToAction("Back");
        } else {
            askUser(confirmationPopup, false, "TC_NO_BOLACCEPT", getBeanListener("tcNoBolAcceptListener"));
        }
    }
    
    public void tcNoBolAcceptListener(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            executeOperation("Rollback");
            clearBlock();
            navigateToAction("Back");            
        }
    }
    
    public void f4DoneActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_BOL_POP_F4,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)){
                     hideMessages();
                     int recordsAccepted = getRecordsAccepted();
                     if (0 == recordsAccepted) {
                         ADFUtils.setBoundAttributeValue("BolNbr", null);
                         showMessage("E", "TC_CANT_NO_BOL", null);
                         navigateToAction("Back");
                     } else { 
                         if (!executeOperation("createApptFb")) {
                             logger.severe("Could not execute 'createApptFb'");
                             return;                
                         }
                         String result = (String)getOperationResult("createApptFb");
                         if ("SUCCESS".equals(result)) { 
                             if (!executeOperation("checkAppt")) {
                                 logger.severe("Could not execute operation 'checkAppt'");
                                 return;          
                             }
                             String apptFound = (String)getOperationResult("checkAppt");                
                             showFormatMessage("I", "TC_APTCREATED_H", "TC_APTCREATED_F", apptFound, null);
                             ADFUtils.setBoundAttributeValue("BolNbr", null);
                             clearBlock();
                             navigateToAction("Back");             
                         } else {   
                             ADFUtils.setBoundAttributeValue("BolNbr", null);
                             showMessage("E", "TC_CANT_CREATE", null);
                             navigateToAction("Back");                
                         }
                     }
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
        
       
    }    
    
    private void clearBlock() {
        logger.finest("Clearing block...");        
        if (!executeOperation("clearData")) {
            logger.severe("Could not execute operation 'clearData'");
            return;
        }
        HhTrailerCheckInSBean pageFlow = getPageFlowBean();
        pageFlow.setTrailerId(null);
        pageFlow.setCarrierCode(null);
        pageFlow.setFocusControl("car");        
    }
    
    @Override
    protected void hideMessages() {        
        super.hideMessages();
        removeErrorStyles();         
    }   
    
    private void removeErrorStyles() {
        removeErrorStyleToComponent(bolNbrControl);
    }
        
    private void incRecordsAccepted() {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        pageFlowBean.setRecordsAccepted(pageFlowBean.getRecordsAccepted() + 1);
    }
    
    private int getRecordsAccepted() {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        return pageFlowBean.getRecordsAccepted();
    }
    
    protected HhTrailerCheckInSBean getPageFlowBean() {
        return (HhTrailerCheckInSBean)getPageFlowBean("HhTrailerCheckInSBean");
    }
    
    public void setBolNbrControl(RichInputText bolNbrControl) {
        this.bolNbrControl = bolNbrControl;
    }

    public RichInputText getBolNbrControl() {
        return bolNbrControl;
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }
    
    /**
     * A helper method to construct a bean-qualified method name.
     * We need the name of this bean as configured in the task flow.
     */
    private String getBeanListener(String methodName) {
        return "backingBeanScope.HhTrailerCheckInSPage3Backing." + methodName;
    }
    
    public void setAcceptLink(RichLink acceptLink) {
        this.acceptLink = acceptLink;
    }

    public RichLink getAcceptLink() {
        return acceptLink;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }
    
    /**
     * Custom event executed only when function keys are pressed
     * It queues action event on the appropriate link/button
     * @param clientEvent
     */
    public void performKey(ClientEvent clientEvent) {
        if(clientEvent != null && clientEvent.getParameters().containsKey("keyPressed"))
        {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            
            if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getAcceptLink());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            }
        }
    }
}
