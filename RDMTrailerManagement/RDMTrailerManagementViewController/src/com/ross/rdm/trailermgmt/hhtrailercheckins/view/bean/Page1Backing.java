package com.ross.rdm.trailermgmt.hhtrailercheckins.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.model.utils.ModelUtils;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMTrailerManagementBackingBean {

    private RichInputText troubleCodeControl;
    private RichInputText zoneControl;
    private RichInputText carrierControl;
    private RichInputText trailerControl;
    private RichInputText yardLocationControl;
    private RichInputText inOutControl;
    private RichInputText sealNbrControl;
    private RichInputText sealIntactControl;
    private static final String HH_TRAILER_CHECK_IN_S_BEAN = "HhTrailerCheckInSBean";

    private RichPopup confirmationPopup;

    private static ADFLogger logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichLink bolLink;
    private RichLink exitLink;
    private RichLink doneLink;
    private RichLink carrierLink;
    private RichLink trblLink;
    private RichLink trblClrLink;

    private final static String CARRIER_COMPONENT_ID = "car";
    private final static String TRAILER_COMPONENT_ID = "tra";
    private final static String ZONE_COMPONENT_ID = "zon2";
    private final static String INOUT_COMPONENT_ID = "inO";
    private final static String SEAL_NBR_COMPONENT_ID = "sea";
    private final static String SEAL_INTACT_COMPONENT_ID = "slInt";
    private final static String TRB_COMPONENT_ID = "tro";
    private final static String LOCATION_ID_COMPONENT_ID = "yar1";
    private RichLink changeTrailerHiddenLink;
    private RichLink changeTroubleHiddenLink;

    public Page1Backing() {
    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        if (!isNavigationExecuted()) {
            String forceFocusPage1 = getPageFlowBean().getForceFocusPage1();
            if (forceFocusPage1 != null) {
                getPageFlowBean().setForceFocusPage1(null);
                switch (forceFocusPage1) {
                case CARRIER_COMPONENT_ID:
                    focusField(getCarrierControl());
                    break;
                case TRAILER_COMPONENT_ID:
                    focusField(getTrailerControl());
                    break;
                case ZONE_COMPONENT_ID:
                    focusField(getZoneControl());
                    break;
                case INOUT_COMPONENT_ID:
                    focusField(getInOutControl());
                    break;
                case SEAL_NBR_COMPONENT_ID:
                    focusField(getSealNbrControl());
                    break;
                case SEAL_INTACT_COMPONENT_ID:
                    focusField(getSealIntactControl());
                    break;
                case TRB_COMPONENT_ID:
                    focusField(getTroubleCodeControl());
                    break;
                case LOCATION_ID_COMPONENT_ID:
                    focusField(getYardLocationControl());
                    break;
                default:
                }
            }
        }
    }

    public void onChangedCarrierCode(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            // String newValue = getPageFlowBean().getCarrierCode();
            String newValue = (String) vce.getNewValue();
            if (null == newValue || newValue.isEmpty()) {
                // showMessage("E", "PARTIAL_ENTRY", carrierControl);
                hideMessages();
                return;
            } else {
                hideMessages();
                updateModel(vce);

                if (!executeOperation("codeExists")) {
                    logger.severe("Could not execute operation 'codeExists'");
                    return;
                }
                if (!(Boolean) getOperationResult("codeExists")) {
                    showMessage("E", "INV_CARRIER", carrierControl);
                } else {
                    focusField(trailerControl);
                }
            }
        }
    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeTrailerHiddenLink());
            actionEvent.queue();
        }
    }

    public void onChangedZone(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            hideMessages();
            updateModel(vce);
            List errors = (List) ADFUtils.findOperation("validateZonePl").execute();
            if (errors == null) {
                if (!getPageFlowBean().isDisableInOutSealNbr()) {
                    focusField(inOutControl);
                } else {
                    focusField(sealIntactControl);
                }
            } else if ("CONT_MOVE_WH_ID".equals(errors.get(1))) {
                disableAllFields();
                askUser(confirmationPopup, false, "CONT_MOVE_WH_ID",
                        getBeanListener("confirmProgramExecutionListener"));
            } else {
                showMessage((String) errors.get(0), (String) errors.get(1), zoneControl);
            }
        }
    }

    public void confirmProgramExecutionListener(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            if (!getPageFlowBean().isDisableInOutSealNbr()) {
                focusField(inOutControl);
            } else {
                focusField(sealIntactControl);
            }
        }
        restoreFocus();
    }

    public void onChangedInOut(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            hideMessages();
            updateModel(vce);
            String newValue = (String) vce.getNewValue();
            if (StringUtils.isNotEmpty(newValue) && !ModelUtils.in(newValue, "I", "O")) {
                showMessage("E", "I_O_ONLY", inOutControl);
            } else {
                focusField(sealNbrControl);
            }
        }
    }

    public void onChangedSealNbr(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            // hideMessages();
            updateModel(vce);
            if (!checkValidationError(sealNbrControl)) {
                focusField(sealIntactControl);
            }
        }
    }

    public void onChangedSealIntact(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            hideMessages();
            updateModel(vce);
            String newValue = (String) vce.getNewValue();
            if (StringUtils.isNotEmpty(newValue) && !ModelUtils.in(newValue, "N", "Y")) {
                showMessage("E", "Y_N_ONLY", sealIntactControl);
            } else {
                focusField(troubleCodeControl);
            }
        }
    }

    public void onChangedTroubleCode(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeTroubleHiddenLink());
            actionEvent.queue();
        }
    }

    public void onChangedYardLocationId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            hideMessages();
            if (vce != null) {
                updateModel(vce);
            }

            executeOperation("validateYardLocation");
            String result = (String) getOperationResult("validateYardLocation");
            if (null != result) {
                showMessage("E", result, yardLocationControl);
            }
        }
    }

    // do we have valid trailer data?
    private boolean hasData() {
        return null != ADFUtils.getBoundAttributeValue("TrailerId");
    }

    private void clearBlock() {
        logger.finest("Clearing block...");
        if (!executeOperation("clearData")) {
            logger.severe("Could not execute operation 'clearData'");
            return;
        }
        HhTrailerCheckInSBean pageFlow = getPageFlowBean();
        pageFlow.setTrailerId(null);
        pageFlow.setCarrierCode(null);
        pageFlow.setTroubleCode(null);
        focusField(carrierControl);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // action listeners
    ////////////////////////////////////////////////////////////////////////////////////

    public void f2BolActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F2,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)){
                     hideMessages();

                     resetRecordsAccepted();
                     String msg = (String) ADFUtils.findOperation("validationsTrailerCheckInF2").execute();
                     if (msg != null) {
                         if ("TC_CHECK_IN".equals(msg)) {
                             focusField(carrierControl);
                             showMessage("E", "TC_CHECK_IN", carrierControl);
                         } else if ("INV_TRAIL_STAT".equals(msg)) {
                             showMessage("E", "INV_TRAIL_STAT", null);
                             clearBlock();
                         } else if (msg.startsWith("TC_APTCREATED_H")) {
                             String apptFound = msg.replace("TC_APTCREATED_H", "");
                             showFormatMessage("E", "TC_APTCREATED_H", "TC_APPT_EXIST_F", apptFound, null);
                             clearBlock();
                         }
                     } else {
                         String inOut = (String) ADFUtils.getBoundAttributeValue("InOut");
                         if ("I".equals(inOut)) {
                             navigateToAction("Page3");
                         }
                     }
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
        
        
        
    }

    private void resetRecordsAccepted() {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        pageFlowBean.setRecordsAccepted(0);
    }

    public void f5CarrierActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F5,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)){
                     hideMessages();
                     navigateToAction("Page2");
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
    
    }

    public void f9TroubleCodeActionListener(ActionEvent actionEvent) {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F9,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)){
                     hideMessages();

                     boolean execute = false;
                     if (hasData()) {
                         boolean checkInFlag = Boolean.TRUE.equals(ADFUtils.getBoundAttributeValue("CheckInFlag"));
                         execute = checkInFlag || StringUtils.isEmpty(getPageFlowBean().getTroubleCode());
                     } else {
                         execute = true; //CHECK_IN_FLAG default value Y in STARTUP_LOCAL
                     }

                     if (execute) {
                         getPageFlowBean().setForceFocusPage1(getSelectedFieldId());
                         navigateToAction("Page2");
                     }
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
        
       
    }

    public void f10TroubleClearActionListener(ActionEvent actionEvent) {
        // OTM ISSUE 2835
        if (fieldErrorExists())
            return;
        hideMessages();
        if (!hasData())
            return;
        if (null != ADFUtils.getBoundAttributeValue("TroubleCode")) {
            disableAllFields();
            askUser(confirmationPopup, false, "CONFIRM_OPER", getBeanListener("confirmOperDialogListener"));
        }
    }

    public void confirmOperDialogListener(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            if (!executeOperation("clearTroubleCodes")) {
                logger.severe("Could not execute operation 'clearTroubleCodes'");
            }
            if (!executeOperation("Commit")) {
                logger.severe("Could not execute operation 'Commit'");
            }
        }
        restoreFocus();
    }
    
    public void f3ActionListener(ActionEvent actionEvent) {
        clearBlockOnExit();
        ADFUtils.invokeAction("backGlobalHome");
    }
    
    public void clearBlockOnExit(){
        HhTrailerCheckInSBean pageFlow = getPageFlowBean();
        pageFlow.setTrailerId(null);
        pageFlow.setCarrierCode(null);
        pageFlow.setTroubleCode(null);
        pageFlow.setSealNbr(null);
        pageFlow.setSealIntact(null);
        this.sealIntactControl.resetValue();
        this.troubleCodeControl.resetValue();
        pageFlow.setLocationId(null);
        this.getYardLocationControl().resetValue();
        //OTM 4318, location was set to null when exiting of the screen
        ADFUtils.findOperation("Rollback").execute();
    }

    public void f4DoneActionListener(ActionEvent actionEvent) {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CHECK_TRAILER_F4,RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)){
                     if (getTrailerControl() != null && !getTrailerControl().isDisabled()) {
                         //Simulate ENTER
                         keyNextItemTrailer();
                     }

                     if (!formSuccess())
                         return;
                     hideMessages();

                     if (!checkFields())
                         return;


                     String locationId = (String) ADFUtils.getBoundAttributeValue("LocationId");
                     String locationIdDBValue = (String) ADFUtils.getBoundAttributeValue("LocationIdDBValue");
                     String inOut = (String) ADFUtils.getBoundAttributeValue("InOut");
                     boolean isShuttleTrailer = (Boolean) ADFUtils.getBoundAttributeValue("ShuttleTrailer");
                     String trailerStatus = (String) ADFUtils.getBoundAttributeValue("TrailerStatus");
                     getPageFlowBean().setRecordsAccepted(0);
                     Boolean checkInRaw = (Boolean) ADFUtils.getBoundAttributeValue("CheckInFlag");
                     boolean checkInFlag = (null != checkInRaw) && checkInRaw;

                     if (!isNew() && !ModelUtils.in(trailerStatus, "CHECKED-OUT", "IN-TRANSIT", "PENDXFER")) {
                         clearBlock();
                         showMessage("E", "INV_STATUS", null);
                         return;
                     }

                     if (!isNew() && checkInFlag) {
                         if ("I".equals(inOut)) {
                             if (!isShuttleTrailer) {
                                 if (null != locationIdDBValue &&
                                     (!"UNLOADED".equals(trailerStatus) && !"CHECKED-OUT".equals(trailerStatus) &&
                                      !"SCHEDULED".equals(trailerStatus))) {
                                     focusField(trailerControl);
                                     showMessage("E", "INV_TRAIL_STAT", trailerControl);
                                     return;
                                 }
                             }
                         } else { // in_out = 'O'
                             if (null != locationIdDBValue) {
                                 focusField(trailerControl);
                                 showMessage("E", "INV_TRAIL_STAT", trailerControl);
                                 return;
                             }
                         }
                     }

                     if (isNew() || checkInFlag) {
                         if (isFirstTime()) {
                             executeOperation("chooseLocation");
                             String result = (String) getOperationResult("chooseLocation");
                             if (null != result) {
                                 showMessageRaw("I", result, null);
                             }
                             setFirstTime(false);
                             focusField(yardLocationControl);
                         } else {
                             executeOperation("checkUnexisting");

                             executeOperation("processTrailer");
                             String result = (String) getOperationResult("processTrailer");
                             if (null != result) {
                                 showMessage("E", result, null);
                                 return;
                             }
                             executeOperation("Commit");
                             setFirstTime(true);
                             ADFUtils.setBoundAttributeValue("ProcessTrailerExecuted", true);
                             focusField(carrierControl);

                             if (!isShuttleTrailer) {
                                 // refresh trailer status, could have been changed in the call to "processTrailer"
                                 trailerStatus = (String) ADFUtils.getBoundAttributeValue("TrailerStatus");
                                 if ("ARRIVED-INB".equals(trailerStatus)) {
                                     if (!executeOperation("updateTmsLoadStatus")) {
                                         logger.severe("Could not execute 'updateTmsLoadStatus' in f4DoneActionListener!");
                                         return;
                                     }
                                     int recordsAccepted = (Integer) getOperationResult("updateTmsLoadStatus");
                                     HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
                                     pageFlowBean.setRecordsAccepted(recordsAccepted);

                                     if (!executeOperation("Commit")) {
                                         logger.severe("Could not execute 'Commit1' in f4DoneActionListener!");
                                         return;
                                     }

                                     if (0 == recordsAccepted) {
                                         setCheckInFlag(true);
                                         showMessage("E", "TC_CANT_CREATE", null);
                                         focusField(carrierControl);
                                         return;
                                     } else {
                                         setCheckInFlag(false);
                                         if ("O".equals(inOut)) {
                                             showMessage("E", "TC_CHECK_IN", null);
                                             clearBlock();
                                             return;
                                         } else {
                                             if (!executeOperation("createApptFb")) {
                                                 logger.severe("Could not execute 'createApptFb' !");
                                                 return;
                                             }
                                             result = (String) getOperationResult("createApptFb");
                                             if (!"SUCCESS".equals(result)) {
                                                 showMessage("E", "TC_CANT_CREATE", null);
                                                 focusField(carrierControl);
                                             } else {
                                                 if (!executeOperation("checkAppt")) {
                                                     logger.severe("Could not execute operation 'checkAppt'");
                                                     return;
                                                 }
                                                 String apptFound = (String) getOperationResult("checkAppt");
                                                 showFormatMessage("I", "TC_APTCREATED_H", "TC_APTCREATED_F", apptFound, null);
                                                 clearBlock();
                                             }
                                         }
                                     }
                                 }
                             } else {
                                 focusField(carrierControl);
                             }
                         }
                     } else {
                         if (!executeOperation("Commit")) {
                             logger.severe("Could not execute 'Commit2' in f4DoneActionListener!");
                             return;
                         }
                         setFirstTime(true);
                         clearBlock();
                     }
                 }else{
                     this.showMessage("E", "NOT_ALLOWED", null);
                 }
        
       
    }

    // tests whether one of the input fields has error
    private boolean fieldErrorExists() {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        return "error".equals(pageFlowBean.getMessageIcon()) && null != pageFlowBean.getFocusControl();
    }

    private void setCheckInFlag(boolean value) {
        ADFUtils.setBoundAttributeValue("CheckInFlag", value);
    }

    private boolean checkFields() {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        String inOut = (String) ADFUtils.getBoundAttributeValue("InOut");
        if (null == pageFlowBean.getCarrierCode()) {
            return markErrorField(carrierControl);
        } else if (null == pageFlowBean.getTrailerId()) {
            return markErrorField(trailerControl);
        } else if (null == ADFUtils.getBoundAttributeValue("Zone")) {
            return markErrorField(zoneControl);
        } else if (null == inOut) {
            return markErrorField(inOutControl);
        } else if (null == ADFUtils.getBoundAttributeValue("SealNbr") && "I".equals(inOut)) {
            return markErrorField(sealNbrControl);
        } else if (null == ADFUtils.getBoundAttributeValue("SealIntact") && "I".equals(inOut)) {
            return markErrorField(sealIntactControl);
        } else if (null == ADFUtils.getBoundAttributeValue("LocationId") && !isFirstTime()) {
            return markErrorField(yardLocationControl);
        }

        return true;
    }

    private boolean markErrorField(RichInputText control) {
        focusField(control);
        showMessage("E", "PARTIAL_ENTRY", control);
        return false;
    }

    @Override
    protected void hideMessages() {
        super.hideMessages();
        removeErrorStyles();
    }

    private void removeErrorStyles() {
        removeErrorStyleToComponent(troubleCodeControl);
        removeErrorStyleToComponent(zoneControl);
        removeErrorStyleToComponent(carrierControl);
        removeErrorStyleToComponent(trailerControl);
        removeErrorStyleToComponent(yardLocationControl);
        removeErrorStyleToComponent(inOutControl);
        removeErrorStyleToComponent(sealNbrControl);
        removeErrorStyleToComponent(sealIntactControl);
    }

    // special method to clear errors for Trouble
    public void resetErrors(ClientEvent clientEvent) {
        hideMessages();
    }

    ////////////////////////////////////////////////////////////////////////////////////

    /**
     * A helper method to construct a bean-qualified method name.
     * We need the name of this bean as configured in the task flow.
     */
    private String getBeanListener(String methodName) {
        return "backingBeanScope.HhTrailerCheckInSPage1Backing." + methodName;
    }

    private boolean isFirstTime() {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        return pageFlowBean.isFirstTime();
    }

    private void setFirstTime(boolean isFirstTime) {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        pageFlowBean.setFirstTime(isFirstTime);
    }

    protected HhTrailerCheckInSBean getPageFlowBean() {
        return (HhTrailerCheckInSBean) getPageFlowBean(HH_TRAILER_CHECK_IN_S_BEAN);
    }

    public void setIsCarrierCode(Boolean isCarrierCode) {
        ((HhTrailerCheckInSBean) this.getPageFlowBean(HH_TRAILER_CHECK_IN_S_BEAN)).setIsCarrierCode(isCarrierCode);
    }

    public Boolean getIsCarrierCode() {
        return ((HhTrailerCheckInSBean) this.getPageFlowBean(HH_TRAILER_CHECK_IN_S_BEAN)).isIsCarrierCode();
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////

    public void setTroubleCodeControl(RichInputText troubleCodeControl) {
        this.troubleCodeControl = troubleCodeControl;
    }

    public RichInputText getTroubleCodeControl() {
        return troubleCodeControl;
    }

    public void setZoneControl(RichInputText zoneControl) {
        this.zoneControl = zoneControl;
    }

    public RichInputText getZoneControl() {
        return zoneControl;
    }

    public void setCarrierControl(RichInputText carrierControl) {
        this.carrierControl = carrierControl;
    }

    public RichInputText getCarrierControl() {
        return carrierControl;
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setTrailerControl(RichInputText trailerControl) {
        this.trailerControl = trailerControl;
    }

    public RichInputText getTrailerControl() {
        return trailerControl;
    }

    public void setYardLocationControl(RichInputText yardLocationControl) {
        this.yardLocationControl = yardLocationControl;
    }

    public RichInputText getYardLocationControl() {
        return yardLocationControl;
    }

    public void setInOutControl(RichInputText inOutControl) {
        this.inOutControl = inOutControl;
    }

    public RichInputText getInOutControl() {
        return inOutControl;
    }

    public void setSealNbrControl(RichInputText sealNbrControl) {
        this.sealNbrControl = sealNbrControl;
    }

    public RichInputText getSealNbrControl() {
        return sealNbrControl;
    }

    public void setSealIntactControl(RichInputText sealIntactControl) {
        this.sealIntactControl = sealIntactControl;
    }

    public RichInputText getSealIntactControl() {
        return sealIntactControl;
    }

    private void disableAllFields() {
        troubleCodeControl.setDisabled(true);
        this.refreshContentOfUIComponent(troubleCodeControl);
        zoneControl.setDisabled(true);
        this.refreshContentOfUIComponent(zoneControl);
        carrierControl.setDisabled(true);
        this.refreshContentOfUIComponent(carrierControl);
        trailerControl.setDisabled(true);
        this.refreshContentOfUIComponent(trailerControl);
        yardLocationControl.setDisabled(true);
        this.refreshContentOfUIComponent(yardLocationControl);
        inOutControl.setDisabled(true);
        this.refreshContentOfUIComponent(inOutControl);
        sealNbrControl.setDisabled(true);
        this.refreshContentOfUIComponent(sealNbrControl);
        sealIntactControl.setDisabled(true);
        this.refreshContentOfUIComponent(sealIntactControl);
    }

    /**
     * Custom event executed only when function keys are pressed
     * It queues action event on the appropriate link/button
     * @param clientEvent
     */
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String item = (String) clientEvent.getParameters().get("item");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");

            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (CARRIER_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getCarrierControl().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equalsIgnoreCase((String) this.getCarrierControl().getValue())) {
                        this.onChangedCarrierCode(new ValueChangeEvent(this.getCarrierControl(), null, submittedValue));
                    }
                } else if (TRAILER_COMPONENT_ID.equals(item)) {
                    if (valueNotChanged(submittedValue, getTrailerControl().getValue())) {
                        this.onChangedTrailerId(new ValueChangeEvent(this.getTrailerControl(), null, submittedValue));
                    }
                } else if (ZONE_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getZoneControl().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equalsIgnoreCase((String) this.getZoneControl().getValue())) {
                        this.onChangedZone(new ValueChangeEvent(this.getZoneControl(), null, submittedValue));
                    }
                } else if (INOUT_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getInOutControl().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equalsIgnoreCase((String) this.getInOutControl().getValue())) {
                        this.onChangedInOut(new ValueChangeEvent(this.getInOutControl(), null, submittedValue));
                    }
                } else if (SEAL_NBR_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getSealNbrControl().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equalsIgnoreCase((String) this.getSealNbrControl().getValue())) {
                        this.onChangedSealNbr(new ValueChangeEvent(this.getSealNbrControl(), null, submittedValue));
                    }
                } else if (SEAL_INTACT_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getSealIntactControl().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equalsIgnoreCase((String) this.getSealIntactControl().getValue())) {
                        this.onChangedSealIntact(new ValueChangeEvent(this.getSealIntactControl(), null,
                                                                      submittedValue));
                    }
                } else if (TRB_COMPONENT_ID.equals(item)) {
                    if (valueNotChanged(submittedValue, getTroubleCodeControl().getValue())) {
                        this.onChangedTroubleCode(new ValueChangeEvent(this.getTroubleCodeControl(), null,
                                                                       submittedValue));
                    }
                } else if (LOCATION_ID_COMPONENT_ID.equals(item)) {
                    if ((StringUtils.isEmpty((String) this.getYardLocationControl().getValue()) &&
                         StringUtils.isEmpty(submittedValue)) ||
                        submittedValue.equalsIgnoreCase((String) this.getYardLocationControl().getValue())) {
                        this.onChangedYardLocationId(new ValueChangeEvent(this.getYardLocationControl(), null,
                                                                          submittedValue));
                    }
                }
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getBolLink());
                actionEvent.queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getCarrierLink());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getTrblLink());
                actionEvent.queue();
            } else if (F10_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getTrblClrLink());
                actionEvent.queue();
            }
        }
    }

    public void setBolLink(RichLink bolLink) {
        this.bolLink = bolLink;
    }

    public RichLink getBolLink() {
        return bolLink;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }

    public void setCarrierLink(RichLink carrierLink) {
        this.carrierLink = carrierLink;
    }

    public RichLink getCarrierLink() {
        return carrierLink;
    }

    public void setTrblLink(RichLink trblLink) {
        this.trblLink = trblLink;
    }

    public RichLink getTrblLink() {
        return trblLink;
    }

    public void setTrblClrLink(RichLink trblClrLink) {
        this.trblClrLink = trblClrLink;
    }

    public RichLink getTrblClrLink() {
        return trblClrLink;
    }

    private void focusField(RichInputText input) {
        this.disableAllFields();
        input.setDisabled(false);
        setFocusOnUIComponent(input);
        getPageFlowBean().setLastFocus(input.getId());
    }

    private String getSelectedFieldId() {
        return null;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_trailermgmt_hhtrailercheckins_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    private boolean valueNotChanged(String submittedVal, Object fieldValue) {
        if (fieldValue instanceof String) {
            return (StringUtils.isEmpty((String) fieldValue) && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase((String) fieldValue);
        } else {
            return (fieldValue == null && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase(fieldValue == null ? "" : fieldValue.toString());
        }
    }

    private List validateTroubleCode(String troubleCode) {
        OperationBinding op = ADFUtils.findOperation("validateTroubleCode");
        op.getParamsMap().put("pFacilityId", JSFUtils.resolveExpression("#{CurrentUser.facilityId}"));
        op.getParamsMap().put("troubleCode", troubleCode);
        return (List) op.execute();
    }

    public void setChangeTrailerHiddenLink(RichLink changeTrailerHiddenLink) {
        this.changeTrailerHiddenLink = changeTrailerHiddenLink;
    }

    public RichLink getChangeTrailerHiddenLink() {
        return changeTrailerHiddenLink;
    }

    public String changeTrailerHiddenAction() {
        if (!isNavigationExecuted()) {
            if (!Boolean.TRUE.equals(getPageFlowBean().getExecutedKey())) {
                keyNextItemTrailer();
            }
            getPageFlowBean().setExecutedKey(null);
        }
        return null;
    }

    public String keyNextItemTrailer() {
        // String newValue = getPageFlowBean().getTrailerId();
        String newValue = getPageFlowBean().getTrailerId();
        if (null == newValue || newValue.isEmpty()) {
            // showMessage("E", "PARTIAL_ENTRY", trailerControl);
            hideMessages();
            return null;
        } else {
            getPageFlowBean().setDisableInOutSealNbr(false);
            hideMessages();
            if (!hasData() || !isNew()) { //OTM3047
                if (!executeOperation("ensureTrailer")) {
                    logger.severe("Could not execute operation 'ensureTrailer'");
                    return null;
                }
                String result = (String) getOperationResult("ensureTrailer");
                if (null != result) {
                    showMessage("E", result, trailerControl);
                    return null;
                }
                if (hasData()) {
                    getPageFlowBean().restoreTroubleCode();
                }
            } else if (hasData() && isNew() && !newValue.equals(ADFUtils.getBoundAttributeValue("TrailerId"))) {
                //To fix error of OTM4157, changing trailerId was not changing the row value
                ADFUtils.setBoundAttributeValue("TrailerId", newValue);
            }

            // OTM 2551
            this.disableAllFields();
            String trailerStatus = (String) ADFUtils.getBoundAttributeValue("TrailerStatus");
            if (isNew() || ModelUtils.in(trailerStatus, "CHECKED-OUT", "IN-TRANSIT", "PENDXFER")) {
                focusField(zoneControl);
                if (ModelUtils.in(trailerStatus, "IN-TRANSIT", "PENDXFER") &&
                    StringUtils.isNotEmpty(getPageFlowBean().getInOut())) {
                    //OTM 3568, disable IN_OUT and SEAL_NBR



                    //TRAILER_STATUS IN ('IN-TRANSIT','PENDXFER') and exist in SHUTTLE_MANIFEST table
                    getPageFlowBean().setDisableInOutSealNbr(true);
                }
            } else {
                focusField(troubleCodeControl);
            }
        }
        return null;
    }

    /**
     * To simulate ENTER + IF FORM_SUCCESS original from 6i
     */
    private boolean formSuccess() {
        if (getYardLocationControl() != null && !getYardLocationControl().isDisabled()) {
            onChangedYardLocationId(null);
        }
        return !fieldErrorExists();
    }

    private void restoreFocus() {
        getPageFlowBean().setForceFocusPage1(getPageFlowBean().getLastFocus());
        onRegionLoad(null);
    }

    public void setChangeTroubleHiddenLink(RichLink changeTroubleHiddenLink) {
        this.changeTroubleHiddenLink = changeTroubleHiddenLink;
    }

    public RichLink getChangeTroubleHiddenLink() {
        return changeTroubleHiddenLink;
    }

    public String changeTroubleHiddenAction() {
        if (!isNavigationExecuted()) {
            if (!getPageFlowBean().getExecutedKey()) {
                hideMessages();
                List errors = validateTroubleCode(getPageFlowBean().getTroubleCode());
                if (errors != null && errors.size() == 2) {
                    showMessage((String) errors.get(0), (String) errors.get(1), troubleCodeControl);
                } else {
                    if (!hasData()) {
                        focusField(carrierControl);
                    } else {
                        //OTM 3569 and 3570
                        //Erik's mail on July 26th 2016:
                        //It is the behavior of this screen to fire the triggers of all of the fields after Pressing Enter Key on TRB field.
                        //We cannot find the logic that specifically call the trigger validation.
                        //What happen here is after pressing Enter, validation on Carrier and Trailer have been triggered
                        //and passed the validation and when come to Zone field, it again fires the trigger and displays the error messages.
                        List zoneErrors = (List) ADFUtils.findOperation("validateZonePl").execute();
                        if (zoneErrors == null) {
                            focusAfterTrouble();
                        } else if ("CONT_MOVE_WH_ID".equals(zoneErrors.get(1))) {
                            disableAllFields();
                            askUser(confirmationPopup, false, "CONT_MOVE_WH_ID",
                                    getBeanListener("valZoneFromTroubleListener"));
                        } else {
                            focusField(zoneControl);
                            showMessage((String) zoneErrors.get(0), (String) zoneErrors.get(1), zoneControl);
                        }
                    }
                }
            }
            getPageFlowBean().setExecutedKey(null);
        }
        return null;
    }

    public void valZoneFromTroubleListener(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            focusAfterTrouble();
        } else {
            focusField(zoneControl);
            //addErrorStyleToComponent(zoneControl);
        }
        restoreFocus();
    }

    private void focusAfterTrouble() {
        // OTM 2551
        String trailerStatus = (String) ADFUtils.getBoundAttributeValue("TrailerStatus");
        if (isNew() || ModelUtils.in(trailerStatus, "CHECKED-OUT", "IN-TRANSIT", "PENDXFER")) {
            focusField(carrierControl);
        }
    }

    private boolean isNew() {
        return Boolean.TRUE.equals(ADFUtils.getBoundAttributeValue("IsNew"));
    }
}
