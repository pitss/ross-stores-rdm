package com.ross.rdm.trailermgmt.hhtrailercheckins.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import javax.faces.event.ActionEvent;

import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.NavigatableRowIterator;

import org.apache.myfaces.trinidad.event.SelectionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMTrailerManagementBackingBean {

    private RichTable carrierTable;
    private RichTable troubleCodeTable;

    private static final String FIRST_RECORD = "FIRST RECORD";
    private static final String LAST_RECORD = "LAST RECORD";
    private static final String HH_TRAILER_CHECK_IN_S_BEAN = "HhTrailerCheckInSBean";


    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f7Link;
    private RichLink f8Link;

    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);


    public Page2Backing() {
    }

    protected HhTrailerCheckInSBean getPageFlowBean() {
        return (HhTrailerCheckInSBean) getPageFlowBean("HhTrailerCheckInSBean");
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
            }
        }
    }

    private void trKeyIn(String key) {
        DCIteratorBinding ib;
        int crow;
        long trow;

        if (getIsCarrierCode())
            ib = ADFUtils.findIterator("CarrierIterator");

        else
            ib = ADFUtils.findIterator("TrailerTroubleCodeIterator");

        crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        trow = ib.getEstimatedRowCount();

        if ("F3".equalsIgnoreCase(key)) {
            ib.setCurrentRowIndexInRange(0);
            goBack(true);
        }
        if ("F4".equalsIgnoreCase(key)) {
            HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
            if (pageFlowBean.isIsCarrierCode()) {
                pageFlowBean.setCarrierCode((String) ADFUtils.getBoundAttributeValue("SelectedCarrierCode"));
            } else {
                pageFlowBean.setTroubleCode((String) ADFUtils.getBoundAttributeValue("SelectedTroubleCode"));
            }
            ib.setCurrentRowIndexInRange(0);
            goBack(false);
        } else if ("F7".equalsIgnoreCase(key)) {
            if (crow - 5 <= 0) {
                this.showMessagesPanel("I", FIRST_RECORD);
                ib.setCurrentRowIndexInRange(0);
            } else
                ib.setCurrentRowIndexInRange(crow - 5);
            this.hideMessagesPanel();
        }

        else if ("F8".equalsIgnoreCase(key)) {
            if (crow + 5 >= trow - 1) {
                this.showMessagesPanel("I", LAST_RECORD);
                ib.setCurrentRowIndexInRange((int) trow - 1);

            } else
                ib.setCurrentRowIndexInRange(crow + 5);
            this.hideMessagesPanel();
        }
        this.refreshContentOfUIComponent(getCarrierTable());
    }

    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib;
        int crow;
        long trow;

        if (getIsCarrierCode())
            ib = ADFUtils.findIterator("CarrierIterator");

        else
            ib = ADFUtils.findIterator("TrailerTroubleCodeIterator");

        crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        trow = ib.getEstimatedRowCount();

        if (crow == trow - 1) {
            this.showMessagesPanel("I", LAST_RECORD);
        } else if (crow == 0) {
            this.showMessagesPanel("I", FIRST_RECORD);
        } else {
            this.hideMessagesPanel();
        }
    }

    private void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("hideMessagesPanel() End");
    }

    public void tableRowSelectionListener(SelectionEvent selectionEvent) {
        if (getIsCarrierCode()) {
            JSFUtils.resolveMethodExpression("#{bindings.Carrier.collectionModel.makeCurrent}", SelectionEvent.class, new Class[] {
                                             SelectionEvent.class }, new Object[] { selectionEvent });
            this.showFirstLastRecordInfo();
        }
        if (!getIsCarrierCode()) {
            JSFUtils.resolveMethodExpression("#{bindings.trailerTroubleCode.collectionModel.makeCurrent}",
                                             SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                             selectionEvent });
            this.showFirstLastRecordInfo();
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (!this.getPageFlowBean().isNotAllowed()) {
            this.showFirstLastRecordInfo();
        }
        _logger.info("onRegionLoad End");
    }

    protected void showMessagesPanel(String messageType, String msg) {
        _logger.info("showMessagesPanel() Start");
        // CLEAR PREVIOUS MESSAGES
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if ("W".equals(messageType)) {
            this.getErrorMessageIcon().setName("warning");
        } else if ("I".equals(messageType) || "M".equals(messageType)) {
            this.getErrorMessageIcon().setName("info");
        } else {
            this.getErrorMessageIcon().setName("error");
        }
        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getErrorPanel());
        _logger.info("showMessagesPanel() End");
    }

    public void setIsCarrierCode(Boolean isCarrierCode) {
        ((HhTrailerCheckInSBean) this.getPageFlowBean(HH_TRAILER_CHECK_IN_S_BEAN)).setIsCarrierCode(isCarrierCode);
    }

    public Boolean getIsCarrierCode() {
        return ((HhTrailerCheckInSBean) this.getPageFlowBean(HH_TRAILER_CHECK_IN_S_BEAN)).isIsCarrierCode();
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CODES_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)) {
            this.trKeyIn("F4");
        } else {
            this.showMessage("E", "NOT_ALLOWED", null);
            this.getPageFlowBean().setNotAllowed(true);
        }
    }

    public void f7ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CODES_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)) {
            this.trKeyIn("F7");
        } else {
            this.showMessage("E", "NOT_ALLOWED", null);
            this.getPageFlowBean().setNotAllowed(true);
        }
    }

    public void f8ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_trailer_check_in_s_CODES_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_trailer_check_in_s)) {
            this.trKeyIn("F8");
        } else {
            this.showMessage("E", "NOT_ALLOWED", null);
            this.getPageFlowBean().setNotAllowed(true);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // accessors
    ////////////////////////////////////////////////////////////////////////////////////

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public static void setLogger(ADFLogger _logger) {
        Page2Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setCarrierTable(RichTable carrierTable) {
        this.carrierTable = carrierTable;
    }

    public RichTable getCarrierTable() {
        return carrierTable;
    }

    public void setTroubleCodeTable(RichTable troubleCodeTable) {
        this.troubleCodeTable = troubleCodeTable;
    }

    public RichTable getTroubleCodeTable() {
        return troubleCodeTable;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    private void goBack(boolean isExit) {
        HhTrailerCheckInSBean pageFlowBean = getPageFlowBean();
        if (pageFlowBean.isIsCarrierCode()) {
            if (isExit) {
                pageFlowBean.setForceFocusPage1("car");
            } else {
                pageFlowBean.setForceFocusPage1("tra");
            }
        } else {
            pageFlowBean.setForceFocusPage1("tro");
        }
        ADFUtils.invokeAction("Back");
    }
}
