package com.ross.rdm.trailermgmt.hhdctodcrcvngs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends Dc2dcBaseBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private static final String DOOR_BINDING = "Door1";
    private static final String TRAILER_BINDING = "TrailerId1";
    private static final String SEAL_BINDING = "SealNbr1";
    private static final String LOCATION_BINDING = "LocationId1";
    private static final String FACILITY_BINDING = "FacilityId";
    private RichLink f3Link;
    private RichLink f5Link;
    private RichLink f6Link;
    private RichLink f7Link;
    private RichLink f8Link;
    private RichInputText door;
    private RichIcon doorIcon;
    private RichInputText trailerId;
    private RichIcon trailerIdIcon;
    private RichInputText sealNbr;
    private RichIcon sealNbrIcon;
    private RichInputText locationId;
    private RichIcon locationIdIcon;
    private RichPopup confirmationPopup;
    private RichOutputText confirmationPopupText;
    private RichLink confirmationPopupYesLink;
    private RichLink confirmationPopupNoLink;
    private RichLink validateDoorHiddenButton;
    private RichLink validateTrailerHiddenButton;
    private RichLink validateLocationHiddenButton;

    public Page1Backing() {

    }

    public void onChangedDoor(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateDoorHiddenButton());
            actionEvent.queue();
        }
    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateTrailerHiddenButton());
            actionEvent.queue();
        }
    }

    public void onChangedLocationId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getValidateLocationHiddenButton());
            actionEvent.queue();
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF8Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    public String f3Action() {
        return logoutExitBTF();
    }

    public String f5Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F5,
                                                RoleBasedAccessConstants.FORM_NAME_hh_dctodc_rcvng_s)) {
            if (checkRequiredField(DOOR_BINDING, getDoor(), getDoorIcon(), HhDctodcRcvngSBean.DOOR_ID) &&
                checkRequiredField(TRAILER_BINDING, getTrailerId(), getTrailerIdIcon(),
                                   HhDctodcRcvngSBean.TRAILER_ID) &&
                checkRequiredField(SEAL_BINDING, getSealNbr(), getSealNbrIcon(), HhDctodcRcvngSBean.SEAL_NBR) &&
                checkValidTrailerDoor() && validateSelectedField()) {
                List errorMessages = (List) ADFUtils.findOperation("openTrailer").execute();
                refreshContentOfUIComponent(getPflMain());
                if (errorMessages != null) {
                    if (errorMessages.size() > 1) {
                        if (ERROR.equals(errorMessages.get(0)) || WARN.equals(errorMessages.get(0))) {
                            setErrorInSelectedField();
                        } else if ("M".equals(errorMessages.get(0))) {
                            this.removeAllErrors();
                            if (errorMessages.size() > 2) {
                                switchFocusToField((String) errorMessages.get(2));
                            }
                        }
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                }
            }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }

    public void f6Action() {
        String nav = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F6,
                                                RoleBasedAccessConstants.FORM_NAME_hh_dctodc_rcvng_s)) {
        if (checkRequiredField(DOOR_BINDING, getDoor(), getDoorIcon(), HhDctodcRcvngSBean.DOOR_ID) &&
            checkRequiredField(TRAILER_BINDING, getTrailerId(), getTrailerIdIcon(), HhDctodcRcvngSBean.TRAILER_ID) &&
            checkRequiredField(SEAL_BINDING, getSealNbr(), getSealNbrIcon(), HhDctodcRcvngSBean.SEAL_NBR) &&
            checkValidTrailerDoor() && validateSelectedField() && validateUnloadingStatus()) {

            OperationBinding op = ADFUtils.findOperation("gScpWh");
            op.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue(FACILITY_BINDING));
            op.getParamsMap().put("wh", ADFUtils.getBoundAttributeValue("FromWhId"));
            op.getParamsMap().put("featureName", "separate_rdm");
            String result = (String) op.execute();
            BigDecimal shuttleBolNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("ShuttleBolNbr");
            BigDecimal smBolNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("SmBolNbr");
            if (YES.equals(result) ||
                (NO.equals(result) &&
                 !(shuttleBolNbr != null ? shuttleBolNbr : BigDecimal.valueOf(0)).equals(smBolNbr != null ? smBolNbr :
                                                                                         BigDecimal.valueOf(0)))) {
                op = ADFUtils.findOperation("continueF4LogicPage1");
                List<String> resultF4 = (List<String>) op.execute();
                if (resultF4 != null) {
                    if (resultF4.size() == 1) {
                        nav = resultF4.get(0);
                    } else if (resultF4.size() == 2) {
                        if (CONF.equals(resultF4.get(0))) {
                            showConfirmationPopup("TRLR_EMPTY", resultF4.get(1));
                        } else {
                            setErrorInSelectedField();
                            showMessagesPanel(resultF4.get(0), resultF4.get(1));
                        }
                    }
                }
            } else {
                finishCloseTrailer();
            }
        }

        if ("goUnloadTeam".equals(nav)) {
            this.setUnloadTeamsParams();
            nav = "goUnloadTeam";
            ADFUtils.invokeAction("goUnloadTeam");
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        //return nav;
    }

    private void setUnloadTeamsParams() {
        ADFUtils.setBoundAttributeValue("UnloadActivity", "TEAMS");
        ADFUtils.findOperation("ctnsUnload").execute();
        ADFUtils.setBoundAttributeValue("GlobalTrailerId", ADFUtils.getBoundAttributeValue("TrailerId1"));
        ADFUtils.setBoundAttributeValue("GlobalBolNbr", ADFUtils.getBoundAttributeValue("MasterBolId"));
        ADFUtils.setBoundAttributeValue("GlobalUnloadActivity", ADFUtils.getBoundAttributeValue("UnloadActivity"));
        ADFUtils.setBoundAttributeValue("GlobalTotalCnt", ADFUtils.getBoundAttributeValue("TotalCnt"));

    }

    public String f7Action() {
        String nav = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_dctodc_rcvng_s)) {
        if (validateInvalidAction() &&
            checkRequiredField(DOOR_BINDING, getDoor(), getDoorIcon(), HhDctodcRcvngSBean.DOOR_ID) &&
            checkRequiredField(TRAILER_BINDING, getTrailerId(), getTrailerIdIcon(), HhDctodcRcvngSBean.TRAILER_ID) &&
            checkRequiredField(SEAL_BINDING, getSealNbr(), getSealNbrIcon(), HhDctodcRcvngSBean.SEAL_NBR) &&
            validateSelectedField()) {
            if ("UNLOADING".equals(ADFUtils.getBoundAttributeValue("TrailerStatus"))) {
                this.setUnloadTeamsParams();
                nav = "goUnloadTeam";
            } else {
                setErrorInSelectedField();
                showMessagesPanel(ERROR, getMessage("INV_TRAIL_STAT"));
            }
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return nav;
    }

    public String f8Action() {
        String nav = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_dctodc_rcvng_s_DCTODC_RECEIVE_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_dctodc_rcvng_s)) {
        if (checkRequiredField(DOOR_BINDING, getDoor(), getDoorIcon(), HhDctodcRcvngSBean.DOOR_ID) &&
            checkRequiredField(TRAILER_BINDING, getTrailerId(), getTrailerIdIcon(), HhDctodcRcvngSBean.TRAILER_ID) &&
            checkRequiredField(SEAL_BINDING, getSealNbr(), getSealNbrIcon(), HhDctodcRcvngSBean.SEAL_NBR) &&
            checkRequiredField(LOCATION_BINDING, getLocationId(), getLocationIdIcon(),
                               HhDctodcRcvngSBean.LOCATION_ID) && checkValidTrailerDoor() && validateSelectedField()) {
            List errorMessages = (List) ADFUtils.findOperation("valUnloadTrailer").execute();
            refreshContentOfUIComponent(getPflMain());
            if (errorMessages != null) {
                if (errorMessages.size() == 1) {
                    nav = (String) errorMessages.get(0);
                } else if (errorMessages.size() == 2) {
                    if (ERROR.equals(errorMessages.get(0)) || WARN.equals(errorMessages.get(0))) {
                        setErrorInSelectedField();
                    }
                    showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                } else if (errorMessages.size() == 3) {
                    if (CONF.equals(errorMessages.get(0))) {
                        showConfirmationPopup((String) errorMessages.get(2), (String) errorMessages.get(1));
                    } else {
                        //This case shouldn't happen
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                }
            }
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return nav;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    public void setDoor(RichInputText door) {
        this.door = door;
    }

    public RichInputText getDoor() {
        return door;
    }

    public void setDoorIcon(RichIcon doorIcon) {
        this.doorIcon = doorIcon;
    }

    public RichIcon getDoorIcon() {
        return doorIcon;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setSealNbr(RichInputText sealNbr) {
        this.sealNbr = sealNbr;
    }

    public RichInputText getSealNbr() {
        return sealNbr;
    }

    public void setSealNbrIcon(RichIcon sealNbrIcon) {
        this.sealNbrIcon = sealNbrIcon;
    }

    public RichIcon getSealNbrIcon() {
        return sealNbrIcon;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        HhDctodcRcvngSBean bean = getPageFlowBean();
        if (bean != null) {
            if (!bean.getDisabledDoor()) {
                setFocusOnUIComponent(getDoor());
            } else if (!bean.getDisabledTrailerId()) {
                setFocusOnUIComponent(getTrailerId());
            } else if (!bean.getDisabledSealNbr()) {
                setFocusOnUIComponent(getSealNbr());
            } else if (!bean.getDisabledLocationId()) {
                setFocusOnUIComponent(getLocationId());
            }
            if (bean.getCallCloseContainer()) {
                this.finishCloseTrailer();
                bean.setCallCloseContainer(false);
            }
        }
    }

    private void queueValueChangeEvent(RichInputText field, String submittedVal) {
        switch (field.getId()) {
        case "doo1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedDoor(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "tra1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedTrailerId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "sea1":
            switchFocusToField(HhDctodcRcvngSBean.LOCATION_ID);
            break;
        case "loc1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedLocationId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        default:

        }
    }

    private void switchFocusToField(String focus) {
        removeAllErrors();
        getPageFlowBean().setPage1Focus(focus);
        if (HhDctodcRcvngSBean.DOOR_ID.equals(focus)) {
            setFocusOnUIComponent(getDoor());
        } else if (HhDctodcRcvngSBean.TRAILER_ID.equals(focus)) {
            setFocusOnUIComponent(getTrailerId());
        } else if (HhDctodcRcvngSBean.SEAL_NBR.equals(focus)) {
            setFocusOnUIComponent(getSealNbr());
        } else if (HhDctodcRcvngSBean.LOCATION_ID.equals(focus)) {
            setFocusOnUIComponent(getLocationId());
        }
    }

    private void removeAllErrors() {
        removeErrorOfField(getDoor(), getDoorIcon());
        removeErrorOfField(getTrailerId(), getTrailerIdIcon());
        removeErrorOfField(getSealNbr(), getSealNbrIcon());
        removeErrorOfField(getLocationId(), getLocationIdIcon());
        hideMessagesPanel();
    }

    private boolean checkRequiredField(String attribute, RichInputText input, RichIcon icon, String focus) {
        boolean valid = true;
        if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue(attribute))) {
            /* if(focus != null){
                switchFocusToField(input, focus);
            } */
            //setErrorOnField(input, icon);
            setErrorInSelectedField();
            showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
            valid = false;
        }
        return valid;
    }

    private boolean checkValidTrailerDoor() {
        boolean valid = true;
        OperationBinding op = ADFUtils.findOperation("checkValidTrailerDoor");
        op.getParamsMap().put("facility", ADFUtils.getBoundAttributeValue(FACILITY_BINDING));
        op.getParamsMap().put("trailer", ADFUtils.getBoundAttributeValue(TRAILER_BINDING));
        op.getParamsMap().put("door", ADFUtils.getBoundAttributeValue(DOOR_BINDING));
        Boolean resultValid = (Boolean) op.execute();
        if (Boolean.FALSE.equals(resultValid)) {
            valid = false;
            switchFocusToField(HhDctodcRcvngSBean.DOOR_ID);
            setErrorOnField(getDoor(), getDoorIcon());
            showMessagesPanel(ERROR, getMessage("TRLR_NOT_AT_DR"));
        }
        return valid;
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setConfirmationPopupText(RichOutputText confirmationPopupText) {
        this.confirmationPopupText = confirmationPopupText;
    }

    public RichOutputText getConfirmationPopupText() {
        return confirmationPopupText;
    }

    public void setConfirmationPopupYesLink(RichLink confirmationPopupYesLink) {
        this.confirmationPopupYesLink = confirmationPopupYesLink;
    }

    public RichLink getConfirmationPopupYesLink() {
        return confirmationPopupYesLink;
    }

    public void setConfirmationPopupNoLink(RichLink confirmationPopupNoLink) {
        this.confirmationPopupNoLink = confirmationPopupNoLink;
    }

    public RichLink getConfirmationPopupNoLink() {
        return confirmationPopupNoLink;
    }

    public void confirmationPopupPerformKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmationPopupYesLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmationPopupNoLink());
                actionEvent.queue();
            }
        }
    }

    public String confirmationPopupYesAction() {
        String nav = null;
        getConfirmationPopup().hide();
        getPageFlowBean().setDisableAllFields(false);
        refreshContentOfUIComponent(getPflMain());
        String code = getConfirmationPopup().getLauncherVar();
        List errorMessages = null;
        switch (code) {
        case "UNLOAD_ALL_CONT":
            errorMessages = (List) ADFUtils.findOperation("unloadAllContainer").execute();
            if (errorMessages != null && errorMessages.size() == 2) {
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            }
            break;
        case "UNLOAD_BY_CONT":
            errorMessages = (List) ADFUtils.findOperation("ctnsUnload").execute();
            if (errorMessages != null && errorMessages.size() == 1) {
                nav = (String) errorMessages.get(0);
            }
            break;
        case "UNLOAD_BY_TRLR":
            errorMessages = (List) ADFUtils.findOperation("processTrailerUnload").execute();
            if (errorMessages != null && errorMessages.size() == 2) {
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            } else {
                this.isLogErrorRollOperation(errorMessages);
            }
            break;
        case "TRLR_EMPTY":
            if (YES.equals(ADFUtils.getBoundAttributeValue("DmsTeam"))) {

                ADFUtils.setBoundAttributeValue("GlobalTrailerId", ADFUtils.getBoundAttributeValue("TrailerId1"));
                ADFUtils.setBoundAttributeValue("GlobalBolNbr", ADFUtils.getBoundAttributeValue("MasterBolId"));
                ADFUtils.setBoundAttributeValue("GlobalUnloadActivity",
                                                ADFUtils.getBoundAttributeValue("UnloadActivity"));
                ADFUtils.setBoundAttributeValue("GlobalTotalCnt", ADFUtils.getBoundAttributeValue("TotalCnt"));

                ADFUtils.setBoundAttributeValue("ForceReceive", YES);
                nav = "goUnloadTeam"; // CALL_FORM( lower('hh_unload_team_s') ,NO_HIDE);
            } else {
                ADFUtils.setBoundAttributeValue("ForceReceive", YES);

                finishCloseTrailer();
                refreshContentOfUIComponent(getPflMain());
            }
            break;
        default:

        }
        return nav;
    }

    public String confirmationPopupNoAction() {
        getConfirmationPopup().hide();
        getPageFlowBean().setDisableAllFields(false);
        refreshContentOfUIComponent(getPflMain());
        if ("UNLOAD_BY_CONT".equals(getConfirmationPopup().getLauncherVar())) {
            showConfirmationPopup("UNLOAD_BY_TRLR", getMessage("UNLOAD_BY_TRLR"));
        } else if ("UNLOAD_BY_TRLR".equals(getConfirmationPopup().getLauncherVar())) {
            switchFocusToField(HhDctodcRcvngSBean.TRAILER_ID);
        }
        //For the rest of messages (UNLOAD_ALL_CONT and UNLOAD_BY_TRLR) do nothing, just close
        return null;
    }

    private void showConfirmationPopup(String code, String message) {
        getConfirmationPopup().setLauncherVar(code);
        getConfirmationPopupText().setValue(message);
        getConfirmationPopup().show(new RichPopup.PopupHints());
        getPageFlowBean().setDisableAllFields(true);
        refreshContentOfUIComponent(getPflMain());
    }

    private boolean validateDoor(boolean focusNext) {
        boolean valid = true;
        String door = (String) ADFUtils.getBoundAttributeValue(DOOR_BINDING);
        OperationBinding op = ADFUtils.findOperation("validateDoorId");
        op.getParamsMap().put("doorId", StringUtils.isEmpty(door) ? null : door.toUpperCase());
        List errorMessages = (List) op.execute();
        if (errorMessages != null && errorMessages.size() == 1) {

            setErrorOnField(getDoor(), getDoorIcon());
            showMessagesPanel((String) errorMessages.get(0), (String) getMessage("INV_DOOR", null, null, null));
            valid = false;
        } else if (focusNext) {
            switchFocusToField(HhDctodcRcvngSBean.TRAILER_ID);
        }
        return valid;
    }

    private boolean validateTrailer(boolean focusNext) {
        boolean valid = true;
        List errorMessages = (List) ADFUtils.findOperation("validateTrailerId").execute();
        if (errorMessages != null && errorMessages.size() == 2) {
            setErrorOnField(getTrailerId(), getTrailerIdIcon());
            showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            valid = false;
        } else if (focusNext) {
            switchFocusToField(HhDctodcRcvngSBean.SEAL_NBR);
        }
        return valid;
    }

    private boolean validateLocation(boolean focusNext) {
        boolean valid = true;
        List errorMessages = (List) ADFUtils.findOperation("validateLocationId").execute();
        refreshContentOfUIComponent(getPflMain());
        if (errorMessages != null && errorMessages.size() == 2) {
            setErrorOnField(getLocationId(), getLocationIdIcon());
            showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            valid = false;
        } else if (focusNext) {
            switchFocusToField(HhDctodcRcvngSBean.DOOR_ID);
        }
        return valid;
    }

    public void setValidateDoorHiddenButton(RichLink validateDoorHiddenButton) {
        this.validateDoorHiddenButton = validateDoorHiddenButton;
    }

    public RichLink getValidateDoorHiddenButton() {
        return validateDoorHiddenButton;
    }

    public void setValidateTrailerHiddenButton(RichLink validateTrailerHiddenButton) {
        this.validateTrailerHiddenButton = validateTrailerHiddenButton;
    }

    public RichLink getValidateTrailerHiddenButton() {
        return validateTrailerHiddenButton;
    }

    public void setValidateLocationHiddenButton(RichLink validateLocationHiddenButton) {
        this.validateLocationHiddenButton = validateLocationHiddenButton;
    }

    public RichLink getValidateLocationHiddenButton() {
        return validateLocationHiddenButton;
    }

    public String validateDoor() {
        if (!getPageFlowBean().getExecutedKey()) {
            validateDoor(true);
        }
        getPageFlowBean().setExecutedKey(null);
        return null;
    }

    public String validateTrailer() {
        if (!getPageFlowBean().getExecutedKey()) {
            validateTrailer(true);
        }
        getPageFlowBean().setExecutedKey(null);
        return null;
    }

    public String validateLocation() {
        if (getPageFlowBean() != null && getPageFlowBean().getExecutedKey() != null) {
            if (!getPageFlowBean().getExecutedKey()) {
                validateLocation(true);
            }
        }
        getPageFlowBean().setExecutedKey(null);
        return null;
    }

    /**
     * This methods emulates the ENTER built-in of Forms (validate current item)
     */
    private boolean validateSelectedField() {
        boolean valid = true;
        if (HhDctodcRcvngSBean.DOOR_ID.equals(getPageFlowBean().getPage1Focus())) {
            valid = validateDoor(false);
        } else if (HhDctodcRcvngSBean.TRAILER_ID.equals(getPageFlowBean().getPage1Focus())) {
            valid = validateTrailer(false);
        } else if (HhDctodcRcvngSBean.SEAL_NBR.equals(getPageFlowBean().getPage1Focus())) {
            valid = true;
        } else if (HhDctodcRcvngSBean.LOCATION_ID.equals(getPageFlowBean().getPage1Focus())) {
            valid = validateLocation(false);
        }
        return valid;
    }

    private void setErrorInSelectedField() {
        this.removeAllErrors();
        if (HhDctodcRcvngSBean.DOOR_ID.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getDoor(), getDoorIcon());
        } else if (HhDctodcRcvngSBean.TRAILER_ID.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getTrailerId(), getTrailerIdIcon());
        } else if (HhDctodcRcvngSBean.SEAL_NBR.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getSealNbr(), getSealNbrIcon());
        } else if (HhDctodcRcvngSBean.LOCATION_ID.equals(getPageFlowBean().getPage1Focus())) {
            setErrorOnField(getLocationId(), getLocationIdIcon());
        }
    }

    private boolean validateInvalidAction() {
        boolean valid = true;
        OperationBinding op = ADFUtils.findOperation("gScpWh");
        op.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue(FACILITY_BINDING));
        op.getParamsMap().put("wh", ADFUtils.getBoundAttributeValue("FromWhId"));
        op.getParamsMap().put("featureName", "separate_rdm");
        String result = (String) op.execute();
        BigDecimal shuttleBolNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("ShuttleBolNbr");
        BigDecimal smBolNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("SmBolNbr");
        if (NO.equals(result) &&
            !(shuttleBolNbr != null ? shuttleBolNbr : BigDecimal.valueOf(0)).equals(smBolNbr != null ? smBolNbr :
                                                                                    BigDecimal.valueOf(0))) {
            /* OTM -> 2759
            switchFocusToField(getTrailerId(), HhDctodcRcvngSBean.TRAILER_ID); */
            setErrorInSelectedField();
            showMessagesPanel(ERROR, getMessage("INV_ACTION_LVP"));
            valid = false;
        }
        return valid;
    }

    private boolean validateUnloadingStatus() {
        OperationBinding op = ADFUtils.findOperation("isUnloadingStatus");
        op.getParamsMap().put("facility", ADFUtils.getBoundAttributeValue(FACILITY_BINDING));
        op.getParamsMap().put("trailer", ADFUtils.getBoundAttributeValue(TRAILER_BINDING));
        Boolean result = (Boolean) op.execute();
        if (!Boolean.TRUE.equals(result)) {
            /* OTM -> 2759
            switchFocusToField(getTrailerId(), HhDctodcRcvngSBean.TRAILER_ID); */
            setErrorInSelectedField();
            showMessagesPanel(ERROR, getMessage("INV_TRAIL_STAT"));
        }
        return Boolean.TRUE.equals(result);
    }

    private void finishCloseTrailer() {
        List errorMessages = (List) ADFUtils.findOperation("closeTrailer").execute();
        if (errorMessages != null && errorMessages.size() == 2) {
            showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
        } else {
            this.isLogErrorRollOperation(errorMessages);
        }
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_trailermgmt_hhdctodcrcvngs_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }
}
