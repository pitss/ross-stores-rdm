package com.ross.rdm.trailermgmt.hhdctodcrcvngs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends Dc2dcBaseBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichInputText containerId;
    private RichIcon containerIdIcon;
    private RichLink f3Link;

    public Page2Backing() {
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            removeAllErrors();
            updateModel(vce);
            List errorMessages = (List) ADFUtils.findOperation("kniContainerIdDc2dc").execute();
            if (errorMessages != null) {
                if (!isLogErrorRollOperation(errorMessages)) {
                    if (errorMessages.size() == 3 && HhDctodcRcvngSBean.CONTAINER_ID.equals(errorMessages.get(2))) {
                        switchFocusToField(getContainerId(), HhDctodcRcvngSBean.CONTAINER_ID);
                    }
                    if (errorMessages.size() > 1) {
                        if (ERROR.equals(errorMessages.get(0)) || WARN.equals(errorMessages.get(0))) {
                            setErrorOnField(getContainerId(), getContainerIdIcon());
                        }
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                }
            }
            refreshContentOfUIComponent(getPflMain());
        }
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public String f3Action() {
        ADFUtils.findOperation("clearBlock").execute();
        getPageFlowBean().setPage1Focus(HhDctodcRcvngSBean.DOOR_ID);
        return "goPage1";
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) &&
                       "con1".equals(field.getId())) {
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                    submittedVal.equals(field.getValue())) {
                    onChangedContainerId(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledContainerId()) {
            setFocusOnUIComponent(getContainerId());
        }
    }

    private void switchFocusToField(RichInputText input, String focus) {
        removeAllErrors();
        setFocusOnUIComponent(input);
    }

    private void removeAllErrors() {
        removeErrorOfField(getContainerId(), getContainerIdIcon());
        hideMessagesPanel();
    }
    
    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_trailermgmt_hhdctodcrcvngs_view_pageDefs_Page2PageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
