package com.ross.rdm.trailermgmt.hhdctodcrcvngs.view.bean;

import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;

import java.util.List;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class Dc2dcBaseBean extends RDMMobileBaseBackingBean {
    private RichPanelFormLayout pflMain;
    private RichPopup dmsErrorPopup;
    private RichOutputText dmsErrorOutputText;

    public Dc2dcBaseBean() {
        super();
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    protected boolean isLogErrorRollOperation(List errorMessages) {
        boolean isError =
            errorMessages != null &&
            ((errorMessages.size() == 3 && logoutExitBTF().equals(errorMessages.get(2))) ||
             (errorMessages.size() == 1 && logoutExitBTF().equals(errorMessages.get(0))));
        if (isError) {
            showDmsErrorPopup();
        }
        return isError;
    }

    public void setDmsErrorPopup(RichPopup dmsErrorPopup) {
        this.dmsErrorPopup = dmsErrorPopup;
    }

    public RichPopup getDmsErrorPopup() {
        return dmsErrorPopup;
    }

    public void setDmsErrorOutputText(RichOutputText dmsErrorOutputText) {
        this.dmsErrorOutputText = dmsErrorOutputText;
    }

    public RichOutputText getDmsErrorOutputText() {
        return dmsErrorOutputText;
    }

    private void showDmsErrorPopup() {
        this.getPageFlowBean().setDisableAllFields(true);
        this.getDmsErrorOutputText().setValue(getMessage(DMS_ERROR));
        this.getDmsErrorPopup().show(new RichPopup.PopupHints());
    }

    protected HhDctodcRcvngSBean getPageFlowBean() {
        return (HhDctodcRcvngSBean) getPageFlowBean("HhDctodcRcvngSBean");
    }
}
