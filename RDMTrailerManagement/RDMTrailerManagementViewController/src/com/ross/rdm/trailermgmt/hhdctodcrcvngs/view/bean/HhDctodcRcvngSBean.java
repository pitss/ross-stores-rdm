package com.ross.rdm.trailermgmt.hhdctodcrcvngs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.common.NameMapping;

import java.io.Serializable;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhDctodcRcvngSBean implements Serializable{
    @SuppressWarnings("compatibility:-5072603818086744931")
    private static final long serialVersionUID = 1L;
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhDctodcRcvngSBean.class);
    
    private String page1Focus = null;
    private String page2Focus = null;
    private Boolean executedKey = Boolean.FALSE;
    public static final String DOOR_ID = "DOOR_ID";
    public static final String TRAILER_ID = "TRAILER_ID";
    public static final String SEAL_NBR = "SEAL_NBR";
    public static final String LOCATION_ID = "LOCATION_ID";
    public static final String CONTAINER_ID = "UNLOAD_TRAILER.CONTAINER_ID";
    private boolean disableAllFields = false;
    private boolean callCloseContainer;

    public void initTaskFlow() {
        this.page1Focus = DOOR_ID;
        this.setCallCloseContainer(false);
        ADFUtils.findOperation("initTaskFlowDctodcRcvng").execute();
    }

    public void setPage1Focus(String page1Focus) {
        this.page1Focus = page1Focus;
    }

    public String getPage1Focus() {
        return page1Focus;
    }

    public void setPage2Focus(String page2Focus) {
        this.page2Focus = page2Focus;
    }

    public String getPage2Focus() {
        return page2Focus;
    }

    public Boolean getDisabledDoor() {
        return disableAllFields || (page1Focus != null && !DOOR_ID.equals(page1Focus));
    }

    public Boolean getDisabledTrailerId() {
        return disableAllFields || !TRAILER_ID.equals(page1Focus);
    }

    public Boolean getDisabledSealNbr() {
        return disableAllFields || !SEAL_NBR.equals(page1Focus);
    }

    public Boolean getDisabledLocationId() {
        return disableAllFields || !LOCATION_ID.equals(page1Focus);
    }

    public Boolean getDisabledContainerId() {
        return disableAllFields || (page2Focus != null && !CONTAINER_ID.equals(page2Focus));
    }

    public void setDisableAllFields(boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public boolean isDisableAllFields() {
        return disableAllFields;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
    
    public TaskFlowId getUnloadTeamSTaskFlowId(){
        // return "/WEB-INF/com/ross/rdm/shipping/hhunloadteams/view/taskflow/HhUnloadTeamSTaskFlow.xml";
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_unload_team_s", "shipping/"));
    }

    public void setCallCloseContainer(boolean callCloseContainer) {
        this.callCloseContainer = callCloseContainer;
    }

    public boolean getCallCloseContainer() {
        return callCloseContainer;
    }
}
