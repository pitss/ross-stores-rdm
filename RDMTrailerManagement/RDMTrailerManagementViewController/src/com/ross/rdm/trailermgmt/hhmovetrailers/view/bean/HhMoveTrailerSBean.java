package com.ross.rdm.trailermgmt.hhmovetrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;

import org.apache.commons.lang.StringUtils;


// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhMoveTrailerSBean {
    public static final String LOCATION_FIELD = "NewLocationField";
    public static final String TRAILER_FIELD = "TrailerIdField";
    private String trailerId; // Item/Parameter hh_move_trailer_s.TRAILER_ID
    private String newTrailerLocation; // Item/Parameter hh_move_trailer_s.NEW_TRAILER_LOCATION
    private String isFocusOn;
    private boolean disableAllFields = false;
    private boolean forceDisableTrailerId = false;

    public void initTaskFlow() {

        //Forms  Module : hh_move_trailer_s
        //Object        : WHEN-NEW-FORM-INSTANCE
        //Source Code   :
        //
        //Tr_Startup;
        //
        //startup_local;
        //AHL.SET_AHL_INFO(:WORK.FACILITY_ID,:SYSTEM.CURRENT_FORM);
        this.initGlobalVariablesMoveTrailer();
        this.initHhMoveTrailerWorkVariables();
        //TODO During page integration process: if parameter.trailer_id is not null then:
        // Following acts as if trailerid is not null
        this.initHhMoveTrailerWorkLocalVariables();
        // set the main block trailer id val to the incoming parameter
        this.callMoveTrailerDisplayForm();
        if("N".equals(ADFUtils.getBoundAttributeValue("TaskManagement"))){
            this.setIsFocusOn(TRAILER_FIELD);
        }else{
            ADFUtils.setBoundAttributeValue("NewLocation", getNewTrailerLocation());
            this.setIsFocusOn(LOCATION_FIELD);
            this.setForceDisableTrailerId(true);
        }
    }

    private void initGlobalVariablesMoveTrailer() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesMoveTrailer");
        oper.execute();
    }

    private void initHhMoveTrailerWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhMoveTrailerWorkVariables");
        oper.execute();
    }

    private void initHhMoveTrailerWorkLocalVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setHhMoveTrailerWorkLocalVariables");
        oper.execute();
        
        if(StringUtils.isNotEmpty(this.getTrailerId())){
            ADFUtils.setBoundAttributeValue("TaskManagement", "Y");
            ADFUtils.setBoundAttributeValue("TrailerId", this.getTrailerId());
        }else{
            ADFUtils.setBoundAttributeValue("TaskManagement", "N");
        }
    }

    private void callMoveTrailerDisplayForm() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("getMoveTrailerDisplayForm");
        oper.getParamsMap().put("trailerId", ADFUtils.getBoundAttributeValue("TrailerId"));
        oper.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
        oper.execute();
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setNewTrailerLocation(String newTrailerLocation) {
        this.newTrailerLocation = newTrailerLocation;
    }

    public String getNewTrailerLocation() {
        return newTrailerLocation;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }
    
    public Boolean getDisableTrailerId(){
        return forceDisableTrailerId || disableAllFields || !TRAILER_FIELD.equals(getIsFocusOn());
    }
    
    public Boolean getDisableLocationId(){
        return disableAllFields || !LOCATION_FIELD.equals(getIsFocusOn());
    }

    public void setDisableAllFields(boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public boolean isDisableAllFields() {
        return disableAllFields;
    }

    public void setForceDisableTrailerId(boolean forceDisableTrailerId) {
        this.forceDisableTrailerId = forceDisableTrailerId;
    }

    public boolean isForceDisableTrailerId() {
        return forceDisableTrailerId;
    }
}
