package com.ross.rdm.trailermgmt.hhmovetrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;
import com.ross.rdm.trailermgmt.hhmovetrailers.model.views.HhMoveTrailerSMainViewRowImpl;
import com.ross.rdm.trailermgmt.view.framework.RDMTrailerManagementBackingBean;

import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMTrailerManagementBackingBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(RDMHotelPickingBackingBean.class);
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String TRAILER_ID_ATTR = "TrailerId";
    private final static String NEW_LOCATION_ATTR = "NewLocation";
    private final static String HH_MOVE_TRAILER_FLOW_BEAN = "HhMoveTrailerSBean";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelFormLayout moveTrailerPanel;
    private RichLink f1Link;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichIcon trailerIdIcon;
    private RichIcon newLocationIcon;
    private RichInputText trailerId;
    private RichInputText newLocation;
    private RichInputText locationId;
    private RichInputText trailerStatus;
    private RichInputText lastUseDate;
    private RichInputText apptDate;
    private RichInputText doorId;
    private Boolean f1Hit;
    private Boolean f4Hit;
    private Boolean trailerIdValid;
    private Boolean newLocationValid;
    private RichPopup confirmChangeWarehousePopup;
    private RichDialog confirmChangeWarehouseDialog;
    private RichOutputText dialogConfirmChangeWarehouseOutputtext;

    public Page1Backing() {

    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        _logger.info("onChangedTrailerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String trailerId = ((String) vce.getNewValue());
            this.callVTrailerId(trailerId);
        }
        _logger.info("onChangedTrailerId() End");

    }

    private void callVTrailerId(String trailerId) {

        _logger.info("callVTrailerId() Start");
        String facilityId = getFacilityIdAttrValue();

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVTrailerId");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("trailerId", trailerId != null ? trailerId.toUpperCase() : null);
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                String returnValue = (String) oper.getResult();
                if ("N".equalsIgnoreCase(returnValue)) {
                    showMessage("error", getMessageText("INV_TRAILER"));
                    this.setTrailerIdError();
                    //this.selectTextOnUIComponent(this.getTrailerId());
                    _logger.info("callVTrailerId() End");
                    this.setTrailerIdValid(false);
                    return;
                }
            }
        }
        OperationBinding oper3 = (OperationBinding) ADFUtils.findOperation("callMTDisplayForm");
        Map map3 = oper3.getParamsMap();
        map3.put("facilityId", facilityId);
        map3.put("trailerId", trailerId != null ? trailerId.toUpperCase() : null);
        oper3.execute();
        if (oper3.getErrors().isEmpty()) {
            if (null != oper3.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if ("E".equalsIgnoreCase(codeList.get(0))) {
                    String msg = codeList.get(1);
                    showMessage("error", msg);
                } else {
                    showMessage("error", codeList.get(0));
                }
                this.setTrailerIdError();
                _logger.info("callVTrailerId() End");
                this.setTrailerIdValid(false);
                return;
            }
        }
        if (!this.getF1Hit()) {
            OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callVBadStatus");
            Map map2 = oper2.getParamsMap();
            map2.put("facilityId", facilityId);
            map2.put("trailerId", trailerId != null ? trailerId.toUpperCase() : null);
            oper2.execute();

            if (oper2.getErrors().isEmpty()) {
                if (null != oper2.getResult()) {
                    String returnValue2 = (String) oper2.getResult();
                    if ("N".equalsIgnoreCase(returnValue2)) {
                        showMessage("error", getMessageText("INV_STATUS"));
                        this.setTrailerIdError();
                        _logger.info("callVTrailerId() End");
                        this.setTrailerIdValid(false);
                        return;
                    }
                }
            }
        }
        ADFUtils.setBoundAttributeValue(TRAILER_ID_ATTR, trailerId != null ? trailerId.toUpperCase() : null);
        this.clearTrailerIdError();

        if (!this.getF1Hit()) {
            this.setFocusNewLocation();
            this.getNewLocation().resetValue();
            if (!this.getF4Hit()) {
                ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, null);
            }
        }
        this.refreshContentOfUIComponent(this.getMoveTrailerPanel());
        this.setTrailerIdValid(true);
        _logger.info("callVTrailerId() End");
        return;
    }

    public void onChangedNewLocation(ValueChangeEvent vce) {

        _logger.info("onChangedNewLocation() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String locationId = ((String) vce.getNewValue());
            this.callNewLocValidation(locationId);
        }
        _logger.info("onChangedNewLocation() End");

    }

    private void callNewLocValidation(String locationId) {

        _logger.info("callNewLocValidation() Start");
        String facilityId = getFacilityIdAttrValue();

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callNewLocValidation");
        Map map = oper.getParamsMap();
        map.put("iNewLocation", locationId);
        map.put("iFacilityId", facilityId);
        map.put("iTaskManagement", ADFUtils.getBoundAttributeValue("TaskManagement"));
        map.put("pNewTrailerLocation", getHhMoveTrailerSBeanPageFlowBean().getNewTrailerLocation());
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> codeList = (List<String>) oper.getResult();
                if ("E".equalsIgnoreCase(codeList.get(0))) {
                    String msg = codeList.get(1);
                    showMessage("error", msg);
                } else {
                    showMessage("error", codeList.get(0));
                }
                this.setNewLocationError();
                this.setNewLocationValid(false);
                _logger.info("callNewLocValidation() End");
                return;
            } else {
                ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, locationId);
                this.clearNewLocationError();
            }
        }
        this.setNewLocationValid(true);
        _logger.info("callNewLocValidation() End");
        return;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String item = (String) clientEvent.getParameters().get("item");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed) ||
                       F1_KEY_CODE.equals(keyPressed)) {
                this.setF1Hit(false);
                this.setF4Hit(false);
                if ("trailerId".equalsIgnoreCase(item)) {
                    if (submittedValue != null && !submittedValue.isEmpty()) {
                        this.getTrailerIdIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                        this.onChangedTrailerId(new ValueChangeEvent(this.getTrailerId(), null, submittedValue));
                    } else {
                        showMessage("error", getMessageText("INV_TRAILER"));
                        setTrailerIdError();
                        return;
                    }
                } else if ("newLocation".equalsIgnoreCase(item)) {
                    if (F1_KEY_CODE.equals(keyPressed)) {
                        this.getNewLocation().resetValue();
                        ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, null);
                    } else if (submittedValue != null && !submittedValue.isEmpty()) {
                        ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, submittedValue);
                        this.setFocusTrailerId();
                    } else {
                        this.setFocusTrailerId();
                    }
                    this.clearNewLocationError();
                }
                this.refreshContentOfUIComponent(this.getMoveTrailerPanel());
            }
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        if ("F1".equalsIgnoreCase(key)) {
            this.setF1Hit(true);
            this.setF4Hit(false);
            if ("N".equals(ADFUtils.getBoundAttributeValue("TaskManagement"))) {
                if (HhMoveTrailerSBean.LOCATION_FIELD.equals(getHhMoveTrailerSBeanPageFlowBean().getIsFocusOn())) {
                    this.getNewLocation().resetValue();
                    ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, null);
                    this.clearNewLocationError();
                }
                if (ADFUtils.getBoundAttributeValue("TrailerId") != null) {
                    this.getTrailerIdIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                    this.onChangedTrailerId(new ValueChangeEvent(this.getTrailerId(), null,
                                                                 ADFUtils.getBoundAttributeValue("TrailerId")));
                } else {
                    showMessage("error", getMessageText("INV_TRAILER"));
                    this.setTrailerIdError();
                }
            }
        } else if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            callTrExit();
            if ("Y".equals(ADFUtils.getBoundAttributeValue("TaskManagement"))) {
                //                ActivityId aed =  (ActivityId)ADFContext.getCurrent().getSessionScope().get("LaunchActivity");
                //                if (aed != null) returnToActivity(aed);
                //                else ADFUtils.invokeAction("backGlobalHome");
                popTaskFlow(3);
            } else
                ADFUtils.invokeAction("backGlobalHome");

        } else if ("F4".equalsIgnoreCase(key)) {
            this.setF1Hit(false);
            this.setF4Hit(true);
            if (!formSuccess()) {
                return;
            }

            //OTM2485 and OTM2486
            if (!chkNewLocation()) {
                return;
            }

            String existingLocationWhId = null;
            if (ADFUtils.getBoundAttributeValue("LocationId") != null) {
                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callGetWhId");
                Map map = oper.getParamsMap();
                map.put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
                map.put("locationId", ADFUtils.getBoundAttributeValue("LocationId"));
                oper.execute();
                existingLocationWhId = (String) oper.getResult();

                if (null == existingLocationWhId) {
                    showMessage("error", getMessageText("NO_WH_ID"));
                    setErrorOnFocusedField();
                    return;
                }
            }

            String newLocationWhId = null;
            if (ADFUtils.getBoundAttributeValue("NewLocation") != null) {
                OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callGetWhId");
                Map map2 = oper2.getParamsMap();
                map2.put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
                map2.put("locationId", ADFUtils.getBoundAttributeValue("NewLocation"));
                oper2.execute();
                newLocationWhId = (String) oper2.getResult();

                if (null == newLocationWhId) {
                    showMessage("error", getMessageText("NO_WH_ID"));
                    setErrorOnFocusedField();
                    return;
                }
            }

            if (existingLocationWhId != null && !existingLocationWhId.equalsIgnoreCase(newLocationWhId)) {

                OperationBinding oper3 = (OperationBinding) ADFUtils.findOperation("callGetWhDescription");
                Map map3 = oper3.getParamsMap();
                map3.put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
                map3.put("newLocationWhId", newLocationWhId);
                oper3.execute();
                String newLocationDescription = (String) oper3.getResult();

                String msg =
                    this.getMessage("CONT_MOVE_TRL", "W", this.getFacilityIdAttrValue(),
                                    this.getLanguageCodeAttrValue());
                msg = msg + " " + newLocationDescription + "?";
                this.getDialogConfirmChangeWarehouseOutputtext().setValue(msg);
                this.showOrHideConfirmPopup(true);
                return;
            }
            okSaveAction();
            successfulOperationMsg();
            clearFields();
            this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        }
    }

    private void callTrExit() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callTrExit");
        oper.execute();
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callGetWhId");
            oper.execute();
            showOrHideConfirmPopup(false);
            okSaveAction();
            successfulOperationMsg();
            clearFields();
            this.refreshContentOfUIComponent(this.getAllMessagesPanel());
            ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, null);
            ADFUtils.setBoundAttributeValue(TRAILER_ID_ATTR, null);
            this.refreshContentOfUIComponent(this.getMoveTrailerPanel());
        } else {
            ADFUtils.setBoundAttributeValue(NEW_LOCATION_ATTR, null);
            this.refreshContentOfUIComponent(this.getNewLocation());
            showOrHideConfirmPopup(false);
        }
    }

    private void successfulOperationMsg() {
        showMessage("logo", getMessageText("SUCCESS_OPER"));
        this.getHhMoveTrailerSBeanPageFlowBean().setForceDisableTrailerId(false);
        this.setFocusTrailerId();
    }

    private String getMessageText(String code) {
        return super.getMessage(code, "", this.getFacilityIdAttrValue(), this.getLanguageCodeAttrValue());
    }

    private void showMessage(String icon, String text) {
        this.getErrorWarnInfoMessage().setValue(text);
        this.getErrorWarnInfoIcon().setName(icon);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void setFocusTrailerId() {
        if (!getHhMoveTrailerSBeanPageFlowBean().isForceDisableTrailerId()) {
            this.getHhMoveTrailerSBeanPageFlowBean().setIsFocusOn(HhMoveTrailerSBean.TRAILER_FIELD);
            this.setFocusOnUIComponent(this.getTrailerId());
        }
    }

    private void setFocusNewLocation() {
        this.getHhMoveTrailerSBeanPageFlowBean().setIsFocusOn(HhMoveTrailerSBean.LOCATION_FIELD);
        this.setFocusOnUIComponent(this.getNewLocation());
    }

    private HhMoveTrailerSBean getHhMoveTrailerSBeanPageFlowBean() {
        return ((HhMoveTrailerSBean) this.getPageFlowBean(HH_MOVE_TRAILER_FLOW_BEAN));
    }

    private void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void onRegionLoad(ComponentSystemEvent cse) {
        if (getHhMoveTrailerSBeanPageFlowBean() == null)
            return;
        Object gotoFld = getHhMoveTrailerSBeanPageFlowBean().getIsFocusOn();
        if (HhMoveTrailerSBean.TRAILER_FIELD.equals(gotoFld))
            setFocusOnUIComponent(this.getTrailerId());
        else if (HhMoveTrailerSBean.LOCATION_FIELD.equals(gotoFld))
            setFocusOnUIComponent(this.getNewLocation());
    }

    public void setF1Link(RichLink f1Link) {
        this.f1Link = f1Link;
    }

    public RichLink getF1Link() {
        return f1Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setMoveTrailerPanel(RichPanelFormLayout moveTrailerPanel) {
        this.moveTrailerPanel = moveTrailerPanel;
    }

    public RichPanelFormLayout getMoveTrailerPanel() {
        return moveTrailerPanel;
    }

    public void f1ActionListener(ActionEvent actionEvent) {

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_move_trailer_s_MAIN_F1,
                                                RoleBasedAccessConstants.FORM_NAME_hh_move_trailer_s)) {
            this.trKeyIn("F1");
        } else {

            showMessage("error", getMessageText("NOT_ALLOWED"));
        }
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {

        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_move_trailer_s_MAIN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_move_trailer_s)) {
            this.trKeyIn("F4");
        } else {
            showMessage("error", getMessageText("NOT_ALLOWED"));
        }
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLanguageCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public static void setLogger(ADFLogger _logger) {
        Page1Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setNewLocationIcon(RichIcon newLocationIcon) {
        this.newLocationIcon = newLocationIcon;
    }

    public RichIcon getNewLocationIcon() {
        return newLocationIcon;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setNewLocation(RichInputText newLocation) {
        this.newLocation = newLocation;
    }

    public RichInputText getNewLocation() {
        return newLocation;
    }

    public void setF1Hit(Boolean f1Hit) {
        this.f1Hit = f1Hit;
    }

    public Boolean getF1Hit() {
        return f1Hit;
    }

    private void setTrailerIdError() {
        this.getTrailerIdIcon().setName("error");
        this.refreshContentOfUIComponent(this.getTrailerIdIcon());
        this.addErrorStyleToComponent(this.getTrailerId());
        this.getHhMoveTrailerSBeanPageFlowBean().setForceDisableTrailerId(false);
        this.setFocusTrailerId();
        this.refreshContentOfUIComponent(this.getMoveTrailerPanel());
        this.setFocusOnUIComponent(this.getTrailerId());
    }

    private void clearTrailerIdError() {
        this.getTrailerIdIcon().setName("required");
        this.hideMessagesPanel();
        this.removeErrorStyleToComponent(this.getTrailerId());
        this.refreshContentOfUIComponent(this.getTrailerId());
    }

    private void setNewLocationError() {
        this.getNewLocationIcon().setName("error");
        this.refreshContentOfUIComponent(this.getNewLocationIcon());
        this.addErrorStyleToComponent(this.getNewLocation());
        this.setFocusNewLocation();
        this.refreshContentOfUIComponent(this.getMoveTrailerPanel());
        this.setFocusOnUIComponent(this.getNewLocation());
    }

    private void clearNewLocationError() {
        this.getNewLocationIcon().setName("required");
        this.refreshContentOfUIComponent(this.getNewLocationIcon());
        this.hideMessagesPanel();
        this.removeErrorStyleToComponent(this.getNewLocation());
        this.refreshContentOfUIComponent(this.getMoveTrailerPanel());
    }

    public void setTrailerIdValid(Boolean trailerIdValid) {
        this.trailerIdValid = trailerIdValid;
    }

    public Boolean getTrailerIdValid() {
        return trailerIdValid;
    }

    public void setNewLocationValid(Boolean newLocationValid) {
        this.newLocationValid = newLocationValid;
    }

    public Boolean getNewLocationValid() {
        return newLocationValid;
    }

    public void setConfirmChangeWarehousePopup(RichPopup confirmChangeWarehousePopup) {
        this.confirmChangeWarehousePopup = confirmChangeWarehousePopup;
    }

    public RichPopup getConfirmChangeWarehousePopup() {
        return confirmChangeWarehousePopup;
    }

    public void setConfirmChangeWarehouseDialog(RichDialog confirmChangeWarehouseDialog) {
        this.confirmChangeWarehouseDialog = confirmChangeWarehouseDialog;
    }

    public RichDialog getConfirmChangeWarehouseDialog() {
        return confirmChangeWarehouseDialog;
    }

    public void setDialogConfirmChangeWarehouseOutputtext(RichOutputText dialogConfirmChangeWarehouseOutputtext) {
        this.dialogConfirmChangeWarehouseOutputtext = dialogConfirmChangeWarehouseOutputtext;
    }

    public RichOutputText getDialogConfirmChangeWarehouseOutputtext() {
        return dialogConfirmChangeWarehouseOutputtext;
    }

    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        showOrHideConfirmPopup(false);
        this.confirmDialogListener(new DialogEvent(this.getConfirmChangeWarehouseDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        showOrHideConfirmPopup(false);
        this.confirmDialogListener(new DialogEvent(this.getConfirmChangeWarehouseDialog(), DialogEvent.Outcome.no));
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                showOrHideConfirmPopup(false);
                this.confirmDialogListener(new DialogEvent(this.getConfirmChangeWarehouseDialog(),
                                                           DialogEvent.Outcome.yes));
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                this.confirmDialogListener(new DialogEvent(this.getConfirmChangeWarehouseDialog(),
                                                           DialogEvent.Outcome.no));
                showOrHideConfirmPopup(false);
            }
        }
    }

    public String okSaveAction() {
        updateTrailerTable();
        this.getHhMoveTrailerSBeanPageFlowBean().setForceDisableTrailerId(false);
        this.setFocusTrailerId();
        OperationBinding oper2 = (OperationBinding) ADFUtils.findOperation("callDeleteTask");
        oper2.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
        oper2.getParamsMap().put("trailerId", ADFUtils.getBoundAttributeValue("TrailerId"));
        oper2.getParamsMap().put("userId", ADFUtils.getBoundAttributeValue("UserId"));
        oper2.getParamsMap().put("newLocation", ADFUtils.getBoundAttributeValue("NewLocation"));
        oper2.execute();
        //all fields nulled
        return null;
    }

    private void updateTrailerTable() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callUpdateTrailer");
        Map map = oper.getParamsMap();
        map.put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
        map.put("trailerId", ADFUtils.getBoundAttributeValue("TrailerId"));
        map.put("userId", ADFUtils.getBoundAttributeValue("UserId"));
        map.put("newLocation", ADFUtils.getBoundAttributeValue("NewLocation"));
        oper.execute();
    }

    public void clearAllFields() {

    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setTrailerStatus(RichInputText trailerStatus) {
        this.trailerStatus = trailerStatus;
    }

    public RichInputText getTrailerStatus() {
        return trailerStatus;
    }

    public void setLastUseDate(RichInputText lastUseDate) {
        this.lastUseDate = lastUseDate;
    }

    public RichInputText getLastUseDate() {
        return lastUseDate;
    }

    public void setApptDate(RichInputText apptDate) {
        this.apptDate = apptDate;
    }

    public RichInputText getApptDate() {
        return apptDate;
    }

    public void setDoorId(RichInputText doorId) {
        this.doorId = doorId;
    }

    public RichInputText getDoorId() {
        return doorId;
    }

    private void clearFields() {
        _logger.info("clearFields() Start");
        HhMoveTrailerSMainViewRowImpl hhRowImpl = this.getHhMoveTrailerSMainViewCurrentRow();
        hhRowImpl.setTrailerId(null);
        hhRowImpl.setLocationId(null);
        hhRowImpl.setNewLocation(null);
        hhRowImpl.setTrailerStatus(null);
        hhRowImpl.setLastUseDate(null);
        hhRowImpl.setApptDate(null);
        hhRowImpl.setDoorId(null);
        this.getTrailerId().resetValue();
        this.getLocationId().resetValue();
        this.getNewLocation().resetValue();
        this.getTrailerStatus().resetValue();
        this.getLastUseDate().resetValue();
        this.getApptDate().resetValue();
        this.getDoorId().resetValue();
        this.refreshContentOfUIComponent(this.getTrailerId());
        this.refreshContentOfUIComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getNewLocation());
        this.refreshContentOfUIComponent(this.getTrailerStatus());
        this.refreshContentOfUIComponent(this.getLastUseDate());
        this.refreshContentOfUIComponent(this.getApptDate());
        this.refreshContentOfUIComponent(this.getDoorId());
        _logger.info("clearFields() End");
    }

    private HhMoveTrailerSMainViewRowImpl getHhMoveTrailerSMainViewCurrentRow() {
        return (HhMoveTrailerSMainViewRowImpl) ADFUtils.findIterator("HhMoveTrailerSMainViewIterator").getCurrentRow();
    }

    public void setF4Hit(Boolean f4Hit) {
        this.f4Hit = f4Hit;
    }

    public Boolean getF4Hit() {
        return f4Hit;
    }

    private void showOrHideConfirmPopup(boolean show) {
        getHhMoveTrailerSBeanPageFlowBean().setDisableAllFields(show);
        if (show) {
            this.getConfirmChangeWarehousePopup().show(new RichPopup.PopupHints());
            refreshContentOfUIComponent(getMoveTrailerPanel());
        } else {
            this.getConfirmChangeWarehousePopup().hide();
            onRegionLoad(null); //Force focus field
        }
    }

    private boolean chkNewLocation() {
        boolean isValid = Boolean.TRUE.equals(ADFUtils.findOperation("isLocationStatusOk").execute());
        if (!isValid) {
            showMessage("warning", getMessageText("INV_LOCATION"));
            setErrorOnFocusedField();
        }
        return isValid;
    }

    private void setErrorOnFocusedField() {
        if (HhMoveTrailerSBean.TRAILER_FIELD.equals(getHhMoveTrailerSBeanPageFlowBean().getIsFocusOn())) {
            setTrailerIdError();
        } else if (HhMoveTrailerSBean.LOCATION_FIELD.equals(getHhMoveTrailerSBeanPageFlowBean().getIsFocusOn())) {
            setNewLocationError();
        }
    }

    @Override
    protected void setFocusOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("setFocusOrSelectOnUIcomp(textInput);"); //changed selectFocusOnComp to selectFocusonUIComp fo IE using jquery
        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }

    private boolean formSuccess() {
        boolean success = false;
        if (getHhMoveTrailerSBeanPageFlowBean() != null) {
            if (!getHhMoveTrailerSBeanPageFlowBean().getDisableTrailerId()) {
                if (ADFUtils.getBoundAttributeValue("TrailerId") != null) {
                    this.getTrailerIdIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                    this.onChangedTrailerId(new ValueChangeEvent(this.getTrailerId(), null,
                                                                 ADFUtils.getBoundAttributeValue("TrailerId")));
                    return this.getTrailerIdValid();
                } else {
                    showMessage("error", getMessageText("PARTIAL_ENTRY"));
                    this.setTrailerIdError();
                    return false;
                }
            } else if (!getHhMoveTrailerSBeanPageFlowBean().getDisableLocationId()) {
                if (ADFUtils.getBoundAttributeValue("NewLocation") != null) {
                    this.getNewLocationIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getNewLocationIcon());
                    this.onChangedNewLocation(new ValueChangeEvent(this.getNewLocation(), null,
                                                                   ADFUtils.getBoundAttributeValue("NewLocation")));
                    return this.getNewLocationValid();
                } else {
                    showMessage("error", getMessageText("PARTIAL_ENTRY"));
                    this.setNewLocationError();
                    return false;
                }
            }
        }
        return success;
    }
}
