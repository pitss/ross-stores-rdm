package com.ross.rdm.trailermgmt.view.framework;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMBackingBeanBase;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.metadata.ActivityId;
import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.controller.activity.TaskFlowReturnActivityLogic;
import oracle.adfinternal.controller.state.AdfcContext;
import oracle.adfinternal.controller.state.PageFlowStack;
import oracle.adfinternal.controller.state.PageFlowStackEntry;
import oracle.adfinternal.controller.state.ViewPortContextImpl;

import oracle.binding.BindingContainer;

import oracle.jbo.ApplicationModule;
import oracle.jbo.server.DBTransaction;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class RDMTrailerManagementBackingBean extends RDMBackingBeanBase  {
  private static String _appModuleName = "TrailerManagementAppModule";
  private static String _appModuleDataControlName = "TrailerManagementAppModuleDataControl";
      
    public final static Double ENTER_KEY_CODE = Double.valueOf(13);
    public final static Double TAB_KEY_CODE = Double.valueOf(9);
    public final static Double F1_KEY_CODE = Double.valueOf(112);
    public final static Double F2_KEY_CODE = Double.valueOf(113);
    public final static Double F3_KEY_CODE = Double.valueOf(114);
    public final static Double F4_KEY_CODE = Double.valueOf(115);
    public final static Double F5_KEY_CODE = Double.valueOf(116);
    public final static Double F6_KEY_CODE = Double.valueOf(117);
    public final static Double F7_KEY_CODE = Double.valueOf(118);
    public final static Double F8_KEY_CODE = Double.valueOf(119);
    public final static Double F9_KEY_CODE = Double.valueOf(120);
    public final static Double F10_KEY_CODE = Double.valueOf(121);
    public final static Double F11_KEY_CODE = Double.valueOf(122);
    public final static Double F12_KEY_CODE = Double.valueOf(123);

  public RDMTrailerManagementBackingBean() {
  }
  
    public void selectTextOnUIComponent(UIComponent component) {
        String clientId = component.getClientId(FacesContext.getCurrentInstance());

        StringBuilder script = new StringBuilder("var textInput = ");
        script.append("document.getElementById('" + clientId + "::content');");
        script.append("if(textInput != null){textInput.select();}");

        this.writeJavaScriptToClient(script.toString());

        this.refreshContentOfUIComponent(component);
    }
    
  /**
 * Return name of the Application Module 
 * set in static class attribute _appModuleName .
 *
 * @return _appModuleName 
 */
  public String getAppModuleName() {
    return _appModuleName;
  }
  /**
 * Return name of the Application Module Data Control Name
 * set in static class attribute _appModuleDataControlName .
 *
 * @return _appModuleDataControlName 
 */
  public String getAppModuleDataControlName() {
    return _appModuleDataControlName;
  }
  /**
 * Return Application Module 
 *
 * @return application module 
 */
  public ApplicationModule getAppModule() {
    return ADFUtils.getApplicationModuleForDataControl(getAppModuleDataControlName());
  }
  /**
 * Return BindingContainer of page
 *
 * @return current page binding container 
 */
  public BindingContainer getBindings() {
    return BindingContext.getCurrent().getCurrentBindingsEntry();
  }
  /**
 * sets the cursor to the given component id 
 *
 * @param  componentId of item on page
 */
  public void setFocusOnUIComponent(String componentId) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ExtendedRenderKitService service = 
      Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
    
    UIComponent uiComponent = facesContext.getViewRoot().findComponent(componentId);
    service.addScript(facesContext,
      " var t=document.getElementById('" + uiComponent.getClientId(facesContext) + "::content'); " +
      " t.focus();" ); 
  }
    
  /**
 * Return current DBTransaction 
 *
 * @return current DBTransaction 
 */
  public DBTransaction getDBTransaction() {
    return (DBTransaction) getAppModule().getTransaction();
  }
  public String logoutExitBTF() {
    // PITSS.CON NOTE : If you want to navigate to the calling Task Flow/Page Flow, 
    //   then please comment out the following code and replace with  
    //   return "backGlobalHome";  
    return "backGlobalHome";  
    //ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
    //HttpSession session = (HttpSession)ectx.getSession(false);
    //String temp = ControllerContext.getInstance().getGlobalViewActivityURL("exit");
    //try {
    //   ectx.redirect(temp);
    //   session.invalidate();
    //} catch (Exception ex) {
    //  ex.printStackTrace();
    //}
    //return null;
  }
  
    @Deprecated
    public String attributeLabel(String facilityId, String langCode, String attrCode) {
        String label = "#???#";
        /* oracle.binding.OperationBinding oper = ADFUtils.findOperation("getRdmAttributeLabel");
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("langCode", langCode);
        oper.getParamsMap().put("attrCode", attrCode);
        oper.execute();
        if (oper.getErrors().isEmpty())
            label = (String) oper.getResult(); 
        System.out.println("label : "+label);*/
        return label;
    }    
    
    protected Object getPageFlowBean(String pageFlowBeanName) {
        return AdfFacesContext.getCurrentInstance().getPageFlowScope().get(pageFlowBeanName);
    }

    protected Object getSessionScopeBean(String beanName) {
        return ADFContext.getCurrent().getSessionScope().get(beanName);
    }    
    
    public void popTaskFlow(int depth) {

        AdfcContext adfcContext = AdfcContext.getCurrentInstance();
        ViewPortContextImpl currViewPort = adfcContext.getCurrentViewPort();
        try {

            ViewPortContextImpl viewPort = (ViewPortContextImpl) ControllerContext.getInstance().getCurrentViewPort();
            adfcContext.getControllerState().setCurrentViewPort(adfcContext, viewPort.getViewPortId());
            viewPort.makeCurrent(adfcContext);

            PageFlowStack stack = viewPort.getPageFlowStack();
            PageFlowStackEntry entry = null;

            for (int i = 1; i < depth; i++) {
                (new TaskFlowReturnActivityLogic()).abandonTaskFlow(adfcContext, stack.peek());
                entry = stack.pop(adfcContext);
            }
            ActivityId newViewActivityId = entry.getCallingViewActivity();
            viewPort.setViewActivityId(adfcContext, newViewActivityId);

        } finally {
            adfcContext.getControllerState().setCurrentViewPort(adfcContext, currViewPort.getViewPortId());
            currViewPort.makeCurrent(adfcContext);
        }
    }
}
