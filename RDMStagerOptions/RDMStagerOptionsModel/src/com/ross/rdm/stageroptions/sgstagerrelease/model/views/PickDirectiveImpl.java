package com.ross.rdm.stageroptions.sgstagerrelease.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.stageroptions.model.services.StagerOptionsAppModuleImpl;

import com.ross.rdm.stageroptions.sgstagerrelease.model.views.common.PickDirective;

import java.math.BigDecimal;

import oracle.jbo.Row;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jun 28 12:43:46 CEST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PickDirectiveImpl extends RDMViewObjectImpl implements PickDirective {
    /**
     * This is the default constructor (do not remove).
     */
    public PickDirectiveImpl() {
    }

    /**
     * Returns the bind variable value for bind_pd_rowid.
     * @return bind variable value for bind_pd_rowid
     */
    public String getbind_pd_rowid() {
        return (String) getNamedWhereClauseParam("bind_pd_rowid");
    }

    /**
     * Sets <code>value</code> for bind variable bind_pd_rowid.
     * @param value value to bind as bind_pd_rowid
     */
    public void setbind_pd_rowid(String value) {
        setNamedWhereClauseParam("bind_pd_rowid", value);
    }

    public void populateIntoWorkLocal(String rowid) {
        setbind_pd_rowid(rowid);
        executeQuery();
        Row firstRow = first();
        if (firstRow != null) {
            SgStagerReleaseWorkLocalViewRowImpl workLocal =
                (SgStagerReleaseWorkLocalViewRowImpl) ((StagerOptionsAppModuleImpl) getApplicationModule()).getSgStagerReleaseWorkLocalView().getCurrentRow();
            workLocal.setDistroNbr((String) firstRow.getAttribute("DistroNbr"));
            workLocal.setDestId((BigDecimal) firstRow.getAttribute("DestId"));
            workLocal.setPickToContainerId((String) firstRow.getAttribute("PickToContainerId"));
            workLocal.setCasePackSize((BigDecimal) firstRow.getAttribute("CasePackSize"));
        }
    }
}

