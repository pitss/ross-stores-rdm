package com.ross.rdm.stageroptions.sgstagerrelease.model.result;

import java.io.Serializable;

public class DBCallResult implements Serializable {
    @SuppressWarnings("compatibility:-4860841261112299346")
    private static final long serialVersionUID = 1L;
    private String goField;
    private String errorType;
    private String errorText;
    private String functionResult;

    public DBCallResult() {
        super();
    }

    public void setGoField(String goField) {
        this.goField = goField;
    }

    public String getGoField() {
        return goField;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setFunctionResult(String functionResult) {
        this.functionResult = functionResult;
    }

    public String getFunctionResult() {
        return functionResult;
    }
}
