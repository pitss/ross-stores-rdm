package com.ross.rdm.stageroptions.model.services;

import com.ross.rdm.common.hhmenuss.model.views.GetLoginDateViewImpl;
import com.ross.rdm.common.model.services.RdmCommonAppModuleImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.model.views.UserMessageViewImpl;
import com.ross.rdm.common.utils.model.base.RDMApplicationModuleImpl;
import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.stageroptions.model.services.common.StagerOptionsAppModule;
import com.ross.rdm.stageroptions.model.view.lookups.UpsLovViewImpl;
import com.ross.rdm.stageroptions.sgstagermove.model.views.SgStagerMoveBStagerMoveViewImpl;
import com.ross.rdm.stageroptions.sgstagermove.model.views.SgStagerMoveMainViewImpl;
import com.ross.rdm.stageroptions.sgstagermove.model.views.SgStagerMoveWorkLocalViewRowImpl;
import com.ross.rdm.stageroptions.sgstagermove.model.views.SgStagerMoveWorkViewImpl;
import com.ross.rdm.stageroptions.sgstagermove.model.views.SgStagerMoveWorkViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.ControlOptionsViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.LocationViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.PickDirectiveImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SelTaskQueueImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage2ViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage3ViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage5ViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage5ViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage6ViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseControlViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkLocalViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkViewImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.UpsCodeImpl;

import java.math.BigDecimal;

import java.sql.Types;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue May 03 12:07:12 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class StagerOptionsAppModuleImpl extends RDMApplicationModuleImpl implements StagerOptionsAppModule {
    private static ADFLogger _logger = ADFLogger.createADFLogger(StagerOptionsAppModuleImpl.class);
    private static final String SG_STAGER_RELEASE_MLP_S = "SG_STAGER_RELEASE_MLP_S";


    /**
     * This is the default constructor (do not remove).
     */
    public StagerOptionsAppModuleImpl() {
    }

    /**
     * Container's getter for SgStagerMoveMainView.
     * @return SgStagerMoveMainView
     */
    public SgStagerMoveMainViewImpl getSgStagerMoveMainView() {
        return (SgStagerMoveMainViewImpl) findViewObject("SgStagerMoveMainView");
    }

    /**
     * Container's getter for SgStagerMoveMainBoilerView.
     * @return SgStagerMoveMainBoilerView
     */
    public RDMTransientViewObjectImpl getSgStagerMoveMainBoilerView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerMoveMainBoilerView");
    }

    /**
     * Container's getter for SgStagerMoveWorkLocalView.
     * @return SgStagerMoveWorkLocalView
     */
    public RDMTransientViewObjectImpl getSgStagerMoveWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerMoveWorkLocalView");
    }

    /**
     * Container's getter for SgStagerMoveWorkLaborProdView.
     * @return SgStagerMoveWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getSgStagerMoveWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerMoveWorkLaborProdView");
    }

    /**
     * Container's getter for SgStagerMoveWorkView.
     * @return SgStagerMoveWorkView
     */
    public SgStagerMoveWorkViewImpl getSgStagerMoveWorkView() {
        return (SgStagerMoveWorkViewImpl) findViewObject("SgStagerMoveWorkView");
    }

    /**
     * Container's getter for SgStagerMoveBStagerMoveView.
     * @return SgStagerMoveBStagerMoveView
     */
    public SgStagerMoveBStagerMoveViewImpl getSgStagerMoveBStagerMoveView() {
        return (SgStagerMoveBStagerMoveViewImpl) findViewObject("SgStagerMoveBStagerMoveView");
    }

    /**
     * Container's getter for SgStagerMoveBStagerMoveBoilerView.
     * @return SgStagerMoveBStagerMoveBoilerView
     */
    public RDMTransientViewObjectImpl getSgStagerMoveBStagerMoveBoilerView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerMoveBStagerMoveBoilerView");
    }

    /**
     * Container's getter for SgStagerReleaseControlView.
     * @return SgStagerReleaseControlView
     */
    public RDMTransientViewObjectImpl getSgStagerReleaseControlView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerReleaseControlView");
    }


    /**
     * Container's getter for SgStagerReleaseBStagerReleaseView.
     * @return SgStagerReleaseBStagerReleaseView
     */
    public RDMTransientViewObjectImpl getSgStagerReleaseBStagerReleaseView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerReleaseBStagerReleaseView");
    }


    /**
     * Container's getter for SgStagerReleaseBPage2View.
     * @return SgStagerReleaseBPage2View
     */
    public SgStagerReleaseBPage2ViewImpl getSgStagerReleaseBPage2View() {
        return (SgStagerReleaseBPage2ViewImpl) findViewObject("SgStagerReleaseBPage2View");
    }


    /**
     * Container's getter for SgStagerReleaseBPage3View.
     * @return SgStagerReleaseBPage3View
     */
    public SgStagerReleaseBPage3ViewImpl getSgStagerReleaseBPage3View() {
        return (SgStagerReleaseBPage3ViewImpl) findViewObject("SgStagerReleaseBPage3View");
    }


    /**
     * Container's getter for SgStagerReleaseBPage5View.
     * @return SgStagerReleaseBPage5View
     */
    public SgStagerReleaseBPage5ViewImpl getSgStagerReleaseBPage5View() {
        return (SgStagerReleaseBPage5ViewImpl) findViewObject("SgStagerReleaseBPage5View");
    }


    /**
     * Container's getter for SgStagerReleaseBPage6View.
     * @return SgStagerReleaseBPage6View
     */
    public SgStagerReleaseBPage6ViewImpl getSgStagerReleaseBPage6View() {
        return (SgStagerReleaseBPage6ViewImpl) findViewObject("SgStagerReleaseBPage6View");
    }


    /**
     * Container's getter for SgStagerReleaseWorkLocalView.
     * @return SgStagerReleaseWorkLocalView
     */
    public RDMTransientViewObjectImpl getSgStagerReleaseWorkLocalView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerReleaseWorkLocalView");
    }

    /**
     * Container's getter for SgStagerReleaseWorkLaborProdView.
     * @return SgStagerReleaseWorkLaborProdView
     */
    public RDMTransientViewObjectImpl getSgStagerReleaseWorkLaborProdView() {
        return (RDMTransientViewObjectImpl) findViewObject("SgStagerReleaseWorkLaborProdView");
    }

    /**
     * Container's getter for SgStagerReleaseWorkView.
     * @return SgStagerReleaseWorkView
     */
    public SgStagerReleaseWorkViewImpl getSgStagerReleaseWorkView() {
        return (SgStagerReleaseWorkViewImpl) findViewObject("SgStagerReleaseWorkView");
    }

    /**
     * Helper Method to Simplify Invoking Stored Procedures.
     * @param stmt
     * @param bindVars
     */
    public void callStoredProcedure(String stmt, Object... bindVars) {
        DBUtils.callStoredProcedure(getDBTransaction(), stmt, bindVars);
    }

    /**
     * Helper Method to Simplify Invoking Stored Functions.
     * @param sqlReturnType
     * @param stmt
     * @param bindVars
     * @return
     */
    public Object callStoredFunction(int sqlReturnType, String stmt, Object... bindVars) {
        return DBUtils.callStoredFunction(getDBTransaction(), sqlReturnType, stmt, bindVars);
    }

    public void initTaskFlowStagerRelease(String formName, String mlpHeader, String idHeader, String cfHeader, String upsHeader) {
        prepareViewObject(getSgStagerReleaseBPage2View());
        prepareViewObject(getSgStagerReleaseBPage3View());
        prepareViewObject(getSgStagerReleaseBPage5View());
        prepareViewObject(getSgStagerReleaseBPage6View());
        prepareViewObject(getSgStagerReleaseBStagerReleaseView());
        prepareViewObject(getSgStagerReleaseControlView());
        prepareViewObject(getSgStagerReleaseWorkLaborProdView());
        prepareViewObject(getSgStagerReleaseWorkLocalView());
        prepareViewObject(getSgStagerReleaseWorkView());

        String facilityId = trStartupStagerRelease(formName, mlpHeader, idHeader, cfHeader, upsHeader);
        setAhlInfo(facilityId, formName);
    }

    public void setAhlInfo(String facilityId, String currentForm) {
        callStoredProcedure("AHL.SET_AHL_INFO", facilityId, currentForm);
    }

    public void initTaskFlowStagerMove(String formName, String moveHeader, String moveAllHeader) {
        prepareViewObject(getSgStagerMoveMainView());
        prepareViewObject(getSgStagerMoveWorkLaborProdView());
        prepareViewObject(getSgStagerMoveWorkView());
        prepareViewObject(getSgStagerMoveBStagerMoveView());
        prepareViewObject(getSgStagerMoveWorkLocalView());

        String facilityId = trStartupStagerMove(formName,  moveHeader,  moveAllHeader);
        setAhlInfo(facilityId, formName);
    }

    private String trStartupStagerRelease(String formName,String mlpHeader, String idHeader, String cfHeader, String upsHeader) {
        GlobalVariablesViewRowImpl global = this.setGlobalVariablesStagerRelease();
        String facilityId = global.getGlobalFacilityId();
        String langCode = global.getGlobalLanguageCode();
        SgStagerReleaseWorkViewRowImpl workRow =
            (SgStagerReleaseWorkViewRowImpl) this.getSgStagerReleaseWorkView().getCurrentRow();
        SgStagerReleaseWorkLocalViewRowImpl workLocal =
            (SgStagerReleaseWorkLocalViewRowImpl) this.getSgStagerReleaseWorkLocalView().getCurrentRow();
        SgStagerReleaseControlViewRowImpl control =
            (SgStagerReleaseControlViewRowImpl) this.getSgStagerReleaseControlView().getCurrentRow();
        SgStagerReleaseBPage5ViewRowImpl page5 =
            (SgStagerReleaseBPage5ViewRowImpl) this.getSgStagerReleaseBPage5View().getCurrentRow();

        workRow.setUser(global.getGlobalUserId());
        workRow.setFacilityId(facilityId);
        workRow.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workRow.setDevice(global.getGlobalDevice());
        workRow.setCallingBlock("B_STAGER_RELEASE");
        workRow.setLanguageCode(global.getGlobalLanguageCode());
        
        if (SG_STAGER_RELEASE_MLP_S.equals(formName)) {
            //callDisplayHeader returns STAGER RELEASE ML, (missing P at the end)
            //so it has been changed to gTranslatedField
            workRow.setHeader(mlpHeader);
        } else {
            workRow.setHeader(this.callDisplayHeader(facilityId, langCode, formName));
        }
        workRow.setMainBlock("B_STAGER_RELEASE");
        //Setup Page 2
        workLocal.setPage2Header( idHeader);
        //Setup Page 3
        workLocal.setPage3Header( cfHeader);
        //setup Control Block, NOT USED
        /* control.setAll(gTranslatedField(facilityId, langCode, "HOTEL_FLOW"));
        control.setFlow(gTranslatedField(facilityId, langCode, "FLOW"));
        control.setHotel(gTranslatedField(facilityId, langCode, "HOTEL")); */
        getControlOptionsView().init(facilityId, langCode);
        //Setup Page 5
        workLocal.setPage5Header( mlpHeader);
        //Setup Page 6
        workLocal.setPage6Header( upsHeader);
        //Setup Page 7
        workLocal.setPage7Header( upsHeader);

        /* If :parameter.form_name = 'SG_STAGER_RELEASE_MLP_S' Then
   		SET_ITEM_PROPERTY('B_PAGE_5_BOILER.SG_MLP', DISPLAYED, PROPERTY_TRUE);
   		SET_ITEM_PROPERTY('B_PAGE_5_BOILER.SG_MLP_SPLIT', DISPLAYED, PROPERTY_FALSE);
            End If;*/
        if (SG_STAGER_RELEASE_MLP_S.equalsIgnoreCase(formName)) {
            page5.setShowLabelSplit(false);
        }

        workLocal.setPutawayStageLoc(gScp(facilityId, "putaway_stage_loc"));
        workLocal.setActivityCode(gScp(facilityId, "release_activity"));
        workLocal.setMoveAll(gScp(facilityId, "stage_move_all"));
        workLocal.setReleaseWip(gScp(facilityId, "release_wip"));
        workLocal.setReleaseBulkLoc(gScp(facilityId, "release_bulk"));
        workLocal.setReleaseBulk("N");
        workLocal.setPtsDropoff(gScp(facilityId, "pts_dropoff"));
        workLocal.setPickToContainer("N");
        workLocal.setPreviousTask(null);
        workLocal.setUndistributed("N");
        workLocal.setMultiWave("N");

        if (SG_STAGER_RELEASE_MLP_S.equalsIgnoreCase(formName)) {
            workLocal.setScanMlpStart("Y");
        }

        return facilityId;
    }

    private String trStartupStagerMove(String formName, String moveHeader, String moveAllHeader) {
        GlobalVariablesViewRowImpl global = this.setGlobalVariablesStagerRelease();

        SgStagerMoveWorkLocalViewRowImpl workLocal =
            (SgStagerMoveWorkLocalViewRowImpl) this.getSgStagerMoveWorkLocalView().getCurrentRow();

        SgStagerMoveWorkViewRowImpl workRow =
            (SgStagerMoveWorkViewRowImpl) this.getSgStagerMoveWorkView().getCurrentRow();

        String facilityId = global.getGlobalFacilityId();
        String langCode = global.getGlobalLanguageCode();


        workLocal.setMoveMlpComplete(gScp(facilityId, "move_mlp_complete"));

        workRow.setUser(global.getGlobalUserId());
        workRow.setFacilityId(facilityId);
        workRow.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workRow.setDevice(global.getGlobalDevice());
        workRow.setCallingBlock("MAIN");
        workRow.setLanguageCode(global.getGlobalLanguageCode());

        if (formName.equals("SG_STAGER_MOVE_ALL_S")) {
            workRow.setMainBlock("MAIN");
            workRow.setHeader(moveAllHeader);
        } else if (formName.equals("SG_STAGER_MOVE_S")) {
            workRow.setMainBlock("B_STAGER_MOVE");
            workLocal.setAction("E");
            workRow.setHeader(moveHeader);
        } else {
            workRow.setHeader(this.callDisplayHeader(facilityId, langCode, formName));
        }
        _logger.info("setGlobalVariablesTaskQueue() End");
        return facilityId;
    }

    public String callDisplayHeader(String facilityId, String langCode, String formName) {
        String header =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "PKG_COMMON_ADF.DISPLAY_HEADER",
                                                facilityId, langCode, formName);
        if (header != null) {
            header = header.replace(facilityId, "").trim();
        }
        return header;
    }

    private void prepareViewObject(RDMViewObjectImpl vo) {
        if (vo != null) {
            if (vo.getCurrentRow() == null) {
                vo.insertRow(vo.createRow());
            } else {
                vo.getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
            }
        }
    }

    private GlobalVariablesViewRowImpl setGlobalVariablesStagerRelease() {
        getRdmCommonAppModule().initCurrentUserIfNeed();

        GlobalVariablesViewImpl globalVO = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) globalVO.getCurrentRow();
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) globalVO.createRow();
        }

        Row loginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) loginUserRow.getAttribute("FacilityId");
        if (facilityId == null) {
            facilityId = "PR";
        }
        String languageCode = (String) loginUserRow.getAttribute("LanguageCode");
        if (languageCode == null) {
            languageCode = "AM";
        }

        variablesRow.setGlobalUserId((String) loginUserRow.getAttribute("UserId"));
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalLanguageCode(languageCode);
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALUSERPRIVILEGE, "1");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALDEVICE, "S");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALMSGCODE, "");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALRESPONSE, "");
        globalVO.insertRow(variablesRow);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        return variablesRow;
    }

    private void defaultValue(RDMViewRowImpl row, int attribute, Object defaultValue) {
        if (row != null && row.getAttribute(attribute) == null) {
            row.setAttribute(attribute, defaultValue);
        }
    }


    public String gTranslatedField(String facilityId, String langCode, String dataBaseValue) {
        return (String) callStoredFunction(Types.VARCHAR, "g_translated_field", facilityId, langCode, dataBaseValue);
    }

    private String gScp(String facilityId, String var) {
        return (String) DBUtils.callStoredFunction(getDBTransaction(), Types.VARCHAR, "g_scp", facilityId, var);
    }

    /**
     * Container's getter for ControlOptionsView1.
     * @return ControlOptionsView1
     */
    public ControlOptionsViewImpl getControlOptionsView() {
        return (ControlOptionsViewImpl) findViewObject("ControlOptionsView");
    }

    public String getUserMessagesByCode(String msgCode, String facilityId, String langCode) {
        UserMessageViewImpl userMessageView = this.getUserMessageView();
        userMessageView.setbindCodeType(msgCode);
        userMessageView.setbindFacilityId(facilityId);
        userMessageView.setbindLangCode(langCode);
        userMessageView.executeQuery();
        Row rowViewKey = this.getUserMessageView().first();
        if (rowViewKey == null) {
            return "???" + msgCode + "???";
        } else {
            return (String) rowViewKey.getAttribute("MessageText");
        }
    }

    /**
     * Container's getter for GlobalVariablesView1.
     * @return GlobalVariablesView1
     */
    public GlobalVariablesViewImpl getGlobalVariablesView() {
        return  this.getRdmCommonAppModule().getGlobalVariablesView();
    }

    /**
     * Container's getter for UserMessageView1.
     * @return UserMessageView1
     */
    public UserMessageViewImpl getUserMessageView() {
        return  this.getRdmCommonAppModule().getUserMessageView();
    }

    /**
     * Container's getter for GetLoginDateView1.
     * @return GetLoginDateView1
     */
    public GetLoginDateViewImpl getGetLoginDateView() {
        return  this.getRdmCommonAppModule().getGetLoginDateView();
    }


    /**
     * Container's getter for LocationView1.
     * @return LocationView1
     */
    public LocationViewImpl getLocationView() {
        return (LocationViewImpl) findViewObject("LocationView");
    }

    /**
     * Container's getter for UpsLovView1.
     * @return UpsLovView1
     */
    public UpsLovViewImpl getUpsLovView() {
        return (UpsLovViewImpl) findViewObject("UpsLovView");
    }

    /**
     * Container's getter for SelTaskQueue1.
     * @return SelTaskQueue1
     */
    public SelTaskQueueImpl getSelTaskQueue() {
        return (SelTaskQueueImpl) findViewObject("SelTaskQueue");
    }

    /**
     * Container's getter for PickDirective1.
     * @return PickDirective1
     */
    public PickDirectiveImpl getPickDirective() {
        return (PickDirectiveImpl) findViewObject("PickDirective");
    }

    /**
     * Container's getter for UpsCode1.
     * @return UpsCode1
     */
    public UpsCodeImpl getUpsCode() {
        return (UpsCodeImpl) findViewObject("UpsCode");
    }

    /**
     * Container's getter for RdmCommonAppModule1.
     * @return RdmCommonAppModule1
     */
    public RdmCommonAppModuleImpl getRdmCommonAppModule() {
        return (RdmCommonAppModuleImpl) findApplicationModule("RdmCommonAppModule");
    }
    
    public void enableReservedMode(){
        if (getReleaseLevel() != ApplicationModule.RELEASE_LEVEL_RESERVED)
           setReleaseLevel(ApplicationModule.RELEASE_LEVEL_RESERVED);
    }
    
    public void disableReservedMode(){
        if (getReleaseLevel() != ApplicationModule.RELEASE_LEVEL_MANAGED)
           setReleaseLevel(ApplicationModule.RELEASE_LEVEL_MANAGED);
    }
    
    
}
