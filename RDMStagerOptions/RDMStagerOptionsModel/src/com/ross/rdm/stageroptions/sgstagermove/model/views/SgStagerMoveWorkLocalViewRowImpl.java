package com.ross.rdm.stageroptions.sgstagermove.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import java.sql.Date;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jul 27 13:47:33 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SgStagerMoveWorkLocalViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        MoveMlpComplete {
            public Object get(SgStagerMoveWorkLocalViewRowImpl obj) {
                return obj.getMoveMlpComplete();
            }

            public void put(SgStagerMoveWorkLocalViewRowImpl obj, Object value) {
                obj.setMoveMlpComplete((String) value);
            }
        }
        ,
        MasterContainerId {
            public Object get(SgStagerMoveWorkLocalViewRowImpl obj) {
                return obj.getMasterContainerId();
            }

            public void put(SgStagerMoveWorkLocalViewRowImpl obj, Object value) {
                obj.setMasterContainerId((String) value);
            }
        }
        ,
        Action {
            public Object get(SgStagerMoveWorkLocalViewRowImpl obj) {
                return obj.getAction();
            }

            public void put(SgStagerMoveWorkLocalViewRowImpl obj, Object value) {
                obj.setAction((String) value);
            }
        }
        ,
        StageScan {
            public Object get(SgStagerMoveWorkLocalViewRowImpl obj) {
                return obj.getStageScan();
            }

            public void put(SgStagerMoveWorkLocalViewRowImpl obj, Object value) {
                obj.setStageScan((String) value);
            }
        }
        ,
        DivertTs {
            public Object get(SgStagerMoveWorkLocalViewRowImpl obj) {
                return obj.getDivertTs();
            }

            public void put(SgStagerMoveWorkLocalViewRowImpl obj, Object value) {
                obj.setDivertTs((Date) value);
            }
        }
        ,
        MovedUnits {
            public Object get(SgStagerMoveWorkLocalViewRowImpl obj) {
                return obj.getMovedUnits();
            }

            public void put(SgStagerMoveWorkLocalViewRowImpl obj, Object value) {
                obj.setMovedUnits((BigDecimal) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(SgStagerMoveWorkLocalViewRowImpl object);

        public abstract void put(SgStagerMoveWorkLocalViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int MOVEMLPCOMPLETE = AttributesEnum.MoveMlpComplete.index();
    public static final int MASTERCONTAINERID = AttributesEnum.MasterContainerId.index();
    public static final int ACTION = AttributesEnum.Action.index();
    public static final int STAGESCAN = AttributesEnum.StageScan.index();
    public static final int DIVERTTS = AttributesEnum.DivertTs.index();
    public static final int MOVEDUNITS = AttributesEnum.MovedUnits.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SgStagerMoveWorkLocalViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute MoveMlpComplete.
     * @return the MoveMlpComplete
     */
    public String getMoveMlpComplete() {
        return (String) getAttributeInternal(MOVEMLPCOMPLETE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MoveMlpComplete.
     * @param value value to set the  MoveMlpComplete
     */
    public void setMoveMlpComplete(String value) {
        setAttributeInternal(MOVEMLPCOMPLETE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute MasterContainerId.
     * @return the MasterContainerId
     */
    public String getMasterContainerId() {
        return (String) getAttributeInternal(MASTERCONTAINERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MasterContainerId.
     * @param value value to set the  MasterContainerId
     */
    public void setMasterContainerId(String value) {
        setAttributeInternal(MASTERCONTAINERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Action.
     * @return the Action
     */
    public String getAction() {
        return (String) getAttributeInternal(ACTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Action.
     * @param value value to set the  Action
     */
    public void setAction(String value) {
        setAttributeInternal(ACTION, value);
    }

    /**
     * Gets the attribute value for the calculated attribute StageScan.
     * @return the StageScan
     */
    public String getStageScan() {
        return (String) getAttributeInternal(STAGESCAN);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute StageScan.
     * @param value value to set the  StageScan
     */
    public void setStageScan(String value) {
        setAttributeInternal(STAGESCAN, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DivertTs.
     * @return the DivertTs
     */
    public Date getDivertTs() {
        return (Date) getAttributeInternal(DIVERTTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DivertTs.
     * @param value value to set the  DivertTs
     */
    public void setDivertTs(Date value) {
        setAttributeInternal(DIVERTTS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute MovedUnits.
     * @return the MovedUnits
     */
    public BigDecimal getMovedUnits() {
        return (BigDecimal) getAttributeInternal(MOVEDUNITS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MovedUnits.
     * @param value value to set the  MovedUnits
     */
    public void setMovedUnits(BigDecimal value) {
        setAttributeInternal(MOVEDUNITS, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

