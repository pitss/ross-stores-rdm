package com.ross.rdm.stageroptions.sgstagermove.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.stageroptions.model.services.StagerOptionsAppModuleImpl;

import java.math.BigDecimal;

import java.sql.Date;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

public class StagerMoveBaseBackingBean extends RDMTransientViewObjectImpl {
    public StagerMoveBaseBackingBean() {
        super();
    }

    public List callSaveLaborProd(String facilityId, String userId, String formName, String confirmLocationId,
                                  String tiConfirmLocationId) {

        List<String> errors = null;

        StagerOptionsAppModuleImpl applicationModule = (StagerOptionsAppModuleImpl) this.getApplicationModule();

        SgStagerMoveWorkLaborProdViewRowImpl workLaborViewRow =
            (SgStagerMoveWorkLaborProdViewRowImpl) applicationModule.findViewObject("SgStagerMoveWorkLaborProdView").getCurrentRow();

        BigDecimal containersProcessed = workLaborViewRow.getContainersProcessed();
        BigDecimal operationsPerformed = workLaborViewRow.getOperationsPerformed();
        Date startTime = workLaborViewRow.getStartTime();

        SQLOutParam v_return = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam i_activity_code = new SQLOutParam(workLaborViewRow.getActivityCode(), Types.VARCHAR);

        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_STAGER_OPTIONS_ADF.SAVE_LABOR_PROD",
                                             v_return, facilityId, userId, i_activity_code, containersProcessed,
                                             operationsPerformed, startTime, formName, confirmLocationId,
                                             tiConfirmLocationId);

        if (null != v_return.getWrappedData()) {
            errors = new ArrayList<String>();
            String errorMsg = (String) v_return.getWrappedData();
            int Raise = errorMsg.indexOf("RAISE");
            if (Raise != -1) {
                errorMsg = errorMsg.substring(0, errorMsg.indexOf("RAISE"));
            }
            errors.add(errorMsg);
        }

        return errors;
    }
}
