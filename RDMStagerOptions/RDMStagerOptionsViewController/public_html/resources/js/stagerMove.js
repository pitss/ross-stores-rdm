function setStagerMoveAllInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('con');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function setStagerMoveInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementById('con');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedStagerMoveAll(event){
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue()

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);
        event.cancel();
    }
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function onKeyPressedStagerMove(event){
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue()

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);
        event.cancel();
    }
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}

function customFindElementByIdContainer(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}