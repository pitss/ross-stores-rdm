var popupShown = false;

function onOpenConfPopupStagerRelease(event) {
    popupShown = true;
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup1');
    linkComp.focus();
}

function onCloseConfPopupStagerRelease(event) {
    popupShown = false;
}

function blurTablePage4StagerRelease(event) {
    if(!popupShown){
        OnBlurTableHandler(event);
    }
}

function onKeyUpConfPopupStagerRelease(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('yesLinkPopup1'), true);
    }else if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfActionEvent.queue(component.findComponent('noLinkPopup1'), true);
    }
    event.cancel();
}

function setInitialFocusStagerReleasePage7() {
    setTimeout(function () {
        var focusOn = customFindElementById('tbb');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyUpStagerReleasePage7(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
            AdfActionEvent.queue(component.findComponent('f4'), true);
        }
        event.cancel();
    }
}

function onKeyUpStagerReleasePages135(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyUpStagerReleasePage4(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfActionEvent.queue(component.findComponent('move'), true);
    }else if(keyPressed == AdfKeyStroke.F3_KEY){
        AdfActionEvent.queue(component.findComponent('f3'), true);
    }else if(keyPressed == AdfKeyStroke.F4_KEY){
        AdfActionEvent.queue(component.findComponent('f4'), true);
    }
    event.cancel();
}

function onOpenErrorPopupStagerRelease(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('okLink');
    linkComp.focus();
}

function onKeyUpErrorPopupStagerRelease(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component, true);
    }
    event.cancel();
}

function onKeyUpStagerReleasePage2(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onKeyUpStagerReleasePage6(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}