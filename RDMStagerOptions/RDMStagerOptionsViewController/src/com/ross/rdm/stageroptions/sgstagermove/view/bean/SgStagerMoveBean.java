package com.ross.rdm.stageroptions.sgstagermove.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingLabelBean;

import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class SgStagerMoveBean {
    private String formName; // Item/Parameter sg_stager_move.FORM_NAME
    private Boolean isTiContainerIdValidated;
    private Boolean isContainerIdValidated;
    private Boolean isTiLpnValidated;
    private Boolean isConfirmLocationIdValidated;
    private Boolean isTiConfirmLocationIdValidated;
    private String isFocusOn;
    private Boolean isError;

    private static ADFLogger _logger = ADFLogger.createADFLogger(SgStagerMoveBean.class);
    private static final String STAGER_MOVE =  "STAGER_MOVE";
    private static final String STAGER_MOVE_ALL =  "STAGER_MOVE_ALL";
    private static final String initTaskFlowStagerMove = "initTaskFlowStagerMove";
    
    public void initTaskFlow() {
        this.initGlobalVariablesTaskQueue();
        this.setIsFocusOn("ContainerIdField");
        this.setIsContainerIdValidated(false);
        this.setIsError(false);
        this.setIsTiContainerIdValidated(false);
        this.setIsTiLpnValidated(false);
        this.setIsTiConfirmLocationIdValidated(false);
    }

    public void initGlobalVariablesTaskQueue() {  
        RDMHotelPickingLabelBean labelBean = new RDMHotelPickingLabelBean();
        OperationBinding opr = ADFUtils.findOperation(initTaskFlowStagerMove);
        opr.getParamsMap().put("formName", getFormName());
        opr.getParamsMap().put("moveHeader",labelBean.attributeLabel(STAGER_MOVE));
        opr.getParamsMap().put("moveAllHeader",labelBean.attributeLabel(STAGER_MOVE_ALL)); 
        opr.execute();
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return formName;
    }

    public void setIsTiContainerIdValidated(Boolean isTiContainerIdValidated) {
        this.isTiContainerIdValidated = isTiContainerIdValidated;
    }

    public Boolean getIsTiContainerIdValidated() {
        return isTiContainerIdValidated;
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsTiLpnValidated(Boolean isTiLpnValidated) {
        this.isTiLpnValidated = isTiLpnValidated;
    }

    public Boolean getIsTiLpnValidated() {
        return isTiLpnValidated;
    }

    public void setIsConfirmLocationIdValidated(Boolean isConfirmLocationIdValidated) {
        this.isConfirmLocationIdValidated = isConfirmLocationIdValidated;
    }

    public Boolean getIsConfirmLocationIdValidated() {
        return isConfirmLocationIdValidated;
    }

    public void setIsTiConfirmLocationIdValidated(Boolean isTiConfirmLocationIdValidated) {
        this.isTiConfirmLocationIdValidated = isTiConfirmLocationIdValidated;
    }

    public Boolean getIsTiConfirmLocationIdValidated() {
        return isTiConfirmLocationIdValidated;
    }


    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }

    public static void setLogger(ADFLogger _logger) {
        SgStagerMoveBean._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }
}
