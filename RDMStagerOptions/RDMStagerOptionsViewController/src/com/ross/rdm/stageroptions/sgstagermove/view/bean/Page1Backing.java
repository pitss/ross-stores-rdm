package com.ross.rdm.stageroptions.sgstagermove.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.stageroptions.view.framework.RDMStagerOptionsBackingBean;

import java.math.BigDecimal;

import java.sql.Date;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMStagerOptionsBackingBean {

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String TI_CONTAINER_ID_ATTR = "TiContainerId";
    private final static String TI_LPN_ATTR = "TiLpn";
    private final static String TI_CONFIRM_LOCATION_ID_ATTR = "TiConfirmLocationId";
    private final static String CONTAINERS_PROCESSED = "ContainersProcessed";
    private final static String DIVERT_TS = "DivertTs";
    private final static String MOVE_MLP_COMPLETE = "MoveMlpComplete";
    private final static String MLP_COMPLETE = "MlpComplete";
    private final static String LPN_QTY = "LpnLeft";
    private final static String LOCATION_ID_ATTR = "LocationId";
    private final static String TI_LPN = "TiLpn";
    private final static String USER_ID = "UserId";

    private final static String FORM_NAME = "SG_STAGER_MOVE_S";
    private final static String SG_STAGER_MOVE_BEAN = "SgStagerMoveBean";

    private RichIcon tiContainerIdIcon;
    private RichInputText tiContainerId;
    private RichIcon tiLpnIcon;
    private RichInputText tiLpn;
    private RichIcon tiConfirmLocationIdIcon;
    private RichInputText tiConfirmLocationId;

    public Boolean isFocusLpn = false;
    public Boolean isLpnMoved = false;

    private RichLink f3Link;
    private RichLink f4Link;
    private RichPanelFormLayout stagerMovePanel;
    Boolean f3 = false;


    public Page1Backing() {

    }

    public void onChangedTiContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateTiContainerId(enteredContainerIdValue);
        } else {
            this.getTiContainerIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getTiContainerIdIcon());
            this.refreshContentOfUIComponent(this.getTiContainerId());
            this.addErrorStyleToComponent(this.getTiContainerId());
            this.selectTextOnUIComponent(this.getTiContainerId());
            this.showMessagesPanel(ERROR,
                                   this.getMessage("SG_INV_MLP", ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLanguageCodeValue()));
        }
    }

    public void onChangedTiLpn(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredLpnValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateTiLpn(enteredLpnValue);
        } else {
            this.getTiLpnIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getTiLpnIcon());
            this.refreshContentOfUIComponent(this.getTiLpn());
            this.addErrorStyleToComponent(this.getTiLpn());
            this.selectTextOnUIComponent(this.getTiLpn());
            this.showMessagesPanel(ERROR,
                                   this.getMessage("SG_INV_LPN", ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLanguageCodeValue()));
        }
    }

    public void onChangedTiConfirmLocationId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredLocationIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateTiConfirmLocationId(enteredLocationIdValue);
        } else {
            this.getTiConfirmLocationIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getTiConfirmLocationIdIcon());
            this.refreshContentOfUIComponent(this.getTiConfirmLocationId());
            this.addErrorStyleToComponent(this.getTiConfirmLocationId());
            this.selectTextOnUIComponent(this.getTiConfirmLocationId());
            this.showMessagesPanel(ERROR,
                                   this.getMessage("SG_LOC_REQ", ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLanguageCodeValue()));
        }
    }

    public Boolean validateTiContainerId(String tiContainerId) {
        _logger.info("validateContainerId() Start");
        if (!this.getTiContainerId().isDisabled()) {
            if (this.getSgStagerMovePageFlowBean().getIsError() && tiContainerId != null) {
                this.setErrorStyleClass(this.getTiContainerId(), true);
                this.selectTextOnUIComponent(this.getTiContainerId());
                this.getSgStagerMovePageFlowBean().setIsTiContainerIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getSgStagerMovePageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getTiContainerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("validateTiContainerId");
                Map map = oper.getParamsMap();
                map.put("tiConfirmLocationId", getTiConfirmLocationIdValue());
                map.put("facilityId", getFacilityIdAttrValue());
                map.put("moveMlpComplete", getMoveMlpComplete());
                map.put("tiContainerId", tiContainerId);
                map.put("containersProcessed", getContainersProcessed());
                map.put("formName", FORM_NAME);
                map.put("userId", getUser());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if ("GoTiLpn".equals(errorCodeList.get(0))) {
                            String currentContainerId = this.getTiContainerIdValue();
                            this.getTiContainerId().resetValue();
                            ADFUtils.setBoundAttributeValue(TI_CONTAINER_ID_ATTR, currentContainerId);
                            this.getTiContainerIdIcon().setName(REQ_ICON);
                            this.refreshContentOfUIComponent(this.getTiContainerIdIcon());
                            this.refreshContentOfUIComponent(this.getTiContainerId());
                            this.hideMessagesPanel();
                            this.removeErrorStyleToComponent(this.getTiContainerId());
                            this.refreshContentOfUIComponent(stagerMovePanel);
                            this.getSgStagerMovePageFlowBean().setIsTiContainerIdValidated(true);
                            setFocusTiLpn();
                            setIsFocusLpn(true);
                            return true;
                        }
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel(ERROR, msg);
                        } else {
                            this.showMessagesPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getTiContainerIdIcon().setName(ERR_ICON);
                        ADFUtils.setBoundAttributeValue(TI_CONTAINER_ID_ATTR, null);
                        this.refreshContentOfUIComponent(this.getTiContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getTiContainerId());
                        this.addErrorStyleToComponent(this.getTiContainerId());
                        this.selectTextOnUIComponent(this.getTiContainerId());
                        this.getTiContainerId().resetValue();
                        this.getSgStagerMovePageFlowBean().setIsTiContainerIdValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        String currentContainerId = this.getTiContainerIdValue();
                        this.getTiContainerId().resetValue();
                        ADFUtils.setBoundAttributeValue(TI_CONTAINER_ID_ATTR, currentContainerId);
                        this.getTiContainerIdIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getTiContainerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getTiContainerId());
                        this.refreshContentOfUIComponent(this.getStagerMovePanel());

                        _logger.info("onChangedContainerId() End");

                    }
                }
            }
        }
        this.getSgStagerMovePageFlowBean().setIsTiContainerIdValidated(true);
        this.getSgStagerMovePageFlowBean().setIsTiConfirmLocationIdValidated(false);
        setFocusTiConfirmLocationId();
        _logger.info("validatedContainerId() End");
        return true;
    }

    public Boolean validateTiLpn(String tiLpn) {
        _logger.info("validateContainerId() Start");
        if (!this.getTiLpn().isDisabled()) {
            if (this.getSgStagerMovePageFlowBean().getIsError() && tiLpn != null) {
                this.setErrorStyleClass(this.getTiLpn(), true);
                this.selectTextOnUIComponent(this.getTiLpn());
                this.getSgStagerMovePageFlowBean().setIsTiLpnValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getSgStagerMovePageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getTiLpn(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("validateTiLpnContainer");
                Map map = oper.getParamsMap();
                map.put("facilityId", getFacilityIdAttrValue());
                map.put("tiContainerId", getTiContainerIdValue());
                map.put("tiLpn", tiLpn);
                map.put("containersProcessed", getContainersProcessed());
                map.put("formName", FORM_NAME);
                map.put("userId", getUser());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if ("StayLpn".equals(errorCodeList.get(0))) {
                            ADFUtils.setBoundAttributeValue(TI_LPN_ATTR, null);
                            this.getTiLpn().resetValue();
                            this.getTiLpnIcon().setName(REQ_ICON);
                            this.refreshContentOfUIComponent(this.getTiLpnIcon());
                            this.hideMessagesPanel();
                            this.removeErrorStyleToComponent(this.getTiLpn());
                            this.refreshContentOfUIComponent(this.getStagerMovePanel());
                            this.setIsLpnMoved(true);
                            return false;
                        } else if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel(ERROR, msg);
                        } else {
                            this.showMessagesPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getTiLpnIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getTiLpnIcon());
                        this.refreshContentOfUIComponent(this.getTiLpn());
                        this.addErrorStyleToComponent(this.getTiLpn());
                        this.selectTextOnUIComponent(this.getTiLpn());
                        ADFUtils.setBoundAttributeValue(TI_LPN_ATTR, null);
                        this.getTiLpn().resetValue();
                        this.getSgStagerMovePageFlowBean().setIsTiLpnValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(TI_LPN_ATTR, null);
                        this.getTiLpnIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getTiLpnIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getTiLpn());
                        this.refreshContentOfUIComponent(this.getStagerMovePanel());
                        this.setIsLpnMoved(true);

                        _logger.info("onChangedContainerId() End");

                    }
                }
            }
        }
        this.getSgStagerMovePageFlowBean().setIsTiLpnValidated(true);
        _logger.info("validatedContainerId() End");
        return true;
    }

    public Boolean validateTiConfirmLocationId(String tiConfirmLocationId) {
        _logger.info("validateContainerId() Start");
        if (!this.getTiConfirmLocationId().isDisabled()) {
            if (this.getSgStagerMovePageFlowBean().getIsError() && tiConfirmLocationId != null) {
                this.setErrorStyleClass(this.getTiConfirmLocationId(), true);
                this.selectTextOnUIComponent(this.getTiConfirmLocationId());
                this.getSgStagerMovePageFlowBean().setIsTiConfirmLocationIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getSgStagerMovePageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getTiConfirmLocationId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("tiConfirmLocationId");
                Map map = oper.getParamsMap();
                map.put("tiConfirmLocationId", tiConfirmLocationId);
                map.put("tiContainerId", getTiContainerIdValue());
                map.put("facilityId", getFacilityIdAttrValue());
                map.put("divertTs", getDivertTsValue());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel(ERROR, msg);
                        } else {
                            this.showMessagesPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getTiConfirmLocationIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getTiConfirmLocationIdIcon());
                        this.refreshContentOfUIComponent(this.getTiConfirmLocationId());
                        this.addErrorStyleToComponent(this.getTiConfirmLocationId());
                        this.selectTextOnUIComponent(this.getTiConfirmLocationId());
                        if (!f3) {
                            ADFUtils.setBoundAttributeValue(TI_CONFIRM_LOCATION_ID_ATTR, null);
                            this.getTiContainerId().resetValue();
                        }
                        this.getSgStagerMovePageFlowBean().setIsTiConfirmLocationIdValidated(false);
                        f3 = false;

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(TI_CONFIRM_LOCATION_ID_ATTR, tiConfirmLocationId);
                        this.getTiConfirmLocationIdIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getTiConfirmLocationIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getTiConfirmLocationId());
                        this.refreshContentOfUIComponent(this.getStagerMovePanel());


                        _logger.info("onChangedContainerId() End");

                    }
                }
            }
        }
        this.getSgStagerMovePageFlowBean().setIsTiConfirmLocationIdValidated(true);
        _logger.info("validatedConfirmLocationIdId() End");
        return true;
    }

    public void performKey(ClientEvent clientEvent) {

        String submittedValue = null;
        submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getSgStagerMovePageFlowBean().getIsFocusOn();
        if (clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {

                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();

            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("ContainerIdField".equals(currentFocus)) {
                    this.setErrorStyleClass(this.getTiContainerId(), false);
                    if (!this.getSgStagerMovePageFlowBean().getIsTiContainerIdValidated())
                        this.onChangedTiContainerId(new ValueChangeEvent(this.getTiContainerId(), null,
                                                                         submittedValue));
                    this.refreshContentOfUIComponent(getTiContainerId());
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                }

                else if ("tiLpn".equals(currentFocus)) {
                    this.setErrorStyleClass(this.getTiLpn(), false);
                    if (!this.getSgStagerMovePageFlowBean().getIsTiLpnValidated())
                        this.onChangedTiLpn(new ValueChangeEvent(this.getTiLpn(), null, submittedValue));
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                    if (this.getSgStagerMovePageFlowBean().getIsTiLpnValidated()) {
                        if (BigDecimal.ZERO.equals(getTiLpnQty())) {
                            this.getTiLpnIcon().setName(REQ_ICON);
                            this.refreshContentOfUIComponent(this.getTiLpnIcon());
                            this.hideMessagesPanel();
                            this.removeErrorStyleToComponent(this.getTiLpn());
                            this.refreshContentOfUIComponent(this.getStagerMovePanel());
                            this.setFocusTiConfirmLocationId();
                        }
                    }
                } else if ("tiLocId".equals(currentFocus)) {
                    this.setErrorStyleClass(this.getTiConfirmLocationId(), false);
                    if (!this.getSgStagerMovePageFlowBean().getIsTiConfirmLocationIdValidated())
                        this.onChangedTiConfirmLocationId(new ValueChangeEvent(this.getTiConfirmLocationId(), null,
                                                                               submittedValue));
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                }
            }
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");

        String currentFocus = this.getSgStagerMovePageFlowBean().getIsFocusOn();

        if ("F4".equalsIgnoreCase(key)) {
            if ("ContainerIdField".equals(currentFocus)) {
                this.setErrorStyleClass(this.getTiContainerId(), false);
                if (!this.getSgStagerMovePageFlowBean().getIsTiContainerIdValidated())
                    this.onChangedTiContainerId(new ValueChangeEvent(this.getTiContainerId(), null,
                                                                     getTiContainerIdValue()));
                if (this.getSgStagerMovePageFlowBean().getIsTiLpnValidated()) {
                    this.setFocusTiConfirmLocationId();
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                }
            } else if (("tiLpn").equals(currentFocus)) {
                this.setErrorStyleClass(this.getTiLpn(), false);

                if (null == ADFUtils.getBoundAttributeValue("tiLpn")) {
                    this.getTiLpnIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getTiLpnIcon());
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getTiLpn());
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                    this.setFocusTiConfirmLocationId();
                    return;
                }
                if (BigDecimal.ZERO.equals(getTiLpnQty())) {
                    this.getTiLpnIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getTiLpnIcon());
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getTiLpn());
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                    this.setFocusTiConfirmLocationId();
                    return;
                }
                if (!this.getSgStagerMovePageFlowBean().getIsTiLpnValidated()) {
                    this.onChangedTiLpn(new ValueChangeEvent(this.getTiLpn(), null, getTiLpnValue()));
                }
                if (this.getSgStagerMovePageFlowBean().getIsTiLpnValidated()) {
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                    this.getTiLpnIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getTiLpnIcon());
                    this.hideMessagesPanel();
                    this.removeErrorStyleToComponent(this.getTiLpn());
                    this.refreshContentOfUIComponent(this.getStagerMovePanel());
                    this.setFocusTiConfirmLocationId();
                }
            } else if ("tiLocId".equals(currentFocus)) {
                this.setErrorStyleClass(this.getTiConfirmLocationId(), false);
                if (!this.getSgStagerMovePageFlowBean().getIsTiConfirmLocationIdValidated())
                    this.onChangedTiConfirmLocationId(new ValueChangeEvent(this.getTiConfirmLocationId(), null,
                                                                           getTiConfirmLocationIdValue()));
                this.refreshContentOfUIComponent(this.getStagerMovePanel());

                if (getSgStagerMovePageFlowBean().getIsTiContainerIdValidated() &&
                    getSgStagerMovePageFlowBean().getIsTiConfirmLocationIdValidated()) {
                    continueTrKeyInF4();
                    resetForm();

                }
            }
        }
    }

    public void resetForm() {
        ADFUtils.setBoundAttributeValue(TI_CONTAINER_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(TI_CONFIRM_LOCATION_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(MLP_COMPLETE, null);
        ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(LPN_QTY, null);
        ADFUtils.setBoundAttributeValue(TI_LPN, null);
        this.getTiConfirmLocationIdIcon().setName(REQ_ICON);
        this.getTiContainerId().resetValue();
        this.getTiConfirmLocationId().resetValue();
        this.getSgStagerMovePageFlowBean().setIsTiContainerIdValidated(false);
        this.getSgStagerMovePageFlowBean().setIsTiConfirmLocationIdValidated(false);
        this.getSgStagerMovePageFlowBean().setIsTiLpnValidated(false);
        this.refreshContentOfUIComponent(this.getTiConfirmLocationId());
        this.refreshContentOfUIComponent(this.getTiContainerId());
        this.refreshContentOfUIComponent(stagerMovePanel);
        this.setFocusTiContainerId();
    }

    public Boolean continueTrKeyInF3() {

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("trKeyInF3Continue");
        Map map = oper.getParamsMap();
        map.put("facilityId", getFacilityIdAttrValue());
        map.put("tiContainerId", getTiContainerIdValue());
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> errorCodeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                    String msg = errorCodeList.get(1);
                    this.showMessagesPanel(ERROR, msg);
                } else {
                    this.showMessagesPanel(ERROR, errorCodeList.get(1));
                }
                this.getTiLpnIcon().setName(REQ_ICON);
                this.removeErrorStyleToComponent(this.getTiLpn());
                this.getTiConfirmLocationIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getTiConfirmLocationIdIcon());
                this.refreshContentOfUIComponent(this.getTiConfirmLocationId());
                this.addErrorStyleToComponent(this.getTiConfirmLocationId());
                this.selectTextOnUIComponent(this.getTiConfirmLocationId());
                this.getSgStagerMovePageFlowBean().setIsTiConfirmLocationIdValidated(false);
                this.setFocusTiConfirmLocationId();

                this.refreshContentOfUIComponent(this.getTiLpn());
                this.refreshContentOfUIComponent(stagerMovePanel);
                return false;
            }

        }

        return true;
    }

    private void continueTrKeyInF4() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("trKeyInF4Continue");
        Map map = oper.getParamsMap();
        map.put("facilityId", getFacilityIdAttrValue());
        map.put("tiContainerId", getTiContainerIdValue());
        map.put("confirmLocationId", getTiConfirmLocationIdValue());
        map.put("userId", getUser());
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> errorCodeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                    String msg = errorCodeList.get(1);
                    this.showMessagesPanel(ERROR, msg);
                } else {
                    this.showMessagesPanel(ERROR, errorCodeList.get(1));
                }
                this.getTiConfirmLocationIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getTiConfirmLocationIdIcon());
                this.refreshContentOfUIComponent(this.getTiConfirmLocationId());
                this.addErrorStyleToComponent(this.getTiConfirmLocationId());
                this.selectTextOnUIComponent(this.getTiConfirmLocationId());
                this.getTiConfirmLocationId().resetValue();
            }
        }
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con")) {
                this.getTiContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getTiContainerIdIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con")) {
                this.getTiContainerIdIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getTiContainerIdIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    private void setFocusTiContainerId() {
        this.getSgStagerMovePageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getTiContainerId());
    }

    private void setFocusTiLpn() {
        this.getSgStagerMovePageFlowBean().setIsFocusOn("tiLpn");
        this.setFocusOnUIComponent(this.getTiLpn());
    }

    private void setFocusTiConfirmLocationId() {
        this.getSgStagerMovePageFlowBean().setIsFocusOn("tiLocId");
        this.setFocusOnUIComponent(this.getTiConfirmLocationId());
    }

    public String f3ActionListener() {
        if (continueTrKeyInF3()) {
            return "backGlobalHome";
        } else {

            return null;
        }
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_stager_move_s_B_STAGER_MOVE_F4,
                                                RoleBasedAccessConstants.FORM_NAME_sg_stager_move_s)) {
            this.trKeyIn("F4");
        } else {
            this.showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    private SgStagerMoveBean getSgStagerMovePageFlowBean() {
        return ((SgStagerMoveBean) this.getPageFlowBean(SG_STAGER_MOVE_BEAN));
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLanguageCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private String getTiContainerIdValue() {
        return (String) ADFUtils.getBoundAttributeValue(TI_CONTAINER_ID_ATTR);
    }

    private String getTiLpnValue() {
        return (String) ADFUtils.getBoundAttributeValue(TI_LPN_ATTR);
    }

    private String getTiConfirmLocationIdValue() {
        return (String) ADFUtils.getBoundAttributeValue(TI_CONFIRM_LOCATION_ID_ATTR);
    }

    private BigDecimal getContainersProcessed() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue(CONTAINERS_PROCESSED);
    }

    private Date getDivertTsValue() {
        return (Date) ADFUtils.getBoundAttributeValue(DIVERT_TS);
    }

    private String getMoveMlpComplete() {
        return (String) ADFUtils.getBoundAttributeValue(MOVE_MLP_COMPLETE);
    }

    private BigDecimal getTiLpnQty() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue(LPN_QTY);
    }

    private String getUser() {
        return (String) ADFUtils.getBoundAttributeValue(USER_ID);
    }


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public static void setLogger(ADFLogger _logger) {
        Page1Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setTiContainerIdIcon(RichIcon tiContainerIdIcon) {
        this.tiContainerIdIcon = tiContainerIdIcon;
    }

    public RichIcon getTiContainerIdIcon() {
        return tiContainerIdIcon;
    }

    public void setTiContainerId(RichInputText tiContainerId) {
        this.tiContainerId = tiContainerId;
    }

    public RichInputText getTiContainerId() {
        return tiContainerId;
    }

    public void setTiLpnIcon(RichIcon tiLpnIcon) {
        this.tiLpnIcon = tiLpnIcon;
    }

    public RichIcon getTiLpnIcon() {
        return tiLpnIcon;
    }

    public void setTiLpn(RichInputText tiLpn) {
        this.tiLpn = tiLpn;
    }

    public RichInputText getTiLpn() {
        return tiLpn;
    }

    public void setTiConfirmLocationIdIcon(RichIcon tiConfirmLocationIdIcon) {
        this.tiConfirmLocationIdIcon = tiConfirmLocationIdIcon;
    }

    public RichIcon getTiConfirmLocationIdIcon() {
        return tiConfirmLocationIdIcon;
    }

    public void setTiConfirmLocationId(RichInputText tiConfirmLocationId) {
        this.tiConfirmLocationId = tiConfirmLocationId;
    }

    public RichInputText getTiConfirmLocationId() {
        return tiConfirmLocationId;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setStagerMovePanel(RichPanelFormLayout stagerMovePanel) {
        this.stagerMovePanel = stagerMovePanel;
    }

    public RichPanelFormLayout getStagerMovePanel() {
        return stagerMovePanel;
    }


    public void setIsFocusLpn(Boolean isFocusLpn) {
        this.isFocusLpn = isFocusLpn;
    }

    public Boolean getIsFocusLpn() {
        return isFocusLpn;
    }

    public void setIsLpnMoved(Boolean isLpnMoved) {
        this.isLpnMoved = isLpnMoved;
    }

    public Boolean getIsLpnMoved() {
        return isLpnMoved;
    }

}
