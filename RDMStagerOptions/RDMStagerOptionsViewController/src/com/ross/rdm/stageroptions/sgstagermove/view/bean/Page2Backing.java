package com.ross.rdm.stageroptions.sgstagermove.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.stageroptions.view.framework.RDMStagerOptionsBackingBean;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMStagerOptionsBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);

    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String CONFIRM_LOCATION_ID_ATTR = "ConfirmLocationId";
    private final static String MOVE_MLP_COMPLETE_ATTR = "MoveMlpComplete";
    private final static String MLP_COMPLETE = "MlpComplete";
    private final static String LPN_QTY = "LpnQty";
    private final static String LOCATION_ID_ATTR = "LocationId";
    private final static String USER = "UserId";

    private final static String FORM_NAME = "SG_STAGER_MOVE_ALL_S";
    private final static String SG_STAGER_MOVE_BEAN = "SgStagerMoveBean";

    private RichIcon containerIdIcon;
    private RichInputText containerId;
    private RichIcon confirmLocationIdIcon;
    private RichInputText confirmLocationId;

    private RichLink f3Link;
    private RichLink f4Link;
    private RichPanelFormLayout stagerMoveAllPanel;

    public Page2Backing() {

    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateContainerId(enteredContainerIdValue);
        } else {
            this.getContainerIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getContainerIdIcon());
            this.refreshContentOfUIComponent(this.getContainerId());
            this.addErrorStyleToComponent(this.getContainerId());
            this.selectTextOnUIComponent(this.getContainerId());
            this.getSgStagerMovePageFlowBean().setIsContainerIdValidated(false);
            this.getContainerId().resetValue();
            this.showMessagesPanel(ERROR,
                                   this.getMessage("SG_INV_MLP", ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLanguageCodeValue()));
        }
    }

    public void onChangedConfirmLocationId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredLocationIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateConfirmLocationId(enteredLocationIdValue);
        } else {
            this.getConfirmLocationIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getConfirmLocationIdIcon());
            this.refreshContentOfUIComponent(this.getConfirmLocationId());
            this.addErrorStyleToComponent(this.getConfirmLocationId());
            this.selectTextOnUIComponent(this.getConfirmLocationId());
            this.getSgStagerMovePageFlowBean().setIsConfirmLocationIdValidated(false);
            this.getConfirmLocationId().resetValue();
            this.showMessagesPanel(ERROR,
                                   this.getMessage("SG_LOC_REQ", ERROR, this.getFacilityIdAttrValue(),
                                                   this.getLanguageCodeValue()));
        }
    }

    public boolean validateContainerId(String containerId) {
        if (!this.getContainerId().isDisabled()) {
            if (this.getSgStagerMovePageFlowBean().getIsError() && containerId != null) {
                this.setErrorStyleClass(this.getContainerId(), true);
                this.selectTextOnUIComponent(this.getContainerId());
                this.getSgStagerMovePageFlowBean().setIsContainerIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getSgStagerMovePageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getContainerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("validateContainerId");
                Map map = oper.getParamsMap();
                map.put("facilityId", getFacilityIdAttrValue());
                map.put("containerId", containerId);
                map.put("mlpComplete", getMoveMlpCompleteValue());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel(ERROR, msg);
                        } else {
                            this.showMessagesPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getContainerIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.selectTextOnUIComponent(this.getContainerId());
                        this.getContainerId().resetValue();
                        this.getSgStagerMovePageFlowBean().setIsContainerIdValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, containerId);
                        this.getContainerIdIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getContainerId());
                        this.refreshContentOfUIComponent(this.getStagerMoveAllPanel());


                        _logger.info("onChangedContainerId() End");

                    }
                }
            }
        }
        this.getSgStagerMovePageFlowBean().setIsContainerIdValidated(true);
        this.getSgStagerMovePageFlowBean().setIsConfirmLocationIdValidated(false);
        return true;
    }

    public boolean validateConfirmLocationId(String confirmLocationId) {
        if (!this.getConfirmLocationId().isDisabled()) {
            if (this.getSgStagerMovePageFlowBean().getIsError() && containerId != null) {
                this.setErrorStyleClass(this.getConfirmLocationId(), true);
                this.selectTextOnUIComponent(this.getContainerId());
                this.getSgStagerMovePageFlowBean().setIsConfirmLocationIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getSgStagerMovePageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getConfirmLocationId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("confirmLocationId");
                Map map = oper.getParamsMap();
                map.put("confirmLocationId", confirmLocationId);
                map.put("containerId", getContainerIdValue());
                map.put("locationId", getLocationIdValue());
                map.put("facilityId", getFacilityIdAttrValue());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showMessagesPanel(ERROR, msg);
                        } else {
                            this.showMessagesPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getConfirmLocationIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getConfirmLocationIdIcon());
                        this.refreshContentOfUIComponent(this.getConfirmLocationId());
                        this.addErrorStyleToComponent(this.getConfirmLocationId());
                        this.selectTextOnUIComponent(this.getConfirmLocationId());
                        ADFUtils.setBoundAttributeValue(CONFIRM_LOCATION_ID_ATTR, null);
                        this.getConfirmLocationId().resetValue();
                        this.refreshContentOfUIComponent(stagerMoveAllPanel);
                        this.getSgStagerMovePageFlowBean().setIsConfirmLocationIdValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(CONFIRM_LOCATION_ID_ATTR, confirmLocationId);
                        this.getConfirmLocationIdIcon().setName("required");
                        this.refreshContentOfUIComponent(this.getConfirmLocationIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getConfirmLocationId());
                        this.refreshContentOfUIComponent(this.getStagerMoveAllPanel());

                        _logger.info("onChangedContainerId() End");

                    }
                }
            }
        }
        this.getSgStagerMovePageFlowBean().setIsConfirmLocationIdValidated(true);
        return true;
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = null;
        submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        SgStagerMoveBean pfBean = this.getSgStagerMovePageFlowBean();
        if (pfBean != null) {
            String currentFocus = pfBean.getIsFocusOn();
            if (clientEvent.getParameters().containsKey("keyPressed")) {
                Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
                if (F3_KEY_CODE.equals(keyPressed)) {
                    ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                    actionEvent.queue();
                } else if (F4_KEY_CODE.equals(keyPressed)) {

                    ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                    actionEvent.queue();

                } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {

                    if ("ContainerIdField".equals(currentFocus)) {
                        this.setErrorStyleClass(this.getContainerId(), false);
                    if (!this.getSgStagerMovePageFlowBean().getIsContainerIdValidated())
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    this.refreshContentOfUIComponent(this.getStagerMoveAllPanel());
                        if (this.getSgStagerMovePageFlowBean().getIsContainerIdValidated()) {
                            this.setFocusLocationId();
                        }
                    } else if ("ConfirmLocationId".equals(currentFocus)) {
                        this.setErrorStyleClass(this.getConfirmLocationId(), false);
                        if (!this.getSgStagerMovePageFlowBean().getIsConfirmLocationIdValidated())
                            this.onChangedConfirmLocationId(new ValueChangeEvent(this.getConfirmLocationId(), null,
                                                                                 submittedValue));
                        this.refreshContentOfUIComponent(this.getStagerMoveAllPanel());
                    }
                }
            }
        }
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con")) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con")) {
                this.getContainerIdIcon().setName("required");
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");

        String currentFocus = this.getSgStagerMovePageFlowBean().getIsFocusOn();

        if ("F3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        }
        if ("F4".equalsIgnoreCase(key)) {
            if ("ContainerIdField".equals(currentFocus)) {
                this.setErrorStyleClass(this.getContainerId(), false);
                if (!this.getSgStagerMovePageFlowBean().getIsContainerIdValidated())
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, getContainerIdValue()));
                this.refreshContentOfUIComponent(this.getStagerMoveAllPanel());
                if (this.getSgStagerMovePageFlowBean().getIsContainerIdValidated()) {
                    this.setFocusLocationId();
                }
            } else if ("ConfirmLocationId".equals(currentFocus)) {
                this.setErrorStyleClass(this.getConfirmLocationId(), false);
                if (!this.getSgStagerMovePageFlowBean().getIsConfirmLocationIdValidated())
                    this.onChangedConfirmLocationId(new ValueChangeEvent(this.getConfirmLocationId(), null,
                                                                         getConfirmLocationIdValue()));
                this.refreshContentOfUIComponent(this.getStagerMoveAllPanel());

                if (getSgStagerMovePageFlowBean().getIsContainerIdValidated() &&
                    getSgStagerMovePageFlowBean().getIsConfirmLocationIdValidated()) {
                    continueTrKeyIn();
                    resetForm();
                }
            }
        }
    }

    private void continueTrKeyIn() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("trKeyInF4Continue");
        Map map = oper.getParamsMap();
        map.put("facilityId", getFacilityIdAttrValue());
        map.put("containerId", getContainerIdValue());
        map.put("confirmLocationId", getConfirmLocationIdValue());
        map.put("userId", getUser());
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> errorCodeList = (List<String>) oper.getResult();
                if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                    String msg = errorCodeList.get(1);
                    this.showMessagesPanel(ERROR, msg);
                } else {
                    this.showMessagesPanel(ERROR, errorCodeList.get(1));
                }
                this.getConfirmLocationIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getConfirmLocationIdIcon());
                this.refreshContentOfUIComponent(this.getConfirmLocationId());
                this.addErrorStyleToComponent(this.getConfirmLocationId());
                this.selectTextOnUIComponent(this.getConfirmLocationId());
                this.getConfirmLocationId().resetValue();
            }
        }
    }

    public void resetForm() {
        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(CONFIRM_LOCATION_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(MLP_COMPLETE, null);
        ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, null);
        ADFUtils.setBoundAttributeValue(LPN_QTY, null);
        this.getConfirmLocationIdIcon().setName(REQ_ICON);
        this.getContainerId().resetValue();
        this.getConfirmLocationId().resetValue();
        this.getSgStagerMovePageFlowBean().setIsContainerIdValidated(false);
        this.getSgStagerMovePageFlowBean().setIsConfirmLocationIdValidated(false);
        this.refreshContentOfUIComponent(this.getConfirmLocationId());
        this.refreshContentOfUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(stagerMoveAllPanel);
        this.setFocusContainerId();
    }

    private void setFocusContainerId() {
        this.getSgStagerMovePageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusLocationId() {
        this.getSgStagerMovePageFlowBean().setIsFocusOn("ConfirmLocationId");
        this.setFocusOnUIComponent(this.getConfirmLocationId());
    }

    private SgStagerMoveBean getSgStagerMovePageFlowBean() {
        return ((SgStagerMoveBean) this.getPageFlowBean(SG_STAGER_MOVE_BEAN));
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLanguageCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private String getContainerIdValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
    }

    private String getLocationIdValue() {
        return (String) ADFUtils.getBoundAttributeValue(LOCATION_ID_ATTR);
    }

    private String getConfirmLocationIdValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONFIRM_LOCATION_ID_ATTR);
    }

    private String getMoveMlpCompleteValue() {
        return (String) ADFUtils.getBoundAttributeValue(MOVE_MLP_COMPLETE_ATTR);
    }

    private String getUser() {
        return (String) ADFUtils.getBoundAttributeValue(USER);
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public static void setLogger(ADFLogger _logger) {
        Page2Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setConfirmLocationIdIcon(RichIcon confirmLocationIdIcon) {
        this.confirmLocationIdIcon = confirmLocationIdIcon;
    }

    public RichIcon getConfirmLocationIdIcon() {
        return confirmLocationIdIcon;
    }

    public void setConfirmLocationId(RichInputText confirmLocationId) {
        this.confirmLocationId = confirmLocationId;
    }

    public RichInputText getConfirmLocationId() {
        return confirmLocationId;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public String f3ActionListener() {
        return "backGlobalHome";
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_stager_move_all_s_MAIN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_sg_stager_move_all_s)) {
            this.trKeyIn("F4");
        } else {
            this.showMessagesPanel("E", getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    public void setStagerMoveAllPanel(RichPanelFormLayout stagerMoveAllPanel) {
        this.stagerMoveAllPanel = stagerMoveAllPanel;
    }

    public RichPanelFormLayout getStagerMoveAllPanel() {
        return stagerMoveAllPanel;
    }
}
