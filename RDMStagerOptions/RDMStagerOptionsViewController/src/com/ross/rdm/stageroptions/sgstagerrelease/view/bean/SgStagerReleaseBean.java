package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingLabelBean;

import java.math.BigDecimal;

import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class SgStagerReleaseBean {
    //FORMS
    public static final String SG_STAGER_RELEASE_MLP_S = "SG_STAGER_RELEASE_MLP_S";
    public static final String SG_STAGER_RELEASE_S = "SG_STAGER_RELEASE_S";
    //FIELDS
    public static final String TI_CONFIRM_LOCATION_ID = "TI_CONFIRM_LOCATION_ID";
    public static final String TI_LPN_CONTAINER_ID = "TI_LPN_CONTAINER_ID";
    //ACTIONS TO EXECUTE AFTER PROCESS_NEXT_PICK
    public static final int LOOP_WAVE_NBR = 1;
    public static final int P_VAL_CONFIRM_LOC_1 = 2;
    public static final int P_VAL_CONFIRM_LOC_2 = 3;

    private String formName; // Item/Parameter sg_stager_release.FORM_NAME(SG_STAGER_RELEASE_MLP_S or SG_STAGER_RELEASE_S)
    private Boolean executedKey = Boolean.FALSE;
    private static ADFLogger _logger = ADFLogger.createADFLogger(SgStagerReleaseBean.class);
    private String showMessageType = null;
    private String showMessageText = null;
    private String nextAction;
    private String nextNavigation;
    private String modeIn; //PROCESS_NEXT_PICK
    private int afterProcessNextPick; //PROCESS_NEXT_PICK
    private int loopCont;
    private BigDecimal lWaveNbr;
    private Boolean exitLoop;
    private Boolean disableAllFields = Boolean.FALSE;
    private String executeSomeKey = null;
    private String page3Focus = null;
    private static final String STAGER_RELEASE_MLP = "STAGER_RELEASE_MLP";
    private static final String STAGER_DISPLAY_UPS = "STAGER_DISPLAY_UPS";
    private static final String STAGER_RELEASE_CF =  "STAGER_RELEASE_CF";
    private static final String STAGER_RELEASE_ID =  "STAGER_RELEASE_ID";
     
    private static final String initTaskFlowStagerRelease = "initTaskFlowStagerRelease";
    
    public void initTaskFlow() {
        RDMHotelPickingLabelBean labelBean = new RDMHotelPickingLabelBean();
        OperationBinding opr = ADFUtils.findOperation(initTaskFlowStagerRelease);
        opr.getParamsMap().put("formName", getFormName());
        opr.getParamsMap().put("mlpHeader",labelBean.attributeLabel(STAGER_RELEASE_MLP));
        opr.getParamsMap().put("idHeader",labelBean.attributeLabel(STAGER_RELEASE_ID));
        opr.getParamsMap().put("cfHeader",labelBean.attributeLabel(STAGER_RELEASE_CF));
        opr.getParamsMap().put("upsHeader",labelBean.attributeLabel(STAGER_DISPLAY_UPS));
        
        opr.execute();
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return formName;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }

    public void setShowMessageType(String showMessageType) {
        this.showMessageType = showMessageType;
    }

    public String getShowMessageType() {
        return showMessageType;
    }

    public void setShowMessageText(String showMessageText) {
        this.showMessageText = showMessageText;
    }

    public String getShowMessageText() {
        return showMessageText;
    }

    public void showMessage(String type, String text) {
        setShowMessageType(type);
        setShowMessageText(text);
    }

    public boolean hasMessageToShow() {
        return getShowMessageType() != null && getShowMessageText() != null;
    }

    public void setNextAction(String nextAction) {
        this.nextAction = nextAction;
    }

    public String getNextAction() {
        return nextAction;
    }

    public void setNextNavigation(String nextNavigation) {
        this.nextNavigation = nextNavigation;
    }

    public String getNextNavigation() {
        return nextNavigation;
    }

    public void setModeIn(String modeIn) {
        this.modeIn = modeIn;
    }

    public String getModeIn() {
        return modeIn;
    }

    public void setLoopCont(int loopCont) {
        this.loopCont = loopCont;
    }

    public int getLoopCont() {
        return loopCont;
    }

    public void setLWaveNbr(BigDecimal lWaveNbr) {
        this.lWaveNbr = lWaveNbr;
    }

    public BigDecimal getLWaveNbr() {
        return lWaveNbr;
    }

    public void setExitLoop(Boolean exitLoop) {
        this.exitLoop = exitLoop;
    }

    public Boolean getExitLoop() {
        return exitLoop;
    }

    public void setDisableAllFields(Boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public Boolean getDisableAllFields() {
        return disableAllFields;
    }

    public void setPage3Focus(String page3Focus) {
        this.page3Focus = page3Focus;
    }

    public String getPage3Focus() {
        return page3Focus;
    }

    public Boolean getDisabledPage3Location() {
        return getDisableAllFields() || !TI_CONFIRM_LOCATION_ID.equals(getPage3Focus());
    }

    public Boolean getDisabledPage3Container() {
        return getDisableAllFields() || !TI_LPN_CONTAINER_ID.equals(getPage3Focus());
    }

    public void setExecuteSomeKey(String executeSomeKey) {
        this.executeSomeKey = executeSomeKey;
    }

    public String getExecuteSomeKey() {
        return executeSomeKey;
    }

    public void setAfterProcessNextPick(int afterProcessNextPick) {
        this.afterProcessNextPick = afterProcessNextPick;
    }

    public int getAfterProcessNextPick() {
        return afterProcessNextPick;
    }
}
