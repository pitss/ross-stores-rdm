package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.stageroptions.sgstagerrelease.model.result.DBCallResult;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:3828974915674016345")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private static final String NOTHING = "NOTHING";
    private RichLink f3Link;
    private RichLink changeLocationHiddenLink;
    private RichInputText locationId;
    private RichIcon locationIdIcon;

    public Page1Backing() {

    }

    public void onChangedTiStartingLocationId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getStagerReleasePFBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeLocationHiddenLink());
            actionEvent.queue();
        }
    }

    public String f3Action() {
        String nav = null;
        if (isStagerReleaseS()) {
            ADFUtils.setBoundAttributeValue("TiStartingLocationId", null);
            if (YES.equals(ADFUtils.getBoundAttributeValue("Flow"))) {
                nav = "goPage6";
            } else {
                nav = "goPage4";
            }
        }
        return nav;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public String changeLocationHiddenAction() {
        String nav = null;
        if (!isNavigationExecuted()) {
            if (!getStagerReleasePFBean().getExecutedKey()) {
                removeErrorOfField(getLocationId(), getLocationIdIcon());
                if (isStagerReleaseS()) {
                    ADFUtils.setBoundAttributeValue("SortCriteria", null);
                }
                DBCallResult dbCallResult = (DBCallResult) ADFUtils.findOperation("pValStartingLocationId").execute();
                if (dbCallResult != null) {
                    nav = dbCallResult.getGoField();
                    if (dbCallResult.getErrorType() != null && dbCallResult.getErrorText() != null) {
                        if (nav != null && !nav.startsWith("B_STAGER_RELEASE")) { //Will navigate to other page
                            showErrorPopup(dbCallResult.getErrorType(), dbCallResult.getErrorText(), NOTHING, nav);
                            nav = null;
                        } else {
                            showMessagesPanel(dbCallResult.getErrorType(), dbCallResult.getErrorText());
                            setErrorOnField(getLocationId(), getLocationIdIcon());
                        }
                    }
                }
            }
            getStagerReleasePFBean().setExecutedKey(null);
        }

        return processNavigation(nav);
    }

    public void setChangeLocationHiddenLink(RichLink changeLocationHiddenLink) {
        this.changeLocationHiddenLink = changeLocationHiddenLink;
    }

    public RichLink getChangeLocationHiddenLink() {
        return changeLocationHiddenLink;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        setFocusOnUIComponent(getLocationId());
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("tiS".equals(field.getId()) && valueNotChanged(submittedVal, field.getValue())) {
                    onChangedTiStartingLocationId(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public String confirmErrorPopupAction() {
        getErrorPopup().hide();
        getStagerReleasePFBean().setDisableAllFields(false);
        //String nextAction = getStagerReleasePFBean().getNextAction();
        String nextNavigation = getStagerReleasePFBean().getNextNavigation();
        return processNavigation(nextNavigation);
    }

    private boolean isNavigationExecuted() {
        return isNavigationExecuted("com_ross_rdm_stageroptions_sgstagerrelease_view_pageDefs_Page1PageDef");
    }
}
