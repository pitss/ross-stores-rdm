package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.stageroptions.sgstagerrelease.model.result.DBCallResult;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page5.jspx
// ---
// ---------------------------------------------------------------------
public class Page5Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:-7856083827168370293")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page5Backing.class);
    private static final String NAVIGATESTARTINGLOC = "navigateStartingLocation";
    private static final String NAVIGATEMLPCONTAINERID = "navigateMlpContainerId";
    private static final String THIS_PAGE = "B_PAGE_5";
    private RichInputText containerId;
    private RichIcon containerIdIcon;
    private RichLink f3Link;
    private RichLink changeMlpHiddenLink;

    public Page5Backing() {

    }

    public void onChangedTiMlpContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getStagerReleasePFBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeMlpHiddenLink());
            actionEvent.queue();
        }
    }

    public String f3Action() {
        String nav = null;
        if (YES.equals(ADFUtils.getBoundAttributeValue("ScanMlpStart")) && isStagerReleaseMlpS()) {
            ADFUtils.setBoundAttributeValue("TiMlpContainerId", null);
            if (YES.equals(ADFUtils.getBoundAttributeValue("Flow"))) {
                nav = "goPage6";
            } else {
                nav = "goPage4";
            }
        } else {
            if ((YES.equals(ADFUtils.getBoundAttributeValue("ReleaseByItem")) && isStagerReleaseMlpS()) ||
                isStagerReleaseS()) {
                ADFUtils.findOperation("unlockTasksAndDirectives").execute();
            }
            ADFUtils.findOperation("saveLaborProd").execute();
            if (isStagerReleaseMlpS()) {
                nav = "B_PAGE_2.TI_CONFIRM_CONTAINER_ID";
                ADFUtils.setBoundAttributeValue("TiConfirmContainerId", null);
                ADFUtils.setBoundAttributeValue("TiMlpContainerId", null);
                // If release by item is No we can just return to the
                // previous screen without deleting the values on the
                // screen since we are still processing the same MLP
                if (!NO.equals(ADFUtils.getBoundAttributeValue("ReleaseByItem"))) {
                    resetFieldValues();

                    DBCallResult dbCallResult = callFGetTaskQueueRecordWrapper();
                    if (dbCallResult != null) {
                        if (dbCallResult.getGoField() != null) {
                            nav = dbCallResult.getGoField();
                        }
                        if (dbCallResult.getErrorType() != null && dbCallResult.getErrorText() != null) {
                            if (nav != null &&
                                !nav.startsWith(THIS_PAGE)) { //Will navigate to other page
                                showErrorPopup(dbCallResult.getErrorType(), dbCallResult.getErrorText(),
                                               NO.equals(dbCallResult.getFunctionResult()) ? NAVIGATEMLPCONTAINERID :
                                               NOTHING, nav);
                            } else {
                                showMessagesPanel(dbCallResult.getErrorType(), dbCallResult.getErrorText());
                                setErrorOnField(getContainerId(), getContainerIdIcon());
                            }
                            nav = null;
                        } else {
                            if (NO.equals(dbCallResult.getFunctionResult())) {
                                nav = navigateMlpContainerId();
                            }
                        }
                    }
                }
            } else {
                ADFUtils.findOperation("clearPage5").execute();
                nav = "B_PAGE_2.TI_CONFIRM_CONTAINER_ID";
                resetFieldValues();

                DBCallResult dbCallResult = callFGetTaskQueueRecordWrapper();
                if (dbCallResult != null) {
                    if (dbCallResult.getGoField() != null) {
                        nav = dbCallResult.getGoField();
                    }
                    if (dbCallResult.getErrorType() != null && dbCallResult.getErrorText() != null) {
                        if (nav != null &&
                            !nav.startsWith(THIS_PAGE)) { //Will navigate to other page
                            showErrorPopup(dbCallResult.getErrorType(), dbCallResult.getErrorText(),
                                           NO.equals(dbCallResult.getFunctionResult()) ? NAVIGATESTARTINGLOC : NOTHING,
                                           nav);
                        } else {
                            showMessagesPanel(dbCallResult.getErrorType(), dbCallResult.getErrorText());
                            setErrorOnField(getContainerId(), getContainerIdIcon());
                        }
                        nav = null;
                    } else {
                        if (NO.equals(dbCallResult.getFunctionResult())) {
                            nav = navigateStartingLocation();
                        }
                    }
                }
            }
        }
        ADFUtils.findOperation("disableReservedMode").execute(); //Reset to Managed Mode
        return processNavigation(nav);
    }

    private void resetFieldValues() {
        ADFUtils.setBoundAttributeValue("TiConfirmContainerId", null);
        ADFUtils.setBoundAttributeValue("PickToContainer", NO);
        ADFUtils.setBoundAttributeValue("PreviousTask", null);
        ADFUtils.setBoundAttributeValue("Undistributed", NO);
        ADFUtils.setBoundAttributeValue("MultiWave", NO);
        ADFUtils.setBoundAttributeValue("TiMlpContainerId", null);
        ADFUtils.setBoundAttributeValue("MlpContainerId1", null);
        ADFUtils.setBoundAttributeValue("MlpContainerId2", null);
        ADFUtils.setBoundAttributeValue("MlpContainerId", null);
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("tiM".equals(field.getId()) && valueNotChanged(submittedVal, field.getValue())) {
                    onChangedTiMlpContainerId(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public String changeMlpHiddenAction() {
        String nav = null;
        if (!isNavigationExecuted()) {
            if (!getStagerReleasePFBean().getExecutedKey()) {
                List errorMessages = null;
                removeErrorOfField(getContainerId(), getContainerIdIcon());
                boolean isValid = true;
                if ((NO.equals(ADFUtils.getBoundAttributeValue("ScanMlpStart")) && isStagerReleaseMlpS()) ||
                    isStagerReleaseS()) {
                    errorMessages = (List) ADFUtils.findOperation("callFValidateMlpCid").execute();
                    if (errorMessages != null && errorMessages.size() == 2) {
                        isValid = false;
                        setErrorOnField(getContainerId(), getContainerIdIcon());
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    }
                }
                if (isValid) {
                    errorMessages = (List) ADFUtils.findOperation("callValidateCreateMlp").execute();
                    if (errorMessages != null && errorMessages.size() == 2) {
                        setErrorOnField(getContainerId(), getContainerIdIcon());
                        showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    } else if (errorMessages != null && errorMessages.size() == 1) { //pBuiltIns
                        BigDecimal pBuiltIns = (BigDecimal) errorMessages.get(0);
                        if (BigDecimal.valueOf(1).equals(pBuiltIns)) {
                            nav = "B_PAGE_3.TI_LPN_CONTAINER_ID";
                        } else if (BigDecimal.valueOf(2).equals(pBuiltIns)) {
                            if (YES.equals(ADFUtils.getBoundAttributeValue("ScanMlpStart"))) {
                                DBCallResult dbCallResult = callFGetTaskQueueRecordWrapper();
                                if (dbCallResult != null) {
                                    if (dbCallResult.getGoField() != null) {
                                        nav = dbCallResult.getGoField();
                                    }
                                    if (dbCallResult.getErrorType() != null && dbCallResult.getErrorText() != null) {
                                        if (nav != null &&
                                            !nav.startsWith(THIS_PAGE)) { //Will navigate to other page
                                            showErrorPopup(dbCallResult.getErrorType(), dbCallResult.getErrorText(),
                                                           NOTHING, nav);
                                        } else {
                                            showMessagesPanel(dbCallResult.getErrorType(), dbCallResult.getErrorText());
                                            setErrorOnField(getContainerId(), getContainerIdIcon());
                                        }
                                        nav = null;
                                    }
                                }
                            }
                            ADFUtils.setBoundAttributeValue("TiMlpContainerId", null);
                        }
                    }
                    }
                }
            getStagerReleasePFBean().setExecutedKey(null);
        }

        return processNavigation(nav);
    }

    public void setChangeMlpHiddenLink(RichLink changeMlpHiddenLink) {
        this.changeMlpHiddenLink = changeMlpHiddenLink;
    }

    public RichLink getChangeMlpHiddenLink() {
        return changeMlpHiddenLink;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        setFocusOnUIComponent(getContainerId());
    }

    public String confirmErrorPopupAction() {
        getErrorPopup().hide();
        getStagerReleasePFBean().setDisableAllFields(false);
        refreshContentOfUIComponent(getContainerId());
        String nextAction = getStagerReleasePFBean().getNextAction();
        String nextNavigation = getStagerReleasePFBean().getNextNavigation();
        if (NAVIGATESTARTINGLOC.equalsIgnoreCase(nextAction)) {
            nextNavigation = navigateStartingLocation();
        } else if (NAVIGATEMLPCONTAINERID.equals(nextAction)) {
            nextNavigation = navigateMlpContainerId();
        }
        return processNavigation(nextNavigation);
    }

    private String navigateStartingLocation() {
        ADFUtils.findOperation("clearPage5").execute();
        // Clear Starting Location value
        ADFUtils.setBoundAttributeValue("TiStartingLocationId", null);
        // Navigate to the Starting Location field
        return "B_STAGER_RELEASE.TI_STARTING_LOCATION_ID";
    }

    private String navigateMlpContainerId() {
        // Clear the values on the scan MLP screen
        ADFUtils.findOperation("clearPage5").execute();
        ADFUtils.setBoundAttributeValue("ScanMlpStart", YES);
        ADFUtils.setBoundAttributeValue("ShowLabelSplit", Boolean.FALSE);
        return "B_PAGE_5.TI_MLP_CONTAINER_ID";
    }

    private boolean isNavigationExecuted() {
        return isNavigationExecuted("com_ross_rdm_stageroptions_sgstagerrelease_view_pageDefs_Page5PageDef");
    }
}
