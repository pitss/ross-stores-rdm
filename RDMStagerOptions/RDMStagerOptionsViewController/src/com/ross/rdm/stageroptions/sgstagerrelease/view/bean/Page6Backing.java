package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page6.jspx
// ---
// ---------------------------------------------------------------------
public class Page6Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:-373312184508858304")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page6Backing.class);
    private RichLink f3Link;
    private RichLink f6Link;
    private RichLink f4Link;
    private RichIcon upsIcon;
    private RichInputText ups;

    public Page6Backing() {

    }

    public String f3Action() {
        ADFUtils.findOperation("clearPage6").execute();
        return "goPage4";
    }

    public String f6Action() {
        if (this.callCheckScreenOptionPrivilege(isStagerReleaseMlpS() ?
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_mlp_s_B_PAGE_6_F6 :
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_s_B_PAGE_6_F6)) {
            ADFUtils.findOperation("loadUps").execute();
            return "goPage7";
        } else {
            this.showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }

    public String f4Action() {
        String nav = null;
        if (this.callCheckScreenOptionPrivilege(isStagerReleaseMlpS() ?
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_mlp_s_B_PAGE_6_F4 :
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_s_B_PAGE_6_F4)) {
            removeErrorOfField(getUps(), getUpsIcon());
            hideMessagesPanel();
            String ups = (String) ADFUtils.getBoundAttributeValue("Ups");
            if (StringUtils.isNotEmpty(ups)) {
                boolean valid = YES.equals(ADFUtils.findOperation("validateUps").execute());
                if (valid) {
                    if (isStagerReleaseMlpS()) {
                        nav = "goPage5";
                    } else {
                        nav = "goPage1";
                    }
                } else {
                    ADFUtils.setBoundAttributeValue("Ups", null);
                    showMessagesPanel(ERROR, getMessage("INV_UPS"));
                    setErrorOnField(getUps(), getUpsIcon());
                }
            } else {
                if (isStagerReleaseMlpS()) {
                    nav = "goPage5";
                } else {
                    nav = "goPage1";
                }
            }
        } else {
            this.showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return nav;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setUpsIcon(RichIcon upsIcon) {
        this.upsIcon = upsIcon;
    }

    public RichIcon getUpsIcon() {
        return upsIcon;
    }

    public void onChangedUps(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            removeErrorOfField(getUps(), getUpsIcon());
        }
    }

    public void setUps(RichInputText ups) {
        this.ups = ups;
    }

    public RichInputText getUps() {
        return ups;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("ups".equals(field.getId()) && valueNotChanged(submittedVal, field.getValue())) {
                    onChangedUps(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        setFocusOnUIComponent(getUps());
    }

    private boolean isNavigationExecuted() {
        return isNavigationExecuted("com_ross_rdm_stageroptions_sgstagerrelease_view_pageDefs_Page6PageDef");
    }
}
