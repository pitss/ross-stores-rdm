package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.util.Iterator;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.nav.RichLink;

import oracle.jbo.Key;

import org.apache.myfaces.trinidad.model.RowKeySet;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page4.jspx
// ---
// ---------------------------------------------------------------------
public class Page4Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:4949094673889237007")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page4Backing.class);
    private RichLink f3Link;
    private RichLink f4Link;
    private RichPopup confirmationPopup;
    private RichTable controlTable;

    public Page4Backing() {

    }

    public String f3Action() {
        ADFUtils.findOperation("trExit").execute();
        return logoutExitBTF();
    }

    public String f4Action() {
        String nav = null;
        if (this.callCheckScreenOptionPrivilege(isStagerReleaseMlpS() ?
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_mlp_s_CONTROL_F4 :
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_s_CONTROL_F4)) {
            String selected = getSelectedRow();
            boolean isHotel = "HOTEL".equals(selected);
            boolean isFlow = "FLOW".equals(selected);
            if (isHotel) {
                ADFUtils.setBoundAttributeValue("WorkLocalHotel", YES);
                ADFUtils.setBoundAttributeValue("WorkLocalFlow", NO);
                ADFUtils.setBoundAttributeValue("WorkLocalHotelFlow", NO);
            } else if (isFlow) {
                ADFUtils.setBoundAttributeValue("WorkLocalHotel", NO);
                ADFUtils.setBoundAttributeValue("WorkLocalFlow", YES);
                ADFUtils.setBoundAttributeValue("WorkLocalHotelFlow", NO);
            } else {
                ADFUtils.setBoundAttributeValue("WorkLocalHotel", NO);
                ADFUtils.setBoundAttributeValue("WorkLocalFlow", NO);
                ADFUtils.setBoundAttributeValue("WorkLocalHotelFlow", YES);
            }
            if (isStagerReleaseMlpS()) {
                showConfirmationPopup();
            } else {
                if (isFlow) {
                    nav = "goPage6";
                } else {
                    nav = "goPage1";
                }
            }
        } else {
            this.showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return nav;
    }

    public String confirmPopupYesAction() {
        return resultConfirmPopup(YES);
    }

    public String confirmPopupNoAction() {
        return resultConfirmPopup(NO);
    }

    public String resultConfirmPopup(String answer) {
        String nav = null;
        String selected = getSelectedRow();
        boolean isFlow = "FLOW".equals(selected);
        ADFUtils.setBoundAttributeValue("ReleaseByItem", answer);
        if (isFlow) {
            nav = "goPage6";
        } else {
            nav = "goPage5";
        }

        return nav;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    private void showConfirmationPopup() {
        //TODO focus, disable table?
        getConfirmationPopup().show(new RichPopup.PopupHints());
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setControlTable(RichTable controlTable) {
        this.controlTable = controlTable;
    }

    public RichTable getControlTable() {
        return controlTable;
    }

    private String getSelectedRow() {
        RowKeySet keySet = getControlTable().getSelectedRowKeys();
        String selected = null;
        if (keySet != null) {
            Iterator selectedEmpIter = keySet.iterator();
            while (selectedEmpIter.hasNext()) {
                Key key = (Key) ((List) selectedEmpIter.next()).get(0);
                selected = (String) key.getKeyValues()[0];
            }
        }
        return selected;
    }

    public String moveToNextRow() {
        DCIteratorBinding iter = ADFUtils.findIterator("ControlOptionsViewIterator");
        int index = iter.getCurrentRowIndexInRange();
        int newIndex = index + 1;
        if (index == 2) {
            newIndex = 0;
        }
        iter.setCurrentRowIndexInRange(newIndex);
        refreshContentOfUIComponent(getControlTable());
        return null;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        setFocusOnUIComponent(getControlTable());
    }
}
