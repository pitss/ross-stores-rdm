package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;
import com.ross.rdm.stageroptions.sgstagerrelease.model.result.DBCallResult;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SelTaskQueueRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage2ViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkLocalViewRowImpl;

import java.math.BigDecimal;

import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;

import org.apache.commons.lang.StringUtils;

public class StagerReleaseBaseBean extends RDMMobileBaseBackingBean {
    protected static final String SAME_TASK = "SAME_TASK";
    protected static final String NEW_TASK = "NEW_TASK";
    protected static final String NOT_FOUND = "@@@@";
    protected static final String TASK_QUEUE_ITER = "SelTaskQueueIterator";
    protected static final String NOTHING = "NOTHING";
    private RichPopup errorPopup;
    private RichIcon errorPopupIcon;
    private RichOutputText errorPopupOutput;
    private RichDialog errorPopupDialog;
    private RichPopup sgPickRemainPopup;
    private RichPanelFormLayout pflMain;
    private RichInputText containerId;
    private RichIcon containerIdIcon;
    private RichInputText locationId;
    private RichIcon locationIdIcon;
    private RichInputText lpn;
    private RichIcon lpnIcon;

    public StagerReleaseBaseBean() {
        super();
    }

    protected SgStagerReleaseBean getStagerReleasePFBean() {
        return (SgStagerReleaseBean) JSFUtils.getManagedBeanValue("pageFlowScope.SgStagerReleaseBean");
    }

    protected boolean isStagerReleaseMlpS() {
        return SgStagerReleaseBean.SG_STAGER_RELEASE_MLP_S.equals(getStagerReleasePFBean().getFormName());
    }

    protected boolean isStagerReleaseS() {
        return SgStagerReleaseBean.SG_STAGER_RELEASE_S.equals(getStagerReleasePFBean().getFormName());
    }

    protected String processNavigation(String goToField) {
        String navigateTo = null;
        if (goToField != null) {
            if (goToField.contains(".")) { //Example: B_PAGE_2.TI_CONFIRM_CONTAINER_ID
                String[] parts = goToField.split("[.]");
                if (parts != null && parts.length == 2) {
                    String page = parts[0];
                    String field = parts[1]; //set focus??
                    if ("B_STAGER_RELEASE".equals(page)) {
                        navigateTo = "goPage1";
                    } else if ("B_PAGE_2".equals(page)) {
                        navigateTo = "goPage2";
                    } else if ("B_PAGE_3".equals(page)) {
                        navigateTo = "goPage3";
                        getStagerReleasePFBean().setPage3Focus(field);
                    } else if ("CONTROL".equals(page)) {
                        navigateTo = "goPage4";
                    } else if ("B_PAGE_5".equals(page)) {
                        navigateTo = "goPage5";
                    } else if ("B_PAGE_6".equals(page)) {
                        navigateTo = "goPage6";
                    } else if ("B_PAGE_7".equals(page)) {
                        navigateTo = "goPage7";
                    }
                }
            } else { //Example: goPage1
                navigateTo = goToField;
            }
        }
        return navigateTo;
    }

    protected boolean valueNotChanged(String submittedVal, Object fieldValue) {
        if (fieldValue instanceof String) {
            return (StringUtils.isEmpty((String) fieldValue) && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase((String) fieldValue);
        } else {
            return (fieldValue == null && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase(fieldValue == null ? "" : fieldValue.toString());
        }
    }

    /**
     * As we have some procedures that return messages but also navigate,
     * we will navigate and will show the message in the destination page through onRegionLoad method
     */
    protected void showMessageIfNeeded() {
        if (getStagerReleasePFBean().hasMessageToShow()) {
            showMessagesPanel(getStagerReleasePFBean().getShowMessageType(),
                              getStagerReleasePFBean().getShowMessageText());
            getStagerReleasePFBean().showMessage(null, null);
        }
    }


    public void setErrorPopup(RichPopup errorPopup) {
        this.errorPopup = errorPopup;
    }

    public RichPopup getErrorPopup() {
        return errorPopup;
    }

    public void setErrorPopupIcon(RichIcon errorPopupIcon) {
        this.errorPopupIcon = errorPopupIcon;
    }

    public RichIcon getErrorPopupIcon() {
        return errorPopupIcon;
    }

    public void setErrorPopupOutput(RichOutputText errorPopupOutput) {
        this.errorPopupOutput = errorPopupOutput;
    }

    public RichOutputText getErrorPopupOutput() {
        return errorPopupOutput;
    }

    public void setErrorPopupDialog(RichDialog errorPopupDialog) {
        this.errorPopupDialog = errorPopupDialog;
    }

    public RichDialog getErrorPopupDialog() {
        return errorPopupDialog;
    }

    protected void showErrorPopup(String errorType, String errorText, String nextAction, String nextNavigation) {
        getStagerReleasePFBean().setDisableAllFields(true);
        if (WARN.equals(errorType)) {
            getErrorPopupDialog().setTitle("Warning");
            getErrorPopupIcon().setName("warning");
        } else if (ERROR.equals(errorType)) {
            getErrorPopupDialog().setTitle("Error");
            getErrorPopupIcon().setName("error");
        } else if (INFO.equals(errorType)) {
            getErrorPopupDialog().setTitle("Information");
            getErrorPopupIcon().setName("info");
        }

        getErrorPopupOutput().setValue(errorText);
        getErrorPopup().show(new RichPopup.PopupHints());
        getStagerReleasePFBean().setNextAction(nextAction);
        getStagerReleasePFBean().setNextNavigation(nextNavigation);
    }

    public void setSgPickRemainPopup(RichPopup sgPickRemainPopup) {
        this.sgPickRemainPopup = sgPickRemainPopup;
    }

    public RichPopup getSgPickRemainPopup() {
        return sgPickRemainPopup;
    }

    protected DBCallResult callFGetTaskQueueRecordWrapper() {
        return (DBCallResult) ADFUtils.findOperation("callFGetTaskQueueRecordWrapper").execute();
    }

    protected BigDecimal getWorkLocalWaveNbr() {
        BigDecimal waveNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("WaveNbr");
        return waveNbr != null ? waveNbr : new BigDecimal(0);
    }

    protected void loopWaveNbr() {
        if (!Boolean.TRUE.equals(getStagerReleasePFBean().getExitLoop())) {
            while (!Boolean.TRUE.equals(getStagerReleasePFBean().getExitLoop())) {
                if (getStagerReleasePFBean().getLoopCont() > 2) {
                    getStagerReleasePFBean().setLWaveNbr(getWorkLocalWaveNbr());
                }
                if (getStagerReleasePFBean().getLWaveNbr() != null &&
                    getStagerReleasePFBean().getLWaveNbr().compareTo(getWorkLocalWaveNbr()) == 0) {
                    getStagerReleasePFBean().setExitLoop(Boolean.TRUE);
                } else {
                    getStagerReleasePFBean().setLoopCont(getStagerReleasePFBean().getLoopCont() + 1);
                    getStagerReleasePFBean().setModeIn(SAME_TASK);
                    getStagerReleasePFBean().setAfterProcessNextPick(SgStagerReleaseBean.LOOP_WAVE_NBR);
                    processNextPick();
                    //if (true) { //IF  Process_Next_Pick ('SAME_TASK') = 'N' THEN
                    //getStagerReleasePFBean().setExitLoop(Boolean.TRUE);
                    //}
                }
            }
        } else {
            callGetToLoc();
        }
    }

    private void callGetToLoc() {
        String page3Location = (String) ADFUtils.findOperation("callGetToLoc").execute();
        ADFUtils.setBoundAttributeValue("Page3LocationId", page3Location);

        ADFUtils.setBoundAttributeValue("ShowLabelSplit", Boolean.TRUE);

        ADFUtils.invokeAction(processNavigation("B_PAGE_5.TI_MLP_CONTAINER_ID"));
    }

    protected void processNextPick() {
        ADFUtils.findOperation("processNextPickPart1").execute();
        initQueryTaskQueue();
        processTaskQueue(false);
    }

    protected void initQueryTaskQueue() {
        OperationBinding op = ADFUtils.findOperation("initQuery");
        op.getParamsMap().put("facility", ADFUtils.getBoundAttributeValue("FacilityId"));
        op.getParamsMap().put("container", ADFUtils.getBoundAttributeValue("ContainerId"));
        op.getParamsMap().put("activity", ADFUtils.getBoundAttributeValue("ActivityCode"));
        op.getParamsMap().put("user", ADFUtils.getBoundAttributeValue("UserId"));
        op.getParamsMap().put("task", ADFUtils.getBoundAttributeValue("TqTaskId"));
        op.execute();
    }

    protected void processTaskQueue(boolean nextRow) {
        SelTaskQueueRowImpl tqRec = getTaskQueueRow();
        SgStagerReleaseBPage2ViewRowImpl page2 =
            (SgStagerReleaseBPage2ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage2ViewIterator").getCurrentRow();
        SgStagerReleaseWorkLocalViewRowImpl workLocal = getWorkLocalRow();

        if (nextRow) {
            ViewObject vo = ADFUtils.findIterator(TASK_QUEUE_ITER).getViewObject();
            if (vo.hasNext()) {
                tqRec = (SelTaskQueueRowImpl) vo.next();
            } else {
                tqRec = null;
            }
        }

        if (tqRec != null) {
            workLocal.setTqTaskId(tqRec.getTaskId());
            page2.setContainerId(tqRec.getContainerId());
            page2.setLocationId(tqRec.getLocationId());
            page2.setPriority(tqRec.getPriority());
            page2.setItemId(tqRec.getItemId());
            page2.setColor(tqRec.getColor());
            page2.setPpk(tqRec.getUnitQty());
            workLocal.setPdRowid(tqRec.getRowId1());

            if (!SAME_TASK.equals(getStagerReleasePFBean().getModeIn())) {
                ADFUtils.setBoundAttributeValue("PickContainerQty", tqRec.getCidQty());
            }

            workLocal.setPickType(tqRec.getToLocationId());
            workLocal.setWaveNbr(tqRec.getWaveNbr());

            boolean validTask = Boolean.TRUE.equals(ADFUtils.findOperation("isValidTask").execute());
            if (validTask) {
                if (!SAME_TASK.equals(getStagerReleasePFBean().getModeIn())) {
                    showSgPickRemainPopup();
                } else {
                    processTaskQueuePerResponse(YES);
                }

                //Here there is an EXIT, so we won't follow the recursive call here
            } else {
                workLocal.setTqTaskId(null);
                page2.setItemId(null);
                page2.setColor(null);
                page2.setPpk(null);
                workLocal.setPdRowid(null);
                ADFUtils.setBoundAttributeValue("PickContainerQty", null);
                workLocal.setPickType(null);
                workLocal.setWaveNbr(null);

                //Recursive call to the next Task queue
                processTaskQueue(true);
            }
        } else { //NO MORE ROWS
            getStagerReleasePFBean().setExitLoop(Boolean.TRUE);

            if (getStagerReleasePFBean().getAfterProcessNextPick() == SgStagerReleaseBean.P_VAL_CONFIRM_LOC_2) {
                resetFieldsGoPage2AndKeyF5();
            }
        }
    }

    protected SelTaskQueueRowImpl getTaskQueueRow() {
        return (SelTaskQueueRowImpl) ADFUtils.findIterator(TASK_QUEUE_ITER).getCurrentRow();
    }

    protected SgStagerReleaseWorkLocalViewRowImpl getWorkLocalRow() {
        return (SgStagerReleaseWorkLocalViewRowImpl) ADFUtils.findIterator("SgStagerReleaseWorkLocalViewIterator").getCurrentRow();
    }

    protected void showSgPickRemainPopup() {
        getStagerReleasePFBean().setDisableAllFields(true);
        refreshContentOfUIComponent(getPflMain());
        getSgPickRemainPopup().show(new RichPopup.PopupHints());
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    public String confirmPopupYesAction() {
        getStagerReleasePFBean().setDisableAllFields(false);
        getSgPickRemainPopup().hide();
        processTaskQueuePerResponse(YES);
        return null;
    }

    public String confirmPopupNoAction() {
        getStagerReleasePFBean().setDisableAllFields(false);
        getSgPickRemainPopup().hide();
        processTaskQueuePerResponse(NO);
        return null;
    }

    protected void processTaskQueuePerResponse(String response) {
        refreshContentOfUIComponent(getPflMain());
        boolean error = false;
        SgStagerReleaseWorkLocalViewRowImpl workLocal = getWorkLocalRow();
        if (YES.equals(response)) {
            if (workLocal.getPdRowid() != null) {
                ADFUtils.findOperation("populateIntoWorkLocal").execute();
            }

            String nvlPickType = workLocal.getPickType() != null ? workLocal.getPickType() : NO;
            if ("BP".equals(nvlPickType) || "CP".equals(nvlPickType)) {
                String foundUpsCode = (String) ADFUtils.findOperation("getUpsCode").execute();
                if (NOT_FOUND.equals(foundUpsCode)) {
                    error = true;
                } else {
                    workLocal.setUnitPickSystemCode(foundUpsCode);
                }
            }

            if (!error) {
                if ((workLocal.getPreviousTask() != null && workLocal.getPdRowid() != null) ||
                    (workLocal.getPreviousTask() == null && workLocal.getPdRowid() == null)) {
                    workLocal.setPickToContainer(workLocal.getPickToContainer()); //NO SENSE, 6i code
                } else {
                    workLocal.setPickToContainer(NO);
                }
                if (getStagerReleasePFBean().getAfterProcessNextPick() == SgStagerReleaseBean.LOOP_WAVE_NBR) {
                    loopWaveNbr();
                } else if (getStagerReleasePFBean().getAfterProcessNextPick() ==
                           SgStagerReleaseBean.P_VAL_CONFIRM_LOC_2) {
                    if (NO.equals(response)) {
                        resetFieldsGoPage2AndKeyF5();
                    } else {
                        callSetupReleaseConfirm();
                    }
                }
            }
        } else {
            workLocal.setTqTaskId(null);
        }

        if (error) {
            showMessagesPanel(WARN, getMessage("SG_INV_UPZLOC"));
            if (getContainerId() != null && getContainerIdIcon() != null &&
                !getStagerReleasePFBean().getDisableAllFields()) {
                setErrorOnField(getContainerId(), getContainerIdIcon());
            } else if (getLocationId() != null && getLocationIdIcon() != null &&
                       !getStagerReleasePFBean().getDisabledPage3Location()) {
                setErrorOnField(getLocationId(), getLocationIdIcon());
            } else if (getLpn() != null && getLpnIcon() != null &&
                       !getStagerReleasePFBean().getDisabledPage3Container()) {
                setErrorOnField(getLpn(), getLpnIcon());
            }
        }
    }

    protected void callSetupReleaseConfirm() {
        String goField = null;
        Map result = (Map) ADFUtils.findOperation("callSetupReleaseConfirm").execute();
        BigDecimal returnVal = (BigDecimal) result.get("returnVal");
        BigDecimal lWaveNbr = (BigDecimal) result.get("lWaveNbr");
        if (BigDecimal.valueOf(1).equals(returnVal)) {
            getStagerReleasePFBean().setLoopCont(0);
            getStagerReleasePFBean().setLWaveNbr(lWaveNbr);
            getStagerReleasePFBean().setExitLoop(Boolean.FALSE);
            if (getStagerReleasePFBean().getLWaveNbr() != null &&
                getStagerReleasePFBean().getLWaveNbr().compareTo(getWorkLocalWaveNbr()) != 0) {
                loopWaveNbr();
            }
            callGetToLoc();
        } else if (BigDecimal.valueOf(2).equals(returnVal)) {
            goField = "B_PAGE_3.TI_CONFIRM_LOCATION_ID";
        } else if (BigDecimal.valueOf(3).equals(returnVal)) {
            goField = "B_PAGE_3.TI_LPN_CONTAINER_ID";
        }
        if (StringUtils.isNotEmpty(goField)) {
            ADFUtils.invokeAction(processNavigation(goField));
        }
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void setLpn(RichInputText lpn) {
        this.lpn = lpn;
    }

    public RichInputText getLpn() {
        return lpn;
    }

    public void setLpnIcon(RichIcon lpnIcon) {
        this.lpnIcon = lpnIcon;
    }

    public RichIcon getLpnIcon() {
        return lpnIcon;
    }

    protected void resetFieldsGoPage2AndKeyF5() {
        ADFUtils.findOperation("releaseCleanup").execute();
        ADFUtils.findOperation("clearBlockPage3").execute();

        SgStagerReleaseWorkLocalViewRowImpl workLocal =
            (SgStagerReleaseWorkLocalViewRowImpl) ADFUtils.findIterator("SgStagerReleaseWorkLocalViewIterator").getCurrentRow();
        workLocal.setUnitPickSystemCode(null);
        workLocal.setDistributedIndicator(null);
        workLocal.setTqTaskId(null);
        workLocal.setPdRowid(null);
        workLocal.setPickToContainer(NO);
        workLocal.setPreviousTask(null);
        workLocal.setUndistributed(NO);
        ADFUtils.setBoundAttributeValue("TiMlpContainerId", null); //:B_PAGE_5.TI_MLP_CONTAINER_ID := NULL;
        workLocal.setSplitType(null);

        SgStagerReleaseBPage2ViewRowImpl page2 =
            (SgStagerReleaseBPage2ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage2ViewIterator").getCurrentRow();
        String vTempCid = page2.getContainerId();
        BigDecimal vPriority = page2.getPriority();
        String vLocation = page2.getLocationId();
        ADFUtils.findOperation("clearBlockPage2").execute();
        page2.setContainerId(vTempCid);
        page2.setPriority(vPriority);
        page2.setLocationId(vLocation);

        getStagerReleasePFBean().setExecuteSomeKey("F5");
        ADFUtils.invokeAction(processNavigation("B_PAGE_2.TI_CONFIRM_CONTAINER_ID"));
    }

    protected boolean isNavigationExecuted(String currentPage) {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null && !currentPageDef.startsWith(currentPage)) {
            navigation = true;
        }
        return navigation;
    }

    protected boolean callCheckScreenOptionPrivilege(String optionName) {
        return callCheckScreenOptionPrivilege(optionName,
                                              isStagerReleaseMlpS() ?
                                              RoleBasedAccessConstants.FORM_NAME_sg_stager_release_mlp_s :
                                              RoleBasedAccessConstants.FORM_NAME_sg_stager_release_s);
    }

    protected String getMessageByCode(String msgCode) {
        return getMessage(msgCode, null, null, null);
    }
}
