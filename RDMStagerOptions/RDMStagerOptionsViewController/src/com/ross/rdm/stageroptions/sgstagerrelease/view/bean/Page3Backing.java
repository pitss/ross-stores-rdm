package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.stageroptions.sgstagerrelease.model.result.DBCallResult;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkLocalViewRowImpl;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page3.jspx
// ---
// ---------------------------------------------------------------------
public class Page3Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:4282003870555145121")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class);
    private RichLink f3Link;
    private RichLink changeLpnHiddenLink;
    private RichLink changeLocationHiddenLink;
    private static final String THIS_PAGE = "B_PAGE_3";

    public Page3Backing() {

    }

    public void onChangedTiLpnContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getStagerReleasePFBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeLpnHiddenLink());
            actionEvent.queue();
        }
    }

    private void removeAllErrors() {
        hideMessagesPanel();
        removeErrorOfField(getLpn(), getLpnIcon(), "");
        removeErrorOfField(getLocationId(), getLocationIdIcon());
    }

    public void onChangedTiConfirmLocationId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getStagerReleasePFBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeLocationHiddenLink());
            actionEvent.queue();
        }
    }

    private void pValConfirmLocationId() {
        String locationIdIn = (String) ADFUtils.getBoundAttributeValue("TiConfirmLocationId");
        String compareLocationIdIn = (String) ADFUtils.getBoundAttributeValue("LocationId");
        String mplContainerId = (String) ADFUtils.getBoundAttributeValue("MlpContainerId");
        boolean raised = false;
        if (StringUtils.isEmpty(locationIdIn)) {
            showMessagesPanel(ERROR, getMessage("SG_CNFRMLOC_REQ"));
            setErrorOnField(getLocationId(), getLocationIdIcon());
            raised = true;
        } else if (compareLocationIdIn != null &&
                   compareLocationIdIn.equals(ADFUtils.getBoundAttributeValue("PutawayStageLoc"))) {
            List errors = (List) ADFUtils.findOperation("callGetWhIdOfMlpLocation").execute();
            if (errors != null && !errors.isEmpty()) {
                showMessagesPanel((String) errors.get(0), (String) errors.get(1));
                setErrorOnField(getLocationId(), getLocationIdIcon());
                raised = true;
            }
        } else if (!locationIdIn.equals(compareLocationIdIn)) {
            showMessagesPanel(ERROR, getMessage("SG_INV_CNFRMLOC"));
            setErrorOnField(getLocationId(), getLocationIdIcon());
            raised = true;
        }

        String page3ContainerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
        if (!raised) {
            if (!page3ContainerId.equals(mplContainerId) || !YES.equals(ADFUtils.getBoundAttributeValue("MoveAll"))) {
                String vProcessNextPick = (String) ADFUtils.findOperation("processLpn").execute();
                if (YES.equals(vProcessNextPick)) {
                    getStagerReleasePFBean().setModeIn(SAME_TASK);
                    getStagerReleasePFBean().setAfterProcessNextPick(SgStagerReleaseBean.P_VAL_CONFIRM_LOC_1);
                    processNextPick();
                }
                incrementLabor(new BigDecimal(1));

                if (BigDecimal.valueOf(0).equals(ADFUtils.getBoundAttributeValue("PickContainerQty"))) {
                    getStagerReleasePFBean().setModeIn(NEW_TASK);
                    getStagerReleasePFBean().setAfterProcessNextPick(SgStagerReleaseBean.P_VAL_CONFIRM_LOC_2);
                    processNextPick();
                } else {
                    ADFUtils.setBoundAttributeValue("TiLpnContainerId", null);
                    ADFUtils.setBoundAttributeValue("TiConfirmLocationId", null);
                    ADFUtils.invokeAction(processNavigation("B_PAGE_3.TI_LPN_CONTAINER_ID"));
                }
            } else {
                ADFUtils.findOperation("processMlp").execute();
                incrementLabor((BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQty"));
                resetFieldsGoPage2AndKeyF5();
            }
        }
        refreshContentOfUIComponent(getPflMain());
    }

    private void incrementLabor(BigDecimal containerProcessed) {
        OperationBinding op = ADFUtils.findOperation("incrementLabor");
        op.getParamsMap().put("containerProcessed", containerProcessed);
        op.execute();
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public String f3Action() {
        String nav = null;
        boolean callFGetTaskQueueRecord = false;
        ADFUtils.findOperation("f3ExitPage3Part1").execute();
        if (isStagerReleaseMlpS()) {
            //Save the pick container qty in a temp variable to be used when release by item is 'N'
            BigDecimal tempPickQty = (BigDecimal) ADFUtils.getBoundAttributeValue("PickContainerQty");
            String releaseByItem = (String) ADFUtils.getBoundAttributeValue("ReleaseByItem");
            ADFUtils.findOperation("clearBlockPage3").execute();
            if (NO.equals(releaseByItem)) {
                ADFUtils.setBoundAttributeValue("PickContainerQty", tempPickQty);
            }
            nav = "B_PAGE_2.TI_CONFIRM_CONTAINER_ID";

            ADFUtils.setBoundAttributeValue("TiConfirmContainerId", null);
            ADFUtils.setBoundAttributeValue("MlpContainerId1", null);
            ADFUtils.setBoundAttributeValue("MlpContainerId2", null);

            //If release by item is No we can just return to the previous screen without deleting the values on the screen since we are still processing the same MLP
            if (!NO.equals(releaseByItem)) {
                resetFieldValues();
                callFGetTaskQueueRecord = true;
            }
        } else {
            ADFUtils.findOperation("clearBlockPage3").execute();
            nav = "B_PAGE_2.TI_CONFIRM_CONTAINER_ID";
            resetFieldValues();
            callFGetTaskQueueRecord = true;


        }
        if (callFGetTaskQueueRecord) {
            DBCallResult dbCallResult = callFGetTaskQueueRecordWrapper();
            if (dbCallResult != null) {
                if (dbCallResult.getGoField() != null) {
                    nav = dbCallResult.getGoField();
                }
                if (NO.equals(dbCallResult.getFunctionResult())) {
                    ADFUtils.findOperation("clearBlockPage3").execute();
                    if (isStagerReleaseMlpS()) {
                        //Clear the values on the scan MLP screen
                        ADFUtils.setBoundAttributeValue("TiMlpContainerId", null);
                        ADFUtils.setBoundAttributeValue("StartLocationId", null);
                        ADFUtils.setBoundAttributeValue("ZoneId", null);
                        ADFUtils.setBoundAttributeValue("ItemId", null);
                        ADFUtils.setBoundAttributeValue("ScanMlpStart", YES);
                        ADFUtils.setBoundAttributeValue("ShowLabelSplit", Boolean.FALSE);
                        nav = "B_PAGE_5.TI_MLP_CONTAINER_ID";
                    } else {
                        //Clear Starting Location value
                        ADFUtils.setBoundAttributeValue("TiStartingLocationId", null);
                        //Navigate to the Starting Location field
                        nav = "B_STAGER_RELEASE.TI_STARTING_LOCATION_ID";
                    }
                }
                if (dbCallResult.getErrorType() != null && dbCallResult.getErrorText() != null) {
                    if (nav != null && !nav.startsWith(THIS_PAGE)) { //Will navigate to other page
                        showErrorPopup(dbCallResult.getErrorType(), dbCallResult.getErrorText(), NOTHING, nav);
                    } else {
                        showMessagesPanel(dbCallResult.getErrorType(), dbCallResult.getErrorText());
                        setErrorOnField(getLpn(), getLpnIcon());
                    }
                    nav = null;
                }
            }
        }

        refreshContentOfUIComponent(getPflMain());
        return processNavigation(nav);
    }

    private void resetFieldValues() {
        SgStagerReleaseWorkLocalViewRowImpl workLocal =
            (SgStagerReleaseWorkLocalViewRowImpl) ADFUtils.findIterator("SgStagerReleaseWorkLocalViewIterator").getCurrentRow();
        ADFUtils.setBoundAttributeValue("TiConfirmContainerId", null);
        workLocal.setPickToContainer(NO);
        workLocal.setMultiWave(NO);
        workLocal.setPreviousTask(null);
        workLocal.setUndistributed(NO);
        ADFUtils.setBoundAttributeValue("TiMlpContainerId", null);
        ADFUtils.setBoundAttributeValue("MlpContainerId1", null);
        ADFUtils.setBoundAttributeValue("MlpContainerId2", null);
    }

    public String confirmErrorPopupAction() {
        refreshContentOfUIComponent(getPflMain());
        getErrorPopup().hide();
        getStagerReleasePFBean().setDisableAllFields(false); 
        String nextNavigation = getStagerReleasePFBean().getNextNavigation();

        return processNavigation(nextNavigation);
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getStagerReleasePFBean().getDisabledPage3Container()) {
            setFocusOnUIComponent(getLpn());
        } else if (!getStagerReleasePFBean().getDisabledPage3Location()) {
            setFocusOnUIComponent(getLocationId());
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("tiL".equals(field.getId()) && valueNotChanged(submittedVal, field.getValue())) {
                    onChangedTiLpnContainerId(new ValueChangeEvent(field, null, submittedVal));
                } else if ("tiC".equals(field.getId()) && valueNotChanged(submittedVal, field.getValue())) {
                    onChangedTiConfirmLocationId(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    private void focusLocationId() {
        getStagerReleasePFBean().setPage3Focus(SgStagerReleaseBean.TI_CONFIRM_LOCATION_ID);
        setFocusOnUIComponent(getLocationId());
        refreshContentOfUIComponent(getPflMain());
    }

    public void setChangeLpnHiddenLink(RichLink changeLpnHiddenLink) {
        this.changeLpnHiddenLink = changeLpnHiddenLink;
    }

    public RichLink getChangeLpnHiddenLink() {
        return changeLpnHiddenLink;
    }

    public void setChangeLocationHiddenLink(RichLink changeLocationHiddenLink) {
        this.changeLocationHiddenLink = changeLocationHiddenLink;
    }

    public RichLink getChangeLocationHiddenLink() {
        return changeLocationHiddenLink;
    }

    public String changeLpnHiddenAction() {
        if (!isNavigationExecuted()) {
            if (!getStagerReleasePFBean().getExecutedKey()) {
                removeAllErrors();
                String lpnContainerId = (String) ADFUtils.getBoundAttributeValue("TiLpnContainerId");
                Object pickCidQty = ADFUtils.getBoundAttributeValue("PickContainerQty");
                BigDecimal pickContainerQty = new BigDecimal(0);
                if (pickCidQty != null) 
                    pickContainerQty = (BigDecimal)pickCidQty;
                if (StringUtils.isEmpty(lpnContainerId) && BigDecimal.valueOf(0).equals(pickContainerQty)) {
                    focusLocationId();
                } else if (StringUtils.isEmpty(lpnContainerId) &&
                           BigDecimal.valueOf(0).compareTo(pickContainerQty) < 0) {
                    showMessagesPanel(WARN, getMessage("SG_MORE_LPNS"));
                    setErrorOnField(getLpn(), getLpnIcon());
                } else {
                    List errors = (List) ADFUtils.findOperation("validateLpnCntr").execute();
                    if (errors != null && errors.size() == 2) {
                        showMessagesPanel((String) errors.get(0), (String) errors.get(1));
                        setErrorOnField(getLpn(), getLpnIcon());
                    } else {
                        focusLocationId();
                    }
                }
            }
            getStagerReleasePFBean().setExecutedKey(null);
        }
        return null;
    }

    public String changeLocationHiddenAction() {
        if (!isNavigationExecuted()) {
          SgStagerReleaseBean pfBean = getStagerReleasePFBean();
          if (pfBean != null && Boolean.FALSE.equals(pfBean.getExecutedKey())) {
              removeAllErrors();
              pValConfirmLocationId();
          }
          getStagerReleasePFBean().setExecutedKey(null);
        }
        return null;
    }

    private boolean isNavigationExecuted() {
        return isNavigationExecuted("com_ross_rdm_stageroptions_sgstagerrelease_view_pageDefs_Page3PageDef");
    }
}
