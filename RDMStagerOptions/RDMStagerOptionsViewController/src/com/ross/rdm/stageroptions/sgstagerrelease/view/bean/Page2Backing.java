package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.stageroptions.sgstagerrelease.model.result.DBCallResult;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage2ViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage3ViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseBPage5ViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkLaborProdViewRowImpl;
import com.ross.rdm.stageroptions.sgstagerrelease.model.views.SgStagerReleaseWorkLocalViewRowImpl;

import java.math.BigDecimal;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:-8609800911785987893")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichLink f3Link;
    private RichLink f5Link;
    private static final String THIS_PAGE = "B_PAGE_2";
    private RichLink changeContainerHiddenLink;

    public Page2Backing() {

    }

    public void onChangedTiConfirmContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getStagerReleasePFBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getChangeContainerHiddenLink());
            actionEvent.queue();
        }
    }

    private void pValidateConfirmCntr() {
        String containerId = (String) ADFUtils.getBoundAttributeValue("TiConfirmContainerId");
        String compareContainerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
        String goField = null;
        if (StringUtils.isEmpty(containerId)) {
            showMessagesPanel(WARN, getMessage("SG_CNFRMCTR_REQ"));
            setErrorOnField(getContainerId(), getContainerIdIcon());
        } else if (!containerId.equals(compareContainerId)) {
            ADFUtils.setBoundAttributeValue("TiConfirmContainerId", null);
            showMessagesPanel(WARN, getMessage("SG_INV_CNFRMCTR"));
            setErrorOnField(getContainerId(), getContainerIdIcon());
        } else {
            callSetupReleaseConfirm();
        }
    }

    public String f3Action() {
        String nav = null;
        ADFUtils.findOperation("resetPickInProgress").execute();
        if (isStagerReleaseMlpS()) {
            SgStagerReleaseBPage5ViewRowImpl page5 =
                (SgStagerReleaseBPage5ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage5ViewIterator").getCurrentRow();
            SgStagerReleaseBPage3ViewRowImpl page3 =
                (SgStagerReleaseBPage3ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage3ViewIterator").getCurrentRow();
            SgStagerReleaseWorkLocalViewRowImpl workLocal = getWorkLocalRow();
            page5.setTiMlpContainerId(null);
            page3.setMlpContainerId(null);
            page5.setStartLocationId(null);
            page5.setZoneId(null);
            page5.setItemId(null);
            workLocal.setScanMlpStart(YES);
            ADFUtils.setBoundAttributeValue("ShowLabelSplit", Boolean.FALSE);
            nav = "goPage5";
        } else {
            ADFUtils.setBoundAttributeValue("TiStartingLocationId", null);
            nav = "goPage1";
        }
        return nav;
    }

    public String f5Action() {
        String nav = null;
        if (this.callCheckScreenOptionPrivilege(isStagerReleaseMlpS() ?
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_mlp_s_B_PAGE_2_F5 :
                                                RoleBasedAccessConstants.OPTION_NAME_sg_stager_release_s_B_PAGE_2_F5)) {
            removeErrorOfField(getContainerId(), getContainerIdIcon());
            refreshContentOfUIComponent(getPflMain());
            hideMessagesPanel();
            ADFUtils.findOperation("resetPickInProgress").execute();

            SgStagerReleaseBPage5ViewRowImpl page5 =
                (SgStagerReleaseBPage5ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage5ViewIterator").getCurrentRow();
            SgStagerReleaseBPage3ViewRowImpl page3 =
                (SgStagerReleaseBPage3ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage3ViewIterator").getCurrentRow();
            SgStagerReleaseBPage2ViewRowImpl page2 =
                (SgStagerReleaseBPage2ViewRowImpl) ADFUtils.findIterator("SgStagerReleaseBPage2ViewIterator").getCurrentRow();
            SgStagerReleaseWorkLocalViewRowImpl workLocal = getWorkLocalRow();

            // reset the container id and location id since the cursor in F_get_task_queue
            // will not fetch any value on the succeeding call becasue of conditions in the
            // cursor.
            if (isStagerReleaseMlpS()) {
                if (NO.equals(workLocal.getReleaseByItem())) {
                    page2.setContainerId(null);
                    page2.setLocationId(null);
                }
            }

            workLocal.setPickToContainer(NO);
            workLocal.setMultiWave(NO);
            workLocal.setPreviousTask(null);
            workLocal.setUndistributed(NO);
            workLocal.setSplitType(null);

            page5.setTiMlpContainerId(null);
            page5.setMlpContainerId1(null);
            page5.setMlpContainerId2(null);
            page3.setMlpContainerId(null);

            DBCallResult dbCallResult = callFGetTaskQueueRecordWrapper();
            if (dbCallResult != null) {
                if (dbCallResult.getGoField() != null) {
                    nav = dbCallResult.getGoField();
                }
                if (NO.equals(dbCallResult.getFunctionResult())) {
                    ADFUtils.findOperation("clearBlockPage2").execute();
                    if (isStagerReleaseMlpS()) {
                        page5.setTiMlpContainerId(null);
                        page5.setStartLocationId(null);
                        page5.setZoneId(null);
                        page5.setItemId(null);
                        workLocal.setScanMlpStart(YES);
                        ADFUtils.setBoundAttributeValue("ShowLabelSplit", Boolean.FALSE);
                        nav = "goPage5";
                    } else {
                        ADFUtils.setBoundAttributeValue("TiStartingLocationId", null);
                        nav = "goPage1";
                    }
                }
                if (dbCallResult.getErrorType() != null && dbCallResult.getErrorText() != null) {
                    if (nav != null && !nav.startsWith(THIS_PAGE)) { //Will navigate to other page
                        showErrorPopup(dbCallResult.getErrorType(), dbCallResult.getErrorText(), NOTHING, nav);
                    } else {
                        showMessagesPanel(dbCallResult.getErrorType(), dbCallResult.getErrorText());
                        setErrorOnField(getContainerId(), getContainerIdIcon());
                    }
                    nav = null;
                }
            }
        } else {
            this.showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return processNavigation(nav);
    }

    public String confirmErrorPopupAction() {
        refreshContentOfUIComponent(getPflMain());
        getErrorPopup().hide();
        getStagerReleasePFBean().setDisableAllFields(false);
        String nextNavigation = getStagerReleasePFBean().getNextNavigation();

        return processNavigation(nextNavigation);
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ("tiC".equals(field.getId()) && valueNotChanged(submittedVal, field.getValue())) {
                    onChangedTiConfirmContainerId(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getStagerReleasePFBean().getDisableAllFields()) {
            setFocusOnUIComponent(getContainerId());
        }
        String key = getStagerReleasePFBean().getExecuteSomeKey();
        if (StringUtils.isNotEmpty(key)) {
            if ("F5".equals(key)) { //From Page3
                String action = f5Action();
                if (action != null)
                    ADFUtils.invokeAction(action);
                getStagerReleasePFBean().setExecuteSomeKey(null); //Reset the key after first execution to avoid dup calls
            }
        }
    }

    private void startupLaborProd() {
        SgStagerReleaseWorkLaborProdViewRowImpl workLaborProd =
            (SgStagerReleaseWorkLaborProdViewRowImpl) ADFUtils.findIterator("SgStagerReleaseWorkLaborProdViewIterator").getCurrentRow();
        workLaborProd.setStartTime(new java.sql.Date(System.currentTimeMillis()));
        workLaborProd.setUnitsProcessed(new BigDecimal(0));
        workLaborProd.setContainersProcessed(new BigDecimal(0));
        workLaborProd.setOperationsPerformed(new BigDecimal(0));
        workLaborProd.setActivityCode("RELESE");
        workLaborProd.setReferenceCode(null);
    }

    public String changeContainerHiddenAction() {
        if (!isNavigationExecuted()) {
            if (!getStagerReleasePFBean().getExecutedKey()) {
                refreshContentOfUIComponent(getPflMain());
                hideMessagesPanel();
                removeErrorOfField(getContainerId(), getContainerIdIcon());
                pValidateConfirmCntr();
                if (!isMessageShown()) { //NO ERROR IN PREVIOUS METHOD
                    if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("WorkLaborActivityCode"))) {
                        startupLaborProd();
                    }
                }
            }
            getStagerReleasePFBean().setExecutedKey(null);
        }
        return null;
    }

    public void setChangeContainerHiddenLink(RichLink changeContainerHiddenLink) {
        this.changeContainerHiddenLink = changeContainerHiddenLink;
    }

    public RichLink getChangeContainerHiddenLink() {
        return changeContainerHiddenLink;
    }

    private boolean isNavigationExecuted() {
        return isNavigationExecuted("com_ross_rdm_stageroptions_sgstagerrelease_view_pageDefs_Page2PageDef");
    }
}
