package com.ross.rdm.stageroptions.sgstagerrelease.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMMobileBaseBackingBean;
import com.ross.rdm.stageroptions.view.framework.RDMStagerOptionsBackingBean;

import java.util.Iterator;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.jbo.Key;

import org.apache.myfaces.trinidad.model.RowKeySet;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page7.jspx
// ---
// ---------------------------------------------------------------------
public class Page7Backing extends StagerReleaseBaseBean {
    @SuppressWarnings("compatibility:2541592831193927168")
    private static final long serialVersionUID = 1L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page7Backing.class);
    private RichTable upsTable;

    public Page7Backing() {

    }

    public String selectRow() {
        RowKeySet keySet = getUpsTable().getSelectedRowKeys();
        String selected = null;
        if (keySet != null) {
            Iterator selectedEmpIter = keySet.iterator();
            while (selectedEmpIter.hasNext()) {
                Key key = (Key) ((List) selectedEmpIter.next()).get(0);
                selected = (String) key.getKeyValues()[0];
            }
        }
        ADFUtils.setBoundAttributeValue("Page6Ups", selected);
        return "goPage6";
    }

    public void setUpsTable(RichTable upsTable) {
        this.upsTable = upsTable;
    }

    public RichTable getUpsTable() {
        return upsTable;
    }
}
