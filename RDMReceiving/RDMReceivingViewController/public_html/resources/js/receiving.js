function onKeyPressedReceivingView(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onOpenExitPopupReceivingView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup');
    linkComp.focus();
}

function onF1PressedPopUpReceiving(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfActionEvent.queue(component.findComponent('yesLinkPopup'), true);
    }
    event.cancel();
}

function onKeyPressedFreightPage1(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function setTroubleCodesFocus() {
    setTimeout(function () {
        var tableId = 'tbb';
        var table = ("[id*='" + tableId + "']");
        SetFocusOnUIcomp(table);
        //        $(table).focus();
        // var focusOn = customFindElementById('tbb');
    },
0);
}

function onKeyPressedTroubleCode(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('f3'), true);
    }
    else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('f4'), true);
    }
    else if (keyPressed == AdfKeyStroke.F7_KEY) {
        AdfActionEvent.queue(component.findComponent('f7'), true);
    }
    else if (keyPressed == AdfKeyStroke.F8_KEY) {
        AdfActionEvent.queue(component.findComponent('f8'), true);
    }
    if (keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY) {
        event.cancel();
    }
}

function onOpenConfirmPopupFreightBill(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup');
    linkComp.focus();
}

function onKeyPressedFreightBillConfirmPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onKeyPressedFreightPage3(event) {
    var component = event.getSource();
    var item = component.getId();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}