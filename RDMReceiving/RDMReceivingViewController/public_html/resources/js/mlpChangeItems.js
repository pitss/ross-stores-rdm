var isKeyPressed = false;

function OnOpenMlpChangeItemPopup(event) {
    ((event.getSource()).findComponent("npc")).focus();
}

function OnOpenItemIsFoodPopup(event) {
    ((event.getSource()).findComponent("l1")).focus();
}

function okItemIsFoodPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l1', component);
    else 
        event.cancel();
}

function onKeyUpCidChangeItem(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY)
        this.isKeyPressed = true;
    this.onKeyUpPage1mlpChange(event);
}

function onKeyUpPage2(event) {
    this.onKeyUpPage2mlpChange(event);
}

function onKeyUpNewPo(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY)
        this.isKeyPressed = true;
    this.onKeyUpPage1mlpChange(event);
}

function onKeyUpNewItem(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        this.isKeyPressed = true;
        this.onKeyUpPage1mlpChange(event);
    }
    else {
        event.cancel();
    }
}

function yesNoMlpChangeItemPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('ypc', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('npc', component);
    else 
        event.cancel();
}

function onKeyUpPage2mlpChange(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    switch (keyPressed) {
        case AdfKeyStroke.F3_KEY:
            this.pressOnLink('f33', component);
            break;
        case AdfKeyStroke.F4_KEY:
            this.pressOnLink('f44', component);
            break;
        case AdfKeyStroke.F7_KEY:
            this.pressOnLink('f77', component);
            break;
        case AdfKeyStroke.F8_KEY:
            this.pressOnLink('f88', component);
            break;
        default :
            event.cancel();
        break 
    }
}

function onKeyUpPage4mlpChange(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY)
        this.pressOnLink('f333', component);
    event.cancel();
}

function onKeyUpPage1mlpChange(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (this.isKeyPressed) {
        switch (keyPressed) {
            case AdfKeyStroke.F2_KEY:
                this.pressOnLink('f2', component);
                break;
            case AdfKeyStroke.F3_KEY:
                this.pressOnLink('f3', component);
                break;
            case AdfKeyStroke.F4_KEY:
                this.pressOnLink('f4', component);
                break;
            case AdfKeyStroke.F5_KEY:
                this.pressOnLink('f5', component);
                break;
            case AdfKeyStroke.F9_KEY:
                this.pressOnLink('f9', component);
                break;
            case AdfKeyStroke.ENTER_KEY:
                this.triggerServerListener(event);
                break;
            case AdfKeyStroke.TAB_KEY:
                this.triggerServerListener(event);
                break;
            default :
                event.cancel();
            break 
        }
    }
}

function onBlurCidChangeItems(event) {
    this.triggerVcl(event);
}

function onBlurNewPoChangeItem(event) {
    this.triggerVcl(event);
}

function triggerServerListener(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    AdfCustomEvent.queue(component, "customKeyEvent", 
    {
        keyPressed : keyPressed, submittedValue : submittedValue
    },
true);
    event.cancel();
}

function triggerVcl(event) {
    var component = event.getSource();
    var submittedValue = component.getSubmittedValue();
    var keyPressed;
    try {
        keyPressed = event.getKeyCode();
        this.isKeyPressed = true;
    }
    catch (err) {
        if (submittedValue) {
            AdfValueChangeEvent.queue(component, null, submittedValue, false);
            this.isKeyPressed = false;
        }
    }
    inputOnBlurFocusHanlder(event);
    event.cancel();
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}

function setFocusPage2MlpChanges() {
    var tableId = 't1';
    var table = ("[id*='" + tableId + "']");
    $(table).focus();
}