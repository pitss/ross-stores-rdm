package com.ross.rdm.receiving.hhfreightbillverifys.view.bean;

import com.ross.rdm.receiving.view.framework.RDMReceivingBackingBean;
import java.sql.Types;
import javax.faces.event.ValueChangeEvent;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.jbo.ApplicationModule;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page4.jspx
// ---    
// ---------------------------------------------------------------------
public class Page4Backing extends RDMReceivingBackingBean {
  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon errorWarnInfoIcon;
  private RichOutputText errorWarnInfoMessage;
  private static ADFLogger _logger = ADFLogger.createADFLogger(Page4Backing.class);

  public Page4Backing() {

  }

  public void onChangedApptNbr(ValueChangeEvent vce) {
    //Forms  Module : hh_freight_bill_verify_s
    //Object        : GEN_FB.APPT_NBR.WHEN-VALIDATE-ITEM
    //Source Code   : 
    //
    //PKG_RECEIVING_MARKING.T_WHEN_VALIDATE_ITEM4(P_DISPLAY.V_DISPLAY_STRING ,:GEN_FB.APPT_NBR ,:WORK.FACILITY_ID);
    //IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    //


    // -------------------------------------------------------------------------------- 
    // -- Automatically converted by PitssCon 
    // -------------------------------------------------------------------------------- 
    //Form Module : hh_freight_bill_verify_s
    //Object      : GEN_FB.APPT_NBR.WHEN-VALIDATE-ITEM
    //
    //ApplicationModule appModule = getAppModule();
    //
    //
    //appModule.callStoredProcedure("pkg_receiving_marking.t_when_validate_item4",
    //                                             Types., 
    //p_display.v_display_string,
    //                      vce.getNewValue(),
    //                      appModule.findViewObject("HhFreightBillVerifySWorkView").getCurrentRow().getAttribute("FacilityId"));
    //if( p_display.handle_mess){ throw new JboException(); /*FORM_TRIGGER_FAILURE*/ }
    // --------------------------------------------------------------------------------
  }

  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()  {
    return allMessagesPanel;
  }
  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()  {
    return errorWarnInfoIcon;
  }
  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()  {
    return errorWarnInfoMessage;
  }
}
