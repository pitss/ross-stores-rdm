package com.ross.rdm.receiving.hhfreightbillverifys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.receiving.base.RDMMobileBaseBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page3.jspx
// ---
// ---------------------------------------------------------------------
public class Page3Backing extends RDMMobileBaseBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class);
    private static final String PAGE1_NAV = "goPage1";
    private RichIcon newFbsIcon;
    private RichIcon locationIdIcon;
    private RichInputText newFbs;
    private RichInputText locationId;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichPanelFormLayout pflMain;

    public Page3Backing() {

    }

    public void onChangedNewFbs(ValueChangeEvent vce) {
        updateModel(vce);
        validateNewFbs(true);
    }

    private boolean validateNewFbs(boolean focusNext) {
        boolean valid = true;
        List errorMessages = (List) ADFUtils.findOperation("tWhenValidateItem2").execute();
        if (errorMessages != null && errorMessages.size() == 2) {
            showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            setErrorOnField(getNewFbs(), getNewFbsIcon());
            valid = false;
        }
        if (valid && focusNext) {
            switchFocusToField(getLocationId(), HhFreightBillVerifySBean.LOCATION_ID);
        }
        refreshContentOfUIComponent(getPflMain());
        return valid;
    }

    public void onChangedLocationId(ValueChangeEvent vce) {
        updateModel(vce);
        validateLocationId((String) vce.getNewValue(), true);
    }

    public void setNewFbsIcon(RichIcon newFbsIcon) {
        this.newFbsIcon = newFbsIcon;
    }

    public RichIcon getNewFbsIcon() {
        return newFbsIcon;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void setNewFbs(RichInputText newFbs) {
        this.newFbs = newFbs;
    }

    public RichInputText getNewFbs() {
        return newFbs;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public String f4Action() {
        String nav = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_SPLIT_FBS_F4,RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)){
        if (!isMessageShown()) {
            if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("NewFbs"))) {
                switchFocusToField(getNewFbs(), HhFreightBillVerifySBean.NEW_FBS);
                setErrorOnField(getNewFbs(), getNewFbsIcon());
                showMessagesPanel(ERROR, getMessage(PARTIAL_ENTRY));
            } else if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("LocationId"))) {
                switchFocusToField(getLocationId(), HhFreightBillVerifySBean.LOCATION_ID);
                setErrorOnField(getLocationId(), getLocationIdIcon());
                showMessagesPanel(ERROR, getMessage(PARTIAL_ENTRY));
            } else if (validateNewFbs(false) &&
                       validateLocationId((String) ADFUtils.getBoundAttributeValue("LocationId"), false)) {
                ADFUtils.findOperation("copyFreightBill").execute();
                getPageFlowBean().setPage1Focus(HhFreightBillVerifySBean.ACTUAL_CONTAINERS);
                nav = PAGE1_NAV;
            }
        }
        refreshContentOfUIComponent(getPflMain());
        
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
            
          }
        return nav;
    }

    public String f3Action() {
        ADFUtils.setBoundAttributeValue("OldFbs", null);
        ADFUtils.setBoundAttributeValue("NewFbs", null);
        ADFUtils.setBoundAttributeValue("LocationId", null);
        
        if(ADFUtils.getBoundAttributeValue("ApptNbr") != null){//Data populated, go to Location field
            getPageFlowBean().setPage1Focus(HhFreightBillVerifySBean.LOCATION_ID);
        }else{
            getPageFlowBean().setPage1Focus(HhFreightBillVerifySBean.FREIGHT_BILL_SEQ_NBR);
        }
        return PAGE1_NAV;
    }

    private void switchFocusToField(RichInputText input, String focus) {
        removeAllErrors();
        getPageFlowBean().setPage3Focus(focus);
        setFocusOnUIComponent(input);
    }

    private HhFreightBillVerifySBean getPageFlowBean() {
        return (HhFreightBillVerifySBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhFreightBillVerifySBean");
    }

    private void removeAllErrors() {
        removeErrorOfField(getNewFbs(), getNewFbsIcon());
        removeErrorOfField(getLocationId(), getLocationIdIcon());
        hideMessagesPanel();
    }

    private boolean validateLocationId(String value, boolean focusNext) {
        boolean valid = true;
        if (StringUtils.isNotEmpty(value)) {
            String upperValue = value.toUpperCase();
            ADFUtils.setBoundAttributeValue("LocationId", upperValue);
            OperationBinding op = ADFUtils.findOperation("callPkgReceivingMarkingAdfVLocationId");
            op.getParamsMap().put("locationId", upperValue);
            List errorMessages = (List) op.execute();
            if (errorMessages != null && errorMessages.size() == 2) {
                setErrorOnField(getLocationId(), getLocationIdIcon());
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                ADFUtils.setBoundAttributeValue("LocationId", null);
                valid = false;
            }
        }
        if (valid && focusNext) {
            switchFocusToField(getNewFbs(), HhFreightBillVerifySBean.NEW_FBS);
        }
        refreshContentOfUIComponent(getPflMain());
        return valid;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                switch (field.getId()) {
                case "new1":
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        onChangedNewFbs(new ValueChangeEvent(field, null, submittedVal));
                    }
                    break;
                case "it1":
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        onChangedLocationId(new ValueChangeEvent(field, null, submittedVal));
                    }
                    break;
                default:
                }
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledNewFbs()) {
            setFocusOnUIComponent(getNewFbs());
        } else if (!getPageFlowBean().getDisabledLocationPage3()) {
            setFocusOnUIComponent(getLocationId());
        }
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }
}
