package com.ross.rdm.receiving.hhfreightbillverifys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhFreightBillVerifySBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhFreightBillVerifySBean.class);
    public static final String FREIGHT_BILL_SEQ_NBR = "FREIGHT_BILL_SEQ_NBR";
    public static final String LOCATION_ID = "LOCATION_ID";
    public static final String ACTUAL_CONTAINERS = "ACTUAL_CONTAINERS";
    public static final String ACTUAL_GOH_UNITS = "ACTUAL_GOH_UNITS";
    public static final String DAMAGED_CONTAINERS = "DAMAGED_CONTAINERS";
    public static final String DAMAGED_GOH_UNITS = "DAMAGED_GOH_UNITS";
    public static final String MISROUTE_QTY = "MISROUTE_QTY";
    public static final String TROUBLE_CODE = "TROUBLE_CODE";
    public static final String NEW_FBS = "NEW_FBS";
    private String page1Focus = null;
    private String page3Focus = null;
    private String page1LinkExecuted;
    private boolean disableFieldsPage1 = false;
    private Boolean executedKey = Boolean.FALSE;

    public void initTaskFlow() {

        //Forms  Module : hh_freight_bill_verify_s
        //Object        : WHEN-NEW-FORM-INSTANCE
        //Source Code   :
        //
        //Tr_Startup;
        //
        //startup_local;
        //-- DEFECT 351639 AHL BEGIN
        //AHL.SET_AHL_INFO(:Work.facility_id, :SYSTEM.CURRENT_FORM);
        //-- DEFECT 351639 AHL END

        OperationBinding ob = ADFUtils.findOperation("initTaskFlowFreightBillVerify");
        ob.execute();

    }

    public void setPage1Focus(String page1Focus) {
        this.page1Focus = page1Focus;
    }

    public String getPage1Focus() {
        return page1Focus;
    }

    public Boolean getDisabledFbs() {
        return disableFieldsPage1 || (page1Focus != null && !FREIGHT_BILL_SEQ_NBR.equals(page1Focus));
    }

    public Boolean getDisabledLocation() {
        return disableFieldsPage1 || !LOCATION_ID.equals(page1Focus);
    }

    public Boolean getDisabledActualContainers() {
        return disableFieldsPage1 || !ACTUAL_CONTAINERS.equals(page1Focus);
    }

    public Boolean getDisabledActualGohUnits() {
        return disableFieldsPage1 || !ACTUAL_GOH_UNITS.equals(page1Focus);
    }

    public Boolean getDisabledDamagedContainers() {
        return disableFieldsPage1 || !DAMAGED_CONTAINERS.equals(page1Focus);
    }

    public Boolean getDisabledDamagedGohUnits() {
        return disableFieldsPage1 || !DAMAGED_GOH_UNITS.equals(page1Focus);
    }

    public Boolean getDisabledMisrouteQty() {
        return disableFieldsPage1 || !MISROUTE_QTY.equals(page1Focus);
    }

    public Boolean getDisabledTroubleCode() {
        return disableFieldsPage1 || !TROUBLE_CODE.equals(page1Focus);
    }

    public void setPage1LinkExecuted(String page1LinkExecuted) {
        this.page1LinkExecuted = page1LinkExecuted;
    }

    public String getPage1LinkExecuted() {
        return page1LinkExecuted;
    }

    public void setPage3Focus(String page3Focus) {
        this.page3Focus = page3Focus;
    }

    public String getPage3Focus() {
        return page3Focus;
    }

    public Boolean getDisabledNewFbs() {
        return page3Focus != null && !NEW_FBS.equals(page3Focus);
    }

    public Boolean getDisabledLocationPage3() {
        return !LOCATION_ID.equals(page3Focus);
    }

    public void setDisableFieldsPage1(boolean disableFieldsPage1) {
        this.disableFieldsPage1 = disableFieldsPage1;
    }

    public boolean isDisableFieldsPage1() {
        return disableFieldsPage1;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }
}
