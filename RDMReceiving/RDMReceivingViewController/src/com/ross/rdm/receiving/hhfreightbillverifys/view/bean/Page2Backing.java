package com.ross.rdm.receiving.hhfreightbillverifys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.receiving.base.RDMMobileBaseBean;

import java.util.Iterator;
import java.util.List;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.jbo.Key;

import org.apache.myfaces.trinidad.model.RowKeySet;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMMobileBaseBean {
    @SuppressWarnings("compatibility:1927350193412152029")
    private static final long serialVersionUID = 1L;
    private static final int VISIBLE_ROWS = 9;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichTable codesTable;

    public Page2Backing() {

    }

    public String f3Action() {
        getPageFlowBean().setPage1Focus(HhFreightBillVerifySBean.TROUBLE_CODE);
        return "goPage1";
    }

    public String f4Action() {
        String value = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_CODES_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)) {

            RowKeySet keySet = getCodesTable().getSelectedRowKeys();
            if (keySet != null) {
                Iterator selectedEmpIter = keySet.iterator();
                while (selectedEmpIter.hasNext()) {
                    Key key = (Key) ((List) selectedEmpIter.next()).get(0);
                    value = (String) key.getKeyValues()[0];
                }
            }
            ADFUtils.setBoundAttributeValue("TroubleCode", value);
            getPageFlowBean().setPage1Focus(HhFreightBillVerifySBean.TROUBLE_CODE);
            return "goPage1";
        } else {
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
            return value;
        }
    }

    private void moveTablePage(int positionsToMove) {
        DCIteratorBinding it = ADFUtils.findIterator("TroubleCodesViewIterator");
        long totalRows = it.getEstimatedRowCount();
        if (totalRows > 0) {
            int newIndex = it.getCurrentRowIndexInRange() + positionsToMove;
            if (newIndex >= totalRows) {
                newIndex = Long.valueOf(totalRows).intValue() - 1;
            } else if (newIndex < 0) {
                newIndex = 0;
            }
            it.setCurrentRowIndexInRange(newIndex);
            refreshContentOfUIComponent(getCodesTable());
        }
    }

    public String f7Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_CODES_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)) {

            moveTablePage(-VISIBLE_ROWS);
        } else {
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));

        }
        return null;
    }

    public String f8Action() {
     
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_CODES_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)) {

            moveTablePage(VISIBLE_ROWS);
        } else {
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));

        }
        return null;
    }

    public void setCodesTable(RichTable codesTable) {
        this.codesTable = codesTable;
    }

    public RichTable getCodesTable() {
        return codesTable;
    }

    private HhFreightBillVerifySBean getPageFlowBean() {
        return (HhFreightBillVerifySBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhFreightBillVerifySBean");
    }

    public String getIconName() {
        String name = null;
        DCIteratorBinding it = ADFUtils.findIterator("TroubleCodesViewIterator");
        long totalRows = it.getEstimatedRowCount();
        int index = -1;
        if (totalRows > 0) {
            index = it.getCurrentRowIndexInRange();
            if (index == 0 || index == totalRows - 1) {
                name = INF_ICON;
            }
        }
        
        return name;
    }

    public String getMessageText() {
        String text = null;
        DCIteratorBinding it = ADFUtils.findIterator("TroubleCodesViewIterator");
        long totalRows = it.getEstimatedRowCount();
        int index = -1;
        if (totalRows > 0) {
            index = it.getCurrentRowIndexInRange();
            if (index == 0) {
                text = "FIRST RECORD";
            } else if (index == totalRows - 1) {
                text = "LAST RECORD";
            }
        }
        
        return text;
    }
}
