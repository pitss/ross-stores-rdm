package com.ross.rdm.receiving.hhfreightbillverifys.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.receiving.base.RDMMobileBaseBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMMobileBaseBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichInputText freightBillSeqNbr;
    private RichIcon freightBillSeqNbrIcon;
    private RichInputText locationId;
    private RichIcon locationIdIcon;
    private RichInputText actualContainers;
    private RichIcon actualContainersIcon;
    private RichInputText actualGohUnits;
    private RichIcon actualGohUnitsIcon;
    private RichInputText damagedContainers;
    private RichIcon damagedContainersIcon;
    private RichInputText damagedGohUnits;
    private RichIcon damagedGohUnitsIcon;
    private RichInputText misrouteQty;
    private RichIcon misrouteQtyIcon;
    private RichInputText troubleCode;
    private RichIcon troubleCodeIcon;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f6Link;
    private RichLink f7Link;
    private RichPanelFormLayout pflMain;
    private RichPopup confirmPopup;
    private RichOutputText confirmPopupText;
    private RichLink confirmPopupYesAction;
    private RichLink confirmPopupYesLink;
    private RichLink confirmPopupNoLink;
    private RichLink fbsHiddenLink;
    private RichLink locationHiddenLink;
    private RichLink cntrQtyHiddenLink;
    private RichLink gohQtyHiddenLink;
    private RichLink dmgCntrHiddenLink;
    private RichLink dmgGohHiddenLink;
    private RichLink misrouteHiddenLink;
    private RichLink troubleHiddenLink;

    public Page1Backing() {

    }

    public void onChangedFreightBillSeqNbr(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getFbsHiddenLink());
            actionEvent.queue();
        }
    }

    public String fbsHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateFbs()) {
            switchFocusToField(getLocationId(), HhFreightBillVerifySBean.LOCATION_ID);
        }
        return null;
    }

    private boolean validateFbs() {
        boolean valid = false;
        if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("FreightBillSeqNbr"))) {
            List errorMessages = (List) ADFUtils.findOperation("vFreightBill").execute();
            if (errorMessages != null && !errorMessages.isEmpty()) {
                if (errorMessages.size() == 2) {
                    setErrorOnField(getFreightBillSeqNbr(), getFreightBillSeqNbrIcon());
                    showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                } else if (errorMessages.size() == 1 &&
                           HhFreightBillVerifySBean.LOCATION_ID.equals(errorMessages.get(0))) {
                    valid = true;
                }
            }
            refreshContentOfUIComponent(getPflMain());
        }
        return valid;
    }

    public void onChangedLocationId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getLocationHiddenLink());
            actionEvent.queue();
        }
    }

    public String locationHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateLocation()) {
            switchFocusToField(getActualContainers(), HhFreightBillVerifySBean.ACTUAL_CONTAINERS);
        }
        return null;
    }

    private boolean validateLocation() {
        boolean valid = true;
        String value = (String) ADFUtils.getBoundAttributeValue("LocationId");
        if (StringUtils.isNotEmpty(value)) {
            value = value.toUpperCase();
            ADFUtils.setBoundAttributeValue("LocationId", value);
            OperationBinding op = ADFUtils.findOperation("callPkgReceivingMarkingAdfVLocationId");
            op.getParamsMap().put("locationId", value);
            List errorMessages = (List) op.execute();
            if (errorMessages != null && errorMessages.size() == 2) {
                setErrorOnField(getLocationId(), getLocationIdIcon());
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                ADFUtils.setBoundAttributeValue("LocationId", null);
                valid = false;
            }
            refreshContentOfUIComponent(getPflMain());
        }
        return valid;
    }

    public void onChangedTroubleCode(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getTroubleHiddenLink());
            actionEvent.queue();
        }
    }

    public String troubleHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateTroubleCode()) {
            if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("ApptNbr"))) {
                //If ApptNbr is null, FBS data has not been popupulated yet
                switchFocusToField(getFreightBillSeqNbr(), HhFreightBillVerifySBean.FREIGHT_BILL_SEQ_NBR);
            } else {
                switchFocusToField(getLocationId(), HhFreightBillVerifySBean.LOCATION_ID);
            }
        }
        return null;
    }

    private boolean validateTroubleCode() {
        boolean valid = true;
        if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("TroubleCode"))) {
            List errorMessages = (List) ADFUtils.findOperation("callPkgReceivingMarkingAdfVTroubleCode").execute();
            if (errorMessages != null && errorMessages.size() == 2) {
                setErrorOnField(getTroubleCode(), getTroubleCodeIcon());
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                valid = false;
            }
            refreshContentOfUIComponent(getPflMain());
        }
        return valid;
    }


    public void setFreightBillSeqNbr(RichInputText freightBillSeqNbr) {
        this.freightBillSeqNbr = freightBillSeqNbr;
    }

    public RichInputText getFreightBillSeqNbr() {
        return freightBillSeqNbr;
    }

    public void setFreightBillSeqNbrIcon(RichIcon freightBillSeqNbrIcon) {
        this.freightBillSeqNbrIcon = freightBillSeqNbrIcon;
    }

    public RichIcon getFreightBillSeqNbrIcon() {
        return freightBillSeqNbrIcon;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setLocationIdIcon(RichIcon locationIdIcon) {
        this.locationIdIcon = locationIdIcon;
    }

    public RichIcon getLocationIdIcon() {
        return locationIdIcon;
    }

    public void setActualContainers(RichInputText actualContainers) {
        this.actualContainers = actualContainers;
    }

    public RichInputText getActualContainers() {
        return actualContainers;
    }

    public void setActualContainersIcon(RichIcon actualContainersIcon) {
        this.actualContainersIcon = actualContainersIcon;
    }

    public RichIcon getActualContainersIcon() {
        return actualContainersIcon;
    }

    public void setActualGohUnits(RichInputText actualGohUnits) {
        this.actualGohUnits = actualGohUnits;
    }

    public RichInputText getActualGohUnits() {
        return actualGohUnits;
    }

    public void setActualGohUnitsIcon(RichIcon actualGohUnitsIcon) {
        this.actualGohUnitsIcon = actualGohUnitsIcon;
    }

    public RichIcon getActualGohUnitsIcon() {
        return actualGohUnitsIcon;
    }

    public void setDamagedContainers(RichInputText damagedContainers) {
        this.damagedContainers = damagedContainers;
    }

    public RichInputText getDamagedContainers() {
        return damagedContainers;
    }

    public void setDamagedContainersIcon(RichIcon damagedContainersIcon) {
        this.damagedContainersIcon = damagedContainersIcon;
    }

    public RichIcon getDamagedContainersIcon() {
        return damagedContainersIcon;
    }

    public void setDamagedGohUnits(RichInputText damagedGohUnits) {
        this.damagedGohUnits = damagedGohUnits;
    }

    public RichInputText getDamagedGohUnits() {
        return damagedGohUnits;
    }

    public void setDamagedGohUnitsIcon(RichIcon damagedGohUnitsIcon) {
        this.damagedGohUnitsIcon = damagedGohUnitsIcon;
    }

    public RichIcon getDamagedGohUnitsIcon() {
        return damagedGohUnitsIcon;
    }

    public void setMisrouteQty(RichInputText misrouteQty) {
        this.misrouteQty = misrouteQty;
    }

    public RichInputText getMisrouteQty() {
        return misrouteQty;
    }

    public void setMisrouteQtyIcon(RichIcon misrouteQtyIcon) {
        this.misrouteQtyIcon = misrouteQtyIcon;
    }

    public RichIcon getMisrouteQtyIcon() {
        return misrouteQtyIcon;
    }

    public void setTroubleCode(RichInputText troubleCode) {
        this.troubleCode = troubleCode;
    }

    public RichInputText getTroubleCode() {
        return troubleCode;
    }

    public void setTroubleCodeIcon(RichIcon troubleCodeIcon) {
        this.troubleCodeIcon = troubleCodeIcon;
    }

    public RichIcon getTroubleCodeIcon() {
        return troubleCodeIcon;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getPageFlowBean().getDisabledFbs()) {
            setFocusOnUIComponent(getFreightBillSeqNbr());
        } else if (!getPageFlowBean().getDisabledLocation()) {
            setFocusOnUIComponent(getLocationId());
        } else if (!getPageFlowBean().getDisabledActualContainers()) {
            setFocusOnUIComponent(getActualContainers());
        } else if (!getPageFlowBean().getDisabledActualGohUnits()) {
            setFocusOnUIComponent(getActualGohUnits());
        } else if (!getPageFlowBean().getDisabledDamagedContainers()) {
            setFocusOnUIComponent(getDamagedContainers());
        } else if (!getPageFlowBean().getDisabledDamagedGohUnits()) {
            setFocusOnUIComponent(getDamagedGohUnits());
        } else if (!getPageFlowBean().getDisabledMisrouteQty()) {
            setFocusOnUIComponent(getMisrouteQty());
        } else if (!getPageFlowBean().getDisabledTroubleCode()) {
            setFocusOnUIComponent(getTroubleCode());
        }
    }

    private HhFreightBillVerifySBean getPageFlowBean() {
        return (HhFreightBillVerifySBean) JSFUtils.getManagedBeanValue("pageFlowScope.HhFreightBillVerifySBean");
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            //String item = (String) clientEvent.getParameters().get("item");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7Link());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    private void queueValueChangeEvent(RichInputText field, String submittedVal) {
        switch (field.getId()) {
        case "fre":
            if (StringUtils.isEmpty(submittedVal)) {
                setErrorOnField(getFreightBillSeqNbr(), getFreightBillSeqNbrIcon());
                showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
            } else if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                       submittedVal.equals(field.getValue())) {
                onChangedFreightBillSeqNbr(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "loc1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedLocationId(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "cntrqty":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedActualContainers(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "gohqty":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedActualGohUnits(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "dmgcntr":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedDamagedContainers(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "dmggoh":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedDamagedGohUnits(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "misrt":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedMisrouteQty(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        case "tro1":
            if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty((String) submittedVal)) ||
                submittedVal.equals(field.getValue())) {
                onChangedTroubleCode(new ValueChangeEvent(field, null, submittedVal));
            }
            break;
        default:
        }
    }

    private void switchFocusToField(RichInputText input, String focus) {
        removeAllErrors();
        getPageFlowBean().setPage1Focus(focus);
        setFocusOnUIComponent(input);
    }

    private void removeAllErrors() {
        removeAllErrors(true);
    }

    private void removeAllErrors(boolean hidePanel) {
        removeErrorOfField(getFreightBillSeqNbr(), getFreightBillSeqNbrIcon());
        removeErrorOfField(getLocationId(), getLocationIdIcon());
        removeErrorOfField(getActualContainers(), getActualContainersIcon());
        removeErrorOfField(getActualGohUnits(), getActualGohUnitsIcon(),"");
        removeErrorOfField(getDamagedContainers(), getDamagedContainersIcon(),"");
        removeErrorOfField(getDamagedGohUnits(), getDamagedGohUnitsIcon(),"");
        removeErrorOfField(getMisrouteQty(), getMisrouteQtyIcon(),"");
        removeErrorOfField(getTroubleCode(), getTroubleCodeIcon(),"");
        if (hidePanel) {
            hideMessagesPanel();
        }
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    public String f6Action() {
        String nav = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_FREIGHT_BILL_F6,RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)){
         if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("FreightBillSeqNbr"))) {
            OperationBinding op = ADFUtils.findOperation("displayCodes");
            op.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
            op.execute();
            nav = "goPage2";
        }
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
          } 
        
        return nav;
    }

    public String f4Action() {
         if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_FREIGHT_BILL_F4,RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)){
            if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("LocationId"))) {
                setErrorOnFocusedField();
                showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
            } else if (validateTroubleCode()) {
                if (validateCurrentField()) {
                    processFreightBill("F4");
                    refreshContentOfUIComponent(getPflMain());
                } else if (HhFreightBillVerifySBean.LOCATION_ID.equals(getPageFlowBean().getPage1Focus())) {
                    //OTM2388, focus FBS
                    removeAllErrors(false);
                    getPageFlowBean().setPage1Focus(HhFreightBillVerifySBean.FREIGHT_BILL_SEQ_NBR);
                    setFocusOnUIComponent(getFreightBillSeqNbr());
                    setErrorOnFocusedField();
                }
            }
            refreshContentOfUIComponent(getPflMain());
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
          } 
        
        return null;
    }

    public String f7Action() {
        String nav = null;
    if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_freight_bill_verify_s_FREIGHT_BILL_F7,RoleBasedAccessConstants.FORM_NAME_hh_freight_bill_verify_s)){
        if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("LocationId"))) {
            setErrorOnFocusedField();
            showMessagesPanel(ERROR, getMessage("PARTIAL_ENTRY"));
        } else {
            nav = processFreightBill("F7");
            ADFUtils.setBoundAttributeValue("OldFbs", ADFUtils.getBoundAttributeValue("FreightBillSeqNbr"));
        }
        refreshContentOfUIComponent(getPflMain());
        }else{
            showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
          }
        return nav;
    }

    private String processFreightBill(String linkExecuted) {
        String nav = null;
        getPageFlowBean().setPage1LinkExecuted(linkExecuted); //TO FINISH AFTER CONFIRMING POPUP

        List errorMessages = (List) ADFUtils.findOperation("callPkgReceivingMarkingAdfProcessFreightBill1").execute();
        if (errorMessages != null) {
            if (errorMessages.size() == 2) { //ONE_QUANT message
                //switchFocusToField(getActualContainers(), HhFreightBillVerifySBean.ACTUAL_CONTAINERS);
                //setErrorOnField(getActualContainers(), getActualContainersIcon());
                setErrorOnFocusedField();
                showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            } else if (errorMessages.size() == 3 && CONF.equals(errorMessages.get(2))) {
                getPageFlowBean().setDisableFieldsPage1(true);
                refreshContentOfUIComponent(getPflMain());
                getConfirmPopupText().setValue(errorMessages.get(1));
                getConfirmPopup().show(new RichPopup.PopupHints());
            }
        } else {
            nav = finishProcessFreightBill();
        }

        return nav;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setConfirmPopupText(RichOutputText confirmPopupText) {
        this.confirmPopupText = confirmPopupText;
    }

    public RichOutputText getConfirmPopupText() {
        return confirmPopupText;
    }

    public void setConfirmPopupYesAction(RichLink confirmPopupYesAction) {
        this.confirmPopupYesAction = confirmPopupYesAction;
    }

    public RichLink getConfirmPopupYesAction() {
        return confirmPopupYesAction;
    }

    public void setConfirmPopupYesLink(RichLink confirmPopupYesLink) {
        this.confirmPopupYesLink = confirmPopupYesLink;
    }

    public RichLink getConfirmPopupYesLink() {
        return confirmPopupYesLink;
    }

    public String confirmPopupYesAction() {
        ADFUtils.findOperation("callPkgReceivingMarkingAdfProcessFreightBill2").execute();
        return finishProcessFreightBill();
    }

    public String confirmPopupNoAction() {
        closeConfirmPopupAndRefresh();

        //FOCUS THE SELECTED FIELD
        switch (getPageFlowBean().getPage1Focus()) {
        case HhFreightBillVerifySBean.FREIGHT_BILL_SEQ_NBR:
            switchFocusToField(getFreightBillSeqNbr(), HhFreightBillVerifySBean.FREIGHT_BILL_SEQ_NBR);
            break;
        case HhFreightBillVerifySBean.LOCATION_ID:
            switchFocusToField(getLocationId(), HhFreightBillVerifySBean.LOCATION_ID);
            break;
        case HhFreightBillVerifySBean.ACTUAL_CONTAINERS:
            switchFocusToField(getActualContainers(), HhFreightBillVerifySBean.ACTUAL_CONTAINERS);
            break;
        case HhFreightBillVerifySBean.ACTUAL_GOH_UNITS:
            switchFocusToField(getActualGohUnits(), HhFreightBillVerifySBean.ACTUAL_GOH_UNITS);
            break;
        case HhFreightBillVerifySBean.DAMAGED_CONTAINERS:
            switchFocusToField(getDamagedContainers(), HhFreightBillVerifySBean.DAMAGED_CONTAINERS);
            break;
        case HhFreightBillVerifySBean.DAMAGED_GOH_UNITS:
            switchFocusToField(getDamagedGohUnits(), HhFreightBillVerifySBean.DAMAGED_GOH_UNITS);
            break;
        case HhFreightBillVerifySBean.MISROUTE_QTY:
            switchFocusToField(getMisrouteQty(), HhFreightBillVerifySBean.MISROUTE_QTY);
            break;
        case HhFreightBillVerifySBean.TROUBLE_CODE:
            switchFocusToField(getTroubleCode(), HhFreightBillVerifySBean.TROUBLE_CODE);
            break;
        default:
        }
        return null;
    }

    public void setConfirmPopupNoLink(RichLink confirmPopupNoLink) {
        this.confirmPopupNoLink = confirmPopupNoLink;
    }

    public RichLink getConfirmPopupNoLink() {
        return confirmPopupNoLink;
    }

    public void performKeyConfirmPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmPopupYesLink());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getConfirmPopupNoLink());
                actionEvent.queue();
            }
        }
    }

    private String finishProcessFreightBill() {
        String nav = null;
        switch (getPageFlowBean().getPage1LinkExecuted()) {
        case "F4":
            ADFUtils.findOperation("clearFreightBillBlock").execute();
            closeConfirmPopupAndRefresh();
            switchFocusToField(getFreightBillSeqNbr(), HhFreightBillVerifySBean.FREIGHT_BILL_SEQ_NBR);
            break;
        case "F7":
            getPageFlowBean().setPage3Focus(HhFreightBillVerifySBean.NEW_FBS);
            getPageFlowBean().setDisableFieldsPage1(false);
            nav = "goPage3";
            break;
        default:
        }
        return nav;
    }

    private void closeConfirmPopupAndRefresh() {
        getPageFlowBean().setDisableFieldsPage1(false);
        refreshContentOfUIComponent(getPflMain());
        getConfirmPopup().hide();
    }

    public void onChangedActualContainers(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            updateModel(valueChangeEvent);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getCntrQtyHiddenLink());
            actionEvent.queue();
        }
    }

    public String cntrQtyHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateCntrQty()) {
            switchFocusToField(getActualGohUnits(), HhFreightBillVerifySBean.ACTUAL_GOH_UNITS);
        }
        return null;
    }

    private boolean validateCntrQty() {
        boolean valid = false;
        if (isNumericValue((String) ADFUtils.getBoundAttributeValue("ActualContainers"), getActualContainers(),
                           getActualContainersIcon())) {
            valid = true;
        }
        return valid;
    }

    public void onChangedActualGohUnits(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            updateModel(valueChangeEvent);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getGohQtyHiddenLink());
            actionEvent.queue();
        }
    }

    public String gohQtyHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateGohQty()) {
            switchFocusToField(getDamagedContainers(), HhFreightBillVerifySBean.DAMAGED_CONTAINERS);
        }
        return null;
    }

    private boolean validateGohQty() {
        boolean valid = false;
        if (isNumericValue((String) ADFUtils.getBoundAttributeValue("ActualGohUnits"), getActualGohUnits(),
                           getActualGohUnitsIcon())) {
            valid = true;
        }
        return valid;
    }

    public void onChangedDamagedContainers(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            updateModel(valueChangeEvent);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getDmgCntrHiddenLink());
            actionEvent.queue();
        }
    }

    public String dmgCntrHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateDmgCntr()) {
            switchFocusToField(getDamagedGohUnits(), HhFreightBillVerifySBean.DAMAGED_GOH_UNITS);
        }
        return null;
    }

    private boolean validateDmgCntr() {
        boolean valid = false;
        if (isNumericValue((String) ADFUtils.getBoundAttributeValue("DamagedContainers"), getDamagedContainers(),
                           getDamagedContainersIcon())) {
            valid = true;
        }
        return valid;
    }

    public void onChangedDamagedGohUnits(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            updateModel(valueChangeEvent);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getDmgGohHiddenLink());
            actionEvent.queue();
        }
    }

    public String dmgGohHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateDmgGoh()) {
            switchFocusToField(getMisrouteQty(), HhFreightBillVerifySBean.MISROUTE_QTY);
        }
        return null;
    }

    private boolean validateDmgGoh() {
        boolean valid = false;
        if (isNumericValue((String) ADFUtils.getBoundAttributeValue("DamagedGohUnits"), getDamagedGohUnits(),
                           getDamagedGohUnitsIcon())) {
            valid = true;
        }
        return valid;
    }

    public void onChangedMisrouteQty(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            updateModel(valueChangeEvent);
            getPageFlowBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getMisrouteHiddenLink());
            actionEvent.queue();
        }
    }

    public String misrouteHiddenAction() {
        if (!getPageFlowBean().getExecutedKey() && validateMisroute()) {
            switchFocusToField(getTroubleCode(), HhFreightBillVerifySBean.TROUBLE_CODE);
        }
        return null;
    }

    private boolean validateMisroute() {
        boolean valid = false;
        if (isNumericValue((String) ADFUtils.getBoundAttributeValue("MisrouteQty"), getMisrouteQty(),
                           getMisrouteQtyIcon())) {
            valid = true;
        }
        return valid;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_receiving_hhfreightbillverifys_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public void setFbsHiddenLink(RichLink fbsHiddenLink) {
        this.fbsHiddenLink = fbsHiddenLink;
    }

    public RichLink getFbsHiddenLink() {
        return fbsHiddenLink;
    }

    public void setLocationHiddenLink(RichLink locationHiddenLink) {
        this.locationHiddenLink = locationHiddenLink;
    }

    public RichLink getLocationHiddenLink() {
        return locationHiddenLink;
    }

    public void setCntrQtyHiddenLink(RichLink cntrQtyHiddenLink) {
        this.cntrQtyHiddenLink = cntrQtyHiddenLink;
    }

    public RichLink getCntrQtyHiddenLink() {
        return cntrQtyHiddenLink;
    }

    public void setGohQtyHiddenLink(RichLink gohQtyHiddenLink) {
        this.gohQtyHiddenLink = gohQtyHiddenLink;
    }

    public RichLink getGohQtyHiddenLink() {
        return gohQtyHiddenLink;
    }

    public void setDmgCntrHiddenLink(RichLink dmgCntrHiddenLink) {
        this.dmgCntrHiddenLink = dmgCntrHiddenLink;
    }

    public RichLink getDmgCntrHiddenLink() {
        return dmgCntrHiddenLink;
    }

    public void setDmgGohHiddenLink(RichLink dmgGohHiddenLink) {
        this.dmgGohHiddenLink = dmgGohHiddenLink;
    }

    public RichLink getDmgGohHiddenLink() {
        return dmgGohHiddenLink;
    }

    public void setMisrouteHiddenLink(RichLink misrouteHiddenLink) {
        this.misrouteHiddenLink = misrouteHiddenLink;
    }

    public RichLink getMisrouteHiddenLink() {
        return misrouteHiddenLink;
    }

    public void setTroubleHiddenLink(RichLink troubleHiddenLink) {
        this.troubleHiddenLink = troubleHiddenLink;
    }

    public RichLink getTroubleHiddenLink() {
        return troubleHiddenLink;
    }

    /**
     * Emulate the ENTER built-in of Forms
     */
    private boolean validateCurrentField() {
        boolean valid = true;
        if (!getPageFlowBean().getDisabledFbs()) {
            valid = validateFbs();
        } else if (!getPageFlowBean().getDisabledLocation()) {
            valid = validateLocation();
        } else if (!getPageFlowBean().getDisabledActualContainers()) {
            valid = validateCntrQty();
        } else if (!getPageFlowBean().getDisabledActualGohUnits()) {
            valid = validateGohQty();
        } else if (!getPageFlowBean().getDisabledDamagedContainers()) {
            valid = validateDmgCntr();
        } else if (!getPageFlowBean().getDisabledDamagedGohUnits()) {
            valid = validateDmgGoh();
        } else if (!getPageFlowBean().getDisabledMisrouteQty()) {
            valid = validateMisroute();
        } else if (!getPageFlowBean().getDisabledTroubleCode()) {
            valid = validateTroubleCode();
        }
        return valid;
    }

    private void setErrorOnFocusedField() {
        if (!getPageFlowBean().getDisabledFbs()) {
            setErrorOnField(getFreightBillSeqNbr(), getFreightBillSeqNbrIcon());
        } else if (!getPageFlowBean().getDisabledLocation()) {
            setErrorOnField(getLocationId(), getLocationIdIcon());
        } else if (!getPageFlowBean().getDisabledActualContainers()) {
            setErrorOnField(getActualContainers(), getActualContainersIcon());
        } else if (!getPageFlowBean().getDisabledActualGohUnits()) {
            setErrorOnField(getActualGohUnits(), getActualGohUnitsIcon());
        } else if (!getPageFlowBean().getDisabledDamagedContainers()) {
            setErrorOnField(getDamagedContainers(), getDamagedContainersIcon());
        } else if (!getPageFlowBean().getDisabledDamagedGohUnits()) {
            setErrorOnField(getDamagedGohUnits(), getDamagedGohUnitsIcon());
        } else if (!getPageFlowBean().getDisabledMisrouteQty()) {
            setErrorOnField(getMisrouteQty(), getMisrouteQtyIcon());
        } else if (!getPageFlowBean().getDisabledTroubleCode()) {
            setErrorOnField(getTroubleCode(), getTroubleCodeIcon());
        }
    }
}
