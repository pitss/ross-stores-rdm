package com.ross.rdm.receiving.hhinitiateunloads.view.bean;

import com.ross.rdm.common.view.common.NameMapping;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhInitiateUnloadSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhInitiateUnloadSBean.class);

    public void initTaskFlow() {

        //Forms  Module : hh_initiate_unload_s
        //Object        : WHEN-NEW-FORM-INSTANCE
        //Source Code   :
        //
        //Tr_Startup;
        //
        //startup_local;
        //-- Ross Store Request CR533 Start()
        //AHL.SET_AHL_INFO(:WORK.FACILITY_ID,:SYSTEM.CURRENT_FORM);
        //-- Ross Store Request CR533 End

    }
    
    public TaskFlowId getReceivingSTaskFlowId(){
        return TaskFlowId.parse(NameMapping.calculateDynamicTaskFlowId("hh_receiving_s", "shipping/"));//TODO which module?
    }
}
