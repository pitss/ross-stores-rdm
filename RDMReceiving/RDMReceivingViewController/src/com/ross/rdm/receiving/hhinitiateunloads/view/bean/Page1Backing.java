package com.ross.rdm.receiving.hhinitiateunloads.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.receiving.base.RDMMobileBaseBean;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMMobileBaseBean {
    private static final String APP_NBR = "APPR_NBR";
    private static final String DOOR_ID = "DOOR_ID";
    private static final String TRAILER_ID = "TRAILER_ID";
    private static final String SEAL_NBR = "SEAL_NBR";
    private static final String NONE_ENABLED = "NONE";
    private static final String CURRENT_FOCUS = "currentFocus";
    private RichOutputText exitPopupText;
    private RichPopup exitPopup;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichInputText doorId;
    private RichInputText trailerId;
    private RichInputText sealNbr;
    private RichIcon apptNbrIcon;
    private RichIcon doorIdIcon;
    private RichIcon trailerIdIcon;
    private RichInputText apptNbr;
    private RichLink f3ExitLink;
    private RichLink f4DoneLink;
    private RichLink f5CloseLink;
    private RichLink f7SuspendLink;
    private RichPanelFormLayout pflAppointment;
    private RichIcon sealNbrIcon;
    private RichIcon exitPopupIcon;

    public Page1Backing() {

    }

    public void onChangedApptNbr(ValueChangeEvent vce) {
        String newValue = (String) vce.getNewValue();
        if (isNumericValue(newValue, getApptNbr(), getApptNbrIcon())) {
            boolean nextField = true;
            if (StringUtils.isNotEmpty(newValue)) {
                this.updateModel(vce);

                List errorMessages = (List) ADFUtils.findOperation("callPkgReceivingMarkingAdfTKeyNextItem1").execute();
                if (errorMessages != null) {
                    this.setCurrentFocus(APP_NBR);
                    this.setErrorOnField(this.getApptNbr(), this.getApptNbrIcon());
                    this.showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
                    nextField = false;
                }
                //PKG_RECEIVING_MARKING.T_KEY_NEXT_ITEM1(P_DISPLAY.V_DISPLAY_STRING ,:APPOINTMENT.APPT_NBR ,:WORK.FACILITY_ID);
                //IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
                //next_item;
            }

            if (nextField) {
                this.setCurrentFocus(DOOR_ID);
                this.setFocusOnUIComponent(getDoorId());
            }
        }
    }

    public String f3ExitAction() {
        return this.logoutExitBTF();
    }

    public String f4OpenAction() {
        /*
         * /*Start ROSRCVNG Change
        IF PKG_RECEIVING_MARKING.V_TRAILER_STATUS( :appointment.trailer_id,:work.facility_id) = 'N' THEN
        Call_Msg_Window_P('INV_TRAILER','E');
            p_go_item('APPOINTMENT.Trailer_ID');
            raise form_trigger_failure;
            end if;
            /*End ROSRCVNG Change*/

        /* Start R.Bergantinos CO87759
        Open C_GET_DOOR_STATUS;
        FETCH C_GET_DOOR_STATUS INTO L_door_status;
        CLOSE C_GET_DOOR_STATUS;

        IF L_door_status <> 'AVAILABLE' THEN
        Call_Msg_Window_P('DOOR_NOT_AVAIL','E');
               raise FORM_TRIGGER_FAILURE;
        END IF;
        /* End R.Bergantinos CO87759

        ENTER;
        IF FORM_SUCCESS THEN
         initiate_unload;
        END IF;
         */
        String nav = null;
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_initiate_unload_s_APPOINTMENT_F4,RoleBasedAccessConstants.FORM_NAME_hh_initiate_unload_s)){
                                if (isNumericValue((String) ADFUtils.getBoundAttributeValue("ApptNbr1"), getApptNbr(), getApptNbrIcon())) {
                                    List errorsMessages = (List) ADFUtils.findOperation("initiateUnloadOpen").execute();
                                    this.refreshContentOfUIComponent(this.getPflAppointment());
                                    nav = manageResultAndNavigate(errorsMessages);
                                }
                            }else{
                           
                                  this.showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
                            }
        
        
        
        return nav;
    }

    public String f5CloseUnitAction() {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_initiate_unload_s_APPOINTMENT_F5,RoleBasedAccessConstants.FORM_NAME_hh_initiate_unload_s)){
                                if (isNumericValue((String) ADFUtils.getBoundAttributeValue("ApptNbr1"), getApptNbr(), getApptNbrIcon()) &&
                                    vApptDoorTrlt()) {
                                    BigDecimal exception =
                                        (BigDecimal) ADFUtils.findOperation("callPkgReceivingMarkingAdfCloseUnitIdAppt").execute();
                                    if (BigDecimal.valueOf(1).equals(exception)) {
                                        this.showExitPopup(MSGTYPE_M, this.getMessage("SUCCESS_OPER"));
                                    } else if (BigDecimal.valueOf(2).equals(exception)) {
                                        this.refreshContentOfUIComponent(this.getPflAppointment());
                                        this.setCurrentFocus(APP_NBR);//OTM1927
                                        this.setErrorOnFocusedField();
                                        this.showMessagesPanel(ERROR, this.getMessage("INV_STATUS"));
                                    } else if (BigDecimal.valueOf(3).equals(exception)) {
                                        this.refreshContentOfUIComponent(this.getPflAppointment());
                                        this.setErrorOnFocusedField();
                                        this.showMessagesPanel(ERROR, this.getMessage("INV_APPT_CLOSE"));
                                    }
                                }
                            }else{
                           
                                                          this.showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
                            }
        
        
        /*
         *        V_appt_door_trlt;
       PKG_RECEIVING_MARKING.CLOSE_UNIT_ID_APPT(:APPOINTMENT.APPT_NBR ,:WORK.FACILITY_ID ,:WORK.USER, V_EXCEPTION   );
       IF V_EXCEPTION = 1 THEN
       		CALL_MSG_WINDOW_P( 'SUCCESS_OPER');
     			Tr_Exit;
       ELSIF V_EXCEPTION = 2 THEN
       		clear_block(no_validate);
    			CALL_MSG_WINDOW_P( 'INV_STATUS','E');
    			RAISE form_trigger_failure;
       ELSIF V_EXCEPTION = 3 THEN
       	 	clear_block(no_validate);
    			CALL_MSG_WINDOW_P( 'INV_APPT_CLOSE','E');
    			RAISE form_trigger_failure;
       END IF;
         */
        return null;
    }

    private void showExitPopup(String type, String text) {
        this.setCurrentFocus(NONE_ENABLED);
        this.getExitPopupIcon().setName(WARN.equals(type) ? WRN_ICON : LOGO_ICON);
        this.getExitPopupText().setValue(text);
        this.getExitPopup().show(new RichPopup.PopupHints());
    }

    public String f7SuspendAction() {
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_initiate_unload_s_APPOINTMENT_F7,RoleBasedAccessConstants.FORM_NAME_hh_initiate_unload_s)){
                             if (isNumericValue((String) ADFUtils.getBoundAttributeValue("ApptNbr1"), getApptNbr(), getApptNbrIcon()) &&
                                 vApptDoorTrlt()) {
                                 List errorsMessages =
                                     (List) ADFUtils.findOperation("callPkgReceivingMarkingAdfSuspendUnitIdAppt").execute();
                                 if (errorsMessages == null) {
                                     this.showExitPopup(MSGTYPE_M, this.getMessage("SUCCESS_OPER"));
                                 } else {
                                     if (this.getMessage("INV_STATUS").equals(errorsMessages.get(1))) {
                                         this.setCurrentFocus(APP_NBR);//OTM1926
                                     }
                                     this.setErrorOnFocusedField();
                                     this.showMessagesPanel((String) errorsMessages.get(0), (String) errorsMessages.get(1));
                                 }
                             }
                         }else{
                        
                                                       this.showMessagesPanel(ERROR, this.getMessage("NOT_ALLOWED"));
                         }
        
       

        /*
         *     	 --start CR529
    	 V_EXCEPTION := 0;        	
    	 V_appt_door_trlt;
    	 PKG_RECEIVING_MARKING.SUSPEND_UNIT_ID_APPT(P_DISPLAY.V_DISPLAY_STRING ,:APPOINTMENT.APPT_NBR ,:APPOINTMENT.DOOR_ID ,:APPOINTMENT.TRAILER_ID ,:WORK.FACILITY_ID ,:WORK.USER, V_EXCEPTION  );
    	 IF P_DISPLAY.HANDLE_MESS THEN RAISE FORM_TRIGGER_FAILURE; END IF;
    	 IF V_EXCEPTION = 1 THEN
    	 		CALL_MSG_WINDOW_P( 'SUCCESS_OPER');
       		Tr_Exit;
    	 END IF;
    	
       --NULL;
       --end CR529
         */
        return null;
    }

    public String okExitPopupAction() {
        this.getExitPopup().hide();
        return this.logoutExitBTF();
    }

    public void setExitPopupText(RichOutputText exitPopupText) {
        this.exitPopupText = exitPopupText;
    }

    public RichOutputText getExitPopupText() {
        return exitPopupText;
    }

    public void setExitPopup(RichPopup exitPopup) {
        this.exitPopup = exitPopup;
    }

    public RichPopup getExitPopup() {
        return exitPopup;
    }

    private boolean vApptDoorTrlt() {
        boolean valid = true;
        List errorMessages = (List) ADFUtils.findOperation("callPkgReceivingMarkingAdfVApptDoorTrlt").execute();
        if (errorMessages != null) {
            valid = false;
            this.manageResultAndNavigate(errorMessages);
        }
        return valid;
    }

    public void setDoorId(RichInputText doorId) {
        this.doorId = doorId;
    }

    public RichInputText getDoorId() {
        return doorId;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setSealNbr(RichInputText sealNbr) {
        this.sealNbr = sealNbr;
    }

    public RichInputText getSealNbr() {
        return sealNbr;
    }

    public void setApptNbrIcon(RichIcon apptNbrIcon) {
        this.apptNbrIcon = apptNbrIcon;
    }

    public RichIcon getApptNbrIcon() {
        return apptNbrIcon;
    }

    public void setDoorIdIcon(RichIcon doorIdIcon) {
        this.doorIdIcon = doorIdIcon;
    }

    public RichIcon getDoorIdIcon() {
        return doorIdIcon;
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setApptNbr(RichInputText apptNbr) {
        this.apptNbr = apptNbr;
    }

    public RichInputText getApptNbr() {
        return apptNbr;
    }

    private String manageResultAndNavigate(List errorMessages) {
        String action = null;
        if (errorMessages != null) {
            if (errorMessages.size() == 1) {
                action = getNavigation((String) errorMessages.get(0));
            } else if (this.isExitError(errorMessages)) {
                this.showExitPopup((String) errorMessages.get(0), (String) errorMessages.get(1));
            } else if (errorMessages.size() > 1) {
                String goItem = getCurrentFocus();
                if (errorMessages.size() > 2) {
                    goItem = (String) errorMessages.get(2);
                }
                this.setCurrentFocus(goItem);
                if (getMessage("APPT_OPEN").equals(errorMessages.get(1))) {
                    this.setCurrentFocus(APP_NBR);
                    this.setFocusOnUIComponent(getApptNbr());
                }else{
                    setErrorOnFocusedField();
                }
                this.showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            }
        }
        return action;
    }

    private void setErrorOnFocusedField() {
        String goItem = this.getCurrentFocus();
        if (APP_NBR.equals(goItem)) {
            this.setErrorOnField(getApptNbr(), getApptNbrIcon());
        } else if (DOOR_ID.equals(goItem)) {
            this.setErrorOnField(getDoorId(), getDoorIdIcon());
        } else if (TRAILER_ID.equals(goItem)) {
            this.setErrorOnField(getTrailerId(), getTrailerIdIcon());
        } else if (SEAL_NBR.equals(goItem)) {
            this.setErrorOnField(getSealNbr(), getSealNbrIcon());
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            //String item = (String) clientEvent.getParameters().get("item");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4DoneLink());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF5CloseLink());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF7SuspendLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void setF4DoneLink(RichLink f4DoneLink) {
        this.f4DoneLink = f4DoneLink;
    }

    public RichLink getF4DoneLink() {
        return f4DoneLink;
    }

    public void setF5CloseLink(RichLink f5CloseLink) {
        this.f5CloseLink = f5CloseLink;
    }

    public RichLink getF5CloseLink() {
        return f5CloseLink;
    }

    public void setF7SuspendLink(RichLink f7SuspendLink) {
        this.f7SuspendLink = f7SuspendLink;
    }

    public RichLink getF7SuspendLink() {
        return f7SuspendLink;
    }

    private void queueValueChangeEvent(RichInputText textFields, Object newValue) {
        ValueChangeEvent vce = new ValueChangeEvent(textFields, null, newValue);
        String id = textFields.getId();
        switch (id) {
        case "app1":
            if ((StringUtils.isEmpty((String) textFields.getValue()) && StringUtils.isEmpty((String) newValue)) ||
                newValue.equals(textFields.getValue())) {
                this.onChangedApptNbr(vce);
            }
            break;
        case "doo1":
            this.setCurrentFocus(TRAILER_ID);
            this.setFocusOnUIComponent(this.getTrailerId());
            break;
        case "tra1":
            this.setCurrentFocus(SEAL_NBR);
            this.setFocusOnUIComponent(this.getSealNbr());
            break;
        case "sea1":
            this.setCurrentFocus(APP_NBR);
            this.setFocusOnUIComponent(this.getApptNbr());
            break;
        default:
            break;
        }

    }

    public void setPflAppointment(RichPanelFormLayout pflAppointment) {
        this.pflAppointment = pflAppointment;
    }

    public RichPanelFormLayout getPflAppointment() {
        return pflAppointment;
    }

    private String getCurrentFocus() {
        return (String) ADFContext.getCurrent().getPageFlowScope().get(CURRENT_FOCUS);
    }

    private void setCurrentFocus(String newFocus) {
        ADFContext.getCurrent().getPageFlowScope().put(CURRENT_FOCUS, newFocus);
        this.removeErrorOfAllFields();
    }

    public Boolean getDisabledApptNbr() {
        String focus = getCurrentFocus();
        return StringUtils.isNotEmpty(focus) && !APP_NBR.equals(focus);
    }

    public Boolean getDisabledDoorId() {
        return !DOOR_ID.equals(getCurrentFocus());
    }

    public Boolean getDisabledTrailerId() {
        return !TRAILER_ID.equals(getCurrentFocus());
    }

    public Boolean getDisabledSealNbr() {
        return !SEAL_NBR.equals(getCurrentFocus());
    }

    public void setSealNbrIcon(RichIcon sealNbrIcon) {
        this.sealNbrIcon = sealNbrIcon;
    }

    public RichIcon getSealNbrIcon() {
        return sealNbrIcon;
    }

    private void removeErrorOfAllFields() {
        this.removeErrorOfField(getApptNbr(), getApptNbrIcon());
        this.removeErrorOfField(getDoorId(), getDoorIdIcon());
        this.removeErrorOfField(getTrailerId(), getTrailerIdIcon());
        this.removeErrorOfField(getSealNbr(), getSealNbrIcon(), null);
        this.hideMessagesPanel();
    }

    private boolean isExitError(List errorMessages) {
        return errorMessages != null && errorMessages.size() == 3 && this.TR_EXIT.equals(errorMessages.get(2));
    }

    public void setExitPopupIcon(RichIcon exitPopupIcon) {
        this.exitPopupIcon = exitPopupIcon;
    }

    public RichIcon getExitPopupIcon() {
        return exitPopupIcon;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!getDisabledApptNbr()) {
            setFocusOnUIComponent(getApptNbr());
        }
    }
}
