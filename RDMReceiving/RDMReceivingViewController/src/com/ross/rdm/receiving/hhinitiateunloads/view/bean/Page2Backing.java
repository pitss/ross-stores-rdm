package com.ross.rdm.receiving.hhinitiateunloads.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.receiving.base.RDMMobileBaseBean;
import com.ross.rdm.receiving.view.framework.RDMReceivingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMMobileBaseBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichLink f3ExitLink;
    private RichLink changeUserChoiceLink;
    private RichInputText userChoice;
    private RichIcon userChoiceIcon;

    public Page2Backing() {

    }

    public String f3ExitAction() {
        ADFUtils.findOperation("clearAsnWindowBlock").execute();
        return "goPage1";
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                ActionEvent actionEvent = new ActionEvent(this.getChangeUserChoiceLink());
                actionEvent.queue();
            }
        }
    }

    public void setChangeUserChoiceLink(RichLink changeUserChoiceLink) {
        this.changeUserChoiceLink = changeUserChoiceLink;
    }

    public RichLink getChangeUserChoiceLink() {
        return changeUserChoiceLink;
    }

    public String changeUserChoiceAction() {
        List errorMessages = (List) ADFUtils.findOperation("callPkgReceivingMarkingAdfProcessChoice").execute();
        String navigate = null;
        if (errorMessages != null) {
            if (errorMessages.size() == 1) {
                navigate = getNavigation((String) errorMessages.get(0));
            } else if (errorMessages.size() == 2) {
                this.setErrorOnField(this.getUserChoice(), this.getUserChoiceIcon());
                this.showMessagesPanel((String) errorMessages.get(0), (String) errorMessages.get(1));
            }
        }
        return navigate;
    }

    public void setUserChoice(RichInputText userChoice) {
        this.userChoice = userChoice;
    }

    public RichInputText getUserChoice() {
        return userChoice;
    }

    public void setUserChoiceIcon(RichIcon userChoiceIcon) {
        this.userChoiceIcon = userChoiceIcon;
    }

    public RichIcon getUserChoiceIcon() {
        return userChoiceIcon;
    }
}
