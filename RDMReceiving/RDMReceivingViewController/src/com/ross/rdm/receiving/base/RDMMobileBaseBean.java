package com.ross.rdm.receiving.base;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import org.apache.commons.lang.StringUtils;
//TODO The common bean name "RDMHotelPickingBackingBean" should be changed, then this class hierarchy should be changed too
public class RDMMobileBaseBean extends RDMHotelPickingBackingBean {
    @SuppressWarnings("compatibility:4366738590034945377")
    private static final long serialVersionUID = 1L;
    public static final String TR_EXIT = "TR_EXIT";

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    public RDMMobileBaseBean() {
        super();
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void showMessagesPanel(String messageType, String msg) {
        // CLEAR PREVIOUS MESSAGES
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getErrorWarnInfoIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    protected String getMessage(String messageCode) {
        return this.getMessage(messageCode, "W", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                               (String) ADFUtils.getBoundAttributeValue("LanguageCode"));
    }

    protected void setErrorOnField(RichInputText input, RichIcon icon) {
        this.addErrorStyleToComponent(input);
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        this.setFocusOnUIComponent(input);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon) {
        this.removeErrorOfField(input, icon, REQ_ICON);
    }

    protected void removeErrorOfField(RichInputText input, RichIcon icon, String name) {
        this.removeErrorStyleToComponent(input);
        icon.setName(name);
        this.refreshContentOfUIComponent(icon);
    }

    protected boolean isMessageShown() {
        return StringUtils.isNotEmpty((String) this.getErrorWarnInfoMessage().getValue());
    }
    
    protected String getNavigation(String navigateTo) {
        String controlFlow = null;
        if ("hh_receiving_s".equalsIgnoreCase(navigateTo)) {
            controlFlow = "hhreceivings";
        } else if ("hh_asn_ucc_receiving_s".equalsIgnoreCase(navigateTo)) {
            //According to Erik's email on March 10th, 2016, ASN Flag will never be set to Y, so this case shouldn't happen.
            controlFlow = null;
        } else if ("hh_po_receiving_s".equalsIgnoreCase(navigateTo)) {
            //According to Erik's email on March 10th, 2016, ASN Flag will never be set to Y, so this case shouldn't happen.
            controlFlow = null;
        } else if ("ASN_WINDOW".equalsIgnoreCase(navigateTo)) {
            //According to Erik's email on March 10th, 2016, ASN Flag will never be set to Y, so this case shouldn't happen.
            controlFlow = "goPage2";
        } else if (TR_EXIT.equalsIgnoreCase(navigateTo)) {
            controlFlow = this.logoutExitBTF();
        }
        return controlFlow;
    }
    
    protected boolean isNumericValue(String submittedVal, RichInputText inputText, RichIcon icon) {
        boolean valid = true;
        if (StringUtils.isNotEmpty(submittedVal) && !submittedVal.matches("[0-9]+")) {
            this.setErrorOnField(inputText, icon);
            this.showMessagesPanel(WARN, this.getMessage("MUST_BE_NUMERIC"));
            valid = false;
        }
        return valid;
    }
}
