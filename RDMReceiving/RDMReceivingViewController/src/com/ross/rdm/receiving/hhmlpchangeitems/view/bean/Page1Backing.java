package com.ross.rdm.receiving.hhmlpchangeitems.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.receiving.hhmlpchangeitems.model.views.HhMlpChangeItemSMainViewRowImpl;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends MlpChangeItemBaseBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichIcon masterCidIcon;
    private RichIcon newPOIcon;
    private RichIcon newItemIcon;
    private RichInputText masterCID;
    private RichInputText newPoNbr;
    private RichPopup confirmPopup;
    private RichDialog confirmDialog;
    private RichOutputText dialogConfirmOutputText;
    private RichInputText newItemId;
    private RichInputText locationId;
    private RichInputText currPoNbr;
    private RichInputText itemId;
    private RichInputText apptNbr;
    private RichOutputText dialoItemIsFoodOutput;
    private RichPopup itemIsFoodPopup;

    public Page1Backing() {

    }

    public void onChangedMasterContainerId(ValueChangeEvent vce) {
        if (!this.isCurrentTaskFlowActive())
            return;
        Object currentCid = ADFUtils.getBoundAttributeValue(ATTR_CURRENT_CID);
        String newVal = (String) vce.getNewValue();
        boolean v_found = false;
        if (currentCid == null || !currentCid.equals(newVal)) {
            if (newVal != null && !"".equals(newVal)) {
                if (!this.lockUnlockContainer(UNLOCK_EXIT))
                    return;
            }
            ADFUtils.setBoundAttributeValue(ATTR_CURRENT_CID, newVal);
            try {
                Map returnVals = this.callDisplayMlpInfo(newVal);
                if (returnVals == null)
                    return;
                v_found = (Boolean) returnVals.get("pFound");
                if (v_found) {
                    this.processVfound();
                    return;
                } else {
                    List<String> errors = (List<String>) returnVals.get("Errors");
                    if (errors != null && errors.size() == 2) {
                        this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                               this.getAllMessagesPanel(), errors.get(0), errors.get(1));
                        this.setErrorOnMasterCid(true);
                        this.setValidCid(false);
                        return;
                    }
                    this.setValidCid(true);
                }
            } catch (Exception e) {
                return;
            }
            if (!this.lockUnlockContainer(LOCK))
                return;
        }
        if (this.isValidCid()) {
            this.hideErrorPanel();
            this.setErrorOnMasterCid(false);
            ADFUtils.setBoundAttributeValue(ATTR_CONFIRM_DNB_DNA, NO);
            this.setFocusNewPo();
        }
    }

    public void onChangedNewPoNbr(ValueChangeEvent vce) {
        if (!this.isCurrentTaskFlowActive())
            return;
        String newVal = (String) vce.getNewValue();
        this.hideErrorPanel();
        if (newVal != null && !newVal.isEmpty()) {
            OperationBinding ob = callValidateNewPoNbr(newVal);
            if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
                List errors = (List) ob.getResult();
                if (errors != null && !errors.isEmpty()) {
                    String errorType = (String) errors.get(0);
                    String error = (String) errors.get(1);
                    if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                        this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                               this.getAllMessagesPanel(), errorType, error);
                        this.setErrorOnNewPo(true);
                        this.setValidNewPo(false);
                        return;
                    } else {
                        this.setErrorOnNewPo(false);
                        this.setValidNewPo(true);
                        if (CONF.equalsIgnoreCase(errorType)) {
                            this.showConfirmNewPoNbr(error);
                        }
                    }
                } else {
                    this.setValidNewPo(true);
                    this.setErrorOnNewPo(false);
                    this.setFocusNewItem();
                }
            }
        } else {
            this.setValidNewPo(true);
            this.setFocusNewItem();
        }
    }

    private void processVfound() {
        this.invokeOperation(OPR_CLR_MAIN_BLK);
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                     this.getAllMessagesPanel(), ERROR, CID_IN_RTV);
        this.setErrorOnMasterCid(true);
        this.selectTextOnUIComponent(this.getMasterCID());
    }

    private void hideErrorPanel() {
        this.hideMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(), this.getAllMessagesPanel());
    }

    private void setErrorOnMasterCid(boolean set) {
        this.setErrorStyleOnComponent(this.getMasterCID(), this.getMasterCidIcon(), set);
        this.selectTextOnUIComponent(this.getMasterCID());
    }

    private void setErrorOnActiveComp(boolean set) {
        this.setErrorStyleOnComponent(this.getActiveInputText(), this.getActiveIcon(), set);
        this.selectTextOnUIComponent(this.getActiveInputText());
    }

    private void setErrorOnNewPo(boolean set) {
        this.setErrorStyleOnComponent(this.getNewPoNbr(), this.getNewPOIcon(), set);
        this.selectTextOnUIComponent(this.getNewPoNbr());
    }

    private void setErrorOnNewItem(boolean set) {
        this.setErrorStyleOnComponent(this.getNewItemId(), this.getNewItemIcon(), set);
        this.selectTextOnUIComponent(this.getNewItemId());
    }

    private void showPartialEntry() {
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                     this.getAllMessagesPanel(), ERROR, "PARTIAL_ENTRY");

    }

    public void setValidCid(boolean is) {
        this.getMlpChangeItemBean().setValidCid(is);
    }

    public boolean isValidCid() {
        return this.getMlpChangeItemBean().isValidCid();
    }

    public void setValidNewPo(boolean is) {
        this.getMlpChangeItemBean().setValidNewPo(is);
    }

    public boolean isValidNewPo() {
        return this.getMlpChangeItemBean().isValidNewPo();
    }


    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String currentFocus = this.getMlpChangeItemBean().getIsFocusOn();
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (ATTR_MASTER_CID.equalsIgnoreCase(currentFocus)) {
                    if (this.validateMasterCid())
                        this.onChangedMasterContainerId(new ValueChangeEvent(this.getMasterCID(), null,
                                                                             submittedValue));
                } else if (ATTR_NEW_PO.equalsIgnoreCase(currentFocus)) {
                    this.onChangedNewPoNbr(new ValueChangeEvent(this.getNewPoNbr(), null, submittedValue));
                } else if (ATTR_NEW_ITEM.equalsIgnoreCase(currentFocus)) {
                    this.hideErrorPanel();
                    this.setFocusOnMasterContainer();
                }
            }
        }
        _logger.info("performKey() End");
    }

    private Map callDisplayMlpInfo(String containerId) {
        OperationBinding ob = ADFUtils.findOperation(OPR_DISPLAY_MLP_INFO);
        ob.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID));
        ob.getParamsMap().put("masterCid", containerId);
        return (Map) ob.execute();
    }

    public boolean lockUnlockContainer(String processIn) {
        if (LOCK.equalsIgnoreCase(processIn)) {
            List checkResult = this.callCheckLock("INITIAL_CHECK");
            String lockCheck = null;
            String lockingUser = null;
            if (checkResult != null) {
                if (checkResult.size() == 2) {
                    lockingUser = (String) checkResult.get(0);
                    lockCheck = (String) checkResult.get(1);
                }
            }
            if (LOCKED.equalsIgnoreCase(lockCheck)) {
                return this.showMlpLockedByUser(lockingUser);
            } else {
                this.callOperLock("insert_lock", "");
            }
        } else {
            ADFUtils.setBoundAttributeValue(ATTR_CURRENT_CID, null);
            String closeLockParam = null;
            if (UNLOCK_EXIT.equalsIgnoreCase(processIn)) {
                closeLockParam = "D";
            } else if (UNLOCK_MLP.equalsIgnoreCase(processIn)) {
                closeLockParam = "M";
            }
            this.callOperLock("close_lock", closeLockParam);
        }

        return true;
    }

    private boolean showMlpLockedByUser(String lockingUser) {
        String errorMlpLocked = this.getMessage(MLP_IN_USE, ERROR) + " " + lockingUser;
        this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(), this.getAllMessagesPanel(),
                               ERROR, errorMlpLocked);
        this.setErrorOnActiveComp(true);
        return false;
    }

    private void callOperLock(String procedureName, String closeLockParam) {
        OperationBinding ob = ADFUtils.findOperation(OPR_LOCK);
        ob.getParamsMap().put("lockProcedureName", procedureName);
        ob.getParamsMap().put("closeLockParam", closeLockParam);
        ob.execute();
    }

    public void yesConfirmListener(ActionEvent actionEvent) {
        String launcherVar = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        if ("PoNbrConfirm".equalsIgnoreCase(launcherVar)) {
            ADFUtils.setBoundAttributeValue("ConfirmDnbDna", YES);
            this.setFocusNewItem();
        } else if ("F5Confirm".equalsIgnoreCase(launcherVar)) {
            this.proccesConfirmF5Yes("F5");
        } else if ("validateItem".equalsIgnoreCase(launcherVar)) {
            ADFUtils.setBoundAttributeValue("ConfirmDnbDna", YES);
            this.setFocusNewItem();
        }
    }


    public HhMlpChangeItemSMainViewRowImpl getMainCurrent() {
        return (HhMlpChangeItemSMainViewRowImpl) ADFUtils.findIterator("HhMlpChangeItemSMainViewIterator").getCurrentRow();
    }

    public void noConfirmActionListener(ActionEvent actionEvent) {
        String launcherVar = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        this.setFocusOnActiveComponent();
        if ("PoNbrConfirm".equalsIgnoreCase(launcherVar)) {
            ADFUtils.setBoundAttributeValue("ConfirmDnbDna", NO);
        } else if ("F5Confirm".equalsIgnoreCase(launcherVar)) {
            return;
        } else if ("validateItem".equalsIgnoreCase(launcherVar)) {
            ADFUtils.setBoundAttributeValue("ConfirmDnbDna", NO);
        }
    }

    private void setFocusNewItem() {
        this.disableActiveInputText();
        this.getNewItemId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getNewItemId());
        this.getMlpChangeItemBean().setIsFocusOn(ATTR_NEW_ITEM);
        this.setFocusOnUIComponent(this.getNewItemId());
    }

    private void setFocusNewPo() {
        this.disableActiveInputText();
        this.getNewPoNbr().setDisabled(false);
        this.refreshContentOfUIComponent(this.getNewPoNbr());
        this.getMlpChangeItemBean().setIsFocusOn(ATTR_NEW_PO);
        this.setFocusOnUIComponent(this.getNewPoNbr());
    }

    private void setFocusOnMasterContainer() {
        this.disableActiveInputText();
        this.getMasterCID().setDisabled(false);
        this.refreshContentOfUIComponent(this.getMasterCID());
        this.getMlpChangeItemBean().setIsFocusOn(ATTR_MASTER_CID);
        this.setFocusOnUIComponent(this.getMasterCID());
    }

    private void setFocusOnActiveComponent() {
        this.getActiveInputText().setDisabled(false);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.setFocusOnUIComponent(this.getActiveInputText());
    }

    private void disableActiveInputText() {
        this.setErrorOnActiveComp(false);
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
    }


    public void processFKey(String fKey) {
        ADFUtils.setBoundAttributeValue("DisplayMessageFlag", NO);
        if (ADFUtils.getBoundAttributeValue("NewItemId") == null &&
            ADFUtils.getBoundAttributeValue("NewPoNbr") == null) {
            this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                         this.getAllMessagesPanel(), ERROR, "MISS_PO_ITEM");
            this.setErrorOnActiveComp(true);
            return;
        }
        if (this.callValidateHazmatItem())
            if ("F5".equalsIgnoreCase(fKey))
                this.showConfirmF5Popup();
            else if ("F4".equalsIgnoreCase(fKey))
                if (this.callValidateItemPo())
                    this.continueProcess(fKey);
    }

    private void proccesConfirmF5Yes(String fKey) {
        if (this.callValidateItemPo())
            this.continueProcess(fKey);
    }

    public void callValidateItem(String functionIn) {
        OperationBinding ob = ADFUtils.findOperation("callValidateItem");
        ob.getParamsMap().put("FunctionIn", functionIn);
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List returnVals = (List) ob.getResult();
            if (returnVals != null) {
                if (returnVals.size() == 2) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), (String) returnVals.get(0),
                                           (String) returnVals.get(1));
                    this.setErrorOnActiveComp(true);
                } else if (returnVals.size() == 1) {
                    Object obj = returnVals.get(0);
                    if (obj instanceof BigDecimal)
                        this.processVSeqBuiltIns((BigDecimal) returnVals.get(0));
                    else if (obj instanceof String)
                        if (YES.equalsIgnoreCase((String) obj))
                            this.callCheckItemIsFood(functionIn);
                        else
                            this.callValidateItem2(functionIn);
                }
            }
        }
    }

    public void okItemIsFoodActionListener(ActionEvent actionEvent) {
        this.getItemIsFoodPopup().hide();
        this.callValidateItem2(this.getItemIsFoodPopup().getLauncherVar());
    }

    public void callValidateItem2(String functionIn) {
        OperationBinding ob = ADFUtils.findOperation("callValidateItem2");
        ob.getParamsMap().put("FunctionIn", functionIn);
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List returnVals = (List) ob.getResult();
            if (returnVals != null) {
                if (returnVals.size() == 2) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), (String) returnVals.get(0),
                                           (String) returnVals.get(1));
                    this.setErrorOnActiveComp(true);
                } else if (returnVals.size() == 1)
                    this.processVSeqBuiltIns((BigDecimal) returnVals.get(0));
            }
        }
    }

    private void callCheckItemIsFood(String fKey) {
        OperationBinding ob = ADFUtils.findOperation("callCheckItemIsFood");
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List returnVals = (List) ob.getResult();
            if (returnVals != null) {
                if (returnVals.size() == 2)
                    this.showCheckIsFoodPopup((String) returnVals.get(1), fKey);
            } else
                this.callValidateItem2(fKey);
        }
    }

    private void showCheckIsFoodPopup(String msg, String functionIn) {
        this.getDialoItemIsFoodOutput().setValue(msg);
        this.getItemIsFoodPopup().setLauncherVar(functionIn);
        this.getItemIsFoodPopup().show(new RichPopup.PopupHints());
    }

    private boolean isCurrentTaskFlowActive() {
        return this.getMlpChangeItemBean() != null;
    }

    private void processVSeqBuiltIns(BigDecimal vSeqBuiltIns) {
        if (vSeqBuiltIns != null) {
            int vSeqBuilt = vSeqBuiltIns.intValue();
            ADFUtils.setBoundAttributeValue("ConfirmDnbDna", NO);
            if (vSeqBuilt == 2 || vSeqBuilt == 3 || vSeqBuilt == 7 || vSeqBuilt == 8) {
                this.unlockMpl();
                return;
            }
            switch (vSeqBuilt) {
            case 1:
                this.lockUnlockContainer("UNLOCK_EXIT");
                this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                             this.getAllMessagesPanel(), WARN, "NOT_UNT_ID_VERF");
                this.clearMainView();
                this.setFocusOnMasterContainer();
                break;
            case 4:
                this.unlockMpl();
                this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                             this.getAllMessagesPanel(), ERROR, "INV_UP_FLAG");
                break;
            case 5:
                this.unlockMpl();
                this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                             this.getAllMessagesPanel(), ERROR, "INV_DIS_ITEM");
                break;
            case 6:
                this.unlockMpl();
                this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                             this.getAllMessagesPanel(), ERROR, "NOT_UNT_ID_VERF");
                break;
            case 9:
                this.unlockMpl();
                this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                             this.getAllMessagesPanel(), ERROR, "BUYER_INSTR_INC");
                break;
            default:
            }
        }
    }

    private void unlockMpl() {
        this.lockUnlockContainer("UNLOCK_MLP");
        this.clearMainView();
        this.setFocusOnMasterContainer();
    }

    private void clearMainView() {
        ADFUtils.findOperation("clearMainBlock").execute();
        this.getMasterCID().resetValue();
        this.getLocationId().resetValue();
        this.getItemId().resetValue();
        this.getApptNbr().resetValue();
        this.getCurrPoNbr().resetValue();
        this.getNewPoNbr().resetValue();
        this.getNewItemId().resetValue();
        this.refreshContentOfUIComponent(this.getMasterCID());
        this.refreshContentOfUIComponent(this.getLocationId());
        this.refreshContentOfUIComponent(this.getItemId());
        this.refreshContentOfUIComponent(this.getApptNbr());
        this.refreshContentOfUIComponent(this.getCurrPoNbr());
        this.refreshContentOfUIComponent(this.getNewPoNbr());
        this.refreshContentOfUIComponent(this.getNewItemId());
    }

    private List callCheckLock(String checkParam) {
        OperationBinding ob = ADFUtils.findOperation(OPR_CHECK_LOCK);
        ob.getParamsMap().put("checkParam", checkParam);
        return (List) ob.execute();
    }

    private boolean callValidateItemPo() {
        OperationBinding ob = ADFUtils.findOperation("callValidateItemPo");
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    this.setFocusOnActiveComponent();
                    this.setErrorOnActiveComp(true);
                    return false;
                } else {
                    this.hideErrorPanel();
                    this.setErrorOnActiveComp(false);
                    if (CONF.equalsIgnoreCase(errorType)) {
                        this.showConfirmValidateItem(error);
                    }
                }
            } else {
                this.setFocusNewItem();
            }
        }
        return true;
    }

    private void continueProcess(String fKey) {
        if (YES.equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("ConfirmDnbDna"))) {
            List checkResult = this.callCheckLock("FINAL_CHECK");
            String lockCheck = null;
            //String lockingUser = null;
            if (checkResult != null) {
                if (checkResult.size() == 2) {
                   //lockingUser = (String) checkResult.get(0);
                    lockCheck = (String) checkResult.get(1);
                }
            }
            if ("PROCEED".equalsIgnoreCase(lockCheck)) {
                this.callValidateItem(fKey);
            } else {
                this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                             this.getAllMessagesPanel(), ERROR, "NEW_SKU_WIP");
                this.clearMainView();
                this.setFocusOnMasterContainer();
                return;
            }
        }
    }


    public OperationBinding callValidateNewPoNbr(String newPoNbr) {
        OperationBinding ob = ADFUtils.findOperation("callValidateNewPoNbr");
        ob.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID));
        ob.getParamsMap().put("newPoNbr", newPoNbr);
        ob.execute();
        return ob;
    }


    private void showConfirmF5Popup() {
        this.setErrorOnActiveComp(false);
        this.getConfirmPopup().setLauncherVar("F5Confirm");
        this.getDialogConfirmOutputText().setValue(this.getMessage("MLP_CHANGE_ALL", WARN));
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.getConfirmPopup().show(new RichPopup.PopupHints());
    }

    private void showConfirmValidateItem(String msg) {
        this.setErrorOnActiveComp(false);
        this.getConfirmPopup().setLauncherVar("validateItem");
        this.getDialogConfirmOutputText().setValue(msg);
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.getConfirmPopup().show(new RichPopup.PopupHints());
    }

    private void showConfirmNewPoNbr(String msg) {
        this.setErrorOnActiveComp(false);
        this.getConfirmPopup().setLauncherVar("PoNbrConfirm");
        this.getDialogConfirmOutputText().setValue(msg);
        this.getActiveInputText().setDisabled(true);
        this.refreshContentOfUIComponent(this.getActiveInputText());
        this.getConfirmPopup().show(new RichPopup.PopupHints());
    }

    public String trKeyIn(String key) {
        String goBlock = "";
        String masterCid = (String) ADFUtils.getBoundAttributeValue("MasterContainerId");
        if ("F2".equalsIgnoreCase(key)) {
            if (null == this.callDisplayColorSize1())
                if (null == this.callDisplayColorSize2())
                    goBlock = "goPage4";

        } else if ("F3".equalsIgnoreCase(key)) {
            if (!nvl(masterCid, "ZZ").equals("ZZ")) {
                this.lockUnlockContainer(UNLOCK_EXIT);
            }
            goBlock = "backGlobalHome";
        } else if ("F4".equalsIgnoreCase(key)) {
            this.processFKey("F4");

        } else if ("F9".equalsIgnoreCase(key)) {
            this.callSetPage2Header();
            goBlock = "goPage2";
        } else if ("F5".equalsIgnoreCase(key)) {
            this.processFKey("F5");
        }
        return goBlock;
    }

    private void triggerNewPoVcl(String newPoNbr) {
        this.onChangedNewPoNbr(new ValueChangeEvent(this.getNewPoNbr(), null, newPoNbr));
    }

    public void callSetPage2Header() {
        ADFUtils.findOperation("callSetPage2Header").execute();
    }

    public Object nvl(Object arg0, Object arg1) {
        return (arg0 == null) ? arg1 : arg0;
    }

    public String f2Action() {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_MAIN_F2,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
            if (this.validateMasterCid() && this.isValidCid())
                return this.trKeyIn("F2");
        }else{
        
               this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
         this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
        }
         return null;
    }

    public String f3Action() {
        return this.trKeyIn("F3");
    }

    public String f4Action() {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_MAIN_F4,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
            if (this.validateMasterCid())
                if (this.validateNewItem())
                    return this.trKeyIn("F4");
        }else{
        
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
        this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
        }
        
      
        return null;
    }

    public String f5Action() {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_MAIN_F5,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
            if (this.validateMasterCid() && this.isValidCid())
                if (this.validateNewItem())
                    return this.trKeyIn("F5");
        }else{
        
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
        this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
        }
        
       
        return null;
    }


    public String f9Action() {
      String action = null;
      if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_MAIN_F9,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
        if (this.lockUnlockContainer(LOCK)){
          this.triggerNewPoVcl((String) ADFUtils.getBoundAttributeValue("NewPoNbr"));
          if (this.validateMasterCid() && this.isValidCid() && this.isValidNewPo())
            action = this.trKeyIn("F9");
        }
      }
      else{
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
        this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
      }
      return action;
    }


    private boolean validateMasterCid() {
        if (this.isMasterCidEmpty()) {
            this.showPartialEntry();
            this.setFocusOnMasterContainer();
            this.setErrorOnMasterCid(true);
            return false;
        }
        return true;
    }

    private boolean validateNewItem() {
        String focus = this.getMlpChangeItemBean().getIsFocusOn();
        if (this.isNewItemEmpty() && ATTR_NEW_ITEM.equalsIgnoreCase(focus)) {
            this.showPartialEntry();
            this.setFocusNewItem();
            this.setErrorOnNewItem(true);
            return false;
        }
        return true;
    }

    private boolean isMasterCidEmpty() {
        String masterCid = (String) ADFUtils.getBoundAttributeValue("MasterContainerId");
        return (masterCid == null || "".equalsIgnoreCase(masterCid));
    }

    private boolean isNewItemEmpty() {
        String newItem = (String) ADFUtils.getBoundAttributeValue("NewItemId");
        return (newItem == null || "".equalsIgnoreCase(newItem));
    }


    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (this.getMlpChangeItemBean() != null) {
            String focus = this.getMlpChangeItemBean().getIsFocusOn();
            if (ATTR_MASTER_CID.equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getMasterCID());
            } else if (ATTR_NEW_PO.equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getNewPoNbr());
            } else if (ATTR_NEW_ITEM.equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getNewItemId());
            }
        }
        _logger.info("onRegionLoad End");
    }

    private Object callDisplayColorSize1() {
        return ADFUtils.findOperation("callDisplayColorSize1").execute();
    }

    private Object callDisplayColorSize2() {
        return ADFUtils.findOperation("callDisplayColorSize2").execute();
    }

    public boolean callValidateHazmatItem() {
        OperationBinding oper = ADFUtils.findOperation("validateHazmatItem");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List errors = (List) oper.getResult();
            if (errors != null && errors.size() > 1) {
                this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                       this.getAllMessagesPanel(), (String) errors.get(0), (String) errors.get(1));
                this.setErrorOnActiveComp(true);
                return false;
            }
        }
        return true;
    }

    public RichInputText getActiveInputText() {
        switch (this.getMlpChangeItemBean().getIsFocusOn()) {
        case ATTR_MASTER_CID:
            return this.getMasterCID();
        case ATTR_NEW_PO:
            return this.getNewPoNbr();
        case ATTR_NEW_ITEM:
            return this.getNewItemId();
        default:
            return null;
        }
    }

    public RichIcon getActiveIcon() {
        switch (this.getMlpChangeItemBean().getIsFocusOn()) {
        case ATTR_MASTER_CID:
            return this.getMasterCidIcon();
        case ATTR_NEW_PO:
            return this.getNewPOIcon();
        case ATTR_NEW_ITEM:
            return this.getNewItemIcon();
        default:
            return null;
        }
    }


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setMasterCidIcon(RichIcon masterCidIcon) {
        this.masterCidIcon = masterCidIcon;
    }

    public RichIcon getMasterCidIcon() {
        return masterCidIcon;
    }

    public void setNewPOIcon(RichIcon newPOIcon) {
        this.newPOIcon = newPOIcon;
    }

    public RichIcon getNewPOIcon() {
        return newPOIcon;
    }

    public void setNewItemIcon(RichIcon newItemIcon) {
        this.newItemIcon = newItemIcon;
    }

    public RichIcon getNewItemIcon() {
        return newItemIcon;
    }

    public void setMasterCID(RichInputText masterCID) {
        this.masterCID = masterCID;
    }

    public RichInputText getMasterCID() {
        return masterCID;
    }

    public void setNewPoNbr(RichInputText newPoNbr) {
        this.newPoNbr = newPoNbr;
    }

    public RichInputText getNewPoNbr() {
        return newPoNbr;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }


    public void setConfirmDialog(RichDialog confirmDialog) {
        this.confirmDialog = confirmDialog;
    }

    public RichDialog getConfirmDialog() {
        return confirmDialog;
    }

    public void setDialogConfirmOutputText(RichOutputText dialogConfirmOutputText) {
        this.dialogConfirmOutputText = dialogConfirmOutputText;
    }

    public RichOutputText getDialogConfirmOutputText() {
        return dialogConfirmOutputText;
    }

    public void setNewItemId(RichInputText newItemId) {
        this.newItemId = newItemId;
    }

    public RichInputText getNewItemId() {
        return newItemId;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setCurrPoNbr(RichInputText currPoNbr) {
        this.currPoNbr = currPoNbr;
    }

    public RichInputText getCurrPoNbr() {
        return currPoNbr;
    }

    public void setItemId(RichInputText itemId) {
        this.itemId = itemId;
    }

    public RichInputText getItemId() {
        return itemId;
    }

    public void setApptNbr(RichInputText apptNbr) {
        this.apptNbr = apptNbr;
    }

    public RichInputText getApptNbr() {
        return apptNbr;
    }

    public void setDialoItemIsFoodOutput(RichOutputText dialoItemIsFoodOutput) {
        this.dialoItemIsFoodOutput = dialoItemIsFoodOutput;
    }

    public RichOutputText getDialoItemIsFoodOutput() {
        return dialoItemIsFoodOutput;
    }

    public void setItemIsFoodPopup(RichPopup itemIsFoodPopup) {
        this.itemIsFoodPopup = itemIsFoodPopup;
    }

    public RichPopup getItemIsFoodPopup() {
        return itemIsFoodPopup;
    }
}
