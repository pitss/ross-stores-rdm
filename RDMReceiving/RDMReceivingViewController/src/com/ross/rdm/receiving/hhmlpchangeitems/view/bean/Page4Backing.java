package com.ross.rdm.receiving.hhmlpchangeitems.view.bean;

import com.ross.rdm.receiving.view.framework.RDMReceivingBackingBean;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page4.jspx
// ---
// ---------------------------------------------------------------------
public class Page4Backing extends MlpChangeItemBaseBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page4Backing.class);

    public Page4Backing() {

    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public String f3ExitAction() {
        // Add event code here...
        this.getMlpChangeItemBean().setIsFocusOn(ATTR_NEW_PO);
        return "goPage1";
    }
}
