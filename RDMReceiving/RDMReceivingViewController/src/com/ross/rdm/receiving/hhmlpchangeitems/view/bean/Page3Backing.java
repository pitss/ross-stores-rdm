package com.ross.rdm.receiving.hhmlpchangeitems.view.bean;

import com.ross.rdm.receiving.view.framework.RDMReceivingBackingBean;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

// ---------------------------------------------------------------------
// ---    
// ---    Backing Bean for Page3.jspx
// ---    
// ---------------------------------------------------------------------
public class Page3Backing extends RDMReceivingBackingBean {
  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon errorWarnInfoIcon;
  private RichOutputText errorWarnInfoMessage;
  private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class);

  public Page3Backing() {

  }

  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()  {
    return allMessagesPanel;
  }
  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()  {
    return errorWarnInfoIcon;
  }
  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()  {
    return errorWarnInfoMessage;
  }
}
