package com.ross.rdm.receiving.hhmlpchangeitems.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.framework.RDMHotelPickingBackingBean;

import java.util.List;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class MlpChangeItemBaseBean extends RDMHotelPickingBackingBean {
    static final String HH_MLP_CHANGE_ITEM_PAGE_FLOW_BEAN = "HhMlpChangeItemSBean";

    public static final String LOCK = "LOCK";
    public static final String LOCKED = "LOCKED";
    public static final String UNLOCK_EXIT = "UNLOCK_EXIT";
    public static final String UNLOCK_MLP = "UNLOCK_MLP";
    public static final String MLP_IN_USE = "MLP_IN_USE"; 
   
    static final String SCC_ICON = "logo";

    static final String NO = "N";
    static final String YES = "Y";
    static final String NONE = "NONE";
    static final String STR_1 = "1";
    static final String STR_2 = "2";
    static final String STR_3 = "3";
    static final String STR_4 = "4";
    //Error Codes
    static final String FIRST_RECORD = "FIRST RECORD";
    static final String LAST_RECORD = "LAST RECORD";
    static final String CID_IN_RTV = "CID_IN_RTV";

    //Attributes
    static final String ATTR_FACILITY_ID = "FacilityId";
    static final String ATTR_LANGUAGE_CODE = "LanguageCode";
    static final String ATTR_MASTER_CID = "MASTER_CID";
    static final String ATTR_NEW_PO = "NEW_PO";
    static final String ATTR_NEW_ITEM = "NEW_ITEM";
    static final String ATTR_CURRENT_CID = "CurrentContainerId";
    static final String ATTR_CONFIRM_DNB_DNA = "ConfirmDnbDna";
    //Operations
    static final String OPR_CLR_MAIN_BLK = "clearMainBlock";
    static final String OPR_DISPLAY_MLP_INFO = "displayContainerInfo";
    static final String OPR_CHECK_LOCK = "callCheckLock";
    static final String OPR_INSERT_LOCK = "callInsertLock";
    static final String OPR_LOCK = "callLockProcedures";
     boolean flag=false;

    public MlpChangeItemBaseBean() {
        super();
    }

    public void invokeOperation(String operation) {
        OperationBinding ob = ADFUtils.findOperation(operation);
        if (ob != null)
            ob.execute();
    }

    public HhMlpChangeItemSBean getMlpChangeItemBean() {
        return (HhMlpChangeItemSBean) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(HH_MLP_CHANGE_ITEM_PAGE_FLOW_BEAN);
    }

    public void getAndShowMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                        RichPanelGroupLayout allMessagesPanel, String messageType, String msgCode) {
        String msg = this.getMessage(msgCode, messageType);
        this.showMessagesPanel(errorWarnInfoMessage, errorWarnInfoIcon, allMessagesPanel, messageType, msg);
    }

    public String getMessage(String msgCode, String messageType) {
        return this.getMessage(msgCode, messageType, this.getBoundAttribute(ATTR_FACILITY_ID),
                               this.getBoundAttribute(ATTR_LANGUAGE_CODE));
    }

    public void showMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                  RichPanelGroupLayout allMessagesPanel, String messageType, String msg) {
        errorWarnInfoMessage.setValue(null);
        errorWarnInfoIcon.setName(null);
        allMessagesPanel.setVisible(false);

        if (WARN.equals(messageType)) {
            errorWarnInfoIcon.setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            errorWarnInfoIcon.setName(INF_ICON);
        } else {
            if (ERROR.equals(messageType)) {
                errorWarnInfoIcon.setName(ERR_ICON);
            } else {
                if (SUCCESS.equals(messageType)) {
                    errorWarnInfoIcon.setName(SCC_ICON);
                }
            }
        }
        
        errorWarnInfoMessage.setValue(msg);
        allMessagesPanel.setVisible(true);
                                                                              refreshContentOfUIComponent(allMessagesPanel);
        
    }

    public void hideMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                  RichPanelGroupLayout allMessagesPanel) {
        errorWarnInfoMessage.setValue(null);
        errorWarnInfoIcon.setName(null);
        refreshContentOfUIComponent(allMessagesPanel);
    }

    public String getBoundAttribute(String attrName) {
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String) val;
        return null;
    }


    public void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set) {
        if (set) {
            this.addErrorStyleToComponent(uiComponent);
            icon.setName(ERR_ICON);

        } else {
            icon.setName(REQ_ICON);
            this.removeErrorStyleToComponent(uiComponent);
        }
        this.refreshContentOfUIComponent(icon);
        this.refreshContentOfUIComponent(uiComponent);
    }

    public boolean processMessages(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                   RichPanelGroupLayout allMessagesPanel, List<String> errors) {
        boolean hasErrors = false;
        if (errors != null && !errors.isEmpty()) {
            this.showMessagesPanel(errorWarnInfoMessage, errorWarnInfoIcon, allMessagesPanel, errors.get(0),
                                   errors.get(1));
            hasErrors = true;
        } else {
            this.hideMessagesPanel(errorWarnInfoMessage, errorWarnInfoIcon, allMessagesPanel);
        }
        return hasErrors;
    }

    public void setIsFocusOn(String comp) {
        getMlpChangeItemBean().setIsFocusOn(comp);
    }

    public String getIsFocuOn() {
        return getMlpChangeItemBean().getIsFocusOn();
    }
}
