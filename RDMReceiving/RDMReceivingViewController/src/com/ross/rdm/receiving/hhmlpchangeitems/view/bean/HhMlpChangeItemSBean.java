package com.ross.rdm.receiving.hhmlpchangeitems.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhMlpChangeItemSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhMlpChangeItemSBean.class);
    private static final String OPR_ADD_RECORD = "addRecord";
    private String isFocusOn;
    private boolean errorOnSelectedItem;
    private boolean popupShowing;
    private boolean validCid;
    private boolean validNewPo;
    private boolean isNotAllowed = false;


    public void initTaskFlow() {
        OperationBinding ob = ADFUtils.findOperation(OPR_ADD_RECORD);
        ob.execute();
        this.setIsFocusOn("MASTER_CID");
        this.setErrorOnSelectedItem(false);
        this.setValidCid(true);
        this.setValidNewPo(true);
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setErrorOnSelectedItem(boolean duplicatedItem) {
        this.errorOnSelectedItem = duplicatedItem;
    }

    public boolean isErrorOnSelectedItem() {
        return errorOnSelectedItem;
    }

    public void setPopupShowing(boolean popupShowing) {
        this.popupShowing = popupShowing;
    }

    public boolean isPopupShowing() {
        return popupShowing;
    }

    public void setValidCid(boolean validCid) {
        this.validCid = validCid;
    }

    public boolean isValidCid() {
        return validCid;
    }

    public void setValidNewPo(boolean validNewPo) {
        this.validNewPo = validNewPo;
    }

    public boolean isValidNewPo() {
        return validNewPo;
    }

    public void setIsNotAllowed(boolean isNotAllowed) {
        this.isNotAllowed = isNotAllowed;
    }

    public boolean isNotAllowed() {
        return isNotAllowed;
    }

}
