package com.ross.rdm.receiving.hhmlpchangeitems.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.trinidad.event.SelectionEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends MlpChangeItemBaseBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    static final String FIRST_RECORD = "FIRST RECORD";
    static final String LAST_RECORD = "LAST RECORD";
    private RichTable itemsTable;
    

    public Page2Backing() {

    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    private void showFirstLastRecordInfo() {
        DCIteratorBinding ib = ADFUtils.findIterator("MlpChangeItemsPage2ViewIterator");
        int crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long trow = ib.getEstimatedRowCount();
        if (crow == trow - 1 && !this.isErrorOnSelectedItem()) {
            this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                   this.getAllMessagesPanel(), INFO, LAST_RECORD);
        } else if (crow == 0 && !this.isErrorOnSelectedItem()) {
            this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                   this.getAllMessagesPanel(), INFO, FIRST_RECORD);
        } else if (!this.isErrorOnSelectedItem())
            this.hideMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                   this.getAllMessagesPanel());
    }


    public void tableRowSelectionListener(SelectionEvent selectionEvent) {                                                                             
        this.setErrorOnSelectedItem(false);
        JSFUtils.resolveMethodExpression("#{bindings.MlpChangeItemsPage2View.collectionModel.makeCurrent}",
                                         SelectionEvent.class, new Class[] { SelectionEvent.class }, new Object[] {
                                         selectionEvent });
        this.showFirstLastRecordInfo();
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
         if(StringUtils.isEmpty((String) this.errorWarnInfoMessage.getValue())){
                if (!this.getPageFlowBean().isNotAllowed()) {
              this.showFirstLastRecordInfo();
                }
            }
        _logger.info("onRegionLoad End");
    }
    
    protected HhMlpChangeItemSBean getPageFlowBean() {
        return (HhMlpChangeItemSBean) getPageFlowBean("HhMlpChangeItemSBean");
    }

    private boolean isErrorOnSelectedItem() {
        return this.getMlpChangeItemBean().isErrorOnSelectedItem();
    }

    private void setErrorOnSelectedItem(boolean is) {
        this.getMlpChangeItemBean().setErrorOnSelectedItem(is);
    }

    public String f4selectAction() {
        String goBlock = "";
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_PAGE_2_F4,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
            Row currentRow = ADFUtils.findIterator("MlpChangeItemsPage2ViewIterator").getCurrentRow();
           
            if (null != currentRow) {
                String newItem = (String) currentRow.getAttribute("ItemId");
                if (this.callCheckDuplicatedItem(newItem))
                    if (this.callCheckLockedItem(newItem)) {
                        this.getMlpChangeItemBean().setIsFocusOn(ATTR_NEW_ITEM);
                        this.setErrorOnSelectedItem(false);
                        goBlock = "goPage1";
                    }
            }
        }else{
        
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
        this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
        }
        
     
        return goBlock;
    }

    public boolean callCheckLockedItem(String newItem) {
        OperationBinding ob = ADFUtils.findOperation("callcheckLockedItem");
        ob.getParamsMap().put("page2Item", newItem);
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.setErrorOnSelectedItem(true);
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    return false;
                } else {
                    this.hideMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel());
                }
            }
        }
        return true;
    }

    private boolean callCheckDuplicatedItem(String newItem) {
        OperationBinding ob = ADFUtils.findOperation("checkDuplicatedItem");
        ob.getParamsMap().put("item", newItem);
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType)) {
                    this.setErrorOnSelectedItem(true);
                    this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                                 this.getAllMessagesPanel(), errorType, error);
                    return false;
                }
            }
        }
        return true;
    }

    public String f3ExitAction() {
        this.setErrorOnSelectedItem(false);
        this.getMlpChangeItemBean().setIsFocusOn(ATTR_MASTER_CID);
        return "goPage1";
    }

    private void pageUpDownHandler(String upOrDown) {
        DCIteratorBinding ib = ADFUtils.findIterator("MlpChangeItemsPage2ViewIterator");
        long trow = ib.getEstimatedRowCount() - 1;
        long crow = ib.getNavigatableRowIterator().getCurrentRowIndex();
        long nextPosition = 0;
        if ("UP".equalsIgnoreCase(upOrDown) && crow != 0) {
            nextPosition = crow - 4;
            if (nextPosition < 0)
                nextPosition = 0;
        } else if ("DOWN".equalsIgnoreCase(upOrDown) && crow != trow) {
            nextPosition = crow + 4;
            if (nextPosition > trow)
                nextPosition = trow;
        } else
            return;
        if (nextPosition >= 0 && nextPosition <= trow)
            ib.setCurrentRowIndexInRange((int) nextPosition);
        this.refreshContentOfUIComponent(this.getItemsTable());
    }

    public void pageUpActionListener(ActionEvent actionEvent) {
        this.pageUpDownHandler("UP");
    }

    public void pageDownActionListener(ActionEvent actionEvent) {
        this.pageUpDownHandler("DOWN");
    }

    public String f7PageUpAction() {
        
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_PAGE_2_F7,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
            this.pageUpDownHandler("UP");
        }else{
        
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
        this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
        }
        
        
      
        return null;

    }

    public String f8PageUpAction() {
      
        if(this.callCheckScreenOptionPrivilege( RoleBasedAccessConstants.OPTION_NAME_hh_mlp_change_item_s_PAGE_2_F8,RoleBasedAccessConstants.FORM_NAME_hh_mlp_change_item_s)){
            this.pageUpDownHandler("DOWN");
        }else{
        
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),this.getAllMessagesPanel(), ERROR, "NOT_ALLOWED");
        }
        
        
        return null;
    }

    public void setItemsTable(RichTable itemsTable) {
        this.itemsTable = itemsTable;
    }

    public RichTable getItemsTable() {
        return itemsTable;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }
}
