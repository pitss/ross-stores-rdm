package com.ross.rdm.receiving.hhinitiateunloads.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Mar 08 16:51:51 CET 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhInitiateUnloadSAsnWindowViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        UserChoice {
            public Object get(HhInitiateUnloadSAsnWindowViewRowImpl obj) {
                return obj.getUserChoice();
            }

            public void put(HhInitiateUnloadSAsnWindowViewRowImpl obj, Object value) {
                obj.setUserChoice((String) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhInitiateUnloadSAsnWindowViewRowImpl object);

        public abstract void put(HhInitiateUnloadSAsnWindowViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int USERCHOICE = AttributesEnum.UserChoice.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhInitiateUnloadSAsnWindowViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute UserChoice.
     * @return the UserChoice
     */
    public String getUserChoice() {
        return (String) getAttributeInternal(USERCHOICE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UserChoice.
     * @param value value to set the  UserChoice
     */
    public void setUserChoice(String value) {
        setAttributeInternal(USERCHOICE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

