package com.ross.rdm.receiving.hhmlpchangeitems.model.views;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.receiving.hhmlpchangeitems.model.views.common.HhMlpChangeItemSMainView;
import com.ross.rdm.receiving.hhmlpchangeitems.model.views.common.MlpChangeItemsPage2View;
import com.ross.rdm.receiving.model.services.ReceivingAppModuleImpl;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 07 10:26:25 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhMlpChangeItemSMainViewImpl extends RDMTransientViewObjectImpl implements HhMlpChangeItemSMainView {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhMlpChangeItemSMainViewImpl.class);
    private static String UNDEFINED = "UNDEFINED";
    private static String HH_MLP_CHANGE_ITEM = "HH_MLP_CHANGE_ITEM_S";
    private static String DISPLAY_CONTAINER_INFO = "PKG_RECEIVING_MARKING_ADF.DISPLAY_CONTAINER_INFO";
    
    private static final String PO_NBR = "PO_NBR";
    private static final String ITEM_ID = "ITEM_ID";
    private static BigDecimal ZERO = new BigDecimal(0);

    public void addRecord() {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        am.prepareViewObject(this);
        am.prepareViewObject(am.getHhMlpChangeItemSWorkView());
        am.prepareViewObject(am.getGlobalVariablesView());
        this.mlpChangeStartup(am);

    }

    public List callCheckLock(String checkParam) {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        HhMlpChangeItemSWorkViewRowImpl hhMlpChangeItemSWorkViewRow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        SQLOutParam userId = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        List result = new ArrayList<String>();
        String lockCheck =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "multi_user.check_lock",
                                                hhMlpChangeItemSWorkViewRow.getFacilityId(),
                                                hhMlpChangeItemSWorkViewRow.getUser(),
                                                hhMlpChangeItemSWorkViewRow.getScreenName(), checkParam, userId,
                                                "APPT_NBR",
                                                hhMlpChangeItemSMainViewRow.getApptNbr() != null ?
                                                hhMlpChangeItemSMainViewRow.getApptNbr().toString() : EMPTY_STRING, PO_NBR,
                                                hhMlpChangeItemSMainViewRow.getCurPoNbr(), ITEM_ID,
                                                hhMlpChangeItemSMainViewRow.getItemId());
        if (userId != null)
            result.add(userId.getWrappedData());
        result.add(lockCheck);
        return result;
    }

    public void callLockProcedures(String lockProcedureName, String closeLockParam) {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        HhMlpChangeItemSWorkViewRowImpl hhMlpChangeItemSWorkViewRow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        String procedureName = "multi_user." + lockProcedureName;
        if ("insert_lock".equalsIgnoreCase(lockProcedureName))
            DBUtils.callStoredProcedure(this.getDBTransaction(), procedureName,
                                        hhMlpChangeItemSWorkViewRow.getFacilityId(),
                                        hhMlpChangeItemSWorkViewRow.getUser(),
                                        hhMlpChangeItemSWorkViewRow.getScreenName(), "APPT_NBR",
                                        hhMlpChangeItemSMainViewRow.getApptNbr() != null ?
                                        hhMlpChangeItemSMainViewRow.getApptNbr().toString() : EMPTY_STRING, PO_NBR,
                                        hhMlpChangeItemSMainViewRow.getCurPoNbr(), ITEM_ID,
                                        hhMlpChangeItemSMainViewRow.getItemId());
        else if ("close_lock".equalsIgnoreCase(lockProcedureName))
            DBUtils.callStoredProcedure(this.getDBTransaction(), procedureName,
                                        hhMlpChangeItemSWorkViewRow.getFacilityId(),
                                        hhMlpChangeItemSWorkViewRow.getUser(),
                                        hhMlpChangeItemSWorkViewRow.getScreenName(), closeLockParam,
                                        hhMlpChangeItemSMainViewRow.getApptNbr() != null ?
                                        hhMlpChangeItemSMainViewRow.getApptNbr().toString() : EMPTY_STRING, PO_NBR,
                                        hhMlpChangeItemSMainViewRow.getCurPoNbr(), ITEM_ID,
                                        hhMlpChangeItemSMainViewRow.getItemId());
        ((ReceivingAppModuleImpl) this.getApplicationModule()).callDoCommit();

    }

    public List callValidateNewPoNbr(String facilityId, String newPoNbr) {
        SQLOutParam p_main_confirm_dnb_dna = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_status_check = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_msg = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_msg_type = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        p_main_confirm_dnb_dna.setWrappedData(hhMlpChangeItemSMainViewRow.getConfirmDnbDna() == null ? EMPTY_STRING :
                                              hhMlpChangeItemSMainViewRow.getConfirmDnbDna());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.validate_new_po_nbr",
                                    facilityId, newPoNbr, p_main_confirm_dnb_dna, p_status_check, p_msg, p_msg_type);
        List errors = null;
        if (null != p_status_check.getWrappedData() && !p_status_check.getWrappedData().toString().isEmpty()) {
            errors = new ArrayList<String>();
            String status = (String) p_status_check.getWrappedData();
            hhMlpChangeItemSMainViewRow.setConfirmDnbDna((String) p_main_confirm_dnb_dna.getWrappedData());
            if ("ERROR".equalsIgnoreCase(status)) {
                errors.add(p_msg_type.getWrappedData());
                errors.add(p_msg.getWrappedData());
            } else if ("USER_CONFIRM".equalsIgnoreCase(status)) {
                errors.add("C");
                errors.add(p_msg.getWrappedData());
            }
        }
        return errors;
    }

    public List validateHazmatItem() {
        SQLOutParam p_msg = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_msg_type = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.VALIDATE_HAZMAT_ITEM", p_msg,
                                    p_msg_type, hhMlpChangeItemSMainViewRow.getItemId(),
                                    hhMlpChangeItemSMainViewRow.getNewItemId(), wrow.getFacilityId());
        return this.processMessagesParams(p_msg, p_msg_type);
    }

    public List callValidateItemPo() {
        SQLOutParam p_main_confirm_dnb_dna = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_status_check = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_msg = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_msg_type = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        p_main_confirm_dnb_dna.setWrappedData(hhMlpChangeItemSMainViewRow.getConfirmDnbDna() == null ? EMPTY_STRING :
                                              hhMlpChangeItemSMainViewRow.getConfirmDnbDna());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.validate_item_po",
                                    wrow.getFacilityId(), hhMlpChangeItemSMainViewRow.getNewPoNbr(),
                                    hhMlpChangeItemSMainViewRow.getCurPoNbr(), hhMlpChangeItemSMainViewRow.getItemId(),
                                    hhMlpChangeItemSMainViewRow.getNewItemId(), p_main_confirm_dnb_dna, p_status_check,
                                    p_msg, p_msg_type);
        List errors = null;
        hhMlpChangeItemSMainViewRow.setConfirmDnbDna((String) p_main_confirm_dnb_dna.getWrappedData());
        if (null != p_status_check.getWrappedData() && !p_status_check.getWrappedData().toString().isEmpty()) {
            errors = new ArrayList<String>();
            String status = (String) p_status_check.getWrappedData();
            if ("ERROR".equalsIgnoreCase(status)) {
                errors.add(p_msg_type.getWrappedData());
                errors.add(p_msg.getWrappedData());
            } else if ("USER_CONFIRM".equalsIgnoreCase(status)) {
                errors.add("C");
                errors.add(p_msg.getWrappedData());
            }
        }
        return errors;
    }


    public List callValidateItem(String FunctionIn) {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        List listElements = null;
        SQLOutParam V_RETURN = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_MSG_DISPLAY = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam P_CHECK_FOOD = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_APPT_NBR =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getApptNbr() == null ? new BigDecimal(0) :
                            hhMlpChangeItemSMainViewRow.getApptNbr(), Types.NUMERIC);
        SQLOutParam I_CURRENT_CONTAINER_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getCurrentContainerId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getCurrentContainerId(), Types.VARCHAR);
        SQLOutParam I_CUR_PO_NBR =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getCurPoNbr() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getCurPoNbr(), Types.VARCHAR);
        SQLOutParam I_ITEM_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getItemId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getItemId(), Types.VARCHAR);
        SQLOutParam I_LOCATION_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getLocationId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getLocationId(), Types.VARCHAR);
        SQLOutParam I_MASTER_CONTAINER_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getMasterContainerId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getMasterContainerId(), Types.VARCHAR);
        SQLOutParam I_NEW_ITEM_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getNewItemId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getNewItemId(), Types.VARCHAR);
        SQLOutParam I_NEW_PO_NBR =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getNewPoNbr() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getNewPoNbr(), Types.VARCHAR);
        SQLOutParam I_UNIT_QTY =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getUnitQty() == null ? new BigDecimal(0) :
                            hhMlpChangeItemSMainViewRow.getUnitQty(), Types.NUMERIC);
        SQLOutParam I_CASE_PICK = new SQLOutParam(wrow.getCasePick() == null ? EMPTY_STRING : wrow.getCasePick(), Types.VARCHAR);
        SQLOutParam I_DISPLAY_MESSAGE_FLAG =
            new SQLOutParam(wrow.getDisplayMessageFlag() == null ? EMPTY_STRING : wrow.getDisplayMessageFlag(), Types.VARCHAR);
        SQLOutParam I_RETURN_PEND_OUT =
            new SQLOutParam(wrow.getReturnPendOut() == null ? EMPTY_STRING : wrow.getReturnPendOut(), Types.VARCHAR);
        SQLOutParam I_RETURN_TICK_OUT =
            new SQLOutParam(wrow.getReturnTickOut() == null ? EMPTY_STRING : wrow.getReturnTickOut(), Types.VARCHAR);
        SQLOutParam I_SHIPPING_CONV =
            new SQLOutParam(wrow.getShippingConv() == null ? EMPTY_STRING : wrow.getShippingConv(), Types.VARCHAR);
        SQLOutParam I_USER = new SQLOutParam(wrow.getUser() == null ? EMPTY_STRING : wrow.getUser(), Types.VARCHAR);
        SQLOutParam P_SEQ_BUILT_INS = new SQLOutParam(0, Types.NUMERIC);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.Validate_Item", V_RETURN,
                                    I_MSG_DISPLAY, I_APPT_NBR, I_CURRENT_CONTAINER_ID, I_CUR_PO_NBR, I_ITEM_ID,
                                    I_LOCATION_ID, I_MASTER_CONTAINER_ID, I_NEW_ITEM_ID, I_NEW_PO_NBR, I_UNIT_QTY,
                                    I_CASE_PICK, I_DISPLAY_MESSAGE_FLAG, wrow.getFacilityId(), I_RETURN_PEND_OUT,
                                    I_RETURN_TICK_OUT, I_SHIPPING_CONV, I_USER, FunctionIn, P_SEQ_BUILT_INS,
                                    P_CHECK_FOOD);
        listElements = this.processMessagesParams(V_RETURN, I_MSG_DISPLAY);
        if (listElements == null) {
            listElements = new ArrayList<Object>();
            String pCheckFood = (String) P_CHECK_FOOD.getWrappedData();
            if (pCheckFood != null && "Y".equalsIgnoreCase(pCheckFood)) {
                listElements.add(pCheckFood);
            } else {
                BigDecimal seqBuiltIns = (BigDecimal) P_SEQ_BUILT_INS.getWrappedData();
                if (seqBuiltIns != null)
                    listElements.add(seqBuiltIns);
            }
        }
        wrow.setDisplayMessageFlag((String) I_DISPLAY_MESSAGE_FLAG.getWrappedData());
        wrow.setShippingConv((String) I_SHIPPING_CONV.getWrappedData());
        wrow.setReturnPendOut((String) I_RETURN_PEND_OUT.getWrappedData());
        wrow.setReturnTickOut((String) I_RETURN_TICK_OUT.getWrappedData());
        hhMlpChangeItemSMainViewRow.setUnitQty((BigDecimal) I_UNIT_QTY.getWrappedData());
        hhMlpChangeItemSMainViewRow.setItemId((String) I_ITEM_ID.getWrappedData());
        return listElements;
    }

    public List callValidateItem2(String FunctionIn) {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        SQLOutParam V_RETURN = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_MSG_DISPLAY = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_APPT_NBR =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getApptNbr() == null ? new BigDecimal(0) :
                            hhMlpChangeItemSMainViewRow.getApptNbr(), Types.NUMERIC);
        SQLOutParam I_CUR_PO_NBR =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getCurPoNbr() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getCurPoNbr(), Types.VARCHAR);
        SQLOutParam I_ITEM_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getItemId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getItemId(), Types.VARCHAR);
        SQLOutParam I_LOCATION_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getLocationId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getLocationId(), Types.VARCHAR);
        SQLOutParam I_MASTER_CONTAINER_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getMasterContainerId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getMasterContainerId(), Types.VARCHAR);
        SQLOutParam I_NEW_ITEM_ID =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getNewItemId() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getNewItemId(), Types.VARCHAR);
        SQLOutParam I_NEW_PO_NBR =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getNewPoNbr() == null ? EMPTY_STRING :
                            hhMlpChangeItemSMainViewRow.getNewPoNbr(), Types.VARCHAR);
        SQLOutParam I_UNIT_QTY =
            new SQLOutParam(hhMlpChangeItemSMainViewRow.getUnitQty() == null ? new BigDecimal(0) :
                            hhMlpChangeItemSMainViewRow.getUnitQty(), Types.NUMERIC);
        SQLOutParam I_CASE_PICK = new SQLOutParam(wrow.getCasePick() == null ? EMPTY_STRING : wrow.getCasePick(), Types.VARCHAR);
        SQLOutParam I_DISPLAY_MESSAGE_FLAG =
            new SQLOutParam(wrow.getDisplayMessageFlag() == null ? EMPTY_STRING : wrow.getDisplayMessageFlag(), Types.VARCHAR);
        SQLOutParam I_RETURN_PEND_OUT =
            new SQLOutParam(wrow.getReturnPendOut() == null ? EMPTY_STRING : wrow.getReturnPendOut(), Types.VARCHAR);
        SQLOutParam I_RETURN_TICK_OUT =
            new SQLOutParam(wrow.getReturnTickOut() == null ? EMPTY_STRING : wrow.getReturnTickOut(), Types.VARCHAR);
        SQLOutParam I_SHIPPING_CONV =
            new SQLOutParam(wrow.getShippingConv() == null ? EMPTY_STRING : wrow.getShippingConv(), Types.VARCHAR);
        SQLOutParam I_USER = new SQLOutParam(wrow.getUser() == null ? EMPTY_STRING : wrow.getUser(), Types.VARCHAR);
        SQLOutParam P_SEQ_BUILT_INS = new SQLOutParam(0, Types.NUMERIC);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.Validate_Item2", V_RETURN,
                                    I_MSG_DISPLAY, I_APPT_NBR, I_CUR_PO_NBR, I_ITEM_ID, I_LOCATION_ID,
                                    I_MASTER_CONTAINER_ID, I_NEW_ITEM_ID, I_NEW_PO_NBR, I_UNIT_QTY, I_CASE_PICK,
                                    I_DISPLAY_MESSAGE_FLAG, wrow.getFacilityId(), I_RETURN_PEND_OUT, I_RETURN_TICK_OUT,
                                    I_SHIPPING_CONV, I_USER, FunctionIn, P_SEQ_BUILT_INS);
        List listElements = this.processMessagesParams(V_RETURN, I_MSG_DISPLAY);
        if (listElements == null) {
            listElements = new ArrayList<Object>();
        }
        listElements.add((BigDecimal) P_SEQ_BUILT_INS.getWrappedData());
        wrow.setDisplayMessageFlag((String) I_DISPLAY_MESSAGE_FLAG.getWrappedData());
        wrow.setShippingConv((String) I_SHIPPING_CONV.getWrappedData());
        wrow.setReturnPendOut((String) I_RETURN_PEND_OUT.getWrappedData());
        wrow.setReturnTickOut((String) I_RETURN_TICK_OUT.getWrappedData());
        hhMlpChangeItemSMainViewRow.setUnitQty((BigDecimal) I_UNIT_QTY.getWrappedData());
        hhMlpChangeItemSMainViewRow.setItemId((String) I_ITEM_ID.getWrappedData());
        return listElements;
    }

    public List callCheckItemIsFood() {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        SQLOutParam V_RETURN = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_MSG_DISPLAY = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.CHECK_ITEM_IS_FOOD", V_RETURN,
                                    I_MSG_DISPLAY, wrow.getFacilityId(), hhMlpChangeItemSMainViewRow.getItemId(),
                                    hhMlpChangeItemSMainViewRow.getNewItemId());
        return this.processMessagesParams(V_RETURN, I_MSG_DISPLAY);
    }

    public void callSetPage2Header() {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        MlpChangeItemsPage2ViewRowImpl mlpChangeItemsPage2Row =
            (MlpChangeItemsPage2ViewRowImpl) am.getMlpChangeItemsPage2View().getCurrentRow();
        MlpChangeItemsPage2View mlpChangeItemView =
            (MlpChangeItemsPage2View) am.getMlpChangeItemsPage2View();
        SQLOutParam p_header = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.display_ChangeItem_Page2Header",
                                    wrow.getFacilityId(), wrow.getLanguageCode(), p_header);
        if (null == mlpChangeItemsPage2Row) {
            mlpChangeItemsPage2Row =
                (MlpChangeItemsPage2ViewRowImpl) am.getMlpChangeItemsPage2View().createRow();
            mlpChangeItemView.insertRow(mlpChangeItemsPage2Row);
        }
        if (p_header != null)
            wrow.setHeaderPage2((String) p_header.getWrappedData());
    }

    public String callDisplayColorSize1() {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        SQLOutParam p_msg = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam header = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.DISPLAY_COLOR_SIZE_1", p_msg,
                                    header, wrow.getFacilityId(), wrow.getLanguageCode());
        String msg = (String) p_msg.getWrappedData();
        if (null == msg || msg.isEmpty()) {
            HhMlpChangeItemSPage4ViewImpl page4View = am.getHhMlpChangeItemSPage4View();
            HhMlpChangeItemSPage4ViewRowImpl page4CurrentRow =
                (HhMlpChangeItemSPage4ViewRowImpl) page4View.getCurrentRow();
            if (null == page4CurrentRow) {
                page4CurrentRow = (HhMlpChangeItemSPage4ViewRowImpl) page4View.createRow();
                page4View.insertRow(page4CurrentRow);
            }
            page4CurrentRow.setheader((String) header.getWrappedData());
        }
        return msg;
    }

    public String callDisplayColorSize2() {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        HhMlpChangeItemSMainViewRowImpl hhMlpChangeItemSMainViewRow =
            (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
        SQLOutParam p_msg = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam header = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_color = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam p_size = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.DISPLAY_COLOR_SIZE_2", p_msg,
                                    header, hhMlpChangeItemSMainViewRow.getItemId(), p_color, p_size,
                                    wrow.getFacilityId());
        String msg = (String) p_msg.getWrappedData();
        if (null == msg || msg.isEmpty()) {
            HhMlpChangeItemSPage4ViewImpl page4View = am.getHhMlpChangeItemSPage4View();
            HhMlpChangeItemSPage4ViewRowImpl page4CurrentRow =
                (HhMlpChangeItemSPage4ViewRowImpl) page4View.getCurrentRow();
            if (null == page4CurrentRow) {
                page4CurrentRow = (HhMlpChangeItemSPage4ViewRowImpl) page4View.createRow();
                page4View.insertRow(page4CurrentRow);
            }
            page4CurrentRow.setColor((String) p_color.getWrappedData());
            page4CurrentRow.setSize1((String) p_size.getWrappedData());
        }
        return msg;
    }

    public void clearMainBlock() {
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) getRootApplicationModule();
        am.prepareViewObject(this);
    }

    public void mlpChangeStartup(ReceivingAppModuleImpl am) {
        am.getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewRowImpl grow = (GlobalVariablesViewRowImpl) am.getGlobalVariablesView().getCurrentRow();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        Row loginRow = am.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) loginRow.getAttribute("FacilityId");
        String userId = (String) loginRow.getAttribute("UserId");
        String userLanguage = (String) loginRow.getAttribute("LanguageCode");
        Integer userPrivilege = (Integer) loginRow.getAttribute("UserPrivilege");
        if (grow.getGlobalHeader() == null)
            grow.setGlobalHeader(UNDEFINED);
        if (grow.getGlobalFacilityId() == null)
            grow.setGlobalFacilityId(facilityId);
        if (grow.getGlobalDevice() == null)
            grow.setGlobalDevice("S");
        if (grow.getGlobalLanguageCode() == null)
            grow.setGlobalLanguageCode(userLanguage);
        if (grow.getGlobalUserId() == null)
            grow.setGlobalUserId(userId);
        if (grow.getGlobalUserPrivilege() == null)
            grow.setGlobalUserPrivilege(userPrivilege.toString());
        wrow.setUserPrivilege(new BigDecimal(grow.getGlobalUserPrivilege()));
        wrow.setUser(grow.getGlobalUserId());
        wrow.setHeader(grow.getGlobalHeader());
        wrow.setFacilityId(grow.getGlobalFacilityId());
        wrow.setDevice(grow.getGlobalDevice());
        wrow.setCallingBlock("MAIN");
        wrow.setUserPrivilege(new BigDecimal(grow.getGlobalUserPrivilege()));
        wrow.setLanguageCode(grow.getGlobalLanguageCode());
        if (UNDEFINED.equals(wrow.getHeader()))
            wrow.setHeader(am.callDisplayHeader(wrow.getFacilityId(), wrow.getLanguageCode(), HH_MLP_CHANGE_ITEM));
        grow.setGlobalMsgCode(EMPTY_STRING);
        grow.setGlobalResponse(EMPTY_STRING);
        grow.setGlobalHeader(null);
        wrow.setMainBlock("MAIN");
        wrow.setVersionNumber("%I%");
        wrow.setScreenName(HH_MLP_CHANGE_ITEM);
        am.callSetAhlInfo(wrow.getFacilityId(), HH_MLP_CHANGE_ITEM);
    }

    public Map displayContainerInfo(String facilityId, String masterCid) {

        SQLOutParam V_RETURN = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_MSG_DISPLAY = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_APPT_NBR = new SQLOutParam(ZERO, Types.NUMERIC);
        SQLOutParam I_CURRENT_CONTAINER_ID = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_CUR_PO_NBR = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_ITEM_ID = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_LOCATION_ID = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam I_MASTER_CONTAINER_ID = new SQLOutParam(masterCid, Types.VARCHAR);
        SQLOutParam I_UNIT_QTY = new SQLOutParam(ZERO, Types.NUMERIC);
        SQLOutParam P_FOUND = new SQLOutParam(ZERO, Types.NUMERIC);
        ReceivingAppModuleImpl am = (ReceivingAppModuleImpl) this.getRootApplicationModule();
        HhMlpChangeItemSWorkViewRowImpl wrow =
            (HhMlpChangeItemSWorkViewRowImpl) am.getHhMlpChangeItemSWorkView().getCurrentRow();
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", wrow.getFacilityId(),
                                    wrow.getLanguageCode());
        DBUtils.callStoredProcedure(this.getDBTransaction(), DISPLAY_CONTAINER_INFO, V_RETURN, I_MSG_DISPLAY,
                                    I_APPT_NBR, I_CURRENT_CONTAINER_ID, I_CUR_PO_NBR, I_ITEM_ID, I_LOCATION_ID,
                                    I_MASTER_CONTAINER_ID, I_UNIT_QTY, facilityId, P_FOUND);
        List<String> errors = null;
        errors = processMessagesParams(V_RETURN, I_MSG_DISPLAY);
        Map returnValues = new HashMap();
        returnValues.put("Errors", errors);
        if (P_FOUND.getWrappedData() != null) {
            BigDecimal found = (BigDecimal) P_FOUND.getWrappedData();
            if (ZERO.equals(found))
                returnValues.put("pFound", false);
            else
                returnValues.put("pFound", true);
        }
        if (errors == null || errors.size() == 0) {
            HhMlpChangeItemSMainViewRowImpl row = (HhMlpChangeItemSMainViewRowImpl) this.getCurrentRow();
            if (I_APPT_NBR.getWrappedData() != null)
                row.setApptNbr((BigDecimal) I_APPT_NBR.getWrappedData());

            if (I_CURRENT_CONTAINER_ID.getWrappedData() != null)
                row.setCurrentContainerId((String) I_CURRENT_CONTAINER_ID.getWrappedData());

            if (I_CUR_PO_NBR.getWrappedData() != null)
                row.setCurPoNbr((String) I_CUR_PO_NBR.getWrappedData());

            if (I_ITEM_ID.getWrappedData() != null)
                row.setItemId((String) I_ITEM_ID.getWrappedData());

            if (I_LOCATION_ID.getWrappedData() != null)
                row.setLocationId((String) I_LOCATION_ID.getWrappedData());

            if (I_MASTER_CONTAINER_ID.getWrappedData() != null)
                row.setMasterContainerId((String) I_MASTER_CONTAINER_ID.getWrappedData());

            if (I_UNIT_QTY.getWrappedData() != null)
                row.setUnitQty((BigDecimal) I_UNIT_QTY.getWrappedData());
        }
        return returnValues;
    }

    /**
     * This is the default constructor (do not remove).
     */
    public HhMlpChangeItemSMainViewImpl() {
    }
 
}
