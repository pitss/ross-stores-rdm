package com.ross.rdm.receiving.hhfreightbillverifys.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.receiving.hhfreightbillverifys.model.views.common.HhFreightBillVerifySWorkView;
import com.ross.rdm.receiving.model.services.ReceivingAppModuleImpl;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 07 10:25:41 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhFreightBillVerifySWorkViewImpl extends RDMTransientViewObjectImpl implements HhFreightBillVerifySWorkView {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhFreightBillVerifySWorkViewImpl.class);


    /**
     * This is the default constructor (do not remove).
     */
    public HhFreightBillVerifySWorkViewImpl() {
    }


    private String nvl(String input) {
        return input != null ? input.toUpperCase() : EMPTY_STRING;
    }

    private String reverseNvl(String input) {
        return StringUtils.isEmpty(input) ? null : input.toUpperCase();
    }

    private ReceivingAppModuleImpl getReceivingAM() {
        return (ReceivingAppModuleImpl) this.getApplicationModule();
    }

    private String getMessageText(String code) {
        HhFreightBillVerifySWorkViewRowImpl workRow = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
        return getReceivingAM().getUserMessagesByCode(code, workRow.getFacilityId(), workRow.getLanguageCode());
    }
 
    public List vFreightBill() {
        List errorMessages = null;
        HhFreightBillVerifySFreightBillViewRowImpl freightBill =
            (HhFreightBillVerifySFreightBillViewRowImpl) getReceivingAM().getHhFreightBillVerifySFreightBillView().getCurrentRow();
        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();

        getReceivingAM().getGetFreightBillView().setbind_facility_id(work.getFacilityId());
        getReceivingAM().getGetFreightBillView().setbind_freight_bill_seq_nbr(freightBill.getFreightBillSeqNbr());
        getReceivingAM().getGetFreightBillView().executeQuery();
        GetFreightBillViewRowImpl first = (GetFreightBillViewRowImpl) getReceivingAM().getGetFreightBillView().first();
        if (first == null) {
            errorMessages = new ArrayList();
            errorMessages.add(ERROR);
            errorMessages.add(getMessageText("INV_FB_NBR"));
        } else if ("R".equals(first.getStatus())) {
            freightBill.refresh(Row.REFRESH_UNDO_CHANGES); //CLEAR_BLOCK
            errorMessages = new ArrayList();
            errorMessages.add(ERROR);
            errorMessages.add(getMessageText("INV_FRT_BIL_ST"));
        } else {
            freightBill.setApptNbr(convertBigDecimalToString(first.getApptNbr()));
            freightBill.setPoNbr(first.getPoNbr());
            freightBill.setStatus(first.getStatus());
            freightBill.setLocationId(first.getLocationId());
            freightBill.setActualContainers(convertBigDecimalToString(first.getActualContainers()));
            freightBill.setActualGohUnits(convertBigDecimalToString(first.getActualGohUnits()));
            freightBill.setDamagedContainers(convertBigDecimalToString(first.getDamagedContainers()));
            freightBill.setDamagedGohUnits(convertBigDecimalToString(first.getDamagedGohUnits()));
            freightBill.setMisrouteQty(convertBigDecimalToString(first.getMisrouteQty()));
            freightBill.setTroubleCode(first.getTroubleCode());
            freightBill.setFreightBillNumber(first.getFreightBillNumber());
            if (StringUtils.isNotEmpty(first.getPoNbr())) {
                errorMessages = new ArrayList();
                errorMessages.add("LOCATION_ID"); //NEXT_ITEM
            }
        }
        return errorMessages;
    }

    public List callPkgReceivingMarkingAdfVLocationId(String locationId) {
        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
        SQLOutParam pVReturn = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pMsgDisplay = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", work.getFacilityId(),
                                    work.getLanguageCode());

        BigDecimal result =
            (BigDecimal) DBUtils.callStoredFunction(this.getDBTransaction(), Types.NUMERIC,
                                                    "PKG_RECEIVING_MARKING_ADF.V_LOCATION_ID", pVReturn, pMsgDisplay,
                                                    work.getFacilityId(), locationId);

        List errorsMessages = this.processMessagesParams(pVReturn, pMsgDisplay);
        return errorsMessages;
    }

    public List callPkgReceivingMarkingAdfVTroubleCode() {
        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
        HhFreightBillVerifySFreightBillViewRowImpl freightBill =
            (HhFreightBillVerifySFreightBillViewRowImpl) getReceivingAM().getHhFreightBillVerifySFreightBillView().getCurrentRow();

        SQLOutParam pVReturn = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pMsgDisplay = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pTroubleCode = new SQLOutParam(nvl(freightBill.getTroubleCode()), Types.VARCHAR);
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", work.getFacilityId(),
                                    work.getLanguageCode());

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.V_TROUBLE_CODE", pVReturn,
                                    pMsgDisplay, pTroubleCode, work.getFacilityId());
        freightBill.setTroubleCode(reverseNvl((String) pTroubleCode.getWrappedData()));

        List errorsMessages = this.processMessagesParams(pVReturn, pMsgDisplay);
        return errorsMessages;
    }

    public List callPkgReceivingMarkingAdfProcessFreightBill1() {
        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
        HhFreightBillVerifySFreightBillViewRowImpl freightBill =
            (HhFreightBillVerifySFreightBillViewRowImpl) getReceivingAM().getHhFreightBillVerifySFreightBillView().getCurrentRow();
        work.setStatus(null);
        SQLOutParam pVReturn = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pMsgDisplay = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pStatus = new SQLOutParam(nvl(work.getStatus()), Types.VARCHAR);
        SQLOutParam pCallMsgWindow = new SQLOutParam("N", Types.VARCHAR);
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", work.getFacilityId(),
                                    work.getLanguageCode());
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.PROCESS_FREIGHT_BILL_1",
                                    pVReturn, pMsgDisplay, convertStringToBigDecimal(freightBill.getActualContainers()),
                                    convertStringToBigDecimal(freightBill.getActualGohUnits()),
                                    convertStringToBigDecimal(freightBill.getDamagedContainers()),
                                    convertStringToBigDecimal(freightBill.getDamagedGohUnits()),
                                    freightBill.getFreightBillNumber(), freightBill.getFreightBillSeqNbr(),
                                    freightBill.getLocationId(),
                                    convertStringToBigDecimal(freightBill.getMisrouteQty()), freightBill.getPoNbr(),
                                    freightBill.getTroubleCode(), work.getFacilityId(), work.getUser(), pStatus,
                                    pCallMsgWindow);
        List errorsMessages = this.processMessagesParams(pVReturn, pMsgDisplay);

        if (errorsMessages == null) {
            callPkgReceivingMarkingAdfProcessFreightBill2((String) pStatus.getWrappedData());
        } else if ("Y".equals(pCallMsgWindow.getWrappedData())) {
            errorsMessages.add("C"); //Third item, show confirm popup
            work.setStatus(reverseNvl((String) pStatus.getWrappedData()));//To continue after popup is accepted, OTM3086
        }
        return errorsMessages;
    }

    public void callPkgReceivingMarkingAdfProcessFreightBill2(String status) {
        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
        HhFreightBillVerifySFreightBillViewRowImpl freightBill =
            (HhFreightBillVerifySFreightBillViewRowImpl) getReceivingAM().getHhFreightBillVerifySFreightBillView().getCurrentRow();

        SQLOutParam pVReturn = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pStatus = new SQLOutParam(status, Types.VARCHAR);
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", work.getFacilityId(),
                                    work.getLanguageCode());
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.PROCESS_FREIGHT_BILL_2",
                                    pVReturn, convertStringToBigDecimal(freightBill.getActualContainers()),
                                    convertStringToBigDecimal(freightBill.getActualGohUnits()),
                                    convertStringToBigDecimal(freightBill.getDamagedContainers()),
                                    convertStringToBigDecimal(freightBill.getDamagedGohUnits()),
                                    freightBill.getFreightBillNumber(), freightBill.getFreightBillSeqNbr(),
                                    freightBill.getLocationId(),
                                    convertStringToBigDecimal(freightBill.getMisrouteQty()), freightBill.getPoNbr(),
                                    freightBill.getTroubleCode(), work.getFacilityId(), work.getUser(), pStatus);
    }

    public void copyFreightBill() {
        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
        HhFreightBillVerifySFreightBillViewRowImpl freightBill =
            (HhFreightBillVerifySFreightBillViewRowImpl) getReceivingAM().getHhFreightBillVerifySFreightBillView().getCurrentRow();
        HhFreightBillVerifySSplitFbsViewRowImpl splitFbs =
            (HhFreightBillVerifySSplitFbsViewRowImpl) getReceivingAM().getHhFreightBillVerifySSplitFbsView().getCurrentRow();
        HhFreightBillVerifySWorkLocalViewRowImpl workLocal =
            (HhFreightBillVerifySWorkLocalViewRowImpl) getReceivingAM().getHhFreightBillVerifySWorkLocalView().getCurrentRow();

        SQLOutParam pVReturn = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", work.getFacilityId(),
                                    work.getLanguageCode());
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.COPY_FREIGHT_BILL", pVReturn,
                                    convertStringToBigDecimal(freightBill.getActualContainers()), splitFbs.getLocationId(), splitFbs.getNewFbs(),
                                    splitFbs.getOldFbs(), work.getFacilityId(), work.getUser(),
                                    workLocal.getUpdateFlag());
        
        String newFbs = splitFbs.getNewFbs();
        splitFbs.refresh(Row.REFRESH_UNDO_CHANGES);
        freightBill.refresh(Row.REFRESH_UNDO_CHANGES);
        freightBill.setFreightBillSeqNbr(newFbs);
        vFreightBill();
        workLocal.setUpdateFlag("N");
    }

    public List tWhenValidateItem2() {

        HhFreightBillVerifySWorkViewRowImpl work = (HhFreightBillVerifySWorkViewRowImpl) this.getCurrentRow();
      
        HhFreightBillVerifySSplitFbsViewRowImpl splitFbs =
            (HhFreightBillVerifySSplitFbsViewRowImpl) getReceivingAM().getHhFreightBillVerifySSplitFbsView().getCurrentRow();
        HhFreightBillVerifySWorkLocalViewRowImpl workLocal =
            (HhFreightBillVerifySWorkLocalViewRowImpl) getReceivingAM().getHhFreightBillVerifySWorkLocalView().getCurrentRow();

        SQLOutParam pVReturn = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pMsgDisplay = new SQLOutParam(EMPTY_STRING, Types.VARCHAR);
        SQLOutParam pUpdateFlag = new SQLOutParam(nvl(workLocal.getUpdateFlag()), Types.VARCHAR);
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", work.getFacilityId(),
                                    work.getLanguageCode());
        
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_RECEIVING_MARKING_ADF.T_WHEN_VALIDATE_ITEM2",
                                    pVReturn, pMsgDisplay, splitFbs.getNewFbs(), splitFbs.getOldFbs(),
                                    work.getFacilityId(), pUpdateFlag);
        workLocal.setUpdateFlag(reverseNvl((String) pUpdateFlag.getWrappedData()));

        List errorsMessages = this.processMessagesParams(pVReturn, pMsgDisplay);
        return errorsMessages;
    }

    private BigDecimal convertStringToBigDecimal(String value) {
        if(NumberUtils.isNumber(value)){
        return value != null ? new BigDecimal(value) : null;
        }else{
            return null;
        }
    }

    private String convertBigDecimalToString(BigDecimal value) {
        return value != null ? value.toString() : null;
    }
}
