package com.ross.rdm.receiving.hhfreightbillverifys.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;
import com.ross.rdm.receiving.hhfreightbillverifys.model.views.common.HhFreightBillVerifySFreightBillView;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 07 10:25:39 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhFreightBillVerifySFreightBillViewImpl extends RDMTransientViewObjectImpl implements HhFreightBillVerifySFreightBillView {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhFreightBillVerifySFreightBillViewImpl.class);


    public void clearFreightBillBlock() {
        this.getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES); //CLEAR_BLOCK
    }
}
