
package com.ross.rdm.shipping.hhshiptrailers.model.oradata;

import com.ross.rdm.common.utils.model.util.dbcall.customTypes.RDMDbSchemaResolverFactory;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;

import oracle.jpub.runtime.MutableStruct;

import oracle.sql.Datum;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.STRUCT;

public class RecReports implements ORAData, ORADataFactory {
    public static final String _SQL_NAME = "REC_REPORTS";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int T_BOL_NBR_IDX = 0;
    public static final int T_QUEUE_IDX = 1;
    public static final int T_TYPE_IDX = 2;
    protected MutableStruct _struct;
    protected static int[] _sqlType = { 2, 12, 12 };
    protected static ORADataFactory[] _factory = new ORADataFactory[3];
    static {
    }
    protected static final RecReports _RecReportsFactory = new RecReports();

    public static ORADataFactory getORADataFactory() {
        return _RecReportsFactory;
    }

    public RecReports() {
        _struct = new MutableStruct(new Object[3], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName());
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(RecReports o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new RecReports();
        o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
        return o;
    }

    public final java.math.BigDecimal getTBolNbr() throws SQLException {
        return (java.math.BigDecimal) _struct.getAttribute(T_BOL_NBR_IDX);
    }

    public final void setTBolNbr(java.math.BigDecimal tBolNbr) throws SQLException {
        _struct.setAttribute(T_BOL_NBR_IDX, tBolNbr);
    }

    public final java.lang.String getTQueue() throws SQLException {
        return (java.lang.String) _struct.getAttribute(T_QUEUE_IDX);
    }

    public final void setTQueue(java.lang.String tQueue) throws SQLException {
        _struct.setAttribute(T_QUEUE_IDX, tQueue);
    }

    public final java.lang.String getTType() throws SQLException {
        return (java.lang.String) _struct.getAttribute(T_TYPE_IDX);
    }

    public final void setTType(java.lang.String tType) throws SQLException {
        _struct.setAttribute(T_TYPE_IDX, tType);
    }

    public static String getSQLTypeName() {
        return RDMDbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);
    }

    public static int getSQLTypeCode() {
        return _SQL_TYPECODE;
    }
} // end class
