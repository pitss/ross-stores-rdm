
package com.ross.rdm.shipping.hhshiptrailers.model.oradata;

import com.ross.rdm.common.utils.model.util.dbcall.customTypes.RDMDbSchemaResolverFactory;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;

import oracle.jpub.runtime.MutableArray;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.Datum;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;

public class TReport implements ORAData, ORADataFactory {
    public static final String _SQL_NAME = "T_REPORT";
    public static final int _SQL_TYPECODE = OracleTypes.ARRAY;
    MutableArray _array;
    private static final TReport _TReportFactory = new TReport();

    public static ORADataFactory getORADataFactory() {
        return _TReportFactory;
    }

    public TReport() {
        this((RecReports[]) null);
    }

    public TReport(RecReports[] a) {
        _array = new MutableArray(2002, a, RecReports.getORADataFactory());
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _array.toDatum(c, getSQLTypeName());
    }

    public int length() throws SQLException {
        return _array.length();
    }

    public int getBaseType() throws SQLException {
        return _array.getBaseType();
    }

    public String getBaseTypeName() throws SQLException {
        return _array.getBaseTypeName();
    }

    public ArrayDescriptor getDescriptor() throws SQLException {
        return _array.getDescriptor();
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        TReport a = new TReport();
        a._array = new MutableArray(2002, (ARRAY) d, RecReports.getORADataFactory());
        return a;
    }

    public RecReports[] getArray() throws SQLException {
        return (RecReports[]) _array.getObjectArray(new RecReports[_array.length()]);
    }

    public RecReports[] getArray(long index, int count) throws SQLException {
        return (RecReports[]) _array.getObjectArray(index, new RecReports[_array.sliceLength(index, count)]);
    }

    public void setArray(RecReports[] a) throws SQLException {
        _array.setObjectArray(a);
    }

    public void setArray(RecReports[] a, long index) throws SQLException {
        _array.setObjectArray(a, index);
    }

    public RecReports getElement(long index) throws SQLException {
        return (RecReports) _array.getObjectElement(index);
    }

    public void setElement(RecReports a, long index) throws SQLException {
        _array.setObjectElement(a, index);
    }

    public static String getSQLTypeName() {
        return RDMDbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);
    }

    public static int getSQLTypeCode() {
        return _SQL_TYPECODE;
    }
} // end class
