package com.ross.rdm.shipping.hhloadcontainers.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import java.sql.Date;

import oracle.jbo.RowSet;
import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed May 25 13:09:14 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class HhLoadContainerSLoadContainerViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        DestId {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getDestId();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setDestId((BigDecimal) value);
            }
        }
        ,
        TrailerId {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getTrailerId();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setTrailerId((String) value);
            }
        }
        ,
        DoorId {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getDoorId();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setDoorId((String) value);
            }
        }
        ,
        ContainerId {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getContainerId();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setContainerId((String) value);
            }
        }
        ,
        LoadTs {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getLoadTs();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setLoadTs((Date) value);
            }
        }
        ,
        CartonCount {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getCartonCount();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setCartonCount((String) value);
            }
        }
        ,
        VA_LoadContaiContainerILov0 {
            public Object get(HhLoadContainerSLoadContainerViewRowImpl obj) {
                return obj.getVA_LoadContaiContainerILov0();
            }

            public void put(HhLoadContainerSLoadContainerViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(HhLoadContainerSLoadContainerViewRowImpl object);

        public abstract void put(HhLoadContainerSLoadContainerViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int DESTID = AttributesEnum.DestId.index();
    public static final int TRAILERID = AttributesEnum.TrailerId.index();
    public static final int DOORID = AttributesEnum.DoorId.index();
    public static final int CONTAINERID = AttributesEnum.ContainerId.index();
    public static final int LOADTS = AttributesEnum.LoadTs.index();
    public static final int CARTONCOUNT = AttributesEnum.CartonCount.index();
    public static final int VA_LOADCONTAICONTAINERILOV0 = AttributesEnum.VA_LoadContaiContainerILov0.index();

    /**
     * This is the default constructor (do not remove).
     */
    public HhLoadContainerSLoadContainerViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute DestId.
     * @return the DestId
     */
    public BigDecimal getDestId() {
        return (BigDecimal) getAttributeInternal(DESTID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DestId.
     * @param value value to set the  DestId
     */
    public void setDestId(BigDecimal value) {
        setAttributeInternal(DESTID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TrailerId.
     * @return the TrailerId
     */
    public String getTrailerId() {
        return (String) getAttributeInternal(TRAILERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TrailerId.
     * @param value value to set the  TrailerId
     */
    public void setTrailerId(String value) {
        setAttributeInternal(TRAILERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DoorId.
     * @return the DoorId
     */
    public String getDoorId() {
        return (String) getAttributeInternal(DOORID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DoorId.
     * @param value value to set the  DoorId
     */
    public void setDoorId(String value) {
        setAttributeInternal(DOORID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContainerId.
     * @return the ContainerId
     */
    public String getContainerId() {
        return (String) getAttributeInternal(CONTAINERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContainerId.
     * @param value value to set the  ContainerId
     */
    public void setContainerId(String value) {
        setAttributeInternal(CONTAINERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute LoadTs.
     * @return the LoadTs
     */
    public Date getLoadTs() {
        return (Date) getAttributeInternal(LOADTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LoadTs.
     * @param value value to set the  LoadTs
     */
    public void setLoadTs(Date value) {
        setAttributeInternal(LOADTS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CartonCount.
     * @return the CartonCount
     */
    public String getCartonCount() {
        return (String) getAttributeInternal(CARTONCOUNT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CartonCount.
     * @param value value to set the  CartonCount
     */
    public void setCartonCount(String value) {
        setAttributeInternal(CARTONCOUNT, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> VA_LoadContaiContainerILov0.
     */
    public RowSet getVA_LoadContaiContainerILov0() {
        return (RowSet) getAttributeInternal(VA_LOADCONTAICONTAINERILOV0);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

