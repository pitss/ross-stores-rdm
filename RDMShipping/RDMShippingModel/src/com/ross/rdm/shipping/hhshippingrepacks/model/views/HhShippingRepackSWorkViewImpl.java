package com.ross.rdm.shipping.hhshippingrepacks.model.views;

import com.ross.rdm.common.utils.model.base.RDMTransientViewObjectImpl;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 03 11:53:27 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class HhShippingRepackSWorkViewImpl extends RDMTransientViewObjectImpl {
  private static ADFLogger _logger = ADFLogger.createADFLogger(HhShippingRepackSWorkViewImpl.class);

  
  /**
   * This is the default constructor (do not remove).
   */
  public HhShippingRepackSWorkViewImpl() { 
  } 
  
}
