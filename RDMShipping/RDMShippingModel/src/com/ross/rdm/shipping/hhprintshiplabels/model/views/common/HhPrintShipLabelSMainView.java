package com.ross.rdm.shipping.hhprintshiplabels.model.views.common;

import java.util.List;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Jun 02 10:20:53 EDT 2016
// ---------------------------------------------------------------------
public interface HhPrintShipLabelSMainView extends ViewObject {
    void setMainVariablesPrintShipLabel();


    Integer callCheckUserAuthWp();

    String callGetRoutePrintFlag(String lWhId);

    List<String> callPrintReprints();

    Integer callReprintLblExists(String layoutCidLength);

    List<String> callScp();


    List<String> printLabels();


    List<String> callVQueue(String queue);

    List<String> callVContainer1(String cid);

    List<Object> callVContainer2(String mainMsg, String scpLabelSet, String scpLayout, Integer layoutCidLength,
                                 String routePrint, Integer reprintLblExists);
}

