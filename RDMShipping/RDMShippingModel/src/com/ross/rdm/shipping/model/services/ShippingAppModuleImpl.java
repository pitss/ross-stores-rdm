package com.ross.rdm.shipping.model.services;

import com.ross.rdm.common.hhmenuss.model.views.GetLoginDateViewImpl;
import com.ross.rdm.common.model.services.RdmCommonAppModuleImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewImpl;
import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.model.views.UserMessageViewImpl;
import com.ross.rdm.common.utils.model.base.RDMApplicationModuleImpl;
import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.shipping.hhconveyorcutoffs.model.views.HhConveyorCutoffSConveyorViewImpl;
import com.ross.rdm.shipping.hhconveyorcutoffs.model.views.HhConveyorCutoffSWorkViewImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSLoadContainerViewImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSWorkLaborProdViewImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSWorkLaborProdViewRowImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSWorkLocalViewImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSWorkLocalViewRowImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSWorkViewImpl;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSWorkViewRowImpl;
import com.ross.rdm.shipping.hhprintshiplabels.model.views.HhPrintShipLabelSMainViewImpl;
import com.ross.rdm.shipping.hhprintshiplabels.model.views.HhPrintShipLabelSReprintAuthViewImpl;
import com.ross.rdm.shipping.hhprintshiplabels.model.views.HhPrintShipLabelSWorkLocalViewImpl;
import com.ross.rdm.shipping.hhprintshiplabels.model.views.HhPrintShipLabelSWorkViewImpl;
import com.ross.rdm.shipping.hhprintshiplabels.model.views.QueueViewImpl;
import com.ross.rdm.shipping.hhqcoutboundaudits.model.views.HhQcOutboundAuditSQcAuditViewImpl;
import com.ross.rdm.shipping.hhqcoutboundaudits.model.views.HhQcOutboundAuditSWorkLaborProdViewImpl;
import com.ross.rdm.shipping.hhqcoutboundaudits.model.views.HhQcOutboundAuditSWorkLocalViewImpl;
import com.ross.rdm.shipping.hhqcoutboundaudits.model.views.HhQcOutboundAuditSWorkLocalViewRowImpl;
import com.ross.rdm.shipping.hhqcoutboundaudits.model.views.HhQcOutboundAuditSWorkViewImpl;
import com.ross.rdm.shipping.hhqcoutboundaudits.model.views.HhQcOutboundAuditSWorkViewRowImpl;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSBRepackViewImpl;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSWorkLaborProdViewImpl;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSWorkLocalViewImpl;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSWorkLocalViewRowImpl;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSWorkViewImpl;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSWorkViewRowImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSEstApptStartViewImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSShipTrailerViewImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSUserPassViewImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSWorkLocalViewImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSWorkLocalViewRowImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSWorkViewImpl;
import com.ross.rdm.shipping.hhshiptrailers.model.views.HhShipTrailerSWorkViewRowImpl;
import com.ross.rdm.shipping.hhunloadcontainers.model.views.HhUnloadContainerSUnloadContainerViewImpl;
import com.ross.rdm.shipping.hhunloadcontainers.model.views.HhUnloadContainerSWorkLaborProdViewImpl;
import com.ross.rdm.shipping.hhunloadcontainers.model.views.HhUnloadContainerSWorkLaborProdViewRowImpl;
import com.ross.rdm.shipping.hhunloadcontainers.model.views.HhUnloadContainerSWorkViewImpl;
import com.ross.rdm.shipping.hhunloadcontainers.model.views.HhUnloadContainerSWorkViewRowImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.CheckTrailerStatusViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.DmsTeamUserViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.DmsTeamViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.HhUnloadTeamSUnloadTeamViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.HhUnloadTeamSUnloadUsersViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.HhUnloadTeamSWorkLocalViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.HhUnloadTeamSWorkViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.HhUnloadTeamSWorkViewRowImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.TeamCartonViewImpl;
import com.ross.rdm.shipping.hhunloadteams.model.views.TeamUsersViewImpl;
import com.ross.rdm.shipping.model.services.common.ShippingAppModule;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;


import oracle.jbo.server.ApplicationModuleImpl;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue May 03 12:06:07 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class ShippingAppModuleImpl extends RDMApplicationModuleImpl implements ShippingAppModule {
    private static ADFLogger _logger = ADFLogger.createADFLogger(ShippingAppModuleImpl.class);

    @Override
    public void afterConnect() {
        super.afterConnect();
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS",
                                    facilityId, languageCode);

        
    }

    public void initTaskFlowShippingRepack() {
        prepareViewObject(this.getHhShippingRepackSBRepackView());
        prepareViewObject(this.getHhShippingRepackSWorkLaborProdView());
        prepareViewObject(this.getHhShippingRepackSWorkLocalView());
        prepareViewObject(this.getHhShippingRepackSWorkView());

        trStartupShippingRepack();
        startupLocalShippingRepack();
    }

    private void trStartupShippingRepack() {
        GlobalVariablesViewRowImpl global = this.setGlobalVariables();

        getRdmCommonAppModule().initCurrentUserIfNeed();
        HhShippingRepackSWorkViewRowImpl workViewImplRowImpl =
            (HhShippingRepackSWorkViewRowImpl) this.getHhShippingRepackSWorkView().getCurrentRow();

        workViewImplRowImpl.setUser(global.getGlobalUserId());
        workViewImplRowImpl.setHeader(global.getGlobalHeader());
        workViewImplRowImpl.setFacilityId(global.getGlobalFacilityId());
        workViewImplRowImpl.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workViewImplRowImpl.setDevice(global.getGlobalDevice());
        workViewImplRowImpl.setLanguageCode(global.getGlobalLanguageCode());

        workViewImplRowImpl.setHeader(this.callDisplayHeader(workViewImplRowImpl.getFacilityId(),
                                                             workViewImplRowImpl.getLanguageCode(),
                                                             "HH_SHIPPING_REPACK_S"));
    }

    private void startupLocalShippingRepack() {
        HhShippingRepackSWorkViewRowImpl workViewImplRowImpl =
            (HhShippingRepackSWorkViewRowImpl) this.getHhShippingRepackSWorkView().getCurrentRow();
        HhShippingRepackSWorkLocalViewRowImpl workLocalViewImplRowImpl =
            (HhShippingRepackSWorkLocalViewRowImpl) this.getHhShippingRepackSWorkLocalView().getCurrentRow();

        workViewImplRowImpl.setVersionNumber("%I%");
        String checkDigit = getGScp(workViewImplRowImpl.getFacilityId(), "check_digit");
        workLocalViewImplRowImpl.setCheckDigit(checkDigit);
        callSetAhlInfo(workViewImplRowImpl.getFacilityId(), "HH_SHIPPING_REPACK_S");
    }

    public void iniTaskFlowUnloadTeams() {
        this.prepareViewObject(this.getHhUnloadTeamSUnloadTeamView());
        this.prepareViewObject(this.getHhUnloadTeamSUnloadUsersView());
        this.prepareViewObject(this.getHhUnloadTeamSWorkLocalView());
        this.prepareViewObject(this.getHhUnloadTeamSWorkView());
        GlobalVariablesViewRowImpl global = this.setGlobalVariables();
        this.trStartupLocalUnloadTeams(global);
    }

    private void trStartupLocalUnloadTeams(GlobalVariablesViewRowImpl global) {
        HhUnloadTeamSWorkViewRowImpl unloadRow =
            (HhUnloadTeamSWorkViewRowImpl) this.getHhUnloadTeamSWorkView().getCurrentRow();
        unloadRow.setUser(global.getGlobalUserId());
        unloadRow.setHeader(global.getGlobalHeader());
        unloadRow.setFacilityId(global.getGlobalFacilityId());
        unloadRow.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        unloadRow.setDevice(global.getGlobalDevice());
        unloadRow.setLanguageCode(global.getGlobalLanguageCode());
        unloadRow.setHeader(this.callDisplayHeader(unloadRow.getFacilityId(), unloadRow.getLanguageCode(),
                                                   "HH_UNLOAD_TEAM_S"));
        this.callSetAhlInfo(unloadRow.getFacilityId(), "HH_UNLOAD_TEAM_S");
    }

    public void initTaskFlowLoadContainer() {
        prepareViewObject(this.getHhLoadContainerSLoadContainerView());
        prepareViewObject(this.getHhLoadContainerSWorkLaborProdView());
        prepareViewObject(this.getHhLoadContainerSWorkLocalView());
        prepareViewObject(this.getHhLoadContainerSWorkView());
        trStartupLoadContainer();
    }

    private void trStartupLoadContainer() {
        GlobalVariablesViewRowImpl global = this.setGlobalVariables();
        getRdmCommonAppModule().initCurrentUserIfNeed();
        HhLoadContainerSWorkViewRowImpl workViewImplRowImpl =
            (HhLoadContainerSWorkViewRowImpl) this.getHhLoadContainerSWorkView().getCurrentRow();

        HhLoadContainerSWorkLocalViewRowImpl workLocalImplRowImpl =
            (HhLoadContainerSWorkLocalViewRowImpl) this.getHhLoadContainerSWorkLocalView().getCurrentRow();
        workViewImplRowImpl.setUser(global.getGlobalUserId());
        workViewImplRowImpl.setHeader(global.getGlobalHeader());
        workViewImplRowImpl.setFacilityId(global.getGlobalFacilityId());
        workViewImplRowImpl.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workViewImplRowImpl.setDevice(global.getGlobalDevice());
        workViewImplRowImpl.setLanguageCode(global.getGlobalLanguageCode());
        startupLaborProdLoadContainer();
        workViewImplRowImpl.setVersionNumber("%I%");

        String loadSequencing = getGScp(workViewImplRowImpl.getFacilityId(), "load_sequencing");
        workLocalImplRowImpl.setLoadSequencing(loadSequencing);

        workViewImplRowImpl.setHeader(this.callDisplayHeader(workViewImplRowImpl.getFacilityId(),
                                                             workViewImplRowImpl.getLanguageCode(),
                                                             "HH_LOAD_CONTAINER_S"));
        callInitializeFacilitySetup(workViewImplRowImpl.getFacilityId());
    }

    public void initTaskFlowUnloadContainer() {
        prepareViewObject(this.getHhUnloadContainerSUnloadContainerView());
        prepareViewObject(this.getHhUnloadContainerSWorkLaborProdView());
        prepareViewObject(this.getHhUnloadContainerSWorkView());
        trStartupUnloadContainer();
    }

    private void trStartupUnloadContainer() {
        GlobalVariablesViewRowImpl global = this.setGlobalVariables();
        getRdmCommonAppModule().initCurrentUserIfNeed();
        HhUnloadContainerSWorkViewRowImpl workViewImplRowImpl =
            (HhUnloadContainerSWorkViewRowImpl) this.getHhUnloadContainerSWorkView().getCurrentRow();

        workViewImplRowImpl.setUser(global.getGlobalUserId());
        workViewImplRowImpl.setHeader(global.getGlobalHeader());
        workViewImplRowImpl.setFacilityId(global.getGlobalFacilityId());
        workViewImplRowImpl.setUserPrivilege(new BigDecimal(global.getGlobalUserPrivilege()));
        workViewImplRowImpl.setDevice(global.getGlobalDevice());
        workViewImplRowImpl.setLanguageCode(global.getGlobalLanguageCode());
        workViewImplRowImpl.setVersionNumber("%I%");

        startupLaborProdUnloadContainer();

        workViewImplRowImpl.setHeader(this.callDisplayHeader(workViewImplRowImpl.getFacilityId(),
                                                             workViewImplRowImpl.getLanguageCode(),
                                                             "HH_UNLOAD_CONTAINER_S"));
    }

    private void startupLaborProdUnloadContainer() {
        HhUnloadContainerSWorkLaborProdViewRowImpl workLaborProdViewImplRowImpl =
            (HhUnloadContainerSWorkLaborProdViewRowImpl) this.getHhUnloadContainerSWorkLaborProdView().getCurrentRow();
        workLaborProdViewImplRowImpl.setStartTime(new java.sql.Date((new java.util.Date()).getTime()));
        workLaborProdViewImplRowImpl.setOperationsPerformed(BigDecimal.ZERO);
        workLaborProdViewImplRowImpl.setContainersProcessed(BigDecimal.ZERO);
        workLaborProdViewImplRowImpl.setUnitsProcessed(BigDecimal.ZERO);
        workLaborProdViewImplRowImpl.setActivityCode("CNTULD");
        workLaborProdViewImplRowImpl.setReferenceCode("---------");
    }

    private void startupLaborProdLoadContainer() {
        HhLoadContainerSWorkLaborProdViewRowImpl workLaborProdViewImplRowImpl =
            (HhLoadContainerSWorkLaborProdViewRowImpl) this.getHhLoadContainerSWorkLaborProdView().getCurrentRow();
        workLaborProdViewImplRowImpl.setStartTime(new java.sql.Date((new java.util.Date()).getTime()));
        workLaborProdViewImplRowImpl.setOperationsPerformed(BigDecimal.ZERO);
        workLaborProdViewImplRowImpl.setContainersProcessed(BigDecimal.ZERO);
        workLaborProdViewImplRowImpl.setUnitsProcessed(BigDecimal.ZERO);
        workLaborProdViewImplRowImpl.setActivityCode("CNTULD");
        workLaborProdViewImplRowImpl.setReferenceCode("---------");
    }

    private GlobalVariablesViewRowImpl setGlobalVariables() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
        }
        Row loginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) loginUserRow.getAttribute("FacilityId");
        String languageCode = (String) loginUserRow.getAttribute("LanguageCode");
        String user = (String) loginUserRow.getAttribute("UserId");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALUSERID, user);
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALFACILITYID, facilityId);
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALUSERPRIVILEGE, "1");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALDEVICE, "R");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALLANGUAGECODE, languageCode);
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALMSGCODE, "");
        defaultValue(variablesRow, GlobalVariablesViewRowImpl.GLOBALRESPONSE, "");
        gVViewImpl.insertRow(variablesRow);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        return variablesRow;
    }

    private void prepareViewObject(RDMViewObjectImpl vo) {
        if (vo != null) {
            if (vo.getCurrentRow() == null) {
                vo.insertRow(vo.createRow());
            } else {
                vo.getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES);
            }
        }
    }

    public String callDisplayHeader(String facilityId, String langCode, String formName) {
        String header =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "PKG_COMMON_ADF.DISPLAY_HEADER",
                                                facilityId, langCode, formName);
        if (header != null) {
            header = header.replaceFirst(facilityId, "").trim();
        }
        return header;
    }

    private void defaultValue(RDMViewRowImpl row, int attribute, Object defaultValue) {
        if (row != null && row.getAttribute(attribute) == null) {
            row.setAttribute(attribute, defaultValue);
        }
    }

    public boolean containsSeveralMessages(String string) {
        return StringUtils.isNotEmpty(string) && string.contains("#") &&
               StringUtils.isNotEmpty(string.replace("#", ""));
    }

    public List<String> processMessagesParams(SQLOutParam p_v_return, SQLOutParam p_msg_display) {
        List<String> errors = null;
        if (p_v_return.getWrappedData() != null && !"RAISE_FAILURE####".equals(p_v_return.getWrappedData())) {
            errors = new ArrayList<String>();
            if (this.containsSeveralMessages((String) p_v_return.getWrappedData()) &&
                this.containsSeveralMessages((String) p_msg_display.getWrappedData())) {
                String p_types = (String) p_msg_display.getWrappedData();
                String[] types = p_types.split("[#]");
                String p_msgs = (String) p_v_return.getWrappedData();
                String[] messages = p_msgs.split("[#]");
                String message = "";
                for (int i = 0; i < messages.length; i++) {
                    message += messages[i];
                    if (i > 0) {
                        message += ". ";
                    }
                }
                if (types.length > 0) {
                    errors.add(types[0]);
                } else {
                    errors.add("E");
                }
                errors.add(message);
            } else {
                errors.add((String) p_msg_display.getWrappedData());
                if (((String) p_v_return.getWrappedData()).contains("RAISE_FAILURE####")) {
                    String error = (String) p_v_return.getWrappedData();
                    error = error.replace("RAISE_FAILURE####", "");
                    errors.add(error);
                } else {
                    errors.add((String) p_v_return.getWrappedData());
                }
            }
        }
        return errors;
    }

    public String callCheckScreenOptionPriv(String facilityId, String userId, String formName, String optionName) {
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                   "PKG_COMMON_ADF.CHECK_SCREEN_OPTION_PRIV", facilityId, userId,
                                                   formName, optionName);
    }

    public String callCheckUserPassword(String facilityId, String userId, String passwordIn, String formName,
                                        String optionName) {
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                   "PKG_HOTEL_PICKING_ADF.CHECK_USER_PASSWORD", facilityId, userId,
                                                   passwordIn, formName, optionName);
    }

    public GlobalVariablesViewRowImpl setGlobalVariablesShipTrailerS() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl globalVO = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl variablesRow = (GlobalVariablesViewRowImpl) globalVO.getCurrentRow();
        Row loginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) loginUserRow.getAttribute("FacilityId");
        String languageCode = (String) loginUserRow.getAttribute("LanguageCode");
        String user = (String) loginUserRow.getAttribute("UserId");
        if (variablesRow == null) {
            variablesRow = (GlobalVariablesViewRowImpl) globalVO.createRow();
        }
        variablesRow.setGlobalUserId(user);
        variablesRow.setGlobalHeader("UNDEFINED");
        variablesRow.setGlobalFacilityId(facilityId);
        variablesRow.setGlobalUserPrivilege("1");
        variablesRow.setGlobalDevice("R");
        variablesRow.setGlobalLanguageCode(languageCode);
        variablesRow.setGlobalMsgCode("");
        variablesRow.setGlobalResponse("");
        globalVO.insertRow(variablesRow);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        return variablesRow;
    }

    public void callDoCommit() {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_COMMON_ADF.doCommit");
    }

    public void callDoRollback() {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_COMMON_ADF.doRollback");
    }

    public void initShipTrailerS() {
        this.prepareViewObject(this.getHhShipTrailerSWorkView());
        this.prepareViewObject(this.getHhShipTrailerSWorkLocalView());
        this.prepareViewObject(this.getHhShipTrailerSShipTrailerView());
        this.prepareViewObject(this.getHhShipTrailerSEstApptStartView());
        this.prepareViewObject(this.getHhShipTrailerSUserPassView());

        GlobalVariablesViewRowImpl globalRow = this.setGlobalVariablesShipTrailerS();

        HhShipTrailerSWorkViewRowImpl workRow =
            (HhShipTrailerSWorkViewRowImpl) this.getHhShipTrailerSWorkView().getCurrentRow();

        workRow.setUser(globalRow.getGlobalUserId());
        workRow.setFacilityId(globalRow.getGlobalFacilityId());
        workRow.setLanguageCode(globalRow.getGlobalLanguageCode());

        String header =
            this.callDisplayHeader(globalRow.getGlobalFacilityId(), globalRow.getGlobalLanguageCode(),
                                   "HH_SHIP_TRAILER_S");
        workRow.setHeader(header);

        workRow.setMainBlock("SHIP_TRAILER");
        workRow.setVersionNumber("%I%");

        HhShipTrailerSWorkLocalViewRowImpl workLocalRow =
            (HhShipTrailerSWorkLocalViewRowImpl) this.getHhShipTrailerSWorkLocalView().getCurrentRow();

        //ADDED BY DAN L. 8/22/16
        SQLOutParam pYardMgmtAuth = new SQLOutParam(null, Types.VARCHAR);
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_SHIP_TRAILER_ADF.STARTUP_LOCAL",
                                             workRow.getFacilityId(), workRow.getUser(), pYardMgmtAuth);
        workLocalRow.setYardMgmtAuth((String) pYardMgmtAuth.getWrappedData());
        //
        workLocalRow.setMultiOpenManifest(this.gScp(globalRow.getGlobalFacilityId(), "multi_open_manifest"));

        String pFacilityId = workRow.getFacilityId();
        SQLOutParam pMldEnabled = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pDestId = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pFacilityType = new SQLOutParam(null, Types.VARCHAR);

        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.initialize", pFacilityId,
                                             pMldEnabled, pDestId, pFacilityType);

        globalRow.setGlobalMldEnabled((String) pMldEnabled.getWrappedData());
        globalRow.setGlobalDestId((String) pDestId.getWrappedData());
        globalRow.setGlobalFacilityType((String) pFacilityType.getWrappedData());

        workLocalRow.setUserId(workRow.getUser());

        callSetAhlInfo(globalRow.getGlobalFacilityId(), "HH_SHIP_TRAILER_S");
    }

    public String getUserMessagesByCode(String msgCode) {
        UserMessageViewImpl userMessageView = this.getUserMessageView();
        userMessageView.setbindCodeType(msgCode);
        GlobalVariablesViewRowImpl globalRow =
            (GlobalVariablesViewRowImpl) this.getGlobalVariablesView().getCurrentRow();
        if (globalRow != null) {
            userMessageView.setbindFacilityId(globalRow.getGlobalFacilityId());
            userMessageView.setbindLangCode(globalRow.getGlobalLanguageCode());
        }
        userMessageView.executeQuery();
        Row rowViewKey = this.getUserMessageView().first();
        if (rowViewKey == null) {
            return "???" + msgCode + "???";
        } else {
            return (String) rowViewKey.getAttribute("MessageText");
        }
    }

    private String trStartupQcOutboundAudit() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row loginUserRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) loginUserRow.getAttribute("FacilityId");
        if (facilityId == null) {
            facilityId = "PR";
        }
        String languageCode = (String) loginUserRow.getAttribute("LanguageCode");
        if (languageCode == null) {
            languageCode = "AM";
        }

        gVViewImplRowImpl.setGlobalUserId((String) (this.getGetLoginDateView().getCurrentRow()).getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("R");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);

        HhQcOutboundAuditSWorkViewRowImpl workRow =
            (HhQcOutboundAuditSWorkViewRowImpl) this.getHhQcOutboundAuditSWorkView().getCurrentRow();

        HhQcOutboundAuditSWorkLocalViewRowImpl workLocalRow =
            (HhQcOutboundAuditSWorkLocalViewRowImpl) this.getHhQcOutboundAuditSWorkLocalView().getCurrentRow();

        facilityId = gVViewImplRowImpl.getGlobalFacilityId();
        String langCode = gVViewImplRowImpl.getGlobalLanguageCode();

        String header = this.callDisplayHeader(facilityId, langCode, "HH_QC_OUTBOUND_AUDIT_S");
        workRow.setHeader(header);

        workRow.setUser(gVViewImplRowImpl.getGlobalUserId());
        workRow.setFacilityId(facilityId);
        workRow.setUserPrivilege(new BigDecimal(gVViewImplRowImpl.getGlobalUserPrivilege()));
        workRow.setDevice(gVViewImplRowImpl.getGlobalDevice());
        workRow.setCallingBlock("MAIN");
        workRow.setLanguageCode(gVViewImplRowImpl.getGlobalLanguageCode());

        workRow.setMainBlock("QC_OUTBOUND");
        workLocalRow.setQcAuditQueue(gScp(facilityId, "qc_audit_queue"));

        _logger.info("setGlobalVariablesQcOutboundAudit() End");
        return facilityId;
    }

    public void initTaskFlowQcOutboundAudit(String formName) {
        prepareViewObject(getHhQcOutboundAuditSQcAuditView());
        prepareViewObject(getHhQcOutboundAuditSWorkView());
        prepareViewObject(getHhQcOutboundAuditSWorkLocalView());

        String facilityId = trStartupQcOutboundAudit();
        setAhlInfo(facilityId, formName);

    }

    public void setAhlInfo(String facilityId, String currentForm) {
        callStoredProcedure("AHL.SET_AHL_INFO", facilityId, currentForm);
    }

    public void callInitializeFacilitySetup(String pFacilityId) {
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl row = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();

        SQLOutParam pMldEnabled = new SQLOutParam(row.getGlobalMldEnabled(), Types.VARCHAR);
        SQLOutParam pDestId = new SQLOutParam(row.getGlobalDestId(), Types.VARCHAR);
        SQLOutParam pFacilityType = new SQLOutParam(row.getGlobalFacilityType(), Types.VARCHAR);

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_HOTEL_PICKING_ADF.INITIALIZE", pFacilityId,
                                    pMldEnabled, pDestId, pFacilityType);

        row.setGlobalMldEnabled((String) pMldEnabled.getWrappedData());
        row.setGlobalDestId((String) pDestId.getWrappedData());
        row.setGlobalDestId((String) pFacilityType.getWrappedData());
    }

    /**
     * Wrapper for a call to database function 'g_scp'
     */
    private String getGScp(String facilityId, String var) {
        return (String) DBUtils.callStoredFunction(getDBTransaction(), Types.VARCHAR, "g_scp", facilityId, var);
    }

    /**
     * This is the default constructor (do not remove).
     */
    public ShippingAppModuleImpl() {
    }

    /**
     * Container's getter for GlobalVariablesView1.
     * @return GlobalVariablesView1
     */
    public GlobalVariablesViewImpl getGlobalVariablesView() {
        return this.getRdmCommonAppModule().getGlobalVariablesView();
    }

    /**
     * Container's getter for UserMessageView1.
     * @return UserMessageView1
     */
    public UserMessageViewImpl getUserMessageView() {
        return this.getRdmCommonAppModule().getUserMessageView();
    }

    /**
     * Container's getter for GetLoginDateView1.
     * @return GetLoginDateView1
     */
    public GetLoginDateViewImpl getGetLoginDateView() {
        return this.getRdmCommonAppModule().getGetLoginDateView();
    }


    /**
     * Container's getter for HhConveyorCutoffSWorkView.
     * @return HhConveyorCutoffSWorkView
     */
    public HhConveyorCutoffSWorkViewImpl getHhConveyorCutoffSWorkView() {
        return (HhConveyorCutoffSWorkViewImpl) findViewObject("HhConveyorCutoffSWorkView");
    }

    /**
     * Container's getter for HhConveyorCutoffSConveyorView.
     * @return HhConveyorCutoffSConveyorView
     */
    public HhConveyorCutoffSConveyorViewImpl getHhConveyorCutoffSConveyorView() {
        return (HhConveyorCutoffSConveyorViewImpl) findViewObject("HhConveyorCutoffSConveyorView");
    }


    /**
     * Container's getter for HhLoadContainerSLoadContainerView.
     * @return HhLoadContainerSLoadContainerView
     */
    public HhLoadContainerSLoadContainerViewImpl getHhLoadContainerSLoadContainerView() {
        return (HhLoadContainerSLoadContainerViewImpl) findViewObject("HhLoadContainerSLoadContainerView");
    }

    /**
     * Container's getter for HhLoadContainerSWorkView.
     * @return HhLoadContainerSWorkView
     */
    public HhLoadContainerSWorkViewImpl getHhLoadContainerSWorkView() {
        return (HhLoadContainerSWorkViewImpl) findViewObject("HhLoadContainerSWorkView");
    }

    /**
     * Container's getter for HhLoadContainerSWorkLaborProdView.
     * @return HhLoadContainerSWorkLaborProdView
     */
    public HhLoadContainerSWorkLaborProdViewImpl getHhLoadContainerSWorkLaborProdView() {
        return (HhLoadContainerSWorkLaborProdViewImpl) findViewObject("HhLoadContainerSWorkLaborProdView");
    }

    /**
     * Container's getter for HhLoadContainerSWorkLocalView.
     * @return HhLoadContainerSWorkLocalView
     */
    public HhLoadContainerSWorkLocalViewImpl getHhLoadContainerSWorkLocalView() {
        return (HhLoadContainerSWorkLocalViewImpl) findViewObject("HhLoadContainerSWorkLocalView");
    }


    /**
     * Container's getter for HhQcOutboundAuditSQcAuditView.
     * @return HhQcOutboundAuditSQcAuditView
     */
    public HhQcOutboundAuditSQcAuditViewImpl getHhQcOutboundAuditSQcAuditView() {
        return (HhQcOutboundAuditSQcAuditViewImpl) findViewObject("HhQcOutboundAuditSQcAuditView");
    }


    /**
     * Container's getter for HhQcOutboundAuditSWorkLaborProdView.
     * @return HhQcOutboundAuditSWorkLaborProdView
     */
    public HhQcOutboundAuditSWorkLaborProdViewImpl getHhQcOutboundAuditSWorkLaborProdView() {
        return (HhQcOutboundAuditSWorkLaborProdViewImpl) findViewObject("HhQcOutboundAuditSWorkLaborProdView");
    }

    /**
     * Container's getter for HhQcOutboundAuditSWorkView.
     * @return HhQcOutboundAuditSWorkView
     */
    public HhQcOutboundAuditSWorkViewImpl getHhQcOutboundAuditSWorkView() {
        return (HhQcOutboundAuditSWorkViewImpl) findViewObject("HhQcOutboundAuditSWorkView");
    }

    /**
     * Container's getter for HhQcOutboundAuditSWorkLocalView.
     * @return HhQcOutboundAuditSWorkLocalView
     */
    public HhQcOutboundAuditSWorkLocalViewImpl getHhQcOutboundAuditSWorkLocalView() {
        return (HhQcOutboundAuditSWorkLocalViewImpl) findViewObject("HhQcOutboundAuditSWorkLocalView");
    }

    /**
     * Container's getter for HhShippingRepackSBRepackView.
     * @return HhShippingRepackSBRepackView
     */
    public HhShippingRepackSBRepackViewImpl getHhShippingRepackSBRepackView() {
        return (HhShippingRepackSBRepackViewImpl) findViewObject("HhShippingRepackSBRepackView");
    }

    /**
     * Container's getter for HhShippingRepackSWorkView.
     * @return HhShippingRepackSWorkView
     */
    public HhShippingRepackSWorkViewImpl getHhShippingRepackSWorkView() {
        return (HhShippingRepackSWorkViewImpl) findViewObject("HhShippingRepackSWorkView");
    }


    /**
     * Container's getter for HhShippingRepackSWorkLaborProdView.
     * @return HhShippingRepackSWorkLaborProdView
     */
    public HhShippingRepackSWorkLaborProdViewImpl getHhShippingRepackSWorkLaborProdView() {
        return (HhShippingRepackSWorkLaborProdViewImpl) findViewObject("HhShippingRepackSWorkLaborProdView");
    }

    /**
     * Container's getter for HhShippingRepackSWorkLocalView.
     * @return HhShippingRepackSWorkLocalView
     */
    public HhShippingRepackSWorkLocalViewImpl getHhShippingRepackSWorkLocalView() {
        return (HhShippingRepackSWorkLocalViewImpl) findViewObject("HhShippingRepackSWorkLocalView");
    }

    /**
     * Container's getter for HhShipTrailerSWorkView.
     * @return HhShipTrailerSWorkView
     */
    public HhShipTrailerSWorkViewImpl getHhShipTrailerSWorkView() {
        return (HhShipTrailerSWorkViewImpl) findViewObject("HhShipTrailerSWorkView");
    }

    /**
     * Container's getter for HhShipTrailerSWorkLocalView.
     * @return HhShipTrailerSWorkLocalView
     */
    public HhShipTrailerSWorkLocalViewImpl getHhShipTrailerSWorkLocalView() {
        return (HhShipTrailerSWorkLocalViewImpl) findViewObject("HhShipTrailerSWorkLocalView");
    }

    /**
     * Container's getter for HhShipTrailerSShipTrailerView.
     * @return HhShipTrailerSShipTrailerView
     */
    public HhShipTrailerSShipTrailerViewImpl getHhShipTrailerSShipTrailerView() {
        return (HhShipTrailerSShipTrailerViewImpl) findViewObject("HhShipTrailerSShipTrailerView");
    }

    /**
     * Container's getter for HhShipTrailerSEstApptStartView.
     * @return HhShipTrailerSEstApptStartView
     */
    public HhShipTrailerSEstApptStartViewImpl getHhShipTrailerSEstApptStartView() {
        return (HhShipTrailerSEstApptStartViewImpl) findViewObject("HhShipTrailerSEstApptStartView");
    }

    /**
     * Container's getter for HhShipTrailerSUserPassView.
     * @return HhShipTrailerSUserPassView
     */
    public HhShipTrailerSUserPassViewImpl getHhShipTrailerSUserPassView() {
        return (HhShipTrailerSUserPassViewImpl) findViewObject("HhShipTrailerSUserPassView");
    }


    /**
     * Container's getter for HhUnloadContainerSUnloadContainerView.
     * @return HhUnloadContainerSUnloadContainerView
     */
    public HhUnloadContainerSUnloadContainerViewImpl getHhUnloadContainerSUnloadContainerView() {
        return (HhUnloadContainerSUnloadContainerViewImpl) findViewObject("HhUnloadContainerSUnloadContainerView");
    }

    /**
     * Container's getter for HhUnloadContainerSWorkLaborProdView.
     * @return HhUnloadContainerSWorkLaborProdView
     */
    public HhUnloadContainerSWorkLaborProdViewImpl getHhUnloadContainerSWorkLaborProdView() {
        return (HhUnloadContainerSWorkLaborProdViewImpl) findViewObject("HhUnloadContainerSWorkLaborProdView");
    }

    /**
     * Container's getter for HhUnloadContainerSWorkView.
     * @return HhUnloadContainerSWorkView
     */
    public HhUnloadContainerSWorkViewImpl getHhUnloadContainerSWorkView() {
        return (HhUnloadContainerSWorkViewImpl) findViewObject("HhUnloadContainerSWorkView");
    }


    /**
     * Container's getter for HhUnloadTeamSWorkView.
     * @return HhUnloadTeamSWorkView
     */
    public HhUnloadTeamSWorkViewImpl getHhUnloadTeamSWorkView() {
        return (HhUnloadTeamSWorkViewImpl) findViewObject("HhUnloadTeamSWorkView");
    }

    /**
     * Container's getter for HhUnloadTeamSWorkLocalView.
     * @return HhUnloadTeamSWorkLocalView
     */
    public HhUnloadTeamSWorkLocalViewImpl getHhUnloadTeamSWorkLocalView() {
        return (HhUnloadTeamSWorkLocalViewImpl) findViewObject("HhUnloadTeamSWorkLocalView");
    }

    /**
     * Container's getter for HhUnloadTeamSUnloadTeamView.
     * @return HhUnloadTeamSUnloadTeamView
     */
    public HhUnloadTeamSUnloadTeamViewImpl getHhUnloadTeamSUnloadTeamView() {
        return (HhUnloadTeamSUnloadTeamViewImpl) findViewObject("HhUnloadTeamSUnloadTeamView");
    }


    /**
     * Container's getter for HhUnloadTeamSUnloadUsersView.
     * @return HhUnloadTeamSUnloadUsersView
     */
    public HhUnloadTeamSUnloadUsersViewImpl getHhUnloadTeamSUnloadUsersView() {
        return (HhUnloadTeamSUnloadUsersViewImpl) findViewObject("HhUnloadTeamSUnloadUsersView");
    }


    /**
     * Container's getter for HhPrintShipLabelSWorkView.
     * @return HhPrintShipLabelSWorkView
     */
    public HhPrintShipLabelSWorkViewImpl getHhPrintShipLabelSWorkView() {
        return (HhPrintShipLabelSWorkViewImpl) findViewObject("HhPrintShipLabelSWorkView");
    }


    /**
     * Container's getter for HhPrintShipLabelSMainView.
     * @return HhPrintShipLabelSMainView
     */
    public HhPrintShipLabelSMainViewImpl getHhPrintShipLabelSMainView() {
        return (HhPrintShipLabelSMainViewImpl) findViewObject("HhPrintShipLabelSMainView");
    }

    /**
     * Container's getter for HhPrintShipLabelSWorkLocalView.
     * @return HhPrintShipLabelSWorkLocalView
     */
    public HhPrintShipLabelSWorkLocalViewImpl getHhPrintShipLabelSWorkLocalView() {
        return (HhPrintShipLabelSWorkLocalViewImpl) findViewObject("HhPrintShipLabelSWorkLocalView");
    }

    /**
     * Container's getter for HhPrintShipLabelSReprintAuthView.
     * @return HhPrintShipLabelSReprintAuthView
     */
    public HhPrintShipLabelSReprintAuthViewImpl getHhPrintShipLabelSReprintAuthView() {
        return (HhPrintShipLabelSReprintAuthViewImpl) findViewObject("HhPrintShipLabelSReprintAuthView");
    }


    /**
     * Helper Method to Simplify Invoking Stored Procedures.
     * @param stmt
     * @param bindVars
     */
    public void callStoredProcedure(String stmt, Object... bindVars) {
        DBUtils.callStoredProcedure(getDBTransaction(), stmt, bindVars);
    }

    public void callStoredProcedureWithNulls(String stmt, Object... bindVars) {
        DBUtils.callStoredProcedureWithNulls(getDBTransaction(), stmt, bindVars);
    }

    /**
     * Helper Method to Simplify Invoking Stored Functions.
     * @param sqlReturnType
     * @param stmt
     * @param bindVars
     * @return
     */
    public Object callStoredFunction(int sqlReturnType, String stmt, Object... bindVars) {
        return DBUtils.callStoredFunction(getDBTransaction(), sqlReturnType, stmt, bindVars);
    }

    public String gScp(String facility, String name) {
        return (String) callStoredFunction(Types.VARCHAR, "g_scp", facility, name);
    }

    public void callSetAhlInfo(String facilityId, String formName) {
        DBUtils.callStoredProcedure(this.getDBTransaction(), "AHL.SET_AHL_INFO", facilityId, formName);
    }

    public void setGlobalVariablesInvItemTroubleSBean() {
    }

    public void setGlobalVariablesConveyorCutOff() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        gVViewImplRowImpl.setGlobalUserId((String) currentRow.getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");

        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);

    }

    /**
     * Container's getter for RdmCommonAppModule1.
     * @return RdmCommonAppModule1
     */
    public RdmCommonAppModuleImpl getRdmCommonAppModule() {
        return (RdmCommonAppModuleImpl) findApplicationModule("RdmCommonAppModule");
    }

    /**
     * Container's getter for CheckTrailerStatus1.
     * @return CheckTrailerStatus1
     */
    public CheckTrailerStatusViewImpl getCheckTrailerStatus() {
        return (CheckTrailerStatusViewImpl) findViewObject("CheckTrailerStatus");
    }

    /**
     * Container's getter for TeamCartonView1.
     * @return TeamCartonView1
     */
    public TeamCartonViewImpl getTeamCartonView() {
        return (TeamCartonViewImpl) findViewObject("TeamCartonView");
    }

    public void setGlobalVariablesPrintShipLabel() {
        getRdmCommonAppModule().initCurrentUserIfNeed();
        GlobalVariablesViewImpl gVViewImpl = this.getGlobalVariablesView();
        GlobalVariablesViewRowImpl gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.getCurrentRow();
        if (gVViewImplRowImpl == null) {
            gVViewImplRowImpl = (GlobalVariablesViewRowImpl) gVViewImpl.createRow();
            gVViewImpl.insertRow(gVViewImplRowImpl);
        }
        Row currentRow = this.getGetLoginDateView().getCurrentRow();
        String facilityId = (String) currentRow.getAttribute("FacilityId");
        String languageCode = (String) currentRow.getAttribute("LanguageCode");
        gVViewImplRowImpl.setGlobalUserId((String) currentRow.getAttribute("UserId"));
        gVViewImplRowImpl.setGlobalHeader("UNDEFINED");
        gVViewImplRowImpl.setGlobalFacilityId(facilityId);
        gVViewImplRowImpl.setGlobalUserPrivilege("1");
        gVViewImplRowImpl.setGlobalDevice("S");
        gVViewImplRowImpl.setGlobalLanguageCode(languageCode);
        gVViewImplRowImpl.setGlobalMsgCode("");
        gVViewImplRowImpl.setGlobalResponse("");
        gVViewImpl.insertRow(gVViewImplRowImpl);
        gVViewImpl.setCurrentRow(gVViewImplRowImpl);
        //DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", facilityId, languageCode);
        //#3874: we do this in the afterConnect method, all other calls of SET_MSG_VARS should become obsolete 
    }

    /**
     * Container's getter for QueueView1.
     * @return QueueView1
     */
    public QueueViewImpl getQueueView() {
        return (QueueViewImpl) findViewObject("QueueView");
    }


    /**
     * Container's getter for dmsTeamView1.
     * @return dmsTeamView1
     */
    public DmsTeamViewImpl getDmsTeamView() {
        return (DmsTeamViewImpl) findViewObject("DmsTeamView");
    }

    /**
     * Container's getter for UsersView1.
     * @return UsersView1
     */
    public TeamUsersViewImpl getTeamUsersView() {
        return (TeamUsersViewImpl) findViewObject("TeamUsersView");
    }

    /**
     * Container's getter for DmsTeamUserView1.
     * @return DmsTeamUserView1
     */
    public DmsTeamUserViewImpl getDmsTeamUserView() {
        return (DmsTeamUserViewImpl) findViewObject("DmsTeamUserView");
    }


}
