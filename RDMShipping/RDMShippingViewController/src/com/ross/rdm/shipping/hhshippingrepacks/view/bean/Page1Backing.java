package com.ross.rdm.shipping.hhshippingrepacks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.hhshippingrepacks.model.views.HhShippingRepackSBRepackViewRowImpl;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {

    private final static String B_REPACK_ITERATOR = "HhShippingRepackSBRepackViewIterator";
    private final static String HH_SHIPPING_REPACK_FLOW_BEAN = "HhShippingRepackSBean";
    private final static String FROM_CONTAINER_ID_ATTR = "FromContainerId";
    private final static String TO_CONTAINER_ID_ATTR = "ToContainerId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichPanelFormLayout shippingRepackPanel;
    private RichLink f3Link;
    private RichLink f8Link;

    //Inputs
    private RichInputText fromContainerId;
    private RichIcon fromContainerIdIcon;
    private RichInputText toContainerId;
    private RichIcon toContainerIdIcon;

    //Outputs
    private RichInputText destId;
    private RichInputText locationId;
    private RichInputText containerStatus;

    public Page1Backing() {

    }

    public void onChangedFromContainerId(ValueChangeEvent vce) {
        boolean validFromContainerId = false;

        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredValue = ((String) vce.getNewValue()).toUpperCase();
            validFromContainerId = callPValFromContainer(enteredValue);
            if (validFromContainerId) {
                ADFUtils.setBoundAttributeValue("FromContainerId", enteredValue);
                setFocusToContainerId(true);
                hideMessagesPanel();
                this.refreshContentOfUIComponent(this.getFromContainerIdIcon());
                this.refreshContentOfUIComponent(this.getFromContainerId());
                removeErrorStyleToComponent(this.getFromContainerId());
                getFromContainerIdIcon().setName(REQ_ICON);
            }
        } else {
            getFromContainerIdIcon().setName(ERR_ICON);
            refreshContentOfUIComponent(this.getFromContainerIdIcon());
            refreshContentOfUIComponent(this.getFromContainerId());
            addErrorStyleToComponent(this.getFromContainerId());
            showMessagesPanel(ERROR, this.getMessage("INV_CID", ERROR, null, this.getLangCodeAttrValue()));
        }
        getHhShippingRepackSPageFlowBean().setIsFromContainerIdValidated(validFromContainerId);
    }
    
    private boolean callPValFromContainer(String fromContainerId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPValFromContainer");
        oper.getParamsMap().put("pFromContainerId", fromContainerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() >= 2) {
                    if (ERROR.equals(responseList.get(0))) {
                        showMessagesPanel(ERROR, responseList.get(1));
                        getFromContainerIdIcon().setName(ERR_ICON);
                        refreshContentOfUIComponent(this.getFromContainerIdIcon());
                        refreshContentOfUIComponent(this.getFromContainerId());
                        addErrorStyleToComponent(this.getFromContainerId());
                        selectTextOnUIComponent(this.getFromContainerId());
                        return Boolean.FALSE;
                    }
                }
            }
        }
        return Boolean.TRUE;
    }
    
    
    public void onChangedToContainerId(ValueChangeEvent vce) {
        boolean validToContainerId = false;

        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredValue = ((String) vce.getNewValue()).toUpperCase();
            validToContainerId = callPValToContainer(enteredValue);
            if (validToContainerId) {
                ADFUtils.setBoundAttributeValue("ToContainerId", enteredValue);
                setFocusFromContainerId();
                clearFields(true);
                this.refreshContentOfUIComponent(this.getToContainerIdIcon());
                this.refreshContentOfUIComponent(this.getToContainerId());
                removeErrorStyleToComponent(this.getToContainerId());
                getToContainerIdIcon().setName(REQ_ICON);
            }
        } else {
            getToContainerIdIcon().setName(ERR_ICON);
            refreshContentOfUIComponent(this.getToContainerIdIcon());
            refreshContentOfUIComponent(this.getToContainerId());
            addErrorStyleToComponent(this.getToContainerId());
            showMessagesPanel(ERROR, this.getMessage("PARTIAL_ENTRY", ERROR, null, this.getLangCodeAttrValue()));
        }
        getHhShippingRepackSPageFlowBean().setIsToContainerIdValidated(validToContainerId);
    }
    
    private boolean callPValToContainer(String toContainerId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPValToContainer");
        oper.getParamsMap().put("pToContainerId", toContainerId);
        oper.getParamsMap().put("pFromContainerId", getFromContainerIdAttrValue());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() >= 1) {
                    if (ERROR.equals(responseList.get(0))) {
                        showMessagesPanel(ERROR, responseList.get(1));
                        getToContainerIdIcon().setName(ERR_ICON);
                        refreshContentOfUIComponent(this.getToContainerIdIcon());
                        refreshContentOfUIComponent(this.getToContainerId());
                        addErrorStyleToComponent(this.getToContainerId());
                        selectTextOnUIComponent(this.getToContainerId());
                        return Boolean.FALSE;
                    }
                    if (responseList.contains(YES)) {
                        showMessagesPanel(MSGTYPE_M, this.getMessage("SUCCESS_OPER", MSGTYPE_M, null, this.getLangCodeAttrValue()));
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getHhShippingRepackSPageFlowBean().getIsFocusOn();

        if (clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                    clearFields(true);
                    setFocusFromContainerId();
                    removeErrorStyleToComponent(getFromContainerId());
                    removeErrorStyleToComponent(getToContainerId());
                    getFromContainerIdIcon().setName(REQ_ICON);
                    getToContainerIdIcon().setName(REQ_ICON);
                    refreshContentOfUIComponent(this.getFromContainerIdIcon());
                    refreshContentOfUIComponent(this.getToContainerIdIcon());
                    hideMessagesPanel();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (submittedValue != null) {
                    if ("FromContainerId".equals(currentFocus)) {
                        onChangedFromContainerId(new ValueChangeEvent(this.getFromContainerId(), null, submittedValue));
                    } else if ("ToContainerId".equals(currentFocus)) {
                        onChangedToContainerId(new ValueChangeEvent(this.getToContainerId(), null, submittedValue));
                    }
                }
            }
            this.refreshContentOfUIComponent(this.getShippingRepackPanel());
        }
    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            clearFields(true);
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F8".equalsIgnoreCase(key)) {
            clearFields(true);
            setFocusFromContainerId();
            removeErrorStyleToComponent(getFromContainerId());
            removeErrorStyleToComponent(getToContainerId());
            getFromContainerIdIcon().setName(REQ_ICON);
            getToContainerIdIcon().setName(REQ_ICON);
            refreshContentOfUIComponent(this.getFromContainerIdIcon());
            refreshContentOfUIComponent(this.getToContainerIdIcon());
            hideMessagesPanel();
        }
    }

    private void clearFields(boolean clearAll) {
        _logger.info("clearFields() Start");
        this.clearCurrentRow(clearAll);
        if (clearAll) {
            this.getFromContainerId().resetValue();
            this.getToContainerId().resetValue();
            this.refreshContentOfUIComponent(this.getFromContainerId());
            this.refreshContentOfUIComponent(this.getToContainerId());
        }
        this.getDestId().resetValue();
        this.getLocationId().resetValue();
        this.getContainerStatus().resetValue();
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow(boolean clearAll) {
        HhShippingRepackSBRepackViewRowImpl hhRowImpl = this.getHhShippingRepackSBRepackViewRow();
        if (hhRowImpl != null) {
            if (clearAll) {
                hhRowImpl.setFromContainerId(null);
                hhRowImpl.setToContainerId(null);
            }
            hhRowImpl.setDestId(null);
            hhRowImpl.setLocationId(null);
            hhRowImpl.setContainerStatus(null);
        }
    }

    public void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAllMessagesPanel());
        _logger.info("hideMessagesPanel() End");
    }

    private String getFromContainerIdAttrValue() {
        if (getHhShippingRepackSPageFlowBean().getIsFromContainerIdValidated()) {
            return (String) ADFUtils.getBoundAttributeValue(FROM_CONTAINER_ID_ATTR);
        }
        return null;
    }

    private String getToContainerIdAttrValue() {
        if (getHhShippingRepackSPageFlowBean().getIsToContainerIdValidated()) {
            return (String) ADFUtils.getBoundAttributeValue(TO_CONTAINER_ID_ATTR);
        }
        return null;
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f8ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_shipping_repack_s_B_REPACK_F8,
                                                FORM_NAME_hh_shipping_repack_s)) {
            this.trKeyIn("F8");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private HhShippingRepackSBRepackViewRowImpl getHhShippingRepackSBRepackViewRow() {
        return (HhShippingRepackSBRepackViewRowImpl) ADFUtils.findIterator(B_REPACK_ITERATOR).getCurrentRow();
    }

    private HhShippingRepackSBean getHhShippingRepackSPageFlowBean() {
        return ((HhShippingRepackSBean) this.getPageFlowBean(HH_SHIPPING_REPACK_FLOW_BEAN));
    }

    public void setShippingRepackPanel(RichPanelFormLayout shippingRepackPanel) {
        this.shippingRepackPanel = shippingRepackPanel;
    }

    public RichPanelFormLayout getShippingRepackPanel() {
        return shippingRepackPanel;
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setFromContainerId(RichInputText fromContainerId) {
        this.fromContainerId = fromContainerId;
    }

    public RichInputText getFromContainerId() {
        return fromContainerId;
    }

    public void setFromContainerIdIcon(RichIcon fromContainerIdIcon) {
        this.fromContainerIdIcon = fromContainerIdIcon;
    }

    public RichIcon getFromContainerIdIcon() {
        return fromContainerIdIcon;
    }

    public void setToContainerId(RichInputText toContainerId) {
        this.toContainerId = toContainerId;
    }

    public RichInputText getToContainerId() {
        return toContainerId;
    }

    public void setToContainerIdIcon(RichIcon toContainerIdIcon) {
        this.toContainerIdIcon = toContainerIdIcon;
    }

    public RichIcon getToContainerIdIcon() {
        return toContainerIdIcon;
    }

    public void setDestId(RichInputText destId) {
        this.destId = destId;
    }

    public RichInputText getDestId() {
        return destId;
    }

    public void setLocationId(RichInputText locationId) {
        this.locationId = locationId;
    }

    public RichInputText getLocationId() {
        return locationId;
    }

    public void setContainerStatus(RichInputText containerStatus) {
        this.containerStatus = containerStatus;
    }

    public RichInputText getContainerStatus() {
        return containerStatus;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }
    
    private void setFocusFromContainerId() {
        this.showMessagesPanel(INFO, "Enter From Container");
        this.getHhShippingRepackSPageFlowBean().setIsFocusOn(FROM_CONTAINER_ID_ATTR);
        this.setFocusOnUIComponent(this.getFromContainerId());
    }
    
    private void setFocusToContainerId(boolean showHint) {
        if (showHint) {
            this.showMessagesPanel(INFO, "Enter To Container");
        }
        this.getHhShippingRepackSPageFlowBean().setIsFocusOn(TO_CONTAINER_ID_ATTR);
        this.setFocusOnUIComponent(this.getToContainerId());
    }
    
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if(StringUtils.isEmpty((String) this.getErrorWarnInfoMessage().getValue())){
            this.setFocusAndEnabledCurrentFocusField();
        }
    }
    
    private void setFocusAndEnabledCurrentFocusField(){
        HhShippingRepackSBean pfBean = this.getHhShippingRepackSPageFlowBean();
        if (FROM_CONTAINER_ID_ATTR.equals(pfBean.getIsFocusOn())) {
            this.setFocusFromContainerId();
        } else if (TO_CONTAINER_ID_ATTR.equals(pfBean.getIsFocusOn())) {
            this.setFocusToContainerId(true);
        }
    }
}
