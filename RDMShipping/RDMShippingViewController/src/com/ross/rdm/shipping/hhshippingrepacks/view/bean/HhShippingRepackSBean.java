package com.ross.rdm.shipping.hhshippingrepacks.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhShippingRepackSBean  {
  private static ADFLogger _logger = ADFLogger.createADFLogger(HhShippingRepackSBean.class);

    private Boolean isFromContainerIdValidated;
    private Boolean isToContainerIdValidated;
    private String isFocusOn;
    private Boolean isError;

    public void initTaskFlow() {
        initTaskFlowLoadContainer();
        setIsFocusOn("FromContainerId");
        setIsFromContainerIdValidated(false);
        setIsToContainerIdValidated(false);
    }

    private void initTaskFlowLoadContainer() {
        ADFUtils.findOperation("initTaskFlowShippingRepack").execute();
    }

    public void setIsFromContainerIdValidated(Boolean isFromContainerIdValidated) {
        this.isFromContainerIdValidated = isFromContainerIdValidated;
    }

    public Boolean getIsFromContainerIdValidated() {
        return isFromContainerIdValidated;
    }

    public void setIsToContainerIdValidated(Boolean isToContainerIdValidated) {
        this.isToContainerIdValidated = isToContainerIdValidated;
    }

    public Boolean getIsToContainerIdValidated() {
        return isToContainerIdValidated;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }
}
