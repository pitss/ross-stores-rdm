package com.ross.rdm.shipping.hhprintshiplabels.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.List;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhPrintShipLabelSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhPrintShipLabelSBean.class);
    private String isFocusOn;
    private boolean authBuild;
    private String lWhId;
    private Integer layoutCidLength;
    private String scpLabelSet;
    private String scpLayout;
    private String vRoutePrint;
    private String finalLocationId;
    private Integer reprintLblExists;
    private boolean regionLoaded;
    private String gDcInfoMessage = null;
    private final static String setGlobalVariablesPrintShipLabel = "setGlobalVariablesPrintShipLabel";
    private final static String setWorkVariablesPrintShipLabel = "setWorkVariablesPrintShipLabel";
    private final static String setWorkLocalVariablesPrintShipLabel = "setWorkLocalVariablesPrintShipLabel";
    private final static String setMainVariablesPrintShipLabel = "setMainVariablesPrintShipLabel";
    private final static String createInsertReprintAuthRow = "createInsertReprintAuthRow";
    private final static String executeQueryWithParamsPrintShipLabel = "executeQueryWithParams";
    public void initTaskFlow() {
        _logger.info("Print ship label page initialization begun");
        this.initGlobalVariablesPrintShipLabel();
        this.initWorkVariablesPrintShipLabel();
        this.initWorkLocalVariablesPrintShipLabel();
        this.initMainVariablesPrintShipLabel();
        this.initQueuePrintShipLabel();
        this.createReprintAuthRow();
        this.setIsFocusOn("QueueField");
        this.setAuthBuild(false);
        _logger.info("Print ship label page initialization completed");
    }

    private void initGlobalVariablesPrintShipLabel() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(setGlobalVariablesPrintShipLabel);
        oper.execute();
    }

    private void initWorkVariablesPrintShipLabel() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(setWorkVariablesPrintShipLabel);
        oper.execute();
    }

    private void initWorkLocalVariablesPrintShipLabel() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(setWorkLocalVariablesPrintShipLabel);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List<String> codesList = (List<String>) oper.getResult();
            if (codesList != null && codesList.size() > 0)
                this.setGDcInfoMessage(codesList.get(0));


        }
    }

    private void initMainVariablesPrintShipLabel() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(setMainVariablesPrintShipLabel);
        oper.execute();
    }

    private void initQueuePrintShipLabel() {
        
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");        
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(executeQueryWithParamsPrintShipLabel);
        oper.getParamsMap().put("facilityId", facilityId);
        oper.execute();
    }

    private void createReprintAuthRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(createInsertReprintAuthRow);
        oper.execute();
    }


    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setAuthBuild(boolean authBuild) {
        this.authBuild = authBuild;
    }

    public boolean isAuthBuild() {
        return authBuild;
    }

    public void setLWhId(String lWhId) {
        this.lWhId = lWhId;
    }

    public String getLWhId() {
        return lWhId;
    }

    public void setLayoutCidLength(Integer layoutCidLength) {
        this.layoutCidLength = layoutCidLength;
    }

    public Integer getLayoutCidLength() {
        return layoutCidLength;
    }

    public void setScpLabelSet(String scpLabelSet) {
        this.scpLabelSet = scpLabelSet;
    }

    public String getScpLabelSet() {
        return scpLabelSet;
    }

    public void setScpLayout(String scpLayout) {
        this.scpLayout = scpLayout;
    }

    public String getScpLayout() {
        return scpLayout;
    }

    public void setVRoutePrint(String vRoutePrint) {
        this.vRoutePrint = vRoutePrint;
    }

    public String getVRoutePrint() {
        return vRoutePrint;
    }

    public void setFinalLocationId(String finalLocationId) {
        this.finalLocationId = finalLocationId;
    }

    public String getFinalLocationId() {
        return finalLocationId;
    }

    public void setReprintLblExists(Integer reprintLblExists) {
        this.reprintLblExists = reprintLblExists;
    }

    public Integer getReprintLblExists() {
        return reprintLblExists;
    }

    public void setRegionLoaded(boolean regionLoaded) {
        this.regionLoaded = regionLoaded;
    }

    public boolean isRegionLoaded() {
        return regionLoaded;
    }

    public void setGDcInfoMessage(String gDcInfoMessage) {
        this.gDcInfoMessage = gDcInfoMessage;
    }

    public String getGDcInfoMessage() {
        return gDcInfoMessage;
    }

}
