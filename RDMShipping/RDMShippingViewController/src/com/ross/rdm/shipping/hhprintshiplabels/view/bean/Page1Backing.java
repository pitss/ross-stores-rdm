package com.ross.rdm.shipping.hhprintshiplabels.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {
    private final static String PAGE_FLOW_BEAN = "HhPrintShipLabelSBean";
    private RichPanelFormLayout panelForm;
    private RichPanelGroupLayout allMessagesPanel;
    private RichOutputText errorWarnInfoMessage;
    private RichInputText queue;
    private RichInputText containerId;
    private RichIcon queueIcon;
    private RichIcon containerIdIcon;
    private RichLink f3Link;
    private RichLink f6Link;
    private RichOutputText labelsPrintedPopupOutputtext;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichIcon errorWarnInfoIcon;
    
    private RichPopup queueLovPopup;
    
    private RichPopup okayPopup;
    private RichLink okayLink;
    private RichIcon okayIcon;
    private RichOutputText okayText;
    
    private RichPopup labelsPrintedPopup;



    public Page1Backing() {

    }


    /******************************************************************************************
     *                                  Validators
     ******************************************************************************************/
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        HhPrintShipLabelSBean bean = this.getHhPrintShipLabelSBeanPageFlowBean();
        if (bean != null) {
            String gDcInfo = this.getHhPrintShipLabelSBeanPageFlowBean().getGDcInfoMessage();
            if (null != gDcInfo)
                this.showErrorPanel(this.getMessage(DMS_ERROR), ERROR);
        }
        this.setFocusOnActiveComp();
        if (this.getHhPrintShipLabelSBeanPageFlowBean().isAuthBuild()) {
            ADFUtils.setBoundAttributeValue("ContainerId", null);
            this.getHhPrintShipLabelSBeanPageFlowBean().setAuthBuild(false);
            this.removeErrorOfAllFields();
            this.showErrorPanel("CONTAINER PROCESSED", MSGTYPE_M);
            this.setFocusContainerId();
            if (this.callPrintReprints()) {
                this.callPrintLabels();
            }
        }
        _logger.info("onRegionLoad End");

    }

    private void setFocusOnActiveComp() {
        HhPrintShipLabelSBean bean = this.getHhPrintShipLabelSBeanPageFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            switch (focus) {
            case "QueueField":
                this.setFocusQueue();
                break;
            case "ContainerIdField":
                this.setFocusContainerId();
                break;
            default:
                break;
            }
        }
    }

    public void onChangedQueue(ValueChangeEvent vce) {
        _logger.info("onChangedQueue() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String queue = ((String) vce.getNewValue());
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVQueue");
            Map map = oper.getParamsMap();
            map.put("queue", queue);
            oper.execute();
            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    List<Object> codeList = (List<Object>) oper.getResult();
                    if (codeList.size() == 2) {
                        this.showErrorPanel((String) codeList.get(1), (String) codeList.get(0));
                        this.setErrorUi(this.getQueueIcon(), this.getQueue());
                        this.setFocusQueue();
                        _logger.info("onChangedQueue() End");
                        return;
                    }
                    this.getQueueIcon().setName(REQ_ICON);
                    this.refreshContentOfUIComponent(this.getQueueIcon());
                    this.getQueue().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getQueue());
                    this.hideMessagesPanel();
                    this.removeErrorOfAllFields();
                    this.setFocusContainerId();
                    this.refreshContentOfUIComponent(this.getPanelForm());
                }
            }
            _logger.info("onChangedQueue() End");
        }
    }


    private List callVContainer1(String cId) {
        List<String> codeList = null;
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVContainer1");
        Map map = oper.getParamsMap();
        map.put("cid", cId);
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                codeList = (List<String>) oper.getResult();
                if(codeList.size() > 0 && (codeList.get(0).equalsIgnoreCase(ERROR) || codeList.get(0).equalsIgnoreCase(MSGTYPE_M))){
                    this.showErrorPanel(codeList.get(1), ERROR);
                        if(codeList.get(2) != null && codeList.get(2).equals("clearField")){
                            this.getContainerId().resetValue();
                        }
                    this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
                    this.setFocusContainerId();
                    _logger.info("onChangedContainerId() End");
                    return codeList;
                }
            }
        }
        _logger.info("onChangedContainerId() End");
        return codeList;
    }

    private List callScp() {
        List<String> codeList = null;
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callScp");
        oper.execute();
        if (oper.getErrors().isEmpty())
            if (null != oper.getResult())
                codeList = (List<String>) oper.getResult();
        return codeList;
    }

    private void callGetRoutePrintFlag() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callGetRoutePrintFlag");
        Map map3 = oper.getParamsMap();
        map3.put("lWhId", this.getLWhId());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                this.setVRoutePrint((String) oper.getResult());
            }
        }
    }

    private void callReprintLblExists() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callReprintLblExists");
        Map map = oper.getParamsMap();
        map.put("layoutCidLength", this.getLayoutCidLength());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                this.setReprintLblExists((Integer) oper.getResult());
            }
        }
    }

    private void callCheckUserAuthWp() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callCheckUserAuthWp");
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                Integer authVal = (Integer) oper.getResult();
                if (authVal != null && authVal.equals(0)) {
                    this.removeErrorOfAllFields();
                    this.getContainerId().setDisabled(true);
                    this.refreshContentOfUIComponent(this.getContainerId());
                    this.getLabelsPrintedPopupOutputtext().setValue(this.getMessage("LABELS_PRINTED"));
                    this.getLabelsPrintedPopup().show(new RichPopup.PopupHints());
                } else if (authVal != null && authVal.equals(1))
                    openOkayPopup();
            }
        }
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.hideMessagesPanel();
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String cId = ((String) vce.getNewValue());
            List codeList = this.callVContainer1(cId);
            if (codeList != null && codeList.size() > 0 && 
                 (codeList.get(0).equals(ERROR) || codeList.get(0).equals(MSGTYPE_M) ) ) {
                
                
                 }
            else {
            //if(codeList.size()>0 &&  !ERROR.equals(codeList.get(0))){
                this.setLayoutCidLength(null);
                if (codeList != null && codeList.size() > 1) {
                        this.getOkayText().setValue(codeList.get(1));    
                    }
                
                codeList = this.callScp();
                if (codeList != null && codeList.size() > 1) {
                    this.setScpLabelSet((String) codeList.get(0));
                    this.setScpLayout((String) codeList.get(1));
                    if (("NEW").equalsIgnoreCase(this.getScpLayout()))
                        this.setLayoutCidLength(16);
                    else
                        this.setLayoutCidLength(10);
                }
                this.callGetRoutePrintFlag();
                this.callReprintLblExists();
                if (null != this.getReprintLblExists() && this.getReprintLblExists().equals(1)){
                    this.callCheckUserAuthWp();
                }
                else
                    openOkayPopup();
                
            }
        }
            _logger.info("onChangedContainerId() End");
        }

    private void vCid(String mainMsg) {
        _logger.info("vCid() Start");
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVContainer2");
        Map map = oper.getParamsMap();
        map.put("mainMsg", mainMsg);
        map.put("scpLabelSet", this.getScpLabelSet());
        map.put("scpLayout", this.getScpLayout());
        map.put("layoutCidLength", this.getLayoutCidLength());
        map.put("vRoutePrint", this.getVRoutePrint());
        map.put("reprintLblExists", this.getReprintLblExists());
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            List<Object> codeList = (List<Object>) oper.getResult();
            if (null != codeList) {
                BigDecimal goItem = null;
                BigDecimal builtIns = null;
                if (codeList.size() == 1) {
                    this.showErrorPanel((String) codeList.get(0), ERROR);
                    this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
                    this.setFocusContainerId();
                } else if (codeList.size() == 2) {
                    goItem = (BigDecimal) codeList.get(0);
                    builtIns = (BigDecimal) codeList.get(1);
                    if (goItem != null && goItem.equals(BigDecimal.ONE)) {
                        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("userId");
                        this.getHhPrintShipLabelSBeanPageFlowBean().setRegionLoaded(false);
                        ADFUtils.invokeAction("ReprintAuthPage");
                    } else if (goItem != null && goItem.equals(new BigDecimal(2))) {
                        this.setFocusContainerId();
                    }
                }
                if (null != builtIns && builtIns.equals(BigDecimal.ONE)) {
                    ADFUtils.setBoundAttributeValue("ContainerId", null);
                    this.getContainerId().resetValue();
                    this.setFocusContainerId();
                    this.removeErrorOfAllFields();
                    this.showErrorPanel("CONTAINER PROCESSED", MSGTYPE_M);
                    if (this.callPrintReprints()) {
                        this.callPrintLabels();
                    }
                }
            }
        }
    }

    private boolean callPrintReprints() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPrintReprints");
        oper.execute();
        return oper.getErrors() == null || oper.getErrors().isEmpty();
    }

    private void callPrintLabels() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("printLabels");
        oper.execute();
        List<String> result = (List<String>) oper.getResult();
        if (result != null && result.size() > 0 && ERROR.equals(result.get(0))) {
            this.showErrorPanel(result.get(1), result.get(0));
            this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
            this.setFocusContainerId();
            _logger.info("vCid() End");
            return;
        }

    }
    
    public void openOkayPopup(){
        this.removeErrorOfAllFields();
        this.hideMessagesPanel();
        if (this.getOkayText().getValue() != null ) { //no msg text means no popup to show
            this.getOkayPopup().show(new RichPopup.PopupHints());
            setFocusOkayPopup();           
        } else
            this.vCid(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    public void performKeyOkayPopup(ClientEvent clientEvent) {
            if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
                Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
                if (F1_KEY_CODE.equals(keyPressed)) {
                    okayPopupActionListener();
                        getOkayPopup().hide();
                    }
                }
    }
    
    public void okayPopupActionListener(){
        getOkayPopup().hide();
        this.vCid(null);
    }
    
    public void okayPopupActionListener(ActionEvent actionEvent){
       okayPopupActionListener();
    }

    /******************************************************************************************
     *                                  TrKeyIn and performKey
     ******************************************************************************************/


    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        if ("f3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F6".equalsIgnoreCase(key)) {
            //ADFUtils.findOperation("executeQueryWithParams").execute(); 
            //NOT here! initTaskFlow does it and even sets the bind variable (Fix #3889)
            this.disablesAll();
            this.getQueueLovPopup().show(new RichPopup.PopupHints());
            setFocusQueueLovPopup();
            this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        }
    }

    private void disablesAll() {
        this.hideMessagesPanel();
        this.removeErrorOfAllFields();
        this.getContainerId().setDisabled(true);
        this.getQueue().setDisabled(true);
        this.refreshContentOfUIComponent(this.getQueue());
        this.refreshContentOfUIComponent(this.getContainerId());

    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getf3Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                RichInputText field = (RichInputText) clientEvent.getComponent();
                String currentFieldId = field.getId();
                if (StringUtils.isNotEmpty(submittedValue)) {
                    if ("con13".equals(currentFieldId)) {
                        this.onChangedQueue(new ValueChangeEvent(this.getQueue(), null, submittedValue));
                    } else if ("con1".equals(currentFieldId)) {
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    }
                }
            }
        }
    }


    /******************************************************************************************
     *                                  Popup window methods
     ******************************************************************************************/


    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getLabelsPrintedPopup().hide();
        this.setFocusContainerId();
        this.vCid(YES);
    }

    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getLabelsPrintedPopup().hide();
        this.setFocusContainerId();
        this.vCid(NO);
    }

    public void performKeySelectQueue(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                if (StringUtils.isNotEmpty(submittedValue)) {
                    ADFUtils.setBoundAttributeValue("Queue", submittedValue);
                }
                this.refreshContentOfUIComponent(this.getQueue());
                this.getQueueLovPopup().hide();
                this.removeErrorOfAllFields();
                this.setFocusContainerId();
                this.refreshContentOfUIComponent(this.getPanelForm());
            }
        }
    }


    /******************************************************************************************
     *                                  Utility methods
     ******************************************************************************************/


    private void setErrorUi(RichIcon icon, RichInputText field) {
        icon.setName(ERR_ICON);
        this.refreshContentOfUIComponent(icon);
        this.addErrorStyleToComponent(field);
        this.selectTextOnUIComponent(field);
        this.refreshContentOfUIComponent(field);
    }

    private void showErrorPanel(String errorName, String errorCode) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.getErrorWarnInfoMessage().setValue(errorName);
        if (errorCode.equalsIgnoreCase(INFO)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else if (errorCode.equalsIgnoreCase(WARN)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (errorCode.equalsIgnoreCase(MSGTYPE_M)) {
            this.getErrorWarnInfoIcon().setName(LOGO_ICON);
        } else {
            this.getErrorWarnInfoIcon().setName(ERR_ICON);
        }
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f6ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(OPTION_NAME_hh_print_ship_label_s_MAIN_F6,
                                                FORM_NAME_hh_print_ship_label_s)) {
            this.trKeyIn("F6");
        } else {
            this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
        }
    }

    private void setFocusQueue() {
        this.getQueue().setDisabled(false);
        this.refreshContentOfUIComponent(this.getQueue());
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("QueueField");
        this.setFocusOnUIComponent(this.getQueue());
    }

    private void setFocusContainerId() {
        this.getContainerId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getContainerId());
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusQueueLovPopup() {
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("QueueLovPopup");
    }

    private void removeErrorOfAllFields() {
        this.removeErrorStyleToComponent(getQueue());
        this.removeErrorStyleToComponent(getContainerId());
        this.getQueueIcon().setName(REQ_ICON);
        this.getContainerIdIcon().setName(REQ_ICON);
        this.refreshContentOfUIComponent(getQueueIcon());
        this.refreshContentOfUIComponent(getContainerIdIcon());
    }


    /******************************************************************************************
     *                                  Getters and Setters
     ******************************************************************************************/
    public void getwhId(){
        ADFUtils.getBoundAttributeValue("Lwh");
    }
    
    public void getfinalLocationId(){
        ADFUtils.getBoundAttributeValue("FinalLocation");
    }
    
    private void setFocusOkayPopup() {
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("OkayPopup");
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }


    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setQueue(RichInputText queue) {
        this.queue = queue;
    }

    public RichInputText getQueue() {
        return queue;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setf3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getf3Link() {
        return f3Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setQueueIcon(RichIcon queueIcon) {
        this.queueIcon = queueIcon;
    }

    public RichIcon getQueueIcon() {
        return queueIcon;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    private HhPrintShipLabelSBean getHhPrintShipLabelSBeanPageFlowBean() {
        return ((HhPrintShipLabelSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }

    public void setPanelForm(RichPanelFormLayout panelForm) {
        this.panelForm = panelForm;
    }

    public RichPanelFormLayout getPanelForm() {
        return panelForm;
    }

    public void setLabelsPrintedPopup(RichPopup labelsPrintedPopup) {
        this.labelsPrintedPopup = labelsPrintedPopup;
    }

    public RichPopup getLabelsPrintedPopup() {
        return labelsPrintedPopup;
    }

    public void setLabelsPrintedPopupOutputtext(RichOutputText labelsPrintedOutputtext) {
        this.labelsPrintedPopupOutputtext = labelsPrintedOutputtext;
    }

    public RichOutputText getLabelsPrintedPopupOutputtext() {
        return labelsPrintedPopupOutputtext;
    }

    public void setLWhId(String lWhId) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setLWhId(lWhId);
    }

    public String getLWhId() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getLWhId();
    }

    public void setLayoutCidLength(Integer layoutCidLength) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setLayoutCidLength(layoutCidLength);
    }

    public Integer getLayoutCidLength() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getLayoutCidLength();
    }

    public void setScpLabelSet(String scpLabelSet) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setScpLabelSet(scpLabelSet);
    }

    public String getScpLabelSet() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getScpLabelSet();
    }

    public void setScpLayout(String scpLayout) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setScpLayout(scpLayout);
    }

    public String getScpLayout() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getScpLayout();
    }

    public void setVRoutePrint(String vRoutePrint) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setVRoutePrint(vRoutePrint);
    }

    public String getVRoutePrint() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getVRoutePrint();
    }

    public void setReprintLblExists(Integer reprintLblExists) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setReprintLblExists(reprintLblExists);
    }

    public Integer getReprintLblExists() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getReprintLblExists();
    }

    public void setFinalLocationId(String finalLocationId) {
        this.getHhPrintShipLabelSBeanPageFlowBean().setFinalLocationId(finalLocationId);
    }

    public String getFinalLocationId() {
        return this.getHhPrintShipLabelSBeanPageFlowBean().getFinalLocationId();
    }

    public void setQueueLovPopup(RichPopup queueLovPopup) {
        this.queueLovPopup = queueLovPopup;
    }

    public RichPopup getQueueLovPopup() {
        return queueLovPopup;
    }


    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }


    public void setOkayPopup(RichPopup okayPopup) {
        this.okayPopup = okayPopup;
    }

    public RichPopup getOkayPopup() {
        return okayPopup;
    }

    public void setOkayIcon(RichIcon okayIcon) {
        this.okayIcon = okayIcon;
    }

    public RichIcon getOkayIcon() {
        return okayIcon;
    }

    public void setOkayLink(RichLink okayLink) {
        this.okayLink = okayLink;
    }

    public RichLink getOkayLink() {
        return okayLink;
    }
    
    public void setOkayText(RichOutputText okayText) {
        this.okayText = okayText;
    }

    public RichOutputText getOkayText() {
        return okayText;
    }

}
