package com.ross.rdm.shipping.hhprintshiplabels.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for ReprintAuthPage.jspx
// ---
// ---------------------------------------------------------------------
public class ReprintAuthPageBacking extends RDMShippingBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(ReprintAuthPageBacking.class);
    private final static String PAGE_FLOW_BEAN = "HhPrintShipLabelSBean";
    private RichPanelGridLayout formPanelGrid;
    private RichIcon passIcone;
    private RichIcon userIdIcon;
    private RichInputText idUser;
    private RichInputText pass;
    private RichLink f3Link;

    public ReprintAuthPageBacking() {

    }

    public void onChangedPassword(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            String newPass = (String) vce.getNewValue();
            if (newPass != null && !newPass.isEmpty()) {
                this.updateModel(vce);
                this.callVContainer();

            } else
                this.setFocusOnUserId();
        }

    }

    public void onChangeUserId(ValueChangeEvent valueChangeEvent) {
        if (!isNavigationExecuted()) {
            this.removeErrorOfAllFields();
            this.hideMessagesPanel();
            this.setFocusOnPassword();
        }
    }

    private void callVContainer() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVContainer3");
        if (oper != null) {
            oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                List codesList = (List) oper.getResult();
                if (codesList != null && codesList.size() > 0) {
                    int listCodeSize = codesList.size();
                    if (listCodeSize == 3) {
                        this.showErrorPanel(this.getMessage((String) codesList.get(0)), (String) codesList.get(1));
                        this.setErrorUi(this.getUserIdIcon(), this.getIdUser());
                        this.setFocusOnUserId();
                    } else if (listCodeSize == 2) {
                        this.showErrorPanel(this.getMessage((String) codesList.get(0)), (String) codesList.get(1));
                        this.setErrorUi(this.getPassIcone(), this.getPass());
                        this.setFocusOnPassword();
                    } else if (listCodeSize == 1) {
                        Object returnVal = codesList.get(0);
                        if (returnVal instanceof BigDecimal) {
                            this.removeErrorOfAllFields();
                            this.hideMessagesPanel();
                            this.getHhPrintShipLabelSBeanPageFlowBean().setAuthBuild(true);
                            this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("ContainerIdField");
                            ADFUtils.setBoundAttributeValue("Password", null);
                            ADFUtils.setBoundAttributeValue("UserId", null);
                            ADFUtils.invokeAction("Page1");
                        } else if (returnVal instanceof String) {
                            this.showErrorPanel((String) returnVal, "E");
                            this.setErrorUi(this.getPassIcone(), this.getPass());
                            this.setFocusOnPassword();
                        }
                    }
                }

            }
        }
    }


    private void setFocusOnPassword() {
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("pass");
        this.getPass().setDisabled(false);
        this.refreshContentOfUIComponent(this.getPass());
        this.getIdUser().setDisabled(true);
        this.refreshContentOfUIComponent(this.getIdUser());
        this.setFocusOnUIComponent(this.getPass());
    }

    private void setFocusOnUserId() {
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("userId");
        this.getPass().setDisabled(true);
        this.refreshContentOfUIComponent(this.getPass());
        this.getIdUser().setDisabled(false);
        this.refreshContentOfUIComponent(this.getIdUser());
        this.setFocusOnUIComponent(this.getIdUser());
    }

    private void removeErrorOfAllFields() {
        this.removeErrorStyleToComponent(this.getPass());
        this.removeErrorStyleToComponent(this.getIdUser());
        this.getUserIdIcon().setName("required");
        this.getPassIcone().setName("required");
        this.refreshContentOfUIComponent(this.getUserIdIcon());
        this.refreshContentOfUIComponent(this.getPassIcone());
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (!this.getHhPrintShipLabelSBeanPageFlowBean().isRegionLoaded()) {
            this.setFocusOnUserId();
            this.getHhPrintShipLabelSBeanPageFlowBean().setRegionLoaded(true);
        }
        _logger.info("onRegionLoad End");

    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public String f3CancelAction() {
        ADFUtils.setBoundAttributeValue("ContainerId", null);
        ADFUtils.setBoundAttributeValue("Password", null);
        ADFUtils.setBoundAttributeValue("UserId", null);
        this.getHhPrintShipLabelSBeanPageFlowBean().setIsFocusOn("ContainerIdField");
        this.getHhPrintShipLabelSBeanPageFlowBean().setRegionLoaded(false);
        return "Page1";
    }

    public void performF3Key(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF3Link()).queue();
            }
        }
    }

    private HhPrintShipLabelSBean getHhPrintShipLabelSBeanPageFlowBean() {
        return ((HhPrintShipLabelSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }

    private void showErrorPanel(String errorName, String errorCode) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        this.getErrorWarnInfoMessage().setValue(errorName);
        if (errorCode.equalsIgnoreCase("I")) {
            this.getErrorWarnInfoIcon().setName("info");
        } else if (errorCode.equalsIgnoreCase("W")) {
            this.getErrorWarnInfoIcon().setName("warning");
        } else if (errorCode.equalsIgnoreCase("M")) {
            this.getErrorWarnInfoIcon().setName("logo");
        } else {
            this.getErrorWarnInfoIcon().setName("error");
        }
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    private void setErrorUi(RichIcon icon, RichInputText field) {
        icon.setName("error");
        this.refreshContentOfUIComponent(icon);
        this.addErrorStyleToComponent(field);
        this.selectTextOnUIComponent(field);
        this.refreshContentOfUIComponent(field);
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }


    public void setFormPanelGrid(RichPanelGridLayout formPanelGrid) {
        this.formPanelGrid = formPanelGrid;
    }

    public RichPanelGridLayout getFormPanelGrid() {
        return formPanelGrid;
    }

    public void setPassIcone(RichIcon passIcone) {
        this.passIcone = passIcone;
    }

    public RichIcon getPassIcone() {
        return passIcone;
    }

    public void setUserIdIcon(RichIcon userIdIcon) {
        this.userIdIcon = userIdIcon;
    }

    public RichIcon getUserIdIcon() {
        return userIdIcon;
    }

    public void setIdUser(RichInputText idUser) {
        this.idUser = idUser;
    }

    public RichInputText getIdUser() {
        return idUser;
    }

    public void setPass(RichInputText pass) {
        this.pass = pass;
    }

    public RichInputText getPass() {
        return pass;
    }
    
    private boolean isNavigationExecuted(){
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();        
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if(currentPageDef != null && !currentPageDef.startsWith("com_ross_rdm_shipping_hhprintshiplabels_view_pageDefs_ReprintAuthPagePageDef")){
            navigation = true;
        }
        return navigation;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }
}
