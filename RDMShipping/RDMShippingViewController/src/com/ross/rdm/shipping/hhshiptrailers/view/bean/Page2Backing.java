package com.ross.rdm.shipping.hhshiptrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMShippingBackingBean {

    private RichLink exitLink;
    private RichLink doneLink;
    private RichInputText inputUserId;
    private RichIcon iconUserId;
    private RichInputText inputPassword;
    private RichIcon iconPassword;
    private RichPanelFormLayout mainPanel;
    private RichLink hiddenUserLink;
    private RichLink hiddenPasswordLink;
    private RichPopup successPopup;
    private RichOutputText dialogOutputText;
    private RichLink okLinkPopup;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText2;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;

    public Page2Backing() {

    }

    public void onChangedUserId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage2(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenUserLink());
            actionEvent.queue();
        }
    }
    
    public String onChangedUserIdAction(){
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage2() == null) {
                if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("UserId"))) {
                    this.switchFocusToField(HhShipTrailerSBean.PASSWORD);
                }
            } else {
                pfBean.setLinkPressedPage2(null);
            }
        }
        return null;
    }

    public void onChangedPassword(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage2(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenPasswordLink());
            actionEvent.queue();
        }
    }
    
    public String onChangedPasswordAction(){
        String action = null;
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage2() == null) {
                if (!"OVERRIDE".equals(ADFUtils.getBoundAttributeValue("Mode"))
                    && StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("Password"))) {
                    OperationBinding op = ADFUtils.findOperation("callOpenTrailerFromPage2");
                    List<String> result = (List<String>) op.execute();
                    if (op.getErrors() == null || op.getErrors().isEmpty()) {
                        if (result != null) {
                            if(result.size() == 1){
                                action = result.get(0);
                                this.switchFocusToField(HhShipTrailerSBean.USER_ID);
                            }
                            else if(MSGTYPE_M.equals(result.get(0))){
                                pfBean.setPage1Focus(HhShipTrailerSBean.TRAILER_ID);
                                
                                switchFocusToField(HhShipTrailerSBean.POP_UP);
                                this.getDialogOutputText().setValue(result.get(1));
                                this.getSuccessPopup().setLauncherVar(HhShipTrailerSBean.SHIP_TRAILER_ACTION);
                                this.getSuccessPopup().show(new RichPopup.PopupHints());
                            }
                            else if(result.size() == 3){
                                // ERROR IN OPEN_TRAILER CALL. SHOW THE ERROR IN SHIP_TRAILER BLOCK
                                action = result.get(2);
                                this.switchFocusToField(HhShipTrailerSBean.USER_ID);
                                pfBean.setGlobalErrorType(result.get(0));
                                pfBean.setGlobalErrorMsg(result.get(1));
                            }
                            else {
                                this.setErrorOnField(this.getInputPassword(), this.getIconPassword());
                                this.showMessagesPanel(result.get(0), result.get(1));
                            }
                        }
                    }
                }    
            } else {
                pfBean.setLinkPressedPage2(null);
            }
        }
        return action;
    }
    
    public String okLinkConfirmSuccessPopupAction() {
        this.getSuccessPopup().hide();
        ADFUtils.findOperation("clearUserPassBlock").execute();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        pfBean.setRefreshFocusNeeded(true);
        pfBean.setPage1Focus(HhShipTrailerSBean.SEAL_NBR);
        this.switchFocusToField(HhShipTrailerSBean.USER_ID);
        return HhShipTrailerSBean.SHIP_TRAILER_ACTION;
    }
    
    public String exitAction() {
        ADFUtils.findOperation("clearUserPassBlock").execute();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        pfBean.setPage1Focus(HhShipTrailerSBean.TRAILER_ID);
        this.switchFocusToField(HhShipTrailerSBean.USER_ID);
        return HhShipTrailerSBean.SHIP_TRAILER_ACTION;
    }

    public String doneAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ship_trailer_s_USER_PASS_F4,
                                                    RoleBasedAccessConstants.FORM_NAME_hh_ship_trailer_s)) {
        if(ADFUtils.getBoundAttributeValue("Password") != null && !"".equals(ADFUtils.getBoundAttributeValue("Password"))){
            if("OVERRIDE".equals(ADFUtils.getBoundAttributeValue("Mode"))){
                
                OperationBinding op = ADFUtils.findOperation("callCheckUserPassword");
                op.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
                op.getParamsMap().put("userId", ADFUtils.getBoundAttributeValue("UserId"));
                op.getParamsMap().put("passwordIn", ADFUtils.getBoundAttributeValue("Password"));
                op.getParamsMap().put("formName", "HH_SHIP_TRAILER_S");
                if (ADFUtils.getBoundAttributeValue("MainBlock") != null) {
                    op.getParamsMap().put("optionName", ADFUtils.getBoundAttributeValue("MainBlock") + ".F7");
                }
                else{
                    op.getParamsMap().put("optionName", "SHIP_TRAILER.F7");
                }
                String validate = (String) op.execute();
                if (op.getErrors() == null || op.getErrors().isEmpty()){
                    if("ON".equals(validate)){
                        op = ADFUtils.findOperation("callGetMStatusCartonCount");
                        Integer result = (Integer) op.execute();
                        if (op.getErrors() == null || op.getErrors().isEmpty()) {
                            String popUpMsg = this.getMessage("CONFIRM_TRAILER") + " " + result + " " + this.getMessage("CONTINUE") + "?";
                            this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                            this.getDialogOutputText2().setValue(popUpMsg);
                            this.getConfirmPopup().setLauncherVar("CONFIRM_TRAILER");
                            this.getConfirmPopup().show(new RichPopup.PopupHints());
                        }
                    }
                    else{
                        String popUpMsg = this.getMessage("UNAUTH_USER");
                        this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                        this.getDialogOutputText2().setValue(popUpMsg);
                        this.getConfirmPopup().setLauncherVar("UNAUTH_USER");
                        this.getConfirmPopup().show(new RichPopup.PopupHints());
                    }
                }
            }
            else{
                OperationBinding op = ADFUtils.findOperation("checkUserAuthorizationAndOpenTrailer");
                List<String> result = (List<String>) op.execute();
                if (op.getErrors() == null || op.getErrors().isEmpty()){
                    if(result != null){
                        if(result.size() == 1){
                            action = result.get(0);
                            this.switchFocusToField(HhShipTrailerSBean.USER_ID);
                        }
                        else if(MSGTYPE_M.equals(result.get(0))){
                            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                            pfBean.setPage1Focus(HhShipTrailerSBean.TRAILER_ID);
                            
                            switchFocusToField(HhShipTrailerSBean.POP_UP);
                            this.getDialogOutputText().setValue(result.get(1));
                            this.getSuccessPopup().setLauncherVar(HhShipTrailerSBean.SHIP_TRAILER_ACTION);
                            this.getSuccessPopup().show(new RichPopup.PopupHints());
                        }
                        else if(result.size() == 3){
                            // ERROR IN OPEN_TRAILER CALL. SHOW THE ERROR IN SHIP_TRAILER BLOCK
                            action = result.get(2);
                            this.switchFocusToField(HhShipTrailerSBean.USER_ID);
                            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                            pfBean.setGlobalErrorType(result.get(0));
                            pfBean.setGlobalErrorMsg(result.get(1));
                        }
                        else {
                            this.setErrorOnField(this.getInputPassword(), this.getIconPassword());
                            this.showMessagesPanel(result.get(0), result.get(1));
                        }
                    }
                }
            }
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }
    
    public String yesLinkConfirmPopupAction(){
        String action = null;
        String popUpType = this.getConfirmPopup().getLauncherVar();
        if("CONFIRM_TRAILER".equals(popUpType)){
            OperationBinding op = ADFUtils.findOperation("callOverDeTrailer");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if(result.size() == 1){
                        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                        pfBean.setPage1Focus(result.get(0));
                        this.switchFocusToField(HhShipTrailerSBean.USER_ID);
                        
                        ADFUtils.setBoundAttributeValue("Mode", "");
                        action = HhShipTrailerSBean.SHIP_TRAILER_ACTION;
                    }
                    else {
                        if(MSGTYPE_M.equals(result.get(0))){
                            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                            pfBean.setPage1Focus(HhShipTrailerSBean.SEAL_NBR);
                            
                            ADFUtils.setBoundAttributeValue("Mode", "");
                            switchFocusToField(HhShipTrailerSBean.POP_UP);
                            this.getDialogOutputText().setValue(result.get(1));
                            this.getConfirmPopup().hide();
                            this.getSuccessPopup().setLauncherVar(HhShipTrailerSBean.SHIP_TRAILER_ACTION);
                            this.getSuccessPopup().show(new RichPopup.PopupHints());
                        }
                        else {
                            //this.setErrorOnField(this.getInputPassword(), this.getIconPassword());
                            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                            pfBean.setGlobalErrorType(result.get(0));
                            pfBean.setGlobalErrorMsg(result.get(1));
                            this.getConfirmPopup().hide();
                            action = this.exitAction();
                        }
                    }
                }
            }
        }
        else if("UNAUTH_USER".equals(popUpType)){
            ADFUtils.findOperation("clearUserPassBlock").execute();
            ADFUtils.setBoundAttributeValue("Mode", "OVERRIDE");
            this.switchFocusToField(HhShipTrailerSBean.USER_ID);
            this.getConfirmPopup().hide();
        }
        return action;
    }
    
    public String noLinkConfirmPopupAction(){
        String action = null;
        String popUpType = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        if("CONFIRM_TRAILER".equals(popUpType) || "UNAUTH_USER".equals(popUpType)){
            ADFUtils.setBoundAttributeValue("Mode", "");
            action = this.exitAction();
        }
        return action;
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if("use2".equals(field.getId())){
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        onChangedUserId(new ValueChangeEvent(field, null, submittedVal));
                    }
                }
                else if("pas1".equals(field.getId())){
                    if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                        submittedVal.equals(field.getValue())) {
                        onChangedPassword(new ValueChangeEvent(field, null, submittedVal));
                    }
                }
            }
        }
    }
    
    public void performKeySuccessPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOkLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            }
            else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    private void switchFocusToField(String focus) {
        removeAllErrors();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        pfBean.setPage2Focus(focus);
        this.refreshContentOfUIComponent(this.getMainPanel());
        RichInputText inputText = null;
        switch (focus) {
        case HhShipTrailerSBean.USER_ID:
            inputText = getInputUserId();
            break;
        case HhShipTrailerSBean.PASSWORD:
            inputText = getInputPassword();
            break;
        default:
                break;
        }

        if (inputText != null) {
            this.setFocusOnUIComponent(inputText);
        }
    }
    
    private void removeAllErrors() {
        removeErrorOfField(getInputUserId(), getIconUserId());
        removeErrorOfField(getInputPassword(), getIconPassword());
        hideMessagesPanel();
    }
    
    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }

    public void setInputUserId(RichInputText inputUserId) {
        this.inputUserId = inputUserId;
    }

    public RichInputText getInputUserId() {
        return inputUserId;
    }

    public void setIconUserId(RichIcon iconUserId) {
        this.iconUserId = iconUserId;
    }

    public RichIcon getIconUserId() {
        return iconUserId;
    }

    public void setInputPassword(RichInputText inputPassword) {
        this.inputPassword = inputPassword;
    }

    public RichInputText getInputPassword() {
        return inputPassword;
    }

    public void setIconPassword(RichIcon iconPassword) {
        this.iconPassword = iconPassword;
    }

    public RichIcon getIconPassword() {
        return iconPassword;
    }
    
    public void setMainPanel(RichPanelFormLayout mainPanel) {
        this.mainPanel = mainPanel;
    }

    public RichPanelFormLayout getMainPanel() {
        return mainPanel;
    }
    
    public void setHiddenUserLink(RichLink hiddenUserLink) {
        this.hiddenUserLink = hiddenUserLink;
    }

    public RichLink getHiddenUserLink() {
        return hiddenUserLink;
    }
    
    public void setHiddenPasswordLink(RichLink hiddenLink) {
        this.hiddenPasswordLink = hiddenLink;
    }

    public RichLink getHiddenPasswordLink() {
        return hiddenPasswordLink;
    }
    
    public void setSuccessPopup(RichPopup successPopup) {
        this.successPopup = successPopup;
    }

    public RichPopup getSuccessPopup() {
        return successPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setOkLinkPopup(RichLink okLinkPopup) {
        this.okLinkPopup = okLinkPopup;
    }

    public RichLink getOkLinkPopup() {
        return okLinkPopup;
    }
    
    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText2(RichOutputText dialogOutputText2) {
        this.dialogOutputText2 = dialogOutputText2;
    }

    public RichOutputText getDialogOutputText2() {
        return dialogOutputText2;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }
    
    private boolean isNavigationExecuted(){
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();        
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if(currentPageDef != null && !currentPageDef.startsWith("com_ross_rdm_shipping_hhshiptrailers_view_pageDefs_Page2PageDef")){
            navigation = true;
        }
        return navigation;
    }
}
