package com.ross.rdm.shipping.hhshiptrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for EstApptStart.jspx
// ---
// ---------------------------------------------------------------------
public class EstApptStartBacking extends RDMShippingBackingBean {
    
    private RichInputText inputEstApptStartTs;
    private RichIcon iconEstApptStartTs;
    private RichLink exitLink;
    private RichLink doneLink;
    private RichLink hiddenStartTsLink;
    private RichPanelFormLayout mainPanel;
    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;    
    private RichPopup successPopup;
    private RichOutputText dialogOutputText2;
    private RichLink okLinkPopup;

    public EstApptStartBacking() {

    }

    public void onChangedEstApptStartTs(ValueChangeEvent vce) {        
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage3(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenStartTsLink());
            actionEvent.queue();
        }
    }
    
    public String onChangedStartTsAction(){        
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage3() == null) {
                this.validateEstApptStartTs();
            } else {
                pfBean.setLinkPressedPage3(null);
            }
        }
        return null;
    }
    
    private boolean validateEstApptStartTs(){
        boolean validationOK = true;
        String loadTs = (String) ADFUtils.getBoundAttributeValue("EstApptStartTs");
        if (loadTs != null) {            
            if ((loadTs.length() == 8 || loadTs.length() == 10) && HhShipTrailerSBean.checkNumeric(loadTs)) {
                Date loadDate = null;
                String yearPattern = "yyyy";
                if(loadTs.length() == 8){
                    yearPattern = "yy";
                }
                String[] knownPatterns = { "MM/dd/"+yearPattern, "MM-dd-"+yearPattern };
                for (String pattern : knownPatterns) {
                    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
                    sdf.setLenient(false);
                    try {
                        loadDate = sdf.parse(loadTs);
                        break;
                    } catch (Exception e) {
                        validationOK = false;
                    }
                }
            } else {
                validationOK = false;
            }

            if (!validationOK) {
                this.showMessagesPanel("E", "Invalid Date");
                this.setErrorOnField(getInputEstApptStartTs(), getIconEstApptStartTs());
            }
            else{
                OperationBinding op = ADFUtils.findOperation("callValidateEstApptStartTs");
                List<String> result = (List<String>) op.execute();
                if (op.getErrors() == null || op.getErrors().isEmpty()) {
                    if (result != null) {
                        validationOK = false;
                        this.showMessagesPanel(result.get(0), result.get(1));
                        this.setErrorOnField(getInputEstApptStartTs(), getIconEstApptStartTs());
                    }
                }
            }
        }
        return validationOK;
    }
    
    public String exitAction() {
        ADFUtils.findOperation("clearEstApptStartBlock").execute();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        pfBean.setPage1Focus(HhShipTrailerSBean.SEAL_NBR);
        return HhShipTrailerSBean.SHIP_TRAILER_ACTION;
    }

    public String doneAction() {
        if(this.validateEstApptStartTs()){
            OperationBinding op = ADFUtils.findOperation("callShipTrailer1");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if ("C".equals(result.get(0))) {
                        this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                        this.getDialogOutputText().setValue(result.get(1));
                        this.getConfirmPopup().setLauncherVar("CONFIRM_SHIP");
                        this.getConfirmPopup().show(new RichPopup.PopupHints());
                    }
                    else{
                        this.setErrorOnField(getInputEstApptStartTs(), getIconEstApptStartTs());
                        this.showMessagesPanel(result.get(0), result.get(1));
                    }
                }
            }
        }
        
        return null;
    }
    
    public String yesLinkConfirmPopupAction() {
        String action = null;
        String popUpType = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        if("CONFIRM_SHIP".equals(popUpType)){
            OperationBinding op = ADFUtils.findOperation("callShipTrailer2");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if ("M".equals(result.get(0))) {
                        ADFUtils.findOperation("clearShipTrailerBlock").execute();
                        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                        pfBean.setPage1Focus(HhShipTrailerSBean.TRAILER_ID);
                        this.getDialogOutputText2().setValue(result.get(1));
                        this.getSuccessPopup().setLauncherVar(HhShipTrailerSBean.SHIP_TRAILER_ACTION);
                        this.getSuccessPopup().show(new RichPopup.PopupHints());
                    }
                    else{
                        this.setErrorOnField(getInputEstApptStartTs(), getIconEstApptStartTs());
                        this.showMessagesPanel(result.get(0), result.get(1));
                    }                    
                }
            }
        }
        return action;
    }

    public String noLinkConfirmPopupAction() {
        String action = null;
        String popUpType = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        if("CONFIRM_SHIP".equals(popUpType)){
            ADFUtils.findOperation("clearEstApptStartBlock").execute();
            ADFUtils.findOperation("clearShipTrailerBlock").execute();
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setPage1Focus(HhShipTrailerSBean.TRAILER_ID);
            action = HhShipTrailerSBean.SHIP_TRAILER_ACTION;
        }
        return action;
    }
    
    
    public String okLinkConfirmSuccessPopupAction() {
        String action = this.getSuccessPopup().getLauncherVar();
        this.getSuccessPopup().hide();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        pfBean.setPage2Focus(HhShipTrailerSBean.EST_APPT_START_TS);
        return action;
    }
    
    
    private void switchFocusToField(String focus) {
        removeErrorOfField(getInputEstApptStartTs(), getIconEstApptStartTs());
        hideMessagesPanel();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        pfBean.setPage2Focus(focus);
        this.refreshContentOfUIComponent(this.getMainPanel());
        RichInputText inputText = null;
        if(HhShipTrailerSBean.EST_APPT_START_TS.equals(focus)){
            inputText = this.getInputEstApptStartTs();   
        }
        if (inputText != null) {
            this.setFocusOnUIComponent(inputText);
        }
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getDoneLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
                    submittedVal.equals(field.getValue())) {
                    onChangedEstApptStartTs(new ValueChangeEvent(field, null, submittedVal));
                }
            }
        }
    }
    
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }
        
    public void performKeySuccessPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOkLinkPopup());
                actionEvent.queue();
            }
        }
    }
    
    private boolean isNavigationExecuted(){
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();        
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if(currentPageDef != null && !currentPageDef.startsWith("com_ross_rdm_shipping_hhshiptrailers_view_pageDefs_EstApptStartPageDef")){
            navigation = true;
        }
        return navigation;
    }
    
    public void setInputEstApptStartTs(RichInputText inputEstApptStartTs) {
        this.inputEstApptStartTs = inputEstApptStartTs;
    }

    public RichInputText getInputEstApptStartTs() {
        return inputEstApptStartTs;
    }

    public void setIconEstApptStartTs(RichIcon iconEstApptStartTs) {
        this.iconEstApptStartTs = iconEstApptStartTs;
    }

    public RichIcon getIconEstApptStartTs() {
        return iconEstApptStartTs;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setDoneLink(RichLink doneLink) {
        this.doneLink = doneLink;
    }

    public RichLink getDoneLink() {
        return doneLink;
    }
    
    public void setMainPanel(RichPanelFormLayout mainPanel) {
        this.mainPanel = mainPanel;
    }

    public RichPanelFormLayout getMainPanel() {
        return mainPanel;
    }
    
    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }
    
    public void setSuccessPopup(RichPopup successPopup) {
        this.successPopup = successPopup;
    }

    public RichPopup getSuccessPopup() {
        return successPopup;
    }

    public void setDialogOutputText2(RichOutputText dialogOutputText2) {
        this.dialogOutputText2 = dialogOutputText2;
    }

    public RichOutputText getDialogOutputText2() {
        return dialogOutputText2;
    }

    public void setOkLinkPopup(RichLink okLinkPopup) {
        this.okLinkPopup = okLinkPopup;
    }

    public RichLink getOkLinkPopup() {
        return okLinkPopup;
    }
    
    public void setHiddenStartTsLink(RichLink hiddenStartTsLink) {
        this.hiddenStartTsLink = hiddenStartTsLink;
    }

    public RichLink getHiddenStartTsLink() {
        return hiddenStartTsLink;
    }
}
