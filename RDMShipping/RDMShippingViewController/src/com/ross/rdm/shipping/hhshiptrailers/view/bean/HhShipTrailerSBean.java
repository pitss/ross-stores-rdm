package com.ross.rdm.shipping.hhshiptrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.util.Calendar;
import java.util.Date;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhShipTrailerSBean {

    private String page1Focus;
    private String page1PreviousFocus;
    private String page2Focus;
    private String pageEstApptFocus;
    private String linkPressedPage1;
    private String linkPressedPage2;
    private String linkPressedPage3;
    
    private String globalErrorType;
    private String globalErrorMsg;
    private Boolean refreshFocusNeeded;

    public static final String TRAILER_ID = "TRAILER_ID";
    public static final String CARRIER_CODE = "CARRIER_CODE";
    public static final String SERVICE_CODE = "SERVICE_CODE";
    public static final String ROUTE = "ROUTE";
    public static final String DEST_ID = "DEST_ID";
    public static final String DOOR_ID = "DOOR_ID";
    public static final String SEAL_NBR = "SEAL_NBR";
    public static final String LOAD_TS = "LOAD_TS";    
    public static final String EST_APPT_START_TS = "EST_APPT_START_TS";
    public static final String USER_ID = "USER_ID";
    public static final String PASSWORD = "PASSWORD";
    public static final String POP_UP = "POP_UP";
    
    public static final String SHIP_TRAILER_ACTION = "SHIP_TRAILER";
    public static final String USER_PASS_ACTION = "USER_PASS";
    public static final String EST_APPT_START_ACTION = "EST_APPT_START";

    public HhShipTrailerSBean() {
    }

    public void initTaskFlow(){
        this.page1Focus = TRAILER_ID;
        this.page2Focus = USER_ID;
        this.pageEstApptFocus = EST_APPT_START_TS;
        this.linkPressedPage1 = null;
        this.linkPressedPage2 = null;
        this.linkPressedPage3 = null;
        this.globalErrorType = null;
        this.globalErrorMsg = null;
        this.setRefreshFocusNeeded(false);
        ADFUtils.findOperation("initShipTrailerS").execute();
    }
    
    public static boolean checkNumeric(String date) {
        String[] tokens = date.split("-|\\/");
        return isNumeric(tokens[0]) && isNumeric(tokens[1]) && isNumeric(tokens[2]);
    }

    public static boolean isNumeric(String s) {
        return java.util.regex.Pattern.matches("\\d+", s);
    }

    public static Date getToday() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    public void setPage1Focus(String page1Focus) {
        this.page1Focus = page1Focus;
    }

    public String getPage1Focus() {
        return page1Focus;
    }

    public void setPage2Focus(String page2Focus) {
        this.page2Focus = page2Focus;
    }

    public String getPage2Focus() {
        return page2Focus;
    }

    public void setPageEstApptFocus(String pageEstApptFocus) {
        this.pageEstApptFocus = pageEstApptFocus;
    }

    public String getPageEstApptFocus() {
        return pageEstApptFocus;
    }
    
    public void setPage1PreviousFocus(String page1PreviousFocus) {
        this.page1PreviousFocus = page1PreviousFocus;
    }

    public String getPage1PreviousFocus() {
        return page1PreviousFocus;
    }
    
    public void setLinkPressedPage1(String linkPressedPage1) {
        this.linkPressedPage1 = linkPressedPage1;
    }

    public String getLinkPressedPage1() {
        return linkPressedPage1;
    }
    
    public void setLinkPressedPage2(String linkPressedPage2) {
        this.linkPressedPage2 = linkPressedPage2;
    }

    public String getLinkPressedPage2() {
        return linkPressedPage2;
    }
    
    public void setLinkPressedPage3(String linkPressedPage3) {
        this.linkPressedPage3 = linkPressedPage3;
    }

    public String getLinkPressedPage3() {
        return linkPressedPage3;
    }
    
    public void setGlobalErrorType(String globalErrorType) {
        this.globalErrorType = globalErrorType;
    }

    public String getGlobalErrorType() {
        return globalErrorType;
    }

    public void setGlobalErrorMsg(String globalErrorMsg) {
        this.globalErrorMsg = globalErrorMsg;
    }

    public String getGlobalErrorMsg() {
        return globalErrorMsg;
    }

    public void setRefreshFocusNeeded(Boolean refreshFocusNeeded) {
        this.refreshFocusNeeded = refreshFocusNeeded;
    }

    public Boolean getRefreshFocusNeeded() {
        return refreshFocusNeeded;
    }

}
