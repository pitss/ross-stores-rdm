package com.ross.rdm.shipping.hhshiptrailers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {

    private RichIcon iconTrailerId;
    private RichInputText inputTrailerId;
    private RichInputText inputCarrierCode;
    private RichIcon iconCarrierCode;
    private RichInputText inputServiceCode;
    private RichIcon iconServiceCode;
    private RichInputText inputRoute;
    private RichIcon iconRoute;
    private RichInputText inputDestId;
    private RichIcon iconDestId;
    private RichInputText inputDoorId;
    private RichIcon iconDoorId;
    private RichInputText inputSealNbr;
    private RichIcon iconSealNbr;
    private RichInputText inputLoadTs;
    private RichIcon iconLoadTs;
    private RichLink exitLink;
    private RichLink openLink;
    private RichLink overDeLink;
    private RichLink closeLink;
    private RichLink shipLink;
    private RichPanelFormLayout mainPanel;

    private RichPopup confirmPopup;
    private RichOutputText dialogOutputText;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;
    private RichPopup warningPopup;
    private RichOutputText warningDialogOutputText;
    private RichOutputText warningDialogOutputText2;
    private RichLink okLinkPopup;

    private RichLink hiddenTrailerIdLink;
    private RichLink hiddenCarrierCodeLink;
    private RichLink hiddenServiceCodeLink;
    private RichLink hiddenRouteLink;
    private RichLink hiddenDestIdLink;
    private RichLink hiddenDoorIdLink;
    private RichLink hiddenSealNbrLink;
    private RichLink hiddenLoadTsLink;

    private static final String SUCCESS_OPER = "SUCCESS_OPER";

    public Page1Backing() {

    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenTrailerIdLink());
            actionEvent.queue();
        }
    }

    public String onChangedTrailerIdAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.validateTrailerId(true);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private boolean validateTrailerId(boolean moveFocusOnSuccess) {
        boolean validationOK = true;
        if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("TrailerId"))) {
            validationOK = false;
            this.showMessagesPanel(ERROR, this.getMessage("PARTIAL_ENTRY"));
            this.setErrorOnField(getInputTrailerId(), getIconTrailerId());
        } else {
            OperationBinding op = ADFUtils.findOperation("callVTrailerId");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    validationOK = false;
                    this.showMessagesPanel(result.get(0), result.get(1));
                    this.setErrorOnField(getInputTrailerId(), getIconTrailerId());
                } else {
                    if (moveFocusOnSuccess) {
                        op = ADFUtils.findOperation("validateTrailerId");
                        result = (List<String>) op.execute();
                        if (op.getErrors() == null || op.getErrors().isEmpty()) {
                            if (result.size() == 1) {
                                this.switchFocusToField(result.get(0));
                            } else if (CONF.equals(result.get(0))) {
                                this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                                this.getDialogOutputText().setValue(getMessage(result.get(1)));
                                this.getConfirmPopup().setLauncherVar("TRANSSHIP_SHIP");
                                this.getConfirmPopup().show(new RichPopup.PopupHints());
                            } else if (result.size() == 3) {
                                this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                                this.getWarningDialogOutputText().setValue(result.get(1));
                                this.getWarningPopup().setLauncherVar(result.get(2));
                                this.getWarningPopup().show(new RichPopup.PopupHints());
                            }
                        }
                    }
                }
            }
        }

        return validationOK;
    }

    private void continueValidateTrailerLogic() {
        if (BigDecimal.valueOf(0).equals(ADFUtils.getBoundAttributeValue("ValueFoundGetManifest")) ||
            YES.equals(ADFUtils.getBoundAttributeValue("MultiOpenManifest"))) {

            String trailerId = (String) ADFUtils.getBoundAttributeValue("TrailerId");
            BigDecimal destId = (BigDecimal) ADFUtils.getBoundAttributeValue("DestId");
            ADFUtils.findOperation("clearShipTrailerBlock").execute();

            ADFUtils.setBoundAttributeValue("BolNbr", null);
            ADFUtils.setBoundAttributeValue("TrailerId", trailerId);

            if (YES.equals(ADFUtils.getBoundAttributeValue("TransshipmentFlag")) && destId != null) {
                ADFUtils.setBoundAttributeValue("DestId", destId);
            }

            if (YES.equals(ADFUtils.getBoundAttributeValue("TransshipmentFlag"))) {
                // SET_ITEM_PROPERTY('SHIP_TRAILER.CARRIER_CODE', NAVIGABLE,  property_false);
                // SET_ITEM_PROPERTY('SHIP_TRAILER.SERVICE_CODE', NAVIGABLE,  property_false);
                // SET_ITEM_PROPERTY('SHIP_TRAILER.DEST_ID', NAVIGABLE,  property_false);
                this.switchFocusToField(HhShipTrailerSBean.ROUTE);
            } else {
                // SET_ITEM_PROPERTY('SHIP_TRAILER.CARRIER_CODE', NAVIGABLE,  property_true);
                // SET_ITEM_PROPERTY('SHIP_TRAILER.SERVICE_CODE', NAVIGABLE,  property_true);
                // SET_ITEM_PROPERTY('SHIP_TRAILER.DEST_ID', NAVIGABLE,  property_true);
                this.switchFocusToField(HhShipTrailerSBean.CARRIER_CODE);
            }
        } else {
            this.switchFocusToField(HhShipTrailerSBean.SEAL_NBR); // PC3_Go_Field('SHIP_TRAILER.SEAL_NBR');
        }
    }

    public void onChangedCarrierCode(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenCarrierCodeLink());
            actionEvent.queue();
        }
    }

    public String onChangedCarrierCodeAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                if (YES.equals(ADFUtils.getBoundAttributeValue("TransshipmentFlag"))) {
                    // SET_ITEM_PROPERTY('SHIP_TRAILER.CARRIER_CODE', NAVIGABLE,  property_false);
                    // SET_ITEM_PROPERTY('SHIP_TRAILER.SERVICE_CODE', NAVIGABLE,  property_false);
                    // SET_ITEM_PROPERTY('SHIP_TRAILER.DEST_ID', NAVIGABLE,  property_false);
                    this.switchFocusToField(HhShipTrailerSBean.ROUTE);
                } else {
                    // SET_ITEM_PROPERTY('SHIP_TRAILER.CARRIER_CODE', NAVIGABLE,  property_true);
                    // SET_ITEM_PROPERTY('SHIP_TRAILER.SERVICE_CODE', NAVIGABLE,  property_true);
                    // SET_ITEM_PROPERTY('SHIP_TRAILER.DEST_ID', NAVIGABLE,  property_true);
                    this.switchFocusToField(HhShipTrailerSBean.SERVICE_CODE);
                }
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    public void onChangedServiceCode(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenServiceCodeLink());
            actionEvent.queue();
        }
    }

    public String onChangedServiceCodeAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.validateServiceCode(true);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private boolean validateServiceCode(boolean moveFocusOnSuccess) {
        boolean validationOK = true;
        OperationBinding op = ADFUtils.findOperation("validateServiceCode");
        List<String> result = (List<String>) op.execute();
        if (op.getErrors() == null || op.getErrors().isEmpty()) {
            if (result != null) {
                validationOK = false;
                this.showMessagesPanel(result.get(0), result.get(1));
                this.setErrorOnField(getInputServiceCode(), getIconServiceCode());
            } else {
                if (moveFocusOnSuccess) {
                    this.switchFocusToField(HhShipTrailerSBean.ROUTE);
                }
            }
        }
        return validationOK;
    }

    public void onChangedRoute(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenRouteLink());
            actionEvent.queue();
        }
    }

    public String onChangedRouteAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.validateRoute(true);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private boolean validateRoute(boolean moveFocusOnSuccess) {
        boolean validationOK = true;
        OperationBinding op = ADFUtils.findOperation("validateRoute");
        List<String> result = (List<String>) op.execute();
        if (op.getErrors() == null || op.getErrors().isEmpty()) {
            if (result != null) {
                validationOK = false;
                this.showMessagesPanel(result.get(0), result.get(1));
                this.setErrorOnField(getInputRoute(), getIconRoute());
            } else {
                if (moveFocusOnSuccess) {
                    if (YES.equals(ADFUtils.getBoundAttributeValue("TransshipmentFlag"))) {
                        // SET_ITEM_PROPERTY('SHIP_TRAILER.CARRIER_CODE', NAVIGABLE,  property_false);
                        // SET_ITEM_PROPERTY('SHIP_TRAILER.SERVICE_CODE', NAVIGABLE,  property_false);
                        // SET_ITEM_PROPERTY('SHIP_TRAILER.DEST_ID', NAVIGABLE,  property_false);
                        this.switchFocusToField(HhShipTrailerSBean.DOOR_ID);
                    } else {
                        // SET_ITEM_PROPERTY('SHIP_TRAILER.CARRIER_CODE', NAVIGABLE,  property_true);
                        // SET_ITEM_PROPERTY('SHIP_TRAILER.SERVICE_CODE', NAVIGABLE,  property_true);
                        // SET_ITEM_PROPERTY('SHIP_TRAILER.DEST_ID', NAVIGABLE,  property_true);
                        this.switchFocusToField(HhShipTrailerSBean.DEST_ID);
                    }
                }
            }
        }
        return validationOK;
    }

    public void onChangedDestId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenDestIdLink());
            actionEvent.queue();
        }
    }

    public String onChangedDestIdAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.validateDestId(true);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private boolean validateDestId(boolean moveFocusOnSuccess) {
        boolean validationOK = true;
        String destId = (String) ADFUtils.getBoundAttributeValue("DestId");
        if (StringUtils.isNotEmpty(destId)) {
            if (!destId.matches("[0-9]+")) {
                validationOK = false;
                this.showMessagesPanel(ERROR, this.getMessage("MUST_BE_NUMERIC"));
                this.setErrorOnField(getInputDestId(), getIconDestId());
            } else {
                OperationBinding op = ADFUtils.findOperation("validateDestId");
                List<String> result = (List<String>) op.execute();
                if (op.getErrors() == null || op.getErrors().isEmpty()) {
                    if (result != null) {
                        validationOK = false;
                        this.showMessagesPanel(result.get(0), result.get(1));
                        this.setErrorOnField(getInputDestId(), getIconDestId());
                    } else {
                        if (moveFocusOnSuccess) {
                            this.switchFocusToField(HhShipTrailerSBean.DOOR_ID);
                        }
                    }
                }
            }
        } else if (moveFocusOnSuccess) {
            this.switchFocusToField(HhShipTrailerSBean.DOOR_ID);
        }
        return validationOK;
    }

    public void onChangedDoorId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenDoorIdLink());
            actionEvent.queue();
        }
    }

    public String onChangedDoorIdAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.validateDoorId(true);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private boolean validateDoorId(boolean moveFocusOnSuccess) {
        boolean validationOK = true;
        if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("DoorId"))) {
            OperationBinding op = ADFUtils.findOperation("validateDoorId");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    validationOK = false;
                    this.showMessagesPanel(result.get(0), result.get(1));
                    this.setErrorOnField(getInputDoorId(), getIconDoorId());
                } else {
                    if (moveFocusOnSuccess) {
                        this.switchFocusToField(HhShipTrailerSBean.SEAL_NBR);
                    }
                }
            }
        } else if (moveFocusOnSuccess) {
            this.switchFocusToField(HhShipTrailerSBean.SEAL_NBR);
        }
        return validationOK;
    }

    public void onChangedSealNbr(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenSealNbrLink());
            actionEvent.queue();
        }
    }

    public String onChangedSealNbrAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.switchFocusToField(HhShipTrailerSBean.LOAD_TS);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    public void onChangedLoadTs(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setLinkPressedPage1(null);
            ActionEvent actionEvent = new ActionEvent(this.getHiddenLoadTsLink());
            actionEvent.queue();
        }
    }

    public String onChangedLoadTsAction() {
        if (!this.isNavigationExecuted()) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            if (pfBean.getLinkPressedPage1() == null) {
                this.validateLoadTs(true);
            } else {
                pfBean.setLinkPressedPage1(null);
            }
        }
        return null;
    }

    private boolean validateLoadTs(boolean moveFocusOnSuccess) {
        boolean validationOK = true;
        String loadTs = (String) ADFUtils.getBoundAttributeValue("LoadTs");
        if (StringUtils.isNotEmpty(loadTs)) {
            String errorMsg = null;
            if ((loadTs.length() == 8 || loadTs.length() == 10) && HhShipTrailerSBean.checkNumeric(loadTs)) {
                Date loadDate = null;
                String yearPattern = "yyyy";
                if (loadTs.length() == 8) {
                    yearPattern = "yy";
                }
                String[] knownPatterns = { "MM/dd/" + yearPattern, "MM-dd-" + yearPattern };
                for (String pattern : knownPatterns) {
                    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
                    sdf.setLenient(false);
                    try {
                        loadDate = sdf.parse(loadTs);
                        break;
                    } catch (Exception e) {
                        validationOK = false;
                        errorMsg = "Month must be between";
                    }
                }

                if (loadDate != null && loadDate.compareTo(HhShipTrailerSBean.getToday()) < 0) {
                    validationOK = false;
                    errorMsg = this.getMessage("INVALID_DATE");
                }
            } else {
                validationOK = false;
                errorMsg = "Date must be entered";
            }

            if (!validationOK) {
                this.showMessagesPanel(ERROR, errorMsg);
                this.setErrorOnField(getInputLoadTs(), getIconLoadTs());
            } else if (moveFocusOnSuccess) {
                this.switchFocusToField(HhShipTrailerSBean.TRAILER_ID);
            }
        } else if (moveFocusOnSuccess) {
            this.switchFocusToField(HhShipTrailerSBean.TRAILER_ID);
        }
        return validationOK;
    }

    public String exitAction() {
        ADFUtils.findOperation("deleteRossEventCode").execute();
        return this.logoutExitBTF();
    }

    public String openAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_YARD_AUTH_F5,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ship_trailer_s)) {
        if (this.validateCurrentField()) {
            OperationBinding op = ADFUtils.findOperation("callOpenTrailer");
            List<String> result = (List<String>) op.execute();
            if (result != null && result.size() >= 2) {
                String pGoItem = null;
                if (result.size() == 3) {
                    pGoItem = result.get(2);
                    result = processMessagesParams(result.get(0), result.get(1));
                    result.add(pGoItem);
                } else {
                    result = processMessagesParams(result.get(0), result.get(1));
                }
            }
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if (result.size() == 1) {
                        action = result.get(0);
                    } else if (result.size() == 2) {
                        this.setErrorInSelectedField();
                        this.showMessagesPanel(result.get(0), result.get(1));
                    } else if (result.size() == 3) {
                        this.switchFocusToField(result.get(2));
                        this.showMessagesPanel(result.get(0), result.get(1));
                    }
                }
            }
        }
        if (HhShipTrailerSBean.USER_PASS_ACTION.equals(action)) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setPage2Focus(HhShipTrailerSBean.USER_ID);
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }
    
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }

    public String overDeAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_F7,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ship_trailer_s)) {
        if (this.validateCurrentField()) {
            OperationBinding op = ADFUtils.findOperation("callCheckScreenOptionPriv");
            op.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
            op.getParamsMap().put("userId", ADFUtils.getBoundAttributeValue("User"));
            op.getParamsMap().put("formName", "HH_SHIP_TRAILER_S");
            op.getParamsMap().put("optionName", "SHIP_TRAILER.F7");
            String screenOptPriv = (String) op.execute();
            if ("OFF".equals(screenOptPriv)) {
                ADFUtils.setBoundAttributeValue("UserId", null);
                ADFUtils.setBoundAttributeValue("Password", null);
                ADFUtils.setBoundAttributeValue("Mode", "OVERRIDE");
                action = HhShipTrailerSBean.USER_PASS_ACTION;
            } else {
                op = ADFUtils.findOperation("callGetMStatusCartonCount");
                Integer result = (Integer) op.execute();
                if (op.getErrors() == null || op.getErrors().isEmpty()) {
                    String popUpMsg =
                        this.getMessage("CONFIRM_TRAILER") + " " + result + " " + this.getMessage("CONTINUE") + "?";
                    this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                    this.getDialogOutputText().setValue(popUpMsg);
                    this.getConfirmPopup().setLauncherVar("CONFIRM_TRAILER_F7");
                    this.getConfirmPopup().show(new RichPopup.PopupHints());
                }
            }
        }
        if (HhShipTrailerSBean.USER_PASS_ACTION.equals(action)) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setPage2Focus(HhShipTrailerSBean.USER_ID);
        }
        
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }

    public List<String> processMessagesParams(String p_v_return, String p_msg_display) {
        List<String> errors = null;
        //ShippingAppModuleImpl am = (ShippingAppModuleImpl) this.getApplicationModule();
        //        System.out.println(" p_v_return : "+ p_v_return);
        //        System.out.println(" p_v_return : "+ p_msg_display);
        if ("RAISE_FAILURE####".equals(p_v_return)) {
            errors = new ArrayList<String>();
            errors.add(ERROR);
            errors.add(getMessage(DMS_ERROR));
        } else if (p_v_return != null) {
            errors = new ArrayList<String>();
            if (p_msg_display != null) {
                errors.add((String) p_msg_display);
            } else {
                errors.add(ERROR);
            }
            System.out.println(" line 550 : " + getMessage(p_v_return));
            errors.add(getMessage(p_v_return));
        }
        return errors;
    }

    public String closeAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_F8,
                                                RoleBasedAccessConstants.FORM_NAME_hh_ship_trailer_s)) {
        if (this.validateCurrentField()) {
            OperationBinding op = ADFUtils.findOperation("callCheckForCutOff");
            List<String> result = (List<String>) op.execute();
            // adding the fix for 3737
            if (result != null && result.size() == 2) {
                result = processMessagesParams(result.get(0), result.get(1));
            }
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if (result.size() == 1) {
                        String popUpMsg =
                            this.getMessage("CONFIRM_TRAILER") + " " + result.get(0) + " " +
                            this.getMessage("CONTINUE") + "?";
                        this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                        this.getDialogOutputText().setValue(popUpMsg);
                        this.getConfirmPopup().setLauncherVar("CONFIRM_TRAILER_F8");
                        this.getConfirmPopup().show(new RichPopup.PopupHints());
                    } else if (result.size() == 2) {
                        this.switchFocusToField(this.getCurrentFocusedField());
                        this.setErrorInSelectedField();
                        this.showMessagesPanel(result.get(0), result.get(1));
                    }
                } else {
                    op = ADFUtils.findOperation("callCloseTrailer");
                    result = (List<String>) op.execute();
                    if (op.getErrors() == null || op.getErrors().isEmpty()) {
                        if (result != null) {
                            if (result.size() == 1) {
                                this.switchFocusToField(result.get(0));
                            } else if (result.size() > 1) {
                                if (result.size() == 3) {
                                    this.switchFocusToField(result.get(2));
                                } else {
                                    this.switchFocusToField(this.getCurrentFocusedField());
                                }

                                if (!MSGTYPE_M.equals(result.get(0))) {
                                    this.setErrorInSelectedField();
                                }
                                this.showMessagesPanel(result.get(0), result.get(1));
                            }
                        } else {
                            this.switchFocusToField(this.getCurrentFocusedField());
                        }
                    }
                }
            }
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }

    public String shipAction() {
        String action = null;
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_ship_trailer_s_SHIP_TRAILER_F9,
                                                    RoleBasedAccessConstants.FORM_NAME_hh_ship_trailer_s)) {
        if (this.validateCurrentField()) {
            OperationBinding op = ADFUtils.findOperation("callShipTrailer");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if (result.size() == 1) {
                        action = result.get(0); // GO_BLOCK -> EST_APPT_START
                    } else if (result.size() > 1) {
                        if (CONF.equals(result.get(0))) {
                            this.switchFocusToField(HhShipTrailerSBean.POP_UP);
                            this.getDialogOutputText().setValue(getMessage(result.get(1)));
                            this.getConfirmPopup().setLauncherVar("CONFIRM_SHIP");
                            this.getConfirmPopup().show(new RichPopup.PopupHints());
                        } else {
                            if (result.size() == 3) {
                                this.switchFocusToField(result.get(2));
                            }
                            if (!MSGTYPE_M.equals(result.get(0))) {
                                this.setErrorInSelectedField();
                            }
                            this.showMessagesPanel(result.get(0), result.get(1));
                        }
                    }
                }
            }
        }
        if (HhShipTrailerSBean.USER_PASS_ACTION.equals(action)) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            pfBean.setPage2Focus(HhShipTrailerSBean.USER_ID);
        }
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            return null;
        }
        return action;
    }

    public void reportTest(ActionEvent event) {
    }

    public String yesLinkConfirmPopupAction() {
        String popUpType = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        if ("TRANSSHIP_SHIP".equals(popUpType)) {
            ADFUtils.setBoundAttributeValue("TransshipmentFlag", YES);
            this.continueValidateTrailerLogic();
        } else if ("CONFIRM_TRAILER_F7".equals(popUpType)) {
            if (this.validateServiceCode(false)) {
                OperationBinding op = ADFUtils.findOperation("callOverDeTrailer");
                List<String> result = (List<String>) op.execute();
                if (op.getErrors() == null || op.getErrors().isEmpty()) {
                    if (result != null) {
                        if (result.size() == 1) {
                            this.switchFocusToField(result.get(0));
                        } else if (result.size() > 1) {
                            if (result.size() == 3) {
                                this.switchFocusToField(result.get(2));
                            } else {
                                HhShipTrailerSBean pfBean =
                                    (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                                this.switchFocusToField(pfBean.getPage1PreviousFocus());
                            }
                            if (!MSGTYPE_M.equals(result.get(0))) {
                                this.setErrorInSelectedField();
                            }
                            this.showMessagesPanel(result.get(0), result.get(1));
                        }
                    } else {
                        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                        this.switchFocusToField(pfBean.getPage1PreviousFocus());
                    }
                }
            } else {
                HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                pfBean.setPage1Focus(HhShipTrailerSBean.SERVICE_CODE);
            }
        } else if ("CONFIRM_TRAILER_F8".equals(popUpType)) {
            OperationBinding op = ADFUtils.findOperation("callCloseTrailer");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    if (result.size() == 1) {
                        this.switchFocusToField(result.get(0));
                    } else if (result.size() > 1) {
                        if (result.size() == 3) {
                            this.switchFocusToField(result.get(2));
                        } else {
                            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                            this.switchFocusToField(pfBean.getPage1PreviousFocus());
                        }

                        if (!MSGTYPE_M.equals(result.get(0))) {
                            this.setErrorInSelectedField();
                        }
                        this.showMessagesPanel(result.get(0), result.get(1));
                    }
                } else {
                    HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                    this.switchFocusToField(pfBean.getPage1PreviousFocus());
                }
            }
        } else if ("CONFIRM_SHIP".equals(popUpType)) {
            OperationBinding op = ADFUtils.findOperation("callShipTrailer2");
            List<String> result = (List<String>) op.execute();
            if (op.getErrors() == null || op.getErrors().isEmpty()) {
                if (result != null) {
                    // 1. W -> SUBMIT_REPORT or INV_QUEUE
                    // 2. W -> BOL_MANUAL
                    // 3. M -> SUCCESS_OPER
                    if (result.size() == 2) {
                        if (MSGTYPE_M.equals(result.get(0))) {
                            ADFUtils.findOperation("clearShipTrailerBlock").execute();
                            this.switchFocusToField(HhShipTrailerSBean.TRAILER_ID);
                        } else {
                            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                            this.switchFocusToField(pfBean.getPage1PreviousFocus());

                            this.setErrorInSelectedField();
                        }
                        this.showMessagesPanel(result.get(0), result.get(1));
                    } else {
                        this.getWarningDialogOutputText().setValue(result.get(1));

                        if (result.size() == 4) {
                            this.getWarningPopup().setLauncherVar(SUCCESS_OPER);
                        } else if (result.size() == 6) {
                            this.getWarningDialogOutputText2().setValue(result.get(3));
                            this.getWarningPopup().setLauncherVar("BOL_MANUAL");
                        }

                        this.getWarningPopup().show(new RichPopup.PopupHints());
                    }
                } else {
                    HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                    this.switchFocusToField(pfBean.getPage1PreviousFocus());
                }
            }
        }
        return null;
    }

    public String okLinkWarningPopupAction() {
        String popUpType = this.getWarningPopup().getLauncherVar();
        if (SUCCESS_OPER.equals(popUpType)) {
            this.getWarningDialogOutputText().setValue(null);
            this.getWarningDialogOutputText2().setValue(null);
            this.getWarningPopup().hide();

            ADFUtils.findOperation("clearShipTrailerBlock").execute();
            this.switchFocusToField(HhShipTrailerSBean.TRAILER_ID);

            this.showMessagesPanel(MSGTYPE_M, this.getMessage(SUCCESS_OPER));
        } else if ("BOL_MANUAL".equals(popUpType)) {
            this.getWarningPopup().hide();
            this.getWarningDialogOutputText().setValue(this.getWarningDialogOutputText2().getValue());
            this.refreshContentOfUIComponent(this.getWarningDialogOutputText());
            this.getWarningPopup().setLauncherVar(SUCCESS_OPER);
            this.getWarningPopup().show(new RichPopup.PopupHints());
        } else {
            this.getWarningDialogOutputText().setValue(null);
            this.getWarningDialogOutputText2().setValue(null);
            this.getWarningPopup().hide();
            this.switchFocusToField(popUpType);
        }
        return null;
    }

    public String noLinkConfirmPopupAction() {
        String popUpType = this.getConfirmPopup().getLauncherVar();
        this.getConfirmPopup().hide();
        if ("TRANSSHIP_SHIP".equals(popUpType)) {
            ADFUtils.setBoundAttributeValue("TransshipmentFlag", NO);
            this.continueValidateTrailerLogic();
        } else if ("CONFIRM_TRAILER_F7".equals(popUpType)) {
            if (getCurrentFocusedField() != null) {
                HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
                if (pfBean.getPage1PreviousFocus().equalsIgnoreCase(HhShipTrailerSBean.CARRIER_CODE)) {
                    this.validateServiceCode(false);
                    pfBean.setPage1Focus(HhShipTrailerSBean.SERVICE_CODE);
                    this.setFocusOnUIComponent(getInputServiceCode());
                } else {
                    this.switchFocusToField(HhShipTrailerSBean.TRAILER_ID);
                }
            }
        } else if ("CONFIRM_TRAILER_F8".equals(popUpType)) {
            HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
            this.switchFocusToField(pfBean.getPage1PreviousFocus());
        } else if ("CONFIRM_SHIP".equals(popUpType)) {
            ADFUtils.findOperation("clearShipTrailerBlock").execute();
            this.switchFocusToField(HhShipTrailerSBean.TRAILER_ID);
        }
        return null;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedVal = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getExitLink());
                actionEvent.queue();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOpenLink());
                actionEvent.queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOverDeLink());
                actionEvent.queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getCloseLink());
                actionEvent.queue();
            } else if (F9_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getShipLink());
                actionEvent.queue();
            } else if ((ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))) {
                this.queueValueChangeEvent(field, submittedVal);
            }
        }
    }

    private void queueValueChangeEvent(RichInputText field, String submittedVal) {
        if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedVal)) ||
            submittedVal.equals(field.getValue())) {
            switch (field.getId()) {
            case "tra1":
                onChangedTrailerId(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "car1":
                onChangedCarrierCode(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "ser1":
                onChangedServiceCode(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "rou1":
                onChangedRoute(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "des1":
                onChangedDestId(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "doo1":
                onChangedDoorId(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "sea1":
                onChangedSealNbr(new ValueChangeEvent(field, null, submittedVal));
                break;
            case "loa1":
                onChangedLoadTs(new ValueChangeEvent(field, null, submittedVal));
                break;
            default:
                break;
            }
        }
    }

    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getYesLinkPopup());
                actionEvent.queue();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getNoLinkPopup());
                actionEvent.queue();
            }
        }
    }

    public void performKeyWarningPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getOkLinkPopup());
                actionEvent.queue();
            }
        }
    }

    private boolean validateCurrentField() {
        boolean validationOK = true;
        switch (this.getCurrentFocusedField()) {
        case HhShipTrailerSBean.TRAILER_ID:
            validationOK = this.validateTrailerId(false);
            break;
        case HhShipTrailerSBean.CARRIER_CODE:
            validationOK = true; // this field has no validation
            break;
        case HhShipTrailerSBean.SERVICE_CODE:
            validationOK = this.validateServiceCode(false);
            break;
        case HhShipTrailerSBean.ROUTE:
            validationOK = this.validateRoute(false);
            break;
        case HhShipTrailerSBean.DEST_ID:
            validationOK = this.validateDestId(false);
            break;
        case HhShipTrailerSBean.DOOR_ID:
            validationOK = this.validateDoorId(false);
            break;
        case HhShipTrailerSBean.SEAL_NBR:
            validationOK = true; // this field has no validation
            break;
        case HhShipTrailerSBean.LOAD_TS:
            validationOK = this.validateLoadTs(false);
            break;
        default:
            break;
        }
        return validationOK;
    }

    private boolean validateAllFields() {
        boolean validationOK = true;
        validationOK = this.validateTrailerId(false);
        if (validationOK)
            validationOK = this.validateServiceCode(false);
        if (validationOK)
            validationOK = this.validateRoute(false);
        if (validationOK)
            validationOK = this.validateDestId(false);
        if (validationOK)
            validationOK = this.validateDoorId(false);
        if (validationOK)
            validationOK = this.validateLoadTs(false);
        return validationOK;
    }

    private void setErrorInSelectedField() {
        this.removeAllErrors();
        switch (this.getCurrentFocusedField()) {
        case HhShipTrailerSBean.TRAILER_ID:
            setErrorOnField(getInputTrailerId(), getIconTrailerId());
            break;
        case HhShipTrailerSBean.CARRIER_CODE:
            setErrorOnField(getInputCarrierCode(), getIconCarrierCode());
            break;
        case HhShipTrailerSBean.SERVICE_CODE:
            setErrorOnField(getInputServiceCode(), getIconServiceCode());
            break;
        case HhShipTrailerSBean.ROUTE:
            setErrorOnField(getInputRoute(), getIconRoute());
            break;
        case HhShipTrailerSBean.DEST_ID:
            setErrorOnField(getInputDestId(), getIconDestId());
            break;
        case HhShipTrailerSBean.DOOR_ID:
            setErrorOnField(getInputDoorId(), getIconDoorId());
            break;
        case HhShipTrailerSBean.SEAL_NBR:
            setErrorOnField(getInputSealNbr(), getIconSealNbr());
            break;
        case HhShipTrailerSBean.LOAD_TS:
            setErrorOnField(getInputLoadTs(), getIconLoadTs());
            break;
        default:
            break;
        }
    }

    private void switchFocusToField(String focus) {
        removeAllErrors();
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        if (HhShipTrailerSBean.POP_UP.equals(focus) && !HhShipTrailerSBean.POP_UP.equals(pfBean.getPage1Focus())) {
            pfBean.setPage1PreviousFocus(pfBean.getPage1Focus());
        }
        pfBean.setPage1Focus(focus);
        this.refreshContentOfUIComponent(this.getMainPanel());
        RichInputText inputText = null;
        switch (focus) {
        case HhShipTrailerSBean.TRAILER_ID:
            inputText = getInputTrailerId();
            break;
        case HhShipTrailerSBean.CARRIER_CODE:
            inputText = getInputCarrierCode();
            break;
        case HhShipTrailerSBean.SERVICE_CODE:
            inputText = getInputServiceCode();
            break;
        case HhShipTrailerSBean.ROUTE:
            inputText = getInputRoute();
            break;
        case HhShipTrailerSBean.DEST_ID:
            inputText = getInputDestId();
            break;
        case HhShipTrailerSBean.DOOR_ID:
            inputText = getInputDoorId();
            break;
        case HhShipTrailerSBean.SEAL_NBR:
            inputText = getInputSealNbr();
            break;
        case HhShipTrailerSBean.LOAD_TS:
            inputText = getInputLoadTs();
            break;
        default:
            break;
        }
        if (inputText != null) {
            this.setFocusOnUIComponent(inputText);
        }
    }

    private String getCurrentFocusedField() {
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        return pfBean.getPage1Focus();
    }

    private void removeAllErrors() {
        removeErrorOfField(getInputTrailerId(), getIconTrailerId());
        removeErrorOfField(getInputCarrierCode(), getIconCarrierCode(), "");
        removeErrorOfField(getInputServiceCode(), getIconServiceCode());
        removeErrorOfField(getInputRoute(), getIconRoute());
        removeErrorOfField(getInputDestId(), getIconDestId(), "");
        removeErrorOfField(getInputDoorId(), getIconDoorId());
        removeErrorOfField(getInputSealNbr(), getIconSealNbr());
        removeErrorOfField(getInputLoadTs(), getIconLoadTs(), "");
        hideMessagesPanel();
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_shipping_hhshiptrailers_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public void setIconTrailerId(RichIcon iconTrailerId) {
        this.iconTrailerId = iconTrailerId;
    }

    public RichIcon getIconTrailerId() {
        return iconTrailerId;
    }

    public void setInputTrailerId(RichInputText inputTrailerId) {
        this.inputTrailerId = inputTrailerId;
    }

    public RichInputText getInputTrailerId() {
        return inputTrailerId;
    }

    public void setInputCarrierCode(RichInputText inputCarrierCode) {
        this.inputCarrierCode = inputCarrierCode;
    }

    public RichInputText getInputCarrierCode() {
        return inputCarrierCode;
    }

    public void setIconCarrierCode(RichIcon iconCarrierCode) {
        this.iconCarrierCode = iconCarrierCode;
    }

    public RichIcon getIconCarrierCode() {
        return iconCarrierCode;
    }

    public void setInputServiceCode(RichInputText inputServiceCode) {
        this.inputServiceCode = inputServiceCode;
    }

    public RichInputText getInputServiceCode() {
        return inputServiceCode;
    }

    public void setIconServiceCode(RichIcon iconServiceCode) {
        this.iconServiceCode = iconServiceCode;
    }

    public RichIcon getIconServiceCode() {
        return iconServiceCode;
    }

    public void setInputRoute(RichInputText inputRoute) {
        this.inputRoute = inputRoute;
    }

    public RichInputText getInputRoute() {
        return inputRoute;
    }

    public void setIconRoute(RichIcon iconRoute) {
        this.iconRoute = iconRoute;
    }

    public RichIcon getIconRoute() {
        return iconRoute;
    }

    public void setInputDestId(RichInputText inputDestId) {
        this.inputDestId = inputDestId;
    }

    public RichInputText getInputDestId() {
        return inputDestId;
    }

    public void setIconDestId(RichIcon iconDestId) {
        this.iconDestId = iconDestId;
    }

    public RichIcon getIconDestId() {
        return iconDestId;
    }

    public void setInputDoorId(RichInputText inputDoorId) {
        this.inputDoorId = inputDoorId;
    }

    public RichInputText getInputDoorId() {
        return inputDoorId;
    }

    public void setIconDoorId(RichIcon iconDoorId) {
        this.iconDoorId = iconDoorId;
    }

    public RichIcon getIconDoorId() {
        return iconDoorId;
    }

    public void setInputSealNbr(RichInputText inputSealNbr) {
        this.inputSealNbr = inputSealNbr;
    }

    public RichInputText getInputSealNbr() {
        return inputSealNbr;
    }

    public void setInputLoadTs(RichInputText inputLoadTs) {
        this.inputLoadTs = inputLoadTs;
    }

    public RichInputText getInputLoadTs() {
        return inputLoadTs;
    }

    public void setIconLoadTs(RichIcon iconLoadTs) {
        this.iconLoadTs = iconLoadTs;
    }

    public RichIcon getIconLoadTs() {
        return iconLoadTs;
    }

    public void setExitLink(RichLink exitLink) {
        this.exitLink = exitLink;
    }

    public RichLink getExitLink() {
        return exitLink;
    }

    public void setOpenLink(RichLink openLink) {
        this.openLink = openLink;
    }

    public RichLink getOpenLink() {
        return openLink;
    }

    public void setOverDeLink(RichLink overDeLink) {
        this.overDeLink = overDeLink;
    }

    public RichLink getOverDeLink() {
        return overDeLink;
    }

    public void setCloseLink(RichLink closeLink) {
        this.closeLink = closeLink;
    }

    public RichLink getCloseLink() {
        return closeLink;
    }

    public void setShipLink(RichLink shipLink) {
        this.shipLink = shipLink;
    }

    public RichLink getShipLink() {
        return shipLink;
    }

    public void setIconSealNbr(RichIcon iconSealNbr) {
        this.iconSealNbr = iconSealNbr;
    }

    public RichIcon getIconSealNbr() {
        return iconSealNbr;
    }

    public void setMainPanel(RichPanelFormLayout mainPanel) {
        this.mainPanel = mainPanel;
    }

    public RichPanelFormLayout getMainPanel() {
        return mainPanel;
    }

    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public void setWarningPopup(RichPopup warningPopup) {
        this.warningPopup = warningPopup;
    }

    public RichPopup getWarningPopup() {
        return warningPopup;
    }

    public void setWarningDialogOutputText(RichOutputText warningDialogOutputText) {
        this.warningDialogOutputText = warningDialogOutputText;
    }

    public RichOutputText getWarningDialogOutputText() {
        return warningDialogOutputText;
    }

    public void setWarningDialogOutputText2(RichOutputText warningDialogOutputText2) {
        this.warningDialogOutputText2 = warningDialogOutputText2;
    }

    public RichOutputText getWarningDialogOutputText2() {
        return warningDialogOutputText2;
    }

    public void setOkLinkPopup(RichLink okLinkPopup) {
        this.okLinkPopup = okLinkPopup;
    }

    public RichLink getOkLinkPopup() {
        return okLinkPopup;
    }

    public void setHiddenTrailerIdLink(RichLink hiddenTrailerIdLink) {
        this.hiddenTrailerIdLink = hiddenTrailerIdLink;
    }

    public RichLink getHiddenTrailerIdLink() {
        return hiddenTrailerIdLink;
    }

    public void setHiddenCarrierCodeLink(RichLink hiddenCarrierCodeLink) {
        this.hiddenCarrierCodeLink = hiddenCarrierCodeLink;
    }

    public RichLink getHiddenCarrierCodeLink() {
        return hiddenCarrierCodeLink;
    }

    public void setHiddenServiceCodeLink(RichLink hiddenServiceCodeLink) {
        this.hiddenServiceCodeLink = hiddenServiceCodeLink;
    }

    public RichLink getHiddenServiceCodeLink() {
        return hiddenServiceCodeLink;
    }

    public void setHiddenRouteLink(RichLink hiddenRouteLink) {
        this.hiddenRouteLink = hiddenRouteLink;
    }

    public RichLink getHiddenRouteLink() {
        return hiddenRouteLink;
    }

    public void setHiddenDestIdLink(RichLink hiddenDestIdLink) {
        this.hiddenDestIdLink = hiddenDestIdLink;
    }

    public RichLink getHiddenDestIdLink() {
        return hiddenDestIdLink;
    }

    public void setHiddenDoorIdLink(RichLink hiddenDoorIdLink) {
        this.hiddenDoorIdLink = hiddenDoorIdLink;
    }

    public RichLink getHiddenDoorIdLink() {
        return hiddenDoorIdLink;
    }

    public void setHiddenSealNbrLink(RichLink hiddenSealNbrLink) {
        this.hiddenSealNbrLink = hiddenSealNbrLink;
    }

    public RichLink getHiddenSealNbrLink() {
        return hiddenSealNbrLink;
    }

    public void setHiddenLoadTsLink(RichLink hiddenLoadTsLink) {
        this.hiddenLoadTsLink = hiddenLoadTsLink;
    }

    public RichLink getHiddenLoadTsLink() {
        return hiddenLoadTsLink;
    }

    /**
     * Method called when the region loads
     * @param cse
     */
    public void onRegionLoad(ComponentSystemEvent cse) {
        HhShipTrailerSBean pfBean = (HhShipTrailerSBean) this.getPageFlowBean("HhShipTrailerSBean");
        if (pfBean.getRefreshFocusNeeded()) {
            this.switchFocusToField(pfBean.getPage1Focus());
            pfBean.setRefreshFocusNeeded(false);
        }
        if (pfBean.getGlobalErrorMsg() != null) {
            setErrorInSelectedField();
            this.showMessagesPanel(pfBean.getGlobalErrorType(), pfBean.getGlobalErrorMsg());
            pfBean.setGlobalErrorType(null);
            pfBean.setGlobalErrorMsg(null);
        }
    }
}
