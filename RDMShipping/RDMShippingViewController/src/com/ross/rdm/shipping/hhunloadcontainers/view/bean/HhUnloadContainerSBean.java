package com.ross.rdm.shipping.hhunloadcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhUnloadContainerSBean  {
  private static ADFLogger _logger = ADFLogger.createADFLogger(HhUnloadContainerSBean.class);

    private Boolean isContainerIdValidated;
    private String isFocusOn;
    private Boolean isError;
    private Boolean firstInit;

    public void initTaskFlow() {
        _logger.info("initTaskFlow() Start");
        initTaskFlowUnloadContainer();
        firstInit = false;
        setIsFocusOn("ContainerId");
        setIsContainerIdValidated(false);
        _logger.info("initTaskFlow() End");
    }

    private void initTaskFlowUnloadContainer() {
        ADFUtils.findOperation("initTaskFlowUnloadContainer").execute();
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }
}
