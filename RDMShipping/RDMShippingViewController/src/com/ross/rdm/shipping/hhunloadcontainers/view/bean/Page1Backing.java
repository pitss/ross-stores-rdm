package com.ross.rdm.shipping.hhunloadcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.shipping.hhunloadcontainers.model.views.HhUnloadContainerSUnloadContainerViewRowImpl;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {
    private final static String HH_UNLOAD_CONTAINER_FLOW_BEAN = "HhUnloadContainerSBean";
    private final static String CONTAINER_ID_ITERATOR = "HhUnloadContainerSUnloadContainerViewIterator";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String FACILITY_ID_ATTR = "FacilityId";

    private RichPopup confirmationPopup;
    private RichOutputText dialogOutputText;
    private RichDialog exitDialog;

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichInputText containerId;
    private RichIcon containerIdIcon;

    private RichPanelFormLayout unloadContainerPanel;
    private RichLink f3Link;

    public Page1Backing() {

    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, enteredContainerIdValue);
            this.callUnldContainer(enteredContainerIdValue);
        } else {
            this.getContainerIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getContainerIdIcon());
            this.refreshContentOfUIComponent(this.getContainerId());
            this.addErrorStyleToComponent(this.getContainerId());
            this.showMessagesPanel(ERROR, this.getMessage("INV_CONTAINER", ERROR, null, this.getLangCodeAttrValue()));
        }
    }

    private void callUnldContainer(String containerId) {

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callUnldContainer");
        oper.getParamsMap().put("pContainerId", containerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.get(1) != null) {
                    if (ERROR.equals(responseList.get(0))) {
                        this.showMessagesPanel(responseList.get(0), responseList.get(1));
                        this.getContainerIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.selectTextOnUIComponent(this.getContainerId());
                    } else if (WARN.equals(responseList.get(0))) {
                        //Display popup with warning message
                        this.getDialogOutputText().setValue(responseList.get(1));
                        this.getConfirmationPopup().show(new RichPopup.PopupHints());
                    } else if (MSGTYPE_M.equals(responseList.get(0))) {
                        showMessagesPanel(responseList.get(0), responseList.get(1));
                        getContainerIdIcon().setName(REQ_ICON);
                        refreshContentOfUIComponent(this.getContainerIdIcon());
                        refreshContentOfUIComponent(this.getContainerId());
                        removeErrorStyleToComponent(this.getContainerId());
                        selectTextOnUIComponent(this.getContainerId());
                        clearFields();
                    }
                }
            }
        }
    }

    private void callUnldContainerWithDissassociation(String containerId) {

        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callUnldContainerWithDissassocation");
        oper.getParamsMap().put("pContainerId", containerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.get(1) != null) {
                    this.showMessagesPanel(responseList.get(0), responseList.get(1));
                    if (ERROR.equals(responseList.get(0))) {
                        this.getContainerIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.selectTextOnUIComponent(this.getContainerId());
                    } else if (MSGTYPE_M.equals(responseList.get(0))) {
                        showMessagesPanel(responseList.get(0), responseList.get(1));
                        getContainerIdIcon().setName(REQ_ICON);
                        refreshContentOfUIComponent(this.getContainerIdIcon());
                        refreshContentOfUIComponent(this.getContainerId());
                        removeErrorStyleToComponent(this.getContainerId());
                        selectTextOnUIComponent(this.getContainerId());
                    }
                }
            }
        }
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getHhUnloadContainerSPageFlowBean().getIsFocusOn();

        if (clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("ContainerId".equals(currentFocus)) {
                    if (submittedValue != null) {
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    }
                }
            }
            this.refreshContentOfUIComponent(this.getUnloadContainerPanel());
        }
    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            clearFields();
            ADFUtils.invokeAction("backGlobalHome");
        }
    }

    private void noConfirmExitLinkActionListener() {
        this.getConfirmationPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
    }

    private void yesConfirmExitLinkActionListener() {
        this.getConfirmationPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }

    public void yesConfirmExitLinkActionListener(ActionEvent actionEvent) {
        this.getConfirmationPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }

    public void noConfirmExitLinkActionListener(ActionEvent actionEvent) {
        this.getConfirmationPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
    }

    public void performFkeyExitPopup(ClientEvent clientEvent) {
        _logger.info("performKkeyExitPopup() Start");
        if (clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                yesConfirmExitLinkActionListener();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                noConfirmExitLinkActionListener();
            }
        }
        _logger.info("performKkeyExitPopup() End");
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmDialogListener() Start");
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            String containerId = (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
            this.callUnldContainerWithDissassociation(containerId);
        } else {

            this.selectTextOnUIComponent(this.getContainerId());
        }
        _logger.info("confirmDialogListener() End");
    }

    public void hideMessagesPanel() {
        _logger.info("hideMessagesPanel() Start");
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getErrorWarnInfoIcon().setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAllMessagesPanel());
        _logger.info("hideMessagesPanel() End");
    }

    private void clearFields() {
        _logger.info("clearFields() Start");
        this.clearCurrentRow();
        this.getContainerId().resetValue();
        this.refreshContentOfUIComponent(this.getContainerId());
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow() {
        HhUnloadContainerSUnloadContainerViewRowImpl hhRowImpl = this.getHhUnloadContainerSUnloadContainerViewRow();
        if (hhRowImpl != null) {
            hhRowImpl.setContainerId(null);
        }
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private HhUnloadContainerSUnloadContainerViewRowImpl getHhUnloadContainerSUnloadContainerViewRow() {
        return (HhUnloadContainerSUnloadContainerViewRowImpl) ADFUtils.findIterator(CONTAINER_ID_ITERATOR).getCurrentRow();
    }

    private HhUnloadContainerSBean getHhUnloadContainerSPageFlowBean() {
        return ((HhUnloadContainerSBean) this.getPageFlowBean(HH_UNLOAD_CONTAINER_FLOW_BEAN));
    }

    private String getContainerIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }


    public void setUnloadContainerPanel(RichPanelFormLayout unloadContainerPanel) {
        this.unloadContainerPanel = unloadContainerPanel;
    }

    public RichPanelFormLayout getUnloadContainerPanel() {
        return unloadContainerPanel;
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_unload_container_s_UNLOAD_CONTAINER_F3,
                                                    RoleBasedAccessConstants.FORM_NAME_hh_unload_container_s)) {
        this.trKeyIn("F3");
        
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
    }
    
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public static void setLogger(ADFLogger _logger) {
        Page1Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setExitDialog(RichDialog exitDialog) {
        this.exitDialog = exitDialog;
    }

    public RichDialog getExitDialog() {
        return exitDialog;
    }
}
