package com.ross.rdm.shipping.hhunloadteams.view.bean;

import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends HhUnloadTeamsBaseBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichInputText trailerId;
    private RichInputText teamId;
    private RichInputText userId;
    private RichIcon teamIdIcon;
    private RichIcon userIdIcon;
    private RichTable usersTable;
    private RichIcon errorWarnInfoIconPopup;
    private RichOutputText errorWarnInfoMessagePopup;
    private RichPanelGroupLayout allMessagesPanelPopup;
    private RichPopup usersTablePopup;
    private RichPopup confirmDeletePopup;
    private RichOutputText confirmDeletePopupMessage;

    public Page2Backing() {

    }

    public void onChangedUserId(ValueChangeEvent vce) {

    }

    public void onChangedUserIdTeamUsers(ValueChangeEvent vce) {

    }

    private Long executeUsersViewCriteria(String facilityId, String teamId, BigDecimal bolNbr) {
        OperationBinding oper = ADFUtils.findOperation("executeUsers");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("teamId", teamId);
        map.put("bolNbr", bolNbr);
        return (Long) oper.execute();
    }

    private GlobalVariablesViewRowImpl getGlobalVariablesRow() {
        return (GlobalVariablesViewRowImpl) ADFUtils.findIterator("GlobalVariablesViewIterator").getCurrentRow();
    }

    private void setTrailerId(String trailerId) {
        ADFUtils.setBoundAttributeValue("TrailerId", trailerId);
    }

    private void setTeamId(String teamId) {
        ADFUtils.setBoundAttributeValue("Team", teamId);
    }


    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();

        if (bean != null) {
            this.setTrailerId(bean.getTrailerId());
            //this.setTeamId(bean.getTeamId());

            String focus = bean.getIsFocusOn();
            if ("TEAM_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getTeamId());
                this.selectTextOnUIComponent(this.getTeamId());
            } else if ("USER_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getUserId());
                this.selectTextOnUIComponent(this.getUserId());
            }
        }
        _logger.info("onRegionLoad End");
    }

    private void executeTeamUsersViewCriteria() {
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String teamId = (String) ADFUtils.getBoundAttributeValue("Team");
        BigDecimal bolNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("BolNbr");
        this.executeUsersViewCriteria(facilityId, teamId, bolNbr);

    }

    private void callPaddUsers() {
        OperationBinding ob = ADFUtils.findOperation("callPaddUsers");
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    this.setFocusOnActiveComp();
                    this.setErrorOnActiveComp(true);
                }
            } else {
                this.hideMessagesPanel();
                this.callDoCommit();
                this.clearFields();
                this.setDeleteMessageShowing(false);
                this.showTableAndMessageInPopup("SUCCESS_OPER", SUCCESS);
            }
        }
    }

    private void callPdeleteUsers() {
        String teamId = (String) ADFUtils.getBoundAttributeValue("TeamId");
        Integer mstrBolId = (Integer) ADFUtils.getBoundAttributeValue("MstrBolId");
        OperationBinding ob = ADFUtils.findOperation("pDeleteUser");
        Map map = ob.getParamsMap();
        map.put("teamId", teamId);
        map.put("facilityId", (String) ADFUtils.getBoundAttributeValue("FacilityId"));
        map.put("bolId", mstrBolId != null ? mstrBolId.longValue() : null);
        map.put("userId", (String) ADFUtils.getBoundAttributeValue("UserId1"));
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            this.callIsertAuditUnloadUsers(teamId);
            this.hideMessagesPanel();
            this.clearFields();
            this.setDeleteMessageShowing(false);
            this.showTableAndMessageInPopup("SUCCESS_OPER", SUCCESS);
        }
    }

    private void callIsertAuditUnloadUsers(String teamId) {
        OperationBinding ob = ADFUtils.findOperation("callIsertAuditUnloadUsers");
        ob.getParamsMap().put("teamId", (teamId));
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    this.setFocusOnActiveComp();
                    this.setErrorOnActiveComp(true);
                }
            }
        }
    }

    private void setFocusOnActiveComp() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            if ("USER_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getUserId());
                this.selectTextOnUIComponent(this.getUserId());
            } else if ("TEAM_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getTeamId());
                this.selectTextOnUIComponent(this.getTeamId());
            }
        }
    }
    
    private void setFocusOnActiveCompPopupExit() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getFocusOnForm();
            if ("USER_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnUserId();
            } else if ("TEAM_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnTeamId();
            }
        }
    }

    public String f4AddAction() {
        if (this.validateTeamId() && this.validateUserId())
            return this.trKeyIn("F4");
        return null;
    }

    public String f5DeleteAction() {
        return this.trKeyIn("F5");
    }

    public String f3ExitAction() {
        return this.trKeyIn("F3");
    }

    private String trKeyIn(String key) {
        String result = this.callCheckScreenOptionPriv(this.getBlockName() + key, null);
        if (!"OFF".equals(result)) {
            if ("F3".equalsIgnoreCase(key)) {
                return this.exit();
            } else if ("F4".equalsIgnoreCase(key)) {
                this.callPaddUsers();
            } else if ("F5".equalsIgnoreCase(key)) {
                this.processShowingPopups();
            }
        }
        return null;
    }

    private String exit() {
        this.getUnloadTeamsFlowBean().setIsFocusOn("TEAM_ID");
        ADFUtils.setBoundAttributeValue("UserId", null);
        return "page1";
    }

    public String f5DeletePopupAction() {
        this.processShowingPopups();
        return null;
    }
    
    public void f3ExitPopupAction(ActionEvent actionEvent) {
        this.hideTeamUsersTableInPopup();
        this.setFocusOnActiveCompPopupExit();
    }

    private void disableActiveInputText() {
        if (!"TEAM_USERS".equalsIgnoreCase(this.getIsFocuOn())) {
            this.setErrorOnActiveComp(false);
            this.getActiveInputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getActiveInputText());
        }
    }

    private void setFocusOnTeamId() {
        this.disableActiveInputText();
        this.getTeamId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getTeamId());
        this.getUnloadTeamsFlowBean().setIsFocusOn("TEAM_ID");
        this.setFocusOnUIComponent(this.getTeamId());
    }

    private void setFocusOnUserId() {
        this.disableActiveInputText();
        this.getUserId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getUserId());
        this.getUnloadTeamsFlowBean().setIsFocusOn("USER_ID");
        this.setFocusOnUIComponent(this.getUserId());
    }

    private boolean validateTeamId() {
        if (this.isTeamIdEmpty()) {
            this.showPartialEntry();
            this.setFocusOnTeamId();
            this.setErrorOnTeamId(true);
            return false;
        }
        return true;
    }

    private boolean validateUserId() {
        if (this.isUserEmpty()) {
            this.showPartialEntry();
            this.setFocusOnUserId();
            this.setErrorOnUserId(true);
            return false;
        }
        return true;
    }

    private void setErrorOnTeamId(boolean set) {
        this.setErrorStyleOnComponent(this.getTeamId(), this.getTeamIdIcon(), set);
        this.selectTextOnUIComponent(this.getTeamId());
    }

    private void setErrorOnUserId(boolean set) {
        this.setErrorStyleOnComponent(this.getUserId(), this.getUserIdIcon(), set);
        this.selectTextOnUIComponent(this.getUserId());
    }

    private void setErrorOnActiveComp(boolean set) {
        this.setErrorStyleOnComponent(this.getActiveInputText(), this.getActiveIcon(), set);
        this.selectTextOnUIComponent(this.getActiveInputText());
    }

    public RichInputText getActiveInputText() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            switch (focus) {
            case "TEAM_ID":
                return this.getTeamId();
            case "USER_ID":
                return this.getUserId();
            default:
                return null;
            }
        }
        return null;
    }


    public RichIcon getActiveIcon() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            switch (focus) {
            case "TEAM_ID":
                return this.getTeamIdIcon();
            case "USER_ID":
                return this.getUserIdIcon();
            default:
                return null;
            }
        }
        return null;
    }

    private void showPartialEntry() {
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                     this.getAllMessagesPanel(), ERROR, "PARTIAL_ENTRY");
    }

    private boolean isTeamIdEmpty() {
        String team = (String) ADFUtils.getBoundAttributeValue("Team");
        return team == null || StringUtils.isEmpty(team);
    }

    private boolean isUserEmpty() {
        String userId = (String) ADFUtils.getBoundAttributeValue("UserId");
        return null == userId || StringUtils.isEmpty(userId);
    }

    private boolean isTablePopupShowing() {
        return this.getUnloadTeamsFlowBean().isTablePopupShowing();
    }

    private void setTablePopupShowing(boolean set) {
        this.getUnloadTeamsFlowBean().setTablePopupShowing(set);
    }

    public void setBlockName(String block) {
        this.getUnloadTeamsFlowBean().setBlockName(block);
    }

    public String getBlockName() {
        return this.getUnloadTeamsFlowBean().getBlockName();
    }

    private void showTeamUsersTableInPopup() {
        this.executeTeamUsersViewCriteria();
        this.hideMessagesPanel();
        this.disableActiveInputText();
        this.setBlockName("TEAM_USERS");
        this.setTablePopupShowing(true);
        this.getUsersTablePopup().show(new RichPopup.PopupHints());
        this.setFocusOnTeamUsersTable();
    }

    private void showTableAndMessageInPopup(String msg, String msgType) {
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessagePopup(), this.getErrorWarnInfoIconPopup(),
                                     this.getAllMessagesPanelPopup(), msgType, msg);
        this.showTeamUsersTableInPopup();
    }

    public void setDeleteMessageShowing(boolean set) {
        this.getUnloadTeamsFlowBean().setDeleteMessageShowing(set);
    }

    public boolean isDeleteMessageShowing() {
        return this.getUnloadTeamsFlowBean().isDeleteMessageShowing();
    }

    private void setFocusOnTeamUsersTable() {
        this.disableActiveInputText();
        if (!"TEAM_USERS".equalsIgnoreCase(this.getIsFocuOn())) {
            this.setFocusOnForm(this.getIsFocuOn());
        }
        this.setIsFocusOn("TEAM_USERS");
    }

    private void hideTeamUsersTableInPopup() {
        this.setBlockName("UNLOAD_USERS");
        this.setTablePopupShowing(false);
        this.hideMessagesPanel(this.getErrorWarnInfoMessagePopup(), this.getErrorWarnInfoIconPopup(),
                               this.getAllMessagesPanelPopup());
        this.getUsersTablePopup().hide();
    }

    private void processShowingPopups() {
        if (!this.isTablePopupShowing()) {
            this.showTableAndMessageInPopup("SELECT_USER", ERROR);
            this.setDeleteMessageShowing(true);
        } else {
            this.hideTeamUsersTableInPopup();
            this.showDeleteConfirmPopup();
        }
    }


    private void clearFields() {
        ADFUtils.setBoundAttributeValue("UserId", null);
        //ADFUtils.setBoundAttributeValue("Team", null);
        //this.getTeamId().resetValue();
        this.getUserId().resetValue();
        //this.refreshContentOfUIComponent(this.getTeamId());
        this.refreshContentOfUIComponent(this.getUserId());
    }

    public void yesConfirmPopup(ActionEvent actionEvent) {
        this.getConfirmDeletePopup().hide();
        this.callPdeleteUsers();
    }

    public void noConfirmPopup(ActionEvent actionEvent) {
        this.getConfirmDeletePopup().hide();
        this.setFocusOnActiveCompPopupExit();
        //this.setDeleteMessageShowing(true);
        //this.showTableAndMessageInPopup("SELECT_TEAM_DEL", ERROR);
    }

    private void processTabEnterKeys() {
        this.hideMessagesPanel();
        String focus = this.getUnloadTeamsFlowBean().getIsFocusOn();
        if ("USER_ID".equalsIgnoreCase(focus)) {
            this.showTeamUsersTableInPopup();
        } else if ("TEAM_USERS".equalsIgnoreCase(focus)) {
            this.hideTeamUsersTableInPopup();
            this.setFocusOnTeamId();
        } else if ("TEAM_ID".equalsIgnoreCase(focus)) {
            this.setFocusOnUserId();
        }
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                this.executeTeamUsersViewCriteria();
                this.processTabEnterKeys();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                this.trKeyIn("F5");
            }
        }
        _logger.info("performKey() End");
    }

    private int getEstimateRowsUsersIterator() {
        return (int) ADFUtils.findIterator("UsersViewIterator").getEstimatedRowCount();
    }

    public void performKeyOnTeamUserTable(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                this.setDeleteMessageShowing(false);
                this.processTabEnterKeys();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                this.trKeyIn("F5");
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                this.f3ExitPopupAction(null);
            }
        }
        _logger.info("performKey() End");
    }

    private void showDeleteConfirmPopup() {
        this.getConfirmDeletePopupMessage().setValue(this.getMessage("CONFIRM_DEL", "C"));
        this.getConfirmDeletePopup().show(new RichPopup.PopupHints());
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setTeamId(RichInputText teamId) {
        this.teamId = teamId;
    }

    public RichInputText getTeamId() {
        return teamId;
    }

    public void setUserId(RichInputText userId) {
        this.userId = userId;
    }

    public RichInputText getUserId() {
        return userId;
    }

    public void setTeamIdIcon(RichIcon teamIdIcon) {
        this.teamIdIcon = teamIdIcon;
    }

    public RichIcon getTeamIdIcon() {
        return teamIdIcon;
    }

    public void setUserIdIcon(RichIcon userIdIcon) {
        this.userIdIcon = userIdIcon;
    }

    public RichIcon getUserIdIcon() {
        return userIdIcon;
    }


    public void setUsersTable(RichTable usersTable) {
        this.usersTable = usersTable;
    }

    public RichTable getUsersTable() {
        return usersTable;
    }


    public void setErrorWarnInfoIconPopup(RichIcon errorWarnInfoIconPopup) {
        this.errorWarnInfoIconPopup = errorWarnInfoIconPopup;
    }

    public RichIcon getErrorWarnInfoIconPopup() {
        return errorWarnInfoIconPopup;
    }

    public void setErrorWarnInfoMessagePopup(RichOutputText errorWarnInfoMessagePopup) {
        this.errorWarnInfoMessagePopup = errorWarnInfoMessagePopup;
    }

    public RichOutputText getErrorWarnInfoMessagePopup() {
        return errorWarnInfoMessagePopup;
    }

    public void setAllMessagesPanelPopup(RichPanelGroupLayout allMessagesPanelPopup) {
        this.allMessagesPanelPopup = allMessagesPanelPopup;
    }

    public RichPanelGroupLayout getAllMessagesPanelPopup() {
        return allMessagesPanelPopup;
    }

    public void setUsersTablePopup(RichPopup usersTablePopup) {
        this.usersTablePopup = usersTablePopup;
    }

    public RichPopup getUsersTablePopup() {
        return usersTablePopup;
    }

    public void setConfirmDeletePopup(RichPopup confirmDeletePopup) {
        this.confirmDeletePopup = confirmDeletePopup;
    }

    public RichPopup getConfirmDeletePopup() {
        return confirmDeletePopup;
    }

    public void setConfirmDeletePopupMessage(RichOutputText confirmDeletePopupMessage) {
        this.confirmDeletePopupMessage = confirmDeletePopupMessage;
    }

    public RichOutputText getConfirmDeletePopupMessage() {
        return confirmDeletePopupMessage;
    }

}
