package com.ross.rdm.shipping.hhunloadteams.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhUnloadTeamSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhUnloadTeamSBean.class);
    private String isFocusOn;
    private String focusOnForm;
    private boolean errorOnSelectedItem;
    private boolean tablePopupShowing;
    private boolean teamAllowedInsert;
    private boolean ctnsUnloadedAllwoedInsert;
    private boolean regionLoaded;
    private String blockName;
    private boolean deleteMessageShowing;
    private BigDecimal bolNbr;
    private String trailerId;
    private String facilityId;
    private String unloadActivity;
    private BigDecimal totalCnt;
    private String callingForm;
    private String teamId;
    private boolean callCloseContainer;
 

    public void initTaskFlow() {
        this.callIniTaskFlow();
        this.setTeamAllowedInsert(false);
        this.setCtnsUnloadedAllwoedInsert(false);
        this.setRegionLoaded(false);
        this.setDeleteMessageShowing(false);
        this.setIsFocusOn("TEAM_ID");
        this.setBlockName("UNLOAD_TEAM");
    }

    private void callIniTaskFlow() {
        ADFUtils.findOperation("iniTaskFlowUnloadTeams").execute();
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setErrorOnSelectedItem(boolean errorOnSelectedItem) {
        this.errorOnSelectedItem = errorOnSelectedItem;
    }

    public boolean isErrorOnSelectedItem() {
        return errorOnSelectedItem;
    }

    public void setTablePopupShowing(boolean popupShowing) {
        this.tablePopupShowing = popupShowing;
    }

    public boolean isTablePopupShowing() {
        return tablePopupShowing;
    }

    public void setTeamAllowedInsert(boolean teamAllowedInsert) {
        this.teamAllowedInsert = teamAllowedInsert;
    }

    public boolean isTeamAllowedInsert() {
        return teamAllowedInsert;
    }

    public void setCtnsUnloadedAllwoedInsert(boolean ctnsUnloadedAllwoedInsert) {
        this.ctnsUnloadedAllwoedInsert = ctnsUnloadedAllwoedInsert;
    }

    public boolean isCtnsUnloadedAllwoedInsert() {
        return ctnsUnloadedAllwoedInsert;
    }

    public void setRegionLoaded(boolean regionLoaded) {
        this.regionLoaded = regionLoaded;
    }

    public boolean isRegionLoaded() {
        return regionLoaded;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setDeleteMessageShowing(boolean deletePopupShowing) {
        this.deleteMessageShowing = deletePopupShowing;
    }

    public boolean isDeleteMessageShowing() {
        return deleteMessageShowing;
    }

    public void setBolNbr(BigDecimal bolNbr) {
        this.bolNbr = bolNbr;
    }

    public BigDecimal getBolNbr() {
        return bolNbr;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setUnloadActivity(String unloadActivity) {
        this.unloadActivity = unloadActivity;
    }

    public String getUnloadActivity() {
        return unloadActivity;
    }

    public void setTotalCnt(BigDecimal totalCnt) {
        this.totalCnt = totalCnt;
    }

    public BigDecimal getTotalCnt() {
        return totalCnt;
    }

    public void setCallingForm(String callingForm) {
        this.callingForm = callingForm;
    }

    public String getCallingForm() {
        return callingForm;
    }
    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setCallCloseContainer(boolean callCloseContainer) {
        this.callCloseContainer = callCloseContainer;
    }

    public boolean getCallCloseContainer() {
        return callCloseContainer;
    }

    public void setFocusOnForm(String focusOnForm) {
        this.focusOnForm = focusOnForm;
    }

    public String getFocusOnForm() {
        return focusOnForm;
    }
}
