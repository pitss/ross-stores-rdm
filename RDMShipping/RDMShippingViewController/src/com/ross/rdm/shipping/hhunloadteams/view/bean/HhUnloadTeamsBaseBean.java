package com.ross.rdm.shipping.hhunloadteams.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.util.List;

import javax.faces.component.UIComponent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class HhUnloadTeamsBaseBean extends RDMShippingBackingBean {
    public HhUnloadTeamsBaseBean() {
        super();
    }
    static final String HH_UNLOAD_TEAM_PAGE_FLOW_BEAN = "HhUnloadTeamSBean";
    static final String ERROR = "E";
    static final String WARN = "W";
    static final String INFO = "I";
    static final String CONF = "C";
    static final String SUCCESS = "S";
    static final String MSGTYPE_M = "M";
    static final String REQ_ICON = "required";
    static final String ERR_ICON = "error";
    static final String WRN_ICON = "warning";
    static final String INF_ICON = "info";
    static final String SCC_ICON = "logo";
    static final String ATTR_FACILITY_ID = "FacilityId";
    static final String ATTR_LANGUAGE_CODE = "LanguageCode";
    
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);


    public void invokeOperation(String operation) {
        OperationBinding ob = ADFUtils.findOperation(operation);
        if (ob != null)
            ob.execute();
    }

    public HhUnloadTeamSBean getUnloadTeamsFlowBean() {
        return (HhUnloadTeamSBean) AdfFacesContext.getCurrentInstance().getPageFlowScope().get(HH_UNLOAD_TEAM_PAGE_FLOW_BEAN);
    }

    public void getAndShowMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                        RichPanelGroupLayout allMessagesPanel, String messageType, String msgCode) {
        String msg = this.getMessage(msgCode, messageType);
        this.showMessagesPanel(errorWarnInfoMessage, errorWarnInfoIcon, allMessagesPanel, messageType, msg);
    }

    public String getMessage(String msgCode, String messageType) {
        return this.getMessage(msgCode, messageType, this.getBoundAttribute(ATTR_FACILITY_ID),
                               this.getBoundAttribute(ATTR_LANGUAGE_CODE));
    }

    public void showMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                  RichPanelGroupLayout allMessagesPanel, String messageType, String msg) {
        errorWarnInfoMessage.setValue(null);
        errorWarnInfoIcon.setName(null);
        allMessagesPanel.setVisible(false);

        if (WARN.equals(messageType)) {
            errorWarnInfoIcon.setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            errorWarnInfoIcon.setName(INF_ICON);
        } else {
            if (ERROR.equals(messageType)) {
                errorWarnInfoIcon.setName(ERR_ICON);
            } else {
                if (SUCCESS.equals(messageType)) {
                    errorWarnInfoIcon.setName(SCC_ICON);
                }
            }
        }
        errorWarnInfoMessage.setValue(msg);
        allMessagesPanel.setVisible(true);
        refreshContentOfUIComponent(allMessagesPanel);
    }
    
    public void hideMessagesPanel(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                  RichPanelGroupLayout allMessagesPanel) {
        errorWarnInfoMessage.setValue(null);
        errorWarnInfoIcon.setName(null);
        refreshContentOfUIComponent(allMessagesPanel);
    }
  

    public String getBoundAttribute(String attrName) {
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String) val;
        return null;
    }


    public void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set) {
        if (set) {
            this.addErrorStyleToComponent(uiComponent);
            icon.setName(ERR_ICON);

        } else {
            icon.setName(REQ_ICON);
            this.removeErrorStyleToComponent(uiComponent);
        }
        this.refreshContentOfUIComponent(icon);
        this.refreshContentOfUIComponent(uiComponent);
    }
    
    public String callCheckScreenOptionsPrivBase(String optionName, String formName){
        _logger.info("callCheckScreenOptionPriv() Start");
        _logger.fine("optionName value: " + optionName + "\n" + "formName value: " + formName);
        String result = "OFF";
        String userId = (String) ADFUtils.getBoundAttributeValue("User");
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String languageCOde = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
        OperationBinding oper = ADFUtils.findOperation("callCheckScreenOptionPriv");
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("userId", userId);
        oper.getParamsMap().put("formName", formName);
        oper.getParamsMap().put("optionName", optionName);
        result = (String) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if ("OFF".equals(result)) {
                this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                       this.getAllMessagesPanel(), WARN,
                                       this.getMessage("NOT_ALLOWED", WARN, facilityId, languageCOde));
            }
        }

        _logger.fine("result value: " + result);
        _logger.info("callCheckScreenOptionPriv() End");
        return result;
    }

    public boolean processMessages(RichOutputText errorWarnInfoMessage, RichIcon errorWarnInfoIcon,
                                   RichPanelGroupLayout allMessagesPanel, List<String> errors) {
        boolean hasErrors = false;
        if (errors != null && !errors.isEmpty()) {
            this.showMessagesPanel(errorWarnInfoMessage, errorWarnInfoIcon, allMessagesPanel, errors.get(0),
                                   errors.get(1));
            hasErrors = true;
        } else {
            this.hideMessagesPanel(errorWarnInfoMessage, errorWarnInfoIcon, allMessagesPanel);
        }
        return hasErrors;
    }

    public void setIsFocusOn(String comp) {
        this.getUnloadTeamsFlowBean().setIsFocusOn(comp);
    }

    public String getIsFocuOn() {
        return this.getUnloadTeamsFlowBean().getIsFocusOn();
    }
    
    public void setFocusOnForm(String comp) {
        this.getUnloadTeamsFlowBean().setFocusOnForm(comp);
    }

    public String getFocusOnForm() {
        return this.getUnloadTeamsFlowBean().getFocusOnForm();
    }

    public void setFocusOnTable(UIComponent component, String focusFunc) {
        StringBuilder script = new StringBuilder("");
        script.append(focusFunc);
        this.writeJavaScriptToClient(script.toString());
        this.refreshContentOfUIComponent(component);
    }

    public void callDoCommit() {
        OperationBinding ob = ADFUtils.findOperation("callDoCommit");
        ob.execute();
    }
    
    @Override
    public void hideMessagesPanel() {
        this.hideMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(), this.getAllMessagesPanel());
    }
    
    protected String callCheckScreenOptionPriv(String optionName, String formName) {
        String result = "OFF";
        String userId = (String) ADFUtils.getBoundAttributeValue("User");
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String languageCOde = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
        OperationBinding oper = ADFUtils.findOperation("callCheckScreenOptionPriv");
        oper.getParamsMap().put("facilityId", facilityId);
        oper.getParamsMap().put("userId", userId);
        oper.getParamsMap().put("formName", formName);
        oper.getParamsMap().put("optionName", optionName);
        result = (String) oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if ("OFF".equals(result)) {
                this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                       this.getAllMessagesPanel(), WARN,
                                       this.getMessage("NOT_ALLOWED", WARN, facilityId, languageCOde));
            }
        }
        return result;
    }
}
