package com.ross.rdm.shipping.hhunloadteams.view.bean;


import com.ross.rdm.common.model.views.GlobalVariablesViewRowImpl;
import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.hhunloadteams.model.views.HhUnloadTeamSWorkLocalViewRowImpl;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends HhUnloadTeamsBaseBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPanelGroupLayout allMessagesPanelPopup;
    private RichIcon errorWarnInfoIconPopup;
    private RichOutputText errorWarnInfoMessagePopup;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private RichInputText trailerId;
    private RichInputText teamId;
    private RichInputText ctnsUnloaded;
    private RichIcon trailerIdIcone;
    private RichIcon teamIdIcone;
    private RichIcon ctnsUnldIcone;
    private RichTable teamCartonTable;
    private RichPopup teamCartonTablePopup;
    private RichOutputText confirmDeletePopupMessage;
    private RichPopup confirmDeletePopup;


    public Page1Backing() {

    }

    public void onChangedTeam(ValueChangeEvent vce) {

    }

    public void onChangedCtnsUnloaded(ValueChangeEvent vce) {

    }


    private void setCnsUnloadedValue(String newCtnsUnloaded) {
        ADFUtils.setBoundAttributeValue("CtnsUnloaded", new BigDecimal(newCtnsUnloaded));
        this.setErrorOnCnsUnloaded(false);
    }

    private boolean checkIfNumeric(String nbr) {
        return NumberUtils.isNumber(nbr);
    }

    private void showMustBeNumericMsg() {
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                     this.getAllMessagesPanel(), ERROR, "MUST_BE_NUMERIC");
        this.setErrorOnCnsUnloaded(true);
    }


    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        this.setFocusOnActiveComp();
        if (!this.getIsRegionLoaded()) {
            HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
            String facilityId = bean.getFacilityId();
            String trailerId = bean.getTrailerId();
            BigDecimal bolNbr = bean.getBolNbr();
            String callingForm = bean.getCallingForm();
            if (null != callingForm && "HH_DCTODC_RCVNG_S".equalsIgnoreCase(callingForm)) {
                this.setTrailerId(trailerId);
                this.setWorkLocalVariables();
                if ((this.callCheckTrailerStatusExecute(facilityId, trailerId)) > 0)
                    this.setInsertionsAllowed();
                this.executeTeamCartonViewCriteria(facilityId, trailerId, bolNbr);
                this.setIsRegionLoaded(true);
            }
        }
        _logger.info("onRegionLoad End");
    }


    public void yesConfirmPopup(ActionEvent actionEvent) {
        this.getConfirmDeletePopup().hide();
        this.callPdeleteTeam();
    }

    public void noConfirmPopup(ActionEvent actionEvent) {
        this.getConfirmDeletePopup().hide();
        this.setFocusOnActiveCompPopupExit();
        //this.setDeleteMessageShowing(true);
        //this.showTableAndMessageInPopup("SELECT_TEAM_DEL", ERROR);
    }

    private void callPaddTeam() {
        OperationBinding ob = ADFUtils.findOperation("callPaddTeam");
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    this.setFocusOnActiveComp();
                    this.setErrorOnActiveComp(true);
                }
            } else {
                this.hideMessagesPanel();
                this.callDoCommit();
                this.clearFields();
                this.executeTeamCartonViewCriteria();
                this.setDeleteMessageShowing(false);
                this.showTableAndMessageInPopup("SUCCESS_OPER", SUCCESS);
            }
        }
    }

    private void callPdeleteTeam() {
        String teamId = (this.getTeamFromCartonRow()).toUpperCase();
        OperationBinding ob = ADFUtils.findOperation("pDeleteTeam");
        Map map = ob.getParamsMap();
        map.put("teamId", teamId);
        map.put("facilityId", (String) ADFUtils.getBoundAttributeValue("FacilityId"));
        map.put("bolId", ((BigDecimal) ADFUtils.getBoundAttributeValue("BolNbr")).longValue());
        map.put("trailerId", (String) ADFUtils.getBoundAttributeValue("TrailerId1"));
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            this.callInsertAudit(teamId);
            this.hideMessagesPanel();
            this.clearFields();
            this.executeTeamCartonViewCriteria();
            this.setDeleteMessageShowing(false);
            this.showTableAndMessageInPopup("SUCCESS_OPER", SUCCESS);

        }
    }

    private void callInsertAudit(String teamId) {
        OperationBinding ob = ADFUtils.findOperation("callIsertAuditUnloadTeam");
        ob.getParamsMap().put("teamId", (teamId));
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    this.setFocusOnActiveComp();
                    this.setErrorOnActiveComp(true);
                }
            }
        }
    }

    private String getTeamFromCartonRow() {
        Row current = ADFUtils.findIterator("TeamCartonViewIterator").getCurrentRow();
        return current != null ? (String) current.getAttribute("TeamId") : null;
    }

    private void clearFields() {
        ADFUtils.setBoundAttributeValue("CtnUnloaded", null);
        ADFUtils.setBoundAttributeValue("Team", null);
        this.getTeamId().resetValue();
        this.getCtnsUnloaded().resetValue();
        this.refreshContentOfUIComponent(this.getTeamId());
        this.refreshContentOfUIComponent(this.getCtnsUnloaded());
    }

    private void executeTeamCartonViewCriteria() {
        String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
        String trailerId = (String) ADFUtils.getBoundAttributeValue("TrailerId1");
        BigDecimal bolNbr = (BigDecimal) ADFUtils.getBoundAttributeValue("BolNbr");
        this.executeTeamCartonViewCriteria(facilityId, trailerId, bolNbr);
    }

    private void showTableAndMessageInPopup(String msg, String msgType) {
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessagePopup(), this.getErrorWarnInfoIconPopup(),
                                     this.getAllMessagesPanelPopup(), msgType, msg);
        this.showTeamCartonTableInPopup();
    }

    private boolean callCheckTeamTrailer() {
        OperationBinding ob = ADFUtils.findOperation("callCheckTeamTrailer");
        ob.execute();
        if (ob.getErrors() == null || ob.getErrors().isEmpty()) {
            List errors = (List) ob.getResult();
            if (errors != null && !errors.isEmpty()) {
                String errorType = (String) errors.get(0);
                String error = (String) errors.get(1);
                if (ERROR.equalsIgnoreCase(errorType) || WARN.equalsIgnoreCase(errorType)) {
                    this.showMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                           this.getAllMessagesPanel(), errorType, error);
                    this.setFocusOnActiveComp();
                    this.setErrorOnActiveComp(true);
                    return false;
                }
            }
        }
        this.hideMessagesPanel();
        return true;
    }

    private void setFocusOnActiveComp() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            if ("TEAM_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getTeamId());
                this.selectTextOnUIComponent(this.getTeamId());
            } else if ("CTNS_UNLD".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getCtnsUnloaded());
                this.selectTextOnUIComponent(this.getCtnsUnloaded());
            }
        }
    }
    
    private void setFocusOnActiveCompPopupExit() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getFocusOnForm();
            if ("TEAM_ID".equalsIgnoreCase(focus)) {
                this.setFocusOnTeamId();
            } else if ("CTNS_UNLD".equalsIgnoreCase(focus)) {
                this.setFocusOnCnsUnloaded();
            }
        }
    }

    public void performKey(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (this.isCtnsUnloadField(submittedValue)) {
                    if (this.validateCtnsUnloaded(submittedValue))
                        this.processTabEnterKeys();
                } else
                    this.processTabEnterKeys();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                this.trKeyIn("F5");
            }
        }
        _logger.info("performKey() End");
    }

    private String trKeyIn(String key) {
        String result = this.callCheckScreenOptionPriv(this.getBlockName() + key, null);
        if (!"OFF".equals(result)) {
            if ("F6".equalsIgnoreCase(key)) {
                return this.setParamsPage2Check();
            } else if ("F3".equalsIgnoreCase(key)) {
                if (this.callCheckTeamTrailer()) {
                    this.setIsRegionLoaded(false);
                    return "backGlobalHome";
                }
            } else if ("F4".equalsIgnoreCase(key)) {
                this.callPaddTeam();
            } else if ("F7".equalsIgnoreCase(key)) {
                this.setIsRegionLoaded(false);
                HhUnloadTeamSBean bean = (HhUnloadTeamSBean)this.getPageFlowBean("HhUnloadTeamSBean");
                bean.setCallCloseContainer(true);
                return "backGlobalHome";
            } else if ("F5".equalsIgnoreCase(key)) {
                this.processShowingPopups();
            }
        }
        return null;
    }

    public String f4AddAction() {
        if (this.validateTeamId() && this.validateCtnsUnloaded())
            return this.trKeyIn("F4");
        return null;
    }

    public String f5DeleteAction() {
        return this.trKeyIn("F5");
    }

    public String f3ExitAction() {
        return this.trKeyIn("F3");
    }

    public String f6UsersAction() {
        return this.trKeyIn("F6");
    }

    public String f7SkipAction() {
        return this.trKeyIn("F7");
    }


    public void setBlockName(String block) {
        this.getUnloadTeamsFlowBean().setBlockName(block);
    }

    public String getBlockName() {
        return this.getUnloadTeamsFlowBean().getBlockName();
    }

    private String setParamsPage2Check() {
        if (this.isTeamIdEmpty()) {
            this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                         this.getAllMessagesPanel(), ERROR, "SELECT_TEAM");
            this.setFocusOnTeamId();
            this.setErrorOnActiveComp(true);
            return null;
        }
        this.setIsFocusOn("TEAM_ID");
        this.setIsRegionLoaded(false);
        this.setBlockName("UNLOAD_USERS");
        String team = (String) ADFUtils.getBoundAttributeValue("Team");
        this.setTeamIdOnPageFlow(team);
        ADFUtils.setBoundAttributeValue("UnloadUsersTeam", team);
        return "page2";
    }

    public void performKeyOnCartonTable(ClientEvent clientEvent) {
        _logger.info("performKey() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                this.setDeleteMessageShowing(false);
                this.processTabEnterKeys();
            } else if (F5_KEY_CODE.equals(keyPressed)) {
                this.trKeyIn("F5");
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                this.f3ExitPopupAction(null);
            }
        }
        _logger.info("performKey() End");
    }

    private void processShowingPopups() {
        if (!this.isTablePopupShowing()) {
            this.showTableAndMessageInPopup("SELECT_TEAM_DEL", ERROR);
            this.setDeleteMessageShowing(true);
        } else {
            this.hideTeamCartonTableInPopup();
            this.showDeleteConfirmPopup();
        }
    }

    private void setTrailerId(String trailerId) {
        ADFUtils.setBoundAttributeValue("TrailerId1", trailerId);
    }

    private void setTeamIdOnPageFlow(String team) {
        this.getUnloadTeamsFlowBean().setTeamId(team);

    }

    private void setIsRegionLoaded(boolean set) {
        this.getUnloadTeamsFlowBean().setRegionLoaded(set);
    }

    private boolean getIsRegionLoaded() {
        return this.getUnloadTeamsFlowBean().isRegionLoaded();
    }

    private boolean isCtnsUnloadField(String submittedValue) {
        return "CTNS_UNLD".equalsIgnoreCase(this.getIsFocuOn()) && StringUtils.isNotEmpty(submittedValue);
    }

    public String f5DeletePopupAction() {
        this.processShowingPopups();
        return null;
    }
    
    public void f3ExitPopupAction(ActionEvent actionEvent) {
        this.hideTeamCartonTableInPopup();
        this.setFocusOnActiveCompPopupExit();
    }

    private void processTabEnterKeys() {
        this.hideMessagesPanel();
        String focus = this.getUnloadTeamsFlowBean().getIsFocusOn();
        if ("CTNS_UNLD".equalsIgnoreCase(focus)) {
            if (this.getEstimateRowsTeamCartonIterator() > 0)
                this.showTeamCartonTableInPopup();
            else
                this.setFocusOnTeamId();
        } else if ("CARTON_TEAM".equalsIgnoreCase(focus)) {
            this.hideTeamCartonTableInPopup();
            ADFUtils.setBoundAttributeValue("Team", this.getTeamFromCartonRow());
            this.setFocusOnTeamId();
        } else if ("TEAM_ID".equalsIgnoreCase(focus)) {
            this.setFocusOnCnsUnloaded();
        }
    }


    private void showTeamCartonTableInPopup() {
        this.executeTeamCartonViewCriteria();
        this.hideMessagesPanel();
        this.disableActiveInputText();
        this.setBlockName("TEAM_CARTON");
        this.setTablePopupShowing(true);
        this.getTeamCartonTablePopup().show(new RichPopup.PopupHints());
        this.setFocusOnTeamCartonTable();
    }

    private void hideTeamCartonTableInPopup() {
        this.setBlockName("UNLOAD_TEAM");
        this.setTablePopupShowing(false);
        this.hideMessagesPanel(this.getErrorWarnInfoMessagePopup(), this.getErrorWarnInfoIconPopup(),
                               this.getAllMessagesPanelPopup());
        this.getTeamCartonTablePopup().hide();
    }

    private void showDeleteConfirmPopup() {
        this.getConfirmDeletePopupMessage().setValue(this.getMessage("CONFIRM_DEL", CONF));
        this.getConfirmDeletePopup().show(new RichPopup.PopupHints());
    }


    private void setFocusOnTeamCartonTable() {
        this.disableActiveInputText();
        if (!"CARTON_TEAM".equalsIgnoreCase(this.getIsFocuOn())) {
            this.setFocusOnForm(this.getIsFocuOn());
        }
        this.setIsFocusOn("CARTON_TEAM");
    }

    private void setFocusOnTeamId() {
        this.disableActiveInputText();
        this.getTeamId().setDisabled(false);
        this.refreshContentOfUIComponent(this.getTeamId());
        this.getUnloadTeamsFlowBean().setIsFocusOn("TEAM_ID");
        this.setFocusOnUIComponent(this.getTeamId());
    }

    private void setFocusOnCnsUnloaded() {
        this.disableActiveInputText();
        this.getCtnsUnloaded().setDisabled(false);
        this.refreshContentOfUIComponent(this.getCtnsUnloaded());
        this.getUnloadTeamsFlowBean().setIsFocusOn("CTNS_UNLD");
        this.setFocusOnUIComponent(this.getCtnsUnloaded());
    }

    private void disableActiveInputText() {
        if (!"CARTON_TEAM".equalsIgnoreCase(this.getIsFocuOn())) {
            this.setErrorOnActiveComp(false);
            this.getActiveInputText().setDisabled(true);
            this.refreshContentOfUIComponent(this.getActiveInputText());
        }
    }

    private void setErrorOnActiveComp(boolean set) {
        this.setErrorStyleOnComponent(this.getActiveInputText(), this.getActiveIcon(), set);
        this.selectTextOnUIComponent(this.getActiveInputText());
    }

    private void setErrorOnTeamId(boolean set) {
        this.setErrorStyleOnComponent(this.getTeamId(), this.getTeamIdIcone(), set);
        this.selectTextOnUIComponent(this.getTeamId());
    }

    private void setErrorOnCnsUnloaded(boolean set) {
        this.setErrorStyleOnComponent(this.getCtnsUnloaded(), this.getCtnsUnldIcone(), set);
        this.selectTextOnUIComponent(this.getCtnsUnloaded());
    }

    private void setInsertionsAllowed() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        bean.setCtnsUnloadedAllwoedInsert(true);
        bean.setTeamAllowedInsert(true);
    }

    private int getEstimateRowsTeamCartonIterator() {
        return (int) ADFUtils.findIterator("TeamCartonViewIterator").getEstimatedRowCount();
    }


    public RichInputText getActiveInputText() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            switch (focus) {
            case "TEAM_ID":
                return this.getTeamId();
            case "CTNS_UNLD":
                return this.getCtnsUnloaded();
            default:
                return null;
            }
        }
        return null;
    }


    public RichIcon getActiveIcon() {
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        if (bean != null) {
            String focus = bean.getIsFocusOn();
            switch (focus) {
            case "TEAM_ID":
                return this.getTeamIdIcone();
            case "CTNS_UNLD":
                return this.getCtnsUnldIcone();
            default:
                return null;
            }
        }
        return null;
    }

    private Long callCheckTrailerStatusExecute(String facilityId, String trailerId) {
        OperationBinding oper = ADFUtils.findOperation("executeCheckTrailerStatus");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("trailerId", trailerId);
        return (Long) oper.execute();
    }

    private void executeTeamCartonViewCriteria(String facilityId, String trailerId, BigDecimal bolNbr) {
        OperationBinding oper = ADFUtils.findOperation("executeTeamCarton");
        Map map = oper.getParamsMap();
        map.put("facilityId", facilityId);
        map.put("trailerId", trailerId);
        map.put("bolNbr", bolNbr);
        oper.execute();
    }

    private void setWorkLocalVariables() {
        HhUnloadTeamSWorkLocalViewRowImpl hhUnloadTeamSWorkLocalViewRowImpl = this.getUnloadTeamsWorkLocalRow();
        HhUnloadTeamSBean bean = this.getUnloadTeamsFlowBean();
        hhUnloadTeamSWorkLocalViewRowImpl.setTrailerId(bean.getTrailerId());
        hhUnloadTeamSWorkLocalViewRowImpl.setBolNbr((bean.getBolNbr()));
        hhUnloadTeamSWorkLocalViewRowImpl.setUnloadActivity(bean.getUnloadActivity());
        hhUnloadTeamSWorkLocalViewRowImpl.setTotalCnt(bean.getTotalCnt());
    }

    private GlobalVariablesViewRowImpl getGlobalVariablesRow() {
        return (GlobalVariablesViewRowImpl) ADFUtils.findIterator("GlobalVariablesViewIterator").getCurrentRow();
    }

    private HhUnloadTeamSWorkLocalViewRowImpl getUnloadTeamsWorkLocalRow() {
        return (HhUnloadTeamSWorkLocalViewRowImpl) ADFUtils.findIterator("HhUnloadTeamSWorkLocalViewIterator").getCurrentRow();
    }


    private void showPartialEntry() {
        this.getAndShowMessagesPanel(this.getErrorWarnInfoMessage(), this.getErrorWarnInfoIcon(),
                                     this.getAllMessagesPanel(), ERROR, "PARTIAL_ENTRY");
    }

    private boolean validateCtnsUnloaded(String newCtnsUnloaded) {
        if (newCtnsUnloaded != null && StringUtils.isNotEmpty(newCtnsUnloaded)) {
            if (!this.checkIfNumeric(newCtnsUnloaded)) {
                this.showMustBeNumericMsg();
                return false;
            } else
                this.setCnsUnloadedValue(newCtnsUnloaded);
        }
        return true;
    }

    private boolean validateTeamId() {
        if (this.isTeamIdEmpty()) {
            this.showPartialEntry();
            this.setFocusOnTeamId();
            this.setErrorOnTeamId(true);
            return false;
        } else
            return true;
    }

    private boolean validateCtnsUnloaded() {
        if (this.isCtnsUnloadedEmpty()) {
            this.showPartialEntry();
            this.setFocusOnCnsUnloaded();
            this.setErrorOnCnsUnloaded(true);
            return false;
        } else if (this.validateCtnsUnloaded((String) ADFUtils.getBoundAttributeValue("CtnUnloaded"))) {
            return true;
        } else
            return false;
    }

    private boolean isTeamIdEmpty() {
        String team = (String) ADFUtils.getBoundAttributeValue("Team");
        return team == null || StringUtils.isEmpty(team);
    }

    private boolean isCtnsUnloadedEmpty() {
        String ctns = (String) ADFUtils.getBoundAttributeValue("CtnUnloaded");
        return null == ctns || StringUtils.isEmpty(ctns);
    }

    private boolean isTablePopupShowing() {
        return this.getUnloadTeamsFlowBean().isTablePopupShowing();
    }

    private void setTablePopupShowing(boolean set) {
        this.getUnloadTeamsFlowBean().setTablePopupShowing(set);
    }

    public void setDeleteMessageShowing(boolean set) {
        this.getUnloadTeamsFlowBean().setDeleteMessageShowing(set);
    }

    public boolean isDeleteMessageShowing() {
        return this.getUnloadTeamsFlowBean().isDeleteMessageShowing();
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setTeamId(RichInputText teamId) {
        this.teamId = teamId;
    }

    public RichInputText getTeamId() {
        return teamId;
    }

    public void setCtnsUnloaded(RichInputText ctnsUnloaded) {
        this.ctnsUnloaded = ctnsUnloaded;
    }

    public RichInputText getCtnsUnloaded() {
        return ctnsUnloaded;
    }

    public void setTrailerIdIcone(RichIcon trailerIdIcone) {
        this.trailerIdIcone = trailerIdIcone;
    }

    public RichIcon getTrailerIdIcone() {
        return trailerIdIcone;
    }

    public void setTeamIdIcone(RichIcon teamIdIcone) {
        this.teamIdIcone = teamIdIcone;
    }

    public RichIcon getTeamIdIcone() {
        return teamIdIcone;
    }

    public void setCtnsUnldIcone(RichIcon ctnsUnldIcone) {
        this.ctnsUnldIcone = ctnsUnldIcone;
    }

    public RichIcon getCtnsUnldIcone() {
        return ctnsUnldIcone;
    }


    public void setTeamCartonTable(RichTable teamCartonTable) {
        this.teamCartonTable = teamCartonTable;
    }

    public RichTable getTeamCartonTable() {
        return teamCartonTable;
    }

    public void setTeamCartonTablePopup(RichPopup teamCartonTablePopup) {
        this.teamCartonTablePopup = teamCartonTablePopup;
    }

    public RichPopup getTeamCartonTablePopup() {
        return teamCartonTablePopup;
    }

    public void setAllMessagesPanelPopup(RichPanelGroupLayout allMessagesPanelPopup) {
        this.allMessagesPanelPopup = allMessagesPanelPopup;
    }

    public RichPanelGroupLayout getAllMessagesPanelPopup() {
        return allMessagesPanelPopup;
    }

    public void setErrorWarnInfoIconPopup(RichIcon errorWarnInfoIconPopup) {
        this.errorWarnInfoIconPopup = errorWarnInfoIconPopup;
    }

    public RichIcon getErrorWarnInfoIconPopup() {
        return errorWarnInfoIconPopup;
    }

    public void setErrorWarnInfoMessagePopup(RichOutputText errorWarnInfoMessagePopup) {
        this.errorWarnInfoMessagePopup = errorWarnInfoMessagePopup;
    }

    public RichOutputText getErrorWarnInfoMessagePopup() {
        return errorWarnInfoMessagePopup;
    }


    public void setConfirmDeletePopupMessage(RichOutputText confirmDeletePopupMessage) {
        this.confirmDeletePopupMessage = confirmDeletePopupMessage;
    }

    public RichOutputText getConfirmDeletePopupMessage() {
        return confirmDeletePopupMessage;
    }

    public void setConfirmDeletePopup(RichPopup confirmDeletePopup) {
        this.confirmDeletePopup = confirmDeletePopup;
    }

    public RichPopup getConfirmDeletePopup() {
        return confirmDeletePopup;
    }


}
