package com.ross.rdm.shipping.hhloadcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhLoadContainerSBean extends RDMShippingBackingBean {
  private static ADFLogger _logger = ADFLogger.createADFLogger(HhLoadContainerSBean.class);
    
    private Boolean isTrailerValidated;
    private Boolean isDoorValidated;
    private Boolean isContainerIdValidated;
    private String isFocusOn;
    private Boolean isError;
    private Boolean openWipErrorFlag = false;
    private Boolean unfinishedWipErrorFlag = false;
    private String messageType;
    private final static String initTaskFlowLoadContainer= "initTaskFlowLoadContainer";
    public void initTaskFlow() {
        _logger.info("initTaskFlow() Start");
        initTaskFlowLoadContainer();
        setIsFocusOn("TrailerId");
        setIsContainerIdValidated(false);
        setIsTrailerValidated(false);
        setIsDoorValidated(false);
        _logger.info("initTaskFlow() End");
    }
    

    private void initTaskFlowLoadContainer() {
        ADFUtils.findOperation(initTaskFlowLoadContainer).execute();
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }

    public void setIsTrailerValidated(Boolean isTrailerValidated) {
        this.isTrailerValidated = isTrailerValidated;
    }

    public Boolean getIsTrailerValidated() {
        return isTrailerValidated;
    }

    public void setIsDoorValidated(Boolean isDoorValidated) {
        this.isDoorValidated = isDoorValidated;
    }

    public Boolean getIsDoorValidated() {
        return isDoorValidated;
    }

    public void setOpenWipErrorFlag(Boolean openWipErrorFlag) {
        this.openWipErrorFlag = openWipErrorFlag;
    }

    public Boolean getOpenWipErrorFlag() {
        return openWipErrorFlag;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setUnfinishedWipErrorFlag(Boolean unfinishedWipErrorFlag) {
        this.unfinishedWipErrorFlag = unfinishedWipErrorFlag;
    }

    public Boolean getUnfinishedWipErrorFlag() {
        return unfinishedWipErrorFlag;
    }
}
