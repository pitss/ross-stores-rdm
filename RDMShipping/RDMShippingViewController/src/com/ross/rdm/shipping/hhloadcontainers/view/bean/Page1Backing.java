package com.ross.rdm.shipping.hhloadcontainers.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.shipping.hhloadcontainers.model.views.HhLoadContainerSLoadContainerViewRowImpl;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {

    private final static String LOAD_CONTAINER_ITERATOR = "HhLoadContainerSLoadContainerViewIterator";
    private final static String HH_LOAD_CONTAINER_FLOW_BEAN = "HhLoadContainerSBean";

    private final static String TRAILER_ID_ATTR = "TrailerId";
    private final static String DOOR_ID_ATTR = "DoorId";
    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String LOCATION_ID_ATTR = "LocationFlag";

    private RichPopup confirmationPopup;
    private RichOutputText dialogOutputText;
    private RichDialog exitDialog;


    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichPanelFormLayout loadContainerPanel;
    private RichLink f3Link;


    //Inputs
    private RichInputText trailerId;
    private RichIcon trailerIdIcon;
    private RichInputText doorId;
    private RichIcon doorIdIcon;
    private RichInputText containerId;
    private RichIcon containerIdIcon;

    //Outputs
    private RichInputText cartonCount;
    private RichInputText loadTs;

    public Page1Backing() {

    }

    public void onChangedTrailerId(ValueChangeEvent vce) {
        boolean validTrailerId = false;

        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredValue = ((String) vce.getNewValue()).toUpperCase();
            validTrailerId = callValidateTrailerId(enteredValue);
            if (validTrailerId) {
                String trailerId = enteredValue;
                removeErrorOfField(this.getTrailerId(), this.getTrailerIdIcon());
                hideMessagesPanel();
                ADFUtils.setBoundAttributeValue(TRAILER_ID_ATTR, trailerId);
                this.refreshContentOfUIComponent(this.getTrailerIdIcon());
                this.refreshContentOfUIComponent(this.getTrailerId());
                setFocusDoorId();
            }
        } else {
            getTrailerIdIcon().setName(ERR_ICON);
            refreshContentOfUIComponent(this.getTrailerIdIcon());
            refreshContentOfUIComponent(this.getTrailerId());
            addErrorStyleToComponent(this.getTrailerId());
            showMessagesPanel(ERROR, this.getMessage("INV_TRAILER", ERROR, null, this.getLangCodeAttrValue()));
        }
        getHhLoadContainerSPageFlowBean().setIsTrailerValidated(validTrailerId);
    }

    public boolean callValidateTrailerId(String trailerId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateTrailerId");
        oper.getParamsMap().put("pTrailerId", trailerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.get(1) != null) {
                    if (ERROR.equals(responseList.get(0))) {
                        showMessagesPanel(ERROR, responseList.get(1));
                        getTrailerIdIcon().setName(ERR_ICON);
                        refreshContentOfUIComponent(this.getTrailerIdIcon());
                        refreshContentOfUIComponent(this.getTrailerId());
                        addErrorStyleToComponent(this.getTrailerId());
                        selectTextOnUIComponent(this.getTrailerId());
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void onChangedDoorId(ValueChangeEvent vce) {
        boolean validDoorId = false;
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredValue = ((String) vce.getNewValue()).toUpperCase();
            validDoorId = callValidateDoorId(enteredValue);
            if (validDoorId) {
                ADFUtils.setBoundAttributeValue(DOOR_ID_ATTR, enteredValue);
                setFocusContainerId();
                hideMessagesPanel();
                refreshContentOfUIComponent(this.getDoorIdIcon());
                refreshContentOfUIComponent(this.getDoorId());
                removeErrorOfField(this.getDoorId(), this.getDoorIdIcon());
            }
        } else {
            getDoorIdIcon().setName(ERR_ICON);
            refreshContentOfUIComponent(this.getDoorIdIcon());
            refreshContentOfUIComponent(this.getDoorId());
            addErrorStyleToComponent(this.getDoorId());
            showMessagesPanel(ERROR, this.getMessage("INV_DOOR", ERROR, null, this.getLangCodeAttrValue()));
        }
        getHhLoadContainerSPageFlowBean().setIsDoorValidated(validDoorId);
    }

    public boolean callValidateDoorId(String doorId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateDoorId");
        oper.getParamsMap().put("pTrailerId", getTrailerIdAttrValue());
        oper.getParamsMap().put("pDoorId", doorId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() == 2) {
                    if (ERROR.equals(responseList.get(0))) {
                        showMessagesPanel(ERROR,
                                          this.getMessage(responseList.get(1), responseList.get(0), null,
                                                          this.getLangCodeAttrValue()));
                        getDoorIdIcon().setName(ERR_ICON);
                        refreshContentOfUIComponent(this.getDoorIdIcon());
                        refreshContentOfUIComponent(this.getDoorId());
                        addErrorStyleToComponent(this.getDoorId());
                        selectTextOnUIComponent(this.getDoorId());
                        return false;
                    }
                }
            }
        }
        refreshContentOfUIComponent(this.getCartonCount());
        getCartonCount().setVisible(true);
        return true;
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, enteredContainerIdValue);
            this.hideMessagesPanel();
            this.callValidateContainerId(enteredContainerIdValue);
        } else {
            getContainerIdIcon().setName(ERR_ICON);
            refreshContentOfUIComponent(this.getContainerIdIcon());
            refreshContentOfUIComponent(this.getContainerId());
            addErrorStyleToComponent(this.getContainerId());
            showMessagesPanel(ERROR, this.getMessage("INV_CONTAINER", ERROR, null, this.getLangCodeAttrValue()));
        }
    }

    public void callValidateContainerId(String containerId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateContainerId");
        oper.getParamsMap().put("pTrailerId", getTrailerIdAttrValue());
        oper.getParamsMap().put("pDoorId", getDoorIdAttrValue());
        oper.getParamsMap().put("pContainerId", containerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() >= 1) {
                    if (ERROR.equals(responseList.get(0))) {
                        showMessagesPanel(responseList.get(0), responseList.get(1));
                        getContainerIdIcon().setName(ERR_ICON);
                        refreshContentOfUIComponent(this.getContainerIdIcon());
                        refreshContentOfUIComponent(this.getContainerId());
                        addErrorStyleToComponent(this.getContainerId());
                        selectTextOnUIComponent(this.getContainerId());
                    } else if (WARN.equals(responseList.get(0)) || CONF.equals(responseList.get(0))) {
                        //Display popup with warning message
                        this.preparePopup(responseList);
                        this.hideMessagesPanel();
                        this.removeErrorOfField(this.getContainerId(), this.getContainerIdIcon());
                        this.getDialogOutputText().setValue(responseList.get(1));
                        this.getContainerId().setDisabled(true);
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.getConfirmationPopup().show(new RichPopup.PopupHints());
                    } else if (MSGTYPE_M.equals(responseList.get(0))) {
                        showMessagesPanel(responseList.get(0), responseList.get(1));
                        refreshContentOfUIComponent(this.getContainerIdIcon());
                        refreshContentOfUIComponent(this.getContainerId());
                        removeErrorOfField(this.getContainerId(), this.getContainerIdIcon());
                        selectTextOnUIComponent(this.getContainerId());
                        refreshContentOfUIComponent(this.getCartonCount());
                        refreshContentOfUIComponent(this.getLoadTs());
                        getCartonCount().setVisible(true);
                        this.selectTextOnUIComponent(this.getContainerId());
                        clearOnSuccess();
                    }
                }
            }
        }
    }

    private void clearOnSuccess() {
        this.getLoadTs().setVisible(true);
        HhLoadContainerSLoadContainerViewRowImpl hhRowImpl = this.getHhLoadContainerSLoadContainerViewRow();
        this.getContainerId().resetValue();
        this.refreshContentOfUIComponent(this.getContainerId());
        this.refreshContentOfUIComponent(this.getLoadTs());
        hhRowImpl.setContainerId(null);
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getHhLoadContainerSPageFlowBean().getIsFocusOn();

        if (clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (submittedValue != null) {
                    if (TRAILER_ID_ATTR.equals(currentFocus)) {
                        onChangedTrailerId(new ValueChangeEvent(this.getTrailerId(), null, submittedValue));
                    } else if (DOOR_ID_ATTR.equals(currentFocus)) {
                        onChangedDoorId(new ValueChangeEvent(this.getDoorId(), null, submittedValue));
                    } else if (CONTAINER_ID_ATTR.equals(currentFocus)) {
                        onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    }
                }
            }
            this.refreshContentOfUIComponent(this.getLoadContainerPanel());
        }
    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            clearFields(true);
            ADFUtils.invokeAction("backGlobalHome");
        }
    }

    private void clearFields(boolean clearAll) {
        _logger.info("clearFields() Start");
        this.clearCurrentRow(clearAll);
        if (clearAll) {
            this.getTrailerId().resetValue();
            this.getDoorId().resetValue();
            this.getContainerId().resetValue();
            this.refreshContentOfUIComponent(this.getTrailerId());
            this.refreshContentOfUIComponent(this.getDoorId());
            this.refreshContentOfUIComponent(this.getContainerId());
        }
        this.getCartonCount().resetValue();
        this.getLoadTs().resetValue();
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow(boolean clearAll) {
        HhLoadContainerSLoadContainerViewRowImpl hhRowImpl = this.getHhLoadContainerSLoadContainerViewRow();
        if (hhRowImpl != null) {
            if (clearAll) {
                hhRowImpl.setTrailerId(null);
                hhRowImpl.setDoorId(null);
                hhRowImpl.setContainerId(null);
            }
            hhRowImpl.setCartonCount(null);
            hhRowImpl.setLoadTs(null);
        }
    }

    private void setFocusContainerId() {
        this.getHhLoadContainerSPageFlowBean().setIsFocusOn(CONTAINER_ID_ATTR);
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusTrailerId() {
        this.getHhLoadContainerSPageFlowBean().setIsFocusOn(TRAILER_ID_ATTR);
        this.setFocusOnUIComponent(this.getTrailerId());
    }

    private void setFocusDoorId() {
        this.getHhLoadContainerSPageFlowBean().setIsFocusOn(DOOR_ID_ATTR);
        this.setFocusOnUIComponent(this.getDoorId());
    }

    private void noConfrimExitLinkActionListener() {
        this.getConfirmationPopup().hide();
        this.refreshContentOfUIComponent(this.getContainerId());
        this.getContainerId().setDisabled(false);
        this.setFocusContainerId();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
    }

    private void yesConfrimExitLinkActionListener() {
        this.getConfirmationPopup().hide();
        this.refreshContentOfUIComponent(this.getContainerId());
        this.getContainerId().setDisabled(false);
        this.setFocusContainerId();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }

    public void yesConfirmExitLinkActionListener(ActionEvent actionEvent) {
        this.getConfirmationPopup().hide();
        this.refreshContentOfUIComponent(this.getContainerId());
        this.getContainerId().setDisabled(false);
        this.setFocusContainerId();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }

    public void noConfirmExitLinkActionListener(ActionEvent actionEvent) {
        this.getConfirmationPopup().hide();
        this.refreshContentOfUIComponent(this.getContainerId());
        this.getContainerId().setDisabled(false);
        this.setFocusContainerId();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
    }

    public void performFkeyExitPopup(ClientEvent clientEvent) {
        _logger.info("performKkeyExitPopup() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                yesConfrimExitLinkActionListener();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                noConfrimExitLinkActionListener();
            }
        }
        _logger.info("performKkeyExitPopup() End");
    }

    public void confirmDialogListener(DialogEvent dialogEvent) {
        _logger.info("confirmDialogListener() Start");
        Boolean openWipErrorFlag = getHhLoadContainerSPageFlowBean().getOpenWipErrorFlag();
        Boolean unfinishedWipErrorFlag = getHhLoadContainerSPageFlowBean().getUnfinishedWipErrorFlag();

        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            String containerId = (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
            this.callValidateContainerId(containerId);
        } else {
            this.selectTextOnUIComponent(this.getContainerId());
            setLocationid(BigDecimal.ZERO);
        }
        if (unfinishedWipErrorFlag || openWipErrorFlag) {
            showMessagesPanel(ERROR, getMessage("UNFINISHED_WIP", ERROR, null, this.getLangCodeAttrValue()));
            getContainerIdIcon().setName(ERR_ICON);
            refreshContentOfUIComponent(this.getContainerIdIcon());
            refreshContentOfUIComponent(this.getContainerId());
            addErrorStyleToComponent(this.getContainerId());
            selectTextOnUIComponent(this.getContainerId());
            getHhLoadContainerSPageFlowBean().setOpenWipErrorFlag(false);
            getHhLoadContainerSPageFlowBean().setUnfinishedWipErrorFlag(false);
        }
        _logger.info("confirmDialogListener() End");
    }

    private void preparePopup(List<String> responseList) {
        if (getMessage("WIP_PROC_HERE", WARN, null,
                       this.getLangCodeAttrValue()).equalsIgnoreCase(responseList.get(1))) {
            getHhLoadContainerSPageFlowBean().setOpenWipErrorFlag(Boolean.TRUE);
        } else if (getMessage("WIP_LAST_CHANCE", WARN, null,
                              this.getLangCodeAttrValue()).equalsIgnoreCase(responseList.get(1))) {
            getHhLoadContainerSPageFlowBean().setUnfinishedWipErrorFlag(Boolean.TRUE);
        }
        if (getMessage("PERISH_EXPIRED", WARN, null,
                       this.getLangCodeAttrValue()).equalsIgnoreCase(responseList.get(1))) {
            getHhLoadContainerSPageFlowBean().setMessageType("Warning");
        } else {
            getHhLoadContainerSPageFlowBean().setMessageType("Confirmation");
        }
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_load_container_s_LOAD_CONTAINER_F3,
                                                RoleBasedAccessConstants.FORM_NAME_hh_load_container_s)) {
            this.trKeyIn("F3");
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));

        }
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(ATTR_FACILITY_ID);
    }

    private String getTrailerIdAttrValue() {
        if (getHhLoadContainerSPageFlowBean().getIsTrailerValidated()) {
            return (String) ADFUtils.getBoundAttributeValue(TRAILER_ID_ATTR);
        }
        return null;
    }

    private String getDoorIdAttrValue() {
        if (getHhLoadContainerSPageFlowBean().getIsDoorValidated()) {
            return (String) ADFUtils.getBoundAttributeValue(DOOR_ID_ATTR);
        }
        return null;
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(ATTR_LANGUAGE_CODE);
    }

    private void setLocationid(BigDecimal locationId) {
        ADFUtils.setBoundAttributeValue(LOCATION_ID_ATTR, locationId);
    }

    private HhLoadContainerSLoadContainerViewRowImpl getHhLoadContainerSLoadContainerViewRow() {
        return (HhLoadContainerSLoadContainerViewRowImpl) ADFUtils.findIterator(LOAD_CONTAINER_ITERATOR).getCurrentRow();
    }

    private HhLoadContainerSBean getHhLoadContainerSPageFlowBean() {
        return ((HhLoadContainerSBean) this.getPageFlowBean(HH_LOAD_CONTAINER_FLOW_BEAN));
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setTrailerId(RichInputText trailerId) {
        this.trailerId = trailerId;
    }

    public RichInputText getTrailerId() {
        return trailerId;
    }

    public void setTrailerIdIcon(RichIcon trailerIdIcon) {
        this.trailerIdIcon = trailerIdIcon;
    }

    public RichIcon getTrailerIdIcon() {
        return trailerIdIcon;
    }

    public void setDoorId(RichInputText doorId) {
        this.doorId = doorId;
    }

    public RichInputText getDoorId() {
        return doorId;
    }

    public void setDoorIdIcon(RichIcon doorIdIcon) {
        this.doorIdIcon = doorIdIcon;
    }

    public RichIcon getDoorIdIcon() {
        return doorIdIcon;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setCartonCount(RichInputText cartonCount) {
        this.cartonCount = cartonCount;
    }

    public RichInputText getCartonCount() {
        return cartonCount;
    }

    public void setLoadTs(RichInputText loadTs) {
        this.loadTs = loadTs;
    }

    public RichInputText getLoadTs() {
        return loadTs;
    }

    public void setLoadContainerPanel(RichPanelFormLayout loadContainerPanel) {
        this.loadContainerPanel = loadContainerPanel;
    }

    public RichPanelFormLayout getLoadContainerPanel() {
        return loadContainerPanel;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setConfirmationPopup(RichPopup confirmationPopup) {
        this.confirmationPopup = confirmationPopup;
    }

    public RichPopup getConfirmationPopup() {
        return confirmationPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }

    public void setExitDialog(RichDialog exitDialog) {
        this.exitDialog = exitDialog;
    }

    public RichDialog getExitDialog() {
        return exitDialog;
    }

    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }
}
