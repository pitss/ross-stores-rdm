package com.ross.rdm.shipping.hhqcoutboundaudits.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {
    private RichPanelGroupLayout errorPanel;
    private RichIcon errorMessageIcon;
    private RichOutputText errorMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private final static String CONTAINER_ID_ATTR = "ContainerId";
    private final static String ITEM_UPC_ATTR = "ItemUpc";
    private final static String FACILITY_ID_ATTR = "FacilityId";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String USER_ATTR = "User";
    private final static String ITEM_ID_ATTR = "ItemId";


    private final static String FORM_NAME = "HH_QC_OUTBOUND_AUDIT_S";
    private final static String HH_QC_OUTBOUND_AUDIT_BEAN = "HhQcOutboundAuditSBean";

    private RichIcon containerIdIcon;
    private RichInputText containerId;
    private RichIcon itemUpcIcon;
    private RichInputText itemUpc;
    private RichPanelFormLayout outboundAuditPanel;
    private RichLink f3Link;
    private RichLink f4Link;

    private RichPopup outboundAuditPopup;
    private RichOutputText outboundAuditPopupOutputtext;
    private RichLink yesLinkPopup;
    private RichLink noLinkPopup;
    private RichDialog exitDialog;
    private RichOutputText dialogOutputText;


    private RichPopup confirmedPopup;
    private RichOutputText confirmedPopupOutputtext;
    private RichLink okayLinkPopup;
 
    static final String SUCCESS = "S"; 
    static final String SCC_ICON = LOGO_ICON;
    static final String INTRLVG = "INTRLVG";

    public Page1Backing() {

    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredContainerIdValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateContainerId(enteredContainerIdValue);
        } else {
            this.getContainerIdIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getContainerIdIcon());
            this.refreshContentOfUIComponent(this.getContainerId());
            this.addErrorStyleToComponent(this.getContainerId());
            this.selectTextOnUIComponent(this.getContainerId());
            this.getHhQcOutboundAuditSPageFlowBean().setIsContainerIdValidated(false);
            this.showErrorPanel(ERROR,
                                this.getMessage("INV_CONTAINER", ERROR, this.getFacilityIdAttrValue(),
                                                this.getLanguageCodeValue()));
        }
    }

    public boolean validateContainerId(String containerId) {
        _logger.info("validateContainerId() Start");
        if (!this.getContainerId().isDisabled()) {
            if (this.getHhQcOutboundAuditSPageFlowBean().getIsError() && containerId != null) {
                this.setErrorStyleClass(this.getContainerId(), true);
                this.selectTextOnUIComponent(this.getContainerId());
                this.getHhQcOutboundAuditSPageFlowBean().setIsContainerIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getHhQcOutboundAuditSPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getContainerId(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("validateContainer");
                Map map = oper.getParamsMap();
                map.put("containerId", containerId);
                map.put("itemId", getItemIdAttrValue());
                map.put("itemUpc", getItemUpcValue());
                map.put("facilityId", getFacilityIdAttrValue());
                map.put("user", getUserAttrValue());
                oper.execute();

                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showErrorPanel(ERROR, msg);
                        } else {
                            this.showErrorPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getContainerIdIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.selectTextOnUIComponent(this.getContainerId());
                        this.getHhQcOutboundAuditSPageFlowBean().setIsContainerIdValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, containerId);
                        this.getContainerIdIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getContainerIdIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getContainerId());
                        this.refreshContentOfUIComponent(this.getOutboundAuditPanel());

                        _logger.info("onChangedContainerId() End");
                    }
                }
            }
        }
        this.getHhQcOutboundAuditSPageFlowBean().setIsContainerIdValidated(true);
        _logger.info("validatedContainerId() End");
        return true;
    }

    public void onChangedItemUpc(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredItemUpcValue = ((String) vce.getNewValue()).toUpperCase();
            this.validateItemUpc(enteredItemUpcValue);
        } else {
            this.getItemUpcIcon().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getItemUpcIcon());
            this.refreshContentOfUIComponent(this.getItemUpc());
            this.addErrorStyleToComponent(this.getItemUpc());
            this.selectTextOnUIComponent(this.getItemUpc());
            this.getHhQcOutboundAuditSPageFlowBean().setIsItemUpcValidated(false);
            this.showErrorPanel(ERROR, "DMS Error; Please see your Administrator");
        }
    }

    public Boolean validateItemUpc(String itemUpc) {
        _logger.info("validateContainerId() Start");
        if (!this.getItemUpc().isDisabled()) {
            if (this.getHhQcOutboundAuditSPageFlowBean().getIsError() && containerId != null) {
                this.setErrorStyleClass(this.getItemUpc(), true);
                this.selectTextOnUIComponent(this.getItemUpc());
                this.getHhQcOutboundAuditSPageFlowBean().setIsContainerIdValidated(false);
                _logger.info("validateContainerId() End");
                return false;
            } else {
                this.getHhQcOutboundAuditSPageFlowBean().setIsError(false);
                this.setErrorStyleClass(this.getItemUpc(), false);

                OperationBinding oper = (OperationBinding) ADFUtils.findOperation("validateItemUpc");
                Map map = oper.getParamsMap();
                map.put("containerId", getContainerIdValue());
                map.put("itemId", getItemIdAttrValue());
                map.put("itemUpc", itemUpc);
                map.put("facilityId", getFacilityIdAttrValue());
                map.put("user", getUserAttrValue());
                oper.execute();
                if (oper.getErrors().isEmpty()) {
                    if (null != oper.getResult()) {
                        List<String> errorCodeList = (List<String>) oper.getResult();
                        if (ERROR.equalsIgnoreCase(errorCodeList.get(0))) {
                            String msg = errorCodeList.get(1);
                            this.showErrorPanel(ERROR, msg);
                        } else {
                            this.showErrorPanel(ERROR, errorCodeList.get(1));
                        }
                        this.getItemUpcIcon().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getItemUpcIcon());
                        this.refreshContentOfUIComponent(this.getItemUpc());
                        this.addErrorStyleToComponent(this.getItemUpc());
                        this.selectTextOnUIComponent(this.getItemUpc());
                        this.getHhQcOutboundAuditSPageFlowBean().setIsItemUpcValidated(false);

                        _logger.info("onChangedContainerId() End");
                        return false;
                    } else {
                        ADFUtils.setBoundAttributeValue(ITEM_UPC_ATTR, itemUpc);
                        this.getItemUpcIcon().setName(REQ_ICON);
                        this.refreshContentOfUIComponent(this.getItemUpcIcon());
                        this.hideMessagesPanel();
                        this.removeErrorStyleToComponent(this.getItemUpc());
                        this.refreshContentOfUIComponent(this.getOutboundAuditPanel());

                        _logger.info("onChangedContainerId() End");
                    }
                }
            }
        }
        ADFUtils.setBoundAttributeValue(ITEM_UPC_ATTR, null);
        this.getItemUpc().resetValue();
        this.getHhQcOutboundAuditSPageFlowBean().setIsItemUpcValidated(true);
        _logger.info("validatedContainerId() End");
        return true;
    }

    private void showErrorPanel(String messageType, String msg) {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.getErrorMessageIcon().setVisible(false);

        if (WARN.equals(messageType)) {
            this.getErrorMessageIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType)) {
            this.getErrorMessageIcon().setName(INF_ICON);
        } else if (MSGTYPE_M.equals(messageType)) {
            this.getErrorMessageIcon().setName(LOGO_ICON);
        } else {
            this.getErrorMessageIcon().setName(ERR_ICON);
        }

        this.getErrorMessage().setValue(msg);
        this.getErrorMessageIcon().setVisible(true);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    private void setErrorStyleClass(RichInputText field, boolean set) {
        _logger.info("setErrorStyleClass() Start");
        _logger.fine("field value: " + field + "\n" + "set value: " + set);

        if (set) {
            this.addErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con")) {
                this.getContainerIdIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            } else if (field.getId().equalsIgnoreCase("ite")) {
                this.getItemUpcIcon().setName(ERR_ICON);
                this.refreshContentOfUIComponent(this.getItemUpcIcon());
            }
        } else {
            this.removeErrorStyleToComponent(field);
            if (field.getId().equalsIgnoreCase("con")) {
                this.getContainerIdIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getContainerIdIcon());
            } else if (field.getId().equalsIgnoreCase("ite")) {
                this.getItemUpcIcon().setName(REQ_ICON);
                this.refreshContentOfUIComponent(this.getItemUpcIcon());
            }
        }
        _logger.info("setErrorStyleClass() End");
    }

    public void hideMessagesPanel() {
        this.getErrorMessage().setValue(null);
        this.getErrorMessageIcon().setName(null);
        this.refreshContentOfUIComponent(this.getErrorPanel());
    }

    public void performKey(ClientEvent clientEvent) {
    
        if (clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String currentFocus = this.getHhQcOutboundAuditSPageFlowBean().getIsFocusOn();
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {

                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();

            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("ContainerIdField".equals(currentFocus)) {
                    this.setErrorStyleClass(this.getContainerId(), false);
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    this.refreshContentOfUIComponent(this.getOutboundAuditPanel());
                    if (this.getHhQcOutboundAuditSPageFlowBean().getIsContainerIdValidated()){
                        this.getContainerId().setDisabled(true);
                        this.getItemUpc().setDisabled(false);
                        this.setFocusItemUpc();
                    }
                } else if ("ItemUpc".equals(currentFocus)) {
                    this.setErrorStyleClass(this.getItemUpc(), false);
                    this.onChangedItemUpc(new ValueChangeEvent(this.getItemUpc(), null, submittedValue));
                    this.refreshContentOfUIComponent(this.getOutboundAuditPanel());
                    /*if (this.getHhQcOutboundAuditSPageFlowBean().getIsItemUpcValidated()){
                    this.setFocusContainerId();
                    }*/
                }
            }
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");


        if ("F3".equalsIgnoreCase(key)) {
            ADFUtils.invokeAction("backGlobalHome");
            _logger.info("trKeyIn() End");

        }
        if ("F4".equalsIgnoreCase(key)) {
            String currentFocus = this.getHhQcOutboundAuditSPageFlowBean().getIsFocusOn();
            
            if("ContainerIdField".equals(currentFocus)){
                this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, getContainerIdValue()));
            }
            
            this.getItemUpcIcon().setName(REQ_ICON);
            this.getContainerIdIcon().setName(REQ_ICON);
            this.refreshContentOfUIComponent(this.getItemUpcIcon());
            this.hideMessagesPanel();
            this.removeErrorStyleToComponent(this.getItemUpc());
            this.removeErrorStyleToComponent(this.getContainerId());
            this.refreshContentOfUIComponent(getItemUpc());
            this.refreshContentOfUIComponent(getContainerIdIcon());
            this.refreshContentOfUIComponent(getOutboundAuditPanel());
            
            this.getOutboundAuditPopupOutputtext().setValue(this.getMessage("CONFIRM_CHECK", CONF,
                                                                                        this.getFacilityIdAttrValue(),
                                                                                        this.getLanguageCodeValue()));

            if("ContainerIdField".equals(currentFocus)){
                this.getContainerId().setDisabled(true);
            }
            else if("ItemUpc".equals(currentFocus)){
                this.getItemUpc().setDisabled(true);
            }
                this.getOutboundAuditPopup().show(new RichPopup.PopupHints());
        }
    }
    
    public void performFkeyExitPopup (ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
               
                yesLinkConfirmPopupActionListener();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                noLinkConfirmPopupActionListener();
            }
        }
        _logger.info("performKkeyExitPopup() End");
        }
    
    public void performFkeyOkayPopup (ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                okayConfirmedPopupActionListener();
        }
        }
    }
    
    public void yesLinkConfirmPopupActionListener() {
        this.getOutboundAuditPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }

    public void noLinkConfirmPopupActionListener() {
        this.getOutboundAuditPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
        }
    
    public void okayConfirmedPopupActionListener() {
        this.getConfirmedPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.ok));
    }
    
    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent){
        this.getOutboundAuditPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.yes));
    }
    
    public void noLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getOutboundAuditPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.no));
    }
    
    public void okayConfirmedPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmedPopup().hide();
        this.confirmDialogListener(new DialogEvent(this.getExitDialog(), DialogEvent.Outcome.ok));
    }
    
    public void confirmDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)){
            this.getConfirmedPopupOutputtext().setValue(this.getMessage("CONT_PROCESSED", INFO,
                                                                        this.getFacilityIdAttrValue(),
                                                                        this.getLanguageCodeValue()));
            printReport();
            this.getConfirmedPopup().show(new RichPopup.PopupHints());
        }
        else if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)){
            
            this.getContainerId().setDisabled(false);
            this.getItemUpc().setDisabled(true);
            resetValues();
        }
        else{
                this.getItemUpc().setDisabled(false);
                this.getContainerId().setDisabled(true);
                this.setFocusItemUpc();
            }
        }
    
    public void resetValues(){
        ADFUtils.setBoundAttributeValue(ITEM_UPC_ATTR, null);
        ADFUtils.setBoundAttributeValue(CONTAINER_ID_ATTR, null);
        this.getHhQcOutboundAuditSPageFlowBean().setIsContainerIdValidated(false);
        this.getHhQcOutboundAuditSPageFlowBean().setIsItemUpcValidated(false);
        getItemUpc().resetValue();
        getContainerId().resetValue();
        this.refreshContentOfUIComponent(getItemUpc());
        this.refreshContentOfUIComponent(getContainerIdIcon());
        this.refreshContentOfUIComponent(getOutboundAuditPanel());
        setFocusContainerId();
    }

    public void printReport(){
    OperationBinding oper = (OperationBinding) ADFUtils.findOperation("printLabels");
    oper.execute();
    
    }

    String getBoundAttribute(String attrName) {
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String) val;
        return null;
    }

    private void setFocusContainerId() {
        this.getHhQcOutboundAuditSPageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getContainerId());
    }

    private void setFocusItemUpc() {
        this.getHhQcOutboundAuditSPageFlowBean().setIsFocusOn("ItemUpc");
        this.setFocusOnUIComponent(this.getItemUpc());
    }

    private HhQcOutboundAuditSBean getHhQcOutboundAuditSPageFlowBean() {
        return ((HhQcOutboundAuditSBean) this.getPageFlowBean(HH_QC_OUTBOUND_AUDIT_BEAN));
    }

    private String getFacilityIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(FACILITY_ID_ATTR);
    }

    private String getLanguageCodeValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private String getUserAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(USER_ATTR);
    }

    private String getItemIdAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(ITEM_ID_ATTR);
    }

    private String getContainerIdValue() {
        return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
    }

    private String getItemUpcValue() {
        return (String) ADFUtils.getBoundAttributeValue(ITEM_UPC_ATTR);
    }

    public void setErrorPanel(RichPanelGroupLayout errorPanel) {
        this.errorPanel = errorPanel;
    }

    public RichPanelGroupLayout getErrorPanel() {
        return errorPanel;
    }

    public void setErrorMessageIcon(RichIcon errorMessageIcon) {
        this.errorMessageIcon = errorMessageIcon;
    }

    public RichIcon getErrorMessageIcon() {
        return errorMessageIcon;
    }

    public void setErrorMessage(RichOutputText errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RichOutputText getErrorMessage() {
        return errorMessage;
    }

    public static void setLogger(ADFLogger _logger) {
        Page1Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setItemUpcIcon(RichIcon itemUpcIcon) {
        this.itemUpcIcon = itemUpcIcon;
    }

    public RichIcon getItemUpcIcon() {
        return itemUpcIcon;
    }

    public void setItemUpc(RichInputText itemUpc) {
        this.itemUpc = itemUpc;
    }

    public RichInputText getItemUpc() {
        return itemUpc;
    }

    public void setOutboundAuditPanel(RichPanelFormLayout outboundAuditPanel) {
        this.outboundAuditPanel = outboundAuditPanel;
    }

    public RichPanelFormLayout getOutboundAuditPanel() {
        return outboundAuditPanel;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_hh_qc_outbound_audit_s_QC_AUDIT_F4,
                                                RoleBasedAccessConstants.FORM_NAME_hh_qc_outbound_audit_s)) {
        this.trKeyIn("F4");
        
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
    }
    
    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }

    public void setOutboundAuditPopup(RichPopup outboundAuditPopup) {
        this.outboundAuditPopup = outboundAuditPopup;
    }

    public RichPopup getOutboundAuditPopup() {
        return outboundAuditPopup;
    }

    public void setOutboundAuditPopupOutputtext(RichOutputText outboundAuditPopupOutputtext) {
        this.outboundAuditPopupOutputtext = outboundAuditPopupOutputtext;
    }

    public RichOutputText getOutboundAuditPopupOutputtext() {
        return outboundAuditPopupOutputtext;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setNoLinkPopup(RichLink noLinkPopup) {
        this.noLinkPopup = noLinkPopup;
    }

    public RichLink getNoLinkPopup() {
        return noLinkPopup;
    }

    public void setConfirmedPopup(RichPopup confirmedPopup) {
        this.confirmedPopup = confirmedPopup;
    }

    public RichPopup getConfirmedPopup() {
        return confirmedPopup;
    }

    public void setConfirmedPopupOutputtext(RichOutputText confirmedPopupOutputtext) {
        this.confirmedPopupOutputtext = confirmedPopupOutputtext;
    }

    public RichOutputText getConfirmedPopupOutputtext() {
        return confirmedPopupOutputtext;
    }

    public void setOkayLinkPopup(RichLink okayLinkPopup) {
        this.okayLinkPopup = okayLinkPopup;
    }

    public RichLink getOkayLinkPopup() {
        return okayLinkPopup;
    }
    
    public void setExitDialog(RichDialog exitDialog) {
        this.exitDialog = exitDialog;
    }

    public RichDialog getExitDialog() {
        return exitDialog;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }
}
