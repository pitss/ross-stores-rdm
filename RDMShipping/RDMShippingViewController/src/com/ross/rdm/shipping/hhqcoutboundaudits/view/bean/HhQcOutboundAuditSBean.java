package com.ross.rdm.shipping.hhqcoutboundaudits.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhQcOutboundAuditSBean  {
  private static ADFLogger _logger = ADFLogger.createADFLogger(HhQcOutboundAuditSBean.class);
  
    private String formName;
    private String  isFocusOn;
    private Boolean isError;
    private Boolean isContainerIdValidated;
    private Boolean isItemUpcValidated;

    public void initTaskFlow() {
        
    this.initGlobalVariablesOutboundAudit();
    this.setIsContainerIdValidated(false);
    this.setIsError(false);
    this.setIsFocusOn("ContainerIdField");
}
    
    private void initGlobalVariablesOutboundAudit() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("initTaskFlowQcOutboundAudit");
        oper.execute();
    }


    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return formName;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }

    public void setIsContainerIdValidated(Boolean isContainerIdValidated) {
        this.isContainerIdValidated = isContainerIdValidated;
    }

    public Boolean getIsContainerIdValidated() {
        return isContainerIdValidated;
    }

    public void setIsItemUpcValidated(Boolean isItemUpcValidated) {
        this.isItemUpcValidated = isItemUpcValidated;
    }

    public Boolean getIsItemUpcValidated() {
        return isItemUpcValidated;
    }
}
