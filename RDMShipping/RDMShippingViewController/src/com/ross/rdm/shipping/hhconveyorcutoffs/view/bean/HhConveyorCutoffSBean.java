package com.ross.rdm.shipping.hhconveyorcutoffs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class HhConveyorCutoffSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(HhConveyorCutoffSBean.class);
    
    private String focus = null;
    private boolean disableAllFields = false;
    
    public final static String CONTAINER_ID = "con1";
    public final static String LOAD_TS = "loa1";
    
    private final static String setGlobalVariablesConveyorCutOff = "setGlobalVariablesConveyorCutOff";
    private final static String setConveyorCutOffWorkVariables = "setConveyorCutOffWorkVariables";
    private final static String CreateInsertConveyorCutOffRow = "CreateInsertConveyorCutOffRow";
    private String isFocusOn;

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getFocus() {
        return focus;
    }

    public void setDisableAllFields(boolean disableAllFields) {
        this.disableAllFields = disableAllFields;
    }

    public boolean isDisableAllFields() {
        return disableAllFields;
    }
    
    public Boolean getDisableContainerId() {
        return disableAllFields || (focus != null && !CONTAINER_ID.equals(focus));
    }

    public Boolean getDisabledLoadTs() {
        return disableAllFields || !LOAD_TS.equals(focus);
    }

    public void initTaskFlow() {

        //Forms  Module : hh_conveyor_cutoff_s
        //Object        : WHEN-NEW-FORM-INSTANCE
        //Source Code   :
        //
//        Tr_Startup;
//
//        startup_local;
//        AHL.SET_AHL_INFO(:WORK.FACILITY_ID,:SYSTEM.CURRENT_FORM);

        _logger.info("Conveyor cut off page initialization begun");
        this.initGlobalVariablesInvVerif();
        this.initConveyorCutOffWorkVariables();
        this.initConveyorCutOffRow();        
        this.setIsFocusOn("ContainerIdField");
        _logger.info("Conveyor cut off page initialization completed");
    }
    
    private void initGlobalVariablesInvVerif() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(setGlobalVariablesConveyorCutOff);
        oper.execute();
    }
    
    private void initConveyorCutOffWorkVariables() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(setConveyorCutOffWorkVariables);
        oper.execute();
    }
    
    private void initConveyorCutOffRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation(CreateInsertConveyorCutOffRow);
        oper.execute();
    }
    
}
