package com.ross.rdm.shipping.hhconveyorcutoffs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.shipping.view.framework.RDMShippingBackingBean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMShippingBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichLink f1DisplayLink;
    private RichLink f3ExitLink;
    private RichLink validateContainerLink;
    private RichLink yesLinkPopup;

    private RichInputText containerId;
    private RichInputText loadTs;
    
    private RichIcon iconContainer;
    private RichIcon iconLoadTs;
    
    private RichPopup confirmPopup;
    
    private RichOutputText dialogOutputText;
    
    private RichPanelGroupLayout globalPgl;
    
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String CONTAINER_ID = "con1";

    public void setGlobalPgl(RichPanelGroupLayout globalPgl) {
        this.globalPgl = globalPgl;
    }

    public RichPanelGroupLayout getGlobalPgl() {
        return globalPgl;
    }

    public void setYesLinkPopup(RichLink yesLinkPopup) {
        this.yesLinkPopup = yesLinkPopup;
    }

    public RichLink getYesLinkPopup() {
        return yesLinkPopup;
    }

    public void setDialogOutputText(RichOutputText dialogOutputText) {
        this.dialogOutputText = dialogOutputText;
    }

    public RichOutputText getDialogOutputText() {
        return dialogOutputText;
    }
    
    public void setConfirmPopup(RichPopup confirmPopup) {
        this.confirmPopup = confirmPopup;
    }

    public RichPopup getConfirmPopup() {
        return confirmPopup;
    }
    
    public void setIconContainer(RichIcon iconContainer) {
        this.iconContainer = iconContainer;
    }

    public RichIcon getIconContainer() {
        return iconContainer;
    }

    public void setIconLoadTs(RichIcon iconLoadTs) {
        this.iconLoadTs = iconLoadTs;
    }
    public RichIcon getIconLoadTs() {
        return iconLoadTs;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setLoadTs(RichInputText loadTs) {
        this.loadTs = loadTs;
    }

    public RichInputText getLoadTs() {
        return loadTs;
    }
  
    public void setValidateContainerLink(RichLink validateContainerLink) {
        this.validateContainerLink = validateContainerLink;
    }

    public RichLink getValidateContainerLink() {
        return validateContainerLink;
    }

    public void setF1DisplayLink(RichLink f1DisplayLink) {
        this.f1DisplayLink = f1DisplayLink;
    }

    public RichLink getF1DisplayLink() {
        return f1DisplayLink;
    }

    public void setF3ExitLink(RichLink f3ExitLink) {
        this.f3ExitLink = f3ExitLink;
    }

    public RichLink getF3ExitLink() {
        return f3ExitLink;
    }
    
    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }
    
    public void onChangedContainerId(ValueChangeEvent vce) {
        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String containerId = ((String) vce.getNewValue()).toUpperCase();
            this.callChangedIdContainer(containerId);
        }
        else {
            this.getIconContainer().setName(ERR_ICON);
            this.refreshContentOfUIComponent(this.getIconContainer());
            this.refreshContentOfUIComponent(this.getContainerId());
            this.addErrorStyleToComponent(this.getContainerId());
            this.showMessagesPanel(WARN, this.getMessage("INV_CONTAINER", WARN, null, this.getLangCodeAttrValue()));
        }
    }
    
    private void callChangedIdContainer(String containerId){
        OperationBinding oper = ADFUtils.findOperation("callConveyCutoff");
        oper.getParamsMap().put("pContainerId", containerId);
        
        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (ArrayList<String>) oper.getResult();
                if(responseList != null)
                {
                    if (responseList.size() > 1) {                    
                        this.showMessagesPanel(responseList.get(0), responseList.get(1));
                        this.getIconContainer().setName(ERR_ICON);
                        this.refreshContentOfUIComponent(this.getIconContainer());
                        this.refreshContentOfUIComponent(this.getContainerId());
                        this.addErrorStyleToComponent(this.getContainerId());
                        this.selectTextOnUIComponent(this.getContainerId());
                    } 
                    else
                    {
                        this.getPageFlowBean().setDisableAllFields(true);
                        this.getDialogOutputText().setValue(this.getMessage("SUCCESS_OPER_CC") + responseList.get(0));
                        this.getPageFlowBean().setIsFocusOn("ConfirmPopUp");
                        this.getConfirmPopup().show(new RichPopup.PopupHints());
                    }
                }
            }
        }
    }


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }
    
    public void f3Exit(ActionEvent actionEvent) {
        ADFUtils.invokeAction("backGlobalHome");
    }
    
    public void performKey(ClientEvent clientEvent) {        
        if (clientEvent.getParameters().containsKey("keyPressed")) {
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String item = (String) clientEvent.getParameters().get("item");
                if (CONTAINER_ID.equals(item)) {
                    this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                }                
           } 
            else if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } 
        }
    }
    
    public void performKeyPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } 
        }
    }
    
    private void f3Exit()
    {
        ADFUtils.invokeAction("backGlobalHome");
    }
    
    private HhConveyorCutoffSBean getPageFlowBean() {
        return (HhConveyorCutoffSBean) this.getPageFlowBean("HhConveyorCutoffSBean");
    }
    
    public void yesLinkConfirmPopupActionListener(ActionEvent actionEvent) {
        this.getConfirmPopup().hide();
        this.f3Exit();
    }
    
    public void performFkeyExitPopup(ClientEvent clientEvent) {
        _logger.info("performKkeyExitPopup() Start");
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3ExitLink());
                actionEvent.queue();
            } 
        }
        _logger.info("performKkeyExitPopup() End");
    }
}
