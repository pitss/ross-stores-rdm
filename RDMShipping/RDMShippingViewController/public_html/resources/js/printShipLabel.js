function onPopupQueueLovOpen(event) {
    var component = ("[id*='soc1']");
    $(component).attr('size', 8);
    SetFocusOnUIcomp(comp);
}

function onClickOnQueLov(event) {
    var codeLovQ = 'soc1';
    var codeLovQComp = ("[id*='" + codeLovQ + "']");
    openListOfValues(codeLovQComp);
}

function onBlurPsl(event) {
    var elem = (event.getSource().getClientId());
    var comp = document.querySelector('[id="' + elem + '::content"]');
    $(comp).select();
}

function fLinksKeyHandlerPsl(event, popupId) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed != AdfKeyStroke.F1_KEY && keyPressed != AdfKeyStroke.F2_KEY) {
        event.cancel();
    }
    else {
        var fLink = component;
        if (keyPressed == AdfKeyStroke.F2_KEY) {
            fLink = component.findComponent('nlp');
            fLink.focus();
        }
        AdfActionEvent.queue(fLink, true);
    }
}

function setInitialFocusPsl() {
    setTimeout(function () {
        var focusOnValue = $("input[id*='focus']").val();
        if ((focusOnValue != null)) {
            switch (focusOnValue) {
                case 'QueueField':
                    SetFocusOnUIcomp("input[id*='loc1']");
                    break;
                case 'ContainerIdField':
                    SetFocusOnUIcomp("input[id*='flu1']");
                    break;
                default :
                    break;
            }
        }
    },
0);
}

function onKeyPressedPsl(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F6_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}

function noYesLinkConfirmPopupPsl(event) {
    fLinksKeyHandlerPsl(event, 'p1')
}

function OnOpenConfirmPopupPsl(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('ylp');
    linkComp.focus();
}

function onKeyPressedOnQueueLovPsl(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    
    //Workaround to fix ENTER not working in IE 11 Enterprise Mode
    if(keyPressed == 0 && navigator.userAgent.indexOf("Trident/4.0") != -1){
        keyPressed = AdfKeyStroke.ENTER_KEY;
    }
    
    if(keyPressed != AdfKeyStroke.ARROWDOWN_KEY && keyPressed != AdfKeyStroke.ARROWUP_KEY){
        if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
            AdfCustomEvent.queue(component, "customKeyEvent", 
            {
                keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
            },
    true);
        }
        event.cancel();
    }
}

function onKeyPressedOkayPopup(event) {
     var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        this.pressOnLink('okayLinkPopup', component) 
    }
    event.cancel();
}

function onOpenConfirmedPopup(event){
    this.onKeyPressedOkayPopup(event,'okayLinkPopup')
}

function okayLinkConfirmOkayPopup(event) {
OnOpen(event, 'okayLinkPopup')
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}