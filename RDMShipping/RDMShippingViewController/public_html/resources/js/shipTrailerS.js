function onKeyPressedShipTrailerSPage1(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F5_KEY || keyPressed == AdfKeyStroke.F7_KEY ||
        keyPressed == AdfKeyStroke.F8_KEY || keyPressed == AdfKeyStroke.F9_KEY ||
        keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);        
    }
    event.cancel();
}

function onOpenPopupShipTrailerSPage1(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('yesLinkPopup');
    linkComp.focus();
}

function onKeyPressedShipTrailerSPage1PopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}

function onOpenPopupShipTrailerSPage2(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('okLinkPopup');
    linkComp.focus();
}

function onKeyPressedShipTrailerSPage2PopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}


function onOpenWarningPopupShipTrailerSPage1(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('okLinkPopup');
    linkComp.focus();
}

function onKeyPressedShipTrailerSPage1WarningPopUp(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}

function onKeyPressedShipTrailerSPage2(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || 
        keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);        
    }
    event.cancel();
}

function onKeyPressedShipTrailerSPageEstApptStart(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || 
        keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);        
    }
    event.cancel();
}