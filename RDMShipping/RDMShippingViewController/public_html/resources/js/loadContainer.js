function setLoadContainerPage1InitialFocus() {
    var focusId = 'tra';
    var focusIdCode = ("[id*='" + focusId + "']");
    setFocusOrSelectOnUIcomp(focusIdCode);
}

function onKeyPressedLoadContainer(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : "containerId"
        },
true);
        event.cancel();
    }
}

function onKeyPressedLoadContainerP1(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode()
    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
        event.cancel();
    }
}

function onKeyPressedPopupP1(event){
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
        event.cancel();
    }
}

function OnOpenExitPopupLoadCont(event) {
    OnOpen(event, 'nel');
}

function yesNolinkConfirmPopupLoadCont(event) {
    var keyPressed = event.getKeyCode();    
    if (keyPressed == AdfKeyStroke.F1_KEY) {
        this.onKeyPressedLoadContainerP1(event, 'yel');
    } else if (keyPressed == AdfKeyStroke.F2_KEY) {
        this.onKeyPressedLoadContainerP1(event, 'nel');
    } else {
        event.cancel();
    }
}

function inputOnBlurFocusHandlerLC(event) {
    SetFocusOnUIcomp(event.getSource());
}