// JS for ConveyorCutOff module--------------------------------------------------

// PAGE1 VIEW JS --------------------------------------------------
function onKeyPressedOnContainerId(event) {
    this.onKeyPressedConveyorCutOffPage1(event, 'con1');
}

function onKeyPressedOnLoadTs(event) {
    this.onKeyPressedConveyorCutOffPage1(event, 'loa1');
}

function onKeyPressedConveyorCutOffPage1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.F9_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue(), item : item
        },
true);
    }
    event.cancel();
}

function onOpenPopupOnConveyorCutOffView(event) {
    var component = event.getSource();
    var linkComp = component.findComponent('confirmPopup');
    linkComp.focus();
}

function okConveyorCutOff(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    
    if (keyPressed == AdfKeyStroke.F1_KEY){
        AdfCustomEvent.queue(component, "customKeyEvent",
        {
            keyPressed : keyPressed
        },
        true);
    }
    event.cancel();
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}