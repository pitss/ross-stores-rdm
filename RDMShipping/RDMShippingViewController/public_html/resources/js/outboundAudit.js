function setOutboundAuditInitialFocus() {
    setTimeout(function () {
        var focusOn = customFindElementByIdContainer('con');
        SetFocusOnUIcomp(focusOn);
    },
0);
}

function onKeyPressedOutboundAudit(event) {

    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue()

    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue
        },
        true);
        event.cancel();
    }
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b1'), true);
    }
     else if (keyPressed == AdfKeyStroke.F4_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function onKeyPressedPopupOutboundAuditYes(event, item) {
     var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function onKeyPressedPopupOutboundAuditNo(event, item) {
     var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "performKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function inputOnBlurFocusHandler(event) {
    setFocusOrSelectOnUIcomp(event.getSource());
}

function setFocusOrSelectOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).select();
    },
0);
}

function SetFocusOnUIcomp(comp) {
    setTimeout(function () {
        $(comp).focus();
    },
0);
}

function customFindElementByIdContainer(id) {
    var element = null;
    var elements = document.getElementsByTagName("*");
    for (i = 0;i < elements.length;i++) {
        if (elements[i].id.indexOf(":" + id) !=  - 1) {
            element = elements[i];
        }
    }
    return element;
}

function OnOpenConfirmPopupOutboundAudit(event) {
   this.onKeyPressedPopupOutboundAuditYes(event, 'yesLinkPopup');
}

function OnOpenConfirmPopupOutboundAuditNo(event) {
   this.onKeyPressedPopupOutboundAuditNo(event, 'noLinkPopup');
}

function onOpenConfirmedPopup(event){
    this.onKeyPressedPopupOutboundAuditYes(event,'okayLinkPopup')
}

function yesNoLinkConfirmPopupOutboundAudit(event) {
 OnOpen(event, 'noLinkPopup')
}
function okayLinkConfirmPopupOutboundAudit(event) {
OnOpen(event, 'okayLinkPopup')
}