function setUnloadContainerPage1InitialFocus() {
    var focusId = 'con';
    var focusIdCode = ("[id*='" + focusId + "']");
    setFocusOrSelectOnUIcomp(focusIdCode);
}

function onKeyPressedUnloadContainerP1(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode()
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        AdfActionEvent.queue(component.findComponent('b2'), true);
    }
}

function onKeyPressedOnContainerIdP1(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : "trailerId"
        },
true);
        event.cancel();
    }
}

function onKeyPressedUnloadContainerP1(event, item) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();

    if (keyPressed == AdfKeyStroke.F1_KEY || keyPressed == AdfKeyStroke.F2_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
    }
    event.cancel();
}

function OnOpenExitPopupUnldCont(event) {
    OnOpen(event, 'nel');
}

function noLinkConfirmPopupUnldCont(event) {
    this.onKeyPressedUnloadContainerP1(event, 'nel');
}

function yesLinkConfirmPopupUnldCont(event) {
    this.onKeyPressedUnloadContainerP1(event, 'yel');
}