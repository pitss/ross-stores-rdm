var isKeyPressed = false;

function onOpenDeletePopup(event) {
    ((event.getSource()).findComponent("fnp")).focus();
}


function onOpenTablePopup(event) {
    ((event.getSource()).findComponent("t1")).focus();
}
/*
 * Page 1
 */

function onBlurTeamId(event) {
    inputOnBlurFocusHanlder(event);
}

function onKeyUpTeamId(event) {
    this.onKeyUpUnloadTeams(event);
}



function onBlurCtnsUnld(event) {
    inputOnBlurFocusHanlder(event);
}

function onKeyUpCtnsUnld(event) {
    this.onKeyUpUnloadTeams(event);
}
/*
 * End page 1
 */

/*
 * Page 2
 */
function onBlurUserld(event) {
    inputOnBlurFocusHanlder(event);
}

function onKeyUpUserld(event) {
    this.onKeyUpUnloadTeams(event);
}

function onBlurTeamIdPage2(event) {
    inputOnBlurFocusHanlder(event);
}

function onKeyUpTeamIdPage2(event) {
    this.onKeyUpUnloadTeams(event);
}

/*
 * End page2
 */
function onKeyUpUnloadTable(event) {
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY || keyPressed == AdfKeyStroke.F5_KEY)
        this.triggerCartonTableServerListener(event);
    else if (keyPressed == AdfKeyStroke.F3_KEY)
        this.exitPopup(event);
    else 
        event.cancel();
}

function triggerCartonTableServerListener(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    AdfCustomEvent.queue(component, "customKeyEvent", 
    {
        keyPressed : keyPressed
    },
true);
    event.cancel();
}

function onKeyUpUnloadTeams(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    switch (keyPressed) {
        case AdfKeyStroke.F6_KEY:
            this.pressOnLink('f6', component);
            break;
        case AdfKeyStroke.F3_KEY:
            this.pressOnLink('f3', component);
            break;
        case AdfKeyStroke.F4_KEY:
            this.pressOnLink('f4', component);
            break;
        case AdfKeyStroke.F5_KEY:
            this.pressOnLink('f5', component);
            break;
        case AdfKeyStroke.F7_KEY:
            this.pressOnLink('f7', component);
            break;
        case AdfKeyStroke.ENTER_KEY:
            this.triggerServerListener(event);
            break;
        case AdfKeyStroke.TAB_KEY:
            this.triggerServerListener(event);
            break;
        default :
            event.cancel();
        break 
    }
}

function triggerServerListener(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    AdfCustomEvent.queue(component, "customKeyEvent", 
    {
        keyPressed : keyPressed, submittedValue : submittedValue
    },
true);
}

function triggerVcl(event) {
    var component = event.getSource();
    var submittedValue = component.getSubmittedValue();
    var keyPressed;
    try {
        keyPressed = event.getKeyCode();
        this.isKeyPressed = true;
    }
    catch (err) {
        if (submittedValue) {
            AdfValueChangeEvent.queue(component, null, submittedValue, false);
            this.isKeyPressed = false;
        }
    }
    inputOnBlurFocusHanlder(event);
    event.cancel();
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}

function setFocusPage1Table() {
    var tableId = 't1';
    var table = ("[id*='" + tableId + "']");
    $(table).focus();
}

function yesNoDeletePopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('fyp', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('fnp', component);
    else 
        event.cancel();
}

function exitPopup(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY)
        this.pressOnLink('f6p', component);
    else 
        event.cancel();
}