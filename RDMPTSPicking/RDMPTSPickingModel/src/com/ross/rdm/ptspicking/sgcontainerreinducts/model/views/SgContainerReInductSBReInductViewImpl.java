package com.ross.rdm.ptspicking.sgcontainerreinducts.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;

import com.ross.rdm.ptspicking.sgcontainerreinducts.model.views.common.SgContainerReInductSBReInductView;

import java.sql.ResultSet;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.ApplicationModule;
import oracle.jbo.server.ViewRowImpl;
import oracle.jbo.server.ViewRowSetImpl;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Feb 05 13:52:21 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------


public class SgContainerReInductSBReInductViewImpl extends RDMViewObjectImpl implements SgContainerReInductSBReInductView {

    private static final String V_CONTAINER_ID = "V_CONTAINER_ID";
    private static final String P_POPULATE_LOC = "PKG_PTS_OPTIONS_ADF.P_POPULATE_LOC";
    private static final String P_PROCESS_NEW_TO_LOC = "PKG_PTS_OPTIONS_ADF.P_PROCESS_NEW_TO_LOC";
    private static final String CONTAINER_RE_INDUCT_WORK_VO = "SgContainerReInductSWorkView";
    private static ADFLogger _logger = ADFLogger.createADFLogger(SgContainerReInductSBReInductViewImpl.class);

    public List<String> callValidateContainerId(String pContainerId) {
        List<String> result = new ArrayList<String>();
        if (callVContainerId()) {
            ApplicationModule am = this.getApplicationModule();

            SgContainerReInductSWorkViewRowImpl workRow =
                (SgContainerReInductSWorkViewRowImpl) am.findViewObject(CONTAINER_RE_INDUCT_WORK_VO).getCurrentRow();
            SgContainerReInductSBReInductViewRowImpl bReInductRow =
                (SgContainerReInductSBReInductViewRowImpl) this.getCurrentRow();

            SQLOutParam pVReturn = new SQLOutParam(null, Types.VARCHAR);
            SQLOutParam pCurToLocation = new SQLOutParam(bReInductRow.getCurToLocation(), Types.VARCHAR);
            SQLOutParam pToLocationId = new SQLOutParam(bReInductRow.getToLocationId(), Types.VARCHAR);
            String pFacilityId = workRow.getFacilityId();

            DBUtils.callStoredProcedure(this.getDBTransaction(), P_POPULATE_LOC, pVReturn, pContainerId, pCurToLocation,
                                        pToLocationId, pFacilityId);
            bReInductRow.setToLocationId((String)pToLocationId.getWrappedData()); 
            bReInductRow.setCurToLocation((String)pCurToLocation.getWrappedData());
            
        } else {
            result.add("E");
            result.add("INV_CID");
        }
        return result;
    }

    private Boolean callVContainerId() {
        ApplicationModule am = this.getApplicationModule();

        SgContainerReInductSWorkViewRowImpl workRow =
            (SgContainerReInductSWorkViewRowImpl) am.findViewObject(CONTAINER_RE_INDUCT_WORK_VO).getCurrentRow();
        SgContainerReInductSBReInductViewRowImpl bReInductRow =
            (SgContainerReInductSBReInductViewRowImpl) this.getCurrentRow();

        String pContainerId = bReInductRow.getContainerId();
        String pFacilityId = workRow.getFacilityId();

        String returnVal =
            (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, V_CONTAINER_ID, pContainerId,
                                                pFacilityId);
        return "Y".equalsIgnoreCase(returnVal);
    }

    public List<String> callProcessNewToLoc(String pContainerId, String pToLocationId) {
        ApplicationModule am = this.getApplicationModule();

        SgContainerReInductSWorkViewRowImpl workRow =
            (SgContainerReInductSWorkViewRowImpl) am.findViewObject(CONTAINER_RE_INDUCT_WORK_VO).getCurrentRow();

        SQLOutParam pVReturn = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pMsgDisplay = new SQLOutParam(null, Types.VARCHAR);
        SQLOutParam pBuiltIns = new SQLOutParam("N", Types.VARCHAR);

        String pCurToLocation = null;
        String pFacilityId = workRow.getFacilityId();

        DBUtils.callStoredProcedure(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS", pFacilityId, workRow.getLanguageCode());
        
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), P_PROCESS_NEW_TO_LOC, pVReturn, pMsgDisplay, pContainerId,
                                    pCurToLocation, pToLocationId, pFacilityId, pBuiltIns);
        
        List<String> result = processMessagesParams(pVReturn, pMsgDisplay);
        if (pBuiltIns != null && "Y".equalsIgnoreCase((String) pBuiltIns.getWrappedData())) {
            result.add((String) pBuiltIns.getWrappedData());
        }
        return result;
    }

    public List<String> processMessagesParams(SQLOutParam p_v_return, SQLOutParam p_msg_display) {
        List<String> msgs = new ArrayList<String>();
        if (p_v_return.getWrappedData() != null && !"RAISE_FAILURE####".equals(p_v_return.getWrappedData())) {
            if (((String) p_v_return.getWrappedData()).contains("RAISE_FAILURE####")) {
                msgs.add("E");
                String error = (String) p_v_return.getWrappedData();
                error = error.replace("RAISE_FAILURE####", "");
                msgs.add(error);
            } else {
                msgs.add((String) p_msg_display.getWrappedData());
                msgs.add((String) p_v_return.getWrappedData());
            }
        }
        return msgs;
    }

    /**
     * This is the default constructor (do not remove).
     */
    public SgContainerReInductSBReInductViewImpl() {
    }

    /**
     * getQueryHitCount - overridden for custom java data source support.
     */
    public long getQueryHitCount(ViewRowSetImpl viewRowSet) {
        //   call necessary database function to get the count of retrieved rows.
        //long value = super.getQueryHitCount(viewRowSet);
        //return value;
        return 1;
    }

    /**
     * create - overridden for custom java data source support.
     * remove all traces of a SQL query for this View Object.
     */
    protected void create() {
        getViewDef().setQuery(null);
        getViewDef().setSelectClause(null);
        setQuery(null);
    }

    /**
     * executeQueryForCollection - overridden for custom java data source support.
     */
    protected void executeQueryForCollection(Object qc, Object[] params, int noUserParams) {
        super.executeQueryForCollection(qc, params, noUserParams);
    }

    /**
     * hasNextForCollection - overridden for custom java data source support.
     */
    protected boolean hasNextForCollection(Object qc) {
        boolean bRet = super.hasNextForCollection(qc);
        return bRet;
    }

    /**
     * createRowFromResultSet - overridden for custom java data source support.
     */
    protected ViewRowImpl createRowFromResultSet(Object qc, ResultSet resultSet) {
        ViewRowImpl value = super.createRowFromResultSet(qc, resultSet);
        return value;
    }
}
