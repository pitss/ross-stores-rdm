package com.ross.rdm.ptspicking.sgptspickandprints.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import java.sql.Date;

import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Aug 08 10:56:42 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SgPtsPickAndPrintSWorkLocalViewRowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        WaveNbr {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getWaveNbr();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setWaveNbr((BigDecimal) value);
            }
        }
        ,
        StartTs {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getStartTs();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setStartTs((Date) value);
            }
        }
        ,
        QueueName {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getQueueName();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setQueueName((String) value);
            }
        }
        ,
        PickFromContainerId {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getPickFromContainerId();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setPickFromContainerId((String) value);
            }
        }
        ,
        LocationId {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getLocationId();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setLocationId((String) value);
            }
        }
        ,
        ItemId {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getItemId();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setItemId((String) value);
            }
        }
        ,
        ItemDesc {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getItemDesc();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setItemDesc((String) value);
            }
        }
        ,
        UnitPickSystemCode {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getUnitPickSystemCode();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setUnitPickSystemCode((String) value);
            }
        }
        ,
        InnerPackQty {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getInnerPackQty();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setInnerPackQty((BigDecimal) value);
            }
        }
        ,
        ShippingConveyable {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getShippingConveyable();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setShippingConveyable((String) value);
            }
        }
        ,
        ShipConveyableReqd {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getShipConveyableReqd();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setShipConveyableReqd((String) value);
            }
        }
        ,
        ProcessByStore {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getProcessByStore();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setProcessByStore((String) value);
            }
        }
        ,
        RoutePrintOrder {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getRoutePrintOrder();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setRoutePrintOrder((String) value);
            }
        }
        ,
        ContQty {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getContQty();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setContQty((BigDecimal) value);
            }
        }
        ,
        UnitQty {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getUnitQty();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setUnitQty((BigDecimal) value);
            }
        }
        ,
        MaxGrabs {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getMaxGrabs();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setMaxGrabs((BigDecimal) value);
            }
        }
        ,
        WhId {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getWhId();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setWhId((String) value);
            }
        }
        ,
        Zone {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getZone();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setZone((String) value);
            }
        }
        ,
        PoNbr {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getPoNbr();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setPoNbr((String) value);
            }
        }
        ,
        StandAlone {
            public Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl obj) {
                return obj.getStandAlone();
            }

            public void put(SgPtsPickAndPrintSWorkLocalViewRowImpl obj, Object value) {
                obj.setStandAlone((String) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(SgPtsPickAndPrintSWorkLocalViewRowImpl object);

        public abstract void put(SgPtsPickAndPrintSWorkLocalViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int WAVENBR = AttributesEnum.WaveNbr.index();
    public static final int STARTTS = AttributesEnum.StartTs.index();
    public static final int QUEUENAME = AttributesEnum.QueueName.index();
    public static final int PICKFROMCONTAINERID = AttributesEnum.PickFromContainerId.index();
    public static final int LOCATIONID = AttributesEnum.LocationId.index();
    public static final int ITEMID = AttributesEnum.ItemId.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int UNITPICKSYSTEMCODE = AttributesEnum.UnitPickSystemCode.index();
    public static final int INNERPACKQTY = AttributesEnum.InnerPackQty.index();
    public static final int SHIPPINGCONVEYABLE = AttributesEnum.ShippingConveyable.index();
    public static final int SHIPCONVEYABLEREQD = AttributesEnum.ShipConveyableReqd.index();
    public static final int PROCESSBYSTORE = AttributesEnum.ProcessByStore.index();
    public static final int ROUTEPRINTORDER = AttributesEnum.RoutePrintOrder.index();
    public static final int CONTQTY = AttributesEnum.ContQty.index();
    public static final int UNITQTY = AttributesEnum.UnitQty.index();
    public static final int MAXGRABS = AttributesEnum.MaxGrabs.index();
    public static final int WHID = AttributesEnum.WhId.index();
    public static final int ZONE = AttributesEnum.Zone.index();
    public static final int PONBR = AttributesEnum.PoNbr.index();
    public static final int STANDALONE = AttributesEnum.StandAlone.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SgPtsPickAndPrintSWorkLocalViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute WaveNbr.
     * @return the WaveNbr
     */
    public BigDecimal getWaveNbr() {
        return (BigDecimal) getAttributeInternal(WAVENBR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute WaveNbr.
     * @param value value to set the  WaveNbr
     */
    public void setWaveNbr(BigDecimal value) {
        setAttributeInternal(WAVENBR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute StartTs.
     * @return the StartTs
     */
    public Date getStartTs() {
        return (Date) getAttributeInternal(STARTTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute StartTs.
     * @param value value to set the  StartTs
     */
    public void setStartTs(Date value) {
        setAttributeInternal(STARTTS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute QueueName.
     * @return the QueueName
     */
    public String getQueueName() {
        return (String) getAttributeInternal(QUEUENAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute QueueName.
     * @param value value to set the  QueueName
     */
    public void setQueueName(String value) {
        setAttributeInternal(QUEUENAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PickFromContainerId.
     * @return the PickFromContainerId
     */
    public String getPickFromContainerId() {
        return (String) getAttributeInternal(PICKFROMCONTAINERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PickFromContainerId.
     * @param value value to set the  PickFromContainerId
     */
    public void setPickFromContainerId(String value) {
        setAttributeInternal(PICKFROMCONTAINERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute LocationId.
     * @return the LocationId
     */
    public String getLocationId() {
        return (String) getAttributeInternal(LOCATIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LocationId.
     * @param value value to set the  LocationId
     */
    public void setLocationId(String value) {
        setAttributeInternal(LOCATIONID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemId.
     * @return the ItemId
     */
    public String getItemId() {
        return (String) getAttributeInternal(ITEMID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemId.
     * @param value value to set the  ItemId
     */
    public void setItemId(String value) {
        setAttributeInternal(ITEMID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemDesc.
     * @return the ItemDesc
     */
    public String getItemDesc() {
        return (String) getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemDesc.
     * @param value value to set the  ItemDesc
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitPickSystemCode.
     * @return the UnitPickSystemCode
     */
    public String getUnitPickSystemCode() {
        return (String) getAttributeInternal(UNITPICKSYSTEMCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitPickSystemCode.
     * @param value value to set the  UnitPickSystemCode
     */
    public void setUnitPickSystemCode(String value) {
        setAttributeInternal(UNITPICKSYSTEMCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute InnerPackQty.
     * @return the InnerPackQty
     */
    public BigDecimal getInnerPackQty() {
        return (BigDecimal) getAttributeInternal(INNERPACKQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute InnerPackQty.
     * @param value value to set the  InnerPackQty
     */
    public void setInnerPackQty(BigDecimal value) {
        setAttributeInternal(INNERPACKQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ShippingConveyable.
     * @return the ShippingConveyable
     */
    public String getShippingConveyable() {
        return (String) getAttributeInternal(SHIPPINGCONVEYABLE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ShippingConveyable.
     * @param value value to set the  ShippingConveyable
     */
    public void setShippingConveyable(String value) {
        setAttributeInternal(SHIPPINGCONVEYABLE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ShipConveyableReqd.
     * @return the ShipConveyableReqd
     */
    public String getShipConveyableReqd() {
        return (String) getAttributeInternal(SHIPCONVEYABLEREQD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ShipConveyableReqd.
     * @param value value to set the  ShipConveyableReqd
     */
    public void setShipConveyableReqd(String value) {
        setAttributeInternal(SHIPCONVEYABLEREQD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ProcessByStore.
     * @return the ProcessByStore
     */
    public String getProcessByStore() {
        return (String) getAttributeInternal(PROCESSBYSTORE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ProcessByStore.
     * @param value value to set the  ProcessByStore
     */
    public void setProcessByStore(String value) {
        setAttributeInternal(PROCESSBYSTORE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RoutePrintOrder.
     * @return the RoutePrintOrder
     */
    public String getRoutePrintOrder() {
        return (String) getAttributeInternal(ROUTEPRINTORDER);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RoutePrintOrder.
     * @param value value to set the  RoutePrintOrder
     */
    public void setRoutePrintOrder(String value) {
        setAttributeInternal(ROUTEPRINTORDER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContQty.
     * @return the ContQty
     */
    public BigDecimal getContQty() {
        return (BigDecimal) getAttributeInternal(CONTQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContQty.
     * @param value value to set the  ContQty
     */
    public void setContQty(BigDecimal value) {
        setAttributeInternal(CONTQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitQty.
     * @return the UnitQty
     */
    public BigDecimal getUnitQty() {
        return (BigDecimal) getAttributeInternal(UNITQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitQty.
     * @param value value to set the  UnitQty
     */
    public void setUnitQty(BigDecimal value) {
        setAttributeInternal(UNITQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute MaxGrabs.
     * @return the MaxGrabs
     */
    public BigDecimal getMaxGrabs() {
        return (BigDecimal) getAttributeInternal(MAXGRABS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MaxGrabs.
     * @param value value to set the  MaxGrabs
     */
    public void setMaxGrabs(BigDecimal value) {
        setAttributeInternal(MAXGRABS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute WhId.
     * @return the WhId
     */
    public String getWhId() {
        return (String) getAttributeInternal(WHID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute WhId.
     * @param value value to set the  WhId
     */
    public void setWhId(String value) {
        setAttributeInternal(WHID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Zone.
     * @return the Zone
     */
    public String getZone() {
        return (String) getAttributeInternal(ZONE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Zone.
     * @param value value to set the  Zone
     */
    public void setZone(String value) {
        setAttributeInternal(ZONE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PoNbr.
     * @return the PoNbr
     */
    public String getPoNbr() {
        return (String) getAttributeInternal(PONBR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PoNbr.
     * @param value value to set the  PoNbr
     */
    public void setPoNbr(String value) {
        setAttributeInternal(PONBR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute StandAlone.
     * @return the StandAlone
     */
    public String getStandAlone() {
        return (String) getAttributeInternal(STANDALONE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute StandAlone.
     * @param value value to set the  StandAlone
     */
    public void setStandAlone(String value) {
        setAttributeInternal(STANDALONE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

