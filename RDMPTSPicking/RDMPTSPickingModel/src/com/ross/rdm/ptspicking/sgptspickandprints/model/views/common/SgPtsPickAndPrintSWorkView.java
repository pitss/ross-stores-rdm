package com.ross.rdm.ptspicking.sgptspickandprints.model.views.common;

import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 02 16:42:54 EDT 2016
// ---------------------------------------------------------------------
public interface SgPtsPickAndPrintSWorkView extends ViewObject {
    void setWorkVariablesPickAndPrint();
}

