package com.ross.rdm.ptspicking.sgptspickings.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;

import java.math.BigDecimal;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Sep 07 10:53:40 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SgPtsPickingSBPtsPickingViewRowImpl extends RDMViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        TiZone {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiZone();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiZone((String) value);
            }
        }
        ,
        TiContainerId {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiContainerId();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiContainerId((String) value);
            }
        }
        ,
        TiItemId {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiItemId();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiItemId((String) value);
            }
        }
        ,
        TiLocationId {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiLocationId();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiLocationId((String) value);
            }
        }
        ,
        TiInnerPackQty {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiInnerPackQty();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiInnerPackQty((BigDecimal) value);
            }
        }
        ,
        TiDestId {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiDestId();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiDestId((BigDecimal) value);
            }
        }
        ,
        TiGrabsReq {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiGrabsReq();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiGrabsReq((BigDecimal) value);
            }
        }
        ,
        TiPriority {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiPriority();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiPriority((String) value);
            }
        }
        ,
        TiDispositionCode {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiDispositionCode();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiDispositionCode((String) value);
            }
        }
        ,
        TiDestCid {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiDestCid();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiDestCid((String) value);
            }
        }
        ,
        TiConfirmQty {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiConfirmQty();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiConfirmQty((BigDecimal) value);
            }
        }
        ,
        TiConfirmQtyString {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getTiConfirmQtyString();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setTiConfirmQtyString((String) value);
            }
        }
        ,
        InnerQtyPack {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getInnerQtyPack();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setInnerQtyPack((BigDecimal) value);
            }
        }
        ,
        innerPackQtyLabel {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getinnerPackQtyLabel();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setinnerPackQtyLabel((String) value);
            }
        }
        ,
        FwdZone {
            public Object get(SgPtsPickingSBPtsPickingViewRowImpl obj) {
                return obj.getFwdZone();
            }

            public void put(SgPtsPickingSBPtsPickingViewRowImpl obj, Object value) {
                obj.setFwdZone((String) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(SgPtsPickingSBPtsPickingViewRowImpl object);

        public abstract void put(SgPtsPickingSBPtsPickingViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int TIZONE = AttributesEnum.TiZone.index();
    public static final int TICONTAINERID = AttributesEnum.TiContainerId.index();
    public static final int TIITEMID = AttributesEnum.TiItemId.index();
    public static final int TILOCATIONID = AttributesEnum.TiLocationId.index();
    public static final int TIINNERPACKQTY = AttributesEnum.TiInnerPackQty.index();
    public static final int TIDESTID = AttributesEnum.TiDestId.index();
    public static final int TIGRABSREQ = AttributesEnum.TiGrabsReq.index();
    public static final int TIPRIORITY = AttributesEnum.TiPriority.index();
    public static final int TIDISPOSITIONCODE = AttributesEnum.TiDispositionCode.index();
    public static final int TIDESTCID = AttributesEnum.TiDestCid.index();
    public static final int TICONFIRMQTY = AttributesEnum.TiConfirmQty.index();
    public static final int TICONFIRMQTYSTRING = AttributesEnum.TiConfirmQtyString.index();
    public static final int INNERQTYPACK = AttributesEnum.InnerQtyPack.index();
    public static final int INNERPACKQTYLABEL = AttributesEnum.innerPackQtyLabel.index();
    public static final int FWDZONE = AttributesEnum.FwdZone.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SgPtsPickingSBPtsPickingViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute TiZone.
     * @return the TiZone
     */
    public String getTiZone() {
        return (String) getAttributeInternal(TIZONE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiZone.
     * @param value value to set the  TiZone
     */
    public void setTiZone(String value) {
        setAttributeInternal(TIZONE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiContainerId.
     * @return the TiContainerId
     */
    public String getTiContainerId() {
        return (String) getAttributeInternal(TICONTAINERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiContainerId.
     * @param value value to set the  TiContainerId
     */
    public void setTiContainerId(String value) {
        setAttributeInternal(TICONTAINERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiItemId.
     * @return the TiItemId
     */
    public String getTiItemId() {
        return (String) getAttributeInternal(TIITEMID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiItemId.
     * @param value value to set the  TiItemId
     */
    public void setTiItemId(String value) {
        setAttributeInternal(TIITEMID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiLocationId.
     * @return the TiLocationId
     */
    public String getTiLocationId() {
        return (String) getAttributeInternal(TILOCATIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiLocationId.
     * @param value value to set the  TiLocationId
     */
    public void setTiLocationId(String value) {
        setAttributeInternal(TILOCATIONID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiInnerPackQty.
     * @return the TiInnerPackQty
     */
    public BigDecimal getTiInnerPackQty() {
        return (BigDecimal) getAttributeInternal(TIINNERPACKQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiInnerPackQty.
     * @param value value to set the  TiInnerPackQty
     */
    public void setTiInnerPackQty(BigDecimal value) {
        setAttributeInternal(TIINNERPACKQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiDestId.
     * @return the TiDestId
     */
    public BigDecimal getTiDestId() {
        return (BigDecimal) getAttributeInternal(TIDESTID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiDestId.
     * @param value value to set the  TiDestId
     */
    public void setTiDestId(BigDecimal value) {
        setAttributeInternal(TIDESTID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiGrabsReq.
     * @return the TiGrabsReq
     */
    public BigDecimal getTiGrabsReq() {
        return (BigDecimal) getAttributeInternal(TIGRABSREQ);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiGrabsReq.
     * @param value value to set the  TiGrabsReq
     */
    public void setTiGrabsReq(BigDecimal value) {
        setAttributeInternal(TIGRABSREQ, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiPriority.
     * @return the TiPriority
     */
    public String getTiPriority() {
        return (String) getAttributeInternal(TIPRIORITY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiPriority.
     * @param value value to set the  TiPriority
     */
    public void setTiPriority(String value) {
        setAttributeInternal(TIPRIORITY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiDispositionCode.
     * @return the TiDispositionCode
     */
    public String getTiDispositionCode() {
        return (String) getAttributeInternal(TIDISPOSITIONCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiDispositionCode.
     * @param value value to set the  TiDispositionCode
     */
    public void setTiDispositionCode(String value) {
        setAttributeInternal(TIDISPOSITIONCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiDestCid.
     * @return the TiDestCid
     */
    public String getTiDestCid() {
        return (String) getAttributeInternal(TIDESTCID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiDestCid.
     * @param value value to set the  TiDestCid
     */
    public void setTiDestCid(String value) {
        setAttributeInternal(TIDESTCID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiConfirmQty.
     * @return the TiConfirmQty
     */
    public BigDecimal getTiConfirmQty() {
        return (BigDecimal) getAttributeInternal(TICONFIRMQTY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiConfirmQty.
     * @param value value to set the  TiConfirmQty
     */
    public void setTiConfirmQty(BigDecimal value) {
        setAttributeInternal(TICONFIRMQTY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TiConfirmQtyString.
     * @return the TiConfirmQtyString
     */
    public String getTiConfirmQtyString() {
        return (String) getAttributeInternal(TICONFIRMQTYSTRING);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TiConfirmQtyString.
     * @param value value to set the  TiConfirmQtyString
     */
    public void setTiConfirmQtyString(String value) {
        setAttributeInternal(TICONFIRMQTYSTRING, value);
    }

    /**
     * Gets the attribute value for the calculated attribute InnerQtyPack.
     * @return the InnerQtyPack
     */
    public BigDecimal getInnerQtyPack() {
        return (BigDecimal) getAttributeInternal(INNERQTYPACK);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute InnerQtyPack.
     * @param value value to set the  InnerQtyPack
     */
    public void setInnerQtyPack(BigDecimal value) {
        setAttributeInternal(INNERQTYPACK, value);
    }

    /**
     * Gets the attribute value for the calculated attribute innerPackQtyLabel.
     * @return the innerPackQtyLabel
     */
    public String getinnerPackQtyLabel() {
        return (String) getAttributeInternal(INNERPACKQTYLABEL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute innerPackQtyLabel.
     * @param value value to set the  innerPackQtyLabel
     */
    public void setinnerPackQtyLabel(String value) {
        setAttributeInternal(INNERPACKQTYLABEL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute FwdZone.
     * @return the FwdZone
     */
    public String getFwdZone() {
        return (String) getAttributeInternal(FWDZONE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute FwdZone.
     * @param value value to set the  FwdZone
     */
    public void setFwdZone(String value) {
        setAttributeInternal(FWDZONE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

