package com.ross.rdm.ptspicking.sgptspickings.model.views;

import java.math.BigDecimal;

public class ZoneSelectionRow {

    private String zone;
    private BigDecimal units;
    private String pipFlag;

    public ZoneSelectionRow() {

    }

    public ZoneSelectionRow(String zone, BigDecimal units, String pipFlag) {
        if (zone != null && units != null && pipFlag != null) {
            this.setPipFlag(pipFlag);
            this.setUnits(units);
            this.setZone(zone);
        }
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getZone() {
        return zone;
    }

    public void setUnits(BigDecimal units) {
        this.units = units;
    }

    public BigDecimal getUnits() {
        return units;
    }

    public void setPipFlag(String pipFlag) {
        this.pipFlag = pipFlag;
    }

    public String getPipFlag() {
        return pipFlag;
    }

}
