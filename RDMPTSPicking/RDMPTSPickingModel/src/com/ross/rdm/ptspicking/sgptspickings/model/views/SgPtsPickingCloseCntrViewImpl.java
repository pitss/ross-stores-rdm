package com.ross.rdm.ptspicking.sgptspickings.model.views;

import com.ross.rdm.common.utils.model.base.RDMViewObjectImpl;
import com.ross.rdm.common.utils.model.base.RDMViewRowImpl;
import com.ross.rdm.common.utils.model.util.dbcall.DBUtils;
import com.ross.rdm.common.utils.model.util.dbcall.SQLOutParam;
import com.ross.rdm.ptspicking.model.services.PTSPickingAppModuleImpl;
import com.ross.rdm.ptspicking.sgptspickings.model.views.common.SgPtsPickingCloseCntrView;

import java.math.BigDecimal;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Sep 19 13:46:04 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SgPtsPickingCloseCntrViewImpl extends RDMViewObjectImpl implements SgPtsPickingCloseCntrView {
    /**
     * This is the default constructor (do not remove).
     */
    public SgPtsPickingCloseCntrViewImpl() {
    }

    public List continueCloseCidProcess() {
        PTSPickingAppModuleImpl ptsAm = (PTSPickingAppModuleImpl) this.getApplicationModule();
        SgPtsPickingSWorkViewRowImpl workRow =
            (SgPtsPickingSWorkViewRowImpl) ptsAm.getSgPtsPickingSWorkView().getCurrentRow();
        SgPtsPickingSBPtsPickingViewRowImpl bPtsPickingRow =
            (SgPtsPickingSBPtsPickingViewRowImpl) ptsAm.getSgPtsPickingSBPtsPickingView().getCurrentRow();
        SgPtsPickingCloseCntrViewRowImpl closeCidRow = (SgPtsPickingCloseCntrViewRowImpl) this.getCurrentRow();
        SgPtsPickingSBWorkLocalViewRowImpl bWorkLocalRow =
            (SgPtsPickingSBWorkLocalViewRowImpl) ptsAm.getSgPtsPickingSBWorkLocalView().getCurrentRow();

        String closeCid = closeCidRow.getCloseCid();
        String facilityId = workRow.getFacilityId();
        List returnedValues = null;
        bPtsPickingRow.setTiDestCid(closeCid);
        if (StringUtils.isEmpty(bWorkLocalRow.getTiNoConfirmClose()) &&
            StringUtils.equalsIgnoreCase(bWorkLocalRow.getPtsConfirm(), "N") &&
            StringUtils.equalsIgnoreCase(this.callVcontainer(closeCid, facilityId), "N")) {
            bWorkLocalRow.setTiNoConfirmClose("F");
        } else
            bWorkLocalRow.setTiNoConfirmClose("D");
        if (StringUtils.isNotEmpty(closeCid)) {
            String noConfirmClose = bWorkLocalRow.getTiNoConfirmClose();
            if (StringUtils.contains(noConfirmClose, "D") || StringUtils.contains(noConfirmClose, "S")) {
                returnedValues = new ArrayList<Object>();
                List valCartonMsgs = this.callValCarton(closeCid, noConfirmClose, facilityId, workRow);
                if (valCartonMsgs != null) {
                    bWorkLocalRow.setTiNoConfirmClose(null);
                    return valCartonMsgs;
                } else {
                    returnedValues.add("C");
                    returnedValues.add("SG_CLS_CRTN");
                }
            } else {
                returnedValues = new ArrayList<Object>();
                returnedValues.add("call_p_confirm_cid");
            }
        }
        return returnedValues;
    }

    public List continueCloseCidProcess2() {
        PTSPickingAppModuleImpl ptsAm = (PTSPickingAppModuleImpl) this.getApplicationModule();
        SgPtsPickingSWorkViewRowImpl workRow =
            (SgPtsPickingSWorkViewRowImpl) ptsAm.getSgPtsPickingSWorkView().getCurrentRow();
        SgPtsPickingSBPtsPickingViewRowImpl bPtsPickingRow =
            (SgPtsPickingSBPtsPickingViewRowImpl) ptsAm.getSgPtsPickingSBPtsPickingView().getCurrentRow();
        SgPtsPickingCloseCntrViewRowImpl closeCidRow = (SgPtsPickingCloseCntrViewRowImpl) this.getCurrentRow();
        SgPtsPickingSBWorkLocalViewRowImpl bWorkLocalRow =
            (SgPtsPickingSBWorkLocalViewRowImpl) ptsAm.getSgPtsPickingSBWorkLocalView().getCurrentRow();

        String closeCid = closeCidRow.getCloseCid();
        List returnedValues = null;
        if (bPtsPickingRow.getTiConfirmQty().compareTo(bPtsPickingRow.getTiGrabsReq()) < 0) {
            bWorkLocalRow.setSplitCont("Y");
            if ((returnedValues = this.callSplitContainer(closeCid, workRow, bWorkLocalRow, bPtsPickingRow)) != null)
                return returnedValues;

        } else {
            returnedValues = new ArrayList<Object>();
            bWorkLocalRow.setSplitCont("N");
            bWorkLocalRow.setTiClosePressed("Y");
            returnedValues.add("p_call_check_qty");
        }
        return returnedValues;
    }

    public void callUpdPickDirective() {
        PTSPickingAppModuleImpl ptsAm = (PTSPickingAppModuleImpl) this.getApplicationModule();
        SgPtsPickingSWorkViewRowImpl workRow =
            (SgPtsPickingSWorkViewRowImpl) ptsAm.getSgPtsPickingSWorkView().getCurrentRow();
        SgPtsPickingSBPtsPickingViewRowImpl bPtsPickingRow =
            (SgPtsPickingSBPtsPickingViewRowImpl) ptsAm.getSgPtsPickingSBPtsPickingView().getCurrentRow();
        SgPtsPickingSBWorkLocalViewRowImpl bWorkLocalRow =
            (SgPtsPickingSBWorkLocalViewRowImpl) ptsAm.getSgPtsPickingSBWorkLocalView().getCurrentRow();
        SgPtsPickingCloseCntrViewRowImpl closeCidRow = (SgPtsPickingCloseCntrViewRowImpl) this.getCurrentRow();
        bPtsPickingRow.setTiDestCid(null);
        closeCidRow.setCloseCid(null);
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_SG_PTS_PICKING_ADF.UPD_Pick_Directive",
                                             workRow.getFacilityId(), bWorkLocalRow.getTiWaveNbr(),
                                             bPtsPickingRow.getTiDestId(), bPtsPickingRow.getTiItemId(),
                                             workRow.getUser());
    }

    public BigDecimal callCgetLpnQtyLeft(String facilityId, String containerId, String itemId) {
        return (BigDecimal) DBUtils.callStoredFunction(this.getDBTransaction(), Types.NUMERIC,
                                                       "PKG_SG_PTS_PICKING_ADF.c_get_lpn_qty_left", facilityId,
                                                       containerId, itemId);
    }

    public List callCloseContainer(String closeCid, String userChoice) {
        PTSPickingAppModuleImpl ptsAm = (PTSPickingAppModuleImpl) this.getApplicationModule();
        SgPtsPickingSWorkViewRowImpl workRow =
            (SgPtsPickingSWorkViewRowImpl) ptsAm.getSgPtsPickingSWorkView().getCurrentRow();
        SgPtsPickingSBPtsPickingViewRowImpl bPtsPickingRow =
            (SgPtsPickingSBPtsPickingViewRowImpl) ptsAm.getSgPtsPickingSBPtsPickingView().getCurrentRow();
        SgPtsPickingSBWorkLocalViewRowImpl bWorkLocalRow =
            (SgPtsPickingSBWorkLocalViewRowImpl) ptsAm.getSgPtsPickingSBWorkLocalView().getCurrentRow();
        SgPtsPickingSBPtsQtyViewRowImpl bPtsQtyViewRow =
            (SgPtsPickingSBPtsQtyViewRowImpl) ptsAm.getSgPtsPickingSBPtsQtyView().getCurrentRow();
        SQLOutParam vReturn = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam msgDisplay = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam priority = new SQLOutParam(bPtsPickingRow.getTiPriority(), Types.VARCHAR);
        SQLOutParam builtIn = new SQLOutParam(new BigDecimal(-1), Types.NUMERIC);
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS",
                                             workRow.getFacilityId(), workRow.getLanguageCode());
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_SG_PTS_PICKING_ADF.P_CLOSE_CNTR", vReturn,
                                             msgDisplay, closeCid, bPtsPickingRow.getTiContainerId(),
                                             bPtsPickingRow.getTiDestId(), bPtsPickingRow.getTiItemId(), priority,
                                             bPtsQtyViewRow.getTiNewGrabQty(), bWorkLocalRow.getMldEnabled(),
                                             bWorkLocalRow.getSplitCont(), bWorkLocalRow.getTiClosePressed(),
                                             bWorkLocalRow.getTiWaveNbr(), bWorkLocalRow.getTiYesPressed(),
                                             workRow.getFacilityId(), workRow.getUser(), userChoice, builtIn);

        List returnedValues = this.processMessagesParams(vReturn, msgDisplay);
        if (returnedValues == null)
            returnedValues = new ArrayList<Object>();
        BigDecimal built = (BigDecimal) builtIn.getWrappedData();
        if (built != null)
            returnedValues.add(built);
        int resultSize = returnedValues.size();
        if (resultSize == 2)
            return returnedValues;
        else if (resultSize == 1) {
            built = (BigDecimal) returnedValues.get(0);
            if (new BigDecimal(2).equals(built)) {
                bWorkLocalRow.setTiYesPressed(null);
                returnedValues.add("B_PTS_QTY.TI_NEW_GRAB_QTY");
                return returnedValues;
            } else if (new BigDecimal(1).equals(built)) {
                String zoneId = bPtsPickingRow.getTiZone();
                if ("Y".equalsIgnoreCase(bWorkLocalRow.getSplitCont())) {
                    this.clearRow((SgPtsPickingCloseCntrViewRowImpl) this.getCurrentRow());
                }
                this.clearRow(bPtsPickingRow);
                bPtsPickingRow.setTiZone(zoneId);
                returnedValues.add("B_PTS_PICKING.TI_CONTAINER_ID");
                return returnedValues;
            }
            returnedValues = null;
        }
        return returnedValues;
    }

    public void clearRow(RDMViewRowImpl row) {
        row.refresh(Row.REFRESH_UNDO_CHANGES | Row.REFRESH_WITH_DB_FORGET_CHANGES);
    }

    public String callCheckCntrContent(String closeCid, String facilityId) {
        SQLOutParam vReturn = new SQLOutParam("", Types.VARCHAR);
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR,
                                                   "PKG_SG_PTS_PICKING_ADF.CHECK_CNTR_CONTENT", vReturn, facilityId,
                                                   closeCid);
    }

    public List callSplitContainer(String closeCid, SgPtsPickingSWorkViewRowImpl workRow,
                                   SgPtsPickingSBWorkLocalViewRowImpl bWorkLocalRow,
                                   SgPtsPickingSBPtsPickingViewRowImpl bPtsPickingRow) {

        SQLOutParam vReturn = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam updCode = new SQLOutParam(bWorkLocalRow.getTiUpsCode(), Types.VARCHAR);

        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS",
                                             workRow.getFacilityId(), workRow.getLanguageCode());
        //callCheckCloseCid only to insert correctly into ROSS_EVENTCODE_USERID_TMP in the same request of the DB changes
        callCheckCloseCid(closeCid);
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_SG_PTS_PICKING_ADF.P_SPLIT_CLOSE", vReturn,
                                             closeCid, bPtsPickingRow.getTiConfirmQty(),
                                             bPtsPickingRow.getTiContainerId(), bPtsPickingRow.getTiDestCid(),
                                             bPtsPickingRow.getTiDestId(), bPtsPickingRow.getTiDispositionCode(),
                                             bPtsPickingRow.getTiItemId(), bPtsPickingRow.getTiLocationId(),
                                             bPtsPickingRow.getTiPriority(), bPtsPickingRow.getTiZone(),
                                             bWorkLocalRow.getTiInnerPackQty(), updCode, bWorkLocalRow.getTiWaveNbr(),
                                             workRow.getFacilityId(), workRow.getUser());
        String error = (String) vReturn.getWrappedData();
        List returnedValues = null;
        if (StringUtils.isNotEmpty(error)) {
            returnedValues = new ArrayList<Object>();
            returnedValues.add("E");
            returnedValues.add(error);
        }

        return returnedValues;
    }

    public List callValCarton(String closeCid, String noConfirmClose, String facilityId,
                              SgPtsPickingSWorkViewRowImpl workRow) {
        SQLOutParam vReturn = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam msgDisplay = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam noConfirmCloseO = new SQLOutParam(noConfirmClose, Types.VARCHAR);
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS",
                                             workRow.getFacilityId(), workRow.getLanguageCode());
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_SG_PTS_PICKING_ADF.VAL_CARTON", vReturn,
                                             msgDisplay, closeCid, noConfirmCloseO, facilityId);
        return this.processMessagesParams(vReturn, msgDisplay);
    }

    public String callVcontainer(String closeCid, String facilityId) {
        return (String) DBUtils.callStoredFunction(this.getDBTransaction(), Types.VARCHAR, "v_container_id", closeCid,
                                                   facilityId);
    }

    public List callCheckCloseCid(String closeCid) {
        PTSPickingAppModuleImpl ptsAm = (PTSPickingAppModuleImpl) this.getApplicationModule();
        SgPtsPickingSWorkViewRowImpl workRow =
            (SgPtsPickingSWorkViewRowImpl) ptsAm.getSgPtsPickingSWorkView().getCurrentRow();
        SgPtsPickingSBPtsPickingViewRowImpl bPtsPickingRow =
            (SgPtsPickingSBPtsPickingViewRowImpl) ptsAm.getSgPtsPickingSBPtsPickingView().getCurrentRow();
        SQLOutParam vReturn = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam msgDisplay = new SQLOutParam("", Types.VARCHAR);
        SQLOutParam confirmQty = new SQLOutParam(bPtsPickingRow.getTiConfirmQty(), Types.NUMERIC);
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_MSG_VARS.SET_MSG_VARS",
                                             workRow.getFacilityId(), workRow.getLanguageCode());
        DBUtils.callStoredProcedureWithNulls(this.getDBTransaction(), "PKG_SG_PTS_PICKING_ADF.CHECK_CLOSE_CID", vReturn,
                                             msgDisplay, confirmQty, bPtsPickingRow.getTiDestId(), closeCid,
                                             bPtsPickingRow.getTiContainerId(), bPtsPickingRow.getTiLocationId(),
                                             bPtsPickingRow.getTiItemId(), workRow.getFacilityId(), workRow.getUser());
        ptsAm.callDoCommit();
        return this.processMessagesParams(vReturn, msgDisplay);
    }
}

