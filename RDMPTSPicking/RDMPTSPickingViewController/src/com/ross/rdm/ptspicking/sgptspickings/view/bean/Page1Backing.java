package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.utils.view.util.JSFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends PtsPickingBaseBacking {
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    private static final String DOWN_PRESSED = "downPressed";

    private RichPopup choseZonePopup;
    private RichOutputText zonePopupOutput;
    private RichLink f1Link;
    private RichLink f2Link;
    private RichLink f3Link;
    private RichLink f4Link;
    private RichLink f5Link;
    private RichLink containerHiddenLink;
    private RichLink destCidHiddenLink;
    private RichLink navigateCloseCrtnHiddenLink;
    private RichLink arrowDownHiddenLink;

    public Page1Backing() {

    }

    public void onChangedTiZone(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
                removeErrorOnActiveField();
                hideMessagesPanel();
                this.updateModel(vce);
                String newZone = (String) vce.getNewValue();
                if (StringUtils.isNotEmpty(newZone)) {
                    this.processZone(this.callVZone(newZone.toUpperCase()));
                }
            } else {
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.getAndShowMessagesPanel("E", "SG_INV_ZONE");
            }
        }
    }

    public void onChangedTiDestCid(ValueChangeEvent vce) {
        if (!isNavigationExecuted() && !isDownPressed()) {
            this.updateModel(vce);
            this.getSgPtsPickingSBean().setExecutedKey(false);
            ActionEvent actionEvent = new ActionEvent(this.getDestCidHiddenLink());
            actionEvent.queue();
        }
    }

    public void onChangedTiContainerId(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            this.updateModel(vce);
            if (StringUtils.isNotEmpty((String) (vce.getNewValue()))) {
                this.getSgPtsPickingSBean().setExecutedKey(false);
                ActionEvent actionEvent = new ActionEvent(this.getContainerHiddenLink());
                actionEvent.queue();
            } else {
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.getAndShowMessagesPanel("E", "SG_INV_CNTR");
            }
        }
    }

    public void onChangedTiConfirmQty(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            removeErrorOnActiveField();
            hideMessagesPanel();
            String newQty = (String) vce.getNewValue();
            if (StringUtils.isNotEmpty(newQty)) {
                if (NumberUtils.isNumber(newQty))
                    this.callKeyNextItemConfirmQty(new BigDecimal(newQty));
                else {
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                    this.getAndShowMessagesPanel("E", "SG_INV_QTY");
                }
            }
        }
    }

    private void processZone(String ValidZone) {
        if ("N".equalsIgnoreCase(ValidZone)) {
            this.setErrorOnField(this.getTiZone(), this.getZoneIcon());
            this.getAndShowMessagesPanel("E", "SG_INV_ZONE");
        } else if (StringUtils.isEmpty((String) ADFUtils.getBoundAttributeValue("SingleZoneFlag"))) {
            this.showSingleZonePopup();
        } else {
            this.setFocusOnCid2();
        }
    }

    private void processContainer(List listResult) {
        if (listResult != null) {
            int resultSize = listResult.size();
            if (resultSize == 2) {
                this.setErrorOnField(this.getTiCid(), this.getTiCid2Icon());
                this.processMessages(listResult);
            } else if (resultSize == 1)
                if (BigDecimal.ONE.equals(listResult.get(0))) {
                    this.removeErrorOnActiveField();
                    this.hideMessagesPanel();
                    this.callPgetPick();
                }
        }
    }

    private String processF2() {
        if (this.checkContainers()) {
            ADFUtils.setBoundAttributeValue("AuditTrailReq", "Y");
            ADFUtils.setBoundAttributeValue("WorkLocalTiLocationId", ADFUtils.getBoundAttributeValue("TiLocationId"));
            if (this.callPopulateZoneSelect((String) ADFUtils.getBoundAttributeValue("TiZone")))
                return "CZoneSelect";
        }
        return null;
    }

    private String processF3() {
        this.callUpdPickDirective();
        if (this.callpSaveLaborProd()) {
            this.callTrExit();
            return "backGlobalHome";
        }
        return null;
    }

    private void processF4F5keyIn(String key) {
        if (this.checkContainers()) {
            this.setFocusOnC1();
            ADFUtils.setBoundAttributeValue("TiConfirmQty", ADFUtils.getBoundAttributeValue("TiGrabsReq"));
            ADFUtils.setBoundAttributeValue("TiConfirmQtyString", ADFUtils.getBoundAttributeValue("TiGrabsReq"));
            ADFUtils.setBoundAttributeValue("KeyPressed", key);
        }
    }

    private void processVlc(String submittedValue, String currentFieldId) {
        if ("tiZ".equals(currentFieldId)) {
            this.onChangedTiZone(new ValueChangeEvent(this.getTiZone(), null, submittedValue));
        } else if ("tiC".equals(currentFieldId)) {
            this.onChangedTiContainerId(new ValueChangeEvent(this.getTiCid(), null, submittedValue));
        } else if ("tiD1".equals(currentFieldId)) {
            this.onChangedTiDestCid(new ValueChangeEvent(this.getTiD1(), null, submittedValue));
        } else if ("tiC1".equals(currentFieldId)) {
            this.onChangedTiConfirmQty(new ValueChangeEvent(this.getTiD1(), null, submittedValue));
        }
    }

    private void emptyVlc(String currentFieldId) {
        if ("tiZ".equals(currentFieldId)) {
            this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.getAndShowMessagesPanel("E", "SG_INV_ZONE");
        } else if ("tiC".equals(currentFieldId)) {
            this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.getAndShowMessagesPanel("E", "SG_INV_CNTR");
        } else if ("tiC1".equals(currentFieldId)) {
            this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.getAndShowMessagesPanel("E", "PARTIAL_ENTRY");
        }
    }

    private void processDestCid() {
        String keyPressed = (String) ADFUtils.getBoundAttributeValue("KeyPressed");
        if ("F4".equalsIgnoreCase(keyPressed))
            new ActionEvent(getNavigateCloseCrtnHiddenLink()).queue();
        else if ("F5".equalsIgnoreCase(keyPressed))
            this.processF5();
        else {
            if ("Y".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("PtsConfirm"))) {
                if (null != (ADFUtils.getBoundAttributeValue("TiDestCid"))) {
                    this.setCurrentBlockName("B_PTS_PICKING");
                    this.callPconfirmCid();
                    this.setTiConfirmInd("N");
                } else {
                    String destCId = (String) ADFUtils.getBoundAttributeValue("TiDestCid");
                    BigDecimal confirmQty = (BigDecimal) ADFUtils.getBoundAttributeValue("TiConfirmQty");
                    if (null == destCId && ((BigDecimal.ZERO).compareTo(confirmQty)) < 0) {
                        this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                        this.getAndShowMessagesPanel("E", "SG_PARTL_ENTRY");
                    }
                }
            } else {
                this.setCurrentBlockName("B_PTS_PICKING");
                if (this.callPconfirmCid()) {
                    //Check the block as P_CONFIRM_CID may navigate to B_PTS_LOC
                    //Original comment in 6i:
                    // Check for current block, if it's currently B_PTS_LOC then a
                    // current open container did not exist and user was prompted
                    // for location_id
                    if (this.isPtsPickingBlock()) {
                        this.callPconfirmQty();
                        this.setTiConfirmInd("Y");
                    }
                }
            }
        }
    }

    public String processF4() {
        ADFUtils.setBoundAttributeValue("KeyPressed", null);
        this.getSgPtsPickingSBean().setFocusOn("B_CLOSE_CRTN.CLOSE_CID");
        this.setCurrentBlockName("B_CLOSE_CNTR");
        this.setRegionLoaded(false);
        return "CCloseCrtn";
    }

    private void processF5() {
        String destCId = (String) ADFUtils.getBoundAttributeValue("TiDestCid");
        BigDecimal confirmQty = (BigDecimal) ADFUtils.getBoundAttributeValue("TiConfirmQty");
        if (null == destCId && ((BigDecimal.ZERO).compareTo(confirmQty)) < 0) {
            this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.getAndShowMessagesPanel("E", "SG_PARTL_ENTRY");
        } else {
            this.callGetLpnQtyLeft();
            this.setCurrentBlockName("B_PTS_PICKING");
            if (this.callPconfirmCid()) {
                this.callPconfirmQty();
                this.setTiConfirmInd("Y");
            }
        }
    }

    private void callValidateContainerId() {
        OperationBinding oper = ADFUtils.findOperation("callValidateCntr");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            this.processContainer((List) oper.getResult());
    }

    private boolean callCheckCarton() {
        OperationBinding oper = ADFUtils.findOperation("callPcheckCarton");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            boolean hasErrors = this.processMessages((List) oper.getResult());
            if (hasErrors) {
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            }
            return !hasErrors;
        }
        return false;
    }

    private boolean callGetLpnQtyLeft() {
        OperationBinding oper = ADFUtils.findOperation("callGetLpnQtyLeft");
        oper.execute();
        return oper.getErrors() == null || oper.getErrors().isEmpty();
    }

    protected boolean callFirstCheckQty() {
        OperationBinding oper = ADFUtils.findOperation("callFirstCheckQty");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                if (resultValues.size() == 2) {
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                    this.processMessages(resultValues);
                    return false;
                }
            }
        }
        return true;
    }

    private void callPconfirmQty() {
        this.setCurrentBlockName("B_PTS_PICKING");
        if (this.callFirstCheckQty()) {
            if (this.callPcheckQty()) {
                endOfPConfirmQty();
            }
        }
    }

    private boolean callCheckDestCidFormat(String destCId) {
        OperationBinding oper = ADFUtils.findOperation("callCheckDestCidFormat");
        oper.getParamsMap().put("destCid", destCId);
        oper.execute();
        boolean hasErrors = false;
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            hasErrors = this.processMessages((List) oper.getResult());
            if (hasErrors) {
                this.setErrorOnField(this.getTiD1(), this.getTiDestCidIcon());
            } else {
                this.removeErrorOnActiveField();
                this.hideMessagesPanel();
            }
        } else {
            hasErrors = true;
        }
        return !hasErrors;
    }

    private boolean callTrExit() {
        OperationBinding oper = ADFUtils.findOperation("callTrExit");
        oper.execute();
        return oper.getErrors() == null || oper.getErrors().isEmpty();
    }

    private boolean callpSaveLaborProd() {
        OperationBinding oper = ADFUtils.findOperation("callSaveLaborProd");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null)
                if (resultValues.size() == 2)
                    return !this.processMessages(resultValues);
        } else
            return false;
        return true;
    }

    private boolean callUpdPickDirective() {
        OperationBinding oper = ADFUtils.findOperation("callUpdPickDirective");
        Map map = oper.getParamsMap();
        map.put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
        map.put("userId", ADFUtils.getBoundAttributeValue("User"));
        oper.execute();
        return oper.getErrors() == null || oper.getErrors().isEmpty();
    }

    public String callKeyDownDestCid() {
        OperationBinding oper = ADFUtils.findOperation("callCheckOpenCarton");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if ("B_pts_picking.ti_confirm_qty".equalsIgnoreCase((String) oper.getResult())) {
                this.hideMessagesPanel();
                this.removeErrorOnActiveField();
                this.setFocusOnC1();
            }
        }
        return null;
    }

    private void callKeyNextItemConfirmQty(BigDecimal newQty) {
        ADFUtils.setBoundAttributeValue("TiConfirmQty", newQty);
        OperationBinding oper = ADFUtils.findOperation("callKeyNextItemConfirmQty");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                int size = resultValues.size();
                if (size == 2) {
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                    this.processMessages(resultValues);
                } else if (size == 1) {
                    String returnVal = (String) resultValues.get(0);
                    if ("call_p_confirm_qty".equalsIgnoreCase(returnVal)) {
                        this.callPconfirmQty();
                    } else if ("B_pts_picking.ti_dest_cid".equalsIgnoreCase(returnVal)) {
                        this.setFocusOnDestCId();
                    } else if ("B_CLOSE_CRTN.CLOSE_CID".equalsIgnoreCase(returnVal)) {
                        new ActionEvent(getNavigateCloseCrtnHiddenLink()).queue();
                    }
                }
            } else {
                this.hideMessagesPanel();
                this.removeErrorOnActiveField();
            }
        }
    }

    private String callVZone(String zone) {
        OperationBinding oper = ADFUtils.findOperation("callVZone");
        oper.getParamsMap().put("zoneId", zone);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            return (String) oper.getResult();
        return null;
    }

    private void showSingleZonePopup() {
        this.removeErrorOnActiveField();
        this.disableActiveField();
        this.hideMessagesPanel();
        this.getZonePopupOutput().setValue(this.getMessage("SINGLEZONEPICK", "W"));
        this.getChoseZonePopup().show(new RichPopup.PopupHints());
    }

    public void yesZonePopupActioListener(ActionEvent actionEvent) {
        this.getChoseZonePopup().hide();
        this.setAttributesAfterPopup("Y");
    }

    public void noZonePopupActionListener(ActionEvent actionEvent) {
        this.getChoseZonePopup().hide();
        this.setAttributesAfterPopup("N");
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            setDownPressed(null);
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            String currentFieldId = (clientEvent.getComponent()).getId();
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (valueNotChanged(submittedValue, field.getValue())) {
                    if ("tiD1".equals(currentFieldId) || StringUtils.isNotEmpty(submittedValue)) {
                        //For tiD1 (DEST_CID) we will always fire the event
                        this.processVlc(submittedValue, currentFieldId);
                    } else {
                        emptyVlc(currentFieldId);
                    }
                }

            } else if (DOWN_KEY_CODE.equals(keyPressed)) {
                if ("tiD1".equals(currentFieldId)) {
                    setDownPressed(Boolean.TRUE);
                    new ActionEvent(getArrowDownHiddenLink()).queue();
                }
            }
        }
    }

    private void setDownPressed(Boolean pressed) {
        ADFContext.getCurrent().getPageFlowScope().put(DOWN_PRESSED, pressed);
    }

    private Boolean isDownPressed() {
        return Boolean.TRUE.equals(ADFContext.getCurrent().getPageFlowScope().get(DOWN_PRESSED));
    }

    private void setTiConfirmInd(String ind) {
        ADFUtils.setBoundAttributeValue("TiConfirmInd", ind);
    }

    private void setAttributesAfterPopup(String choice) {
        ADFUtils.setBoundAttributeValue("SingleZoneFlag", choice);
        ADFUtils.setBoundAttributeValue("TiClosePressed", "N");
        ADFUtils.setBoundAttributeValue("TiYesPressed", null);
        this.setFocusOnCid2();
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!this.isRegionLoaded()) {
            RichInputText active = this.getActiveFocusedField();
            if (active == null) {
                setFocusOn(SG_ZONE);
            }

            this.setInnerPackQtyLabel((String) ADFUtils.getBoundAttributeValue("innerPackQtyLabel"));
            if ("p_validate_cntr".equalsIgnoreCase(this.getToExecuteOnLoad())) {
                this.callValidateContainerId();
                this.setToExecuteOnLoad(null);
            } else if ("p_get_pick".equalsIgnoreCase(this.getToExecuteOnLoad())) {
                this.callPgetPick();
                this.setToExecuteOnLoad(null);
            } else if ("p_check_container_empty".equalsIgnoreCase(this.getToExecuteOnLoad())) {
                this.callpCheckEmptyContainer();
                this.setToExecuteOnLoad(null);
            } else if ("p_confirm_qty".equalsIgnoreCase(this.getToExecuteOnLoad())) {
                this.callPconfirmQty();
                this.setTiConfirmInd("Y");
                this.setToExecuteOnLoad(null);
            } else if ("TR_KEY_IN_F4".equalsIgnoreCase(this.getToExecuteOnLoad())) {
                this.setToExecuteOnLoad(null);
                new ActionEvent(getF4Link()).queue();
            } else if (THIRD_POPUP_WHILE_CLOSING.equalsIgnoreCase(this.getToExecuteOnLoad())) {
                this.setToExecuteOnLoad(null);
                showLpnEmptyPopup("W", "LPN_EMPTY", THIRD_POPUP_WHILE_CLOSING);
            } else if (StringUtils.isNotEmpty(this.getMsgShowOnLoadPage1())) {
                this.showMessagesPanel("E", this.getMsgShowOnLoadPage1());
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.setMsgShowOnLoadPage1(null);
            }

            if (getActiveFocusedField() != null) {
                this.setFocusOnUIComponent(this.getActiveFocusedField());
            }
            this.setRegionLoaded(true);
        }
    }

    private boolean checkContainers() {
        return this.nvl((String) ADFUtils.getBoundAttributeValue("TiContainerId"),
                        "x").equalsIgnoreCase(this.nvl((String) ADFUtils.getBoundAttributeValue("TiContainerId1"),
                                                       "y"));
    }

    private boolean checkIsNumeric(String cid) {
        boolean valid = false;
        if (StringUtils.isNotEmpty(cid)) {
            if (!NumberUtils.isNumber(cid)) {
                this.setErrorOnField(this.getTiD1(), this.getTiDestCidIcon());
                this.getAndShowMessagesPanel("E", "MUST_BE_NUMERIC");
            } else {
                this.hideMessagesPanel();
                valid = true;
            }
        } else {
            valid = true;
        }
        return valid;
    }

    public String f2SgZsAction() {
        return this.trKeyIn("F2");
    }

    public String f3SgExAction() {
        return this.trKeyIn("F3");
    }

    public String f4SgScAction() {
        return this.trKeyIn("F4");
    }

    public String f5SgEeAction() {
        return this.trKeyIn("F5");
    }

    public String trKeyIn(String keyIn) {
        String goTo = null;
        if ("F2".equalsIgnoreCase(keyIn)) {
            if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F2,
                                                    RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
                goTo = this.processF2();
            } else {
                showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            }
        } else if ("F3".equalsIgnoreCase(keyIn)) {
            goTo = this.processF3();
        } else if ("F4".equalsIgnoreCase(keyIn)) {
            if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F4,
                                                    RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
                this.removeErrorOnActiveField();
                this.hideMessagesPanel();
                this.processF4F5keyIn(keyIn);
            } else {
                showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            }
        } else if ("F5".equalsIgnoreCase(keyIn)) {
            if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F5,
                                                    RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
                this.removeErrorOnActiveField();
                this.hideMessagesPanel();
                this.processF4F5keyIn(keyIn);
            } else {
                showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            }
        }
        return goTo;
    }

    public void setChoseZonePopup(RichPopup choseZonePopup) {
        this.choseZonePopup = choseZonePopup;
    }

    public RichPopup getChoseZonePopup() {
        return choseZonePopup;
    }

    public void setZonePopupOutput(RichOutputText zonePopupOutput) {
        this.zonePopupOutput = zonePopupOutput;
    }

    public RichOutputText getZonePopupOutput() {
        return zonePopupOutput;
    }


    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setF5Link(RichLink f5Link) {
        this.f5Link = f5Link;
    }

    public RichLink getF5Link() {
        return f5Link;
    }

    public void setContainerHiddenLink(RichLink containerHiddenLink) {
        this.containerHiddenLink = containerHiddenLink;
    }

    public RichLink getContainerHiddenLink() {
        return containerHiddenLink;
    }

    public void setDestCidHiddenLink(RichLink destCidHiddenLink) {
        this.destCidHiddenLink = destCidHiddenLink;
    }

    public RichLink getDestCidHiddenLink() {
        return destCidHiddenLink;
    }

    public String getContainerHiddenAction() {
        if (this.getSgPtsPickingSBean() != null) {
            if(this.getSgPtsPickingSBean().getExecutedKey()!=null){
            if (!this.getSgPtsPickingSBean().getExecutedKey()) {
                if (StringUtils.isNotEmpty((String) ADFUtils.getBoundAttributeValue("TiContainerId"))) {
                    this.callValidateContainerId();
                }
            }}
            this.getSgPtsPickingSBean().setExecutedKey(null);
        }
        return null;
    }

    public String destCidHiddenAction() {
        if (!isNavigationExecuted() && this.getSgPtsPickingSBean() != null) {
            if (!this.getSgPtsPickingSBean().getExecutedKey()) {
                this.removeErrorOnActiveField();
                this.hideMessagesPanel();
                String newDestCid = (String) ADFUtils.getBoundAttributeValue("TiDestCid");
                if (this.checkIsNumeric(newDestCid)) {
                    if (this.callCheckDestCidFormat(newDestCid)) {
                        if (this.callCheckCarton())
                            this.processDestCid();
                    }
                }
            }
            this.getSgPtsPickingSBean().setExecutedKey(null);
        }
        return null;
    }

    public void setNavigateCloseCrtnHiddenLink(RichLink navigateCloseCrtnHiddenLink) {
        this.navigateCloseCrtnHiddenLink = navigateCloseCrtnHiddenLink;
    }

    public RichLink getNavigateCloseCrtnHiddenLink() {
        return navigateCloseCrtnHiddenLink;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_ptspicking_sgptspickings_view_pageDefs_Page1PageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public void setF1Link(RichLink f1Link) {
        this.f1Link = f1Link;
    }

    public RichLink getF1Link() {
        return f1Link;
    }

    public String processF1() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_PTS_PICKING_F1,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
            hideMessagesPanel();
            removeErrorOnActiveField();
            ADFUtils.findOperation("processF1").execute();
            resetIp();
            setFocusOnZone();
            refreshMainPanel();
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }

    public void setArrowDownHiddenLink(RichLink arrowDownHiddenLink) {
        this.arrowDownHiddenLink = arrowDownHiddenLink;
    }

    public RichLink getArrowDownHiddenLink() {
        return arrowDownHiddenLink;
    }

    public String yesContainerEmptyAction() {
        this.getEmptyCntrPopup().hide();
        this.callCloseCntr("Y");
        return null;
    }

    public String noContainerEmptyAction() {
        this.getEmptyCntrPopup().hide();
        this.enableActiveField();
        return null;
    }
}
