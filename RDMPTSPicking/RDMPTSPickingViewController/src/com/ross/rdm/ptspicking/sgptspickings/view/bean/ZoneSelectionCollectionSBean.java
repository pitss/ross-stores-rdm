package com.ross.rdm.ptspicking.sgptspickings.view.bean;
import com.ross.rdm.ptspicking.sgptspickings.model.views.ZoneSelectionRow;
import java.io.Serializable;
import java.util.List;
import oracle.adf.view.rich.component.rich.data.RichTable;
public class ZoneSelectionCollectionSBean implements Serializable {
    @SuppressWarnings("compatibility:-6634707556745440139")
    private static final long serialVersionUID = 1L;

    private RichTable zoneTable;
    private List<ZoneSelectionRow> zoneList;


    public ZoneSelectionCollectionSBean() {
        super();
    }

    public void setZoneTable(RichTable zoneTable) {
        this.zoneTable = zoneTable;
    }

    public RichTable getZoneTable() {
        return zoneTable;
    }

    public void setZoneList(List<ZoneSelectionRow> zoneList) {
        this.zoneList = zoneList;
    }

    public List<ZoneSelectionRow> getZoneList() {
        return zoneList;
    }
}
