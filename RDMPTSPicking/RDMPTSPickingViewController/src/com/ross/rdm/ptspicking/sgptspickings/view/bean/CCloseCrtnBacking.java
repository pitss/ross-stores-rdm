package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for CCloseCrtn.jspx
// ---
// ---------------------------------------------------------------------
public class CCloseCrtnBacking extends PtsPickingBaseBacking {

    private RichPopup closeCidPopup;
    private RichOutputText closeCidOutputPopup;
    private RichLink f3Link;

    public CCloseCrtnBacking() {

    }

    public void onChangedCloseCid(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            this.removeErrorOfCloseCid();
            this.hideMessagesPanel();
            this.updateModel(vce);
            if (vce.getNewValue() != null) {
                this.processCloseCid(((String) vce.getNewValue()).toUpperCase());
            }
        }
    }

    public boolean processCloseCid(String newCloseCid) {
        if (this.callCheckCloseCid(newCloseCid))
            return this.callNextProcessCloseCId();
        return true;
    }


    public String yesPopupCloseCidAction() {
        return this.processYNCLoseCid("Y");
    }

    public String noPopupCloseCidAction() {
        return this.processYNCLoseCid("N");
    }

    public String yesContainerEmptyAction() {
        this.getEmptyCntrPopup().hide();
        this.callCloseCntr("Y");
        return null;
    }

    public String noContainerEmptyAction() {
        this.getEmptyCntrPopup().hide();
        this.setFocusOnCloseCid();
        return null;
    }

    public String f3Action() {
        return this.processF3();
    }

    private String processYNCLoseCid(String confirm) {
        this.getCloseCidPopup().hide();
        this.enableActiveField();
        this.setCurrentBlockName("B_CLOSE_CNTR");

        if ("Y".equalsIgnoreCase(confirm)) {
            if (null != ADFUtils.getBoundAttributeValue("TiConfirmQty")) {
                if (this.callPconfirmCid()) {
                    if (this.callNextProcessCloseCId2()) {
                        processCloseAfterCheckQty();
                    }
                }
            } else {
                if (!this.callcallCheckCntrContent()) {
                    this.showEmptyCntrPopup(this.getMessage("SG_EMPTY_CONT", "E"));
                } else {
                    //callCheckCloseCid only to insert correctly into ROSS_EVENTCODE_USERID_TMP in the same request of the DB changes
                    this.callCheckCloseCid((String) ADFUtils.getBoundAttributeValue("CloseCid"));
                    this.callCloseCntr("N");
                }
                ADFUtils.setBoundAttributeValue("TiDestCid", null);
                ADFUtils.setBoundAttributeValue("CloseCid", null);
                ADFUtils.setBoundAttributeValue("TiConfirmQty", null);
                ADFUtils.setBoundAttributeValue("TiConfirmQtyString", null);
                this.callPgetPick();
            }
            ADFUtils.setBoundAttributeValue("TiNoConfirmClose", null);
        } else if ("N".equalsIgnoreCase(confirm)) {
            ADFUtils.setBoundAttributeValue("TiNoConfirmClose", null);
            //SAME CODE THAN F3
            return processF3();
        }
        return null;
    }

    private void callUpdPickDirective() {
        OperationBinding oper = ADFUtils.findOperation("callUpdPickDirective1");
        oper.execute();
    }

    private boolean callNextProcessCloseCId2() {
        OperationBinding oper = ADFUtils.findOperation("continueCloseCidProcess2");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List codesList = (List) oper.getResult();
            if (codesList != null) {
                int sizeLisrCodes = codesList.size();
                if (sizeLisrCodes == 2) {
                    this.setErrorOnCloseCid();
                    return !this.processMessages(codesList);
                } else if (sizeLisrCodes == 1) {
                    if ("p_call_check_qty".equalsIgnoreCase((String) codesList.get(0)))
                        return this.callPcheckQty();
                } else {
                    this.removeErrorOfCloseCid();
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public boolean callNextProcessCloseCId() {
        OperationBinding oper = ADFUtils.findOperation("continueCloseCidProcess");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List codesList = (List) oper.getResult();
            if (codesList != null) {
                int sizeLisrCodes = codesList.size();
                if (sizeLisrCodes == 2) {
                    String msgType = (String) codesList.get(0);
                    String msg = getMessage((String) codesList.get(1), msgType);
                    if ("C".equalsIgnoreCase(msgType))
                        this.showCloseCidPopup(msg);
                    else if ("E".equalsIgnoreCase(msgType)) {
                        this.showMessagesPanel(msgType, msg);
                        return false;
                    }
                } else if (sizeLisrCodes == 1) {
                    if ("call_p_confirm_cid".equalsIgnoreCase((String) codesList.get(0)))
                        this.callPconfirmCid();
                }
            }
        }
        return true;
    }

    public void setFocusOnCloseCid() {
        this.setFocusOnField("SG_CLS_CRTN", this.getClo());
    }

    public void showCloseCidPopup(String msg) {
        this.removeErrorOfCloseCid();
        this.hideMessagesPanel();
        this.disableCloseCid();
        this.getCloseCidOutputPopup().setValue(msg);
        this.getCloseCidPopup().show(new RichPopup.PopupHints());
    }


    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (valueNotChanged(submittedValue, field.getValue())) {
                    this.onChangedCloseCid(new ValueChangeEvent(this.getClo(), null, submittedValue));
                }
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!this.isRegionLoaded()) {
            this.setFocusOnCloseCid();
            this.setRegionLoaded(true);
        }
    }


    private String processF3() {
        this.setFocusOnDestCId();
        ADFUtils.setBoundAttributeValue("CloseCid", null);
        ADFUtils.setBoundAttributeValue("KeyPressed", null);
        ADFUtils.setBoundAttributeValue("TiDestCid", null);
        if ("N".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("PtsConfirm"))) {
            ADFUtils.setBoundAttributeValue("TiConfirmQty", ADFUtils.getBoundAttributeValue("TiGrabsReq"));
            ADFUtils.setBoundAttributeValue("TiConfirmQtyString", ADFUtils.getBoundAttributeValue("TiGrabsReq"));
        } else {
            this.setFocusOnC1();
        }
        this.setRegionLoaded(false);
        this.setCurrentBlockName("B_PTS_PICKING");
        return "Page1";
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }


    public void setCloseCidPopup(RichPopup closeCidPopup) {
        this.closeCidPopup = closeCidPopup;
    }

    public RichPopup getCloseCidPopup() {
        return closeCidPopup;
    }

    public void setCloseCidOutputPopup(RichOutputText closeCidOutputPopup) {
        this.closeCidOutputPopup = closeCidOutputPopup;
    }

    public RichOutputText getCloseCidOutputPopup() {
        return closeCidOutputPopup;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_ptspicking_sgptspickings_view_pageDefs_CCloseCrtnPageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
