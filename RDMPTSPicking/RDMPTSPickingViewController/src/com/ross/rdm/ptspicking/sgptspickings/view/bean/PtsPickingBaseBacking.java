package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.math.BigDecimal;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.commons.lang.StringUtils;

public class PtsPickingBaseBacking extends RDMPTSPickingBackingBean {
    static final String SCC_ICON = "logo";
    protected static final String B_PTS_PICKING = "B_PTS_PICKING";
    protected static final String SET_ZONE_IN_TABLE = "setCurrentZone";
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private RichPopup emptyCidPopup;
    private RichIcon emptyCidPopupIcon;
    private RichOutputText emptyCidPoupOutput;
    private RichPopup valDestExistPopup;
    private RichOutputText validDestExistPopupOutput;
    private RichPopup emptyLpnPopup;
    private RichPopup noPicksFwdPopup;
    private RichPopup noPicksZnPopup;
    private RichIcon emptyLpnPopupIcon;
    private RichOutputText noPicksFwdOutput;
    private RichOutputText noPicksZnOutput;
    private RichOutputText emptyLpnPoupOutput;
    private RichInputText tiD1;
    private RichInputText tiC1;
    private RichInputText tiZone;
    private RichInputText tiCid;
    private RichInputText clo;
    private RichIcon closeCidIcon;
    private RichIcon zoneIcon;
    private RichIcon tiCid2Icon;
    private RichIcon tiDestCidIcon;
    private RichIcon tiConfirmQtyIcon;
    private RichIcon zonePopupIcon;
    private RichPanelFormLayout pflMain;
    private RichPopup emptyCntrPopup;
    private RichOutputText emptyCntrPopupOutput;
    private RichLink navigatePtsLocHiddenLink;
    private boolean notAllowed = false;

    static final String SG_PTS_PICKING_PAGE_FLOW_BEAN = "SgPtsPickingSBean";
    static final String SG_ZONE = "SG_ZONE";
    static final String SG_CID2 = "SG_CID2";
    static final String SG_SL = "SG_SL";
    static final String SG_CLS_CRTN = "SG_CLS_CRTN";
    static final String SG_CONFIRM_GRAB = "SG_CONFIRM_GRAB";
    static final String ATTR_NEW_ITEM = "NEW_ITEM";
    static final String THIRD_POPUP_WHILE_CLOSING = "THIRD_POPUP_WHILE_CLOSING";
    static final Double UP_KEY_CODE = new Double("38.0");
    static final Double DOWN_KEY_CODE = new Double("40.0");


    public PtsPickingBaseBacking() {
        super();
    }

    protected boolean isPtsPickingBlock() {
        return B_PTS_PICKING.equalsIgnoreCase(this.getCurrentBlockName());
    }

    protected boolean callPconfirmCid() {
        OperationBinding oper = ADFUtils.findOperation("callPconfirmCid");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List codesList = (List) oper.getResult();
            if (codesList != null) {
                int sizeLisrCodes = codesList.size();
                if (sizeLisrCodes == 2) {
                    if (this.isPtsPickingBlock()) {
                        this.processMessages(codesList);
                        this.setErrorOnField(this.getTiD1(), this.getTiDestCidIcon());
                        return false;
                    } else {
                        this.setMsgShowOnLoadPage1((String) codesList.get(1));
                        this.commonDestCidBehav();
                    }
                } else if (sizeLisrCodes == 3) {
                    if (this.isPtsPickingBlock()) {
                        this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                        this.showMessagesPanel((String) codesList.get(0), (String) codesList.get(1));
                    }
                    return this.processBuiltInsConfirmCid((BigDecimal) codesList.get(2), (String) codesList.get(1));
                } else if (sizeLisrCodes == 1)
                    return this.processBuiltInsConfirmCid((BigDecimal) codesList.get(0), null);
            }
        }
        return false;
    }


    private void callPvalDestExist() {
        OperationBinding oper = ADFUtils.findOperation("callPvalDestExist");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null) {
                int sizeValues = returnedValues.size();
                if (sizeValues == 3) {
                    String msgType = (String) returnedValues.get(0);
                    String msg = (String) returnedValues.get(1);
                    String popupOrGoItem = (String) returnedValues.get(2);
                    if ("P_VAL_DEST_EXIST_POPUP".equalsIgnoreCase(popupOrGoItem)) {
                        this.showValidDestExistPopup(msg);
                    } else if ("B_pts_picking.TI_Dest_cid".equalsIgnoreCase(popupOrGoItem)) {
                        this.setMsgShowOnLoadPage1(msg);
                        if (this.isPtsPickingBlock()) {
                            this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                            this.showMessagesPanel(msgType, msg);
                        }
                        this.commonDestCidBehav();
                    }
                } else if (sizeValues == 1) {
                    if ("B_pts_picking.TI_Dest_cid".equalsIgnoreCase((String) returnedValues.get(0))) {
                        this.setFocusOnDestCId();
                        if (!this.isPtsPickingBlock()) {
                            this.setRegionLoaded(false);
                            this.setCurrentBlockName(B_PTS_PICKING);
                            ADFUtils.invokeAction("Page1");
                        }
                    }
                }
            }
        }
    }

    protected boolean callPvalPopupDestExist() {
        OperationBinding oper = ADFUtils.findOperation("callPvalPopupDestExist");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            return !this.processMessages((List) oper.getResult());
        return false;
    }

    public String noValidDestExistAction() {
        this.getValDestExistPopup().hide();
        this.commonDestCidBehav();
        return null;
    }

    public String yesValidDestExistAction() {
        if (!this.isPtsPickingBlock()) {
            if (!this.callPvalPopupDestExist()) {
                this.hideMessagesPanel();
                this.removeErrorOfCloseCid();
                this.setFocusOn("SG_SL");
                return "Page1";
            }
        } else {
            this.hideMessagesPanel();
            this.removeErrorOnActiveField();
            this.setFocusOnC1();
        }
        return null;
    }

    protected void showValidDestExistPopup(String msg) {
        this.removeErrorOnActiveField();
        this.disableActiveField();
        this.getValidDestExistPopupOutput().setValue(msg);
        this.getValDestExistPopup().show(new RichPopup.PopupHints());
    }

    protected void commonDestCidBehav() {
        ADFUtils.setBoundAttributeValue("TiDestCid", null);
        this.setFocusOnDestCId();
        if (this.isPtsPickingBlock()) {
            this.getTiD1().resetValue();
            this.refreshContentOfUIComponent(this.getTiD1());
        } else {
            this.setRegionLoaded(false);
            this.setCurrentBlockName(B_PTS_PICKING);
            ADFUtils.invokeAction("Page1");
        }
    }


    protected boolean callPcheckQty() {
        OperationBinding oper = ADFUtils.findOperation("callpCheckQty");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null && !returnedValues.isEmpty()) {
                int sizeVals = returnedValues.size();
                if (sizeVals == 3) {
                    String popupName = (String) returnedValues.get(2);
                    if ("THIRD_POPUP".equals(popupName) && this.getClass().toString().contains("CCloseCrtnBacking") &&
                        this.isPtsPickingBlock()) {
                        //Issue 4140, trying to show LPN_EMPTY popup but navigating, so popup was not shown
                        this.setToExecuteOnLoad(THIRD_POPUP_WHILE_CLOSING);
                    } else {
                        this.showLpnEmptyPopup((String) returnedValues.get(0), (String) returnedValues.get(1),
                                               popupName);
                    }
                    return false;
                } else if (sizeVals == 2) {
                    this.showMessagesPanel((String) returnedValues.get(0), (String) returnedValues.get(1));
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                    return !this.processMessages(returnedValues);
                } else
                    return this.callpFirstPopupCheckQty(null);
            } else {
                return callpEndCheckQty();
            }
        }
        return false;
    }

    protected boolean callpFirstPopupCheckQty(String userChoice) {
        OperationBinding oper = ADFUtils.findOperation("callpFirstPopupCheckQty");
        if (userChoice != null)
            oper.getParamsMap().put("userChoice", userChoice);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null && !returnedValues.isEmpty()) {
                int sizeVals = returnedValues.size();
                if (sizeVals == 2) {
                    Object valReturned = returnedValues.get(1);
                    this.showMessagesPanel("E", (String) valReturned);
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                } else if (sizeVals == 1) {
                    Object valReturned = returnedValues.get(0);
                    if (new BigDecimal(1).equals(valReturned)) {
                        this.processBuiltInOneFirstCheckQty();
                        this.endOfPConfirmQty();
                    } else if (new BigDecimal(2).equals(valReturned))
                        this.callPgetPick();
                } else
                    return this.callpSecondPopupCheckQty(null);
            }
        }
        return false;
    }

    protected boolean callpSecondPopupCheckQty(String userChoice) {
        OperationBinding oper = ADFUtils.findOperation("callpSecondPopupCheckQty");
        if (userChoice != null)
            oper.getParamsMap().put("userChoice", userChoice);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null && !returnedValues.isEmpty()) {
                int sizeVals = returnedValues.size();
                if (sizeVals == 1) {
                    if (new BigDecimal(2).equals(returnedValues.get(0)))
                        this.callPgetPick();
                } else if (sizeVals == 3)
                    this.showLpnEmptyPopup((String) returnedValues.get(0), (String) returnedValues.get(1),
                                           (String) returnedValues.get(2));
                else {
                    return this.callpEndCheckQty();
                }
            } else {
                //OTM 4077, this method wasn't being called for MAX GRABS
                return this.callpEndCheckQty();
            }
        }
        return false;
    }

    protected boolean callpThirdPopupCheckQty(String userChoice) {
        OperationBinding oper = ADFUtils.findOperation("callpThirdPopupCheckQty");
        if (userChoice != null)
            oper.getParamsMap().put("userChoice", userChoice);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null && !returnedValues.isEmpty()) {
                int sizeVals = returnedValues.size();
                if (sizeVals == 1) {
                    Object valReturned = returnedValues.get(0);
                    if (new BigDecimal(3).equals(valReturned)) {
                        if (this.isPtsPickingBlock()) {
                            this.setRegionLoaded(false);
                            ADFUtils.invokeAction("CPtsQty");
                        } else {
                            this.setGoBlock("CPtsQty");
                        }
                    }
                } else {
                    return this.callpEndCheckQty();
                }
            } else {
                if (!"B_CLOSE_CNTR".equals(getCurrentBlockName())) {
                    return this.callpEndCheckQty();
                } else if (this.callpEndCheckQty()) {
                    this.processWorkLaborProd();
                    if (!this.callcallCheckCntrContent())
                        this.showEmptyCntrPopup(this.getMessage("SG_EMPTY_CONT", "E"));
                    else
                        this.callCloseCntr("N");
                }
            }
        }
        return false;
    }

    protected boolean callpFourthPopupCheckQty(String userChoice) {
        OperationBinding oper = ADFUtils.findOperation("callpFourthPopupCheckQty");
        if (userChoice != null)
            oper.getParamsMap().put("userChoice", userChoice);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null && !returnedValues.isEmpty()) {
                int sizeVals = returnedValues.size();
                if (sizeVals == 1) {
                    Object valReturned = returnedValues.get(0);
                    if (new BigDecimal(2).equals(valReturned))
                        this.callPgetPick();
                    else if (new BigDecimal(4).equals(valReturned)) {
                        this.fourthCheckQtyBuilt4();
                    }
                } else {
                    return this.callpEndCheckQty();
                }
            } else {
                return this.callpEndCheckQty();
            }
        }
        return false;
    }

    protected void fourthCheckQtyBuilt4() {
        this.clearRowSBPtsPicking();
        ADFUtils.setBoundAttributeValue("TiZone", ADFUtils.getBoundAttributeValue("TiZone1"));
        this.setFocusOnCid2();
        if (!this.isPtsPickingBlock()) {
            this.navigateToPage1();
        } else if (getPflMain() != null) {
            refreshContentOfUIComponent(getPflMain());
        }
    }

    protected boolean callpEndCheckQty() {
        OperationBinding oper = ADFUtils.findOperation("callpEndPopupCheckQty");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List returnedValues = (List) oper.getResult();
            if (returnedValues != null && !returnedValues.isEmpty()) {
                int sizeVals = returnedValues.size();
                if (sizeVals == 1) {
                    Object valReturned = returnedValues.get(0);
                    if (new BigDecimal(5).equals(valReturned)) {
                        this.processCommonBuiltIn();
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected void callPopupChoiceCheckEmptyCid(String userChoice, String popupName) {
        OperationBinding oper = ADFUtils.findOperation("callPopupChoiceCheckEmptyCid");
        Map map = oper.getParamsMap();
        map.put("userChoice", userChoice);
        map.put("popupName", popupName);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            this.processCheckEmptyCid((Map) oper.getResult());
        }
    }

    protected void callPgetPick() {
        OperationBinding oper = ADFUtils.findOperation("pGetPick");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                this.refreshMainPanel();
                int size = resultValues.size();
                if (size == 3) {
                    if ("C".equalsIgnoreCase((String) resultValues.get(0)))
                        this.showConfrimPopupCheckContainerEmpty(resultValues);
                    else
                        this.showMsgOrNavigate(resultValues);
                } else if (size == 2) {
                    if (this.isPtsPickingBlock()) {
                        this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                        this.processMessages(resultValues);
                    }
                } else if (size == 1) {
                    Object returnVal = resultValues.get(0);
                    if (returnVal instanceof BigDecimal)
                        this.processBuiltIns((BigDecimal) resultValues.get(0));
                    else if (returnVal instanceof String)
                        if ("GO_B_PTS_PICKING.TI_CONTAINER_ID".equalsIgnoreCase((String) returnVal)) {
                            this.setFocusOnCid2();
                            this.resetIp();
                        } else if ("GO_B_PTS_PICKING.TI_DEST_CID".equalsIgnoreCase((String) returnVal)) {
                            this.setFocusOnDestCId();
                        } else if ("GO_B_PTS_PICKING.TI_CONFIRM_QTY".equalsIgnoreCase((String) returnVal)) {
                            this.setFocusOnC1();
                        }
                    if (!this.isPtsPickingBlock())
                        this.navigateToPage1();
                }
            }
        }

    }

    protected void callLogAuditTrailPCheckQty() {
        ADFUtils.findOperation("logAuditTrailPCheckQty").execute();
    }


    protected boolean callPopulateZoneSelect(String zoneId) {
        this.setRegionLoaded(false);
        OperationBinding oper = ADFUtils.findOperation("populateZoneSelect");
        oper.getParamsMap().put("zoneId", zoneId);
        oper.execute();
        return oper.getErrors() == null || oper.getErrors().isEmpty();
    }

    public String yesEmptyLpnAction() {
        this.processYesNoEmptyLpn("Y");
        return null;
    }


    public String noEmptyLpnAction() {
        this.processYesNoEmptyLpn("N");
        return null;
    }

    public String yesEmptyCidPopupAction() {
        this.callPopupChoiceCheckEmptyCid("Y", this.getEmptyCidPopup().getLauncherVar());
        this.getEmptyCidPopup().hide();
        return null;
    }

    public String noEmptyCidPopupAction() {
        this.callPopupChoiceCheckEmptyCid("N", this.getEmptyCidPopup().getLauncherVar());
        this.getEmptyCidPopup().hide();
        return null;
    }

    private void processYesNoEmptyLpn(String userChoice) {
        this.getEmptyLpnPopup().hide();
        this.enableActiveField();
        String popupName = this.getEmptyLpnPopup().getLauncherVar();
        switch (popupName) {
        case "FIRST_POPUP":
            this.callpFirstPopupCheckQty(userChoice);
            break;
        case "SECOND_POPUP":
            this.callpSecondPopupCheckQty(userChoice);
            break;
        case "THIRD_POPUP":
            this.callpThirdPopupCheckQty(userChoice);
            break;
        case THIRD_POPUP_WHILE_CLOSING:
            if (this.callpThirdPopupCheckQty(userChoice)) {
                processCloseAfterCheckQty();
            }
            break;
        case "FOURTH_POPUP":
            this.callpFourthPopupCheckQty(userChoice);
            break;
        default:
            break;
        }
    }

    protected void processWorkLaborProd() {
        addBigDecimal("UnitsProcessed", (BigDecimal) ADFUtils.getBoundAttributeValue("TiConfirmQty"));
        addBigDecimal("ContainersProcessed", BigDecimal.ONE);
        addBigDecimal("OperationsPerformed", BigDecimal.ONE);
    }

    private void addBigDecimal(String attributeBinding, BigDecimal qtyToAdd) {
        BigDecimal qty = (BigDecimal) ADFUtils.getBoundAttributeValue(attributeBinding);
        if (qty == null || qtyToAdd == null) {
            ADFUtils.setBoundAttributeValue(attributeBinding, null);
        } else {
            ADFUtils.setBoundAttributeValue(attributeBinding, qty.add(qtyToAdd));
        }
    }

    protected void processCommonBuiltIn() {
        this.clearRowSBPtsPicking();
        ADFUtils.setBoundAttributeValue("TiZone", ADFUtils.getBoundAttributeValue("TiZone1"));
        this.setFocusOnCid2();
        this.resetIp();
        if (!this.isPtsPickingBlock())
            this.navigateToPage1();
    }

    protected void processBuiltIns(BigDecimal builtIn) {
        if ((new BigDecimal(1)).equals(builtIn) || (new BigDecimal(4)).equals(builtIn) ||
            (new BigDecimal(5)).equals(builtIn)) {
            this.processCommonBuiltIn();
        } else if ((new BigDecimal(2)).equals(builtIn)) {
            ADFUtils.setBoundAttributeValue("TiAlreadyCheckFlag", "N");
            this.processCommonBuiltIn();
        } else if ((new BigDecimal(6)).equals(builtIn)) {
            ADFUtils.setBoundAttributeValue("TiEndOfZone", "N");
            this.processCommonBuiltIn();
        } else if ((new BigDecimal(3)).equals(builtIn)) {
            this.goToPtsQty();
        }
    }

    protected void processBuiltInOneFirstCheckQty() {
        boolean nullWorkLocalTiDestId = false;

        if (ADFUtils.getBoundAttributeValue("WorkLocalTiDestId") == null &&
            ADFUtils.getBoundAttributeValue("TiDestId") != null) {
            //Workaround to fix 4077, Work_local.Ti_dest_id was null when inserting into Ross_Audit_Trail
            ADFUtils.setBoundAttributeValue("WorkLocalTiDestId", ADFUtils.getBoundAttributeValue("TiDestId"));
            nullWorkLocalTiDestId = true;
        }

        this.clearRowSBPtsPicking();
        this.setFocusOnZone();
        ADFUtils.setBoundAttributeValue("TiQtyShorted", "Y");
        if ("F5".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("KeyPressed"))) {
            this.callLogAuditTrailPCheckQty();
            if (nullWorkLocalTiDestId) {
                ADFUtils.setBoundAttributeValue("WorkLocalTiDestId", null);
            }
            ADFUtils.setBoundAttributeValue("KeyPressed", null);
        }
        this.resetIp();
        if (!this.isPtsPickingBlock())
            this.navigateToPage1();

    }


    protected void processCheckEmptyCid(Map resultMap) {
        if (resultMap != null) {
            Set set = resultMap.entrySet();
            Iterator iterator = set.iterator();
            if (iterator.hasNext()) {
                Map.Entry me = (Map.Entry) iterator.next();
                Object key = me.getKey();
                Object object = me.getValue();
                if ("BUILT_INS".equalsIgnoreCase((String) key))
                    this.processBuiltIns((BigDecimal) object);
                else if ("P_GO_BLOCK".equalsIgnoreCase((String) key))
                    this.processGoBlock((BigDecimal) object);
                else if ("ERROR".equalsIgnoreCase((String) key)) {
                    if (this.isPtsPickingBlock()) {
                        this.showMessagesPanel("E", (String) object);
                        this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                    }
                }
            }
        }
    }


    protected boolean processBuiltInsConfirmCid(BigDecimal built, String msg) {
        if (new BigDecimal(1).equals(built) || new BigDecimal(2).equals(built)) {
            this.setMsgShowOnLoadPage1(msg);
            this.commonDestCidBehav();
            return false;
        } else if (new BigDecimal(5).equals(built)) {
            this.setRegionLoaded(false);
            if (this.isPtsPickingBlock()) {
                this.setCurrentBlockName("B_PTS_LOC");
                new ActionEvent(getNavigatePtsLocHiddenLink()).queue();
            } else
                this.setGoBlock("CPtsLoc");
        } else if (new BigDecimal(7).equals(built)) {
            this.setMsgShowOnLoadPage1(msg);
            this.callPvalDestExist();
        }
        return true;
    }

    private void processBPtsPickingBlock(String goTo, String msgType, String msgCode, String msgText) {
        if ("NO_PICKS_ZN_CONT".equals(msgCode) && !"Y".equals(ADFUtils.getBoundAttributeValue("SingleZoneFlag"))) {
            //This is a particular case, we have to show the NO_PICKS_ZN message in a popup and continue after accepting it
            showNoPicksZnPopup(this.getMessage("NO_PICKS_ZN", "I"));
        } else {
            switch (goTo) {
            case "GO_B_PTS_PICKING.ZONE_ID":
                this.setFocusOnZone();
                if (this.isEWmsgType(msgType))
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.showMessagesPanel(msgType, msgText);
                this.refreshMainPanel();
                break;
            case "GO_B_PTS_PICKING.TI_CONTAINER_ID":
                this.setFocusOnCid2();
                if (this.isEWmsgType(msgType))
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.showMessagesPanel(msgType, msgText);
                this.refreshMainPanel();
                break;
            case "GO_B_PTS_PICKING.TI_DEST_CID":
                this.setFocusOnDestCId();
                if (this.isEWmsgType(msgType))
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.showMessagesPanel(msgType, msgText);
                this.refreshMainPanel();
                break;
            case "GO_B_PTS_PICKING.TI_CONFIRM_QTY":
                this.setFocusOnC1();
                if (this.isEWmsgType(msgType))
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                this.showMessagesPanel(msgType, msgText);
                this.refreshMainPanel();
                break;
            case "GO_B_Zone_Select":
                if (StringUtils.isEmpty(msgText)) {
                    this.setRegionLoaded(false);
                    ADFUtils.invokeAction("CZoneSelect");
                } else {
                    showNoPicksFwdPopup(msgText);
                }
                break;
            default:
                break;
            }
        }
    }

    private void showNoPicksFwdPopup(String msg) {
        this.hideMessagesPanel();
        this.removeErrorOnActiveField();
        this.disableActiveField();
        getNoPicksFwdOutput().setValue(msg);
        getNoPicksFwdPopup().show(new RichPopup.PopupHints());
    }

    private void processBPtsCloseCntrBlock(String goTo, String msgType, String msgText) {
        switch (goTo) {
        case "GO_B_PTS_PICKING.ZONE_ID":
            this.setFocusOnZone();
            if (this.isEWmsgType(msgType))
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.showMessagesPanel(msgType, msgText);
            break;
        case "GO_B_PTS_PICKING.TI_CONTAINER_ID":
            this.setFocusOnCid2();
            if (this.isEWmsgType(msgType))
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.showMessagesPanel(msgType, msgText);
            break;
        case "GO_B_PTS_PICKING.TI_DEST_CID":
            this.setFocusOnDestCId();
            if (this.isEWmsgType(msgType))
                this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
            this.showMessagesPanel(msgType, msgText);
            break;
        case "GO_B_Zone_Select":
            this.setRegionLoaded(false);
            ADFUtils.invokeAction("CZoneSelect");
            break;
        default:
            break;
        }
    }

    protected void processGoBlock(BigDecimal goBlock) {
        if (null != goBlock && (BigDecimal.ONE.equals(goBlock))) {
            ADFUtils.invokeAction("CPtsQty");
            this.setRegionLoaded(false);
        }
    }

    protected void showConfrimPopupCheckContainerEmpty(List resultValues) {
        this.removeErrorOnActiveField();
        this.disableActiveField();
        String msg = (String) resultValues.get(1);
        if ("LPN_EMPTY".equals(msg)) {
            this.getEmptyCidPoupOutput().setValue(this.getMessage(msg, "C"));
        } else {
            this.getEmptyCidPoupOutput().setValue(msg);
        }
        this.getEmptyCidPopup().setLauncherVar((String) resultValues.get(2));
        this.getEmptyCidPopup().show(new RichPopup.PopupHints());
    }

    protected void showLpnEmptyPopup(String msgType, String msgCode, String popupName) {
        this.hideMessagesPanel();
        this.removeErrorOnActiveField();
        this.disableActiveField();
        if ("LPN_EMPTY".equals(msgCode)) {
            this.getEmptyLpnPoupOutput().setValue(this.getMessage(msgCode, msgType));
        } else {
            this.getEmptyLpnPoupOutput().setValue(msgCode);
        }
        this.getEmptyLpnPopup().setLauncherVar(popupName);
        this.getEmptyLpnPopup().show(new RichPopup.PopupHints());
    }

    protected void showMsgOrNavigate(List resultValues) {
        String msgType = (String) resultValues.get(0);
        String msgCode = (String) resultValues.get(1);
        String msgText = this.getMessage("NO_PICKS_ZN_CONT".equals(msgCode) ? "NO_PICKS_ZN" : msgCode, msgType);
        String goTo = (String) resultValues.get(2);
        if ("PREPACK".equalsIgnoreCase(msgCode) || "NESTED".equalsIgnoreCase(msgCode)) {
            msgText = callMsgWindow(msgCode);
            if ("NESTED".equalsIgnoreCase(msgCode)) {
                msgText += " " + ADFUtils.getBoundAttributeValue("TiInnerPackQty").toString();
            } else {
                msgText += " " + ADFUtils.getBoundAttributeValue("InnerQtyPack").toString();
            }
            this.setInnerPackQtyLabel((String) ADFUtils.getBoundAttributeValue("innerPackQtyLabel"));
        }
        if (this.isPtsPickingBlock())
            this.processBPtsPickingBlock(goTo, msgType, msgCode, msgText);
        else
            this.processBPtsCloseCntrBlock(goTo, msgType, msgText);
    }


    public void clearRowSBPtsPicking() {
        ADFUtils.findIterator("SgPtsPickingSBPtsPickingViewIterator").getCurrentRow().refresh(Row.REFRESH_UNDO_CHANGES |
                                                                                              Row.REFRESH_WITH_DB_FORGET_CHANGES);
    }


    public void commonBuiltInBehaviour() {
        ADFUtils.setBoundAttributeValue("TiZone", ADFUtils.getBoundAttributeValue("TiZone1"));
        ADFUtils.setBoundAttributeValue("TiContainerId", ADFUtils.getBoundAttributeValue("TiContainerId1"));
        ADFUtils.setBoundAttributeValue("TiDispositionCode", ADFUtils.getBoundAttributeValue("TiDispositionCode1"));
        ADFUtils.setBoundAttributeValue("TiItemId", ADFUtils.getBoundAttributeValue("TiItemId1"));

    }

    public RichInputText getActiveFocusedField() {
        String focusOn = this.getSgPtsPickingSBean().getFocusOn();
        if (focusOn != null) {
            switch (focusOn) {
            case SG_ZONE:
                return this.getTiZone();
            case SG_CID2:
                return this.getTiCid();
            case SG_SL:
                return this.getTiD1();
            case SG_CONFIRM_GRAB:
                return this.getTiC1();
            case SG_CLS_CRTN:
                return this.getClo();
            default:
                return null;
            }
        } else {
            return null;
        }
    }

    public RichIcon getActiveFocusedIcon() {
        String focusOn = this.getFocusOn();
        if (focusOn != null) {
            switch (focusOn) {
            case SG_ZONE:
                return this.getZoneIcon();
            case SG_CID2:
                return this.getTiCid2Icon();
            case SG_SL:
                return this.getTiDestCidIcon();
            case SG_CONFIRM_GRAB:
                return this.getTiConfirmQtyIcon();
            case SG_CLS_CRTN:
                return this.getCloseCidIcon();
            default:
                return null;
            }
        } else {
            return null;
        }

    }

    protected void removeErrorOnActiveField() {
        this.setErrorStyleOnComponent(this.getActiveFocusedField(), this.getActiveFocusedIcon(), false);
    }

    protected void disableActiveField() {
        if (this.getActiveFocusedField() != null) {
            this.getActiveFocusedField().setDisabled(true);
            this.refreshContentOfUIComponent(this.getActiveFocusedField());
        }
    }

    protected void enableActiveField() {
        if (this.getActiveFocusedField() != null) {
            this.getActiveFocusedField().setDisabled(false);
            this.setFocusOnUIComponent(getActiveFocusedField());
        }
    }

    protected void setFocusOnC1() {
        if (this.isPtsPickingBlock())
            this.setFocusOnField("SG_CONFIRM_GRAB", this.getTiC1());
        else
            this.setFocusOn("SG_CONFIRM_GRAB");
    }

    protected void setFocusOnCid2() {
        if (this.isPtsPickingBlock())
            this.setFocusOnField("SG_CID2", this.getTiCid());
        else
            this.setFocusOn("SG_CID2");
    }

    protected void setFocusOnDestCId() {
        if (this.isPtsPickingBlock())
            this.setFocusOnField("SG_SL", this.getTiD1());
        else
            this.setFocusOn("SG_SL");
    }

    protected void setFocusOnZone() {
        if (this.isPtsPickingBlock())
            this.setFocusOnField("SG_ZONE", this.getTiZone());
        else
            this.setFocusOn("SG_ZONE");
    }

    protected void resetIp() {
        if (!(this.nvl((String) ADFUtils.getBoundAttributeValue("TiContainerId"),
                       "~")).equalsIgnoreCase(this.nvl((String) ADFUtils.getBoundAttributeValue("TiContainerIdSave"),
                                                       "~~"))) {
            ADFUtils.setBoundAttributeValue("TiContainerIdSave", null);
            ADFUtils.setBoundAttributeValue("innerPackQtyLabel", "SG_IP");
        }
    }

    protected boolean isEWmsgType(String msgType) {
        return "E".equalsIgnoreCase(msgType) || "W".equalsIgnoreCase(msgType);
    }

    protected <T> T nvl(T arg0, T arg1) {
        return (arg0 == null) ? arg1 : arg0;
    }

    protected void getAndShowMessagesPanel(String messageType, String msgCode) {
        String msg = this.getMessage(msgCode, messageType);
        this.showMessagesPanel(messageType, msg);
    }

    protected String getMessage(String msgCode, String messageType) {
        return this.getMessage(msgCode, messageType, null, this.getBoundAttribute(ATTR_LANGUAGE_CODE));
    }

    public void showMessagesPanel(String messageType, String msg) {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.getAllMessagesPanel().setVisible(false);
        if (WARN.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(WRN_ICON);
        } else if (INFO.equals(messageType) || MSGTYPE_M.equals(messageType)) {
            this.getErrorWarnInfoIcon().setName(INF_ICON);
        } else {
            if (ERROR.equals(messageType)) {
                this.getErrorWarnInfoIcon().setName(ERR_ICON);
            } else {
                if (SUCCESS.equals(messageType)) {
                    this.getErrorWarnInfoIcon().setName(SCC_ICON);
                }
            }
        }
        this.getErrorWarnInfoMessage().setValue(msg);
        this.getAllMessagesPanel().setVisible(true);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public void hideMessagesPanel() {
        this.getErrorWarnInfoMessage().setValue(null);
        this.getErrorWarnInfoIcon().setName(null);
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }

    public String getBoundAttribute(String attrName) {
        Object val = ADFUtils.getBoundAttributeValue(attrName);
        if (val != null)
            return (String) val;
        return null;
    }


    public void setErrorStyleOnComponent(RichInputText uiComponent, RichIcon icon, boolean set) {
        if (uiComponent != null && icon != null) {
            if (set) {
                this.addErrorStyleToComponent(uiComponent);
                icon.setName(ERR_ICON);

            } else {
                icon.setName(REQ_ICON);
                this.removeErrorStyleToComponent(uiComponent);
            }
            this.refreshContentOfUIComponent(icon);
            this.refreshContentOfUIComponent(uiComponent);
        }
    }

    public boolean processMessages(List<String> errors) {
        boolean hasErrors = false;
        if (errors != null && !errors.isEmpty()) {
            this.showMessagesPanel(errors.get(0), errors.get(1));
            hasErrors = true;
        } else {
            this.hideMessagesPanel();
        }
        return hasErrors;
    }


    protected void navigateToPage1() {
        this.setRegionLoaded(false);
        this.setCurrentBlockName(B_PTS_PICKING);
        ADFUtils.invokeAction("Page1");
    }

    protected void setFocusOnField(String focusVar, RichInputText focuOnInputText) {
        if (this.getActiveFocusedField() != null) {
            this.getActiveFocusedField().setDisabled(true);
            this.refreshContentOfUIComponent(this.getActiveFocusedField());
        }
        this.setFocusOn(focusVar);
        if (focuOnInputText != null) {
            focuOnInputText.setDisabled(false);
            this.refreshContentOfUIComponent(focuOnInputText);
            this.setFocusOnUIComponent(focuOnInputText);
        }
    }


    public SgPtsPickingSBean getSgPtsPickingSBean() {
        return (SgPtsPickingSBean) this.getPageFlowBean(SG_PTS_PICKING_PAGE_FLOW_BEAN);
    }

    protected void removeErrorOfCloseCid() {
        this.removeErrorOfField(this.getClo(), this.getCloseCidIcon());
    }

    protected void setErrorOnCloseCid() {
        this.setErrorOnField(this.getClo(), this.getCloseCidIcon());
    }

    protected void disableCloseCid() {
        this.getClo().setDisabled(true);
        this.refreshContentOfUIComponent(this.getClo());
    }

    public void setCurrentBlockName(String block) {
        this.getSgPtsPickingSBean().setCurrentBlock(block);
    }

    public String getCurrentBlockName() {
        return this.getSgPtsPickingSBean().getCurrentBlock();
    }

    public void setToExecuteOnLoad(String task) {
        this.getSgPtsPickingSBean().setToExecuteOnLoad(task);
    }

    public String getToExecuteOnLoad() {
        return this.getSgPtsPickingSBean().getToExecuteOnLoad();
    }

    public void setFocusOn(String focus) {
        this.getSgPtsPickingSBean().setFocusOn(focus);
    }

    public String getFocusOn() {
        return this.getSgPtsPickingSBean().getFocusOn();
    }

    public void setMsgShowOnLoadPage1(String msg) {
        this.getSgPtsPickingSBean().setMsgShowOnLoad(msg);
    }

    public String getMsgShowOnLoadPage1() {
        return this.getSgPtsPickingSBean().getMsgShowOnLoad();
    }

    public void setGoBlock(String go) {
        this.getSgPtsPickingSBean().setGoBlock(go);
    }

    public String getGoBlock() {
        return this.getSgPtsPickingSBean().getGoBlock();
    }

    public boolean isRegionLoaded() {
        return this.getSgPtsPickingSBean().isRegionLoaded();
    }

    public void setRegionLoaded(boolean set) {
        this.getSgPtsPickingSBean().setRegionLoaded(set);
    }


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setEmptyCidPopup(RichPopup emptyCidPopup) {
        this.emptyCidPopup = emptyCidPopup;
    }

    public RichPopup getEmptyCidPopup() {
        return emptyCidPopup;
    }

    public void setEmptyCidPopupIcon(RichIcon emptyCidPopupIcon) {
        this.emptyCidPopupIcon = emptyCidPopupIcon;
    }

    public RichIcon getEmptyCidPopupIcon() {
        return emptyCidPopupIcon;
    }

    public void setEmptyCidPoupOutput(RichOutputText emptyCidPoupOutput) {
        this.emptyCidPoupOutput = emptyCidPoupOutput;
    }

    public RichOutputText getEmptyCidPoupOutput() {
        return emptyCidPoupOutput;
    }

    public void setValDestExistPopup(RichPopup valDestExistPopup) {
        this.valDestExistPopup = valDestExistPopup;
    }

    public RichPopup getValDestExistPopup() {
        return valDestExistPopup;
    }

    public void setValidDestExistPopupOutput(RichOutputText validDestExistPopupOutput) {
        this.validDestExistPopupOutput = validDestExistPopupOutput;
    }

    public RichOutputText getValidDestExistPopupOutput() {
        return validDestExistPopupOutput;
    }

    public void setEmptyLpnPopup(RichPopup emptyLpnPopup) {
        this.emptyLpnPopup = emptyLpnPopup;
    }

    public RichPopup getEmptyLpnPopup() {
        return emptyLpnPopup;
    }

    public void setNoPicksFwdPopup(RichPopup noPicksFwdPopup) {
        this.noPicksFwdPopup = noPicksFwdPopup;
    }

    public RichPopup getNoPicksFwdPopup() {
        return noPicksFwdPopup;
    }

    public void setNoPicksFwdOutput(RichOutputText noPicksFwdOutput) {
        this.noPicksFwdOutput = noPicksFwdOutput;
    }

    public RichOutputText getNoPicksFwdOutput() {
        return noPicksFwdOutput;
    }

    public String okNoPicksFwdAction() {
        this.setRegionLoaded(false);
        return "CZoneSelect";
    }

    public void setEmptyLpnPopupIcon(RichIcon emptyLpnPopupIcon) {
        this.emptyLpnPopupIcon = emptyLpnPopupIcon;
    }

    public RichIcon getEmptyLpnPopupIcon() {
        return emptyLpnPopupIcon;
    }

    public void setEmptyLpnPoupOutput(RichOutputText emptyLpnPoupOutput) {
        this.emptyLpnPoupOutput = emptyLpnPoupOutput;
    }

    public RichOutputText getEmptyLpnPoupOutput() {
        return emptyLpnPoupOutput;
    }

    public void setTiD1(RichInputText tiD1) {
        this.tiD1 = tiD1;
    }

    public RichInputText getTiD1() {
        return tiD1;
    }

    public void setTiC1(RichInputText tiC1) {
        this.tiC1 = tiC1;
    }

    public RichInputText getTiC1() {
        return tiC1;
    }

    public void setTiZone(RichInputText tiZone) {
        this.tiZone = tiZone;
    }

    public RichInputText getTiZone() {
        return tiZone;
    }

    public void setTiCid(RichInputText tiCid) {
        this.tiCid = tiCid;
    }

    public RichInputText getTiCid() {
        return tiCid;
    }

    public void setZoneIcon(RichIcon zoneIcon) {
        this.zoneIcon = zoneIcon;
    }

    public RichIcon getZoneIcon() {
        return zoneIcon;
    }

    public void setTiCid2Icon(RichIcon tiCid2Icon) {
        this.tiCid2Icon = tiCid2Icon;
    }

    public RichIcon getTiCid2Icon() {
        return tiCid2Icon;
    }

    public void setTiDestCidIcon(RichIcon tiDestCidIcon) {
        this.tiDestCidIcon = tiDestCidIcon;
    }

    public RichIcon getTiDestCidIcon() {
        return tiDestCidIcon;
    }

    public void setTiConfirmQtyIcon(RichIcon tiConfirmQtyIcon) {
        this.tiConfirmQtyIcon = tiConfirmQtyIcon;
    }

    public RichIcon getTiConfirmQtyIcon() {
        return tiConfirmQtyIcon;
    }

    public void setZonePopupIcon(RichIcon zonePopupIcon) {
        this.zonePopupIcon = zonePopupIcon;
    }

    public RichIcon getZonePopupIcon() {
        return zonePopupIcon;
    }

    public void setClo(RichInputText clo) {
        this.clo = clo;
    }

    public RichInputText getClo() {
        return clo;
    }

    public void setCloseCidIcon(RichIcon closeCidIcon) {
        this.closeCidIcon = closeCidIcon;
    }

    public RichIcon getCloseCidIcon() {
        return closeCidIcon;
    }

    public void setInnerPackQtyLabel(String innerPackQtyLabel) {
        this.getSgPtsPickingSBean().setInnerPackQtyLabel(innerPackQtyLabel);
    }

    public String getInnerPackQtyLabel() {
        return this.getSgPtsPickingSBean().getInnerPackQtyLabel();
    }


    /**
     * This method will help us to detect if the value of a field has changed.
     * This way we can execute programmatically a valueChangeEvent ONLY when the value has not changed.
     * Because if we execute it always, it will be executed twice, as the lifecycle will execute it as usual also.
     * @param submittedVal value submitted, usually to a clientListener
     * @param fieldValue value of the binded component, RichInputText for example (it is the old value)
     * @return
     */
    protected boolean valueNotChanged(String submittedVal, Object fieldValue) {
        if (fieldValue instanceof String) {
            return (StringUtils.isEmpty((String) fieldValue) && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase((String) fieldValue);
        } else {
            return (fieldValue == null && StringUtils.isEmpty(submittedVal)) ||
                   submittedVal.equalsIgnoreCase(fieldValue == null ? "" : fieldValue.toString());
        }
    }

    public void setPflMain(RichPanelFormLayout pflMain) {
        this.pflMain = pflMain;
    }

    public RichPanelFormLayout getPflMain() {
        return pflMain;
    }

    private String callMsgWindow(String msgCode) {
        OperationBinding oper = ADFUtils.findOperation("callMsgWindow");
        oper.getParamsMap().put("code", msgCode);
        return (String) oper.execute();
    }

    public void setNoPicksZnPopup(RichPopup noPicksZnPopup) {
        this.noPicksZnPopup = noPicksZnPopup;
    }

    public RichPopup getNoPicksZnPopup() {
        return noPicksZnPopup;
    }

    public void setNoPicksZnOutput(RichOutputText noPicksZnOutput) {
        this.noPicksZnOutput = noPicksZnOutput;
    }

    public RichOutputText getNoPicksZnOutput() {
        return noPicksZnOutput;
    }

    private void showNoPicksZnPopup(String msgText) {
        this.hideMessagesPanel();
        this.removeErrorOnActiveField();
        this.disableActiveField();
        getNoPicksZnOutput().setValue(msgText);
        getNoPicksZnPopup().show(new RichPopup.PopupHints());
    }

    public String okNoPicksZnAction() {
        getNoPicksZnPopup().hide();
        ADFUtils.setBoundAttributeValue("TiZone", ADFUtils.getBoundAttributeValue("FwdZone"));
        ADFUtils.setBoundAttributeValue("TiZone1", ADFUtils.getBoundAttributeValue("TiZone"));
        ADFUtils.setBoundAttributeValue("TiContainerId", ADFUtils.getBoundAttributeValue("TiContainerId1"));
        this.setFocusOnCid2();
        this.setRegionLoaded(false);
        this.setToExecuteOnLoad("p_validate_cntr");
        return null;
    }

    protected void refreshMainPanel() {
        if (getPflMain() != null) {
            refreshContentOfUIComponent(getPflMain());
        }
    }

    protected void endOfPConfirmQty() {
        if ("Y".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("TiCheckSuccess"))) {
            BigDecimal bworkUnitQty = (BigDecimal) ADFUtils.getBoundAttributeValue("TiUnitQty");
            BigDecimal tiConfirmQty = (BigDecimal) ADFUtils.getBoundAttributeValue("TiConfirmQty");
            ADFUtils.setBoundAttributeValue("TiUnitQty",
                                            tiConfirmQty != null ? bworkUnitQty.subtract(tiConfirmQty) : null);
            bworkUnitQty = (BigDecimal) ADFUtils.getBoundAttributeValue("TiUnitQty");
            if ((BigDecimal.ZERO).equals(bworkUnitQty))
                this.callpCheckEmptyContainer();
            else if ("Y".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("TiQtyShorted")))
                this.processYesQtyShorted();
            else
                this.processNoQtyShorted();
        }
        this.processWorkLaborProd();
    }

    protected void callpCheckEmptyContainer() {
        OperationBinding oper = ADFUtils.findOperation("callPcheckContainerEmpty");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                int size = resultValues.size();
                if (size == 3) {
                    if ("C".equalsIgnoreCase((String) resultValues.get(0)))
                        this.showConfrimPopupCheckContainerEmpty(resultValues);
                    else
                        this.showMsgOrNavigate(resultValues);
                } else if (size == 2) {
                    this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                    this.processMessages(resultValues);
                } else if (size == 1) {
                    Object returnVal = resultValues.get(0);
                    if (returnVal instanceof BigDecimal)
                        this.processBuiltIns((BigDecimal) resultValues.get(0));
                    else if (returnVal instanceof String)
                        if ("GO_B_PTS_PICKING.TI_CONTAINER_ID".equalsIgnoreCase((String) returnVal)) {
                            this.resetIp();
                            this.setFocusOnCid2();
                        }
                }
            }
        }
    }

    private void processYesQtyShorted() {
        ADFUtils.setBoundAttributeValue("TiQtyShorted", "N");
        ADFUtils.setBoundAttributeValue("TiZone", ADFUtils.getBoundAttributeValue("TiZone1"));
        this.setFocusOnCid2();
    }

    private void processNoQtyShorted() {
        this.clearRowSBPtsPicking();
        this.commonBuiltInBehaviour();
        this.callPgetPick();
    }

    public boolean callcallCheckCntrContent() {
        OperationBinding oper = ADFUtils.findOperation("callCheckCntrContent");
        oper.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
        oper.getParamsMap().put("closeCid", ADFUtils.getBoundAttributeValue("CloseCid"));
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            return ("Y".equalsIgnoreCase((String) oper.getResult()));
        return false;
    }

    public void showEmptyCntrPopup(String msg) {
        this.removeErrorOnActiveField();
        this.hideMessagesPanel();
        this.disableActiveField();
        this.getEmptyCntrPopupOutput().setValue(msg);
        this.getEmptyCntrPopup().show(new RichPopup.PopupHints());
    }

    public void setEmptyCntrPopup(RichPopup emptyCntrPopup) {
        this.emptyCntrPopup = emptyCntrPopup;
    }

    public RichPopup getEmptyCntrPopup() {
        return emptyCntrPopup;
    }

    public void setEmptyCntrPopupOutput(RichOutputText emptyCntrPopupOutput) {
        this.emptyCntrPopupOutput = emptyCntrPopupOutput;
    }

    public RichOutputText getEmptyCntrPopupOutput() {
        return emptyCntrPopupOutput;
    }

    protected boolean callCloseCntr(String userChoice) {
        OperationBinding oper = ADFUtils.findOperation("callCloseContainer");
        oper.getParamsMap().put("userChoice", userChoice);
        oper.getParamsMap().put("closeCid", ADFUtils.getBoundAttributeValue("CloseCid"));
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                List codesList = (List) oper.getResult();
                if (codesList != null) {
                    int sizeLisrCodes = codesList.size();
                    if (sizeLisrCodes == 2) {
                        Object built = codesList.get(0);
                        if (built instanceof BigDecimal) {
                            if (new BigDecimal(2).equals(built))
                                this.goToPtsQty();
                            else if (new BigDecimal(1).equals(built))
                                this.goToPtsPicking();
                        } else {
                            this.setErrorOnField(this.getActiveFocusedField(), this.getActiveFocusedIcon());
                            return !this.processMessages(codesList);
                        }
                    } else {
                        this.removeErrorOnActiveField();
                        return true;
                    }
                } else
                    this.callNextProcessCloseCId3();
            }
        }
        return false;
    }

    private void goToPtsQty() {
        this.setGoBlock("B_PTS_QTY");
        this.setFocusOn("SG_GRABS_LEFT");
        this.setCurrentBlockName("B_PTS_QTY");
        this.setRegionLoaded(false);
        ADFUtils.invokeAction("CPtsQty");
    }

    private void goToPtsPicking() {
        this.setGoBlock(B_PTS_PICKING);
        this.setFocusOnCid2();
        this.setCurrentBlockName(B_PTS_PICKING);
        this.setRegionLoaded(false);
        ADFUtils.invokeAction("Page1");
    }

    protected void callNextProcessCloseCId3() {
        ADFUtils.setBoundAttributeValue("TiDestCid", null);
        ADFUtils.setBoundAttributeValue("CloseCid", null);
        BigDecimal tUnit = this.callCgetLpnQtyLeft();
        if (tUnit != null && !BigDecimal.ZERO.equals(tUnit)) {
            this.callPgetPick();
        }
        if (BigDecimal.ZERO.equals(ADFUtils.getBoundAttributeValue("TiGrabsReq"))) {
            String zoneId = (String) ADFUtils.getBoundAttributeValue("TiZone");
            this.clearRowSBPtsPicking();
            ADFUtils.setBoundAttributeValue("TiZone", zoneId);
            this.setFocusOnZone();
            if (!isPtsPickingBlock()) {
                navigateToPage1();
            }
        }
        ADFUtils.setBoundAttributeValue("TiClosePressed", null);
    }

    protected BigDecimal callCgetLpnQtyLeft() {
        OperationBinding oper = ADFUtils.findOperation("callCgetLpnQtyLeft");
        oper.getParamsMap().put("facilityId", ADFUtils.getBoundAttributeValue("FacilityId"));
        oper.getParamsMap().put("containerId", ADFUtils.getBoundAttributeValue("TiContainerId"));
        oper.getParamsMap().put("itemId", ADFUtils.getBoundAttributeValue("TiItemId"));
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            return (BigDecimal) oper.getResult();
        return null;

    }

    public void setNavigatePtsLocHiddenLink(RichLink navigatePtsLocHiddenLink) {
        this.navigatePtsLocHiddenLink = navigatePtsLocHiddenLink;
    }

    public RichLink getNavigatePtsLocHiddenLink() {
        return navigatePtsLocHiddenLink;
    }

    public boolean callCheckCloseCid(String newCloseCid) {
        OperationBinding oper = ADFUtils.findOperation("callCheckCloseCid");
        oper.getParamsMap().put("closeCid", newCloseCid);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List codesList = (List) oper.getResult();
            if (codesList != null) {
                int sizeLisrCodes = codesList.size();
                if (sizeLisrCodes == 2) {
                    this.setErrorOnCloseCid();
                    return !this.processMessages(codesList);
                } else {
                    this.removeErrorOfCloseCid();
                    return true;
                }
            }
        }
        return true;
    }

    protected void processCloseAfterCheckQty() {
        this.processWorkLaborProd();
        if (!this.callcallCheckCntrContent()) {
            this.showEmptyCntrPopup(this.getMessage("SG_EMPTY_CONT", "E"));
        } else {
            //callCheckCloseCid only to insert correctly into ROSS_EVENTCODE_USERID_TMP in the same request of the DB changes
            this.callCheckCloseCid((String) ADFUtils.getBoundAttributeValue("CloseCid"));
            this.callCloseCntr("N");
        }
    }

    protected String getMessageByCode(String msgCode) {
        return this.getMessage(msgCode, null, null, null);
    }

    public void setNotAllowed(boolean notAllowed) {
        this.notAllowed = notAllowed;
    }

    public boolean isNotAllowed() {
        return notAllowed;
    }
}
