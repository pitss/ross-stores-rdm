package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;

import java.io.Serializable;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class SgPtsPickingSBean implements Serializable {
    @SuppressWarnings("compatibility:-3250572951876463685")
    private static final long serialVersionUID = -4397346618589355092L;
    private static ADFLogger _logger = ADFLogger.createADFLogger(SgPtsPickingSBean.class);
    private String focusOn;
    private boolean regionLoaded;
    private String toExecuteOnLoad = null;
    private String goBlock = null;
    private String msgShowOnLoad = null;
    private String currentBlock = null;
    private String innerPackQtyLabel = "";
    private Boolean executedKey = Boolean.FALSE;
    private boolean notAllowed = false;


    public void initTaskFlow() {
        this.callInitTaskFlow();
        this.setFocusOn("SG_ZONE");
        this.setRegionLoaded(false);
        this.setCurrentBlock("B_PTS_PICKING");
        ADFUtils.setBoundAttributeValue("innerPackQtyLabel", "SG_IP");
    }

    public void callInitTaskFlow() {
        ADFUtils.findOperation("initTaskFlowSgPtsPicking").execute();
    }

    public void setFocusOn(String focusOn) {
        this.focusOn = focusOn;
    }

    public String getFocusOn() {
        return focusOn;
    }

    public void setRegionLoaded(boolean regionLoaded) {
        this.regionLoaded = regionLoaded;
    }

    public boolean isRegionLoaded() {
        return regionLoaded;
    }

    public void setToExecuteOnLoad(String toExecuteOnLoad) {
        this.toExecuteOnLoad = toExecuteOnLoad;
    }

    public String getToExecuteOnLoad() {
        return toExecuteOnLoad;
    }

    public void setGoBlock(String goBlock) {
        this.goBlock = goBlock;
    }

    public String getGoBlock() {
        return goBlock;
    }

    public void setMsgShowOnLoad(String msgShowOnLoad) {
        this.msgShowOnLoad = msgShowOnLoad;
    }

    public String getMsgShowOnLoad() {
        return msgShowOnLoad;
    }


    public void setCurrentBlock(String currentBlock) {
        this.currentBlock = currentBlock;
    }

    public String getCurrentBlock() {
        return currentBlock;
    }

    public void setInnerPackQtyLabel(String innerPackQtyLabel) {
        if (innerPackQtyLabel != null) {
            this.innerPackQtyLabel =
                (String) JSFUtils.resolveExpression("#{resourceLabelBean.attributeLabel('" + innerPackQtyLabel + "')}");
        }
    }

    public String getInnerPackQtyLabel() {
        return innerPackQtyLabel;
    }

    public void setExecutedKey(Boolean executedKey) {
        this.executedKey = executedKey;
    }

    public Boolean getExecutedKey() {
        return executedKey;
    }

    public void setNotAllowed(boolean notAllowed) {
        this.notAllowed = notAllowed;
    }

    public boolean isNotAllowed() {
        return notAllowed;
    }
}
