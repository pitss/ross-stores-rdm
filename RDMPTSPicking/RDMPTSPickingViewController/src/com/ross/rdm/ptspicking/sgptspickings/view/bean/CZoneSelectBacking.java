package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.utils.view.util.JSFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.ptspicking.sgptspickings.model.views.ZoneSelectionRow;

import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.event.ViewMapListener;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for CZoneSelect.jspx
// ---
// ---------------------------------------------------------------------
public class CZoneSelectBacking extends PtsPickingBaseBacking {
    private RichLink f2Link;
    private RichLink f3Link;
    private RichLink f7Link;
    private RichLink f8Link;
    private RichLink executeEnterHiddenLink;
    private static ADFLogger _logger = ADFLogger.createADFLogger(CZoneSelectBacking.class);
    private static final int VISIBLE_ROWS = 7;

    public CZoneSelectBacking() {

    }

    public void onChangedZone(ValueChangeEvent vce) {
        if (StringUtils.isNotEmpty((String) vce.getNewValue()))
            this.callKeyNextItem();
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!this.getPageFlowBean().isNotAllowed()) {
            if (!this.isRegionLoaded()) {
                if (this.isNonConvZone())
                    this.executeZonesQuery("executeNonConZoneQuery");
                else
                    this.executeZonesQuery("executeConZoneQuery");
                this.setRegionLoaded(true);
            }
        }
    }
    
    private SgPtsPickingSBean getPageFlowBean() {
           return (SgPtsPickingSBean) this.getPageFlowBean("SgPtsPickingSBean");
    }

    public boolean isNonConvZone() {
        return "NON_CONV".equalsIgnoreCase((String) ADFUtils.getBoundAttributeValue("nonConvOrConv"));
    }

    public void executeZonesQuery(String operationNam) {
        OperationBinding oper = ADFUtils.findOperation(operationNam);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            this.getZoneSelectionCollectionBean().setZoneList((List<ZoneSelectionRow>) oper.getResult());
            this.setCurrentRowInTable();
        }
    }

    private void callKeyNextItem() {
        new ActionEvent(getExecuteEnterHiddenLink()).queue();
    }

    private ZoneSelectionRow getSelectRowData() {
        return (ZoneSelectionRow) this.getZoneSelectionCollectionBean().getZoneTable().getSelectedRowData();
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                this.callKeyNextItem();
            } else if (F2_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF2Link()).queue();
            } else if (F3_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF3Link()).queue();
            } else if (F7_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF7Link()).queue();
            } else if (F8_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF8Link()).queue();
            } else if (UP_KEY_CODE.equals(keyPressed)) {
                setSelectedIndexInTable(getSelectedIndexInTable() - 1);
            } else if (DOWN_KEY_CODE.equals(keyPressed)) {
                setSelectedIndexInTable(getSelectedIndexInTable() + 1);
            }
        }
    }

    private String getRenderedTableZoneInputId() {
        if (this.isNonConvZone())
            return "it4";
        return "it1";
    }

    private ZoneSelectionCollectionSBean getZoneSelectionCollectionBean() {
        return (ZoneSelectionCollectionSBean) this.getPageFlowBean("ZoneSelectionCollectionSBean");
    }

    public String f2Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_ZONE_SELECT_F2,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
            this.setRegionLoaded(false);
            ADFUtils.setBoundAttributeValue("ChZone", null);
            return "CChZone";
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            this.getPageFlowBean().setNotAllowed(true);
        }
        return null;
    }

    public String f3Action() {
        if (ADFUtils.getBoundAttributeValue("TiDestId") == null) {
            this.setToExecuteOnLoad("p_validate_cntr");
        } else {
            this.setFocusOn("SG_SL");
        }
        this.setRegionLoaded(false);
        this.setCurrentBlockName("B_PTS_PICKING");
        return "Page1";
    }

    public String f7Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_ZONE_SELECT_F7,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
            setSelectedIndexInTable(getSelectedIndexInTable() - VISIBLE_ROWS);
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            this.getPageFlowBean().setNotAllowed(true);
        }
        return null;
    }

    public String f8Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_ZONE_SELECT_F8,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
            setSelectedIndexInTable(getSelectedIndexInTable() + VISIBLE_ROWS);
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
            this.getPageFlowBean().setNotAllowed(true);
        }

        return null;
    }

    public void setF2Link(RichLink f2Link) {
        this.f2Link = f2Link;
    }

    public RichLink getF2Link() {
        return f2Link;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF7Link(RichLink f7Link) {
        this.f7Link = f7Link;
    }

    public RichLink getF7Link() {
        return f7Link;
    }

    public void setF8Link(RichLink f8Link) {
        this.f8Link = f8Link;
    }

    public RichLink getF8Link() {
        return f8Link;
    }

    public void setExecuteEnterHiddenLink(RichLink executeEnterHiddenLink) {
        this.executeEnterHiddenLink = executeEnterHiddenLink;
    }

    public RichLink getExecuteEnterHiddenLink() {
        return executeEnterHiddenLink;
    }

    public String executeEnterHiddenAction() {
        String navigation = null;
        ZoneSelectionRow zoneSelectionData = this.getSelectRowData();
        if (zoneSelectionData != null && zoneSelectionData.getZone() != null) {
            OperationBinding oper = ADFUtils.findOperation("callTKeyNextItem6");
            Map map = oper.getParamsMap();
            map.put("zone", zoneSelectionData.getZone());
            map.put("pick", zoneSelectionData.getPipFlag());
            oper.execute();
            if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
                List resultValues = (List) oper.getResult();
                if (resultValues != null && resultValues.size() == 2) {
                    this.processMessages(resultValues);
                } else {
                    this.setToExecuteOnLoad("p_validate_cntr");
                    this.setRegionLoaded(false);
                    navigation = "Page1";
                }
            }
        }
        return navigation;
    }

    private void setCurrentRowInTable() {
        String currentZone = (String) ADFUtils.getBoundAttributeValue("TiZone");

        String zoneFromChZone = (String) ADFContext.getCurrent().getPageFlowScope().get(SET_ZONE_IN_TABLE);
        if (zoneFromChZone != null) {
            currentZone = zoneFromChZone;
            ADFContext.getCurrent().getPageFlowScope().put(SET_ZONE_IN_TABLE, null);
        }

        List<ZoneSelectionRow> list = getZoneSelectionCollectionBean().getZoneList();
        if (list != null && list.size() > 0) {
            int foundInPos = -1;
            for (int i = 0; i < list.size() && foundInPos == -1; i++) {
                ZoneSelectionRow zoneRow = list.get(i);
                if (currentZone != null && currentZone.equals(zoneRow.getZone())) {
                    foundInPos = i;
                }
            }

            setSelectedIndexInTable(foundInPos != -1 ? foundInPos : 0);
        }
    }

    private int getSelectedIndexInTable() {
        int selectedIndex = -1;
        List<ZoneSelectionRow> list = getZoneSelectionCollectionBean().getZoneList();
        RichTable table = getZoneSelectionCollectionBean().getZoneTable();
        if (list != null && list.size() > 0) {
            RowKeySet keySet = table.getSelectedRowKeys();
            if (keySet != null) {
                Object[] array = keySet.toArray();
                if (array != null && array.length > 0) {
                    Object sel = array[0];
                    if (sel != null) {
                        selectedIndex = Integer.parseInt(sel.toString());
                    }
                }
            }
        }
        return selectedIndex;
    }

    private void setSelectedIndexInTable(int newIndex) {
        List<ZoneSelectionRow> list = getZoneSelectionCollectionBean().getZoneList();
        RichTable table = getZoneSelectionCollectionBean().getZoneTable();
        if (table != null && list != null && list.size() > 0) {
            if (newIndex >= list.size()) {
                newIndex = list.size() - 1;
            } else if (newIndex < 0) {
                newIndex = 0;
            }
            RowKeySetImpl rks = new RowKeySetImpl();
            rks.add(newIndex);
            table.setSelectedRowKeys(rks);
            table.setActiveRowKey(newIndex);
            table.setDisplayRowKey(newIndex);
            //setFocusOnNextZoneInput();
            refreshContentOfUIComponent(table);
        }
    }
}
