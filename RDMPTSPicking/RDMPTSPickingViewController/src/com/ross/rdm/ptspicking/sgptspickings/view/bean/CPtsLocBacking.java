package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for CPtsLoc.jspx
// ---
// ---------------------------------------------------------------------
public class CPtsLocBacking extends PtsPickingBaseBacking {
    private RichLink f3Link;
    private RichLink f4Link;
    private static ADFLogger _logger = ADFLogger.createADFLogger(CPtsLocBacking.class);
    private RichIcon locIdIcon;
    private RichInputText locId;

    public CPtsLocBacking() {

    }

    private String callpValidateLocId(String locationId) {
        OperationBinding oper = ADFUtils.findOperation("callpValidateLocId");
        oper.getParamsMap().put("locationId", locationId);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                int resultSize = resultValues.size();
                if (resultSize == 2 && !"GO_ITEM".equals(resultValues.get(0))) {
                    this.showMessagesPanel("E", (String) resultValues.get(1));
                    this.setErrorOnLocId();
                } else if (resultSize == 2) {
                    String goTo = (String) resultValues.get(1);
                    if ("B_pts_picking.TI_dest_cid".equalsIgnoreCase(goTo)) {
                        String ptsConfirm = (String) ADFUtils.getBoundAttributeValue("PtsConfirm");
                        String tiNoConfirmClose = (String) ADFUtils.getBoundAttributeValue("TiNoConfirmClose");
                        if ("N".equals(ptsConfirm) && StringUtils.isEmpty(tiNoConfirmClose)) {
                            setToExecuteOnLoad("p_confirm_qty");
                        } else if ("N".equals(ptsConfirm) && "F".equals(tiNoConfirmClose)) {
                            //pts_confirm IS N and TI_no_confirm_close IS NOT NULL
                            //Set flag to 'S' for second time through
                            ADFUtils.setBoundAttributeValue("TiNoConfirmClose", "S");
                            setToExecuteOnLoad("TR_KEY_IN_F4");
                        }
                        return f3Action();
                    }
                }
            }
        }
        return null;
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!this.isRegionLoaded()) {
            this.setFocusOn("SG_LOCATION_ID");
            this.setFocusOnUIComponent(this.getLocId());
            this.setRegionLoaded(true);
        }
    }

    private void setErrorOnLocId() {
        this.setErrorOnField(this.getLocId(), this.getLocIdIcon());
    }

    public String f3Action() {
        this.setRegionLoaded(false);
        this.setFocusOnDestCId();
        this.setCurrentBlockName("B_PTS_PICKING");
        return "Page1";
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public String f4Action() {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_picking_s_B_PTS_LOC_F4,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_picking_s)) {
            return this.callpValidateLocId((String) ADFUtils.getBoundAttributeValue("TiLocationId"));
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
        return null;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setLocIdIcon(RichIcon locIdIcon) {
        this.locIdIcon = locIdIcon;
    }

    public RichIcon getLocIdIcon() {
        return locIdIcon;
    }

    public void setLocId(RichInputText locId) {
        this.locId = locId;
    }

    public RichInputText getLocId() {
        return locId;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF3Link()).queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                new ActionEvent(getF4Link()).queue();
            }
        }
    }
}
