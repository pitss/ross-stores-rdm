package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for CChZone.jspx
// ---
// ---------------------------------------------------------------------
public class CChZoneBacking extends PtsPickingBaseBacking {

    private RichLink f3Link;
    private static ADFLogger _logger = ADFLogger.createADFLogger(CChZoneBacking.class);
    private RichInputText chZone;
    private RichIcon chZoneIcon;

    public CChZoneBacking() {

    }

    public void onChangedChZone(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            this.updateModel(vce);
            String newZone = (String) vce.getNewValue();
            if (StringUtils.isNotEmpty(newZone))
                this.processZone(newZone.toUpperCase(), this.callVZone(newZone.toUpperCase()));
        }
    }

    private String callVZone(String zone) {
        OperationBinding oper = ADFUtils.findOperation("callVZone");
        oper.getParamsMap().put("zoneId", zone);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            return (String) oper.getResult();
        return null;
    }

    private void processZone(String newZone, String validZone) {
        if ("N".equalsIgnoreCase(validZone)) {
            this.setErrorOnField(this.getChZone(), this.getChZoneIcon());
            this.getAndShowMessagesPanel("E", "SG_INV_ZONE");
        } else {
            this.setRegionLoaded(false);
            //this.callPopulateZoneSelect(ValidZone);
            ADFContext.getCurrent().getPageFlowScope().put(SET_ZONE_IN_TABLE, newZone);
            ADFUtils.invokeAction("CZoneSelect");
        }
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))
                if (StringUtils.isNotEmpty(submittedValue)){
                    this.onChangedChZone(new ValueChangeEvent(this.getChZone(), null, submittedValue));
                }
                else{
                    this.setErrorOnField(this.getChZone(), this.getChZoneIcon());
                    this.getAndShowMessagesPanel("E", "SG_INV_ZONE");
                }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!this.isRegionLoaded()) {
            this.setFocusOn("STARTING_ZONE");
            this.refreshContentOfUIComponent(this.getChZone());
            this.setFocusOnUIComponent(this.getChZone());
            this.setRegionLoaded(true);
        }
    }

    public String f3Action() {
        this.setRegionLoaded(false);
        return "CZoneSelect";
    }


    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setChZone(RichInputText chZone) {
        this.chZone = chZone;
    }

    public RichInputText getChZone() {
        return chZone;
    }

    public void setChZoneIcon(RichIcon chZoneIcon) {
        this.chZoneIcon = chZoneIcon;
    }

    public RichIcon getChZoneIcon() {
        return chZoneIcon;
    }
    
    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_ptspicking_sgptspickings_view_pageDefs_CChZonePageDef")) {
            navigation = true;
        }
        return navigation;
    }
}
