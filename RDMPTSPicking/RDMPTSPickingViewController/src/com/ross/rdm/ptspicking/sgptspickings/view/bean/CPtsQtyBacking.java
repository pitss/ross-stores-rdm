package com.ross.rdm.ptspicking.sgptspickings.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for CPtsQty.jspx
// ---
// ---------------------------------------------------------------------
public class CPtsQtyBacking extends PtsPickingBaseBacking {

    private static final String NEXT_ACTION_PTS_QTY = "nextActionPtsQty";
    private static final String IS_NAVIGAT_AUTH_FORM = "isNavigatToAuthForm";
    private RichLink f3Link;
    private RichLink goPage1HiddenLink;
    private static ADFLogger _logger = ADFLogger.createADFLogger(CPtsQtyBacking.class);
    private RichInputText grabsLeft;
    private RichIcon grabLeftIcon;
    private RichOutputText approvalPopupOutput;
    private RichOutputText warningPopupOutput;
    private RichPopup approvalPopup;
    private RichPopup warningPopup;

    public CPtsQtyBacking() {

    }

    public void onChangedTiNewGrabQty(ValueChangeEvent vce) {
        if (!isNavigationExecuted()) {
            updateModel(vce);
            String newGrabLeft = (String) vce.getNewValue();
            if (StringUtils.isNotEmpty(newGrabLeft)) {
                if (NumberUtils.isNumber(newGrabLeft)) {
                    this.removeErrorOnGrabLeft();
                    this.hideMessagesPanel();
                    this.processNewGrabLeft(new BigDecimal(newGrabLeft));
                } else {
                    this.setErrorOnGrabLeft();
                    this.getAndShowMessagesPanel("E", "SG_INV_QTY");
                }
            }
        }
    }

    private void processNewGrabLeft(BigDecimal grabLeft) {
        ADFUtils.setBoundAttributeValue("TiNewGrabQty", grabLeft);
        if (grabLeft != null) {
            if (grabLeft.compareTo(BigDecimal.ZERO) < 0) {
                this.processInvalidGrabbLeft();
                return;
            } else if (!grabLeft.setScale(0, RoundingMode.HALF_UP).equals(grabLeft)) {
                this.processInvalidGrabbLeft();
                return;
            } else if (grabLeft.compareTo(BigDecimal.ZERO) == 0) {
                this.processGrabLeftEqual0();
                return;
            }
            this.callProcessGrabLeft();
        }
    }

    private String processBuiltInsGrabLeft(BigDecimal builtIn, boolean invokeAction) {
        this.setRegionLoaded(false);
        String nav = null;
        if ((new BigDecimal(1)).equals(builtIn)) {
            this.clearRowSBPtsPicking();
            this.commonBuiltInBehaviour();
            this.setToExecuteOnLoad("p_get_pick");
            return this.navigateToPage1(invokeAction);
        } else if ((new BigDecimal(2)).equals(builtIn))
            return this.processCommonBuiltIns2And3(invokeAction);
        else if ((new BigDecimal(3)).equals(builtIn)) {
            this.resetIp();
            return this.processCommonBuiltIns2And3(invokeAction);
        }
        return nav;
    }

    private String processCommonBuiltIns2And3(boolean invokeAction) {
        this.clearRowSBPtsPicking();
        ADFUtils.setBoundAttributeValue("TiZone", ADFUtils.getBoundAttributeValue("TiZone1"));
        ADFUtils.setBoundAttributeValue("TiAlreadyCheckFlag", "N");
        this.setFocusOnCid2();
        return this.navigateToPage1(invokeAction);
    }

    private void processGrabLeftEqual0() {
        this.processInvalidGrabbLeft();
        this.setFocusOnCid2();
        this.setToExecuteOnLoad("p_check_container_empty");
        this.navigateToPage1();
    }

    private void processInvalidGrabbLeft() {
        ADFUtils.setBoundAttributeValue("TiNewGrabQty", null);
        this.refreshContentOfUIComponent(this.getGrabsLeft());
    }

    private void processApprovalActions(String confirm, String launcherVar) {
        getApprovalPopup().hide();
        if ("Y".equals(confirm)) {
            if ("FIRST".equalsIgnoreCase(launcherVar))
                this.callExecSuppApproval("Y");
            else if ("SECOND".equalsIgnoreCase(launcherVar))
                this.callExecOverageApproval("Y", "NOT APPROVED");
            else if ("THIRD".equalsIgnoreCase(launcherVar) || "FOURTH".equalsIgnoreCase(launcherVar))
                this.callExecOverageApproval("Y", "NOT APPROVED");
        } else {
            if ("FIRST".equalsIgnoreCase(launcherVar))
                this.callExecSuppApproval("N");
            else if ("SECOND".equalsIgnoreCase(launcherVar))
                this.setFocusOnGrabLeft();
            else if ("THIRD".equalsIgnoreCase(launcherVar) || "FOURTH".equalsIgnoreCase(launcherVar))
                this.callExecOverageApproval("N", null);
        }
    }

    private void callProcessGrabLeft() {
        OperationBinding oper = ADFUtils.findOperation("processGrabLeft");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                int resultSize = resultValues.size();
                if (resultSize == 1) {
                    Object returnedVal = resultValues.get(0);
                    if (returnedVal instanceof BigDecimal)
                        this.processBuiltInsGrabLeft((BigDecimal) returnedVal, true);
                } else if (resultSize == 2) {
                    this.showMessagesPanel((String) resultValues.get(0), (String) resultValues.get(1));
                    this.setErrorOnGrabLeft();
                } else if (resultSize == 3) {
                    this.hideMessagesPanel();
                    this.showApprovalPopup(this.getMessageByCode((String) resultValues.get(0)),
                                           (String) resultValues.get(2));
                }
            }
        }
    }

    private void callExecOverageApproval(String confirm, String approval) {
        OperationBinding oper = ADFUtils.findOperation("execOverageApproval");
        oper.getParamsMap().put("userChoice", confirm);
        oper.getParamsMap().put("approval", approval);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                int resultSize = resultValues.size();
                if (resultSize == 1) {
                    Object returnedVal = resultValues.get(0);
                    if ("GO_B_PTS_PICKING.TI_CONTAINER_ID".equalsIgnoreCase((String) returnedVal)) {
                        this.setFocusOnCid2();
                        this.navigateToPage1();
                    } else if ("GO_B_PTS_QTY.TI_NEW_GRAB_QTY".equalsIgnoreCase((String) returnedVal))
                        this.setFocusOnGrabLeft();

                }
            }
        }
    }

    private void callExecSuppApproval(String confirm) {
        OperationBinding oper = ADFUtils.findOperation("execSuppApproval");
        oper.getParamsMap().put("userChoice", confirm);
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty()) {
            List resultValues = (List) oper.getResult();
            if (resultValues != null) {
                int resultSize = resultValues.size();
                if (resultSize == 1) {
                    Object returnedVal = resultValues.get(0);
                    if ("CALL_ROSS_RF_AUTH_S".equalsIgnoreCase((String) returnedVal)) {
                        this.setRegionLoaded(false);
                        ADFUtils.invokeAction("RossRfAuthSTaskFlow");
                    }
                } else if (resultSize == 3) {
                    this.hideMessagesPanel();
                    this.showApprovalPopup(this.getMessageByCode((String) resultValues.get(1)),
                                           (String) resultValues.get(2));
                }
            }
        }
    }

    private List callContinueExecSuppApproval() {
        List resultValues = null;
        OperationBinding oper = ADFUtils.findOperation("continueExcSuppApproval");
        oper.execute();
        if (oper.getErrors() == null || oper.getErrors().isEmpty())
            resultValues = (List) oper.getResult();
        return resultValues;
    }

    private void showApprovalPopup(String msg, String order) {
        this.removeErrorOnGrabLeft();
        this.disableGabLeftField();
        this.getApprovalPopup().setLauncherVar(order);
        this.getApprovalPopupOutput().setValue(msg);
        this.getApprovalPopup().show(new RichPopup.PopupHints());
    }

    public String yesApprovalAction() {
        this.processApprovalActions("Y", this.getApprovalPopup().getLauncherVar());
        return null;
    }

    public String noApprovalAction() {
        this.processApprovalActions("N", this.getApprovalPopup().getLauncherVar());
        return null;
    }

    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
            RichInputText field = (RichInputText) clientEvent.getComponent();
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (StringUtils.isNotEmpty(submittedValue) && valueNotChanged(submittedValue, field.getValue()))
                    this.onChangedTiNewGrabQty(new ValueChangeEvent(this.getGrabsLeft(), null, submittedValue));
            }
        }
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        if (!this.isRegionLoaded()) {
            this.setRegionLoaded(true);
            if (Boolean.TRUE.equals(ADFUtils.getBoundAttributeValue(IS_NAVIGAT_AUTH_FORM))) {
                ADFUtils.setBoundAttributeValue(IS_NAVIGAT_AUTH_FORM, Boolean.FALSE);
                List nextActionPtsQty = (List) ADFContext.getCurrent().getPageFlowScope().get(NEXT_ACTION_PTS_QTY);
                ADFContext.getCurrent().getPageFlowScope().put(NEXT_ACTION_PTS_QTY, null);
                if (nextActionPtsQty != null && !nextActionPtsQty.isEmpty()) {
                    int resultSize = nextActionPtsQty.size();
                    if (resultSize == 2) {
                        this.showMessagesPanel((String) nextActionPtsQty.get(0), (String) nextActionPtsQty.get(1));
                        this.setErrorOnGrabLeft();
                    } else if (resultSize == 5) {
                        showWarningPopup(getMessageByCode((String) nextActionPtsQty.get(1)),
                                         (String) nextActionPtsQty.get(4));
                    }
                }
            } else {
                this.setFocusOnGrabLeft();
            }
        }
    }

    private void setFocusOnGrabLeft() {
        this.setFocusOn("SG_GRABS_LEFT");
        this.getGrabsLeft().setDisabled(false);
        this.setFocusOnUIComponent(this.getGrabsLeft());
    }

    private void disableGabLeftField() {
        this.getGrabsLeft().setDisabled(true);
        this.refreshContentOfUIComponent(this.getGrabsLeft());
    }

    private void setErrorOnGrabLeft() {
        this.setErrorOnField(this.getGrabsLeft(), this.getGrabLeftIcon());
    }

    private void removeErrorOnGrabLeft() {
        this.removeErrorOfField(this.getGrabsLeft(), getGrabLeftIcon());
    }

    public String f3Action() {
        ADFUtils.setBoundAttributeValue("TiNewGrabQtyString", null);
        this.setFocusOnCid2();
        this.setRegionLoaded(false);
        this.setToExecuteOnLoad("p_check_container_empty");
        return "Page1";
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setGrabsLeft(RichInputText grabsLeft) {
        this.grabsLeft = grabsLeft;
    }

    public RichInputText getGrabsLeft() {
        return grabsLeft;
    }

    public void setGrabLeftIcon(RichIcon grabLeftIcon) {
        this.grabLeftIcon = grabLeftIcon;
    }

    public RichIcon getGrabLeftIcon() {
        return grabLeftIcon;
    }

    public void setApprovalPopupOutput(RichOutputText approvalPopupOutput) {
        this.approvalPopupOutput = approvalPopupOutput;
    }

    public RichOutputText getApprovalPopupOutput() {
        return approvalPopupOutput;
    }

    public void setApprovalPopup(RichPopup approvalPopup) {
        this.approvalPopup = approvalPopup;
    }

    public RichPopup getApprovalPopup() {
        return approvalPopup;
    }

    private boolean isNavigationExecuted() {
        boolean navigation = false;
        BindingContext bindingContext = BindingContext.getCurrent();
        String currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
        if (currentPageDef != null &&
            !currentPageDef.startsWith("com_ross_rdm_ptspicking_sgptspickings_view_pageDefs_CPtsQtyPageDef")) {
            navigation = true;
        }
        return navigation;
    }

    public void setWarningPopup(RichPopup warningPopup) {
        this.warningPopup = warningPopup;
    }

    public RichPopup getWarningPopup() {
        return warningPopup;
    }

    public void setWarningPopupOutput(RichOutputText warningPopupOutput) {
        this.warningPopupOutput = warningPopupOutput;
    }

    public RichOutputText getWarningPopupOutput() {
        return warningPopupOutput;
    }

    private void showWarningPopup(String msg, String order) {
        this.removeErrorOnGrabLeft();
        this.disableGabLeftField();
        this.getWarningPopup().setLauncherVar(order);
        this.getWarningPopupOutput().setValue(msg);
        this.getWarningPopup().show(new RichPopup.PopupHints());
    }

    public String okWarningAction() {
        this.getWarningPopup().hide();
        showApprovalPopup(getMessageByCode("OVERAGE_APPRVL"), getWarningPopup().getLauncherVar());
        return null;
    }

    private String navigateToPage1(boolean invokeAction) {
        this.setRegionLoaded(false);
        this.setCurrentBlockName(B_PTS_PICKING);
        if (invokeAction) {
            //ADFUtils.invokeAction("Page1");
            new ActionEvent(getGoPage1HiddenLink()).queue();
        }
        return "Page1";
    }

    public String continueExcSuppApproval() {
        restoreGlobalValues();
        List result = callContinueExecSuppApproval();
        String nav = null;
        if (result != null && !result.isEmpty()) {
            if (result.size() == 1) {
                if (result.get(0) instanceof BigDecimal) {
                    nav = processBuiltInsGrabLeft((BigDecimal) result.get(0), false);
                    ADFUtils.setBoundAttributeValue(IS_NAVIGAT_AUTH_FORM, Boolean.FALSE);
                }
            } else {
                ADFContext.getCurrent().getPageFlowScope().put(NEXT_ACTION_PTS_QTY, result);
                setRegionLoaded(false);
                nav = "CPtsQty";
            }
        }
        return nav;
    }

    public void setGoPage1HiddenLink(RichLink goPage1HiddenLink) {
        this.goPage1HiddenLink = goPage1HiddenLink;
    }

    public RichLink getGoPage1HiddenLink() {
        return goPage1HiddenLink;
    }

    private void restoreGlobalValues() {
        ADFUtils.setBoundAttributeValue("GlobalAuthOk", ADFContext.getCurrent().getPageFlowScope().get("globalAuthOk"));
        ADFUtils.setBoundAttributeValue("GlobalUserPrivLevel",
                                        ADFContext.getCurrent().getPageFlowScope().get("globalUserPrivLevel"));
        ADFUtils.setBoundAttributeValue("GlobalAuthUserId",
                                        ADFContext.getCurrent().getPageFlowScope().get("globalAuthUserId"));
    }
}
