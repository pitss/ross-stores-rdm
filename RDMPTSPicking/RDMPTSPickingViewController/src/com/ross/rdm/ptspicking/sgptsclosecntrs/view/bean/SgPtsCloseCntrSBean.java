package com.ross.rdm.ptspicking.sgptsclosecntrs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class SgPtsCloseCntrSBean {
    private static ADFLogger _logger = ADFLogger.createADFLogger(SgPtsCloseCntrSBean.class);
    
    private Boolean isTiContainerIdValidated;
    private String isFocusOn;
    private Boolean isError;
    
    public void initTaskFlow() {        
        initTaskFlowContainerReInduct();
        setIsFocusOn("TiContainerId");
        setIsTiContainerIdValidated(false);
    }
    
    private void initTaskFlowContainerReInduct() {
        ADFUtils.findOperation("initTaskFlowPtsCloseCntr").execute();
    }

    public void setIsTiContainerIdValidated(Boolean isTiContainerIdValidated) {
        this.isTiContainerIdValidated = isTiContainerIdValidated;
    }

    public Boolean getIsTiContainerIdValidated() {
        return isTiContainerIdValidated;
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return isError;
    }
}
