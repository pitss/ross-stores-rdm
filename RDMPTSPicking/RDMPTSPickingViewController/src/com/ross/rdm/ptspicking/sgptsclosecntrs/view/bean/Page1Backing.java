package com.ross.rdm.ptspicking.sgptsclosecntrs.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.ptspicking.sgptsclosecntrs.model.views.SgPtsCloseCntrSBPtsCloseViewRowImpl;
import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMPTSPickingBackingBean {

    private final static String B_PTS_CLOSE_ITERATOR = "SgPtsCloseCntrSBPtsCloseViewIterator";
    private final static String SG_PTS_CLOSE_CNTR_FLOW_BEAN = "SgPtsCloseCntrSBean";
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";

    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);

    private RichPanelFormLayout ptsCloseCntrPanel;
    private RichLink f3Link;
    private RichLink f4Link;

    //Inputs & Outputs
    private RichInputText tiContainerId;
    private RichIcon tiContainerIdIcon;
    private RichInputText tiLocationId;
    private RichInputText tiDestId;

    public Page1Backing() {

    }

    public void onChangedTiContainerId(ValueChangeEvent vce) {
        boolean validFromContainerId = false;

        if (null != vce.getNewValue() && !((String) vce.getNewValue()).isEmpty()) {
            String enteredValue = ((String) vce.getNewValue());
            validFromContainerId = callPValContainer(enteredValue);
            if (validFromContainerId) {
                ADFUtils.setBoundAttributeValue("TiContainerId", enteredValue);
                hideMessagesPanel();
                this.removeErrorOfField(getTiContainerId(), getTiContainerIdIcon());
            }
        } else {
            setErrorOnField(getTiContainerId(), getTiContainerIdIcon());
            showMessagesPanel("E", this.getMessage("SG_INV_CNTR", "E", null, this.getLangCodeAttrValue()));
            setFocusOnField(getTiContainerId());
        }
        getSgPtsCloseCntrSPageFlowBean().setIsTiContainerIdValidated(validFromContainerId);
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = (String) clientEvent.getParameters().get("submittedValue");

        if (clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if (submittedValue != null) {
                    onChangedTiContainerId(new ValueChangeEvent(getTiContainerId(), null, submittedValue));
                }
            }
            this.refreshContentOfUIComponent(getPtsCloseCntrPanel());
        }
    }

    private void trKeyIn(String key) {
        if ("F3".equalsIgnoreCase(key)) {
            callPSaveLaborProd();
            clearFields(true);
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F4".equalsIgnoreCase(key)) {
            if (getTiContainerId().getValue() != null) {
                if (getTiLocationId().getValue() != null || getTiDestId().getValue() != null) {
                    boolean passedPClosePick = callPClosePick((String) getTiContainerId().getValue());
                    if (passedPClosePick) {
                        clearFields(true);
                    }
                    setFocusOnField(getTiContainerId());
                } else {
                    boolean validFromContainerId = callPValContainer((String) getTiContainerId().getValue());
                    if (validFromContainerId) {
                        ADFUtils.setBoundAttributeValue("TiContainerId", getTiContainerId().getValue());
                        hideMessagesPanel();
                        this.removeErrorOfField(getTiContainerId(), getTiContainerIdIcon());
                    }
                }
            } else {
                setErrorOnField(getTiContainerId(), getTiContainerIdIcon());
                showMessagesPanel("E", this.getMessage("SG_CNTR_REQ", "E", null, this.getLangCodeAttrValue()));
                clearFields(true);
                selectTextOnUIComponent(getTiContainerId());
            }
        }
    }

    private Boolean callPSaveLaborProd() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPSaveLaborProd");

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() >= 2) {
                    if ("E".equals(responseList.get(0))) {
                        showMessagesPanel(responseList.get(0), responseList.get(1));
                        setErrorOnField(getTiContainerId(), getTiContainerIdIcon());
                        return Boolean.FALSE;
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    private Boolean callPValContainer(String tiContainerId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPValContainer");
        oper.getParamsMap().put("pTiContainerId", tiContainerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() >= 2) {
                    if ("E".equals(responseList.get(0))) {
                        showMessagesPanel(responseList.get(0), responseList.get(1));
                        setErrorOnField(getTiContainerId(), getTiContainerIdIcon());
                        clearFields(false);
                        return Boolean.FALSE;
                    }
                }
            }
        }
        return Boolean.TRUE;
    }


    private Boolean callPClosePick(String tiContainerId) {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPClosePick");
        oper.getParamsMap().put("pTiContainerId", tiContainerId);

        oper.execute();
        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<String> responseList = (List<String>) oper.getResult();
                if (responseList != null && responseList.size() >= 2) {
                    if ("E".equals(responseList.get(0)) || "W".equals(responseList.get(0))) {
                        showMessagesPanel("E", responseList.get(1));
                        setErrorOnField(getTiContainerId(), getTiContainerIdIcon());
                        return Boolean.FALSE;
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    private void clearFields(boolean clearAll) {
        _logger.info("clearFields() Start");
        if (clearAll) {
            getTiContainerId().resetValue();
            refreshContentOfUIComponent(this.getTiContainerId());
        }
        clearCurrentRow(clearAll);
        getTiLocationId().resetValue();
        getTiDestId().resetValue();
        refreshContentOfUIComponent(this.getTiLocationId());
        refreshContentOfUIComponent(this.getTiDestId());
        _logger.info("clearFields() End");
    }

    private void clearCurrentRow(boolean clearAll) {
        SgPtsCloseCntrSBPtsCloseViewRowImpl sgRowImpl = this.getSgPtsCloseCntrSBPtsCloseViewRow();
        if (sgRowImpl != null) {
            if (clearAll) {
                sgRowImpl.setTiContainerId(null);
            }
            sgRowImpl.setTiLocationId(null);
            sgRowImpl.setTiDestId(null);
        }
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_close_cntr_s_B_PTS_CLOSE_F4,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_close_cntr_s)) {
            this.trKeyIn("F4");
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
    }

    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private SgPtsCloseCntrSBPtsCloseViewRowImpl getSgPtsCloseCntrSBPtsCloseViewRow() {
        return (SgPtsCloseCntrSBPtsCloseViewRowImpl) ADFUtils.findIterator(B_PTS_CLOSE_ITERATOR).getCurrentRow();
    }

    private SgPtsCloseCntrSBean getSgPtsCloseCntrSPageFlowBean() {
        return ((SgPtsCloseCntrSBean) this.getPageFlowBean(SG_PTS_CLOSE_CNTR_FLOW_BEAN));
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setPtsCloseCntrPanel(RichPanelFormLayout ptsCloseCntrPanel) {
        this.ptsCloseCntrPanel = ptsCloseCntrPanel;
    }

    public RichPanelFormLayout getPtsCloseCntrPanel() {
        return ptsCloseCntrPanel;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setTiContainerId(RichInputText tiContainerId) {
        this.tiContainerId = tiContainerId;
    }

    public RichInputText getTiContainerId() {
        return tiContainerId;
    }

    public void setTiContainerIdIcon(RichIcon tiContainerIdIcon) {
        this.tiContainerIdIcon = tiContainerIdIcon;
    }

    public RichIcon getTiContainerIdIcon() {
        return tiContainerIdIcon;
    }

    public void setTiLocationId(RichInputText tiLocationId) {
        this.tiLocationId = tiLocationId;
    }

    public RichInputText getTiLocationId() {
        return tiLocationId;
    }

    public void setTiDestId(RichInputText tiDestId) {
        this.tiDestId = tiDestId;
    }

    public RichInputText getTiDestId() {
        return tiDestId;
    }
}
