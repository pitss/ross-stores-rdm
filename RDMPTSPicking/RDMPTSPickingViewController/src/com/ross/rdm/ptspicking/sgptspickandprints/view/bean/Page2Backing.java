package com.ross.rdm.ptspicking.sgptspickandprints.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.commons.lang.math.NumberUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page2.jspx
// ---
// ---------------------------------------------------------------------
public class Page2Backing extends RDMPTSPickingBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page2Backing.class);
    private RichLink f3Link;
    private RichLink f4Link;
    private RichInputText nbrOfLpns;
    private RichInputText maxGrabs;
    private RichIcon nbrOfLpnsIcon;
    private RichIcon maxGrabsIcon;

    public String NBR_OF_LPNS = "NbrOfLpns";
    public String MAX_GRABS = "MaxGrabs";
    
    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    
    private final static String PAGE_FLOW_BEAN = "SgPtsPickAndPrintSBean";
    private RichPanelFormLayout panelForm;
    private RichPopup acknowledgementPopup;
    private RichOutputText acknowledgementOutputText;
    private RichLink acknowledgementPopUpLink;
    
    private RichPopup okayPopup;
    private RichLink okayLink;
    private RichIcon okayIcon;
    private RichOutputText okayText;

    public Page2Backing() {

    }
    
    public void onChangedNbrOfLpnsNull(ValueChangeEvent vce){
        this.updateModel(vce);
        if(null == vce.getNewValue()){
            ADFUtils.setBoundAttributeValue("NbrOfLpns", null);
        }
        else{
            ADFUtils.setBoundAttributeValue("NbrOfLpns", vce.getNewValue());
        }
    }
    
    public void onChangedMaxGrabsNull(ValueChangeEvent vce){
        this.updateModel(vce);
        
        
        if(null == vce.getNewValue()){
            ADFUtils.setBoundAttributeValue("MaxGrabs", null);
        }
       else {
            ADFUtils.setBoundAttributeValue("MaxGrabs", vce.getNewValue());
        }
    }

    public void onChangedNbrOfLpns(ValueChangeEvent vce) {
        _logger.info("onChangedNbrOfLpns() Start");
        this.updateModel(vce);
        if(null == vce.getNewValue() || vce.getNewValue().toString().isEmpty()){
            ADFUtils.setBoundAttributeValue("NbrOfLpns", null);
        }
        else if (null != vce.getNewValue() && vce.getOldValue() != vce.getNewValue()) {
            String errorMsg = null;
            String newValue = (String) vce.getNewValue();
            if(!checkIfNumeric(newValue)){
                showMustBeNumericMsg();
                this.setErrorUi(this.getNbrOfLpnsIcon(), this.getNbrOfLpns());
                this.getSgPtsPickAndPrintSPageFlowBean().setNbrOfLpnsValidated(false);
                return;
            };
            BigDecimal nbrOfLpns = new BigDecimal(newValue);
            try {
                Integer numLpns = nbrOfLpns.intValueExact();
                Integer contQty = ((BigDecimal) ADFUtils.getBoundAttributeValue("ContQty")).intValueExact();
                if (numLpns == 0) {
                    errorMsg = "CANNOT_ZERO";
                } else if (numLpns < 0) {
                    errorMsg = "INV_NUM_NEG";
                } else if (numLpns > contQty) {
                    errorMsg = "LPN_NBR_EXCEED";
                }
            } catch (Exception ex) {
                errorMsg = "WHOLE_NUM";
            }

            if (null != errorMsg) {
                this.getSgPtsPickAndPrintSPageFlowBean().setNbrOfLpnsValidated(false);
                this.setErrorUi(this.getNbrOfLpnsIcon(), this.getNbrOfLpns());
                showErrorPanel(this.getMessage(errorMsg, "E", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                               (String) ADFUtils.getBoundAttributeValue("LanguageCode")), "E");
                _logger.info("onChangedNbrOfLpns() End");
                return;
            } else {
                this.getSgPtsPickAndPrintSPageFlowBean().setNbrOfLpnsValidated(true);
                this.getNbrOfLpnsIcon().setName("required");
                ADFUtils.setBoundAttributeValue("NbrOfLpns", nbrOfLpns);
                this.refreshContentOfUIComponent(this.getNbrOfLpnsIcon());
                this.hideMessagesPanel();
                this.removeErrorOfAllFields();
                this.refreshContentOfUIComponent(this.getPanelForm());
                _logger.info("onChangedNbrOfLpns() End");
            }
        }
    }
    
    private boolean checkIfNumeric(String nbr) {
            return NumberUtils.isNumber(nbr);
        }
    
    private void showMustBeNumericMsg() {
            this.showErrorPanel(this.getMessage("MUST_BE_NUMERIC", "E", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                        (String) ADFUtils.getBoundAttributeValue("LanguageCode")), "E");
        }

    public void onChangedMaxGrabs(ValueChangeEvent vce) {
        _logger.info("onChangedMaxGrabs() Start");
        this.updateModel(vce);
        if(null == vce.getNewValue() || vce.getNewValue().toString().isEmpty()) {
            ADFUtils.setBoundAttributeValue("MaxGrabs", null);
        }
        else if (null != vce.getNewValue() && vce.getNewValue() != vce.getOldValue()) {
            String newValue = (String) vce.getNewValue();
            if(!checkIfNumeric(newValue)){
                showMustBeNumericMsg();
                this.setErrorUi(this.getMaxGrabsIcon(), this.getMaxGrabs());
                this.getSgPtsPickAndPrintSPageFlowBean().setMaxGrabsValidated(false);
                return;
            };
            BigDecimal newMaxGrabs = new BigDecimal(newValue);
            String errorMsg = null;
            try {
                Integer maxGrabs = newMaxGrabs.intValueExact();
                Integer unitQtyWl = ((BigDecimal) ADFUtils.getBoundAttributeValue("UnitQtyWl")).intValueExact();
                if (maxGrabs == 0) {
                    errorMsg = "CANNOT_ZERO";
                } else if (maxGrabs < 0) {
                    errorMsg = "NEG_UNIT_QTY";
                } else if (maxGrabs > unitQtyWl) {
                    errorMsg = "EXCEEDED_MIN";
                }
            } catch (Exception ex) {
                errorMsg = "WHOLE_NUM";
            }

            if (null != errorMsg) {
                this.getSgPtsPickAndPrintSPageFlowBean().setMaxGrabsValidated(false);
                this.setErrorUi(this.getMaxGrabsIcon(), this.getMaxGrabs());
                showErrorPanel(this.getMessage(errorMsg, "E", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                               (String) ADFUtils.getBoundAttributeValue("LanguageCode")), "E");
                _logger.info("onChangedMaxGrabs() End");
                return;
            } else {
                this.getSgPtsPickAndPrintSPageFlowBean().setMaxGrabsValidated(true);
                ADFUtils.setBoundAttributeValue("MaxGrabs", newMaxGrabs);
                ADFUtils.setBoundAttributeValue("MaxGrabs1", newMaxGrabs);
                this.getNbrOfLpnsIcon().setName("required");
                this.refreshContentOfUIComponent(this.getNbrOfLpnsIcon());
                this.hideMessagesPanel();
                this.removeErrorOfAllFields();
                this.refreshContentOfUIComponent(this.getPanelForm());
                _logger.info("onChangedMaxGrabs() End");
            }
        }
    }


    /******************************************************************************************
     *                                  TrKeyIn and performKey
     ******************************************************************************************/


    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        if ("f3".equalsIgnoreCase(key)) {
            f3PickPrint();
            _logger.info("trKeyIn() End");
        } else if ("F4".equalsIgnoreCase(key)) {
            String currentFocus = this.getSgPtsPickAndPrintSPageFlowBean().getIsFocusOn();

            if ("NbrOfLpnsField".equals(currentFocus)) {
                if (null != getNbrOfLpnsValue()) {
                    this.onChangedNbrOfLpns(new ValueChangeEvent(this.getNbrOfLpns(), null,
                                                                 getNbrOfLpnsValue().toString()));
                    if (this.getSgPtsPickAndPrintSPageFlowBean().getNbrOfLpnsValidated() &&
                        !this.getSgPtsPickAndPrintSPageFlowBean().getMaxGrabsValidated()) {
                        this.setFocusMaxGrabs();
                    } else if (this.getSgPtsPickAndPrintSPageFlowBean().getNbrOfLpnsValidated() &&
                               this.getSgPtsPickAndPrintSPageFlowBean().getMaxGrabsValidated()) {
                        if (!ADFUtils.getBoundAttributeValue("NbrOfLpns").equals(ADFUtils.getBoundAttributeValue("ContQty"))) {
                            //Call_Msg_Window_P ('INC_LPN_COUNT','E');

                            this.getOkayText().setValue(this.getMessage("INC_LPN_COUNT", "E", null, this.getLangCodeAttrValue()));
                            openOkayPopup();
                            return;
                        }

                        else {
                            this.getSgPtsPickAndPrintSPageFlowBean().pickAndPrint();
                        }
                    }
                    this.refreshContentOfUIComponent(this.getAllMessagesPanel());
                }
            }
            if ("MaxGrabsField".equals(currentFocus)) {
                if (null != getMaxGrabsValue()) {
                    this.onChangedMaxGrabs(new ValueChangeEvent(this.getNbrOfLpns(), null,
                                                                getMaxGrabsValue().toString()));
                    if (this.getSgPtsPickAndPrintSPageFlowBean().getMaxGrabsValidated() &&
                        !this.getSgPtsPickAndPrintSPageFlowBean().getNbrOfLpnsValidated()) {
                        this.setFocusNbrOfLpns();
                    } else if (this.getSgPtsPickAndPrintSPageFlowBean().getMaxGrabsValidated() &&
                               this.getSgPtsPickAndPrintSPageFlowBean().getNbrOfLpnsValidated()) {
                        if (!ADFUtils.getBoundAttributeValue("NbrOfLpns").equals(ADFUtils.getBoundAttributeValue("ContQty"))) {
                            //Call_Msg_Window_P ('INC_LPN_COUNT','E');
                            this.getOkayText().setValue(this.getMessage("INC_LPN_COUNT", "E", null, this.getLangCodeAttrValue()));
                            openOkayPopup();
                            return;
                        }

                        else {
                            this.getSgPtsPickAndPrintSPageFlowBean().pickAndPrint();
                        }
                    }
                }
            }
        }
    }

    

    public void createLpnDataGroup() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("executeLpnDataGroup");
        Map map = oper.getParamsMap();
        map.put("facilityId", (String) ADFUtils.getBoundAttributeValue("FacilityId1"));
        map.put("itemId", (String) ADFUtils.getBoundAttributeValue("ItemId1"));
        map.put("pickFromContainerId", (String) ADFUtils.getBoundAttributeValue("PickFromContainerId"));
        map.put("user", (String) ADFUtils.getBoundAttributeValue("User"));
        oper.execute();

        DCIteratorBinding iterBind = ADFUtils.findIterator("LpnDataGroupViewIterator");
        long iterSize = iterBind.getEstimatedRowCount();
        if (iterSize == 0) {
            showErrorPanel(this.getMessage("PROCESS_CONT", "E", (String) ADFUtils.getBoundAttributeValue("FacilityId"),
                                           (String) ADFUtils.getBoundAttributeValue("LanguageCode")), "E");
            clearPickAndPrint(true);
            return;
        }
        ADFUtils.setBoundAttributeValue("NbrOfLpns1", 0);
        ADFUtils.invokeAction("Page3");
        return;
    }

    public void f3PickPrint() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callLockUnlockPicks");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() > 1) {
                    String errorMsg = (String) codeList.get(1);
                    if (null != codeList.get(2) && !((String) codeList.get(2)).isEmpty()) {
                        errorMsg = errorMsg + " " + (String) codeList.get(2);
                    }
                    this.showErrorPanel(errorMsg, (String) codeList.get(0));
                }
            }
        }

        clearPickAndPrint(true);
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue =null;
        if (clientEvent !=null)
            submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getSgPtsPickAndPrintSPageFlowBean().getIsFocusOn();
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {

                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();

            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("NbrOfLpnsField".equals(currentFocus)) {
                    if (null != submittedValue && !submittedValue.isEmpty()) {
                        this.onChangedNbrOfLpns(new ValueChangeEvent(this.getNbrOfLpns(), null, submittedValue));
                        this.refreshContentOfUIComponent(this.getPanelForm());
                        if (this.getSgPtsPickAndPrintSPageFlowBean().getNbrOfLpnsValidated()) {
                            this.setFocusMaxGrabs();
                        }
                    }
                } else if ("MaxGrabsField".equals(currentFocus)) {
                    if (null != submittedValue && !submittedValue.isEmpty()) {
                        this.onChangedMaxGrabs(new ValueChangeEvent(this.getMaxGrabs(), null, submittedValue));
                        this.refreshContentOfUIComponent(this.getPanelForm());
                        if (this.getSgPtsPickAndPrintSPageFlowBean().getMaxGrabsValidated())
                            this.setFocusNbrOfLpns();
                    }

                }
            }
        }
    }

    public void performKeyAcknowledgementPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getAcknowledgementPopUpLink());
                actionEvent.queue();
            }
        }
    }

    public void yesLinkAcknowledgementPopupActionListener(ActionEvent actionEvent) {
        this.getSgPtsPickAndPrintSPageFlowBean().getAcknowledgementPopup().hide();
        boolean unlock = "error".equals(this.getSgPtsPickAndPrintSPageFlowBean().getAcknowledgementPopupIcon());
        if (unlock) {
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callLockUnlockPicks");
            oper.execute();

            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    List<Object> codeList = (List<Object>) oper.getResult();
                    if (codeList.size() == 2) {
                        this.showErrorPanel((String) codeList.get(1), (String) codeList.get(0));
                        return;
                    }
                    clearPickAndPrint(true);
                }
            }
        }else{
            clearPickAndPrint(false);
        }
    }

    public void lockUnlockPicks() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callLockUnlockPicks");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 1) {
                    String errorMsg = (String) codeList.get(0);
                    String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
                    String langCode = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
                    String msgText = this.getMessage(errorMsg, "E", facilityId, langCode);
                    this.getAcknowledgementOutputText().setValue(msgText);
                    this.getAcknowledgementPopup().show(new RichPopup.PopupHints());
                    return;
                }
            }
        }
    }

    public void clearPickAndPrint(boolean unlock) {
        if (unlock) {
            lockUnlockPicks();
        }
        ADFUtils.setBoundAttributeValue("PickFromContainerId", null);
        ADFUtils.setBoundAttributeValue("ItemId1", null);
        ADFUtils.setBoundAttributeValue("ItemId", null);
        ADFUtils.setBoundAttributeValue("UnitQty", null);
        ADFUtils.setBoundAttributeValue("InnerPackQty", null);
        ADFUtils.setBoundAttributeValue("ItemDesc", null);
        ADFUtils.setBoundAttributeValue("UnitPickSystemCode", null);
        ADFUtils.setBoundAttributeValue("InnerPackQty1", null);
        ADFUtils.setBoundAttributeValue("ShippingConveyable", null);
        ADFUtils.setBoundAttributeValue("ContQty", null);
        ADFUtils.setBoundAttributeValue("UnitQtyWl", null);
        ADFUtils.setBoundAttributeValue("RoutePrintOrder", null);
        ADFUtils.setBoundAttributeValue("MaxGrabsWl", null);
        ADFUtils.setBoundAttributeValue("NbrOfLpns", null);
        ADFUtils.setBoundAttributeValue("MaxGrabs", null);
        ADFUtils.setBoundAttributeValue("NbrOfLpnString", null);
        ADFUtils.setBoundAttributeValue("MaxGrabString", null);
        ADFUtils.setBoundAttributeValue("ContainerId1", null);
        ADFUtils.setBoundAttributeValue("ContainerId", null);
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("ContainerIdField");
        ADFUtils.invokeAction("Page1");
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (this.getSgPtsPickAndPrintSPageFlowBean() != null) {
            String focus = this.getSgPtsPickAndPrintSPageFlowBean().getIsFocusOn();
            if ("NbrOfLpnsField".equalsIgnoreCase(focus)) {
                this.setFocusNbrOfLpns();
            } else if ("MaxGrabsField".equalsIgnoreCase(focus)) {
                this.setFocusOnUIComponent(this.getMaxGrabs());
            }
        }
        _logger.info("onRegionLoad End");
    }
    
    public void openOkayPopup(){
        this.removeErrorOfAllFields();
        this.hideMessagesPanel(); 
        this.getOkayPopup().show(new RichPopup.PopupHints());
        setFocusOkayPopup();
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    public void performKeyOkayPopup(ClientEvent clientEvent) {
            if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
                Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
                if (F1_KEY_CODE.equals(keyPressed)) {
                    okayPopupActionListener();
                    }
                }
    }
    
    public void okayPopupActionListener(){
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("LpnField");        
        createLpnDataGroup();
        getOkayPopup().hide();
    }
    
    public void okayPopupActionListener(ActionEvent actionEvent){
       okayPopupActionListener();
    }

    /******************************************************************************************
     *                                  Utility methods
     ******************************************************************************************/

    private void setFocusOkayPopup() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("OkayPopup");
    }
    
    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }
    
    private void setFocusNbrOfLpns() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("NbrOfLpnsField");
        this.setFocusOnUIComponent(this.getNbrOfLpns());
    }

    private void setFocusMaxGrabs() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("MaxGrabsField");
        this.setFocusOnUIComponent(this.getMaxGrabs());
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_pick_print_s_PICK_PRINT_F4,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_pick_print_s)) {
            this.trKeyIn("F4");
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
    }

    private void removeErrorOfAllFields() {
        this.removeErrorStyleToComponent(getNbrOfLpns());
        this.removeErrorStyleToComponent(getMaxGrabs());
        this.getNbrOfLpnsIcon().setName("required");
        this.getMaxGrabsIcon().setName("required");
        this.refreshContentOfUIComponent(getNbrOfLpnsIcon());
        this.refreshContentOfUIComponent(getMaxGrabsIcon());
    }


    /******************************************************************************************
     *                                  Getters and Setters
     ******************************************************************************************/

    private BigDecimal getNbrOfLpnsValue() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue(NBR_OF_LPNS);
    }

    private BigDecimal getMaxGrabsValue() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue(MAX_GRABS);
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setNbrOfLpns(RichInputText nbrOfLpns) {
        this.nbrOfLpns = nbrOfLpns;
    }

    public RichInputText getNbrOfLpns() {
        return nbrOfLpns;
    }

    public void setMaxGrabs(RichInputText maxGrabs) {
        this.maxGrabs = maxGrabs;
    }

    public RichInputText getMaxGrabs() {
        return maxGrabs;
    }

    public void setNbrOfLpnsIcon(RichIcon nbrOfLpnsIcon) {
        this.nbrOfLpnsIcon = nbrOfLpnsIcon;
    }

    public RichIcon getNbrOfLpnsIcon() {
        return nbrOfLpnsIcon;
    }

    public void setMaxGrabsIcon(RichIcon maxGrabsIcon) {
        this.maxGrabsIcon = maxGrabsIcon;
    }

    public RichIcon getMaxGrabsIcon() {
        return maxGrabsIcon;
    }

    private SgPtsPickAndPrintSBean getSgPtsPickAndPrintSPageFlowBean() {
        return ((SgPtsPickAndPrintSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }

    public void setPanelForm(RichPanelFormLayout panelForm) {
        this.panelForm = panelForm;
    }

    public RichPanelFormLayout getPanelForm() {
        return panelForm;
    }

    public void setAcknowledgementPopup(RichPopup acknowledgementPopup) {
        this.acknowledgementPopup = acknowledgementPopup;
    }

    public RichPopup getAcknowledgementPopup() {
        return acknowledgementPopup;
    }

    public void setAcknowledgementOutputText(RichOutputText acknowledgementOutputText) {
        this.acknowledgementOutputText = acknowledgementOutputText;
    }

    public RichOutputText getAcknowledgementOutputText() {
        return acknowledgementOutputText;
    }

    public void setAcknowledgementPopUpLink(RichLink acknowledgementPopUpLink) {
        this.acknowledgementPopUpLink = acknowledgementPopUpLink;
    }

    public RichLink getAcknowledgementPopUpLink() {
        return acknowledgementPopUpLink;
    }

    public void setOkayPopup(RichPopup okayPopup) {
        this.okayPopup = okayPopup;
    }

    public RichPopup getOkayPopup() {
        return okayPopup;
    }

    public void setOkayLink(RichLink okayLink) {
        this.okayLink = okayLink;
    }

    public RichLink getOkayLink() {
        return okayLink;
    }

    public void setOkayIcon(RichIcon okayIcon) {
        this.okayIcon = okayIcon;
    }

    public RichIcon getOkayIcon() {
        return okayIcon;
    }

    public void setOkayText(RichOutputText okayText) {
        this.okayText = okayText;
    }

    public RichOutputText getOkayText() {
        return okayText;
    }
}
