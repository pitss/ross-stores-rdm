package com.ross.rdm.ptspicking.sgptspickandprints.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.util.List;
import java.util.Map;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class SgPtsPickAndPrintSBean extends RDMPTSPickingBackingBean{
    private static ADFLogger _logger = ADFLogger.createADFLogger(SgPtsPickAndPrintSBean.class);
    private String isFocusOn;
    private Boolean nbrOfLpnsValidated;
    private Boolean maxGrabsValidated;
    
    private RichPopup acknowledgementPopup;
    private RichOutputText acknowledgementOutputText;
    private RichLink acknowledgementPopUpLink;
    private RichIcon acknowledgementPopupIcon;
    private RichDialog acknowledgementPopupDialogue;
    
    private final static String PAGE_2_BACKING = "SgPtsPickAndPrintSPage2Backing";
    private final static String PAGE_3_BACKING = "SgPtsPickAndPrintSPage3Backing";
    
    public void initTaskFlow() {
        _logger.info("Pts pick and print page initialization begun");
        initGlobalVariablesPickAndPrint();
        initWorkLaborProdVariablesPickAndPrint();
        createMainRow();
        createLpnRow();
        createPickPrintRow();
        createWorkLocalRow();
        setIsFocusOn("QueueField");
        this.setMaxGrabsValidated(false);
        this.setNbrOfLpnsValidated(false);
    }
    
    public void pickAndPrint() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPickAndPrint");
        Map map = oper.getParamsMap();
        map.put("procName", "PKG_PTS_PICK_AND_PRINT.pick_and_print_1");
        map.put("cursorBlock", "PICK_PRINT");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 1) {
                    String errorMsg = (String) codeList.get(0);
                    String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
                    String langCode = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
                    String msgText = this.getMessage(errorMsg, "E", facilityId, langCode);
                    this.getAcknowledgementPopupIcon().setName("error");
                    this.getAcknowledgementPopupDialogue().setTitle("Error");
                    this.getAcknowledgementOutputText().setValue(msgText);
                    this.getAcknowledgementPopup().show(new RichPopup.PopupHints());
                    return;
                }
            }
        }
        continuePickAndPrint();
    }
    
    public void continuePickAndPrint() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callPickAndPrint2");
        Map map = oper.getParamsMap();
        map.put("procName", "PKG_PTS_PICK_AND_PRINT.pick_and_print_2");
        map.put("cursorBlock", "Pick_Print");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 1) {
                    String errorMsg = (String) codeList.get(0);
                    String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
                    String langCode = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
                    String msgText;
                    if(errorMsg.equals("OTHERS")){
                        msgText = "OTHERS";
                    }
                    else {
                        msgText = this.getMessage(errorMsg, "E", facilityId, langCode);
                    }
                    this.getAcknowledgementPopupIcon().setName("error");
                    this.getAcknowledgementPopupDialogue().setTitle("Error");
                    this.getAcknowledgementOutputText().setValue(msgText);
                    this.getAcknowledgementPopup().show(new RichPopup.PopupHints());
                    return;
                }
            }
        }
        printLabels();
        this.getAcknowledgementPopupDialogue().setTitle("Information");
        this.getAcknowledgementPopupIcon().setName("logo");
        this.getAcknowledgementOutputText().setValue(this.getMessage("SUCCESS_OPER", "E", null, (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
        this.getAcknowledgementPopup().show(new RichPopup.PopupHints());
    }
    
    public void printLabels() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("printLabels");
        oper.execute();
    }

    private void initGlobalVariablesPickAndPrint() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setGlobalVariablesPickAndPrint");
        oper.execute();
    }
    
    private void initWorkLaborProdVariablesPickAndPrint() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("setWorkLaborProdVariablesPickAndPrint");
        oper.execute();
    }
    
    private void createMainRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("createInsertMainRow");
        oper.execute();
    }
    
    private void createLpnRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("createInsertLpnRow");
        oper.execute();
    }
    
    private void createPickPrintRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("createInsertPickPrintRow");
        oper.execute();
    }

    private void createWorkLocalRow() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("createInsertWorkLocalRow");
        oper.execute();
    }

    public void setIsFocusOn(String isFocusOn) {
        this.isFocusOn = isFocusOn;
    }

    public String getIsFocusOn() {
        return isFocusOn;
    }

    public void setNbrOfLpnsValidated(Boolean nbrOfLpnsValidated) {
        this.nbrOfLpnsValidated = nbrOfLpnsValidated;
    }

    public Boolean getNbrOfLpnsValidated() {
        return nbrOfLpnsValidated;
    }

    public void setMaxGrabsValidated(Boolean maxGrabsValidated) {
        this.maxGrabsValidated = maxGrabsValidated;
    }

    public Boolean getMaxGrabsValidated() {
        return maxGrabsValidated;
    }

    public void setAcknowledgementOutputText(RichOutputText acknowledgementOutputText) {
        this.acknowledgementOutputText = acknowledgementOutputText;
    }

    public RichOutputText getAcknowledgementOutputText() {
        return acknowledgementOutputText;
    }

    public void setAcknowledgementPopup(RichPopup acknowledgementPopup) {
        this.acknowledgementPopup = acknowledgementPopup;
    }

    public RichPopup getAcknowledgementPopup() {
        return acknowledgementPopup;
    }

    public void setAcknowledgementPopUpLink(RichLink acknowledgementPopUpLink) {
        this.acknowledgementPopUpLink = acknowledgementPopUpLink;
    }

    public RichLink getAcknowledgementPopUpLink() {
        return acknowledgementPopUpLink;
    }

    public void setAcknowledgementPopupIcon(RichIcon acknowledgementPopupIcon) {
        this.acknowledgementPopupIcon = acknowledgementPopupIcon;
    }

    public RichIcon getAcknowledgementPopupIcon() {
        return acknowledgementPopupIcon;
    }

    public void setAcknowledgementPopupDialogue(RichDialog acknowledgementPopupDialogue) {
        this.acknowledgementPopupDialogue = acknowledgementPopupDialogue;
    }

    public RichDialog getAcknowledgementPopupDialogue() {
        return acknowledgementPopupDialogue;
    }
}
