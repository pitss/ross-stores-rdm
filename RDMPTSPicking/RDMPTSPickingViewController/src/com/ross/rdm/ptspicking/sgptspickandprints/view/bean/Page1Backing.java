package com.ross.rdm.ptspicking.sgptspickandprints.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.sql.Types;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.ApplicationModule;

import org.apache.commons.lang.StringUtils;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMPTSPickingBackingBean {
    private final static String PAGE_FLOW_BEAN = "SgPtsPickAndPrintSBean";
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;
    private static ADFLogger _logger = ADFLogger.createADFLogger(Page1Backing.class);
    
    
    private RichLink f3Link;
    private RichLink f6Link;
    
    private RichPopup nestedPopup;
    private RichOutputText nestedItem;
    private RichIcon nestedIcon;
    private RichPanelFormLayout nestedForm;
    private RichLink okayPopup;
    
    private RichPopup queueLovPopup;
    private RichInputText queue;
    private RichInputText containerId;
    private RichIcon queueIcon;
    private RichIcon containerIdIcon;
    private RichPanelFormLayout panelForm;
    private RichLink page2Link;

    public Page1Backing() {

    }

    public void onChangedQueueName(ValueChangeEvent vce) {
        _logger.info("onChangedQueueName() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String queue = ((String) vce.getNewValue());
            this.validateQueueName(queue);
            
        }
    }
    
    public void validateQueueName(String queue){
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVQueueName");
            oper.execute();

            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    List<Object> codeList = (List<Object>) oper.getResult();
                    if (codeList.size() == 2) {
                        this.showErrorPanel((String) codeList.get(1), (String) codeList.get(0));
                        this.setErrorUi(this.getQueueIcon(), this.getQueue());
                        this.setFocusQueue();
                        _logger.info("onChangedQueueName() End");
                        return;
                    }
                    this.getQueueIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getQueueIcon());
                    this.hideMessagesPanel();
                    this.removeErrorOfAllFields();
                    this.setFocusContainerId();
                    this.refreshContentOfUIComponent(this.getPanelForm());
                }
            }
            ADFUtils.setBoundAttributeValue("QueueName1", queue);
            _logger.info("onChangedQueueName() End");
    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String cId = ((String) vce.getNewValue());
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVContainerId");
            Map map = oper.getParamsMap();
            map.put("containerId", cId);
            
            oper.execute();

            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    List<Object> codeList = (List<Object>) oper.getResult();
                    if (codeList.size() > 1) {
                        if("nestedPopup".equals(codeList.get(0))){
                            this.getNestedItem().setValue((String) codeList.get(1) + " " + codeList.get(2));
                            openNestedPopup();
                            return;
                        }
                        else {String errorMsg = (String) codeList.get(1);
                        if (null != codeList.get(2) && !((String) codeList.get(2)).isEmpty()){
                            errorMsg = errorMsg + " " + (String) codeList.get(2);
                        }
                        this.getContainerId().resetValue();
                        ADFUtils.setBoundAttributeValue("ContainerId", null);
                        showErrorPanel(errorMsg, (String) codeList.get(1));
                        this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
                        this.setFocusContainerId();
                        _logger.info("onChangedContainerId() End");
                        return;
                    }
                    }
                    this.getContainerIdIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getContainerIdIcon());
                    this.hideMessagesPanel();
                    this.removeErrorOfAllFields();
                    this.refreshContentOfUIComponent(this.getPanelForm());
                    this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("NbrOfLpnsField");
                    //ADFUtils.invokeAction("Page2");
                    ActionEvent actionEvent = new ActionEvent(this.getPage2Link());
                    actionEvent.queue();                    
                }
            }   
            _logger.info("onChangedContainerId() End");
        }
    }


    /******************************************************************************************
     *                                  TrKeyIn and performKey
     ******************************************************************************************/


    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        if ("f3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.invokeAction("backGlobalHome");
        } else if ("F6".equalsIgnoreCase(key)) {
            ADFUtils.findOperation("executeQueryWithParams").execute();
            this.removeErrorOfAllFields();
            this.hideMessagesPanel();
            this.getQueueLovPopup().show(new RichPopup.PopupHints());
            setFocusQueueLovPopup();
            this.refreshContentOfUIComponent(this.getAllMessagesPanel());
        }
    }
    
    public void openNestedPopup(){
        this.removeErrorOfAllFields();
        this.hideMessagesPanel(); 
        this.getNestedPopup().show(new RichPopup.PopupHints());
        setFocusNestedPopup();
        this.refreshContentOfUIComponent(this.getAllMessagesPanel());
    }
    
    public void performKey(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F6_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF6Link());
                actionEvent.queue();
            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                RichInputText field = (RichInputText) clientEvent.getComponent();
                String currentFieldId = field.getId();
                if ((StringUtils.isEmpty((String) field.getValue()) && StringUtils.isEmpty(submittedValue)) ||
                    submittedValue.equals(field.getValue())) {
                    if ("con13".equals(currentFieldId)) {
                        this.onChangedQueueName(new ValueChangeEvent(this.getQueue(), null, submittedValue));
                    } else if ("con1".equals(currentFieldId)) {
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                    }
                }
            }
        }
    }
    
    
    /******************************************************************************************
     *                                  Popup window methods
     ******************************************************************************************/

    
    public void performKeySelectQueue(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
                if (StringUtils.isNotEmpty(submittedValue)) {
                    ADFUtils.setBoundAttributeValue("Queue", submittedValue);
                    validateQueueName(submittedValue);
                    getQueueLovPopup().hide();

                }
            }
        }
    }
    
    public void performKeyNestedPopup(ClientEvent clientEvent) {
            if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
                Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
                if (F1_KEY_CODE.equals(keyPressed)) {
                    nestedPopupActionListener();
                        getNestedPopup().hide();
                        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("NbrOfLpnsField");
                        ADFUtils.invokeAction("Page2");
                    }
                }
    }
    
    public void nestedPopupActionListener(){
        getNestedPopup().hide();
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("NbrOfLpnsField");
        ADFUtils.invokeAction("Page2");
    }
    
    public void nestedPopupActionListener(ActionEvent actionEvent){
        nestedPopupActionListener();
    }
            
    
    private void setFocusQueue() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("QueueField");
        this.setFocusOnUIComponent(this.getQueue());
    }

    private void setFocusContainerId() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("ContainerIdField");
        this.setFocusOnUIComponent(this.getContainerId());
    }
    
    private void setFocusQueueLovPopup() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("QueueLovPopup");
    }
    
    private void setFocusNestedPopup() {
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("NestedPopup");
    }
    
    private void removeErrorOfAllFields() {
        this.removeErrorStyleToComponent(getQueue());
        this.removeErrorStyleToComponent(getContainerId());
        this.getQueueIcon().setName("required");
        this.getContainerIdIcon().setName("required");
        this.refreshContentOfUIComponent(getQueueIcon());
        this.refreshContentOfUIComponent(getContainerIdIcon());
    }


    /******************************************************************************************
     *                                  Utility methods
     ******************************************************************************************/


    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f6ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_pick_print_s_MAIN_F6,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_pick_print_s)) {
            this.trKeyIn("F6");
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
    }
    
    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (this.getSgPtsPickAndPrintSPageFlowBean() != null) {
            if("ContainerIdField".equals(this.getSgPtsPickAndPrintSPageFlowBean().getIsFocusOn())){
                this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("ContainerIdField");
                this.setFocusOnUIComponent(this.getContainerId());

            }
        }
        _logger.info("onRegionLoad End");
    }


    /******************************************************************************************
     *                                  Getters and Setters
     ******************************************************************************************/


    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF6Link(RichLink f6Link) {
        this.f6Link = f6Link;
    }

    public RichLink getF6Link() {
        return f6Link;
    }

    public void setQueueLovPopup(RichPopup queueLovPopup) {
        this.queueLovPopup = queueLovPopup;
    }

    public RichPopup getQueueLovPopup() {
        return queueLovPopup;
    }

    public void setQueue(RichInputText queue) {
        this.queue = queue;
    }

    public RichInputText getQueue() {
        return queue;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setQueueIcon(RichIcon queueIcon) {
        this.queueIcon = queueIcon;
    }

    public RichIcon getQueueIcon() {
        return queueIcon;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }
    
    private SgPtsPickAndPrintSBean getSgPtsPickAndPrintSPageFlowBean() {
        return ((SgPtsPickAndPrintSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }

    public void setPanelForm(RichPanelFormLayout panelForm) {
        this.panelForm = panelForm;
    }

    public RichPanelFormLayout getPanelForm() {
        return panelForm;
    }

    public static void setLogger(ADFLogger _logger) {
        Page1Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setNestedPopup(RichPopup nestedPopup) {
        this.nestedPopup = nestedPopup;
    }

    public RichPopup getNestedPopup() {
        return nestedPopup;
    }


    public void setNestedItem(RichOutputText nestedItem) {
        this.nestedItem = nestedItem;
    }

    public RichOutputText getNestedItem() {
        return nestedItem;
    }

    public void setNestedIcon(RichIcon nestedIcon) {
        this.nestedIcon = nestedIcon;
    }

    public RichIcon getNestedIcon() {
        return nestedIcon;
    }

    public void setNestedForm(RichPanelFormLayout nestedForm) {
        this.nestedForm = nestedForm;
    }

    public RichPanelFormLayout getNestedForm() {
        return nestedForm;
    }

    public void setOkayPopup(RichLink okayPopup) {
        this.okayPopup = okayPopup;
    }

    public RichLink getOkayPopup() {
        return okayPopup;
    }

    public void setPage2Link(RichLink page2Link) {
        this.page2Link = page2Link;
    }

    public RichLink getPage2Link() {
        return page2Link;
    }
}
