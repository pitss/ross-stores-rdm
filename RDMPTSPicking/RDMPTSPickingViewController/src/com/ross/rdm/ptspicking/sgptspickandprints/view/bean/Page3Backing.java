package com.ross.rdm.ptspicking.sgptspickandprints.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.common.view.constants.RoleBasedAccessConstants;
import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.render.ClientEvent;

// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page3.jspx
// ---
// ---------------------------------------------------------------------
public class Page3Backing extends RDMPTSPickingBackingBean {
    private RichPanelGroupLayout allMessagesPanel;
    private RichIcon errorWarnInfoIcon;
    private RichOutputText errorWarnInfoMessage;

    private static ADFLogger _logger = ADFLogger.createADFLogger(Page3Backing.class);

    private RichLink f3Link;
    private RichLink f4Link;
    private RichInputText containerId;
    private RichIcon containerIdIcon;
    private RichPanelFormLayout panelForm;
    
    private RichPopup acknowledgementPopup;
    private RichOutputText acknowledgementOutputText;
    private RichLink acknowledgementPopUpLink;

    private final static String LANGUAGE_CODE_ATTR = "LanguageCode";
    private final static String PAGE_FLOW_BEAN = "SgPtsPickAndPrintSBean";


    public Page3Backing() {

    }

    public void onChangedContainerId(ValueChangeEvent vce) {
        _logger.info("onChangedContainerId() Start");
        this.updateModel(vce);
        if (null != vce.getNewValue() && !"".equalsIgnoreCase((String) vce.getNewValue())) {
            String cId = ((String) vce.getNewValue());
            OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callVLpn");
            Map map = oper.getParamsMap();
            map.put("containerId", cId);
            oper.execute();

            if (oper.getErrors().isEmpty()) {
                if (null != oper.getResult()) {
                    List<Object> codeList = (List<Object>) oper.getResult();
                    if (codeList.size() > 1) {
                        String errorMsg = (String) codeList.get(1);

                        this.showErrorPanel(errorMsg, (String) codeList.get(0));
                        this.setErrorUi(this.getContainerIdIcon(), this.getContainerId());
                        ADFUtils.setBoundAttributeValue("ContainerId", null);
                        this.getContainerId().resetValue();
                        this.refreshContentOfUIComponent(this.getPanelForm());
                        _logger.info("onChangedContainerId() End");
                        return;
                    }
                    this.getContainerIdIcon().setName("required");
                    this.refreshContentOfUIComponent(this.getContainerIdIcon());
                    this.hideMessagesPanel();
                    this.removeErrorOfAllFields();
                    ADFUtils.setBoundAttributeValue("ContainerId", null);
                    this.getContainerId().resetValue();
                    this.refreshContentOfUIComponent(this.getPanelForm());
                }
            }
            _logger.info("onChangedContainerId() End");
        }
    }

    private void trKeyIn(String key) {
        _logger.info("trKeyIn() Start");
        if ("f3".equalsIgnoreCase(key)) {
            _logger.info("trKeyIn() End");
            ADFUtils.setBoundAttributeValue("ContainerId", null);
            ADFUtils.setBoundAttributeValue("NbrOfLpns", BigDecimal.ZERO);
            DCIteratorBinding iterBind = ADFUtils.findIterator("LpnDataGroupViewIterator");
            iterBind.clearForRecreate();
            this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("NbrOfLpnsField");
            
            ADFUtils.invokeAction("Page2");
        } else if ("F4".equalsIgnoreCase(key)) {
            if(null != getContainerIdValue()){
                this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, getContainerIdValue()));
                if(this.getErrorWarnInfoIcon().isVisible()){
                    return;
                }
            }
            if(null != getNbrOfLpnsValue() && BigDecimal.ZERO.compareTo(getNbrOfLpnsValue()) != 0){
                ADFUtils.setBoundAttributeValue("ContQty", getNbrOfLpnsValue());
                this.getSgPtsPickAndPrintSPageFlowBean().pickAndPrint();
            }
            else{
                String errorMsg = this.getMessage("INC_LPN_COUNT", "E", null, this.getLangCodeAttrValue());
                this.showErrorPanel(errorMsg, "E");
            }
        }
    }

    public void performKey(ClientEvent clientEvent) {
        String submittedValue = null;
        if (clientEvent !=null) 
            submittedValue = (String) clientEvent.getParameters().get("submittedValue");
        String currentFocus = this.getSgPtsPickAndPrintSPageFlowBean().getIsFocusOn();
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F3_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getF3Link());
                actionEvent.queue();
            } else if (F4_KEY_CODE.equals(keyPressed)) {

                ActionEvent actionEvent = new ActionEvent(this.getF4Link());
                actionEvent.queue();

            } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed)) {
                if ("NbrOfLpnsField".equals(currentFocus)) {
                    if (null != submittedValue && !submittedValue.isEmpty()) {
                        this.onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
                        this.refreshContentOfUIComponent(this.getPanelForm());
                    }
                }
            }
        }
    }
    
    public void performKeyAcknowledgementPopup(ClientEvent clientEvent) {
        if (clientEvent != null && clientEvent.getParameters().containsKey("keyPressed")) {
            Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
            if (F1_KEY_CODE.equals(keyPressed)) {
                ActionEvent actionEvent = new ActionEvent(this.getAcknowledgementPopUpLink());
                actionEvent.queue();
            }
        }
    }
    
    public void yesLinkAcknowledgementPopupActionListener(ActionEvent actionEvent) {
        this.getSgPtsPickAndPrintSPageFlowBean().getAcknowledgementPopup().hide();
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callLockUnlockPicks");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 2) {
                    this.showErrorPanel((String) codeList.get(1), (String) codeList.get(0));
                    return;
                }
                clearPickAndPrint();
            }
        }
    }
    
    public void lockUnlockPicks() {
        OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callLockUnlockPicks");
        oper.execute();

        if (oper.getErrors().isEmpty()) {
            if (null != oper.getResult()) {
                List<Object> codeList = (List<Object>) oper.getResult();
                if (codeList.size() == 1) {
                    String errorMsg = (String) codeList.get(0);
                    String facilityId = (String) ADFUtils.getBoundAttributeValue("FacilityId");
                    String langCode = (String) ADFUtils.getBoundAttributeValue("LanguageCode");
                    String msgText = this.getMessage(errorMsg, "E", facilityId, langCode);
                    this.getAcknowledgementOutputText().setValue(msgText);
                    this.getAcknowledgementPopup().show(new RichPopup.PopupHints());
                    return;
                }
            }
        }
    }
    
    public void clearPickAndPrint() {
        lockUnlockPicks();
        ADFUtils.setBoundAttributeValue("PickFromContainerId", null);
        ADFUtils.setBoundAttributeValue("ItemId1", null);
        ADFUtils.setBoundAttributeValue("ItemId", null);
        ADFUtils.setBoundAttributeValue("UnitQty", null);
        ADFUtils.setBoundAttributeValue("InnerPackQty", null);
        ADFUtils.setBoundAttributeValue("ItemDesc", null);
        ADFUtils.setBoundAttributeValue("UnitPickSystemCode", null);
        ADFUtils.setBoundAttributeValue("InnerPackQty1", null);
        ADFUtils.setBoundAttributeValue("ShippingConveyable", null);
        ADFUtils.setBoundAttributeValue("ContQty", null);
        ADFUtils.setBoundAttributeValue("UnitQtyWl", null);
        ADFUtils.setBoundAttributeValue("RoutePrintOrder", null);
        ADFUtils.setBoundAttributeValue("MaxGrabsWl", null);
        ADFUtils.setBoundAttributeValue("NbrOfLpns", null);
        ADFUtils.setBoundAttributeValue("MaxGrabs", null);
        ADFUtils.setBoundAttributeValue("NbrOfLpnString", null);
        ADFUtils.setBoundAttributeValue("MaxGrabString", null);
        ADFUtils.setBoundAttributeValue("ContainerId1", null);
        ADFUtils.setBoundAttributeValue("ContainerId", null);
        this.getSgPtsPickAndPrintSPageFlowBean().setIsFocusOn("ContainerIdField");
        ADFUtils.invokeAction("Page1");
    }

    public void onRegionLoad(ComponentSystemEvent componentSystemEvent) {
        _logger.info("onRegionLoad Start");
        if (this.getSgPtsPickAndPrintSPageFlowBean() != null) {
            this.setFocusOnUIComponent(this.getContainerId());

        }
        _logger.info("onRegionLoad End");
    }
    
    private SgPtsPickAndPrintSBean getSgPtsPickAndPrintSPageFlowBean() {
        return ((SgPtsPickAndPrintSBean) this.getPageFlowBean(PAGE_FLOW_BEAN));
    }
    
    private String getContainerIdValue() {
        return (String) ADFUtils.getBoundAttributeValue("ContainerId");
    }
    
    private BigDecimal getNbrOfLpnsValue() {
        return (BigDecimal) ADFUtils.getBoundAttributeValue("NbrOfLpns");
    }
    
    private String getLangCodeAttrValue() {
        return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
    }

    private void removeErrorOfAllFields() {
        this.removeErrorStyleToComponent(getContainerId());
        this.getContainerIdIcon().setName("required");
        this.refreshContentOfUIComponent(getContainerIdIcon());
    }

    public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel) {
        this.allMessagesPanel = allMessagesPanel;
    }

    public RichPanelGroupLayout getAllMessagesPanel() {
        return allMessagesPanel;
    }

    public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon) {
        this.errorWarnInfoIcon = errorWarnInfoIcon;
    }

    public RichIcon getErrorWarnInfoIcon() {
        return errorWarnInfoIcon;
    }

    public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage) {
        this.errorWarnInfoMessage = errorWarnInfoMessage;
    }

    public RichOutputText getErrorWarnInfoMessage() {
        return errorWarnInfoMessage;
    }

    public static void setLogger(ADFLogger _logger) {
        Page3Backing._logger = _logger;
    }

    public static ADFLogger getLogger() {
        return _logger;
    }

    public void setF3Link(RichLink f3Link) {
        this.f3Link = f3Link;
    }

    public RichLink getF3Link() {
        return f3Link;
    }

    public void setF4Link(RichLink f4Link) {
        this.f4Link = f4Link;
    }

    public RichLink getF4Link() {
        return f4Link;
    }

    public void setContainerId(RichInputText containerId) {
        this.containerId = containerId;
    }

    public RichInputText getContainerId() {
        return containerId;
    }

    public void setContainerIdIcon(RichIcon containerIdIcon) {
        this.containerIdIcon = containerIdIcon;
    }

    public RichIcon getContainerIdIcon() {
        return containerIdIcon;
    }

    public void setPanelForm(RichPanelFormLayout panelForm) {
        this.panelForm = panelForm;
    }

    public RichPanelFormLayout getPanelForm() {
        return panelForm;
    }

    public void f3ActionListener(ActionEvent actionEvent) {
        this.trKeyIn("F3");
    }

    public void f4ActionListener(ActionEvent actionEvent) {
        if (this.callCheckScreenOptionPrivilege(RoleBasedAccessConstants.OPTION_NAME_sg_pts_pick_print_s_LPN_F4,
                                                RoleBasedAccessConstants.FORM_NAME_sg_pts_pick_print_s)) {
            this.trKeyIn("F4");
        } else {
            showMessagesPanel("E", getMessageByCode("NOT_ALLOWED"));
        }
    }

    public void setAcknowledgementPopup(RichPopup acknowledgementPopup) {
        this.acknowledgementPopup = acknowledgementPopup;
    }

    public RichPopup getAcknowledgementPopup() {
        return acknowledgementPopup;
    }

    public void setAcknowledgementOutputText(RichOutputText acknowledgementOutputText) {
        this.acknowledgementOutputText = acknowledgementOutputText;
    }

    public RichOutputText getAcknowledgementOutputText() {
        return acknowledgementOutputText;
    }

    public void setAcknowledgementPopUpLink(RichLink acknowledgementPopUpLink) {
        this.acknowledgementPopUpLink = acknowledgementPopUpLink;
    }

    public RichLink getAcknowledgementPopUpLink() {
        return acknowledgementPopUpLink;
    }

}
