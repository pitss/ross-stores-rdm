package com.ross.rdm.ptspicking.sgcontainerreinducts.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;
import com.ross.rdm.ptspicking.sgcontainerreinducts.model.views.SgContainerReInductSBReInductViewRowImpl;
import com.ross.rdm.ptspicking.view.framework.RDMPTSPickingBackingBean;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichIcon;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.render.ClientEvent;


// ---------------------------------------------------------------------
// ---
// ---    Backing Bean for Page1.jspx
// ---
// ---------------------------------------------------------------------
public class Page1Backing extends RDMPTSPickingBackingBean
{

  private final static String B_RE_INDUCT_ITERATOR = "SgContainerReInductSBReInductViewIterator";
  private final static String SG_CONTAINER_RE_INDUCT_FLOW_BEAN = "SgContainerReInductSBean";
  private final static String CONTAINER_ID_ATTR = "ContainerId";
  private final static String TO_LOCATION_ID_ATTR = "ToLocationId";
  private final static String LANGUAGE_CODE_ATTR = "LanguageCode";

  private RichPanelGroupLayout allMessagesPanel;
  private RichIcon             errorWarnInfoIcon;
  private RichOutputText       errorWarnInfoMessage;
  private static ADFLogger     _logger = ADFLogger.createADFLogger(Page1Backing.class);

  private RichPanelFormLayout containerReInductPanel;
  private RichLink            f3Link;
  private RichLink            f4Link;

  //Inputs
  private RichInputText containerId;
  private RichIcon      containerIdIcon;
  private RichInputText toLocationId;
  private RichIcon      toLocationIdIcon;
  private RichLink      hiddenContainerIdLink;
  private RichLink      hiddenLocationIdLink;

  public Page1Backing()
  {

  }

  public void onChangedContainerId(ValueChangeEvent vce)
  {
    if (!isNavigationExecuted())
    {
      updateModel(vce);
      SgContainerReInductSBean pfBean = this.getSgContainerReInductSPageFlowBean();
      pfBean.setLinkPressedPage1(null);
      ActionEvent actionEvent = new ActionEvent(this.getHiddenContainerIdLink());
      actionEvent.queue();
    }
  }
  
  
  public String onChangedContainerIdAction()
  {
    boolean validFromContainerId = false;
    String containerId = (String) ADFUtils.getBoundAttributeValue("ContainerId");
    if (containerId != null && !containerId.isEmpty())
    {
      validFromContainerId = callValidateContainerId(containerId);
      if (validFromContainerId)
      {
        setFocusToLocatonId();
        hideMessagesPanel();
        this.removeErrorOfField(getContainerId(), getContainerIdIcon());
      }
    } else
    {
      setErrorOnField(this.getContainerId(), getContainerIdIcon());
      showMessagesPanel("E", this.getMessage("INV_CID", "E", null, this.getLangCodeAttrValue()));
      setFocusContainerId();
    }
    getSgContainerReInductSPageFlowBean().setIsContainerIdValidated(validFromContainerId);
    return null;
  }

  private Boolean callValidateContainerId(String containerId)
  {
    OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callValidateContainerId");
    oper.getParamsMap().put("pContainerId", containerId);

    oper.execute();
    if (oper.getErrors().isEmpty())
    {
      if (null != oper.getResult())
      {
        List<String> responseList = (List<String>) oper.getResult();
        if (responseList != null && responseList.size() >= 2)
        {
          if ("E".equals(responseList.get(0)))
          {
            this.showMessagesPanel("E", this.getMessage(responseList.get(1), responseList.get(0), (String) ADFUtils.getBoundAttributeValue("FacilityId"), (String) ADFUtils.getBoundAttributeValue("LanguageCode")));
            setErrorOnField(this.getContainerId(), getContainerIdIcon());
            selectTextOnUIComponent(this.getContainerId());
            return Boolean.FALSE;
          }
        }
      }
    }
    return Boolean.TRUE;
  }

  public void onChangedToLocationId(ValueChangeEvent vce)
  {
    if (!isNavigationExecuted())
    {
      updateModel(vce);
      SgContainerReInductSBean pfBean = this.getSgContainerReInductSPageFlowBean();
      pfBean.setLinkPressedPage1(null);
      ActionEvent actionEvent = new ActionEvent(this.getHiddenLocationIdLink());
      actionEvent.queue();
    }
  }
  
  public String onChangedLocationIdAction()
  {
    String locationId = (String) ADFUtils.getBoundAttributeValue("ToLocationId");
    if (locationId != null && !locationId.isEmpty())
    {
      Boolean validToLocationId = callProcessNewToLoc(this.getContainerIdAttrValue(), this.getToLocationIdAttrValue());
      if (validToLocationId)
      {
        setFocusContainerId();
        hideMessagesPanel();
        this.removeErrorOfField(getToLocationId(), getToLocationIdIcon());
        successResetForm();
      }
    } else
    {
      setErrorOnField(this.getToLocationId(), getToLocationIdIcon());
      showMessagesPanel("E", this.getMessage("SG_INV_LOC", "E", null, this.getLangCodeAttrValue()));
    }
    return null;
  }

  public void performKey(ClientEvent clientEvent)
  {
    String submittedValue = (String) clientEvent.getParameters().get("submittedValue");
    String currentFocus = getSgContainerReInductSPageFlowBean().getIsFocusOn();

    if (clientEvent.getParameters().containsKey("keyPressed"))
    {
      Double keyPressed = (Double) clientEvent.getParameters().get("keyPressed");
      if (F3_KEY_CODE.equals(keyPressed))
      {
        ActionEvent actionEvent = new ActionEvent(this.getF3Link());
        actionEvent.queue();
      } else if (F4_KEY_CODE.equals(keyPressed))
      {
        ActionEvent actionEvent = new ActionEvent(this.getF4Link());
        actionEvent.queue();
      } else if (ENTER_KEY_CODE.equals(keyPressed) || TAB_KEY_CODE.equals(keyPressed))
      {
        if (submittedValue != null)
        {
          if ("ContainerId".equals(currentFocus))
          {
            onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, submittedValue));
          } else if ("ToLocationId".equals(currentFocus))
          {
            onChangedToLocationId(new ValueChangeEvent(this.getToLocationId(), null, submittedValue));
          }
        }
      }
      this.refreshContentOfUIComponent(this.getContainerReInductPanel());
    }
  }

  private void trKeyIn(String key)
  {
    String currentFocus = getSgContainerReInductSPageFlowBean().getIsFocusOn();

    if ("F4".equalsIgnoreCase(key))
    {
      if (getContainerId().getValue() == null && "ContainerId".equals(currentFocus))
      {
        setErrorOnField(this.getContainerId(), getContainerIdIcon());
        showMessagesPanel("E", this.getMessage("ENTER_CONT", "E", null, this.getLangCodeAttrValue()));
      } else if (getToLocationId().getValue() == null && "ToLocationId".equals(currentFocus))
      {
        setErrorOnField(this.getToLocationId(), getToLocationIdIcon());
        showMessagesPanel("E", this.getMessage("SG_INV_LOC", "E", null, this.getLangCodeAttrValue()));
      } else
      {
        if ("ContainerId".equals(currentFocus))
        {
          onChangedContainerId(new ValueChangeEvent(this.getContainerId(), null, getContainerId().getValue()));
        } else if ("ToLocationId".equals(currentFocus))
        {
          onChangedToLocationId(new ValueChangeEvent(this.getToLocationId(), null, getToLocationId().getValue()));
        }
      }
    }
  }

  private Boolean callProcessNewToLoc(String containerId, String toLocationId)
  {
    OperationBinding oper = (OperationBinding) ADFUtils.findOperation("callProcessNewToLoc");
    oper.getParamsMap().put("pContainerId", containerId);
    oper.getParamsMap().put("pToLocationId", toLocationId);

    oper.execute();
    if (oper.getErrors().isEmpty())
    {
      if (null != oper.getResult())
      {
        List<String> responseList = (List<String>) oper.getResult();
        if (responseList != null && responseList.size() >= 2)
        {
          if ("E".equals(responseList.get(0)))
          {
            this.showMessagesPanel(responseList.get(0), responseList.get(1));
            setErrorOnField(this.getToLocationId(), getToLocationIdIcon());
            selectTextOnUIComponent(this.getToLocationId());
            return Boolean.FALSE;
          }
        } else if (responseList != null && !responseList.contains("Y"))
        {
          return Boolean.FALSE;
        }
      }
    }
    return Boolean.TRUE;
  }

  private void successResetForm()
  {
    clearFields();
    setFocusContainerId();
    hideMessagesPanel();
    //Remove Error style from containerId
    this.removeErrorOfField(getContainerId(), getContainerIdIcon());
    //Remove Error style from toLocationId
    this.removeErrorOfField(getToLocationId(), getToLocationIdIcon());
  }

  private void clearFields()
  {
    _logger.info("clearFields() Start");
    this.clearCurrentRow();
    this.getContainerId().resetValue();
    this.getToLocationId().resetValue();
    this.refreshContentOfUIComponent(this.getContainerId());
    this.refreshContentOfUIComponent(this.getToLocationId());
    _logger.info("clearFields() End");
  }

  private void clearCurrentRow()
  {
    SgContainerReInductSBReInductViewRowImpl hhRowImpl = this.getSgContainerReInductSBRepackViewRow();
    if (hhRowImpl != null)
    {
      hhRowImpl.setContainerId(null);
      hhRowImpl.setToLocationId(null);
    }
  }

  private void setFocusContainerId()
  {
    this.getSgContainerReInductSPageFlowBean().setIsFocusOn("ContainerId");
    this.setFocusOnUIComponent(this.getContainerId());
  }

  private void setFocusToLocatonId()
  {
    this.getSgContainerReInductSPageFlowBean().setIsFocusOn("ToLocationId");
    this.setFocusOnUIComponent(this.getToLocationId());
  }

  private String getToLocationIdAttrValue()
  {
    return (String) ADFUtils.getBoundAttributeValue(TO_LOCATION_ID_ATTR);
  }

  private String getContainerIdAttrValue()
  {
    return (String) ADFUtils.getBoundAttributeValue(CONTAINER_ID_ATTR);
  }

  public String f3Action()
  {
    clearFields();
    return this.logoutExitBTF();
  }

  public void f4ActionListener(ActionEvent actionEvent)
  {
    if (this.callCheckScreenOptionPrivilege(OPTION_NAME_sg_container_re_induct_s_B_RE_INDUCT_F4, FORM_NAME_sg_container_re_induct_s))
    {
      this.trKeyIn("F4");
    } else
    {
      this.showMessagesPanel("E", this.getMessage("NOT_ALLOWED", null, null, null));
    }
  }

  private String getLangCodeAttrValue()
  {
    return (String) ADFUtils.getBoundAttributeValue(LANGUAGE_CODE_ATTR);
  }

  private SgContainerReInductSBReInductViewRowImpl getSgContainerReInductSBRepackViewRow()
  {
    return (SgContainerReInductSBReInductViewRowImpl) ADFUtils.findIterator(B_RE_INDUCT_ITERATOR).getCurrentRow();
  }

  private SgContainerReInductSBean getSgContainerReInductSPageFlowBean()
  {
    return ((SgContainerReInductSBean) this.getPageFlowBean(SG_CONTAINER_RE_INDUCT_FLOW_BEAN));
  }

  public void setAllMessagesPanel(RichPanelGroupLayout allMessagesPanel)
  {
    this.allMessagesPanel = allMessagesPanel;
  }

  public RichPanelGroupLayout getAllMessagesPanel()
  {
    return allMessagesPanel;
  }

  public void setErrorWarnInfoIcon(RichIcon errorWarnInfoIcon)
  {
    this.errorWarnInfoIcon = errorWarnInfoIcon;
  }

  public RichIcon getErrorWarnInfoIcon()
  {
    return errorWarnInfoIcon;
  }

  public void setErrorWarnInfoMessage(RichOutputText errorWarnInfoMessage)
  {
    this.errorWarnInfoMessage = errorWarnInfoMessage;
  }

  public RichOutputText getErrorWarnInfoMessage()
  {
    return errorWarnInfoMessage;
  }

  public void setContainerReInductPanel(RichPanelFormLayout containerReInductPanel)
  {
    this.containerReInductPanel = containerReInductPanel;
  }

  public RichPanelFormLayout getContainerReInductPanel()
  {
    return containerReInductPanel;
  }

  public void setF3Link(RichLink f3Link)
  {
    this.f3Link = f3Link;
  }

  public RichLink getF3Link()
  {
    return f3Link;
  }

  public void setF4Link(RichLink f4Link)
  {
    this.f4Link = f4Link;
  }

  public RichLink getF4Link()
  {
    return f4Link;
  }

  public void setContainerId(RichInputText containerId)
  {
    this.containerId = containerId;
  }

  public RichInputText getContainerId()
  {
    return containerId;
  }

  public void setContainerIdIcon(RichIcon containerIdIcon)
  {
    this.containerIdIcon = containerIdIcon;
  }

  public RichIcon getContainerIdIcon()
  {
    return containerIdIcon;
  }

  public void setToLocationId(RichInputText toLocationId)
  {
    this.toLocationId = toLocationId;
  }

  public RichInputText getToLocationId()
  {
    return toLocationId;
  }

  public void setToLocationIdIcon(RichIcon toLocationIdIcon)
  {
    this.toLocationIdIcon = toLocationIdIcon;
  }

  public RichIcon getToLocationIdIcon()
  {
    return toLocationIdIcon;
  }

  private boolean isNavigationExecuted()
  {
    boolean        navigation = false;
    BindingContext bindingContext = BindingContext.getCurrent();
    String         currentPageDef = bindingContext.getCurrentBindingsEntry().getName();
    if (currentPageDef != null && !currentPageDef.startsWith("com_ross_rdm_ptspicking_sgcontainerreinducts_view_pageDefs_Page1PageDef"))
    {
      navigation = true;
    }
    return navigation;
  }

  public void setHiddenContainerIdLink(RichLink hiddenContainerIdLink)
  {
    this.hiddenContainerIdLink = hiddenContainerIdLink;
  }

  public RichLink getHiddenContainerIdLink()
  {
    return hiddenContainerIdLink;
  }

  public void setHiddenLocationIdLink(RichLink hiddenLocationIdLink)
  {
    this.hiddenLocationIdLink = hiddenLocationIdLink;
  }

  public RichLink getHiddenLocationIdLink()
  {
    return hiddenLocationIdLink;
  }
}
