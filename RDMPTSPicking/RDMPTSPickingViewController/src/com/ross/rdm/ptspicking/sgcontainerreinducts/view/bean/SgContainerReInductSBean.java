package com.ross.rdm.ptspicking.sgcontainerreinducts.view.bean;

import com.ross.rdm.common.utils.view.util.ADFUtils;

import oracle.adf.share.logging.ADFLogger;

// ---------------------------------------------------------------------
// ---    Bounded Task Flow Bean                                     ---
// ---------------------------------------------------------------------


public class SgContainerReInductSBean
{
  private static ADFLogger _logger = ADFLogger.createADFLogger(SgContainerReInductSBean.class);

  private Boolean isContainerIdValidated;
  private Boolean isToLocationIdValidated;
  private String  isFocusOn;
  private Boolean isError;
  private String  linkPressedPage1;

  public void initTaskFlow()
  {
    initTaskFlowContainerReInduct();
    setIsFocusOn("ContainerId");
    setIsContainerIdValidated(false);
    setIsToLocationIdValidated(false);
  }

  private void initTaskFlowContainerReInduct()
  {
    ADFUtils.findOperation("initTaskFlowContainerReInduct").execute();
  }

  public void setIsContainerIdValidated(Boolean isContainerIdValidated)
  {
    this.isContainerIdValidated = isContainerIdValidated;
  }

  public Boolean getIsContainerIdValidated()
  {
    return isContainerIdValidated;
  }

  public void setIsToLocationIdValidated(Boolean isToLocationIdValidated)
  {
    this.isToLocationIdValidated = isToLocationIdValidated;
  }

  public Boolean getIsToLocationIdValidated()
  {
    return isToLocationIdValidated;
  }

  public void setIsFocusOn(String isFocusOn)
  {
    this.isFocusOn = isFocusOn;
  }

  public String getIsFocusOn()
  {
    return isFocusOn;
  }

  public void setIsError(Boolean isError)
  {
    this.isError = isError;
  }

  public Boolean getIsError()
  {
    return isError;
  }

  public void setLinkPressedPage1(String linkPressedPage1)
  {
    this.linkPressedPage1 = linkPressedPage1;
  }

  public String getLinkPressedPage1()
  {
    return linkPressedPage1;
  }
}
