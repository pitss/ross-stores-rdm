function setSgContainerReInductPage1InitialFocus() {
    var focusId = 'con';
    var focusIdCode = ("[id*='" + focusId + "']");
    setFocusOrSelectOnUIcomp(focusIdCode);
}

function onKeyPressedContainerReInduct(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    var submittedValue = component.getSubmittedValue();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY || keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : submittedValue, item : "ContainerId"
        },
true);
        event.cancel();
    }
}