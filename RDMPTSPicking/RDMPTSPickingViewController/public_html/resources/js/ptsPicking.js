function setFocusRowById(id) {
    var elem = findInputElementById(id);
    $(elem).focus();
    $(elem).select();
}

function onKeyUpSelecTable(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY || keyPressed == AdfKeyStroke.F2_KEY || keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F7_KEY || keyPressed == AdfKeyStroke.F8_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
        event.cancel();
    }
}

function onKeyCloseCidPtsPicking(event) {
    this.onKeyCloseCidHandler(event);
}

function onKeyChZonePtsPicking(event) {
    this.onKeySgPtsChZoneHandler(event);
}

function onKeyGrabLeftPtsPicking(event) {
    this.onKeySgPtsQtyHandler(event);
}

function onKeySgTiLocationPtsPicking(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY || keyPressed == AdfKeyStroke.F4_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed
        },
true);
        event.cancel();
    }
}

function onKeySgZonePtsPicking(event) {
    this.onKeySgPtsPickingHandler(event);
}

function onKeyCidPtsPicking(event) {
    this.onKeySgPtsPickingHandler(event);
}

function onKeyDestCidPtsPicking(event) {
    this.onKeySgPtsPickingHandler(event);
}

function onKeyConfirQtyPtsPicking(event) {
    this.onKeySgPtsPickingHandler(event);
}

function onKeyCloseCidHandler(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        this.pressOnLink("b3", component);
        return;
    }
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}

function onKeySgPtsQtyHandler(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        this.pressOnLink("b3", component);
        return;
    }
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}

function onKeySgPtsChZoneHandler(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY) {
        this.pressOnLink("b3", component);
        return;
    }
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
}

function onKeySgPtsTiLocationHandler(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F3_KEY)
        this.pressOnLink("b3", component);
    else if (keyPressed == AdfKeyStroke.F4_KEY)
        this.pressOnLink("b4", component);
}

function onKeySgPtsPickingHandler(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.ENTER_KEY || keyPressed == AdfKeyStroke.TAB_KEY || keyPressed == AdfKeyStroke.ARROWDOWN_KEY) {
        AdfCustomEvent.queue(component, "customKeyEvent", 
        {
            keyPressed : keyPressed, submittedValue : component.getSubmittedValue()
        },
true);
        event.cancel();
    }
    else {
        switch (keyPressed) {
            case AdfKeyStroke.F1_KEY:
                this.pressOnLink('f1l', component);
                break;
            case AdfKeyStroke.F2_KEY:
                this.pressOnLink('l4', component);
                break;
            case AdfKeyStroke.F3_KEY:
                this.pressOnLink('l5', component);
                break;
            case AdfKeyStroke.F4_KEY:
                this.pressOnLink('l6', component);
                break;
            case AdfKeyStroke.F5_KEY:
                this.pressOnLink('b5', component);
                break;
            default :
        }
    }
}

function yesNoZoneSelect(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l1', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('l2', component);
    event.cancel();
}

function yesNoEmptyCid(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l3', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('l7', component);
    event.cancel();
}

function checkCloseDialog(event) {
    event.cancel();
}

function yesNoCloseCid(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l3s', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('l7s', component);
    event.cancel();
}

function yesNoEmptyCntr(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l42', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('l14', component);
    event.cancel();
}

function yesNoDestExist(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l9', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('l8', component);
    event.cancel();
}

function yesNoEmptyLpn(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('l10', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('l11', component);
    event.cancel();
}

function onKeyOkNoPicksFwd(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('okLink', component);
    event.cancel();
}

function onKeyOkNoPicksZn(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('znLink', component);
    event.cancel();
}

function yesNoApproval(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('yesApprvl', component);
    else if (keyPressed == AdfKeyStroke.F2_KEY)
        this.pressOnLink('noApprvl', component);
    event.cancel();
}

function okWarningQty(event) {
    var component = event.getSource();
    var keyPressed = event.getKeyCode();
    if (keyPressed == AdfKeyStroke.F1_KEY)
        this.pressOnLink('okLink', component);
    event.cancel();
}

function pressOnLink(linkId, component) {
    AdfActionEvent.queue(component.findComponent(linkId), true);
}

function onOpenApprovalPopup(event) {
    OnOpen(event, 'yesApprvl');
}

function onOpenWarningPopup(event) {
    OnOpen(event, 'okLink');
}

function onOpenZonePopup(event) {
    OnOpen(event, 'l2');
}

function onOpenEmptyCidPopup(event) {
    OnOpen(event, 'l7');
}

function onOpenCloseCidPopup(event) {
    OnOpen(event, 'l7s');
}

function onOpenDestExistPopup(event) {
    OnOpen(event, 'l8');
}

function onOpenEmptyCntrPopup(event) {
    OnOpen(event, 'l14');
}

function onOpenEmptyLpnPopup(event) {
    OnOpen(event, 'l11');
}

function onOpenNoPicksFwdPopup(event) {
    OnOpen(event, 'okLink');
}

function onOpenNoPicksZnPopup(event) {
    OnOpen(event, 'znLink');
}

function setZoneSelectFocus() {
    setTimeout(function () {
        var tableId = 'tbb';
        var table = ("[id*='" + tableId + "']");
        SetFocusOnUIcomp(table);
    },
0);
}